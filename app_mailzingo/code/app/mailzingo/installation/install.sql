-- phpMyAdmin SQL Dump
-- version 4.4.14
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Feb 14, 2017 at 01:45 PM
-- Server version: 5.6.26
-- PHP Version: 5.6.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `smartmailer`
--

-- --------------------------------------------------------

--
-- Table structure for table `ci_sessions`
--

CREATE TABLE IF NOT EXISTS `ci_sessions` (
  `session_id` varchar(40) NOT NULL DEFAULT '0',
  `ip_address` varchar(45) NOT NULL DEFAULT '0',
  `user_agent` varchar(255) NOT NULL,
  `last_activity` int(10) unsigned NOT NULL DEFAULT '0',
  `user_data` longtext NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ci_sessions`
--

INSERT INTO `ci_sessions` (`session_id`, `ip_address`, `user_agent`, `last_activity`, `user_data`) VALUES
('34fbe221e08708e8d40e76a9325444b7', '192.168.0.127', 'Mozilla/5.0 (Windows NT 6.1; rv:51.0) Gecko/20100101 Firefox/51.0', 1487068316, 'a:7:{s:9:"user_data";s:0:"";s:6:"mem_id";s:1:"1";s:8:"username";s:9:"coderjakc";s:11:"profile_pic";N;s:13:"business_name";N;s:5:"email";s:20:"coderjack9@gmail.com";s:18:"newsletter_session";a:1:{s:11:"editor_type";s:6:"inline";}}'),
('463299aeeba55ba59c566908bf041395', '192.168.0.127', 'Mozilla/5.0 (Windows NT 6.1; rv:51.0) Gecko/20100101 Firefox/51.0', 1487069007, ''),
('a4b154d4ced05a801f1209f3ba550895', '192.168.0.128', 'Mozilla/5.0 (Windows NT 6.3; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/56.0.2924.87 Safari/537.36', 1487074601, 'a:2:{s:9:"user_data";s:0:"";s:5:"step1";s:8:"complete";}');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_autoresponder`
--

CREATE TABLE IF NOT EXISTS `tbl_autoresponder` (
  `id` int(11) NOT NULL,
  `member_id` int(11) NOT NULL,
  `from_name` varchar(255) NOT NULL,
  `from_email` varchar(255) NOT NULL,
  `reply_to` varchar(255) NOT NULL,
  `auto_name` varchar(255) NOT NULL,
  `subject` varchar(255) NOT NULL,
  `template_id` int(11) DEFAULT NULL,
  `template_text` longtext NOT NULL,
  `image` varchar(255) DEFAULT NULL,
  `in_campaign_id` varchar(255) NOT NULL,
  `mail_day` int(11) DEFAULT NULL,
  `enable_on` varchar(255) DEFAULT NULL,
  `send_time` time NOT NULL,
  `status` enum('active','inactive') NOT NULL DEFAULT 'active',
  `add_time` int(11) NOT NULL,
  `ip` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_campaign`
--

CREATE TABLE IF NOT EXISTS `tbl_campaign` (
  `campaign_id` bigint(20) NOT NULL,
  `campaign_name` varchar(255) NOT NULL,
  `campaign_description` text,
  `member_id` bigint(20) NOT NULL,
  `add_time` int(11) NOT NULL,
  `ip` varchar(255) NOT NULL,
  `status` enum('active','inactive') NOT NULL DEFAULT 'active'
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_campaign`
--

INSERT INTO `tbl_campaign` (`campaign_id`, `campaign_name`, `campaign_description`, `member_id`, `add_time`, `ip`, `status`) VALUES
(1, 'bouncecheck', NULL, 1, 1484893711, '192.168.0.127', 'active'),
(2, 'sdsd', NULL, 1, 1486458713, '192.168.0.127', 'active');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_contact`
--

CREATE TABLE IF NOT EXISTS `tbl_contact` (
  `id` int(11) NOT NULL,
  `member_id` int(11) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `email` varchar(255) NOT NULL,
  `campaign_id` int(11) NOT NULL,
  `status` enum('active','inactive') NOT NULL DEFAULT 'active',
  `ip` varchar(255) NOT NULL,
  `add_time` int(11) NOT NULL,
  `address` text,
  `city` varchar(255) DEFAULT NULL,
  `state` varchar(255) DEFAULT NULL,
  `country` varchar(255) DEFAULT NULL,
  `zip` varchar(255) DEFAULT NULL,
  `phone` varchar(255) DEFAULT NULL,
  `custom_1` varchar(255) DEFAULT NULL,
  `custom_2` varchar(255) DEFAULT NULL,
  `custom_3` varchar(255) DEFAULT NULL,
  `add_from` enum('import','add','form') NOT NULL DEFAULT 'import',
  `varification_code` varchar(255) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_contact`
--

INSERT INTO `tbl_contact` (`id`, `member_id`, `name`, `email`, `campaign_id`, `status`, `ip`, `add_time`, `address`, `city`, `state`, `country`, `zip`, `phone`, `custom_1`, `custom_2`, `custom_3`, `add_from`, `varification_code`) VALUES
(1, 1, 'coderjack1', 'team@saglusinfo.com', 1, 'active', '192.168.0.127', 1485939096, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'form', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_cron_urls`
--

CREATE TABLE IF NOT EXISTS `tbl_cron_urls` (
  `id` int(11) NOT NULL,
  `time` datetime NOT NULL,
  `newsletter_id` int(11) NOT NULL,
  `url` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_email_queue`
--

CREATE TABLE IF NOT EXISTS `tbl_email_queue` (
  `id` int(11) NOT NULL,
  `newsletter_id` int(11) NOT NULL,
  `contact_id` int(11) NOT NULL,
  `cron_id` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_file_attatch`
--

CREATE TABLE IF NOT EXISTS `tbl_file_attatch` (
  `id` int(11) NOT NULL,
  `file_name` varchar(255) NOT NULL,
  `newsletter_id` int(11) NOT NULL,
  `type` enum('newsletter','autoresponder') NOT NULL DEFAULT 'newsletter'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_form_template`
--

CREATE TABLE IF NOT EXISTS `tbl_form_template` (
  `id` int(11) NOT NULL,
  `template_name` varchar(255) DEFAULT NULL,
  `template_text` longtext,
  `member_id` int(11) DEFAULT NULL,
  `add_time` int(11) DEFAULT NULL,
  `ip` varchar(255) NOT NULL,
  `status` enum('active','inactive') NOT NULL DEFAULT 'active'
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_form_template`
--

INSERT INTO `tbl_form_template` (`id`, `template_name`, `template_text`, `member_id`, `add_time`, `ip`, `status`) VALUES
(1, 'Template 1', '<div class="form1-bg" id="demo-preview">\n<div class="row"> \n<div class="col-md-12 col-sm-12 col-xs-12">\n<div class="col-md-12 col-sm-12 col-xs-12 form_area1">\n<div class="mdem17 smem15 xsem12 blue text-center" contenteditable="true">TAKE THE BEST</div>\n<div class="mdem17 smem15 xsem12 blue text-center" contenteditable="true"><b>ONLINE COURSES</b> AVAILABLE</div>\n<div class="col-md-10 col-md-offset-1 col-sm-10 col-sm-offset-1 col-xs-10 col-xs-offset-1 mt7 xsmt5">\n<div class="namefield">\n<div class="mdem11 smem11 xsem10 blacktext w500" contenteditable="true">Name</div>\n<div class="mdem11 smem11 xsem10"><input type="text"  name="name" class="fieldinput" placeholder="Name"/></div>\n</div>\n<div class="copydiv">\n<div class="mdem11 smem11 xsem10 blacktext mt2 xsmt4 w500 fieldname" contenteditable="true">Email Address</div>\n<div class="mdem12 smem11 xsem10"><input type="text" name="email" class="fieldinput" placeholder="Email" /></div>\n</div>\n<div class="mdem12 smem12 xsem11 mt15 xsmt10 w400">\n<a href="#" class="form-btn1" contenteditable="true" id="rect">Sign Up Today</a></div>\n</div>\n</div>\n</div>				\n</div>\n</div>', 1, 1470394007, '', 'active'),
(2, 'Template 2', '<div class="form2-bg" id="demo-preview">\n<div class="row"> \n<div class="col-md-12 col-sm-12 col-xs-12">\n<div class="col-md-12 col-sm-12 col-xs-12 form_area2">\n<div class="mdem17 smem15 xsem13 heading w500 text-center robotoslab" contenteditable="true">EXPERIENCE ADVANTURE <br> OF A LIFE TIME</div>\n<div class="mdem13 smem12 xsem12 preheading text-center w300 mt1 xsmt1" contenteditable="true">For discount, fill in the form</div>\n<div class="col-md-10 col-md-offset-1 col-sm-10 col-sm-offset-1 col-xs-10 col-xs-offset-1 mt9 xsmt10">\n<div class="namefield">\n<div class="mdem10 smem10 xsem10 whitetext w300" contenteditable="true">Full Name</div>\n<div class="mdem10 smem10 xsem10"><input type="text" name="name" class="fieldinput" placeholder="Name"></div>\n</div>\n<div class="copydiv">\n<div class="mdem10 smem10 xsem10 whitetext mt2 xsmt4 w300 fieldname" contenteditable="true">Email Address</div>\n<div class="mdem10 smem10 xsem10"><input type="text"  name="email" class="fieldinput" placeholder="Email"></div>\n</div>\n<div class="mdem11 smem11 xsem11 mt10 xsmt10 w300"><a href="#" class="form-btn2" contenteditable="true"  id="rect">Send My Offer</a></div>\n</div>\n</div>\n</div>				\n</div>\n</div>', 1, 1470394007, '', 'active'),
(3, 'Template 3', '<div class="form3-bg" id="demo-preview">\n<div class="row"> \n<div class="form_area3">\n<div class="col-md-12 col-sm-12 col-xs-12 innerform">\n<div class="mdem17 smem15 xsem13 whitetext text-center w600" contenteditable="true">Want To Get Smart Tips <br> From Our Experts</div>\n<div class="mdem11 smem11 xsem11 whitetext text-center w300 mt2 xsmt2" contenteditable="true">For Details, Fill In The Form</div>\n<div class="col-md-10 col-md-offset-1 col-sm-10 col-sm-offset-1 col-xs-10 col-xs-offset-1 mt9 xsmt10">\n<div class="namefield">\n<div class="mdem10 smem10 xsem10 whitetext w300" contenteditable="true">Full Name</div>\n<div class="mdem10 smem10 xsem10"><input type="text"  name="name" class="fieldinput" placeholder="Name"></div>\n</div>\n<div class="copydiv">\n<div class="mdem10 smem10 xsem10 whitetext mt2 xsmt4 w300 fieldname" contenteditable="true">Email Address</div>\n<div class="mdem10 smem10 xsem10"><input type="text"  name="email" class="fieldinput" placeholder="Email"></div>\n</div>\n<div class="mdem11 smem10 xsem10 mt10 xsmt10 w300"><a href="#" class="form-btn3" contenteditable="true"  id="rect">Send Me Details</a></div>\n</div>\n</div>\n</div>				\n</div>\n</div>', 1, 1470394007, '', 'active'),
(4, 'Template 4', '<div class="form4-bg"  id="demo-preview">\n<div class="row"> \n<div class="col-md-12 col-sm-12 col-xs-12">\n<div class="col-md-12 col-sm-12 col-xs-12 form_area4">\n<div class="mdem18 smem15 xsem12 whitetext w700 text-center" contenteditable="true">CONCERTS</div>\n<div class="mdem10 smem12 xsem11 whitetext text-center w500 mt3 xsmt3" contenteditable="true">DON''T MISS ANY OF OUR AWESOME CONCERTS</div>\n<div class="mdem9 smem12 xsem11 whitetext text-center w300 mt1 xsmt1" contenteditable="true">Limited amount of passes, reserve your ticket today!</div>\n<div class="col-md-8 col-md-offset-2 col-sm-10 col-sm-offset-1 col-xs-10 col-xs-offset-1 mt7 xsmt10">\n<div class="namefield">\n<div class="mdem10 smem10 xsem10 whitetext w300" contenteditable="true">Full Name</div>\n<div class="mdem10 smem10 xsem10"><input type="text" name="name" class="fieldinput" placeholder="Name"></div>\n</div>\n<div class="copydiv">\n<div class="mdem10 smem10 xsem10 whitetext w300 fieldname" contenteditable="true">Email Address</div>\n<div class="mdem10 smem10 xsem10"><input type="text" name="email" class="fieldinput" placeholder="Email"></div>\n</div>\n<div class="mdem11 smem11 xsem11 mt5 xsmt5 w600"><a href="#" class="form-btn4" contenteditable="true"  id="rect">Reserve Today</a></div>\n</div>\n</div>\n</div>				\n</div>\n</div>', 1, 1470394007, '', 'active'),
(5, 'Template 5', '<div class="form5-bg" id="demo-preview">\n<div class="row"> \n<div class="col-md-12 col-sm-12 col-xs-12">\n<div class="col-md-12 col-sm-12 col-xs-12 form_area5">\n<div class="mdem32 smem30 xsem22 greytext text-center breeserif" contenteditable="true">CAR EXHIBITION</div>\n<div class="col-md-10 col-md-offset-1 col-sm-10 col-sm-offset-1 col-xs-10 col-xs-offset-1 mt25 xsmt25">\n<div class="namefield">\n<div class="mdem10 smem9 xsem9"><input type="text" placeholder="Full Name"  name="name" class="fieldinput"></div>\n</div>\n<div class="phonefield">\n<div class="mdem10 smem9 xsem9"><input type="text"  placeholder="Phone Number"  name="phone" class="fieldinput"></div>\n</div>\n<div class="copydiv">\n<div class="mdem10 smem9 xsem9"><input type="text" placeholder="Email Address"   name="email" class="fieldinput"></div>\n</div>\n<div class="mdem15 smem13 xsem13 mt10 xsmt10 w600"><a href="#" class="form-btn5" contenteditable="true"  id="rect">Join Now</a></div>\n</div>\n</div>	\n</div>			\n</div>\n</div>', 1, 1470394007, '', 'active'),
(6, 'Template 6', '<div class="form6-bg"  id="demo-preview">\n<div class="row"> \n<div class="col-md-12 col-sm-12 col-xs-12 form_area6">\n<div class="col-md-8 col-md-offset-2 col-sm-8 col-sm-offset-2 col-xs-10 col-xs-offset-1 arrow_box">\n<div class="mdem17 smem16 xsem15 whitetext w600 text-center letterspace mt13 xsmt5"  contenteditable="true">FREE COURSE</div>\n<div class="mdem10 smem10 xsem9 whitetext text-center w300 mt3 xsmt2"  contenteditable="true">Inroduction to computer science <br> For join TODAY, Fill in the form</div>\n</div>\n<div class="col-md-10 col-md-offset-1 col-sm-10 col-sm-offset-1 col-xs-10 col-xs-offset-1">\n<br><br>\n<div class="namefield">\n<div class="mdem10 smem10 xsem9 whitetext w300"  contenteditable="true">Full Name</div>\n<div class="mdem10 smem10 xsem9"><input type="text"  name="name" class="fieldinput" placeholder="Name"></div>\n</div>\n<div class="copydiv">\n<div class="mdem10 smem10 xsem9 whitetext mt2 xsmt0 w300 fieldname"  contenteditable="true">Email Address</div>\n<div class="mdem10 smem10 xsem9"><input type="text"  name="email" class="fieldinput" placeholder="Email"></div>\n</div>\n<div class="mdem13 smem12 xsem11 mt10 xsmt10 w500"><a href="#" class="form-btn6" contenteditable="true"  id="rect">Join Now</a></div>\n</div>\n</div>				\n</div>\n</div>', 1, 1470394007, '', 'active'),
(7, 'Template 7', '<div class="form7-bg"  id="demo-preview">\n<div class="row"> \n<div class="col-md-12 col-sm-12 col-xs-12">\n<div class="col-md-12 col-sm-12 col-xs-12 form_area7">\n<div class="mdem17 smem15 xsem15 redtext w700 text-center" contenteditable="true">LETS KEEP IN TOUCH !</div>\n<div class="redarrow_box"><div class="mdem12 smem12 xsem11 whitetext text-center w300" contenteditable="true">Sign up below to get <br> regular updates from our company</div></div>\n<div class="col-md-10 col-md-offset-1 col-sm-10 col-sm-offset-1 col-xs-10 col-xs-offset-1 mt5 xsmt5">\n<div class="namefield">\n<div class="mdem10 smem10 xsem10 black w500" contenteditable="true">Full Name</div>\n<div class="mdem10 smem10 xsem10"><input type="text" name="name" class="fieldinput" placeholder="Name"></div>\n</div>\n<div class="copydiv">\n<div class="mdem10 smem10 xsem10 black w500 fieldname" contenteditable="true">Email Address</div>\n<div class="mdem10 smem10 xsem10"><input type="text" name="email" class="fieldinput" placeholder="Email"></div>\n</div>\n<div class="mdem13 smem11 xsem11 mt5 xsmt5 w600"><a href="#" class="form-btn7" contenteditable="true"  id="rect">Join Now</a></div>\n</div>\n</div>\n</div>				\n</div>\n</div>', 1, 1470394007, '', 'active'),
(8, 'Template 8', '<div class="form8-bg" id="demo-preview">\n<div class="row"> \n<div class="col-md-12 col-sm-12 col-xs-12">\n<div class="col-md-12 col-sm-12 col-xs-12 form_area8 whitetext">\n<div class="mdem12 smem11 xsem10 text-center" contenteditable="true">Want to recieve the best offers of the weak ? </div>\n<div class="mdem22 smem15 xsem14 text-center w700 xsmb5" contenteditable="true">Join Today </div>\n<div class="col-md-10 col-md-offset-1 col-sm-10 col-sm-offset-1 col-xs-10 col-xs-offset-1 mt10 xsmt10">\n<div class="namefield">\n<div class="mdem10 smem9 xsem9"><input type="text" placeholder="Full Name"  name="name" class="fieldinput"></div>\n</div>\n<div class="phonefield">\n<div class="mdem10 smem9 xsem9"><input type="text"  placeholder="Phone Number"  name="phone" class="fieldinput"></div>\n</div>\n<div class="copydiv">\n<div class="mdem10 smem9 xsem9"><input type="text" placeholder="Email Address"  name="email" class="fieldinput"></div>\n</div>\n<div class="mdem14 smem13 xsem13 mt10 xsmt10 w500"><a href="#" class="form-btn8" contenteditable="true"  id="rect">Join Now</a></div>\n</div>\n</div>				\n</div>\n</div>\n</div>', 1, 1470394007, '', 'active'),
(9, 'Template 9', '<div class="form9-bg" id="demo-preview">\n<div class="row"> \n<div class="col-md-12 col-sm-12 col-xs-12">\n<div class="col-md-12 col-sm-12 col-xs-12 form_area9">\n<div class="ribbon"><span  contenteditable="true">&nbsp;Hurry UP!&nbsp;</span></div>\n<div class="mdem25 smem15 xsem12 text-center w600 mt16 xsmt15 whitetext" contenteditable="true">Start Your Fitness</div>\n<div class="col-md-10 col-md-offset-1 col-sm-10 col-sm-offset-1 col-xs-10 col-xs-offset-1 mt7 xsmt5">\n<div class="namefield">\n<div class="mdem11 smem11 xsem10 w300 whitetext" contenteditable="true">Full Name</div>\n<div class="mdem11 smem11 xsem10"><input type="text" name="name" class="fieldinput" placeholder="Name"></div>\n</div>\n<div class="copydiv">\n<div class="mdem11 smem11 xsem10 w300 whitetext fieldname" contenteditable="true">Email Address</div>\n<div class="mdem12 smem11 xsem10"><input type="text" name="email" class="fieldinput" placeholder="Email"></div>\n</div>\n<div class="mdem12 smem12 xsem11 mt5 xsmt5 w400"><a href="#" class="form-btn9" contenteditable="true"  id="rect">Sign Up</a></div>\n</div>\n</div>\n</div>				\n</div>\n</div>', 1, 1470394007, '', 'active'),
(10, 'Template 10', '<div class="form10-bg"  id="demo-preview">\n<div class="row"> \n<div class="col-md-12 col-sm-12 col-xs-12 segoe">\n<div class="col-md-12 col-sm-12 col-xs-12 form_area10 whitetext">\n<div class="mdem15 smem13 xsem12 w600 text-center" contenteditable="true">ONLY TODAY !</div>\n<div class="mdem42 smem35 xsem30 yellowtext text-center w700" contenteditable="true">50% OFF</div>\n<div class="mdem11 smem10 xsem9 text-center" contenteditable="true">Strength is the product of struggle, You <br> must do what others don''t to acheive <br> what others won''t</div>\n<div class="mdem10 smem9 xsem9 text-center mt5 xsmt8" contenteditable="true">If so, enter your detail below</div>\n<div class="col-md-10 col-md-offset-1 col-sm-10 col-sm-offset-1 col-xs-10 col-xs-offset-1 mt2 xsmt4">\n<div class="namefield">\n<div class="mdem9 smem9 xsem9"><input type="text" placeholder="Name" name="name" class="fieldinput"></div>\n</div>\n<div class="copydiv">\n<div class="mdem9 smem9 xsem9"><input type="text" placeholder="Email" name="email" class="fieldinput"></div>\n</div>\n<div class="mdem11 smem11 xsem11 w500"><a href="#" class="form-btn10" contenteditable="true"  id="rect">Hurry! Register Today</a></div>\n<div class="mdem8 smem8 xsem8 text-center mt4 xsmt5" contenteditable="true"> <img src="./assets/default/images/form-images/icon.png">&nbsp;&nbsp;Your information will never be shared.</div>\n\n</div>\n</div>\n</div>				\n</div>\n</div>', 1, 1470394007, '', 'active');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_from_email`
--

CREATE TABLE IF NOT EXISTS `tbl_from_email` (
  `id` int(11) NOT NULL,
  `member_id` int(11) NOT NULL,
  `email` varchar(255) NOT NULL,
  `status` enum('varify','unvarify') NOT NULL DEFAULT 'unvarify',
  `verify_code` varchar(255) DEFAULT NULL,
  `add_time` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_from_email`
--

INSERT INTO `tbl_from_email` (`id`, `member_id`, `email`, `status`, `verify_code`, `add_time`) VALUES
(1, 1, 'coderjack9@gmail.com', 'varify', NULL, 1470394007);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_member`
--

CREATE TABLE IF NOT EXISTS `tbl_member` (
  `mem_id` bigint(20) NOT NULL,
  `email` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `city` text,
  `state` varchar(250) DEFAULT NULL,
  `country` varchar(250) DEFAULT NULL,
  `address` text,
  `pin_code` varchar(250) DEFAULT NULL,
  `business_name` varchar(255) DEFAULT NULL,
  `profile_pic` varchar(200) DEFAULT NULL,
  `forget_pass_code` varchar(255) DEFAULT NULL,
  `forget_pass_exptime` int(11) DEFAULT NULL,
  `status` enum('active','inactive') NOT NULL DEFAULT 'active',
  `add_time` int(11) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_member`
--

INSERT INTO `tbl_member` (`mem_id`, `email`, `password`, `name`, `city`, `state`, `country`, `address`, `pin_code`, `business_name`, `profile_pic`, `forget_pass_code`, `forget_pass_exptime`, `status`, `add_time`) VALUES
(1, 'coderjack9@gmail.com', 'e10adc3949ba59abbe56e057f20f883e', 'coderjakc', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'active', 1470394007);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_newsletter`
--

CREATE TABLE IF NOT EXISTS `tbl_newsletter` (
  `id` int(11) NOT NULL,
  `member_id` int(11) NOT NULL,
  `message_type` enum('schedule','instant') DEFAULT NULL,
  `newsletter_type` enum('newsletter','autoresponder') NOT NULL DEFAULT 'newsletter',
  `from_name` varchar(255) NOT NULL,
  `from_email` varchar(255) NOT NULL,
  `reply_to` varchar(255) NOT NULL,
  `message_name` varchar(255) NOT NULL,
  `subject` varchar(255) NOT NULL,
  `template_id` int(11) DEFAULT NULL,
  `template_text` longtext NOT NULL,
  `in_campaign_id` text NOT NULL,
  `ex_campaign_id` text,
  `sep_list_id` text,
  `contact_id` longtext,
  `image` varchar(255) DEFAULT NULL,
  `schedule_date` datetime DEFAULT NULL,
  `schedule_timestamp` int(11) DEFAULT NULL,
  `status` enum('sent','scheduled','process','draft') DEFAULT NULL,
  `process_stage` enum('complete','inprocess') NOT NULL DEFAULT 'inprocess',
  `last_procees_id` int(11) DEFAULT NULL,
  `add_time` int(11) NOT NULL,
  `sent_time` int(11) DEFAULT NULL,
  `ip` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_statistics`
--

CREATE TABLE IF NOT EXISTS `tbl_statistics` (
  `id` int(11) NOT NULL,
  `type` enum('newsletter','autoresponder') NOT NULL DEFAULT 'newsletter',
  `newsletter_id` int(11) NOT NULL,
  `contact_id` int(11) NOT NULL,
  `email` varchar(255) NOT NULL,
  `open` enum('yes','no') NOT NULL DEFAULT 'no',
  `click` enum('yes','no') NOT NULL DEFAULT 'no',
  `unsubscribed` enum('yes','no') NOT NULL DEFAULT 'no',
  `bounced` enum('yes','no') NOT NULL DEFAULT 'no',
  `complaints` enum('yes','no') NOT NULL DEFAULT 'no',
  `add_time` int(11) NOT NULL,
  `ip` varchar(255) DEFAULT NULL,
  `browser` varchar(255) DEFAULT NULL,
  `device` enum('mobile','desktop') DEFAULT NULL,
  `country_code` varchar(255) DEFAULT NULL,
  `open_time` int(11) DEFAULT NULL,
  `click_time` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_suppression`
--

CREATE TABLE IF NOT EXISTS `tbl_suppression` (
  `id` int(11) NOT NULL,
  `member_id` int(11) NOT NULL,
  `email` varchar(255) NOT NULL,
  `list_id` int(11) NOT NULL,
  `status` enum('active','inactive') NOT NULL DEFAULT 'active',
  `ip` varchar(255) NOT NULL,
  `add_time` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_suppression_list`
--

CREATE TABLE IF NOT EXISTS `tbl_suppression_list` (
  `id` bigint(20) NOT NULL,
  `list_name` varchar(255) NOT NULL,
  `member_id` bigint(20) NOT NULL,
  `add_time` int(11) NOT NULL,
  `ip` varchar(255) NOT NULL,
  `status` enum('active','inactive') NOT NULL DEFAULT 'active'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_template`
--

CREATE TABLE IF NOT EXISTS `tbl_template` (
  `id` bigint(20) NOT NULL,
  `template_type` enum('default','draft') NOT NULL,
  `editor_type` enum('inline','plain') NOT NULL DEFAULT 'inline',
  `template_name` varchar(255) NOT NULL,
  `template_text` text NOT NULL,
  `template_screenshot` varchar(255) DEFAULT NULL,
  `member_id` bigint(20) NOT NULL,
  `add_time` int(11) NOT NULL,
  `ip` varchar(255) NOT NULL,
  `status` enum('active','inactive') NOT NULL DEFAULT 'active'
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_template`
--

INSERT INTO `tbl_template` (`id`, `template_type`, `editor_type`, `template_name`, `template_text`, `template_screenshot`, `member_id`, `add_time`, `ip`, `status`) VALUES
(1, 'default', 'inline', 'Template 1', '<div style="width:100%; max-width:600px; margin: auto;"><table style="border-collapse:collapse;height:100%;margin:0;padding:0;background-color:#e9e9e9; width:100%;max-width:600px; margin:0 auto;" width="100%" cellspacing="0" cellpadding="0" border="0" align="center" height="100%">\n<tbody>\n<tr>\n<td style="height:100%;margin:0;padding:0;width:100%;border-top:0" valign="top" align="center">\n\n<table style="border-collapse:collapse;width:100%;max-width:600px;" width="100%" cellspacing="0" cellpadding="0" border="0">\n<tbody>\n<tr><td valign="top" align="center">\n\n<table style="border-collapse:collapse; background:#3a617e;width:100%;max-width:600px;" width="600" cellspacing="0" cellpadding="0" border="0">\n<tbody>\n<tr>\n<td style="padding:9px;text-align:center" valign="top">\n<img alt="" src="{image_path}images/logo.png" style="box-sizing: border-box;max-width:163px;padding-bottom:0;display:inline!important;vertical-align:bottom;border:0;height:auto;outline:none;text-decoration:none" width="163" align="middle"></td></tr></tbody></table>\n                                    \n                                </td>\n                            </tr>\n                            <tr>\n                                <td valign="top" align="center">\n                                    \n<table style="border-collapse:collapse;border-top:0;border-bottom:0;width:100%;max-width:600px;" width="100%" cellspacing="0" cellpadding="0" border="0">\n<tbody><tr><td valign="top" align="center">\n                                                \n<table style="border-collapse:collapse;width:100%;max-width:600px;" width="600" cellspacing="0" cellpadding="0" border="0">\n<tbody><tr><td valign="top" align="center">\n\n<table style="border-collapse:collapse;background-color:#ffffff;border:0px solid #d5d5d5;width:100%;max-width:600px;" width="100%" cellspacing="0" cellpadding="0" border="0">\n<tbody><tr><td valign="top">\n\n\n<table style="min-width:100%;border-collapse:collapse;width:100%;max-width:600px;" width="100%" cellspacing="0" cellpadding="0" border="0" align="left">\n<tbody><tr><td valign="top">\n<img alt="" src="{image_path}images/banner.png" style="box-sizing: border-box;max-width:100%;padding-bottom:0;display:inline!important;vertical-align:bottom;border:0;height:auto;outline:none;text-decoration:none" tabindex="0" width="600" align="middle"></td></tr>\n</tbody></table>\n\n\n<table style="max-width:100%;min-width:100%;border-collapse:collapse;  clear:both;width:100%;max-width:600px;" width="100%" cellspacing="0" cellpadding="0" border="0" align="left"><tbody><tr><td style="padding-top:0; padding-right:18px; padding-bottom:9px; padding-left:18px; word-break:break-word; color:#606060; font-size:15px; text-align:left;" valign="top">\n<div class="editblock" contenteditable="true">\n<div style="display:block; margin:15px 0 0 0; padding:0; font-size:40px; font-style:normal; line-height:100%; color:#d95433;" align="center"><b>Apply Online</b></div>\n</div>\n<div class="editblock" contenteditable="true">\n<p style="display:block; margin:15px 0 0 0; padding:0; font-size:20px; font-style:normal; line-height:100%; text-align:center; color:#d95433;">for your Loan and get faster approval!</p>\n</div>\n<div class="editblock" contenteditable="true">\n<p style="display:block; margin:5px 0 0 0; font-size:15px; text-align:center; color:#353535!important;">Explore Loan offers from leading Banks now. Rates from 11.99% available.</p>\n</div>\n<hr style="background:#d6d6d6; height:1px; border:0; margin:15px 0 5px 0;"></td></tr></tbody>\n</table>\n\n\n\n	<table style="width:100%;max-width:600px;min-width:100%;border-collapse:collapse; clear:both;" width="100%" cellspacing="0" cellpadding="15" border="0" align="left">\n    <tbody>\n	\n	<tr>\n	<td width="40%"><img alt="" src="{image_path}images/education-loan.png" style="box-sizing: border-box;max-width:100%;padding-bottom:0;display:inline!important;vertical-align:bottom;border:1px solid #d6d6d6;height:auto;outline:none;text-decoration:none" tabindex="0" width="200" align="middle"></td>\n    <td style="word-break:break-word;color:#4a4949;text-align:left" valign="top">\n	<div class="editblock" contenteditable="true">\n	<p style="display:block; margin:0; padding:0; line-height:100%; font-family:Helvetica; font-size:20px; font-style:normal; line-height:100%;">\n	<b>Education Loan</b>\n	</p>\n	</div>\n	<div class="editblock" contenteditable="true">\n	<p style="display:block; margin-top:15px; margin-right:0px; margin-bottom:0px; margin-left:0px; padding:0; font-family:Helvetica; font-size:14px; font-style:normal; line-height:150%;">\nLorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.\n	</p>\n	</div>\n	\n	<div class="editblock" contenteditable="true">\n	<a href="#" style="font-family:Helvetica; border-radius:03px; background-color:#d95433; padding:9px 18px; text-align:center; text-decoration:none; color:#ffffff; display:inline-block; margin-top:6px;" class="sm-button">Apply Now</a>\n	</div>\n	\n	<!--<a style="font-weight:bold; font-family:Helvetica; letter-spacing:normal; border-radius:3px; background-color:#d95433; padding:12px; line-height:100%; text-align:center; text-decoration:none; color:#ffffff; display:inline-block; margin-top:6px;"><div contenteditable="true">Apply Now</div></a>-->\n    </td>\n    </tr>\n	\n	\n	<tr>\n	<td><img alt="" src="{image_path}images/home-loan.png" style="box-sizing: border-box;max-width:100%;padding-bottom:0;display:inline!important;vertical-align:bottom;border:1px solid #d6d6d6;height:auto;outline:none;text-decoration:none" tabindex="0" width="200" align="middle"></td>\n    <td style="word-break:break-word;color:#4a4949;text-align:left" valign="top">\n	<div class="editblock" contenteditable="true"><p style="display:block; margin:0; padding:0; line-height:100%; font-family:Helvetica; font-size:20px; font-style:normal; line-height:100%;"><b>Home Loan</b></p></div>\n	<div class="editblock" contenteditable="true">\n	<p style="display:block; margin:15px 0 0 0; padding:0; font-family:Helvetica; font-size:14px; font-style:normal; line-height:150%;">Lorem ipsum <span>dolor</span> sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.\n	</p>\n	\n	</div>\n	\n	<div class="editblock" contenteditable="true">\n	<a href="#" style="font-family:Helvetica; border-radius:03px; background-color:#d95433; padding:9px 18px; text-align:center; text-decoration:none; color:#ffffff; display:inline-block; margin-top:6px;" class="sm-button">Apply Now</a></div>\n	\n    </td>\n    </tr>\n	\n	\n	<tr style="border-bottom:15px solid #3a617e;">\n	<td><img alt="" src="{image_path}images/car-loan.png" style="box-sizing: border-box;max-width:100%;padding-bottom:0;display:inline!important;vertical-align:bottom;border:1px solid #d6d6d6;height:auto;outline:none;text-decoration:none" tabindex="0" width="200" align="middle"></td>\n    <td style="word-break:break-word;color:#4a4949;text-align:left" valign="top">\n	<div class="editblock" contenteditable="true"><p style="display:block; margin:0; padding:0; line-height:100%; font-family:Helvetica; font-size:20px; font-style:normal; line-height:100%;"><b>Loan Loan</b></p></div>\n	<div class="editblock" contenteditable="true"><p style="display:block; margin:15px 0 0 0; padding:0; font-family:Helvetica; font-size:14px; font-style:normal; line-height:150%;">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p></div>\n	\n	<div class="editblock" contenteditable="true">\n	<a href="#" style="font-family:Helvetica; border-radius:03px; background-color:#d95433; padding:9px 18px; text-align:center; text-decoration:none; color:#ffffff; display:inline-block; margin-top:6px;" class="sm-button">Apply Now</a></div>\n    \n    \n    \n    </td>\n    </tr>\n	\n	\n	\n	\n	\n	\n</tbody>\n</table>				\n\n				\n\n\n</td>\n                                                    </tr>\n\n                                                </tbody></table>\n                                            </td>\n                                        </tr>\n                                    </tbody></table>\n                                    \n                                </td>\n                            </tr>\n                           \n                        </tbody></table>\n                        \n                    </td>\n</tr>\n\n</tbody>\n</table>\n			\n	</td>\n    </tr>\n    </tbody>\n    </table>	\n    </div>', 'template1.jpg', 1, 1470394007, '', 'active'),
(2, 'default', 'inline', 'Template 2', '<div style="width:100%; max-width:600px; margin: auto;"><table style="border-collapse:collapse;height:100%;margin:0 auto;padding:0;width:100%;background-color:#e9e9e9;max-width:600px;" width="100%" cellspacing="0" cellpadding="0" border="0" align="center" height="100%">\n<tbody>\n<tr>\n<td style="height:100%;margin:0;padding:0;width:100%;border-top:0" valign="top" align="center">\n\n<table style="border-collapse:collapse;max-width:600px;width:100%;" width="100%" cellspacing="0" cellpadding="0" border="0">\n<tbody>\n<tr><td valign="top" align="center">\n\n<table style="border-collapse:collapse; background:#41aa9a;max-width:600px;width:100%;" width="100%" cellspacing="0" cellpadding="0" border="0">\n<tbody>\n<tr>\n<td style="padding:18px;text-align:center" valign="top">\n<img alt="" src="{image_path}images/logo2.png" style="box-sizing: border-box;height:70px;max-height:70px;padding-bottom:0;display:inline!important;vertical-align:bottom;border:0;width:auto;outline:none;text-decoration:none" align="middle"></td></tr></tbody></table>\n                                    \n                                </td>\n                            </tr>\n                            <tr>\n                                <td valign="top" align="center">\n                                    \n<table style="border-collapse:collapse;border-top:0;border-bottom:0;max-width:600px;width:100%;" width="100%" cellspacing="0" cellpadding="0" border="0">\n<tbody><tr><td valign="top" align="center">\n                                                \n<table style="border-collapse:collapse;max-width:600px;width:100%;" width="100%" cellspacing="0" cellpadding="0" border="0">\n<tbody><tr><td valign="top" align="center">\n\n<table style="border-collapse:collapse;background-color:#41aa9a;border:1px solid #41aa9a;max-width:600px;width:100%;" width="100%" cellspacing="0" cellpadding="0" border="0">\n<tbody><tr><td valign="top">\n<table style="border-collapse:collapse;background-color:#fff;border:1px solid #fff;margin:0 auto 25px auto;max-width:550px;width:100%;" width="92%" cellspacing="0" cellpadding="0" border="0">\n<tbody><tr><td valign="top">\n\n\n<table style="max-width:600px;width:100%;min-width:100%;border-collapse:collapse;  clear:both;" width="100%;" cellspacing="0" cellpadding="0" border="0" align="left"><tbody><tr><td style="color:#4a4949; font-size:15px; text-align:left;" valign="top">\n<div  contenteditable="true">\n<div style="display:block; margin:5px 15px 0 0; padding:0; font-size:13px; text-align:right;line-height:100%; color:#dadada;" align="center;"><b>*T &amp; C Apply</b></div></div>\n<div contenteditable="true">\n<div style="display:block; margin:15px 0 0 0; padding:0; font-size:100px; text-align:center;line-height:100%; color:#4a4949;font-weight:100;" align="center;">Summer</div>\n</div>\n\n<div contenteditable="true">\n<div style="display:block; margin:15px 0 0 0; padding:0; font-size:160px; font-style:normal; line-height:100%; color:#4a4949;" align="center"><b>SALE</b></div>\n</div>\n\n<div contenteditable="true">\n<div style="display:block; margin:15px 0 15px 0;font-size:45px; padding: 0% 15%; font-style:normal; line-height:100%; color:#fff;" align="center"><div style="background-color:#41aa9a;padding: 2.5% 0%;">40% OFF</div></div>\n</div>\n\n<div contenteditable="true">\n<div style="display:block; margin:10px 0 10px 0;font-size:23px; padding: 0; font-style:normal; line-height:100%; color:#4a4949;font-weight:400;" align="center">ALL PURCHASES OVER $ 100</div>\n</div>\n\n<div contenteditable="true">\n<div style="display:block; margin:20px 0 20px 0;font-size:50px; padding: 0; font-style:normal; line-height:100%; color:#41aa9a;" align="center">LAST WEEK!</div>\n</div>\n\n<div contenteditable="true">\n<div style="display:block; margin:20px 0 20px 0;font-size:15px; padding: 0; font-style:normal; line-height:150%; color:#4a4949;padding:0 5%;" align="center">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</div>\n</div>\n\n<div style="text-align:center;" contenteditable="true">\n<a href="#" style="border-radius:03px; background-color:#3d455c; padding: 6px 30px; text-align:center; text-decoration:none; color:#ffffff; display:inline-block;font-size:30px;margin:15px 0 15px 0;" class="sm-button">Shop Now</a></div>\n\n</td>\n</tr>\n</tbody>\n</table>\n</td>\n</tr>\n</tbody></table>\n</td>\n</tr>\n</tbody></table>\n\n\n\n </td>\n</tr>\n</tbody></table>\n</td>\n</tr>\n\n\n</tbody></table>\n</td>\n</tr>\n</tbody>\n</table>\n</td></tr>\n</tbody>\n</table>\n</div>', 'template3.jpg', 1, 1470394007, '', 'active'),
(3, 'default', 'inline', 'Template 3', '<div style="width:100%; max-width:600px; margin: auto;"><table style="border-collapse:collapse;height:100%;margin:0;padding:0;width:100%;background-color:#e9e9e9; max-width:600px; margin:0 auto;" width="100%" cellspacing="0" cellpadding="0" border="0" align="center" height="100%">\n<tbody>\n<tr>\n<td style="height:100%;margin:0;padding:0;width:100%;border-top:0" valign="top" align="center">\n\n<table style="border-collapse:collapse;width:100%;max-width:600px;" width="100%" cellspacing="0" cellpadding="0" border="0">\n<tbody>\n<tr>\n<td valign="top" align="center">\n\n<table style="border-collapse:collapse; background:#405292;width:100%;max-width:600px;" width="600" cellspacing="0" cellpadding="0" border="0">\n<tbody>\n<tr>\n<td style="padding:9px;text-align:center;word-break:break-word; color:#606060; font-size:15px;" valign="top">\n\n<div contenteditable="true">\n<div style="display:block; margin:15px 0; padding:0; font-size:28px; font-style:normal; line-height:100%; color:#fff;" align="center"><b>Providing Total Health Care Solution</b></div>\n</div>\n\n</td>\n</tr>\n</tbody>\n</table>\n                                    \n</td>\n</tr>\n\n<tr>\n<td valign="top" align="center">\n                                    \n<table style="border-collapse:collapse;border-top:0;border-bottom:0;background-color:#ffffff;width:100%;max-width:600px;" width="100%" cellspacing="0" cellpadding="0" border="0">\n<tbody><tr><td valign="top" align="center">\n                                                \n\n\n\n<table style="min-width:100%;border-collapse:collapse;width:100%;max-width:600px;" width="100%" cellspacing="0" cellpadding="0" border="0" align="left">\n<tbody><tr><td valign="top">\n<img alt="" src="{image_path}images/temp3.png" style="box-sizing: border-box;max-height:310px;max-width:100%;padding-bottom:0;display:inline!important;vertical-align:bottom;border:0;height:auto;outline:none;text-decoration:none" tabindex="0" width="600" align="middle"></td></tr>\n</tbody></table>\n\n\n<table style="max-width:100%;min-width:100%;border-collapse:collapse;  clear:both;width:100%;max-width:600px;" width="100%;" cellspacing="0" cellpadding="0" border="0" align="left"><tbody><tr><td style="padding-top:0; padding-right:18px; padding-bottom:9px; padding-left:18px; word-break:break-word; color:#606060; font-size:15px; text-align:left;" valign="top">\n\n<div contenteditable="true">\n<p style="display:block; margin:20px 0 10px 0; padding:0; font-size:25px; font-weight:500;  text-align:center; color:#4a4949; line-height:30px;">Lorem ipsum dolor sit amet, no quodsi definitiones vis. Verterem postulant intellegat </p>\n</div>\n\n<hr style="background:#d6d6d6; height:1px; border:0; margin:15px 0 15px 0;">\n\n\n<div contenteditable="true">\n<p style="display:block; margin:15px 0 10px 0; padding:0; font-size:25px; font-weight:400; text-align:center; color:#000000; line-height:30px;">Description</p>\n</div>\n\n\n<div contenteditable="true">\n<p style="display:block; margin:2px 0 10px 0; padding:10px 25px ; font-size:15px; font-weight:400; text-align:center; color:#818080; line-height:25px;">Lorem ipsum dolor sit amet, no quodsi definitiones vis. Verterem postulant intellegat est et, an partiendo argumentum elaboraret qui. Diam salutatus cu est, choro sententiae nam id. An sint meliore conceptam qui, eum dicit corpora consequuntur no. Aperiam repudiandae pri an. Vidisse torquatos mea ne, debet ludus duo ne, in quodsi fuisset theophrastus qui.\n</p>\n</div>\n\n<div style="text-align:center;" contenteditable="true">\n<a href="#" style="font-family:Roboto; border-radius:03px; background-color:#ff4747; padding:2% 20%; text-align:center; text-decoration:none; color:#ffffff; display:inline-block; margin:10px 0 15px 0;font-size:20px;" class="sm-button">Get Appointment</a>\n</div>\n\n\n</td>\n</tr>\n</tbody>\n</table>\n\n\n\n\n</td>\n</tr>\n</tbody></table>\n</td>\n</tr>\n</tbody>\n</table>\n</td>\n</tr>\n</tbody>\n</table>\n</div>', 'template9.jpg', 1, 1470394007, '', 'active'),
(4, 'default', 'inline', 'Template 4', '<div style="width:100%; max-width:600px; margin: auto;"><table style="border-collapse:collapse;height:100%;margin:0;padding:0;width:100%;background-color:#e9e9e9; max-width:600px; margin:0 auto;" width="100%" cellspacing="0" cellpadding="0" border="0" align="center" height="100%">\n<tbody>\n<tr>\n<td style="height:100%;margin:0;padding:0;width:100%;border-top:0" valign="top" align="center">\n\n<table style="border-collapse:collapse;width:100%;max-width:600px;" width="100%" cellspacing="0" cellpadding="0" border="0">\n<tbody>\n<tr>\n<td valign="top" align="center">\n<table style="border-collapse:collapse;background:#fff;max-width:600px;width:100%;" width="600" cellspacing="0" cellpadding="0" border="0">\n<tbody>\n<tr>\n<td style="text-align:center;word-break:break-word;padding:0 9px;" valign="top">\n\n<div contenteditable="true">\n<div style="display:block; margin:30px 0 10px 0; padding:0; font-size:26px; line-height:40px; color:#65a3ca;" align="center">This Area Is Home Sweet Home For A Sweet,Catchy Headline</div>\n</div>\n\n<div contenteditable="true">\n<div style="display:block; margin:8px 0 35px 0; padding:0; font-size:16px; font-style:normal; line-height:40px; color:#4a4949;" align="center">Maybe A Little Description Section Too, In Case There’s More To Say!</div>\n</div>\n\n</td>\n</tr>\n</tbody>\n</table>                                  \n</td>\n</tr>\n\n<tr>\n<td valign="top" align="center">\n                                    \n<table style="border-collapse:collapse;border-top:0;border-bottom:0;background-color:#ffffff;width:100%;max-width:600px;" width="100%" cellspacing="0" cellpadding="0" border="0">\n<tbody><tr><td valign="top" align="center">\n                                                \n\n\n\n<table style="min-width:100%;border-collapse:collapse;max-width:600px;width:100%;" width="100%" cellspacing="0" cellpadding="0" border="0" align="left">\n<tbody><tr><td valign="top">\n<img alt="" src="{image_path}images/temp4.png" style="box-sizing: border-box;max-height:390px;max-width:100%;padding-bottom:0;display:inline!important;vertical-align:bottom;border:0;height:auto;outline:none;text-decoration:none" tabindex="0" width="600" align="middle"></td></tr>\n</tbody></table>\n\n\n<table style="min-width:100%;border-collapse:collapse;  clear:both;max-width:600px;width:100%;" width="100%;" cellspacing="0" cellpadding="0" border="0" align="left"><tbody><tr><td style="padding-top:0; padding-right:18px; padding-bottom:9px; padding-left:18px; word-break:break-word; color:#606060; font-size:15px; text-align:left;" valign="top">\n\n<div style="text-align:center;margin:15px 0 15px 0;padding:2% 0;" contenteditable="true">\n<a href="#" style="font-family:Roboto; border-radius:03px; background-color:#65a3ca; padding:2% 13%; text-align:center; text-decoration:none; color:#ffffff; display:inline-block;font-size:22px;" class="sm-button">Call To Action</a>\n</div>\n\n<div contenteditable="true">\n<p style="display:block; margin:15px 0 10px 0; padding:0; font-size:25px; font-weight:400; text-align:center; color:#606060; line-height:30px;">Company Name</p>\n</div>\n\n<div contenteditable="true">\n<p style="display:block; margin:2px 0 10px 0; padding:10px 30px ; font-size:15px; font-weight:400; text-align:center; color:#818080; line-height:25px;">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.\n</p>\n</div>\n\n</td>\n</tr>\n</tbody>\n</table>\n\n</td>\n</tr>\n</tbody></table>\n</td>\n</tr>\n</tbody>\n</table>\n</td>\n</tr>\n</tbody>\n</table>\n</div>', 'template7.jpg', 1, 1470394007, '', 'active'),
(5, 'default', 'inline', 'Template 5', '<div style="width:100%; max-width:600px; margin: auto;"><table style="border-collapse:collapse;height:100%;margin:0;padding:0;width:100%;background-color:#fff; max-width:600px; margin:0 auto;border-left:20px solid #75caed;" width="100%" cellspacing="0" cellpadding="0" border="0" align="center" height="100%">\n<tbody>\n<tr>\n<td style="height:100%;margin:0;padding:0;width:100%;border-top:0" valign="top" align="center">\n\n\n                                                \n\n\n\n\n<table style="max-width:100%;min-width:100%;border-collapse:collapse;  clear:both;width:100%;max-width:600px;" width="100%;" cellspacing="0" cellpadding="0" border="0" align="left"><tbody><tr><td style="padding:20px 30px; word-break:break-word; color:#606060; font-size:15px; text-align:left;" valign="top">\n\n\n\n\n<div contenteditable="true">\n<div style="display:block; margin:15px 0; padding:0; font-size:28px; font-style:normal; line-height:100%; color:#75caed;" align="left"><b>Converting Your Dreams Into Reality Is Our Passion</b></div>\n</div>\n\n\n\n<hr style="background:#75caed; height:1px; border:0; margin:15px 0 15px 0;">\n\n\n<div contenteditable="true">\n<p style="display:block; margin:10px 0 10px 0; padding:0; font-size:18px; font-weight:500;  text-align:left; color:#4a4949; line-height:30px;">Dear Customers,</p>\n</div>\n\n\n\n\n<div contenteditable="true">\n<p style="display:block; margin:2px 0 5px 0; padding:10px 0px ; font-size:14px; font-weight:400; text-align:left; color:#000; line-height:25px;">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Verum tamen cum de rebus grandioribus dicas, ipsae res verba rapiunt; Luxuriam non reprehendit, modo sit vacua infinita cupiditate et timore. Apud imperitos tum illa dicta sunt, aliquid etiam coronae datum; Quod non faceret, si in voluptate summum bonum poneret. Quid ei reliquisti, nisi te, quoquo modo loqueretur, intellegere, quid diceret? Ego vero volo in virtute vim esse quam maximam\n</p>\n</div>\n\n<div contenteditable="true">\n<p style="display:block; margin:2px 0 5px 0; padding:10px 0px ; font-size:14px; font-weight:400; text-align:left; color:#000; line-height:25px;">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Verum tamen cum de rebus grandioribus dicas, ipsae res verba rapiunt; Luxuriam non reprehendit, modo sit vacua infinita cupiditate et timore. Apud imperitos tum illa dicta sunt, aliquid etiam coronae datum; Quod non faceret, si in voluptate summum bonum poneret. Quid ei reliquisti, nisi te, quoquo modo loqueretur, intellegere, quid diceret? Ego vero volo in virtute vim esse quam maximam\n</p>\n</div>\n\n<div contenteditable="true">\n<p style="display:block; margin:2px 0 5px 0; padding:10px 0px ; font-size:14px; font-weight:400; text-align:left; color:#000; line-height:25px;">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Verum tamen cum de rebus grandioribus dicas, ipsae res verba rapiunt; Luxuriam non reprehendit, modo sit vacua infinita cupiditate et timore. Apud imperitos tum illa dicta sunt, aliquid etiam coronae datum; Quod non faceret, si in voluptate summum bonum poneret. Quid ei reliquisti, nisi te, quoquo modo loqueretur, intellegere, quid diceret? Ego vero volo in virtute vim esse quam maximam\n</p>\n</div>\n\n\n<div contenteditable="true">\n<p style="display:block; margin:2px 0 5px 0; padding:10px 0px ; font-size:14px; font-weight:400; text-align:left; color:#000; line-height:25px;">Thanks for a great year.\n</p>\n</div>\n\n<div contenteditable="true">\n<p style="display:block; margin:2px 0 5px 0; padding:10px 0px ; font-size:14px; font-weight:400; text-align:left; color:#000; line-height:18px;">\nSincerely<br>\nJay Prakash Nagar\n</p>\n</div>\n\n<div style="text-align:center;" contenteditable="true">\n<a href="#" style="font-family:Roboto; border-radius:03px; background-color:#656e72; padding:2% 20%; text-align:center; text-decoration:none; color:#ffffff; display:inline-block; margin:20px 0 15px 0;font-size:20px;" class="sm-button">Contact Our Business Experts Now</a>\n</div>\n\n\n</td>\n</tr>\n</tbody>\n</table>\n\n\n                                       \n          \n</td>\n</tr>\n\n</tbody>\n</table>\n</div>', 'template15.jpg', 1, 1470394007, '', 'active'),
(6, 'default', 'inline', 'Template 6', '<div style="width:100%; max-width:600px; margin: auto;"><table style="border-collapse:collapse;height:100%;margin:0;padding:0;width:100%;background-color:#fff; max-width:600px; margin:0 auto;border:20px solid #d6d6d6;" width="100%" cellspacing="0" cellpadding="0" border="0" align="center" height="100%">\n<tbody>\n<tr>\n<td style="height:100%;margin:0;padding:0;width:100%;border-top:0" valign="top" align="center">\n\n\n\n<table style="max-width:100%;min-width:100%;border-collapse:collapse;  clear:both;width:100%;max-width:600px;" width="100%;" cellspacing="0" cellpadding="0" border="0"><tbody><tr><td style="padding:20px 30px; word-break:break-word; color:#606060; font-size:15px;" valign="top">\n\n\n<div contenteditable="true">\n<div style="display:block; margin:15px 0; padding:0; font-size:40px; font-style:normal; line-height:100%; color:#fb6735;" align="center"><b>Explore Our Music App!</b></div>\n</div>\n\n<table style="min-width:100%;border-collapse:collapse;max-width:600px;width:100%;margin:10% 0; " width="100%" cellspacing="0" cellpadding="0" border="0" align="center">\n<tbody><tr><td valign="top" align="center">\n<img alt="" src="{image_path}images/temp6.png" style="box-sizing: border-box;padding-bottom:0;display:inline!important;vertical-align:bottom;border:0;height:auto;outline:none;text-decoration:none" tabindex="0" align="middle"></td></tr>\n</tbody></table>\n\n\n<div contenteditable="true">\n<div style="display:block; margin:9% 0 5% 0; padding:0; font-size:25px; font-style:normal; line-height:100%; color:#fb6735;" align="center">Our Product Will Amaze You</div>\n</div>\n\n<div contenteditable="true">\n<p style="display:block; margin:2px 0 10px 0; padding:10px 30px ; font-size:15px; font-weight:400; text-align:center; color:#818080; line-height:25px;">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.\n</p>\n</div>\n\n<div style="text-align:center;margin:15px 0 15px 0;padding:2% 0;" contenteditable="true">\n<a href="#" style="font-family:Roboto; border-radius:03px; background-color:#232c3b; padding:2% 13%; text-align:center; text-decoration:none; color:#ffffff; display:inline-block;font-size:22px;" class="sm-button">Explore Now</a>\n</div>\n\n\n</td>\n</tr>\n</tbody>\n</table>\n                        \n          \n</td>\n</tr>\n\n</tbody>\n</table>\n\n</div>', 'template11.jpg', 1, 1470394007, '', 'active'),
(7, 'default', 'inline', 'Template 7', '<div style="width:100%; max-width:600px; margin: auto;">\n\n<table align="center" border="0" cellpadding="0" cellspacing="0" height="100%" width="100%" style="border-collapse:collapse;height:100%;margin:0;padding:0;width:100%;background-color:#e9e9e9; max-width:600px; margin:0 auto;">\n<tbody>\n<tr>\n<td align="center" valign="top" style="height:100%;margin:0;padding:0;width:100%;border-top:0">\n\n<table border="0" cellpadding="0" cellspacing="0" width="100%" style="border-collapse:collapse;width:100%;max-width:600px;">\n<tbody>\n<tr>\n<td align="center" valign="top">\n<table border="0" cellpadding="0" cellspacing="0" width="600" style="border-collapse:collapse;background:#ff854f;max-width:600px;width:100%;">\n<tbody>\n<tr>\n<td valign="top" style="text-align:center;word-break:break-word;padding:0 9px;">\n<div contenteditable="true">\n<div style="display:block; margin:15px 0 0 0; padding:0; font-size:125px; font-style:normal; line-height:100%; color:#fff;    letter-spacing: 15px;" align="center"><b>OUR</b></div></div>\n\n\n<div style=" margin:5px auto;"><hr style="background:#d8612d; height:2px; border:0; margin:0 auto;width:50%;"></div>\n\n<div contenteditable="true">\n<div style="display:block; margin:15px 0 25px 0; padding:0; font-size:95px; text-align:center;line-height:100%; color:#fff;" align="center;">EVENT</div>\n</div>\n</td>\n</tr>\n\n</tbody>\n</table>                                  \n</td>\n</tr>\n\n<tr>\n<td align="center" valign="top">\n<table border="0" cellpadding="0" cellspacing="0" width="600" style="border-collapse:collapse;background:#d8612d;max-width:600px;width:100%;">\n<tbody>\n<tr>\n<td valign="top" style="text-align:center;word-break:break-word;padding:9px;">\n</td>\n</tr>\n</tbody>\n</table>                                  \n</td>\n</tr>\n\n\n<tr>\n<td align="center" valign="top">\n                                    \n<table border="0" cellpadding="0" cellspacing="0" width="100%" style="border-collapse:collapse;border-top:0;border-bottom:0;background-color:#ffffff;width:100%;max-width:600px;">\n<tbody><tr><td align="center" valign="top">\n                                                \n\n<table align="left" border="0" cellpadding="0" cellspacing="0" style="min-width:100%;border-collapse:collapse;  clear:both;max-width:600px;width:100%;" width="100%;">\n<tbody>\n<tr>\n<td valign="top" style="padding-top:0; padding-right:18px; padding-bottom:9px; padding-left:18px;">\n\n<div contenteditable="true">\n<p style="display:block; margin:18px 0 15px 0; padding:0; font-size:23px; font-weight:400; text-align:center; color:#000; line-height:30px;">Your Awesome Event Discriotion Right Here</p>\n</div>\n\n<div contenteditable="true">\n<p style="display:block; margin:20px 0 18px 0; padding:0; font-size:60px; font-weight:400; text-align:center; color:#4a4949; line-height:30px;"><b>31.5.2016</b></p>\n</div>\n\n<div style=" margin:5px auto;"><hr style="background:#d6d6d6; height:2px; border:0; margin:0 auto;width:70%;"></div>\n\n<div contenteditable="true">\n<p style="display:block; margin:17px 0 10px 0; padding:0; font-size:21px; font-weight:400; text-align:center; color:#4a4949; line-height:30px;">And Where It All Gonna Happen?</p>\n</div>\n\n<div contenteditable="true">\n<div style="display:block; margin:15px 0 10px 0; padding:0; font-size:60px; font-style:normal; line-height:100%; color:#ff854f;" align="center"><b>YOUR CITY</b></div>\n</div>\n\n<div contenteditable="true">\n<div style="display:block; margin:15px 0 15px 0; padding:0; font-size:40px; text-align:center;line-height:100%; color:#ff854f;font-weight:100;" align="center;">AWESOME STREET</div>\n</div>\n\n<div contenteditable="true" style="text-align:center;">\n<a href="#" style="border-radius:05px; background-color:#d8612d; padding: 6px 30px; text-align:center; text-decoration:none; color:#ffffff; display:inline-block;font-size:23px;margin:30px 0 30px 0;" class="sm-button">Join Now</a></div>\n\n</td>\n</tr>\n\n<tr>\n<td align="center" valign="top">\n<table border="0" cellpadding="0" cellspacing="0" width="600" style="border-collapse:collapse;background:#d8612d;max-width:600px;width:100%;">\n<tbody>\n<tr>\n<td valign="top" style="padding:2px;">\n</td>\n</tr>\n</tbody>\n</table>                                  \n</td>\n</tr>\n\n\n</tbody>\n</table>\n\n\n</td>\n</tr>\n</tbody></table>\n</td>\n</tr>\n\n</tbody>\n</table>\n\n\n</td>\n</tr>\n</tbody>\n</table>\n\n\n</div>', 'template4.jpg', 1, 1470394007, '', 'active'),
(8, 'default', 'inline', 'Template 8', '<div style="width:100%; max-width:600px; margin: auto;">\n\n<table align="center" border="0" cellpadding="0" cellspacing="0" height="100%" width="100%" style="border-collapse:collapse;height:100%;margin:0;padding:0;background-color:#e9e9e9; width:100%;max-width:600px; margin:0 auto;">\n<tbody>\n<tr>\n<td align="center" valign="top" style="height:100%;margin:0;padding:0;width:100%;border-top:0">\n\n<table border="0" cellpadding="0" cellspacing="0" width="100%" style="border-collapse:collapse;width:100%;max-width:600px;">\n<tbody>\n<tr>\n<td align="left" valign="top">\n\n<table border="0" cellpadding="0" cellspacing="0" width="600" style="border-collapse:collapse; background:#fe546b;width:100%;max-width:600px;">\n<tbody>\n<tr>\n<td valign="top" style="padding:9px;text-align:left">\n<img align="left" alt="" src="{image_path}images/logo8.png" style="height:33px;max-height:33px;padding-bottom:0;display:inline!important;vertical-align:bottom;border:0;height:auto;outline:none;text-decoration:none"></td></tr></tbody></table>\n                                    \n</td>\n</tr>\n\n<tr>\n<td align="center" valign="top">\n                                    \n<table border="0" cellpadding="0" cellspacing="0" width="100%" style="border-collapse:collapse;border-top:0;border-bottom:0;width:100%;max-width:600px;">\n<tbody><tr><td align="center" valign="top">\n                                                \n<table border="0" cellpadding="0" cellspacing="0" width="600" style="border-collapse:collapse;width:100%;max-width:600px;">\n<tbody><tr><td align="center" valign="top">\n\n<table border="0" cellpadding="0" cellspacing="0" width="100%" style="border-collapse:collapse;background-color:#ffffff;border:0px solid #d5d5d5;width:100%;max-width:600px;">\n<tbody><tr><td valign="top">\n\n\n<table align="left" width="100%" border="0" cellpadding="0" cellspacing="0" style="min-width:100%;border-collapse:collapse;width:100%;max-width:600px;background-color:#eaede2;">\n<tbody><tr>\n\n<td valign="top" width="60%">\n<div contenteditable="true">\n<div style="display:block; padding:0; font-size:40px; color:#fe546b;font-weight:300;margin-left:50px;margin-top:60px;">Look and feel Your Best.</div>\n</div>\n<div contenteditable="true">\n<p style="display:block;padding:0; font-size:16px;color:#4a4949;margin-left:50px;margin-top:20px;">We have you covered from head to toe Hair, Body & facial Treatments</p>\n</div>\n</td>\n\n<td valign="top">\n<img align="center" alt="" src="{image_path}images/temp8.png" style="max-width:240px;max-height:273px;padding-bottom:0;display:inline!important;vertical-align:bottom;border:0;height:auto;outline:none;text-decoration:none" tabindex="0"></td>\n</tr>\n</tbody></table>\n\n\n<table align="left" border="0" cellpadding="0" cellspacing="0" style="max-width:100%;min-width:100%;border-collapse:collapse;  clear:both;width:100%;max-width:600px;" width="100%"><tbody><tr><td valign="top" style="padding-top:0; padding-right:18px; padding-bottom:9px; padding-left:18px; word-break:break-word; color:#606060; font-size:15px; text-align:left;">\n<div contenteditable="true">\n<div style="display:block; margin:15px 0 0 0; padding:0; font-size:40px; font-weight:300;color:#fe546b;" align="center">Beauty Zone</div>\n</div>\n\n<div contenteditable="true">\n<p style="display:block; margin:15px 0 5px 0; font-size:15px; text-align:center; color:#353535!important;padding:0 5%;">Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus.</p>\n</div>\n</td></tr></tbody>\n</table>\n\n\n\n<table border="0" cellpadding="15" cellspacing="0" style="width:100%;max-width:600px;min-width:100%;border-collapse:collapse; clear:both;text-align:center;" width="100%">\n<tbody>\n\n<tr>\n<td>\n<img alt="" src="{image_path}images/icon1.png" tabindex="0" style="border:1px solid #dbdbdb;border-radius:50%;width:94px;height:94px;margin:5px auto;">\n<div contenteditable="true">\n<p style="display:block; margin:15px 0 5px 0; font-size:18px;color:#4a4949;font-weight:500;">Cutting & Styling</p>\n</div>\n</td>\n\n<td>\n<img alt="" src="{image_path}images/icon2.png" tabindex="0" style="border:1px solid #dbdbdb;border-radius:50%;background:#fff;width:94px;height:94px;margin:5px auto;">\n<div contenteditable="true">\n<p style="display:block; margin:15px 0 5px 0; font-size:18px;color:#4a4949;font-weight:500;">Spa & Relaxing</p>\n</div>\n</td>\n\n<td>\n<img alt="" src="{image_path}images/icon3.png" tabindex="0" style="border:1px solid #dbdbdb;border-radius:50%;width:94px;height:94px;margin:5px auto;">\n<div contenteditable="true">\n<p style="display:block; margin:15px 0 5px 0; font-size:18px;color:#4a4949;font-weight:500;">Nail Treatment</p>\n</div>\n</td>\n\n</tr>\n</tbody>\n</table>\n\n<table align="left" border="0" cellpadding="0" cellspacing="0" style="max-width:100%;min-width:100%;border-collapse:collapse;  clear:both;width:100%;max-width:600px;" width="100%">\n<tbody><tr>\n<td valign="top" style="padding-top:0; padding-right:18px; padding-bottom:9px; padding-left:18px;">\n<div contenteditable="true" style="text-align:center;">\n<a href="#" style="border-radius:05px; background-color:#fe546b; padding: 1.8% 25%; text-align:center; text-decoration:none; color:#ffffff; display:inline-block;font-size:18px;margin:30px 0 30px 0;" class="sm-button">Get A Appoitment</a></div>\n</td></tr></tbody>\n</table>\n				\n\n</td>\n</tr>\n</tbody></table>\n</td>\n</tr>\n</tbody></table>\n</td>\n</tr>\n</tbody></table>\n</td>\n</tr>\n\n</tbody>\n</table>\n			\n	</td>\n    </tr>\n    </tbody>\n    </table>\n\n\n</div>', 'template13.jpg', 1, 1470394007, '', 'active'),
(9, 'default', 'inline', 'Template 9', '<div style="width:100%; max-width:600px; margin: auto;">\n\n\n<table align="center" border="0" cellpadding="0" cellspacing="0" height="100%" width="100%" style="border-collapse:collapse;height:100%;margin:0;padding:0;width:100%;background-color:#e9e9e9; max-width:600px; margin:0 auto;">\n<tbody>\n<tr>\n<td align="center" valign="top" style="height:100%;margin:0;padding:0;width:100%;border-top:0">\n\n<table border="0" cellpadding="0" cellspacing="0" width="100%" style="border-collapse:collapse;width:100%;max-width:600px;">\n<tbody>\n<tr>\n<td align="left" valign="top">\n\n<table border="0" cellpadding="0" cellspacing="0" width="600" style="border-collapse:collapse; background:#fff;width:100%;max-width:600px;">\n<tbody>\n<tr>\n<td valign="top" style="padding:9px;">\n<img align="left" alt="" src="{image_path}images/logo9.png" width="163" style="max-width:163px;padding-bottom:0;display:inline!important;vertical-align:bottom;border:0;height:auto;outline:none;\ntext-decoration:none;margin:10px 0 5px 15px;"></td></tr></tbody></table>                                 \n</td>\n</tr>\n\n<tr>\n<td align="center" valign="top">\n                                    \n<table border="0" cellpadding="0" cellspacing="0" width="100%" style="border-collapse:collapse;border-top:0;border-bottom:0;background-color:#ffffff;width:100%;max-width:600px;">\n<tbody><tr><td align="center" valign="top">\n                                                \n\n<table align="left" width="100%" border="0" cellpadding="0" cellspacing="0" style="min-width:100%;border-collapse:collapse;width:100%;max-width:600px;">\n<tbody><tr><td valign="top">\n<img align="center" alt="" src="{image_path}images/temp9.png" width="600" style="max-width:100%;padding-bottom:0;display:inline!important;vertical-align:bottom;border:0;height:auto;outline:none;text-decoration:none;max-height:203px;" tabindex="0"></td></tr>\n</tbody></table>\n\n\n<table align="left" border="0" cellpadding="0" cellspacing="0" style="max-width:100%;min-width:100%;border-collapse:collapse;  clear:both;width:100%;max-width:600px;" width="100%;"><tbody><tr><td valign="top" style="padding-top:0; padding-right:18px; padding-bottom:9px; padding-left:18px; word-break:break-word; color:#606060; font-size:15px; text-align:left;">\n\n\n<div contenteditable="true">\n<p style="display:block; margin:20px 0 10px 0; padding:0; font-size:25px;text-align:center; color:#4a4949;">Email Confirmation</p>\n</div>\n\n\n<div contenteditable="true">\n<p style="display:block; margin:2px 0 10px 0; padding:10px 25px ; font-size:15px; font-weight:400; text-align:center; color:#818080; line-height:25px;">Lorem ipsum dolor sit amet, no quodsi definitiones vis. Verterem postulant intellegat est et, an partiendo argumentum elaboraret qui. Diam salutatus cu est, choro sententiae nam id. An sint meliore conceptam qui, eum dicit corpora consequuntur no.\n</p>\n</div>\n\n<div contenteditable="true" style="text-align:center;">\n<a href="#" style="font-family:Roboto; border-radius:04px; background-color:#ffb200; padding:1.7% 6%; text-align:center; text-decoration:none; color:#ffffff; display:inline-block; margin:10px 0 20px 0;font-size:23px;" class="sm-button">Verify Email Address</a>\n</div>\n\n</td>\n</tr>\n</tbody>\n</table>\n                                 \n                                    \n</td>\n</tr>\n</tbody></table>\n</td>\n</tr>\n</tbody>\n</table>\n</td>\n</tr>\n</tbody>\n</table>\n\n\n</div>', 'template10.jpg', 1, 1470394007, '', 'active'),
(10, 'default', 'inline', 'Template 10', '<div style="width:100%; max-width:600px; margin: auto;">\n\n\n<table align="center" border="0" cellpadding="0" cellspacing="0" height="100%" width="100%" style="border-collapse:collapse;height:100%;margin:0;padding:0;background-color:#e9e9e9; width:100%;max-width:600px; margin:0 auto;">\n<tbody>\n<tr>\n<td align="center" valign="top" style="height:100%;margin:0;padding:0;width:100%;border-top:0">\n\n<table border="0" cellpadding="0" cellspacing="0" width="100%" style="border-collapse:collapse;width:100%;max-width:600px;">\n<tbody>\n<tr>\n<td align="left" valign="top">\n\n<table border="0" cellpadding="0" cellspacing="0" width="600" style="border-collapse:collapse; background:#98ba57;width:100%;max-width:600px;">\n<tbody>\n<tr>\n<td valign="top" style="padding:9px;text-align:left">\n<img align="left" alt="" src="{image_path}images/logo10.png" style="padding-bottom:0;display:inline!important;vertical-align:bottom;border:0;height:auto;outline:none;text-decoration:none"></td></tr></tbody></table>\n                                    \n</td>\n</tr>\n\n<tr>\n<td align="center" valign="top">\n                                    \n<table border="0" cellpadding="0" cellspacing="0" width="100%" style="border-collapse:collapse;border-top:0;border-bottom:0;width:100%;max-width:600px;">\n<tbody><tr><td align="center" valign="top">\n                                                \n<table border="0" cellpadding="0" cellspacing="0" width="600" style="border-collapse:collapse;width:100%;max-width:600px;">\n<tbody><tr><td align="center" valign="top">\n\n<table border="0" cellpadding="0" cellspacing="0" width="100%" style="border-collapse:collapse;background-color:#ffffff;border:0px solid #d5d5d5;width:100%;max-width:600px;">\n<tbody><tr><td valign="top">\n\n\n<table align="left" width="100%" border="0" cellpadding="0" cellspacing="0" style="min-width:100%;border-collapse:collapse;width:100%;max-width:600px;background-color:#ffffff;">\n<tbody><tr>\n\n<td valign="top" width="55%;">\n<div contenteditable="true">\n<div style="display:block; padding:0; font-size:38px; color:#4a4949;margin-left:50px;margin-top:30px;"><b>Free</b></div>\n</div>\n<div contenteditable="true">\n<div style="display:block; padding:0; font-size:33px; color:#98ba57;margin-left:50px;"><b>First Consultant</b></div>\n</div>\n<div contenteditable="true">\n<p style="display:block;padding:0 10%; font-size:10px;color:#b6b6b6;margin-left:25px;margin-top:20px;">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>\n</div>\n</td>\n\n<td valign="top">\n<img align="center" alt="" src="{image_path}images/temp10.png" style="max-width:270px;max-height:240px;padding-bottom:0;display:inline!important;vertical-align:bottom;border:0;height:auto;outline:none;text-decoration:none" tabindex="0"></td>\n</tr>\n</tbody></table>\n\n\n<table border="0" cellpadding="15" cellspacing="0" style="width:100%;max-width:600px;min-width:100%;border-collapse:collapse; clear:both;text-align:center;" width="100%">\n<tbody>\n\n<tr>\n<td width="30%;" style="background-color:#f4f4f4;">\n<img alt="" src="{image_path}images/icon4.png" tabindex="0" style="border-radius:50%;background:#fff;width:70px;height:70px;margin:5px auto;">\n<div contenteditable="true">\n<p style="display:block; margin:10px 0; font-size:15px;color:#4a4949;">Painless Treatment</p>\n</div>\n</td>\n\n<td width="30%;" style="background-color:#ececec;">\n<img alt="" src="{image_path}images/icon5.png" tabindex="0" style="border-radius:50%;background:#fff;width:70px;height:70px;margin:5px auto;">\n<div contenteditable="true">\n<p style="display:block; margin:15px 0 5px 0; font-size:15px;color:#4a4949;">Professional Services</p>\n</div>\n</td>\n\n<td width="30%;" style="background-color:#f4f4f4;">\n<img alt="" src="{image_path}images/icon6.png" tabindex="0" style="border-radius:50%;background:#fff;width:70px;height:70px;margin:5px auto;">\n<div contenteditable="true">\n<p style="display:block; margin:15px 0 5px 0; font-size:15px;color:#4a4949;">High Quality Drugs</p>\n</div>\n</td>\n\n</tr>\n</tbody>\n</table>\n\n\n<table align="left" border="0" cellpadding="0" cellspacing="0" style="max-width:100%;min-width:100%;border-collapse:collapse; clear:both;width:100%;max-width:600px;" width="100%"><tbody><tr><td valign="top" style="padding-top:0; padding-right:18px; padding-bottom:9px; padding-left:18px;color:#606060; font-size:15px;">\n\n\n<div contenteditable="true">\n<p style="display:block;font-size:13px; text-align:center; color:#bfbfbf!important;line-height:21px;margin-top:5%;">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.</p>\n</div>\n</td></tr></tbody>\n</table>\n\n\n<table align="left" border="0" cellpadding="0" cellspacing="0" style="min-width:100%;border-collapse:collapse;  clear:both;width:100%;max-width:600px;" width="100%">\n<tbody><tr>\n<td valign="top" style="padding-top:0; padding-right:18px; padding-bottom:9px; padding-left:18px;">\n<div contenteditable="true" style="text-align:center;">\n<a href="#" style="border-radius:05px; background-color:#98ba57; padding: 2.2% 5%; text-align:center; text-decoration:none; color:#ffffff; display:inline-block;font-size:21px;font-weight:300;margin:30px 0 30px 0;" class="sm-button">Click Here, To Book Your Appointment Today.</a></div>\n</td></tr></tbody>\n</table>\n				\n\n\n</td>\n</tr>\n</tbody></table>\n</td>\n</tr>\n</tbody></table>\n</td>\n</tr>\n</tbody></table>\n</td>\n</tr>\n\n</tbody>\n</table>\n			\n</td>\n</tr>\n</tbody>\n</table>\n\n\n</div>', 'template6.jpg', 1, 1470394007, '', 'active');
INSERT INTO `tbl_template` (`id`, `template_type`, `editor_type`, `template_name`, `template_text`, `template_screenshot`, `member_id`, `add_time`, `ip`, `status`) VALUES
(11, 'default', 'inline', 'Template 11', '<div style="width:100%; max-width:600px; margin: auto;">\n\n\n<table align="center" border="0" cellpadding="0" cellspacing="0" height="100%" width="100%" style="border-collapse:collapse;height:100%;margin:0;padding:0;background-color:#fff; width:100%;max-width:600px; margin:0 auto;">\n<tbody>\n<tr>\n<td align="center" valign="top" style="height:100%;margin:0;padding:0;width:100%;border-top:0">\n\n<table border="0" cellpadding="0" cellspacing="0" width="100%" style="border-collapse:collapse;width:100%;max-width:600px;">\n<tbody>\n<tr>\n<td>\n\n\n<table width="100%" border="0" cellpadding="0" cellspacing="0" style="min-width:100%;border-collapse:collapse;width:100%;max-width:600px;background-color:#fff;">\n<tbody><tr>\n\n<td valign="top"> \n<table width="100%" border="0" cellpadding="0" cellspacing="0" style="border-collapse:collapse;width:100%;max-width:600px;background-color:#fff;">\n<tbody><tr>\n<td valign="top" style="padding:20px 20px;text-align:left">\n<img align="left" alt="" src="{image_path}images/logo11.png" style="padding-bottom:0;display:inline!important;vertical-align:bottom;border:0;height:auto;outline:none;text-decoration:none"></td>\n\n</tr>\n</tbody>\n</table>\n</td>\n</tr>\n\n</tbody>\n</table>\n\n</td>\n</tr>\n\n</tbody>\n</table>\n \n<table width="100%" border="0" cellpadding="0" cellspacing="0" style="border-collapse:collapse;width:97%;max-width:600px;background-color:#00a2b5;">\n<tbody>\n\n<tr>\n\n<td valign="top">\n<img align="center" alt="" src="{image_path}images/temp11.png" style="max-height:292px;max-width:214px;padding-bottom:0;display:inline!important;vertical-align:bottom;border:0;height:auto;outline:none;text-decoration:none;margin-top:20px;margin-left:10px;" tabindex="0"></td>\n\n<td valign="top">\n<div contenteditable="true">\n<div style="display:block; padding:0; font-size:65px; color:#fff;margin-left:20px;margin-top:25px;font-weight:100;">Free</div>\n</div>\n<div contenteditable="true">\n<div style="display:block; padding:0; font-size:60px; color:#fff;margin-left:20px;font-weight:100;">Shipping</div>\n</div>\n<div contenteditable="true">\n<a href="#" style="border-radius:25px; background-color:#fff; padding: 1.5% 17%; text-align:center; text-decoration:none; color:#4a4949; display:inline-block;font-size:21px;font-weight:400;margin:15px 0 5px 20px;" class="sm-button">On All Orders</a></div>\n<div contenteditable="true" style="display:block;font-size:14px;color:#fff;padding-left: 25px;\n padding-bottom: 5px; padding-top: 5px;">\nYou will get your delivery by Diwali\n</div>\n</td>\n</tr>\n\n</tbody>\n</table>\n\n\n\n\n\n\n<table border="0" cellpadding="0" cellspacing="0" width="100%" style="border-collapse:collapse;background-color:#ffffff;border:0px solid #d5d5d5;width:95%;max-width:600px;">\n<tbody><tr><td valign="top">\n<table border="0" cellpadding="15" cellspacing="0" style="width:100%;max-width:600px;min-width:100%;border-collapse:collapse; clear:both;text-align:center;margin-top:3%;" width="100%">\n<tbody>\n\n<tr>\n<td>\n<img align="center" alt="" src="{image_path}images/icon7.png" style="border:1px solid #e7e7e7;outline:none;max-height:108px;" tabindex="0">\n<div contenteditable="true" style="">\n<div style="display:block; font-size:16px;color:#4a4949;margin:5px 0 0 0;">Hand Bags</div>\n<div style="display:block;font-size:13px;color:#4a4949;margin:5px 0 0 0; ">Under Rs 499</div>\n</div>\n<div contenteditable="true" style="text-align:center;">\n<a href="#" style="border-radius:05px; background-color:#fe9e01;padding: 4% 15%; text-align:center; text-decoration:none; color:#ffffff; display:inline-block;font-size:14px;margin:5px 0 5px 0;" class="sm-button">Buy Now</a></div>\n</td>\n\n<td>\n<img align="center" alt="" src="{image_path}images/icon8.png"   style="border:1px solid #e7e7e7;outline:none;max-height:108px;" tabindex="0">\n<div contenteditable="true" style="">\n<div style="display:block; font-size:16px;color:#4a4949;margin:5px 0 0 0;">Sunglasses</div>\n<div style="display:block;font-size:13px;color:#4a4949;margin:5px 0 0 0; ">Under Rs 499</div>\n</div>\n<div contenteditable="true" style="text-align:center;">\n<a href="#" style="border-radius:05px; background-color:#fe9e01;padding: 4% 15%; text-align:center; text-decoration:none; color:#ffffff; display:inline-block;font-size:14px;margin:5px 0 5px 0;" class="sm-button">Buy Now</a></div>\n</td>\n\n<td>\n<img align="center" alt="" src="{image_path}images/icon9.png"  style="border:1px solid #e7e7e7;outline:none;max-height:108px;" tabindex="0">\n<div contenteditable="true" style="">\n<div style="display:block; font-size:16px;color:#4a4949;margin:5px 0 0 0;">Backpacks</div>\n<div style="display:block;font-size:13px;color:#4a4949;margin:5px 0 0 0; ">Under Rs 499</div>\n</div>\n<div contenteditable="true" style="text-align:center;">\n<a href="#" style="border-radius:05px; background-color:#fe9e01;padding: 4% 15%; text-align:center; text-decoration:none; color:#ffffff; display:inline-block;font-size:14px;margin:5px 0 5px 0;" class="sm-button">Buy Now</a></div>\n</td>\n\n</tr>\n\n<tr>\n\n<td>\n<img align="center" alt="" src="{image_path}images/icon12.png" style="border:1px solid #e7e7e7;outline:none;max-height:108px;" tabindex="0">\n<div contenteditable="true" style="">\n<div style="display:block; font-size:16px;color:#4a4949;margin:5px 0 0 0;">Watches</div>\n<div style="display:block;font-size:13px;color:#4a4949;margin:5px 0 0 0; ">Under Rs 499</div>\n</div>\n<div contenteditable="true" style="text-align:center;">\n<a href="#" style="border-radius:05px; background-color:#fe9e01;padding: 4% 15%; text-align:center; text-decoration:none; color:#ffffff; display:inline-block;font-size:14px;margin:5px 0 5px 0;" class="sm-button">Buy Now</a></div>\n</td>\n\n<td>\n<img align="center" alt="" src="{image_path}images/icon10.png" style="border:1px solid #e7e7e7;outline:none;max-height:108px;" tabindex="0">\n<div contenteditable="true" style="">\n<div style="display:block; font-size:16px;color:#4a4949;margin:5px 0 0 0;">Earings</div>\n<div style="display:block;font-size:13px;color:#4a4949;margin:5px 0 0 0; ">Under Rs 499</div>\n</div>\n<div contenteditable="true" style="text-align:center;">\n<a href="#" style="border-radius:05px; background-color:#fe9e01;padding: 4% 15%; text-align:center; text-decoration:none; color:#ffffff; display:inline-block;font-size:14px;margin:5px 0 5px 0;" class="sm-button">Buy Now</a></div>\n</td>\n\n<td>\n<img align="center" alt="" src="{image_path}images/icon11.png" style="border:1px solid #e7e7e7;outline:none;max-height:108px;" tabindex="0">\n<div contenteditable="true" style="">\n<div style="display:block; font-size:16px;color:#4a4949;margin:5px 0 0 0;">Bangles</div>\n<div style="display:block;font-size:13px;color:#4a4949;margin:5px 0 0 0; ">Under Rs 499</div>\n</div>\n<div contenteditable="true" style="text-align:center;">\n<a href="#" style="border-radius:05px; background-color:#fe9e01;padding: 4% 15%; text-align:center; text-decoration:none; color:#ffffff; display:inline-block;font-size:14px;margin:5px 0 5px 0;" class="sm-button">Buy Now</a></div>\n</td>\n</tr>\n\n</tbody>\n</table>\n\n\n<tr>\n<td align="center" valign="top">\n<table border="0" cellpadding="0" cellspacing="0" width="600" style="border-collapse:collapse;max-width:600px;width:100%;">\n<tbody>\n<tr>\n<div contenteditable="true">\n<div style="display:block; font-size:13px; text-align:right;line-height:100%; color:#4a4949;" align="center;">*T & C Apply</div></div>\n</tr>\n</tbody>\n</table>                                  \n</td>\n</tr>\n\n\n<tr>\n<td align="center" valign="top">\n<table border="0" cellpadding="0" cellspacing="0" width="600" style="border-collapse:collapse;background:#00a2b5;max-width:600px;width:100%;margin:2% 0;">\n<tbody>\n<tr>\n<td valign="top" style="padding:5px;">\n</td>\n</tr>\n</tbody>\n</table>                                  \n</td>\n</tr>\n\n\n</td>\n</tr>\n</tbody></table>\n			\n</td>\n</tr>\n</tbody>\n</table>\n\n\n</div>', 'template2.jpg', 1, 1470394007, '', 'active'),
(12, 'default', 'inline', 'Template 12', '<div style="width:100%; max-width:600px; margin: auto;">\n\n\n<table align="center" border="0" cellpadding="0" cellspacing="0" height="100%" width="100%" style="border-collapse:collapse;height:100%;margin:0;padding:0;background-color:#e9e9e9; width:100%;max-width:600px; margin:0 auto;">\n<tbody>\n<tr>\n<td align="center" valign="top" style="height:100%;margin:0;padding:0;width:100%;border-top:0">\n\n<table border="0" cellpadding="0" cellspacing="0" width="100%" style="border-collapse:collapse;width:100%;max-width:600px;">\n<tbody>\n<tr>\n<td align="left" valign="top">\n\n<table border="0" cellpadding="0" cellspacing="0" width="600" style="border-collapse:collapse; background:#fff;width:100%;max-width:600px;">\n<tbody>\n<tr>\n<td valign="top" style="padding:17px 30px;text-align:left">\n<img align="left" alt="" src="{image_path}images/logo12.png" style="padding-bottom:0;display:inline!important;vertical-align:bottom;border:0;height:auto;outline:none;text-decoration:none;\nmax-height:39px;"></td></tr></tbody></table>\n\n<tr>\n<td align="center" valign="top">\n<table border="0" cellpadding="0" cellspacing="0" width="600" style="border-collapse:collapse;background:#eb775e;max-width:600px;width:100%;">\n<tbody>\n<tr>\n<td valign="top" style="padding:2px;">\n</td>\n</tr>\n</tbody>\n</table>                                  \n</td>\n</tr>\n                                    \n</td>\n</tr>\n\n<tr>\n<td align="center" valign="top">\n                                    \n<table border="0" cellpadding="0" cellspacing="0" width="100%" style="border-collapse:collapse;border-top:0;border-bottom:0;width:100%;max-width:600px;">\n<tbody><tr><td align="center" valign="top">\n                                                \n<table border="0" cellpadding="0" cellspacing="0" width="600" style="border-collapse:collapse;width:100%;max-width:600px;">\n<tbody><tr><td align="center" valign="top">\n\n<table border="0" cellpadding="0" cellspacing="0" width="100%" style="border-collapse:collapse;background-color:#ffffff;border:0px solid #d5d5d5;width:100%;max-width:600px;">\n<tbody><tr><td valign="top">\n\n\n<table align="left" width="100%" border="0" cellpadding="0" cellspacing="0" style="min-width:100%;border-collapse:collapse;width:100%;max-width:600px;background-color:#fbf7eb;">\n<tbody><tr>\n\n<td valign="top">\n<div style="background:#f36345;margin-top: 23%;margin-left:14%;padding:7% 0;margin-right:5%;" align="center">\n<div contenteditable="true">\n<div style="font-size:45px; color:#fff;">Get</div>\n<div style="font-size:26px; color:#fff;">20% Discount</div>\n<div style="font-size:19px;color:#fff;margin-top:1%;">This Summer</div>\n</div>\n</div>\n</td>\n\n<td valign="top" width="60%">\n<img align="center" alt="" src="{image_path}images/temp12.png" style="max-width:360px;max-height:274px;padding-bottom:0;display:inline!important;vertical-align:bottom;border:0;height:auto;outline:none;text-decoration:none" tabindex="0"></td>\n</tr>\n</tbody></table>\n\n\n<table border="0" cellpadding="15" cellspacing="0" style="width:100%;max-width:600px;min-width:100%;border-collapse:collapse; clear:both;" width="100%">\n<tbody>\n<tr>\n<td>\n\n<table border="0" cellpadding="15" cellspacing="0" style="width:100%;max-width:600px;min-width:100%;border-collapse:collapse; clear:both;" width="100%">\n<tbody>\n<tr>\n<td>\n<img align="center" alt="" src="{image_path}images/icon13.png" tabindex="0" style="max-width:214px;max-height:142px;">\n</td>\n\n<td>\n<div contenteditable="true" style="padding:2%;">\n<div style="font-size:16px;color:#f36345;">Your Beauty Starts Here</div>\n<div style="font-size:13px;color:#4a4949;margin-top:2%;">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</div>\n</div>\n</td>\n</tr>\n\n</tbody>\n</table>\n\n<table border="0" cellpadding="15" cellspacing="0" style="width:100%;max-width:600px;min-width:100%;border-collapse:collapse; clear:both;" width="100%">\n<tbody>\n<tr>\n<td>\n<div contenteditable="true">\n<div style="font-size:16px;color:#f36345;">Style Your Hair With Latest Hairstyles</div>\n<div style="font-size:13px;color:#4a4949;margin-top:2%;">\n<ul style="margin-left: -7%;">\n<li>Fish Tail</li>\n<li>Water Fall</li>\n<li>French Bub</li>\n</ul>\n</div>\n</div>\n</td>\n<td>\n<img alt="" src="{image_path}images/icon14.png" tabindex="0" style="max-width:210px;max-height:140px;">\n</td>\n</tr>\n</tbody>\n</table>\n\n<table border="0" cellpadding="15" cellspacing="0" style="width:100%;max-width:600px;min-width:100%;border-collapse:collapse; clear:both;" width="100%">\n<tbody>\n<tr>\n<td>\n<img alt="" src="{image_path}images/icon15.png" tabindex="0" style="max-width:214px;max-height:143px;">\n</td>\n<td>\n<div contenteditable="true">\n<div style="font-size:16px;color:#f36345;">Special Discount On Bridal Package</div>\n<div style="font-size:13px;color:#4a4949;margin-top:2%;">\n<ul  style="margin-left: -7%;">\n<li>Water Proof Makeup</li>\n<li>Every Type Of Hair Styles</li>\n<li>Manicure & Pedicure</li>\n<li>Body Polishing </li>  \n</ul>\n</div>\n</div>\n</td>\n</tr>\n</tbody>\n</table>\n\n</td>\n</tr>\n</tbody>\n</table>\n\n\n<table align="left" border="0" cellpadding="0" cellspacing="0" style="min-width:100%;border-collapse:collapse;  clear:both;width:100%;max-width:600px;" width="100%">\n<tbody><tr>\n<td valign="top" style="padding-top:0; padding-right:18px; padding-bottom:9px; padding-left:18px;">\n<div contenteditable="true" style="text-align:center;">\n<a href="#" style="border-radius:03px; background-color:#f36345; padding: 2.2% 23%; text-align:center; text-decoration:none; color:#ffffff; display:inline-block;font-size:19px;margin:30px 0 30px 0;" class="sm-button">Book Your Appointment Now</a></div>\n</td></tr></tbody>\n</table>\n				\n			\n\n\n</td>\n</tr>\n</tbody></table>\n</td>\n</tr>\n</tbody></table>\n</td>\n</tr>\n</tbody></table>\n</td>\n</tr>\n\n</tbody>\n</table>\n			\n</td>\n</tr>\n</tbody>\n</table>\n\n\n</div>', 'template12.jpg', 1, 1470394007, '', 'active'),
(13, 'default', 'inline', 'Template 13', '<div style="width:100%; max-width:600px; margin: auto;">\n\n\n<table align="center" border="0" cellpadding="0" cellspacing="0" height="100%" width="100%" style="border-collapse:collapse;height:100%;margin:0 auto;padding:0;width:100%;background-color:#e9e9e9;max-width:600px;">\n<tbody>\n<tr>\n<td align="center" valign="top" style="height:100%;margin:0;padding:0;width:100%;border-top:0">\n\n<table border="0" cellpadding="0" cellspacing="0" width="100%" style="border-collapse:collapse;max-width:600px;width:100%;">\n<tbody>\n<table border="0" cellpadding="0" cellspacing="0" width="600" style="border-collapse:collapse; background:#fff;max-width:600px;width:100%;">\n<tbody>\n<tr>\n<td valign="top" style="padding:18px 0;text-align:center">\n<img align="center" alt="" src="{image_path}images/logo13.png" style="padding-bottom:0;display:inline!important;vertical-align:bottom;border:0;width:105px;height:51px;outline:none;text-decoration:none">\n</td>\n</tr>\n\n<tr>\n<td>\n<div contenteditable="true">\n<div style="display:block; margin:0px 0 15px 0;font-size:15px;color:#050505;" align="center">Womens / Mens / Shoes / Denim</div>\n</div>\n</td>\n</tr>\n\n</tbody>\n</table>\n\n\n\n\n<table border="0" cellpadding="0" cellspacing="0" width="100%" style="border-collapse:collapse;background-color:#fff;border:1px solid #fff;max-width:600px;width:100%;">\n<tbody>\n<tr>\n<td valign="top">\n\n<table border="0" cellpadding="0" cellspacing="0" width="92%" style="border-collapse:collapse;margin:0 auto 25px auto;max-width:550px;width:100%;background: #fba5a7;\nbackground: -moz-linear-gradient(left,  #fba5a7 1%, #f5aa98 49%, #ed948d 53%, #9dc3be 100%);\nbackground: -webkit-linear-gradient(left,  #fba5a7 1%,#f5aa98 49%,#ed948d 53%,#9dc3be 100%);\nbackground: linear-gradient(to right,  #fba5a7 1%,#f5aa98 49%,#ed948d 53%,#9dc3be 100%);\nfilter: progid:DXImageTransform.Microsoft.gradient( startColorstr=''#fba5a7'', endColorstr=''#9dc3be'',GradientType=1 );\n">\n<tbody>\n<tr>\n<td valign="top">\n<table border="0" cellpadding="0" cellspacing="0" style="max-width:470px;width:100%;border-collapse:collapse;clear:both;margin:35px auto;background:rgba(255,255,255, 0.5);" width="100%;"><tbody><tr><td valign="top">\n\n\n<tbody>\n<tr>\n<td valign="top">\n<table border="0" cellpadding="0" cellspacing="0" style="max-width:440px;width:100%;border-collapse:collapse;clear:both;margin:15px auto;border:1px solid #e6e6e6;" width="100%;"><tbody><tr><td valign="top" style="">\n\n<div contenteditable="true">\n<div style="display:block; margin:15px 0 0 0; padding:0; font-size:50px; text-align:center;line-height:100%; color:#050505;font-weight:400;" align="center;">48 HOUR SALE!</div>\n</div>\n\n<div contenteditable="true">\n<div style="display:block; margin:10px 0 0 0; padding:0; font-size: 23px; color:#050505;font-weight:300;" align="center">EXCLUSIVELY FOR YOU</div>\n</div>\n\n<div contenteditable="true"><div style="display:block; margin:10px 0 0px 0;font-size:23px;color:#ee562f;" align="center">ENDS TONIGHT</div></div>\n\n<hr style="background:#e4d8d5; height:1px; border:0; margin:15px 11px 5px 11px;">\n\n<div contenteditable="true"><div style="display:block; margin:20px 0 0px 0;font-size:31px;color:#050505;font-weight:400;" align="center"><b>15% OFF & 50 OR MORE</b></div></div>\n\n<div contenteditable="true">\n<div style="display:block; margin:5px 0 0px 0;font-size:17px;color:#9b857f;" align="center">USED CODE: JUST4U</div>\n</div>\n\n<div contenteditable="true" style="text-align:center;margin:30px 0 10px 0;">\n<a href="#" style="border-radius:0px; background-color:#ee562f; padding:6px 18px;text-align:center;text-decoration:none; color:#ffffff; display:inline-block;font-size:17px;border:2px solid #ce401b;font-weight:300;margin-bottom:2%;" class="sm-button">SHOP WOMENS</a> &nbsp;\n<a href="#" style="border-radius:0px; background-color:#ee562f; padding:6px 30px;text-align:center;text-decoration:none; color:#ffffff; display:inline-block;font-size:17px;border:2px solid #ce401b;font-weight:300;margin-bottom:2%;" class="sm-button">SHOP MENS</a>\n</div>\n</td>\n</tr>\n</tbody>\n</table>\n</td>\n</tr>\n</tbody></td></tr></tbody>\n</table>\n</td>\n</tr>\n\n</tbody>\n</table>\n</td>\n</tr>\n\n</tbody>\n</table>\n\n\n</td>\n</tr>\n</tbody>\n</table>\n\n\n</td>\n</tr>\n</tbody>\n</table>\n\n\n</div>', 'template8.jpg', 1, 1470394007, '', 'active'),
(14, 'default', 'inline', 'Template 14', '<div style="width:100%; max-width:600px; margin: auto;">\n\n\n<table align="center" border="0" cellpadding="0" cellspacing="0" height="100%" width="100%" style="border-collapse:collapse;height:100%;margin:0;padding:0;background-color:#e9e9e9; width:100%;max-width:600px; margin:0 auto;">\n<tbody>\n<tr>\n<td align="center" valign="top" style="height:100%;margin:0;padding:0;width:100%;border-top:0">\n\n<table border="0" cellpadding="0" cellspacing="0" width="100%" style="border-collapse:collapse;width:100%;max-width:600px;">\n<tbody>\n<tr>\n<td align="left" valign="top">\n\n<table border="0" cellpadding="0" cellspacing="0" width="600" style="border-collapse:collapse; background:#fff;width:100%;max-width:600px;">\n<tbody>\n<tr>\n<td valign="top" align="center">\n<div contenteditable="true" style="text-align:center;">\n<div style="display:block; padding:0; font-size:35px; color:#0085b1;margin:15px auto;font-weight:500;">Converting Your Dreams Into<br>Reality Is Our Passion</div>\n</div></td></tr></tbody></table>\n                                    \n</td>\n</tr>\n\n<tr>\n<td align="center" valign="top">\n                                    \n<table border="0" cellpadding="0" cellspacing="0" width="100%" style="border-collapse:collapse;border-top:0;border-bottom:0;width:100%;max-width:600px;">\n<tbody><tr><td align="center" valign="top">\n                                                \n<table border="0" cellpadding="0" cellspacing="0" width="600" style="border-collapse:collapse;width:100%;max-width:600px;">\n<tbody><tr><td align="center" valign="top">\n\n<table border="0" cellpadding="0" cellspacing="0" width="100%" style="border-collapse:collapse;background-color:#ffffff;border:0px solid #d5d5d5;width:100%;max-width:600px;">\n<tbody><tr><td valign="top">\n\n\n<table align="center" width="100%" border="0" cellpadding="0" cellspacing="0" style="min-width:100%;border-collapse:collapse;width:100%;max-width:600px;background-color:#0085b1;">\n<tbody><tr>\n\n<td valign="top" align="center" style="padding-top:0; padding-right:18px; padding-bottom:9px; padding-left:18px; word-break:break-word;">\n<div contenteditable="true">\n<div style="display:block;font-size:18px; color:#ffffff;margin:25px 21px 25px 21px;font-weight:300;line-height:25px;">We constantly work with the sole objective of seeing you grow in every facet of business. Whether you are a newbie or an experienced business pro, we work to create a distinct entity for you from your competitors. Our dedicated team of experts are standing by your side always.</div>\n</div>\n\n<div contenteditable="true">\n<div style="display:block; margin:15px 0 0 0; font-size:30px;color:#ffffff;" align="center">Checkout Our Exclusive<br>Business Oriented Services</div>\n</div>\n\n</td>\n</tr>\n</tbody></table>\n\n\n<table border="0" cellpadding="15" cellspacing="0" style="width:100%;max-width:600px;min-width:100%;border-collapse:collapse; clear:both;text-align:center;background:#0085b1;" width="100%">\n<tbody>\n<tr>\n<td>\n\n\n<table border="0" cellpadding="15" cellspacing="0" style="width:100%;max-width:600px;min-width:100%;border-collapse:collapse; clear:both;text-align:center;background:#0085b1;" width="100%">\n<tbody>\n\n<tr>\n<td>\n<div style="background:#fff;border-radius:5px;padding:10% 0;">\n<img alt="" src="{image_path}images/icon19.png" tabindex="0" style="border:1px solid #dbdbdb;border-radius:50%;width:82px;height:82px;margin:0 auto;max-width:82px;max-height:82px;">\n<div contenteditable="true">\n<div style="display:block; margin:15px 0 0px 0; font-size:16px;font-weight:400;color:#0085b1;">Email Marketing</div>\n<div style="display:block;font-size:13px;color:#818080;font-weight:300;padding:5% 5% 5% 5%;">Lorem ipsum dolor sit amet, no quodsi definitiones vis.</div> \n</div>\n</div>\n</td>\n\n<td>\n<div style="background:#fff;border-radius:5px;padding:10% 0;">\n<img alt="" src="{image_path}images/icon20.png" tabindex="0" style="border:1px solid #dbdbdb;border-radius:50%;width:82px;height:82px;margin:0 auto;max-width:82px;max-height:82px;">\n<div contenteditable="true">\n<div style="display:block; margin:15px 0 0px 0; font-size:16px;font-weight:400;color:#0085b1;">Content Creation</div>\n<div style="display:block;font-size:13px;color:#818080;font-weight:300;padding:5% 5% 5% 5%;">Lorem ipsum dolor sit amet, no quodsi definitiones vis.</div>\n</div>\n</div>\n</td>\n\n<td>\n<div style="background:#fff;border-radius:5px;padding:10% 0;">\n<img alt="" src="{image_path}images/icon21.png" tabindex="0" style="border:1px solid #dbdbdb;border-radius:50%;width:82px;height:82px;margin:0 auto;max-width:82px;max-height:82px;">\n<div style="display:block; margin:15px 0 0px 0; font-size:16px;font-weight:400;color:#0085b1;">Data Analysis</div>\n<div style="display:block;font-size:13px;color:#818080;font-weight:300;padding:5% 5% 5% 5%;">Lorem ipsum dolor sit amet, no quodsi definitiones vis.</div>\n</div>\n</td>\n</tr>\n\n</tbody></table>\n</td>\n</tr>\n</tbody>\n</table>\n\n</td>\n</tr>\n</tbody>\n</table>\n\n<table border="0" cellpadding="15" cellspacing="0" style="width:100%;max-width:600px;min-width:100%;border-collapse:collapse; clear:both;text-align:center;background:#f1f1f1;" width="100%">\n<tbody>\n\n<tr><td>\n<div contenteditable="true">\n<div style="display:block;font-size:25px;color:#4a4949;margin:15px auto;">Our Satisfied Consumers</div>\n</div>\n</td></tr>\n</tbody></table>\n\n\n<table border="0" cellpadding="15" cellspacing="0" style="width:100%;max-width:600px;min-width:100%;border-collapse:collapse; clear:both;text-align:center;background:#f1f1f1;" width="100%">\n<tbody>\n<tr>\n<td>\n\n<table border="0" cellpadding="15" cellspacing="0" style="width:100%;max-width:600px;min-width:100%;border-collapse:collapse; clear:both;text-align:center;background:#f1f1f1;" width="100%">\n<tbody>\n<tr>\n<td><img alt="" src="{image_path}images/icon16.png" tabindex="0" style="margin:0 auto;border:1px solid #62b3ce;border-radius:50%;background:#fff;width: 115px;height: 115px;"></td>\n<td><img alt="" src="{image_path}images/icon17.png" tabindex="0" style="margin:0 auto;border:1px solid #62b3ce;border-radius:50%;background:#fff;width: 115px;height: 115px;"></td>\n<td><img alt="" src="{image_path}images/icon18.png" tabindex="0" style="margin:0 auto;border:1px solid #62b3ce;border-radius:50%;background:#fff;width: 115px;height: 115px;"></td>\n</tr>\n</tbody>\n</table>\n</td>\n</tr>\n</tbody>\n</table>\n\n<table align="center" border="0" cellpadding="0" cellspacing="0" style="max-width:100%;min-width:100%;border-collapse:collapse; clear:both;width:100%;max-width:600px;background:#fff;" width="100%">\n<tbody><tr>\n<td valign="top" style="padding-top:0; padding-right:14px; padding-bottom:9px; padding-left:14px;">\n\n<div contenteditable="true">\n<div style="display:block;font-size:15px; color:#818080;margin:25px 40px 0 40px;font-weight:300;line-height:25px;text-align:center;">We don’t need to speak much about our work as our huge client base speaks volumes. Stop thinking now and contact us as you could be the next on this list of happy and satisfied businesses.</div>\n</div>\n\n<div contenteditable="true" style="text-align:center;">\n<a href="#" style="border-radius:05px; background-color:#0085b1; padding: 2.5% 4%; text-align:center; text-decoration:none; color:#ffffff; display:inline-block;font-size:20px;margin:30px 0 30px 0;" class="sm-button">CONTACT OUR BUSINESS EXPERTS NOW</a></div>\n\n</td>\n</tr></tbody>\n</table>\n				\n\n\n</td>\n</tr>\n</tbody></table>\n</td>\n</tr>\n</tbody></table>\n</td>\n</tr>\n</tbody></table>\n</td>\n</tr>\n\n</tbody>\n</table>\n\n\n</div>', 'template14.jpg', 1, 1470394007, '', 'active');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_unsubscribe_list`
--

CREATE TABLE IF NOT EXISTS `tbl_unsubscribe_list` (
  `id` int(11) NOT NULL,
  `member_id` int(11) NOT NULL,
  `contact_id` int(11) NOT NULL,
  `email` varchar(255) NOT NULL,
  `ip` varchar(255) NOT NULL,
  `add_time` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_webform`
--

CREATE TABLE IF NOT EXISTS `tbl_webform` (
  `id` int(11) NOT NULL,
  `member_id` int(11) NOT NULL,
  `form_name` varchar(255) NOT NULL,
  `campaign_ids` varchar(255) NOT NULL,
  `template_id` int(11) DEFAULT NULL,
  `form_html` longtext NOT NULL,
  `status` enum('active','inactive') NOT NULL DEFAULT 'active',
  `add_time` int(11) NOT NULL,
  `thankyou_page` enum('default','custom') NOT NULL DEFAULT 'default',
  `thankyou_custom` text,
  `already_subscribe` enum('default','custom') NOT NULL DEFAULT 'default',
  `alreadysub_custom` text,
  `double_otp` enum('yes','no') NOT NULL DEFAULT 'no',
  `image` varchar(255) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_webform`
--

INSERT INTO `tbl_webform` (`id`, `member_id`, `form_name`, `campaign_ids`, `template_id`, `form_html`, `status`, `add_time`, `thankyou_page`, `thankyou_custom`, `already_subscribe`, `alreadysub_custom`, `double_otp`, `image`) VALUES
(1, 1, 'testing', '1', 8, '<div class="form8-bg" id="demo-preview">\r\n<div class="row"> \r\n<div class="col-md-12 col-sm-12 col-xs-12">\r\n<div class="col-md-12 col-sm-12 col-xs-12 form_area8 whitetext">\r\n<div class="mdem12 smem11 xsem10 text-center" contenteditable="false">Want to recieve the best offers of the weak ? </div>\r\n<div class="mdem22 smem15 xsem14 text-center w700 xsmb5" contenteditable="false">Join Today </div>\r\n<div class="col-md-10 col-md-offset-1 col-sm-10 col-sm-offset-1 col-xs-10 col-xs-offset-1 mt10 xsmt10">\r\n<div class="namefield">\r\n<div class="mdem10 smem9 xsem9"><input placeholder="Full Name" name="name" class="fieldinput" type="text"></div>\r\n</div>\r\n<div class="phonefield">\r\n<div class="mdem10 smem9 xsem9"><input placeholder="Phone Number" name="phone" class="fieldinput" type="text"></div>\r\n</div>\r\n<div class="copydiv">\r\n<div class="mdem10 smem9 xsem9"><input placeholder="Email Address" name="email" class="fieldinput" type="text"></div>\r\n</div>\r\n<div class="mdem14 smem13 xsem13 mt10 xsmt10 w500"><a href="#" class="form-btn8" id="rect" contenteditable="false">Join Now</a></div>\r\n</div>\r\n</div>				\r\n</div>\r\n</div>\r\n</div>', 'active', 1485939085, 'default', NULL, 'default', NULL, 'no', '11485940159.png');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_website_settings`
--

CREATE TABLE IF NOT EXISTS `tbl_website_settings` (
  `id` int(11) NOT NULL,
  `member_id` int(11) NOT NULL,
  `footer_text` longtext NOT NULL,
  `application_URL` text NOT NULL,
  `database_name` varchar(250) NOT NULL,
  `license_key` text,
  `plan_array` text NOT NULL,
  `mail_type` enum('default','smtp') NOT NULL DEFAULT 'default',
  `SMTP_hostname` text,
  `SMTP_username` text,
  `SMTP_password` text,
  `SMTP_port` text,
  `bounce_type` enum('default','custom') DEFAULT 'default',
  `bounce_address` text,
  `bounce_server` text,
  `bounce_password` text,
  `bounce_port` int(11) DEFAULT NULL,
  `account_type` text,
  `app_name` text,
  `app_footer` text,
  `app_logo` text,
  `favicon` text
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_website_settings`
--

INSERT INTO `tbl_website_settings` (`id`, `member_id`, `footer_text`, `application_URL`, `database_name`, `license_key`, `plan_array`, `mail_type`, `SMTP_hostname`, `SMTP_username`, `SMTP_password`, `SMTP_port`, `bounce_type`, `bounce_address`, `bounce_server`, `bounce_password`, `bounce_port`, `account_type`, `app_name`, `app_footer`, `app_logo`, `favicon`) VALUES
(1, 1, '', '', 'smartmailer', '366ea6-8a99de-2f896f-e81234c46', 'done', 'default', NULL, NULL, NULL, NULL, 'default', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `ci_sessions`
--
ALTER TABLE `ci_sessions`
  ADD PRIMARY KEY (`session_id`),
  ADD KEY `last_activity_idx` (`last_activity`);

--
-- Indexes for table `tbl_autoresponder`
--
ALTER TABLE `tbl_autoresponder`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_campaign`
--
ALTER TABLE `tbl_campaign`
  ADD PRIMARY KEY (`campaign_id`);

--
-- Indexes for table `tbl_contact`
--
ALTER TABLE `tbl_contact`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_cron_urls`
--
ALTER TABLE `tbl_cron_urls`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_email_queue`
--
ALTER TABLE `tbl_email_queue`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_file_attatch`
--
ALTER TABLE `tbl_file_attatch`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_form_template`
--
ALTER TABLE `tbl_form_template`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_from_email`
--
ALTER TABLE `tbl_from_email`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_member`
--
ALTER TABLE `tbl_member`
  ADD PRIMARY KEY (`mem_id`);

--
-- Indexes for table `tbl_newsletter`
--
ALTER TABLE `tbl_newsletter`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_statistics`
--
ALTER TABLE `tbl_statistics`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_suppression`
--
ALTER TABLE `tbl_suppression`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_suppression_list`
--
ALTER TABLE `tbl_suppression_list`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_template`
--
ALTER TABLE `tbl_template`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_unsubscribe_list`
--
ALTER TABLE `tbl_unsubscribe_list`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_webform`
--
ALTER TABLE `tbl_webform`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_website_settings`
--
ALTER TABLE `tbl_website_settings`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tbl_autoresponder`
--
ALTER TABLE `tbl_autoresponder`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tbl_campaign`
--
ALTER TABLE `tbl_campaign`
  MODIFY `campaign_id` bigint(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `tbl_contact`
--
ALTER TABLE `tbl_contact`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `tbl_cron_urls`
--
ALTER TABLE `tbl_cron_urls`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tbl_email_queue`
--
ALTER TABLE `tbl_email_queue`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tbl_file_attatch`
--
ALTER TABLE `tbl_file_attatch`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tbl_form_template`
--
ALTER TABLE `tbl_form_template`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `tbl_from_email`
--
ALTER TABLE `tbl_from_email`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `tbl_member`
--
ALTER TABLE `tbl_member`
  MODIFY `mem_id` bigint(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `tbl_newsletter`
--
ALTER TABLE `tbl_newsletter`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tbl_statistics`
--
ALTER TABLE `tbl_statistics`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tbl_suppression`
--
ALTER TABLE `tbl_suppression`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tbl_suppression_list`
--
ALTER TABLE `tbl_suppression_list`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tbl_template`
--
ALTER TABLE `tbl_template`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=15;
--
-- AUTO_INCREMENT for table `tbl_unsubscribe_list`
--
ALTER TABLE `tbl_unsubscribe_list`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tbl_webform`
--
ALTER TABLE `tbl_webform`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `tbl_website_settings`
--
ALTER TABLE `tbl_website_settings`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
