-- phpMyAdmin SQL Dump
-- version 4.4.14
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Nov 09, 2016 at 10:04 AM
-- Server version: 5.6.26
-- PHP Version: 5.6.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `smartmailer`
--

-- --------------------------------------------------------

--
-- Table structure for table `tbl_template`
--

CREATE TABLE IF NOT EXISTS `tbl_template` (
  `id` bigint(20) NOT NULL,
  `template_type` enum('default','draft') NOT NULL,
  `template_name` varchar(255) NOT NULL,
  `template_text` text NOT NULL,
  `member_id` bigint(20) NOT NULL,
  `add_time` int(11) NOT NULL,
  `ip` varchar(255) NOT NULL,
  `status` enum('active','inactive') NOT NULL DEFAULT 'active'
) ENGINE=InnoDB AUTO_INCREMENT=33 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_template`
--

INSERT INTO `tbl_template` (`id`, `template_type`, `template_name`, `template_text`, `member_id`, `add_time`, `ip`, `status`) VALUES
(8, 'draft', 'Sushil Audichya', '<p><em><strong><span style="font-size:24px">This is testing template 2</span></strong></em></p>\n\n<p>&nbsp;</p>\n\n<p><em><strong><span style="font-size:24px">This is testing template 2</span></strong></em></p>\n\n<p><em><strong><span style="font-size:24px">This is testing template 2</span></strong></em></p>\n', 1, 1475657292, '192.168.0.186', 'active'),
(9, 'draft', 'Tested temp', '<h2 style="font-style:italic;"><strong><span style="font-size:36px">New testing template&nbsp;</span></strong></h2>', 1, 1475737059, '192.168.0.186', 'active');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tbl_template`
--
ALTER TABLE `tbl_template`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tbl_template`
--
ALTER TABLE `tbl_template`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=33;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
