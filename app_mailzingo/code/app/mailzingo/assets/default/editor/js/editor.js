$(window).load(function(){
$('input[type="range"]').change(function () {
    var val = ($(this).val() - $(this).attr('min')) / ($(this).attr('max') - $(this).attr('min'));
    $(this).css('background-image',
                '-webkit-gradient(linear, left top, right top, '
                + 'color-stop(' + val + ', #749ced), '
                + 'color-stop(' + val + ', #a8a8a8)'
                + ')'
                );
});
});



//To make spans with inline css
//document.execCommand("styleWithCSS", true, null);
$(document).on("click",".buttonoptions",function() {
	$(".list-options").hide();
	$(".font-options").hide();
	$(".align-options").hide();
	$(".style-options").hide();
	$(".media-options").hide();
	$(".personalize-options").hide();
	$(".personalizeoptions").removeClass("is-open");
	$(".mediaoptions").removeClass("is-open");
	$(".listoptions").removeClass("is-open");
	$(".fontoptions").removeClass("is-open");
	$(".alignoptions").removeClass("is-open");
	$(".styleoptions").removeClass("is-open");
	$(".button-options").toggle();
    $(".buttonoptions").toggleClass("is-open");
});
$(document).on("click",".alignoptions",function() {
	$(".list-options").hide();
	$(".font-options").hide();
	$(".button-options").hide();
	$(".style-options").hide();
	$(".media-options").hide();
	$(".personalize-options").hide();
	$(".personalizeoptions").removeClass("is-open");
	$(".mediaoptions").removeClass("is-open");
	$(".listoptions").removeClass("is-open");
	$(".fontoptions").removeClass("is-open");
	$(".buttonoptions").removeClass("is-open");
	$(".styleoptions").removeClass("is-open");
	$(".align-options").toggle();
    $(".alignoptions").toggleClass("is-open");
});
$(document).on("click",".listoptions",function() {
	$(".align-options").hide();
	$(".font-options").hide();
	$(".button-options").hide();
	$(".style-options").hide();
	$(".media-options").hide();
	$(".personalize-options").hide();
	$(".personalizeoptions").removeClass("is-open");
	$(".mediaoptions").removeClass("is-open");
	$(".alignoptions").removeClass("is-open");
	$(".fontoptions").removeClass("is-open");
	$(".buttonoptions").removeClass("is-open");
	$(".styleoptions").removeClass("is-open");
	$(".list-options").toggle();
    $(".listoptions").toggleClass("is-open");
});
$(document).on("click",".fontoptions",function() {
	$(".align-options").hide();
	$(".list-options").hide();
	$(".button-options").hide();
	$(".style-options").hide();
	$(".media-options").hide();
	$(".personalize-options").hide();
	$(".personalizeoptions").removeClass("is-open");
	$(".mediaoptions").removeClass("is-open");
	$(".alignoptions").removeClass("is-open");
	$(".listoptions").removeClass("is-open");
	$(".buttonoptions").removeClass("is-open");
	$(".styleoptions").removeClass("is-open");
	$(".font-options").toggle();
    $(".fontoptions").toggleClass("is-open");
});
$(document).on("click",".styleoptions",function() {
	$(".align-options").hide();
	$(".list-options").hide();
	$(".button-options").hide();
	$(".font-options").hide();
	$(".media-options").hide();
	$(".personalize-options").hide();
	$(".personalizeoptions").removeClass("is-open");
	$(".mediaoptions").removeClass("is-open");
	$(".alignoptions").removeClass("is-open");
	$(".listoptions").removeClass("is-open");
	$(".buttonoptions").removeClass("is-open");
	$(".fontoptions").removeClass("is-open");
	$(".style-options").toggle();
    $(".styleoptions").toggleClass("is-open");
});
$(document).on("click",".mediaoptions",function() {
	$(".align-options").hide();
	$(".list-options").hide();
	$(".button-options").hide();
	$(".font-options").hide();
	$(".style-options").hide();
	$(".personalize-options").hide();
	$(".personalizeoptions").removeClass("is-open");
	$(".alignoptions").removeClass("is-open");
	$(".listoptions").removeClass("is-open");
	$(".buttonoptions").removeClass("is-open");
	$(".fontoptions").removeClass("is-open");
	$(".styleoptions").removeClass("is-open");
	$(".media-options").toggle();
    $(".mediaoptions").toggleClass("is-open");
});
$(document).on("click",".personalizeoptions",function() {
	$(".align-options").hide();
	$(".list-options").hide();
	$(".button-options").hide();
	$(".font-options").hide();
	$(".style-options").hide();
	$(".media-options").hide();
	$(".mediaoptions").removeClass("is-open");
	$(".alignoptions").removeClass("is-open");
	$(".listoptions").removeClass("is-open");
	$(".buttonoptions").removeClass("is-open");
	$(".fontoptions").removeClass("is-open");
	$(".styleoptions").removeClass("is-open");
	$(".personalize-options").toggle();
    $(".personalizeoptions").toggleClass("is-open");
});
$(document).on("click",".g-body",function(event) {
	event.preventDefault();
      event.stopPropagation();
});

//is used to get select editing element;
var EditingElement;

$(document).on("click",".edit_template",function(event) {
	target = event.target || event.srcElement
	if($(target).closest("#ImageEditor").length==0 && $(target).attr('id') != "tabs-3") {
		EditingElement = event.target || event.srcElement;
	}
});

$(document).on("change","#imgBorderColorvalue",function(event) {
	$(EditingElement).css({"border-color": "#"+$("#imgBorderColorvalue").val()});
});
$(document).on("click",".imgBorderColorvalue",function(event) {
	if($(EditingElement).outerWidth() == $(EditingElement).width()){
		$(EditingElement).css({"border-style": "solid"});
		$(EditingElement).css({"border-width": "1px"});
	}
	$(EditingElement).css({"border-color": "#"+$("#imgBorderColorvalue").val()});
	
});
$(document).on("input",".img-border-width",function(event) {
	$(EditingElement).css({"border-style": "solid"});
	$(EditingElement).css({"border-width": $(".img-border-width").val() + "px"});

});
$(document).on("input",".img-border-radius",function(event) {
	$(EditingElement).css({"border-style": "solid"});
	if($(EditingElement).outerWidth() == $(EditingElement).width()){
		$(EditingElement).css({"border-style": "solid"});
		$(EditingElement).css({"border-width": "1px"});
	}
	$(EditingElement).css({"border-radius": $(".img-border-radius").val() + "%"});
});


function buttonupdate(jscolor) {
    document.getElementById('rect').style.backgroundColor = '#' + jscolor
}





function getTemplate(){
	return $(".edit_template").html();
}

$(document).ready(function(){
	$(".editblock").removeClass("editblock");
});