(function() {
  /*jshint multistr:true */
  var EDGE = -999;

  var root = this,   // Root object, this is going to be the window for now
      document = this.document, // Safely store a document here for us to use
      editableNodes = document.querySelectorAll(".g-body article"),
      editNode = editableNodes[0], // TODO: cross el support for imageUpload
      isFirefox = navigator.userAgent.toLowerCase().indexOf('firefox') > -1,
      options = {
        animate: true
      },
      textMenu,
      imgMenu,
      optionsNode,
      urlInput,
      previouslySelectedText,
      imageTooltip,
      imageInput,
      imageBound;

      lpb_inline_editor = {
        bind: function(bindableNodes, opts) {
          if (bindableNodes) {
            editableNodes = bindableNodes;
          }

          options = opts || options;

          attachToolbarTemplate();
          bindTextSelectionEvents();
          bindTextStylingEvents();
        },
        select: function() {
          triggerTextSelection();
        }
      },

      tagClassMap = {
        "b": "bold",
        "i": "italic",
		"u": "underline",
		"strike": "strikeThrough",
		"alignleft": "justifyLeft",
		"aligncenter": "justifyCenter",
		"alignright": "justifyRight",
		"alignjustify": "justifyFull",
		"unorderedlist": "insertUnorderedList",
		"orderedlist": "insertOrderedList",
		"textColor": "textColor",
		"buttonbgcolor": "buttonbgcolor",
        "a": "customurl",
		"fontfamily" : "au-target",
		"personalize" : "personalize-target",
		"fontsize" : "fontsize",
		"fontspace" : "fontspace",
		"square" : "buttonsquare",
		"buttonrounded" : "buttonrounded",
		"buttonradius" : "buttonradius",
		"buttonflat" : "buttonflat",
		"buttonline" : "buttonline",
		"buttongradient" : "buttongradient",
		"personalizeoptions" : "personalizeoptions",
		"delete" : "delete"
      };

  function attachToolbarTemplate() {
    var div = document.createElement("div"),
	jscolor_code = "{valueElement:'textColorvalue'}";
	jscolor_code1 = "{valueElement:'buttonbgcolor'}";
        toolbarTemplate = '<div class="options"> \
          <span class="no-overflow"> \
            <span class="ui-inputs"> \
			  <button class="personalizeoptions"><i class="icon-personalization"></i></button> \
			  <button class="fontoptions"><i class="icon-text"></i></button> \
			  <button class="textColor jscolor '+jscolor_code+'"></button> \
			  <input id="textColorvalue" value="ff6699" class="textColor"> \
              <button class="bold"><i class="icon-bold"></i></button> \
              <button class="italic"><i class="icon-italic"></i></button> \
			  <button class="underline"><i class="icon-underline"></i></button> \
			  <button class="strikeThrough"><i class="icon-strik-ethrough"></i></button> \
			  <button class="alignoptions"><i class="icon-center-text"></i></button> \
			  <button class="listoptions"><i class="icon-dot-list"></i></button> \
			  <button class="buttonoptions"><i class="icon-setting"></i></button> \
              <button class="customurl useicons"><i class="icon-link"></i></button> \
			  <button class="delete"><i class="icon-delete"></i></button> \
              <input class="url-input" type="text" placeholder="Paste or type a link and press Enter"/> \
            </span> \
          </span> \
        </div><div class="align-options submenu-option"><div class="no-overflow"> \
            <span class="ui-inputs"> \
			  <button class="justifyLeft"><i class="icon-left-text"></i></button> \
			  <button class="justifyCenter"><i class="icon-center-text"></i></button> \
			  <button class="justifyRight"><i class="icon-right-text"></i></button> \
			  <button class="justifyFull"><i class="icon-justify-all"></i></button> \
            </span> \
          </div> \</div><div class="button-options submenu-option"><div class="no-overflow"> \
            <div class="popupbox"><span class="ui-inputs"> \
			  Roundness : <input class="buttonradius img-range" type="range" min="0" max="50" step="1"> \
			  <button class="buttonflat">Flat</button> \
			  <button class="buttonline">Line</button> \
			  <input id="buttonbgcolor" value="ff6699" class="buttonbgcolor"> \
			  <button class="buttonbgcolor jscolor '+jscolor_code1+'"></button> \
            </span></div> \
          </div> \</div><div class="list-options submenu-option"><div class="no-overflow"> \
            <span class="ui-inputs"> \
			  <button class="insertUnorderedList"><i class="icon-dot-list"></i></button> \
			  <button class="insertOrderedList"><i class="icon-numbering-list"></i></button> \
            </span> \
          </div> \</div><div class="font-options submenu-option"><div class="no-overflow"> \
            <span class="ui-inputs"> \
		<div class="range range--dark"><div class="range__title">SIZE</div><div class="range__slider"><input class="fontsize" type="range" min="3" max="60" step="1"></div></div><div class="range range--dark editor-seperator"><div class="range__title">SPACING</div><div class="range__slider"><input class="fontspace" type="range" min="1" max="3" step="0.1"></div></div> <div class="range padding-fonts range--dark editor-seperator menu_scroll mCustomScrollbar mCustomScrollbar2">   <button class="au-target" style="font-family: Alegreya, sans-serif;">Alegreya </button><button class="au-target" style="font-family: &quot;Alegreya Sans&quot;, sans-serif;">Alegreya Sans </button><button class="au-target" style="font-family: &quot;Archivo Narrow&quot;, sans-serif;">Archivo Narrow </button><button class="au-target" style="font-family: Arial, sans-serif;">Arial </button><button class="au-target" style="font-family: BioRhyme, sans-serif;">BioRhyme </button><button class="au-target" style="font-family: Chivo, sans-serif;">Chivo </button><button class="au-target" style="font-family: &quot;Cormorant Garamond&quot;, sans-serif;">Cormorant Garamond </button><button class="au-target" style="font-family: &quot;Crimson Text&quot;, sans-serif;">Crimson Text </button><button class="au-target" style="font-family: Domine, sans-serif;">Domine </button><button class="au-target" style="font-family: Eczar, sans-serif;">Eczar </button><button class="au-target" style="font-family: Georgia, sans-serif;">Georgia </button><button class="au-target" style="font-family: &quot;Fira Sans&quot;, sans-serif;">Fira Sans </button><button class="au-target" style="font-family: Handlee, sans-serif;">Handlee </button><button class="au-target" style="font-family: Inconsolata, sans-serif;">Inconsolata </button><button class="au-target" style="font-family: Lato, sans-serif;">Lato </button><button class="au-target" style="font-family: &quot;Libre Franklin&quot;, sans-serif;">Libre Franklin </button><button class="au-target" style="font-family: Lobster, sans-serif;">Lobster </button><button class="au-target" style="font-family: Lora, sans-serif;">Lora </button><button class="au-target" style="font-family: Karla, sans-serif;"> Karla </button><button class="au-target" style="font-family: Montserrat, sans-serif;">Montserrat </button><button class="au-target" style="font-family: Neuton, sans-serif;">Neuton </button><button class="au-target" style="font-family: &quot;Old Standard TT&quot;, sans-serif;">Old Standard TT </button><button class="au-target" style="font-family: &quot;Open Sans&quot;, sans-serif;">Open Sans </button><button class="au-target" style="font-family: &quot;Playfair Display&quot;, sans-serif;">Playfair Display </button><button class="au-target" style="font-family: Poppins, sans-serif;">Poppins </button><button class="au-target" style="font-family: Raleway, sans-serif;">Raleway </button><button class="au-target" style="font-family: Roboto, sans-serif;">Roboto </button><button class="au-target" style="font-family: &quot;Roboto Slab&quot;, sans-serif;">Roboto Slab </button><button class="au-target" style="font-family: Rubik, sans-serif;">Rubik </button><button class="au-target" style="font-family: &quot;Space Mono&quot;, sans-serif;">Space Mono </button><button class="au-target" style="font-family: &quot;Source Sans Pro&quot;, sans-serif;">Source Sans Pro </button><button class="au-target" style="font-family: &quot;Source Serif Pro&quot;, sans-serif;">Source Serif Pro </button><button class="au-target" style="font-family: &quot;Times New Roman&quot;, sans-serif;">Times New Roman </button><button class="au-target" style="font-family: &quot;Varela Round&quot;, sans-serif;">Varela Round </button><button class="au-target" style="font-family: &quot;Work Sans&quot;, sans-serif;">Work Sans </button></div> \
            </span> \
          </div> \</div>          <div class="personalize-options submenu-option"><div class="no-overflow"> \
            <span class="ui-inputs"> \
		<div class="range padding-fonts range--dark menu_scroll mCustomScrollbar mCustomScrollbar2">   <button class="personalize-target">{#name}</button><button class="personalize-target">{#email}</button><button class="personalize-target">{#address}</button><button class="personalize-target">{#city}</button><button class="personalize-target">{#state}</button><button class="personalize-target">{#country}</button><button class="personalize-target">{#zip}</button><button class="personalize-target">{#phone}</button><button class="personalize-target">{#custom_1}</button><button class="personalize-target">{#custom_2}</button><button class="personalize-target">{#custom_3}</button></div> \
            </span> \</div> \</div>',
        imageTooltipTemplate = document.createElement("div"),
        toolbarContainer = document.createElement("div");

    toolbarContainer.className = "g-body";
    document.body.appendChild(toolbarContainer);

    imageTooltipTemplate.innerHTML = "<div class='pos-abs file-label'>Insert image</div> \
                                        <input class='file-hidden pos-abs' type='file' id='files' name='files[]' accept='image/*' multiple/>";
    imageTooltipTemplate.className = "image-tooltip hide";

    div.className = "text-menu hide";
    div.innerHTML = toolbarTemplate;

    if (document.querySelectorAll(".text-menu").length === 0) {
      toolbarContainer.appendChild(div);
      toolbarContainer.appendChild(imageTooltipTemplate);
    }

    imageInput = document.querySelectorAll(".file-label + input")[0];
    imageTooltip = document.querySelectorAll(".image-tooltip")[0];
    textMenu = document.querySelectorAll(".text-menu")[0];
    
    optionsNode = document.querySelectorAll(".text-menu .options")[0];
    urlInput = document.querySelectorAll(".text-menu .url-input")[0];
  }

    
  function bindTextSelectionEvents() {
    var i,
        len,
        node;

    // Trigger on both mousedown and mouseup so that the click on the menu
    // feels more instantaneously active
    document.onmousedown = triggerTextSelection;
    document.onmouseup = function(event) {
      setTimeout(function() {
        triggerTextSelection(event);
      }, 1);
    };

    document.onkeydown = preprocessKeyDown;

    document.onkeyup = function(event){
      var sel = window.getSelection();

      // FF will return sel.anchorNode to be the parentNode when the triggered keyCode is 13
      if (sel.anchorNode && sel.anchorNode.nodeName !== "ARTICLE") {
        triggerNodeAnalysis(event);

        if (sel.isCollapsed) {
          triggerTextParse(event);
        }
      }
    };

    // Handle window resize events
    root.onresize = triggerTextSelection;

    urlInput.onblur = triggerUrlBlur;
    urlInput.onkeydown = triggerUrlSet;

    if (options.allowImages) {
      imageTooltip.onmousedown = triggerImageUpload;
      imageInput.onchange = uploadImage;
      document.onmousemove = triggerOverlayStyling;
    }

    for (i = 0, len = editableNodes.length; i < len; i++) {
      node = editableNodes[i];
      node.contentEditable = true;
      node.onmousedown = node.onkeyup = node.onmouseup = triggerTextSelection;
    }
  }

  function triggerOverlayStyling(event) {
    toggleImageTooltip(event, event.target);
  }

  function triggerImageUpload(event) {
    // Cache the bound that was originally clicked on before the image upload
    var childrenNodes = editNode.children,
        editBounds = editNode.getBoundingClientRect();

    imageBound = getHorizontalBounds(childrenNodes, editBounds, event);
  }

  function uploadImage(event) {
    // Only allow uploading of 1 image for now, this is the first file
    var file = this.files[0],
        reader = new FileReader(),
        figEl;

    reader.onload = (function(f) {
      return function(e) {
        figEl = document.createElement("figure");
        figEl.innerHTML = "<img src=\"" + e.target.result + "\"/>";
        editNode.insertBefore(figEl, imageBound.bottomElement);
      };
    }(file));

    reader.readAsDataURL(file);
  }

  function toggleImageTooltip(event, element) {
    var childrenNodes = editNode.children,
        editBounds = editNode.getBoundingClientRect(),
        bound = getHorizontalBounds(childrenNodes, editBounds, event);

    if (bound) {
      imageTooltip.style.left = (editBounds.left - 90 ) + "px";
      imageTooltip.style.top = (bound.top - 17) + "px";
    } else {
      imageTooltip.style.left = EDGE + "px";
      imageTooltip.style.top = EDGE + "px";
    }
  }

  function getHorizontalBounds(nodes, target, event) {
    var bounds = [],
        bound,
        i,
        len,
        preNode,
        postNode,
        bottomBound,
        topBound,
        coordY;

    // Compute top and bottom bounds for each child element
    for (i = 0, len = nodes.length - 1; i < len; i++) {
      preNode = nodes[i];
      postNode = nodes[i+1] || null;

      bottomBound = preNode.getBoundingClientRect().bottom - 5;
      topBound = postNode.getBoundingClientRect().top;

      bounds.push({
        top: topBound,
        bottom: bottomBound,
        topElement: preNode,
        bottomElement: postNode,
        index: i+1
      });
    }

    coordY = event.pageY - root.scrollY;

    // Find if there is a range to insert the image tooltip between two elements
    for (i = 0, len = bounds.length; i < len; i++) {
      bound = bounds[i];
      if (coordY < bound.top && coordY > bound.bottom) {
        return bound;
      }
    }

    return null;
  }

  function iterateTextMenuButtons(callback) {
    var textMenuButtons = document.querySelectorAll(".text-menu button"),
		textMenuInputs = document.querySelectorAll(".text-menu input"),
		textMenuselect = document.querySelectorAll(".text-menu select"),
        i,
        len,
        node,
        fnCallback = function(n) {
          callback(n);
        };

    for (i = 0, len = textMenuButtons.length; i < len; i++) {
      node = textMenuButtons[i];
      fnCallback(node);
    }
	
	 fnCallback_input = function(n) {
			  n.onchange = function(event) {
				triggerTextStyling(n);
		  };
     };
		
	for (i = 0, len = textMenuInputs.length; i < len; i++) {
      node = textMenuInputs[i];
		 fnCallback_input(node);
    }
	for (i = 0, len = textMenuselect.length; i < len; i++) {
      node = textMenuselect[i];
		 fnCallback_input(node);
    }
  }

  function bindTextStylingEvents() {
    iterateTextMenuButtons(function(node) {
      node.onmousedown = function(event) {
        triggerTextStyling(node);
      };
    });
  }


  function getFocusNode() {
    return root.getSelection().focusNode;
  }

  function reloadMenuState() {
    var className,
        focusNode = getFocusNode(),
        tagClass,
        reTag;

    iterateTextMenuButtons(function(node) {
      className = node.className;

      for (var tag in tagClassMap) {
        tagClass = tagClassMap[tag];
        reTag = new RegExp(tagClass);


        if (reTag.test(className)) {
			
          if (hasParentWithTag(focusNode, tag)) {
			  
            $(node).addClass("active");
          }else {
           $(node).removeClass("active");
          }

          break;
        }
      }
	  
    });	
  }

  function preprocessKeyDown(event) {
    var sel = window.getSelection(),
        parentParagraph = getParentWithTag(sel.anchorNode, "p"),
        p,
        isHr;

    if (event.keyCode === 13 && parentParagraph) {
      prevSibling = parentParagraph.previousSibling;
      isHr = prevSibling && prevSibling.nodeName === "HR" &&
        !parentParagraph.textContent.length;

      // Stop enters from creating another <p> after a <hr> on enter
      if (isHr) {
        event.preventDefault();
      }
    }
  }

  function triggerNodeAnalysis(event) {
    var sel = window.getSelection(),
        anchorNode,
        parentParagraph;

    if (event.keyCode === 13) {

      // Enters should replace it's parent <div> with a <p>
      if (sel.anchorNode.nodeName === "DIV") {
        toggleFormatBlock("p");
      }

      parentParagraph = getParentWithTag(sel.anchorNode, "p");

      if (parentParagraph) {
        insertHorizontalRule(parentParagraph);
      }
    }
  }

  function insertHorizontalRule(parentParagraph) {
    var prevSibling,
        prevPrevSibling,
        hr;

    prevSibling = parentParagraph.previousSibling;
    prevPrevSibling = prevSibling;

    while (prevPrevSibling) {
      if (prevPrevSibling.nodeType != Node.TEXT_NODE) {
        break;
      }

      prevPrevSibling = prevPrevSibling.previousSibling;
    }

    if (prevSibling.nodeName === "P" && !prevSibling.textContent.length && prevPrevSibling.nodeName !== "HR") {
      hr = document.createElement("hr");
      hr.contentEditable = false;
      parentParagraph.parentNode.replaceChild(hr, prevSibling);
    }
  }

  function getTextProp(el) {
    var textProp;

    if (el.nodeType === Node.TEXT_NODE) {
      textProp = "data";
    } else if (isFirefox) {
      textProp = "textContent";
    } else {
      textProp = "innerText";
    }

    return textProp;
  }

  function insertListOnSelection(sel, textProp, listType) {
    var execListCommand = listType === "ol" ? "insertOrderedList" : "insertUnorderedList",
        nodeOffset = listType === "ol" ? 3 : 2;

    document.execCommand(execListCommand);
    sel.anchorNode[textProp] = sel.anchorNode[textProp].substring(nodeOffset);

    return getParentWithTag(sel.anchorNode, listType);
  }

  function triggerTextParse(event) {
    var sel = window.getSelection(),
        textProp,
        subject,
        insertedNode,
        unwrap,
        node,
        parent,
        range;

    // FF will return sel.anchorNode to be the parentNode when the triggered keyCode is 13
    if (!sel.isCollapsed || !sel.anchorNode || sel.anchorNode.nodeName === "ARTICLE") {
      return;
    }

    textProp = getTextProp(sel.anchorNode);
    subject = sel.anchorNode[textProp];

    if (subject.match(/^[-*]\s/) && sel.anchorNode.parentNode.nodeName !== "LI") {
      insertedNode = insertListOnSelection(sel, textProp, "ul");
    }

    if (subject.match(/^1\.\s/) && sel.anchorNode.parentNode.nodeName !== "LI") {
      insertedNode = insertListOnSelection(sel, textProp, "ol");
    }

    unwrap = insertedNode &&
            ["ul", "ol"].indexOf(insertedNode.nodeName.toLocaleLowerCase()) >= 0 &&
            ["p", "div"].indexOf(insertedNode.parentNode.nodeName.toLocaleLowerCase()) >= 0;

    if (unwrap) {
      node = sel.anchorNode;
      parent = insertedNode.parentNode;
      parent.parentNode.insertBefore(insertedNode, parent);
      parent.parentNode.removeChild(parent);
      moveCursorToBeginningOfSelection(sel, node);
    }
  }

  function moveCursorToBeginningOfSelection(selection, node) {
    range = document.createRange();
    range.setStart(node, 0);
    range.setEnd(node, 0);
    selection.removeAllRanges();
    selection.addRange(range);
  }
  
  
  
  function convertHex(hex,opacity){
    hex = hex.replace('#','');
    r = parseInt(hex.substring(0,2), 16);
    g = parseInt(hex.substring(2,4), 16);
    b = parseInt(hex.substring(4,6), 16);

    result = 'rgba('+r+','+g+','+b+','+opacity/100+')';
    return result;
}
  

  function triggerTextStyling(node) {
    var className = node.className,
        sel = window.getSelection(),
        selNode = sel.anchorNode,
        tagClass,
        reTag;

    for (var tag in tagClassMap) {
      tagClass = tagClassMap[tag];
      reTag = new RegExp(tagClass);
      if (reTag.test(className)) {
		  
        switch(tag) {
		case "delete":
			$(".editblock").remove();
			break;
        case "b":
            if (selNode && !hasParentWithTag(selNode, "h1") && !hasParentWithTag(selNode, "h2")) {
              document.execCommand(tagClass, false);
            }
             break;
        case "i":
		case "u":
		case "strike":
            document.execCommand(tagClass, false);
            break;

		case "h1":
		case "h2":
		case "h3":
		case "h4":
		case "h5":
		case "h6":
            toggleFormatBlock(tag);
            break;
			
			
		case "unorderedlist":
			document.execCommand(tagClass, false);
			break;
			
		case "orderedlist":
			document.execCommand(tagClass, false);
			break;
			
		case "fontfamily":
				$(".au-target").click(function(e){
					var fname = $(this).html();
					document.execCommand("fontName", false, fname);
					$( ".au-target" ).unbind( "click" );
					//var fname = $(gettarget(e)).html();
					//document.execCommand("fontName", false, fname);
				});
			break;
		  
		case "textColor":
			if(EditingElement.nodeName == "A"){
				$(EditingElement).css({"color": "#"+$("#textColorvalue").val()});
			}else{
				document.execCommand('foreColor',false,"#"+$("#textColorvalue").val());
			}
			break;
			
		case "alignleft":
		case "aligncenter":
		case "alignright":
		case "alignjustify":
          document.execCommand(tagClass, false, null);
		  // $(EditingElement).css("text-align", tagClass.replace("justify", "").toLowerCase());
          break;

        case "a":
            toggleUrlInput();
            optionsNode.className = "options url-mode";
            console.log("hello");
			break;
		case "fontsize":
			var val = $(".fontsize").val();
			var styleQuery = "font-size: (.*?)px;";
			NewStyle = "font-size: "+val+"px;";
			var insertHTML = getSelectionHtml();
			insertHTML = getinsertHTML(insertHTML, styleQuery);
			wrap('span', insertHTML, NewStyle, styleQuery);
			break;

		case "fontspace":
			var val = $(".fontspace").val()*100;
			
			if($(getFocusNode()).closest("a").get(0)){
				$($(getFocusNode()).closest("a").get(0)).css("line-height", val+"%"); 
			}else if(getFocusNode().nodeType ==3){
				$(getFocusNode()).parent().css("line-height", val+"%"); 
			}else{
				$(getFocusNode()).css("line-height", val+"%"); 
			}	
			break;		
			
		case "buttonrounded":
			if(getFocusNode().nodeType ==3){
				var ele = getFocusNode().parentElement;
			}else{
				var ele = getFocusNode().parentElement;
				
			}	
			$(ele).css({"border-radius": "1000px"});
			break;
			
		case "buttonradius":
			if($(getFocusNode()).closest("a").get(0)){
				var ele = $(getFocusNode()).closest("a").get(0);
				
			}else if(getFocusNode().nodeType ==3){
				var ele = getFocusNode().parentElement;
			}else{
				var ele = getFocusNode();
			}	
			console.log(ele);
			//console.log(EditingElement);
			$(ele).css({"border-radius": $(".buttonradius").val()+"px"});
			break;
		
		case "buttonbgcolor":
			if($(EditingElement).css("background-color") == "transparent"){
				$(EditingElement).css({"border": "1px solid #"+$(".buttonbgcolor").val()});
				$(EditingElement).css({"background": "none"});
			}
			else{
				$(EditingElement).css({"background": "#"+$(".buttonbgcolor").val()});
				$(EditingElement).css({"border": "0"});
			}
			break;

		case "buttonflat":
			$(EditingElement).css({"background": "#"+$(".buttonbgcolor").val()});
			$(EditingElement).css({"border": "0"});
			break;
			
		case "buttonline":
			$(EditingElement).css({"border": "1px solid #"+$(".buttonbgcolor").val()});
			$(EditingElement).css({"background": "none"});
			break;
		
	
		case "personalizeoptions":
			//$(".personalize-options").toggle();
			
			$( ".personalize-target" ).unbind( "click" );
			$(".personalize-target").click(function(e){
				// console.log($(this).html());
				var persona = $(this).html();
				// document.execCommand("insertHTML", false, persona);
				pasteHtmlAtCaret(persona);
				parent.mobielPreviewUpdate(getTemplate());
			});
			break;
			}
      
		console.log("outer");
		parent.mobielPreviewUpdate(getTemplate());
	  }
    }
	// console.log("outer");
	// parent.mobielPreviewUpdate(getTemplate());
	
    //triggerTextSelection();
	//triggerUrlBlur(e);
  }
  
 var wrap_recall = 0; 
	function wrap(tagName, insertHTML, NewStyle, styleQuery){ // insert new html with tags
		if(window.getSelection().isCollapsed){
			return;
		}
		var selection;
		var elements = [];
		var ranges = [];
		var rangeCount = 0;
		var frag;
		var lastChild;
		if (window.getSelection)
		{
			selection = window.getSelection();
			if (selection.rangeCount)
			{
				var i = selection.rangeCount;
				while (i--)
				{
					ranges[i] = selection.getRangeAt(i).cloneRange();
					elements[i] = document.createElement(tagName);
					elements[i].style.cssText = NewStyle;

					if(ranges[i].startContainer.nodeName == "A"){
						
						ranges[i].selectNode(ranges[i].startContainer);
						selection.addRange(ranges[i]);
						
						var insertHTML = getSelectionHtml();
						insertHTML = getinsertHTML(insertHTML, styleQuery);
						wrap_recall++;
						wrap('span', insertHTML, NewStyle, styleQuery);
						return;
					}

					$(ranges[i].extractContents()).remove(); 
					//sel.focusNode.parentElement.appendChild(ranges[i].extractContents());
					$(elements[i]).append(decodeEntities(insertHTML));
					ranges[i].insertNode(elements[i]);
					clean(elements[i].parentNode);
					
					if(wrap_recall > 0){
						ranges[i].selectNode(elements[i].lastChild.lastChild);
						wrap_recall = 0;
					}else{
						ranges[i].selectNode(elements[i]);
					}
					EditingElement = elements[i].lastChild;
				}

				// Restore ranges
				selection.removeAllRanges();
				i = ranges.length;			
				while (i--)
				{
					selection.addRange(ranges[i]);
				}
			}
		}
	}

function pasteHtmlAtCaret(html) {
    var sel, range;
    if (window.getSelection) {
        // IE9 and non-IE
        sel = window.getSelection();
        if (sel.getRangeAt && sel.rangeCount) {
            range = sel.getRangeAt(0);
            range.deleteContents();

            // Range.createContextualFragment() would be useful here but is
            // non-standard and not supported in all browsers (IE9, for one)
            var el = document.createElement("div");
            el.innerHTML = html;
            var frag = document.createDocumentFragment(), node, lastNode;
            while ( (node = el.firstChild) ) {
                lastNode = frag.appendChild(node);
            }
            range.insertNode(frag);
            
            // Preserve the selection
            if (lastNode) {
                range = range.cloneRange();
                range.setStartAfter(lastNode);
                range.collapse(true);
                sel.removeAllRanges();
                sel.addRange(range);
            }
        }
    } else if (document.selection && document.selection.type != "Control") {
        // IE < 9
        document.selection.createRange().pasteHTML(html);
    }
}	
	function clean(node){ // used to clean all empty tags
		for(var n = 0; n < node.childNodes.length; n ++)
		{
			var child = node.childNodes[n];
			if($(child).html() == ''){
				node.removeChild(child);
				n --;
			}
			if(child.nodeType === 1)
			{
				clean(child);
			}
		}
	}  
	  

  function triggerUrlBlur(event) {
	  setTimeout(function() {	
			var customurl = urlInput.value;
			optionsNode.className = "options";
			
			if (customurl === "") {
				document.execCommand("unlink", false);
				return false;
			}
			else if (!customurl.match("^(http://|https://|mailto:)")) {
				alert("Invalid input url. Please use http:// or https://");
				return false;
			}
			document.execCommand("unlink", false);
			//ranges = selection.getRangeAt(0).cloneRange();
			selection = ( window.getSelection || document.getSelection )();
			selection.removeAllRanges();
			window.getSelection().addRange(previouslySelectedText);
			if($(getFocusNode()).closest("a").size()){
				$(getFocusNode()).closest("a").attr('href', customurl);
			}else{
				document.execCommand("createLink", false, customurl);
			}
			urlInput.value = "";
	  }, 500);
  }

  function triggerUrlSet(event) {
    if (event.keyCode === 13) {
      event.preventDefault();
      event.stopPropagation();

      urlInput.blur();
    }
  }

  function toggleFormatBlock(tag) {
    if (hasParentWithTag(getFocusNode(), tag)) {
      document.execCommand("formatBlock", false, "p");
      document.execCommand("outdent");
    } else {
      document.execCommand("formatBlock", false, tag);
    }
  }

  function toggleUrlInput() {
	setTimeout(function() {
		var customurl = $(getFocusNode()).closest("a").get(0);
		if (typeof customurl !== "undefined") {
			urlInput.value = customurl;
		}
		// else {
			// document.execCommand("createLink", false, "/");
		// }
      previouslySelectedText = window.getSelection().getRangeAt(0);
      urlInput.focus();
    }, 150);
  }

  function getParent(node, condition, returnCallback) {
    if (node === null) {
      return;
    }

    while (node.parentNode) {
      if (condition(node)) {
        return returnCallback(node);
      }

      node = node.parentNode;
    }
  }

  function getParentWithTag(node, nodeType) {
    var checkNodeType = function(node) { return node.nodeName.toLowerCase() === nodeType; },
        returnNode = function(node) { return node; };

    return getParent(node, checkNodeType, returnNode);
  }

  function hasParentWithTag(node, nodeType) {
    return !!getParentWithTag(node, nodeType);
  }

  function getParentHref(node) {
    var checkHref = function(node) { return typeof node.href !== "undefined"; },
        returnHref = function(node) { return node.href; };

    return getParent(node, checkHref, returnHref);
  }

  function triggerTextSelection(e) {
    var selectedText = root.getSelection(),
        range,
        clientRectBounds,
        target = e.target || e.srcElement;

	if($(".options").hasClass('url-mode') || $(target).closest('.colorpikerele').length){
		return;
	}
	resetEditor(e);
	
	if(target.nodeName == "IMG"){
		if($(target).closest('#ImageEditor').length > 0){
			EditingElement.src = target.src;
			parent.mobielPreviewUpdate(getTemplate());
			return;
		}
		//EditingElement = target;
		var top = $(target).offset().top + target.offsetHeight + 5 ;
		var left = $(target).offset().left + (target.offsetWidth / 2) - ($(".img-menu").width()/2);
		setImageMenuPosition(top, left);
	}
	else{
			if($(target).closest('#ImageEditor').length == 0){
				setImageMenuPosition(EDGE, EDGE);
			}
			if(target.nodeName == "A"){
				selection = ( window.getSelection || document.getSelection )();
				
				//ranges = selection.getRangeAt(0).cloneRange();
				// ranges = document.createRange();
				//console.log(target);
				// ranges.selectNode(target.firstChild);
				//selection.removeAllRanges();
				// selection.addRange(ranges);
			}
		// The selected text is not editable
		if(!target.isContentEditable){
			if($(target).closest('.g-body').length == 0){
				setTextMenuPosition(EDGE, EDGE);
			}
			reloadMenuState();
			return;
		}
		
		//console.log(selectedText);
		// The selected text is collapsed, push the menu out of the way
		if(selectedText.isCollapsed){
			//setTextMenuPosition(EDGE, EDGE);
			//textMenu.className = "text-menu hide";
				//ranges = document.createRange ();
				clientRectBounds = $(target).offset();
				reloadMenuState();
				setTextMenuPosition(clientRectBounds.top + $(target).height()-45,clientRectBounds.left-50 +  ($(target).width()/ 2));
		}
		else{
				range = selectedText.getRangeAt(0);
				clientRectBounds = range.getBoundingClientRect();
				setTextMenuPosition(clientRectBounds.bottom-30,(clientRectBounds.left + clientRectBounds.right) / 2);
			}
		}
  }

  function setTextMenuPosition(top, left) {
	  $('.submenu-option').hide();
    textMenu.style.top = top + "px";
    textMenu.style.left = left + "px";

    if (options.animate) {
      if (top === EDGE) {
        textMenu.className = "text-menu hide";
      }
	  else {
		if($(getFocusNode()).closest("a").get(0)){
			$('.buttonoptions').show();
			$('.alignoptions').hide();
			$('.listoptions').hide();
			$('.personalizeoptions').hide();
		}
		else{
			$('.buttonoptions').hide();
			$('.alignoptions').show();
			$('.listoptions').show();
			$('.personalizeoptions').show();
		}
		textMenu.className = "text-menu active";
		setImageMenuPosition(EDGE, EDGE);
      }
    }
  }
  
  function setImageMenuPosition(top, left) {
	  imgMenu = document.querySelectorAll(".img-menu")[0];
		if (top === EDGE) {
			$(imgMenu).hide();
		} else {
			$(imgMenu).show();
			setTextMenuPosition(EDGE, EDGE);
			textMenu.className = "text-menu hide";
			
			window.getSelection().removeAllRanges();
		}
	imgMenu.style.top = top + "px";
	imgMenu.style.left = left + "px";
  }
  

	function getSelectionHtml() { // used to get selected text in html formate.
		var html = "";
		if (typeof window.getSelection != "undefined") {
			var sel = window.getSelection();
			if (sel.rangeCount) {
				var container = document.createElement("div");
				for (var i = 0, len = sel.rangeCount; i < len; ++i) {
					container.appendChild(sel.getRangeAt(i).cloneContents());
				}
				html = container.innerHTML;
			}
		} else if (typeof document.selection != "undefined") {
			if (document.selection.type == "Text") {
				html = document.selection.createRange().htmlText;
			}
		}
		
		html = html.replace(/&nbsp;/g, " ");
		return html;
	}
	
	function getinsertHTML(insertHTML, styleQuery) { //used to get inner html after remove same type feature 
		//console.log(insertHTML);
			var regex = new RegExp(styleQuery, "g");
			insertHTML = insertHTML.replace(regex, "");
			insertHTML_arr = insertHTML.match(/<span style="">(.*?)<\/span>/g);
			if(insertHTML_arr){
				$.each(insertHTML_arr, function( index, value ) {
					value_inner = value.replace('<span style="">', "").replace('</span>', "");
					insertHTML = insertHTML.replace(value, value_inner);
				});
			}
			
			//console.log(insertHTML);
			return insertHTML;
	}
	function decodeEntities(encodedString) {
		var div = document.createElement('div');
		div.innerHTML = encodedString;
		return div.innerHTML;
	}
	function saveSelection(containerEl) {
		var charIndex = 0, start = 0, end = 0, foundStart = false, stop = {};
		var sel = window.getSelection(), range;

		function traverseTextNodes(node, range) {
			if (node.nodeType == 3) {
				if (!foundStart && node == range.startContainer) {
					start = charIndex + range.startOffset;
					foundStart = true;
				}
				if (foundStart && node == range.endContainer) {
					end = charIndex + range.endOffset;
					throw stop;
				}
				charIndex += node.length;
			} else {
				for (var i = 0, len = node.childNodes.length; i < len; ++i) {
					traverseTextNodes(node.childNodes[i], range);
				}
			}
		}

		if (sel.rangeCount) {
			try {
				traverseTextNodes(containerEl, sel.getRangeAt(0));
			} catch (ex) {
				if (ex != stop) {
					throw ex;
				}
			}
		}

		return {
			start: start,
			end: end
		};
	}

	function restoreSelection(containerEl, savedSel) {
		//console.log(containerEl);
    var charIndex = 0, range = document.createRange(), foundStart = false, stop = {};
    range.setStart(containerEl, 0);
    range.collapse(true);

    function traverseTextNodes(node) {
        if (node.nodeType == 3) {
            var nextCharIndex = charIndex + node.length;
            if (!foundStart && savedSel.start >= charIndex && savedSel.start <= nextCharIndex) {
                range.setStart(node, savedSel.start - charIndex);
                foundStart = true;
            }
            if (foundStart && savedSel.end >= charIndex && savedSel.end <= nextCharIndex) {
                range.setEnd(node, savedSel.end - charIndex);
                throw stop;
            }
            charIndex = nextCharIndex;
        } else {
            for (var i = 0, len = node.childNodes.length; i < len; ++i) {
                traverseTextNodes(node.childNodes[i]);
            }
        }
    }

    try {
        traverseTextNodes(containerEl);
    } catch (ex) {
        if (ex == stop) {
            var sel = window.getSelection();
            sel.removeAllRanges();
			
            sel.addRange(range);
        } else {
            throw ex;
        }
    }
}
	
	function hexcolor_hold(colorval) { // convert color code from rgb to hex
		var parts = colorval.match(/^rgb\((\d+),\s*(\d+),\s*(\d+)\)$/);
		delete(parts[0]);
		for (var i = 1; i <= 3; ++i) {
			parts[i] = parseInt(parts[i]).toString(16);
			if (parts[i].length == 1) parts[i] = '0' + parts[i];
		}
		return color = '#' + parts.join('');
	}
   
   function resetEditor(e){
	  target = e.target || e.srcElement;
	   if($(target).closest('.g-body').length > 0){
				return;
		}
		
		if($(target).closest('.edit_template').length > 0){
			$(".editblock").removeClass("editblock");
			$(target).parent().addClass("editblock");
		}
		if(target.nodeName == "IMG"){
				$(".editblock").removeClass("editblock");
		}
		//$(target).attr('contenteditable','true');
   }

  root.lpb_inline_editor = lpb_inline_editor;
  
	$(document).on("keyup",function() {
		parent.mobielPreviewUpdate(getTemplate());
	}); 

}).call(this);


$(function(){
	$(document).on("click", ".personalizeoptions, .fontoptions, .alignoptions, .listoptions, .styleoptions, .mediaoptions", function(e){
		if(parseInt($("body").height()) - parseInt($(".text-menu").css("top")) < 300){
			$(".submenu-option").css({"bottom": "40px"});
		}else{
			$(".submenu-option").css({"bottom": "inherit"});
		}
	});
	
});

function hexcolor(colorval) { // convert color code from rgb to hex
	var parts = colorval.match(/^rgb\((\d+),\s*(\d+),\s*(\d+)\)$/);
	delete(parts[0]);
	for (var i = 1; i <= 3; ++i) {
		parts[i] = parseInt(parts[i]).toString(16);
		if (parts[i].length == 1) parts[i] = '0' + parts[i];
	}
	return color = '#' + parts.join('');
}