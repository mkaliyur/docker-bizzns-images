# CodeIgniter 2
Open Source PHP Framework (originally from EllisLab)

For more info, please refer to the user-guide at http://www.codeigniter.com/userguide2/  
(also available within the download package for offline use)

**WARNING:** *CodeIgniter 2.x is no longer under development and only receives security patches until October 31st, 2015.
Please update your installation to the latest CodeIgniter 3.x version available
(upgrade instructions [here](http://www.codeigniter.com/userguide3/installation/upgrade_300.html)).*


Aditional Task for all upcomming launches 

* Set App Controller to manage installation and licencing saperatly - Time 1 Days [Done]
* Add Email Template Manager to avoid sales team interaction with developers. We are providing a saperate manager to manage and update all email content. Time 1 Day
* Add Training Videos Manager to avoid sales team interaction with developers. now sales team can add and update training video without interect with developer.
* Package managed by user_packages table. avoiding use of xml and starting authintication with db. [Process]
* User Licencing : now we are using licencing authentication with db. Main banifite is Our customer can use any different emails for their product on their domains. [Done]
* Permission according to packages managed by BMS not included into customer package now.
 


 
 