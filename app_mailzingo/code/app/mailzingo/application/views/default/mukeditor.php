<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padding15 footer-height">
  <!-- dash header-->
  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 mb2 xsmb4">
    <div class="dash-header">
      <div class="row">
        <div class="col-md-12 col-sm-12">
          <h1 class="smart-color em22 smem18 xsem16">Newsletter</h1>
          <p class="em11 smem10 xsem10">Here You Can Create Regular Bulk Mails</p>
        </div>
      </div>
    </div>
  </div>
  <!-- dash header end-->
  <div class="clearfix"></div>
  <form action="" method="post">
    <div class="col-md-12 col-sm-12 col-xs-12 mb2 mt1 xsmt1 xsmb4">
      <div class="layout-box1 clearfix">
        <div class="bs-wizard">
          <div class="col-xs-3 bs-wizard-step complete">
            <div class="progress">
              <div class="progress-bar"></div>
            </div>
            <a href="<?php echo site_url('add-newsletter?step=1')?>" class="bs-wizard-dot"></a>
            <div class="text-center em8 smem9 xsem8 hidden-xs">setting</div>
          </div>
          <div class="col-xs-3 bs-wizard-step complete">
            <div class="progress">
              <div class="progress-bar"></div>
            </div>
            <a href="<?php echo site_url('add-newsletter?step=2')?>" class="bs-wizard-dot"></a>
            <div class="text-center em8 smem9 xsem8 hidden-xs">Templates</div>
          </div>
          <div class="col-xs-3 bs-wizard-step complete active">
            <div class="progress">
              <div class="progress-bar"></div>
            </div>
            <a href="<?php if($newsletter_session['step3']=='yes') { echo site_url('add-newsletter?step=3'); } else { echo 'javascript:'; } ?>" class="bs-wizard-dot"></a>
            <div class="text-center em8 smem9 xsem8 hidden-xs">Editor</div>
          </div>
          <div class="col-xs-3 bs-wizard-step  <?php if($newsletter_session['step4']!='yes') { ?> disabled <?php } ?>">
            <div class="progress">
              <div class="progress-bar"></div>
            </div>
            <a href="<?php if($newsletter_session['step4']=='yes') { echo site_url('add-newsletter?step=4'); } else { echo 'javascript:'; } ?>" class="bs-wizard-dot"></a>
            <div class="text-center em8 smem9 xsem8 hidden-xs">Subscriber</div>
          </div>
          <div class="col-xs-3 bs-wizard-step  <?php if($newsletter_session['step5']!='yes') { ?> disabled <?php } ?>">
            <div class="progress">
              <div class="progress-bar"></div>
            </div>
            <a href="<?php if($newsletter_session['step5']=='yes') { echo site_url('add-newsletter?step=5'); } else { echo 'javascript:'; } ?>" class="bs-wizard-dot"></a>
            <div class="text-center em8 smem9 xsem8 hidden-xs">Send Option</div>
          </div>
        </div>
        <div class="box-padding xsmt15 mt4">
          <div class="col-xs-6 col-lg-2 col-md-3 col-sm-3 xsmb4 mb0 padding0 campaign-links">
            <ul>
              <li><a data-toggle="modal" data-target="#msgattach"> <i class="fa fa-paperclip fa-rotate-270 em14 smem14 xsem14" aria-hidden="true"></i></a> </li>
              <li class="mt4 xsmt6 em9 smem9 xsem9"><span class="attatchCount">
                <?php echo ($newsletter_session['attatchment']?sizeof($newsletter_session['attatchment']):0)?>
                </span> Attachment</li>
            </ul>
          </div>
          <div class="col-xs-6 col-lg-2 xsmb4 col-md-3 text-right col-sm-4 padding0 visible-xs"> <a data-toggle="modal" data-target="#testMail" class="yes-btn">Test Mail</a> </div>
          <div class="col-xs-12 col-lg-2 col-md-3 mb0 xsmb4 col-sm-4 padding0 smart-field">
            <select name="personalization" class="personal">
              <option value="">Personalization</option>
              <option value="name">Name</option>
              <option value="email">Email</option>
              <option value="address">Address</option>
              <option value="city">City</option>
              <option value="state">State</option>
              <option value="country">Country</option>
              <option value="zip">Zip</option>
              <option value="phone">Phone</option>
            </select>
          </div>
        </div>
        <div class="col-xs-12 padding0 mt1 line-bottom"></div>
        <div class="clearfix"></div>
        <div class="col-xs-12 box-padding xsmb2 mb1 xsmt2">
          <div class="row">
            <!----------------------------------Editor--------------------------------------------->
            <div class="edit_template" style="position:relative;">
              <iframe id="templateeditor" src="http://localhost/smartmailer/inline-editor/<?php echo  $_GET['id']; ?>" style="width: 100%;"></iframe>
            </div>
            <script>
function getTemplate(){
	var temptale = document.getElementById('templateeditor').contentWindow.getTemplate();
	console.log(temptale);
}
$('#templateeditor').load(function() {
    setTimeout(iResize, 50);
    // Safari and Opera need a kick-start.
    var iSource = document.getElementById('your-iframe-id').src;
    document.getElementById('your-iframe-id').src = '';
    document.getElementById('your-iframe-id').src = iSource;
});
function iResize() {
    document.getElementById('templateeditor').style.height = 
    document.getElementById('templateeditor').contentWindow.document.body.offsetHeight + 'px';
}
</script>
            <!----------------------------------Editor--------------------------------------------->
          </div>
        </div>
      </div>
      <div class="col-xs-6 col-md-5 col-sm-6 mt2 xsmt8 padding0 hidden-xs"> <a data-toggle="modal" data-target="#testMail" class="blank-btn" title="Back">Test Mail </a> </div>
      <div class="col-xs-12 col-md-7 col-sm-6 xstext-right xstext-center mt2 xsmt8 padding0"> <a href="<?php echo site_url('add-newsletter?step=2')?>" class="activity-btn" title="Back">Back </a> &nbsp; <a href="javascript:"  class="activity-btn saveTemplate" title="Save Template" onclick="getTemplate();">Save Template </a> &nbsp;
        <button type="submit" class="smart-btn" name="step3"  value="step3">Next </button>
          </div>
    </div>
  </form>
</div>
<!-- message attachment modalpopup-->
<div id="msgattach" class="modal fade" role="dialog">
  <div class="modal-dialog campaign-modal">
    <div class="modal-content">
      <div class="modal-body padding0 clearfix xsmb4 xsmt0 mt4 mb2">
        <div class="col-md-5 col-sm-5 col-xs-12 mt7 xsmt4 mb4">
          <div class="col-xs-12 bottom-border"> <img src="<?php echo $this->config->item('templateAssetsPath')?>images/import-pc.png" class="img-responsive center-block mobile-img" /></div>
        </div>
        <div class="col-md-7 col-sm-7 col-xs-12 left-border campaign-field xsmt4 mt0 ">
          <div class="col-xs-12">
            <h3 class="em16 smem16 xsem12">Attachment</h3>
            <form action="<?php echo site_url('add-temp-attatchment')?>" enctype="multipart/form-data" class="attatchForm" method="post">
              <label class="custom-file-upload mt6 mb4">
              <input type="file" name="attatchment" class="attatchment"/>
              <span class="w300">Add Attachment </span></label>
              <div class="aWait_div" style="display:none"><img src="<?php echo $this->config->item("templateAssetsPath")?>images/loader.gif" class="center-block" /></div>
            </form>
            <div class="attachedDiv">
              <?php $attatchment = $newsletter_session['attatchment']; 
			 foreach($attatchment as $valattach){ ?>
              <div>
                <div class="col-md-10 col-sm-10 col-xs-10 mt1 xsmt0 mb1 xsmb1 text-left padding0 em9 smem9 xsem9 mt1 xsmt0">
                  <?php echo $valattach['name'].' ('.$valattach['size'].' kb)'?>
                </div>
                <div class="col-md-2 col-sm-2 col-xs-2 mt2 xsmt2 text-right mb1 xsmb1"> <a href="javascript:" class="smart-grey w300 deleteAttatch" data-name="<?php echo $valattach['name']?>"><i class="fa fa-times" aria-hidden="true"></i></a></div>
              </div>
              <?php } ?>
            </div>
            <div class="clearfix"></div>
            <div class="text-right mt14 xsmt6 xstext-right xstext-center"> <a href="#" class="blank-btn" data-dismiss="modal">Close</a> </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<!-- Test Mail modalpopup-->
<div id="testMail" class="modal fade" role="dialog">
  <div class="modal-dialog campaign-modal">
    <div class="modal-content">
      <div class="modal-body padding0 clearfix xsmb4 xsmt4 ">
        <div class="col-md-5 col-sm-5 col-xs-12 mt1 xsmt2">
          <div class="col-xs-12 bottom-border"> <img src="<?php echo $this->config->item('templateAssetsPath')?>images/testmail.png" class="img-responsive center-block mobile-img" /></div>
        </div>
        <div class="col-md-7 col-sm-7 col-xs-12 left-border campaign-field xsmt4 mt0 ">
          <div class="col-xs-12 mb2">
            <h3 class="em16 smem16 xsem14">Sent Test Mail</h3>
            <div class="form-group mt12 xsmt12">
              <label class="w300 sr-only">Campaign Name</label>
              <input type="text" name="testemail" placeholder="abc@sitename.com" required class="form-control testemail" />
            </div>
            <p class="em9 smem9 xsem9"><span class="smart-red">Note : </span> Test Your Message
              By Delivering Them To Your Email Address.</p>
            <div class="text-right mt12 xsmt12 xsmb4 xstext-right xstext-center"><a href="#" class="blank-btn" data-dismiss="modal">Cancel</a> <a href="javascript:" class="smart-btn sendTestmail">Send</a></div>
            <div class="testWait_div" style="display:none"><img src="<?php echo $this->config->item("templateAssetsPath")?>images/loader.gif" class="center-block" /></div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<script>
/*-------------------------- personalize starts here ---------------------------------------------------*/
$('body').delegate(".personal", 'change', function() {
    
	var perVal = $(this).val();
	CKEDITOR.instances.editor1.insertText('{#'+perVal+'}');
});
/*-------------------------- personalize ends here ---------------------------------------------------*/

/*-------------------------- Attatch file starts here ---------------------------------------------------*/
$('.attatchment').on('change', function() 
{ 
  $('.attatchForm').submit();
});
$(document).ready(function() {
    $(".attatchForm").submit(function(event) {
        var posturl = $(this).attr('action');
		var valueOrg;
        $(this).ajaxSubmit({
            url: posturl,
            dataType: 'json',
			beforeSend: function() { $('.aWait_div').fadeIn(); },
            success: function(response) {
             if (response.success) {
			    $('.aWait_div').fadeOut();
				$('.attachedDiv').html('');
				$.each(response.attatchment, function (index, value) {
					$('.attachedDiv').append('<div><div class="col-md-10 col-sm-10 col-xs-10 mt1 xsmt0 mb1 xsmb1 text-left padding0 em9 smem9 xsem9 mt1 xsmt0">'+value.name+' ('+value.size+' kb)</div><div class="col-md-2 col-sm-2 col-xs-2 mt2 xsmt2 text-right mb1 xsmb1"> <a href="javascript:" class="smart-grey w300 deleteAttatch" data-name="'+value.name+'"><i class="fa fa-times" aria-hidden="true"></i></a></div></div>');
			     });
				 $('.attatchCount').html(response.attatchCount);
			} 
            },
        });
        return false;
    
});
});
/*-------------------------- Attatch file ends here ---------------------------------------------------*/

/*-------------------------- Delete Attatch file starts here ---------------------------------------------------*/
$('body').delegate(".deleteAttatch", 'click', function() {
        var posturl = site_url+'delete-attatchment';
		var name = $(this).attr('data-name');
		var $this = $(this);
        $.ajax({
        url: posturl,
        dataType: 'json',
        type: "POST",
        data: {name: name},
        success: function(data) {
            if (data.success) {
			    $this.parent().parent().remove();
				$('.attatchCount').html(data.attatchCount);
            }
        },
      });
    
});
/*-------------------------- Delete Attatch file starts here ---------------------------------------------------*/

/*-------------------------- Save Template starts her ---------------------------------------------------*/
$('body').delegate(".saveTemplate", 'click', function() {
        var posturl = site_url+'save-template';
		var template_text = CKEDITOR.instances.editor1.getData();

        $.ajax({
        url: posturl,
        dataType: 'json',
        type: "POST",
        data: {template_text: template_text},
        success: function(data) {
            if (data.success) {
			    PNotify.removeAll();
				new PNotify({title: 'Success',text: 'Template Saved Successfully',type: 'success'});
            }
        },
      });
    
});
/*-------------------------- Save Template starts here ---------------------------------------------------*/

/*-------------------------- Send test mail starts here ---------------------------------------------------*/
$('body').delegate(".sendTestmail", 'click', function() {
        var posturl = site_url+'send-test-mail';
		var template_text = CKEDITOR.instances.editor1.getData();
        var testemail = $('.testemail').val();
		
        $.ajax({
        url: posturl,
        dataType: 'json',
        type: "POST",
        data: {template_text: template_text,testemail: testemail},
		beforeSend: function() { 
		  $('.sendTestmail').fadeOut(); 
		  $('.testWait_div').fadeIn(); 
		},
        success: function(data) {
		$('.testWait_div').fadeOut();
		$('.sendTestmail').fadeIn(); 
            if (data.error) {
			    PNotify.removeAll();
			    new PNotify({title: 'Error',text: data.error,type: 'error'});
            } else {
			    $('#testMail').modal('toggle');
				PNotify.removeAll();
				new PNotify({title: 'Success',text: 'Mail Sent Successfully',type: 'success'});
			}
			
        },
      });
    
});
/*-------------------------- Send test mail ends here ---------------------------------------------------*/

/*-------------------------- error message starts here---------------------------------------------------*/
<?php if(validation_errors()!='' || $error!='') { ?>
new PNotify({
	  title: 'Error',
	  text: <?php echo json_encode(validation_errors())?>,
	  type: 'error'
	});
<?php } ?>
/*-------------------------- error message ends here---------------------------------------------------*/

</script>
