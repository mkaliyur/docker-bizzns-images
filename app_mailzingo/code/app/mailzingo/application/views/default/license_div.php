<!-------------- license key div start ----------------------->
<div class="license-key-div">
  <!-- dash header-->
  <form action="<?php echo $this->config->item('membersarea_url')?>check-license-key" method="post" class="licenseKeyForm">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 license-key-inside-div">
      <div class="row">
        <div class="col-md-12 text-center smart-field">
          <h4 class="smart-color">Enter Your License Key</h4>
          <div class="col-md-5 col-sm-10 col-xs-12 center-div1">
            <input name="license_key" placeholder="Enter Your License Key" class="mt1" type="text">
          </div>
          <input name="site_url" class="mt1" type="hidden" value="<?php echo site_url()?>">
          <button type="submit" name="license_submit" value="email_submit" class="smart-btn mt1 xsmt3 license_submit" title="Verify Email">Submit Your License Key</button>
        </div>
      </div>
    </div>
  </form>
  <!-- dash header end-->
  <div class="clearfix"></div>
</div>
<!-------------- license key div end ----------------------->