<!--- arrow script-->
<script type="text/javascript">
function arrowicon(id){
	console.log($(id).attr("aria-expanded"));
	
	$('.arrowclose').find('li').removeClass("arrowexp");
	
	if($(id).attr("aria-expanded") != 'true'){
		$(id).closest("li").addClass("arrowexp");
	}else{
		$(id).closest("li").removeClass("arrowexp");
	}
}
</script>
<div class="side-menu sidebar-inverse">
<nav class="navbar navbar-default" role="navigation">
<div class="side-menu-container menu_scroll mCustomScrollbar mCustomScrollbar2">
       
            <ul class="nav navbar-nav arrowclose">
             <li><a href="<?php echo site_url('dashboard')?>"><span class="icon icon-Dashboard"></span><span class="title">Dashboard</span></a></li>
             <li><a href="<?php echo site_url('campaign-list')?>"><span class="icon icon-Campaigns"></span><span class="title">Campaigns</span></a></li>
            <li class="panel panel-default dropdown">
            <a data-toggle="collapse" href="#dropdown-project" onclick="arrowicon(this);">
            <span class="icon icon-Subscribers"></span><span class="title">Subscribers</span></a>
             <!-- Dropdown level 1 -->
                 <div id="dropdown-project" class="panel-collapse collapse">
                 <div class="panel-body">
                 <ul class="nav navbar-nav">
                 <li><a href="<?php echo site_url('add-contact')?>">Add Subscriber</a></li>
                 <li><a href="<?php echo site_url('contact-list')?>">Management</a></li>
                 <li><a href="<?php echo site_url('suppression-list')?>">Suppression List</a></li>
                 </ul>
                 </div>
                 </div>
            </li>
            <li class="panel panel-default dropdown">
            <a data-toggle="collapse" href="#dropdown-Stats" onclick="arrowicon(this);">
            <span class="icon icon-Message"></span><span class="title">Message</span> </a>
              <!-- Dropdown level 1 -->
              <div id="dropdown-Stats" class="panel-collapse collapse">
                <div class="panel-body">
                  <ul class="nav navbar-nav">
                    <li><a href="<?php echo site_url('newsletter-list/sent')?>">Newsletter</a></li>
                    <li><a href="<?php echo site_url('autoresponder-list')?>">Autoresponder</a></li>
                  </ul>
                </div>
              </div>
            </li>
            <!-- Dropdown-->
            <li class="panel panel-default dropdown">
            <a data-toggle="collapse" href="#component-Viral" onclick="arrowicon(this);">
            <span class="icon icon-Statistics"></span><span class="title">Statistics</span></a>
              <!-- Dropdown level 1 -->
              <div id="component-Viral" class="panel-collapse collapse">
                <div class="panel-body">
                  <ul class="nav navbar-nav">
                    <li><a href="<?php echo site_url('newsletter-stats')?>">Newsletter</a></li>
                    <li><a href="<?php echo site_url('autoresponder-stats')?>">Autoresponder</a></li>
                  </ul>
                </div>
              </div>
            </li>
            <!-- Dropdown-->
            <li class="panel panel-default dropdown">
            <a data-toggle="collapse" href="#dropdown-example" onclick="arrowicon(this);">
            <span class="icon icon-Templates"></span><span class="title">Templates</span></a>
              <!-- Dropdown level 1 -->
              <div id="dropdown-example" class="panel-collapse collapse">
                <div class="panel-body">
                  <ul class="nav navbar-nav">
                     <li><a href="<?php echo site_url('add-template')?>">Create</a></li>
                    <li><a href="<?php echo site_url('template-list')?>">Manage</a></li>
                  </ul>
                </div>
              </div>
            </li>
            
            <li><a href="<?php echo site_url('forms')?>"><span class="icon icon-Forms"></span><span class="title">Forms</span></a></li>
            <li><a href="<?php echo site_url('settings/user')?>"><span class="icon icon-Settings"></span><span class="title">Settings</span></a></li>
			<li class="panel panel-default dropdown">
            <a data-toggle="collapse" href="#dropdown-help" onclick="arrowicon(this);">
            <span class="icon icon-Help"></span><span class="title">Help</span></a>
              <!-- Dropdown level 1 -->
              <div id="dropdown-help" class="panel-collapse collapse">
                <div class="panel-body">
                  <ul class="nav navbar-nav">
                     <li><a href="<?php echo site_url('training')?>">Training</a></li>
                    <li><a href="<?php echo site_url('faq')?>">Faq</a></li>
                  </ul>
                </div>
              </div>
            </li>
            
			 
			
            <!-- Dropdown-->
          </ul>
        </div>
        <!--<div class="visible-xs text-cente menu_logo_fix" align="center"> <a href="#"> <img src="images/saglus_menu.png" class="img-responsive sag_logo_menu"></a> <a href="#"><img src="" class="img-responsive edu_logo_menu"></a> </div> -->
        <!-- /.navbar-collapse -->
      </nav>
    </div>