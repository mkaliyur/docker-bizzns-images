<script>
    $(document).ready(function(){
        if (Modernizr.touch) {
            // show the close overlay button
            $(".close-overlay").removeClass("hidden");
            // handle the adding of hover class when clicked
            $(".img").click(function(e){
                if (!$(this).hasClass("hover")) {
                    $(this).addClass("hover");
                }
            });
            // handle the closing of the overlay
            $(".close-overlay").click(function(e){
                e.preventDefault();
                e.stopPropagation();
                if ($(this).closest(".img").hasClass("hover")) {
                    $(this).closest(".img").removeClass("hover");
                }
            });
        } else {
            // handle the mouseenter functionality
            $(".img").mouseenter(function(){
                $(this).addClass("hover");
            })
            // handle the mouseleave functionality
            .mouseleave(function(){
                $(this).removeClass("hover");
            });
        }
    });
</script>

<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padding15 footer-height"> 
  <!-- dash header-->
  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 mb2 xsmb4">
    <div class="dash-header">
      <h1 class="smart-color em22 smem18 xsem16">Frequent Asked Questions</h1>
      <p class="em11 smem10 xsem10">We are offering following Faqs with MailZingo</p>
    </div>
  </div>
  <!-- dash header end-->
  <div class="clearfix"></div>
  <div class="col-md-10 col-md-offset-1">
    <form action="" method="post">
      <div class="input-group add-on">
        <input class="form-control" placeholder="Search Here" name="search" id="srch-term" value="<?php echo $search;?>" type="text">
        <div class="input-group-btn">
          <button class="btn btn-default member-search-btn" type="submit"><i class="fa fa-search"></i></button>
        </div>
      </div>
      <!--<div class="col-md-10 col-sm-10 col-xs-8"><input type="text" class="form-control" name="search" placeholder="Search here"> </div>
	<div class="col-md-2 col-sm-2 col-xs-4"><button class="btn btn-search">SEARCH</div>   -->
    </form>
    <br>
    <br>
    <br>
  </div>
  <div class="clearfix"></div>
  <div class="col-md-12 mb2 xsmb2">
    <div class="row">
      <div class="col-md-12">
        <div class="col-md-2 col-sm-3 col-xs-12 padding0">
          <ul class="nav nav-tabs em12 mdem11 smem10 xsem12 nav-tabs2">
            <?php
		
		if($allCat)
		{
			if($_POST)
			{
				?>
            <li  class="active"><a href="javascript:" class="xstextcenter"><i class="icon icon-requirements"></i>Search Results</a></li>
            <?php
			}
			foreach($allCat as $cat)
			{
				 
				?>
            <li  class="<?php if($active_slug == $cat->slug && empty($_POST)){ echo 'active';}?>"><a href="<?php echo site_url().'faq/'.$cat->slug;?>" class="xstextcenter"><i class="icon icon-requirements"></i><?php echo $cat->title;?></a></li>
            <?php
			}
		}
	
	?>
          </ul>
        </div>
        
		<div class="col-md-10 col-sm-9 col-xs-12 faq-area gallery"  >
		<div class="col-xs-12 effects clearfix" id="effect-2">	
		

		<?php
				if($fetch_data)
				{
				 ?>
				 <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
				 <?php
					foreach($fetch_data as $key=>$faq)
						{
							?>
							<div class="panel panel-default faq-border-radius">
							<div class="panel-heading" role="tab" id="headingOne">
							<h4 class="panel-title">
							<a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne<?php echo $key;?>" aria-expanded="true" aria-controls="collapseOne">
							<!-- <i class="more-less glyphicon glyphicon-plus"></i>-->
							<?php echo $key+1;?>. <?php echo $faq->question;?>
							</a>
							</h4>
							</div>
							<div id="collapseOne<?php echo $key;?>" class="panel-collapse collapse <?php if($key==0){ echo "in";}?>" role="tabpanel" aria-labelledby="headingOne">
							<div class="panel-body faq-tabs-content">
							<?php echo $faq->answers;?>
							<p>
								<?php
									if($faq->attachments && !empty($faq->attachments)){
										$i =0; 
										$attchment = unserialize($faq->attachments);
										foreach($attchment as $attach){
										if(++$i==1){ echo 'Attachments:-';}
										if (strpos($attach, '.pdf') !== false) {
											$img_url = 'no_image.jpg';
											$img_url = $this->config->item('membersarea_url').'assets/training/document/'.$img_url;
											$target = '_blank';
										}
										else
										{
											$img_url = $attach;
											$img_url = $this->config->item('membersarea_url').'assets/uploads/faq_attachments/'.$img_url;
											$target = '';
										}
										
										?>
											<div class="col-xs-6 col-md-3 col-sm-3 col-lg-2 mb2 xsmb4 my-class looptr">
											  <div class="img tem-hover" id="element1"> <img src="<?php echo $img_url;?>" class="img-responsive template-img center-block" title="Beautiful Image" style="min-height:180px; height:180px;" />
												<div class="overlay">
													<div class="expand"><a href="<?php echo $this->config->item('membersarea_url').'assets/uploads/faq_attachments/'.$attach;?>" target="<?php echo $target;?>"><i class="fa fa-search-plus" aria-hidden="true" ></i></a> </div>
												</div>
											  </div>
											  </div> 
										<?php
										}
									}
								?>
								</p>
								<div class="clearfix"></div>
							</div>
							</div>
							</div>
							
					  <?php
						}
					?>
				</div>
				<?php					
				}
				else
				{
					echo '<div class="alert alert-record alert-dismissible" role="alert">Record Not Found.</div>';
				}
				?>
                
                
 
        </div>
		</div>
		
	 </div>
    </div>
  </div>
  
   <div class="clearfix"></div>
  
  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 mb2 xsmb4">
    <div class="layout-box2">
      <p class="em11 smem10 xsem10 mb0 xsmb0">Still you have any query you can contact our help desk  <a href="http://bizomart.kayako.com/" class="w600 smart-color" target="_blank"><u>bizomart.kayako.com </u></a></p>
    </div>
  </div>
  
</div>

<!------------ faq tabs script------------>
<script type="text/javascript">
	function toggleIcon(e) {
    $(e.target)
        .prev('.panel-heading')
        .find(".more-less")
        .toggleClass('glyphicon-plus glyphicon-minus');
}
$('.panel-group').on('hidden.bs.collapse', toggleIcon);
$('.panel-group').on('shown.bs.collapse', toggleIcon);
</script>
<!------------ faq tabs script------------>

