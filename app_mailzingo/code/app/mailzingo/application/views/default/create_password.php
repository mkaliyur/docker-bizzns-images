<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title><?php echo $this->config->item('site_title')?></title>

<!-- Tell the browser to be responsive to screen width -->
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
<!-- fonts-->
<link rel="icon" href="<?php echo $this->config->item('templateAssetsPath')?>images/favicon-icon.png" type="image/png"/>
<link rel="stylesheet" href="<?php echo $this->config->item('templateAssetsPath')?>css/font-awesome.min.css" type="text/css" />
<link href='https://fonts.googleapis.com/css?family=Roboto:300,400,600,800' rel='stylesheet' type='text/css'>
<link href='https://fonts.googleapis.com/css?family=Raleway:400,600,800' rel='stylesheet' type='text/css'>
<link rel="stylesheet" href="<?php echo $this->config->item('templateAssetsPath')?>css/bootstrap.min.css" type="text/css" />
<!-- left menu-->
<link rel="stylesheet" href="<?php echo $this->config->item('templateAssetsPath')?>css/left_menu.css" type="text/css" />
<!-- smart mailer design css-->
<link rel="stylesheet" href="<?php echo $this->config->item('templateAssetsPath')?>css/style.css" type="text/css" />
<!-- scroll css-->
<link rel="stylesheet" href="<?php echo $this->config->item('templateAssetsPath')?>css/CustomScrollbar.css" type="text/css" />
<script type='text/javascript' src="<?php echo $this->config->item('templateAssetsPath')?>js/jquery.min.js"></script>
<script type='text/javascript' src="<?php echo $this->config->item('templateAssetsPath')?>js/bootstrap.min.js"></script>
<!-- scroll js-->
<script type='text/javascript' src="<?php echo $this->config->item('templateAssetsPath')?>js/CustomScrollbar.min.js"></script>
<!-- left menu-->
<script type='text/javascript' src="<?php echo $this->config->item('templateAssetsPath')?>js/app.js"></script>
<!-- image lighbox css and js-->
<link rel="stylesheet" href="<?php echo $this->config->item('templateAssetsPath')?>css/simplelightbox.css" type="text/css" />
<script type='text/javascript' src="<?php echo $this->config->item('templateAssetsPath')?>js/simple-lightbox.js"></script>

</head>
<body class="login-bg">
<div class="container">
<div class="smart-login">
<div align="center">
<img src="<?php echo $this->config->item('templateAssetsPath')?>images/smart_logo.png" class="login-logo img-responsive" alt="smart mailer" /></div>
<div class="login-box">
<?php if($error_msg) { ?>
	  <span class="text-danger"><?php echo $error_msg?></span>
	  <?php } ?>
          <?php if($is_valid=='yes') { ?>
          <form method="post" action="" role="login">
                                            
					    <div class="form-group">
						<label class="w300">New Password</label>
                        <input type="password" name="pass1" required class="form-control" placeholder="New Password(Minimun length is 6 character )" />
						<span class="text-danger"><?php echo form_error('pass1');?></span>
					    </div>
					
					    <div class="form-group">
						<label class="w300">Confirm New Password</label>
						<input type="password" name="pass2" required class="form-control" placeholder="Confirm New Password" />
						<span class="text-danger"><?php echo form_error('pass2');?></span>
					    </div>
					
					    <input type="submit" value="Create Password" class="btn btn-block w600">
</form>
          <?php } else { ?>
			<p class="mdem12 smem11 xsem10 text-center mt1 w600 greycolor">Your link to set new password is expired</p>
					<center><a href="<?php echo site_url().'forget-password';?>">Click Here </a> to generate new link.
					<br><b>Or</b><br>
					<a href="<?php echo site_url();?>"><button type="button" class="btn btn-block w600">Login</button></a></center>
					
          <?php } ?>
</div>
</div>
</div>

</body>
</html>