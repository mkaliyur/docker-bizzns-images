<div class="modal-dialog campaign-modal">
    <div class="modal-content">
      <div class="modal-body padding0 clearfix xsmb4 xsmt4">
       <div class="col-md-5 col-sm-5 col-xs-12 mt7 xsmt2">
       <div class="col-xs-12 bottom-border">
      
       <img src="<?php echo $this->config->item('templateAssetsPath')?>images/embed-code-img.png" class="img-responsive center-block mobile-img" /></div>
       </div>
       <div class="col-md-7 col-sm-7 col-xs-12 left-border campaign-field xsmt4 mt0 ">
        <div class="col-xs-12">
       <input type="radio" name="code" value="Java Script" checked="checked" class="jscode" data-type="javascript"><span class="em13"> Java Script</span>&nbsp;&nbsp;&nbsp;
       <input type="radio" name="code" value="Our Hosted Link" class="jscode" data-type="link"> <span class="em13">Our Hosted Link</span> <br />
	   <input type="radio" name="code" value="Our Hosted Link" class="jscode" data-type="html"> <span class="em13">HTML Code</span>
       <textarea name="" cols="" rows="8" class="mt4 xsmt5" id="javascriptCode" readonly="readonly"><script type="text/javascript" src="<?php echo site_url('view-webform'.$form_id.'.js')?>"></script></textarea>
       <textarea name="" cols="" rows="8" class="mt4 xsmt5" id="hostedLink" style="display:none" readonly="readonly"><?php echo site_url('view-webform/'.$form_id)?></textarea>
	    <textarea name="" cols="" rows="8" class="mt4 xsmt5" id="htmlcode" style="display:none" readonly="readonly">
		<link href='https://fonts.googleapis.com/css?family=Raleway:400,600,800' rel='stylesheet' type='text/css'>
		<style>
		<?php echo file_get_contents('./assets/default/css/webform/common_form.css'); ?>
		<?php echo str_replace('../images',site_url('assets/default/images'),file_get_contents('./assets/default/css/webform/form'.$formdetail->template_id.'.css')); ?>
		</style>
		<form action="<?php echo site_url('webform-submit-html')?>" method="post" class="webform"  id="webform">
		<?php echo $formdetail->form_html; ?>
		<input name="form_id" type="hidden" value="<?php echo $form_id; ?>">
		</form>
		<script>
		document.getElementById("rect").addEventListener("click", submitForm);
		
		function submitForm() {
			 document.getElementsByClassName("webform")[0].submit();
		}
		</script>
		</textarea>
  
        <div class="xstext-right xstext-center mt10 xsmt10"><a href="javascript:" class="blank-btn" data-dismiss="modal">Cancel</a> &nbsp; <a href="javascript:" class="smart-btn copybutt" data-copytarget="#javascriptCode">&nbsp;&nbsp;Copy&nbsp;&nbsp;</a></div>
       </div></div>
      </div>
    </div>
  </div>
<script>
$('body').delegate(".jscode", 'click', function() {

    var type = $(this).attr('data-type');
	if(type=='javascript')
	{ $('#javascriptCode').show();
	  $('#htmlcode').hide();
	  $('#hostedLink').hide(); 
	  $('.copybutt').attr('data-copytarget','#javascriptCode');
	  } 
	else if(type=='html') {
	  $('#htmlcode').show();
	  $('#javascriptCode').hide();
	  $('#hostedLink').hide();
	  $('.copybutt').attr('data-copytarget','#htmlcode');
	}
	else {
	  $('#javascriptCode').hide();
	  $('#htmlcode').hide();
	  $('#hostedLink').show();
	  $('.copybutt').attr('data-copytarget','#hostedLink');
	}
});


</script>