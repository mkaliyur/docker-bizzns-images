<script src="<?php echo $this->config->item('templateAssetsPath')?>js/jscolor.js"></script>
<link rel="stylesheet" href="<?php echo $this->config->item('templateAssetsPath')?>css/webform.css" />
<script>
$(document).on("click","#bgcolor-options",function() {
	$(".mylibrary").toggle();
});


//If clicked
$(document).mouseup(function (e)
{
    var libcontainer = $(".mylibrary");

    if (!libcontainer.is(e.target) // if the target of the click isn't the container...
        && libcontainer.has(e.target).length === 0) // ... nor a descendant of the container
    {
        libcontainer.hide();
    }
});


$(document).on("click",".mylibrary img",function() {
	var src = $(this).closest('img').attr('src');
	document.getElementById('demo-preview').style.backgroundImage =  "url("+src+")";
});
$(document).on("click",".noimage",function() {
	document.getElementById('demo-preview').style.backgroundImage =  "none";
});


function uploadimage(file){
	var formdata = new FormData();
	var imageType = ['jpeg', 'jpg', 'png', 'gif'];  
	if (-1 == $.inArray(file.type.split('/')[1], imageType)){
		alert("not valid");
		return false;
	}
	formdata.append('newmediaimg', file);
	var ajax = new XMLHttpRequest();
	ajax.addEventListener("load", appendimage, false);
	ajax.open("POST", "<?php echo  site_url().'uploadmediaimg'; ?>");
	ajax.send(formdata);
}
function appendimage(){ 
	$(".mediaimg").load(location.href + " .mediaimg");
}


function deleteImage(file_name)
{
    var r = confirm("Are you sure you want to delete this Image?")
	//var src=$(file_name).closest(".uploadedpic").find(".mCS_img_loaded").attr("src");
	//alert(src);
    if(r == true)
    {
        $.ajax({
          url: '',
          data: {
			  'file' : $(file_name).closest(".uploadedpic").find(".mCS_img_loaded").attr("src"),
			  "action" : "deleteImage"
		  },
          success: function (response) {
             $(file_name).parent().parent().remove();
			 PNotify.removeAll();
			 new PNotify({title: 'Success',text: 'Image Deleted Successfully.',type: 'success'});
          },
          error: function () {
             //alert("hello1");
          }
        });
		
    }
}	


</script>
<?php
if(isset($_GET['action'])){
	unlink(str_replace(site_url(), "./", $_GET['file']));
	die(str_replace(site_url(), "./", $_GET['file']));
}
?>
<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padding15 footer-height">
  <!-- dash header-->
  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 mb2 xsmb4">
    <div class="dash-header">
      <div class="row">
        <div class="col-md-12 col-sm-12">
          <h1 class="smart-color em22 smem18 xsem16">Forms</h1>
          <p class="em11 smem10 xsem10">Create, manage and use forms for your subscribers easily</p>
        </div>
        <!--<div class="col-md-5 col-sm-5 xstext-right xstext-center mt2 xsmt4 mb0 xsmb2">
<a href="#" class="smart-btn">Create Forms</a></div> -->
      </div>
    </div>
  </div>
  <!-- dash header end-->
  <div class="clearfix"></div>
  <div class="col-md-12 smart-tabs clearfix">
    <ul>
      <li><a href="<?php echo site_url('form-editor')?>" class="active" title="Settings"> <span class="hidden-xs">Settings</span><i class="icon-Settings visible-xs"></i> </a></li>
    </ul>
  </div>
  <div class="col-md-12 col-sm-12 col-xs-12 mb2 xsmb4">
    <div class="layout-box clearfix">
      <div class="bs-wizard">
        <div class="col-xs-3 bs-wizard-step complete">
          <div class="progress">
            <div class="progress-bar"></div>
          </div>
          <a href="<?php echo site_url('form-settings')?>" class="bs-wizard-dot"></a>
          <div class="text-center em8 smem9 xsem8 hidden-xs">setting</div>
        </div>
        <div class="col-xs-3 bs-wizard-step complete">
          <div class="progress">
            <div class="progress-bar"></div>
          </div>
          <a href="<?php if($form_session['step2']=='yes') { echo site_url('form-template'); } else { echo 'javascript:'; } ?>" class="bs-wizard-dot"></a>
          <div class="text-center em8 smem9 xsem8 hidden-xs">Templates</div>
        </div>
        <div class="col-xs-3 bs-wizard-step active">
          <div class="progress">
            <div class="progress-bar"></div>
          </div>
          <a href="<?php if($form_session['step3']=='yes') { echo site_url('form-editor'); } else { echo 'javascript:'; } ?>" class="bs-wizard-dot"></a>
          <div class="text-center em8 smem9 xsem8 hidden-xs">Editor</div>
        </div>
        <div class="col-xs-3 bs-wizard-step <?php if($form_session['step4']!='yes') { ?> disabled <?php } ?>">
          <div class="progress">
            <div class="progress-bar"></div>
          </div>
          <a href="<?php if($form_session['step4']=='yes') { echo site_url('form-publish'); } else { echo 'javascript:'; } ?>" class="bs-wizard-dot"></a>
          <div class="text-center em8 smem9 xsem8 hidden-xs">Publish</div>
        </div>
      </div>
      <div class="col-md-12 mt4 xsmt15 padding0 smart-field">
        <div class="col-xs-12 padding0">
          <div class="row">
            <div class="col-md-6 col-md-push-6 col-sm-6 col-xs-12 col-sm-push-6">
              <div class="ediotr-design-bg clearfix">
                <div class="editor-design">
                  <ul>
                    <li>
                      <button onclick="document.execCommand( 'bold',false,null);"><strong>B</strong></button>
                    </li>
                    <li>
                      <button onclick="document.execCommand('italic',false,null);"><em>I</em></button>
                    </li>
                    <li>
                      <button onclick="document.execCommand('underline',false,null);"><u>U</u></button>
                    </li>
                    <li>
                      <button onclick="document.execCommand('strikeThrough',false,null);"><strike>S</strike> </button>
                    </li>
                    <li>
                      <button id="bgcolor-options"> Background </button>
                      <div class="mylibrary menu_scroll mCustomScrollbar mCustomScrollbar2" id="mylibrary">
                        <button id="bgcolor-button" class="jscolor {valueElement:null,value:'2d97bb'} mt2 xsmt2">Background Color</button>
                        <div class="mediaimg">
                          <?php 
if(!is_dir("./assets/media_image"))
{
	mkdir("./assets/media_image");
}
foreach(scandir("./assets/media_image") as $key=>$image){
	if($key > 1){
	echo '<div class="uploadedpic"><img alt="" src="'.site_url().'assets/media_image/'.$image.'" ><div class="deleteimg"><a onclick="deleteImage(this)"><i class="fa fa-trash"></i></a></div></div>';
	}
	
}	
?>
                          <img src="https://cdn3.iconfinder.com/data/icons/softwaredemo/PNG/256x256/DeleteRed.png" class="noimage"> </div>
                        <label for="file-upload" class="custom-file-upload">Upload</label>
                        <input id="file-upload" type="file" onChange="uploadimage(this.files[0])"/>
                      </div>
                    </li>
                    <li>
                      <button class="jscolor {onFineChange:'buttonupdate(this)',valueElement:null,value:'2d97bb'}" value="cc66ff"> Button </button>
                    </li>
                    <li>
                      <button class="jscolor {onFineChange:'textcolor(this)',valueElement:null,value:'2d97bb'}">Text </button>
                    </li>
                    <li>
                      <div class="dropdown">
                        <button class="dropdown-toggle" type="button" data-toggle="dropdown">Headings <span class="caret"></span></button>
                        <ul class="dropdown-menu heading-list">
                          <li><a href="javascript:" onclick="document.execCommand('fontSize',false,7);" style="font-size:32px;" >Heading 1</a></li>
                          <li><a href="javascript:" onclick="document.execCommand('fontSize',false,6);" style="font-size:24px;">Heading 2</a></li>
                          <li><a href="javascript:" onclick="document.execCommand('fontSize',false,5);" style="font-size:20.8px;">Heading 3</a></li>
                          <li><a href="javascript:" onclick="document.execCommand('fontSize',false,4);" style="font-size:16px;">Heading 4</a></li>
                          <li><a href="javascript:" onclick="document.execCommand('fontSize',false,3);" style="font-size:12.8px;">Heading 5</a></li>
                          <li><a href="javascript:" onclick="document.execCommand('fontSize',false,2);" style="font-size:11.8px;">Heading 6</a></li>
                        </ul>
                      </div>
                    </li>
                    <li>
                      <button data-toggle="collapse" data-target="#demo1" class="pointer setting-icon">Add Fields</button>
                      <div id="demo1" class="collapse">
                        <input type="checkbox" name="name" class="addCustom" value="name" />
                        &nbsp; Name &nbsp;
                        <input type="checkbox" name="address" class="addCustom" value="address" />
                        &nbsp; Address &nbsp;
                        <input type="checkbox" name="city" class="addCustom" value="city" />
                        &nbsp; City &nbsp;
                        <input type="checkbox" name="state" class="addCustom" value="state" />
                        &nbsp; State &nbsp;
                        <input type="checkbox" name="country" class="addCustom" value="country" />
                        &nbsp; Country &nbsp;
                        <input type="checkbox" name="zip" class="addCustom" value="zip" />
                        &nbsp; Zip &nbsp;
                        <input type="checkbox" name="phone" class="addCustom" value="phone" />
                        &nbsp; Phone </div>
                    </li>
                  </ul>
                </div>
              </div>
              <div class="form-bgborder clearfix">
                <div class="form-login">
                  <div class="template_text_div" id="target">

                      <?php echo $template_text?>

                  </div>
                </div>
              </div>
            </div>
            <form action="" method="post" class="form_editor">
              <div class="col-md-6 col-md-pull-6 col-sm-6 col-xs-12 col-sm-pull-6">
                <div class="col-xs-12 xsmb3 mb5 padding0 xsmt8 mt0">
                  <div class="row">
                    <div class="col-md-4 col-sm-5 col-xs-6 xsmb2 em10 smem10 xsem9">Thank You Page</div>
                    <div class="col-md-6 col-sm-7 col-xs-6 em10 smem10 xsem9">
                      <input type="radio" name="thankyou_page" value="default" <?php if($thankyou_page!='custom') { ?> checked="checked" <?php } ?> class="thankyou_page" />
                      Default Page<br>
                      <input type="radio" name="thankyou_page" value="custom" <?php if($thankyou_page=='custom') { ?> checked="checked" <?php } ?>  class="thankyou_page"  />
                      Custom Page<br>
                    </div>
                  </div>
                </div>
                <div class="col-xs-12 xsmb4 mb5 padding0 thankyou_div" style="display:<?php if($thankyou_page=='custom') { echo 'block'; } else { echo 'none'; } ?>">
                  <div class="row">
                    <div class="col-md-4 col-sm-5 col-xs-6 xsmb2 em10 smem10 xsem9">Custom Page</div>
                    <div class="col-md-6 col-sm-7 col-xs-6">
                      <input name="thankyou_custom" type="text" placeholder="Custom Page URL" value="<?php echo $thankyou_custom?>" />
                    </div>
                  </div>
                </div>
                <div class="col-xs-12 xsmb4 mb5 padding0">
                  <div class="row">
                    <div class="col-md-4 col-sm-5 col-xs-6 xsmb2 em10 smem10 xsem9">Already Subscribe</div>
                    <div class="col-md-6 col-sm-7 col-xs-6 em10 smem10 xsem9">
                      <input type="radio" name="already_subscribe" value="default" <?php if($already_subscribe!='custom') { ?> checked="checked" <?php } ?> class="already_subscribe">
                      Default Page<br>
                      <input type="radio" name="already_subscribe" value="custom" <?php if($already_subscribe=='custom') { ?> checked="checked" <?php } ?> class="already_subscribe">
                      Custom Page<br>
                    </div>
                  </div>
                </div>
                <div class="col-xs-12 xsmb3 mb5 padding0 alreadysub_div" style="display:<?php if($already_subscribe=='custom') { echo 'block'; } else { echo 'none'; } ?>">
                  <div class="row">
                    <div class="col-md-4 col-sm-5 col-xs-6 xsmb2 em10 smem10 xsem9">Custom Page</div>
                    <div class="col-md-6 col-sm-7 col-xs-6">
                      <input name="alreadysub_custom" type="text" placeholder="Custom Page URL" value="<?php echo $alreadysub_custom?>">
                    </div>
                  </div>
                </div>
                <div class="col-xs-12 xsmb3 mb5 padding0">
                  <div class="row">
                    <div class="col-md-4 col-sm-5 col-xs-6 xsmb2 em10 smem10 xsem9">Double OTP</div>
                    <div class="col-md-6 col-sm-7 col-xs-6">
                      <label class="switch" style="margin:0 !important;">
                      <input class="switch-input" type="checkbox" name="double_otp" value="yes" <?php if($double_otp=='yes') { ?> checked="checked" <?php } ?> />
                      <input type="hidden" name="template_text" value="" class="template_text" />
					   <input type="hidden" name="image_text" value="" class="image_text" />
                      <span class="switch-label" data-on="On" data-off="Off"></span> <span class="switch-handle"></span> </label>
                      <p class="em9 smem9 xsem8 mt3 xsmt5">Send Confirmation Mail To Subscriber</p>
                    </div>
                  </div>
                </div>
                <div class="clearfix"></div>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
    <div class="col-xs-12 xstext-right xstext-center mt2 xsmt8 padding0">
      <?php if($form_session['update']=='yes') { ?>
      <a href="<?php echo site_url('form-settings')?>" class="blank-btn" title="Back">Back</a> &nbsp;
      <?php } else { ?>
      <a href="<?php echo site_url('form-template')?>" class="blank-btn" title="Back">Back</a> &nbsp;
      <?php }?>
      <button type="button" class="smart-btn publishbutt" name="step3"  value="step3" title="Publish">Publish </button>
    </div>
  </div>
</div>


<!--GET SCREENSHOT -->
<script type="text/javascript" src="<?php echo $this->config->item('templateAssetsPath')?>js/html2canvas.js"></script>
<script type="text/javascript" src="<?php echo $this->config->item('templateAssetsPath')?>js/jquery.plugin.html2canvas.js"></script>
<div id="img_val" style="display:none;"></div>
<!--GET SCREENSHOT -->


<script>
$( document ).ready(function() {
    var editable_elements = document.querySelectorAll("[contenteditable=false]");
    for(var i=0; i<editable_elements.length; i++)
     editable_elements[i].setAttribute("contentEditable", true);
	 
	 if($(".template_text_div" ).find('.namefield').length!=0)
	 $(".addCustom[name='name']").attr('checked','checked');
	 
	 if($(".template_text_div" ).find('.addressfield').length!=0)
	 $(".addCustom[name='address']").attr('checked','checked');
	 
	 if($(".template_text_div" ).find('.cityfield').length!=0)
	 $(".addCustom[name='city']").attr('checked','checked');
	 
	 if($(".template_text_div" ).find('.statefield').length!=0)
	 $(".addCustom[name='state']").attr('checked','checked');
	 
	 if($(".template_text_div" ).find('.countryfield').length!=0)
	 $(".addCustom[name='country']").attr('checked','checked');
	 
	 if($(".template_text_div" ).find('.zipfield').length!=0)
	 $(".addCustom[name='zip']").attr('checked','checked');
	 
	 if($(".template_text_div" ).find('.phonefield').length!=0)
	 $(".addCustom[name='phone']").attr('checked','checked');
	 
	  
	  
});

/*-------------------------- view hide js for default and custome fields starts here ------------------------------------*/
$('body').delegate(".thankyou_page", 'click', function() {
	var tvalue = $(this).val();
	if(tvalue=='custom')
	{ $('.thankyou_div').fadeIn(); }
	else { $('.thankyou_div').fadeOut(); }
	
});
$('body').delegate(".already_subscribe", 'click', function() {
    
	var tvalue = $(this).val();
	if(tvalue=='custom')
	{ $('.alreadysub_div').fadeIn(); }
	else { $('.alreadysub_div').fadeOut(); }
	
});
$('body').delegate(".publishbutt", 'click', function() {
    
	$('#preloader').show();
    var editable_elements = document.querySelectorAll("[contenteditable=true]");
    for(var i=0; i<editable_elements.length; i++)
     editable_elements[i].setAttribute("contentEditable", false);
	//capture();
	
	$('#target').html2canvas({
		onrendered: function (canvas) {
			  $('.image_text').val(canvas.toDataURL("image/png"));
		}
	});
	
	var template_text = $('.template_text_div').html().replace("javascript:", "");
	$('.template_text').val(template_text);
	
	var delay=1000; //1 second
	setTimeout(function() {
	   $('.form_editor').submit();
	}, delay);

	
	
});
/*-------------------------- view hide js for default and custome fields ends here ------------------------------------*/

/*-------------------------- Custom field add js starts here ----------------------------------------------------------*/
$('body').delegate(".addCustom", 'click', function() {
	var fieldname = $(this).val();
	var capFieldName = capitalizeFirstLetter(fieldname);

	if($(this).is(":checked"))
	{
		if($(".template_text_div" ).find('.'+fieldname+'field').length==0)
		 { 
			var copydata = $(".template_text_div").find('.copydiv').clone();
			$(copydata).find('.fieldname').text(capFieldName);
			$(copydata).find('.fieldinput').attr('name',fieldname);
			$(copydata).find('.fieldinput').attr('placeholder',capFieldName);
			$(copydata).addClass(fieldname+'field');
			$(copydata).removeClass('copydiv');
			$('.copydiv').after(copydata);
		 }
	 } else {
	     $(".template_text_div").find('.'+fieldname+'field').remove();
	 }
	
});
function capitalizeFirstLetter(string) {
    return string.charAt(0).toUpperCase() + string.slice(1);
}
/*-------------------------- Custom field add js ends here ----------------------------------------------------------*/

/*-------------------------- error message starts here---------------------------------------------------*/
<?php if(validation_errors()!='' || $error!='') { ?>
new PNotify({
	  title: 'Error',
	  text: <?php echo json_encode(validation_errors())?>,
	  type: 'error'
	});
<?php } ?>
/*-------------------------- error message ends here---------------------------------------------------*/

/*----------------------- For Editor js starts here --------------------------------------*/
var options = {
    valueElement: null,
    width: 300,
    height: 120,
    sliderSize: 20,
    position: 'top',
    //borderColor: '#CCC',
    //insetColor: '#CCC',
    //backgroundColor: '#202020'
};

var pickers = {};

pickers.bgcolor = new jscolor('bgcolor-button', options);
pickers.bgcolor.onFineChange = "update('bgcolor')";
pickers.bgcolor.fromString('2d97bb');

pickers.fgcolor = new jscolor('fgcolor-button', options);
pickers.fgcolor.onFineChange = "update('fgcolor')";
pickers.fgcolor.fromString('FFFFFF');

function update(id) {

    document.getElementById('demo-preview').style.backgroundColor =  pickers.bgcolor.toHEXString();
}

function setRGB(id, r, g, b) {
    pickers[id].fromRGB(r, g, b);
    update(id);
}

function setString(id, str) {
    pickers[id].fromString(str);
    update(id);
}

update('bgcolor');
update('fgcolor');


function buttonupdate(jscolor) {
    // 'jscolor' instance can be used as a string

    document.getElementById('rect').style.backgroundColor = '#' + jscolor
}

function textcolor(jscolor) {

    //alert(jscolor);
    // 'jscolor' instance can be used as a string

    document.execCommand('foreColor', false, "#"+jscolor);

}
/*------------------------ editor js ends here ----------------------------------*/
</script>
