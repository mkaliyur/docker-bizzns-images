<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Webform</title>
<link href='https://fonts.googleapis.com/css?family=Raleway:400,600,800' rel='stylesheet' type='text/css'>
<link rel="icon" href="<?php echo $this->config->item('templateAssetsPath')?>images/favicon-icon.png" type="image/png"/>
<link rel="stylesheet" href="<?php echo $this->config->item('templateAssetsPath')?>css/bootstrap.min.css" type="text/css" />
<link rel="stylesheet" href="<?php echo $this->config->item('templateAssetsPath')?>css/style.css" type="text/css" />
<link rel="stylesheet" href="<?php echo $this->config->item('templateAssetsPath')?>css/webform.css" type="text/css" />

<script type='text/javascript' src="<?php echo $this->config->item('templateAssetsPath')?>js/jquery.min.js"></script>
<script src="<?php echo $this->config->item('templateAssetsPath')?>js/jquery.form.js"></script>
<!-- PNotify -->
<script type="text/javascript" src="<?php echo $this->config->item('templateAssetsPath')?>js/notify/pnotify.core.js"></script>
<script type="text/javascript" src="<?php echo $this->config->item('templateAssetsPath')?>js/notify/pnotify.buttons.js"></script>
<script>
$(document).ready(function(){
    $(".hide").click(function(){
        $(".error-msg").hide();
		 $(".success-msg").hide();
    });
  
  
});
</script>
</head>
<body style="background:transparent!important">

<div class="smart-form-preview">
<form action="<?php echo site_url('webform-submit')?>" method="post" class="webform"  id="webform">
<?php echo $formdetail->form_html?>
<input name="form_id" type="hidden" value="<?php echo $formdetail->id?>">
</form>
</div>
<script type="text/javascript" src="<?php echo $this->config->item('templateAssetsPath')?>/js/iframeResizer.contentWindow.min.js" defer></script>
</body>
</html>


<script>
$(document).ready(function() {
$("#rect").click(function(){
   $(".webform").submit();
});  
    $(".webform").submit(function(event) {
        var posturl = $(this).attr('action');
        $(this).ajaxSubmit({
            url: posturl,
            dataType: 'json',
            success: function(response) {
				
                if (response.success) {
					
					if(response.redirect=='yes')
					{
					  window.top.location.href = response.redirect_url;
					}
					else
					{
					 PNotify.removeAll();
				     new PNotify({title: 'Success',text: 'Subscribed Successfully',type: 'success'});
					}
					
					document.getElementById("webform").reset();
					$(".error-msg").hide();

					 
                } else {
				   if(response.redirect=='yes')
					{
					  window.top.location.href = response.redirect_url;
					}
				   else
					{
				   PNotify.removeAll();
				   new PNotify({title: 'Error',text: response.error,type: 'error'});
				   }
                }
            },
        });
        return false;
    });
});
</script>


