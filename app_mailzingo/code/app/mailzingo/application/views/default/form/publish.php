<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padding15 footer-height">
<!-- dash header-->
<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 mb2 xsmb4">
<div class="dash-header">
<div class="row">
<div class="col-md-12 col-sm-12"><h1 class="smart-color em22 smem18 xsem16">Forms</h1>
<p class="em11 smem10 xsem10">Create, manage and use forms for your subscribers easily</p></div>
<!--<div class="col-md-5 col-sm-5 xstext-right xstext-center mt2 xsmt4 mb0 xsmb2">
<a href="#" class="smart-btn">Create Forms</a></div> -->
</div>
</div>
</div>
<!-- dash header end-->

<div class="clearfix"></div>
<div class="col-md-12 col-sm-12 col-xs-12 mb2 xsmb4">
<div class="layout-box clearfix">
<div class="bs-wizard">
<div class="col-xs-3 bs-wizard-step complete">
<div class="progress"><div class="progress-bar"></div></div>
<a href="<?php echo site_url('form-settings/'.$form_id)?>" class="bs-wizard-dot"></a>
<div class="text-center em8 smem9 xsem8 hidden-xs">setting</div>
</div>
                
<div class="col-xs-3 bs-wizard-step complete">
<div class="progress"><div class="progress-bar"></div></div>
<a href="<?php echo site_url('form-template/'.$form_id)?>" class="bs-wizard-dot"></a>
<div class="text-center em8 smem9 xsem8 hidden-xs">Templates</div>
</div>
           
<div class="col-xs-3 bs-wizard-step complete">
<div class="progress"><div class="progress-bar"></div></div>
<a href="<?php echo site_url('form-editor/'.$form_id)?>" class="bs-wizard-dot"></a>
<div class="text-center em8 smem9 xsem8 hidden-xs">Editor</div>
</div>
<div class="col-xs-3 bs-wizard-step active">
<div class="progress"><div class="progress-bar"></div></div>
<a href="<?php echo site_url('form-publish/'.$form_id)?>" class="bs-wizard-dot"></a>
<div class="text-center em8 smem9 xsem8 hidden-xs">Publish</div>
</div>
</div>
<div class="col-md-12 mt2 xsmt15 padding0 smart-field">
<div class="mt0 mb2 xsmb3 w600 em11 smem11 xsem11">Here You Can Choose How To Publish Your Form</div>






<div class="col-xs-12 padding0 smart-field">
<div class="row">
<div class="col-md-4 col-sm-4 col-xs-12">
<p>Java Script Code</p>
<textarea name="" cols="" rows="10" class="jstextarea" id="jscopycode" readonly="readonly">
<script type="text/javascript" src="<?php echo site_url('view-webform'.$form_id.'.js')?>"></script>
</textarea>

<div class="row">
<div class="col-md-9 mt2 xsmt3"><p class="em9 smem9 xsem9">Use Java Script Code And Put In Your Website</p></div>
<div class="col-md-3 text-right mt2 xsmt0 mb0 xsmb5"><a href="javascript:" class="smart-btn" data-copytarget="#jscopycode">Copy</a></div>
</div>
</div>


<div class="col-md-4 col-sm-4 col-xs-12">
<p>Our Hosted Link</p>
<textarea name="" cols="" rows="10" id="linkcopycode" readonly="readonly">
<?php echo site_url('view-webform/'.$form_id)?>
</textarea>

<div class="row">
<div class="col-md-9 mt2 xsmt3"><p class="em9 smem9 xsem9">Use Our Hosted Link Directly</p></div>
<div class="col-md-3 text-right mt2 xsmt0 mb0 xsmb5"><a href="javascript:" class="smart-btn" data-copytarget="#linkcopycode">Copy</a></div>
</div>

</div>

<div class="col-md-4 col-sm-4 col-xs-12">
<p>HTML Code</p>
<textarea name="" cols="" rows="10" id="htmlcopycode" readonly="readonly">
<link href='https://fonts.googleapis.com/css?family=Raleway:400,600,800' rel='stylesheet' type='text/css'>
<style>
<?php echo file_get_contents('./assets/default/css/webform/common_form.css'); ?>
<?php echo str_replace('../images',site_url('assets/default/images'),file_get_contents('./assets/default/css/webform/form'.$formdetail->template_id.'.css')); ?>
</style>
<form action="<?php echo site_url('webform-submit-html')?>" method="post" class="webform"  id="webform">
<?php echo $formdetail->form_html; ?>
<input name="form_id" type="hidden" value="<?php echo $form_id; ?>">
</form>
<script>
document.getElementById("rect").addEventListener("click", submitForm);

function submitForm() {
	 document.getElementsByClassName("webform")[0].submit();
}
</script>
</textarea>

<div class="row">
<div class="col-md-9 mt2 xsmt3"><p class="em9 smem9 xsem9">Use Our HTML Code</p></div>
<div class="col-md-3 text-right mt2 xsmt0 mb0 xsmb5"><a href="javascript:" class="smart-btn" data-copytarget="#htmlcopycode">Copy</a></div>
</div>

</div>

</div>
</div>
</div>
</div>

<div class="col-xs-12 xstext-right xstext-center mt2 xsmt8 padding0">
<a href="<?php echo site_url('forms')?>" class="smart-btn" title="Go To Forms">Go To Forms </a>
</div>
</div>
</div>
<script>
/*
  Copy text from any appropriate field to the clipboard
  By Craig Buckler, @craigbuckler
  use it, abuse it, do whatever you like with it!
*/
(function() {

    'use strict';
  
  // click events
  document.body.addEventListener('click', copy, true);

    // event handler
    function copy(e) {

    // find target element
    var 
      t = e.target,
      c = t.dataset.copytarget,
      inp = (c ? document.querySelector(c) : null);
      
    // is element selectable?
    if (inp && inp.select) {
      
      // select text
      inp.select();

      try {
        // copy text
        document.execCommand('copy');
        inp.blur();
		PNotify.removeAll();
		new PNotify({title: 'Success',text: 'Copied Successfully', type: 'success'});
        
      }
      catch (err) {
        alert('please press Ctrl/Cmd+C to copy');
      }
      
    }
    
    }

})();
</script>