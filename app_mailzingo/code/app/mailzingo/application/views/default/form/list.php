<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padding15 footer-height">
<!-- dash header-->
<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 mb2 xsmb4">
<div class="dash-header">
<div class="row">
<div class="col-md-7 col-sm-7"><h1 class="smart-color em22 smem18 xsem16">Forms</h1>
<p class="em11 smem10 xsem10">Create, manage and use forms for your subscribers easily</p></div>
<div class="col-md-5 col-sm-5 xstext-right xstext-center mt2 xsmt4 mb0 xsmb2">
<a href="<?php echo site_url('form-settings?add_type=new')?>" class="smart-btn">Create Forms</a></div> </div>
</div>
</div>
<!-- dash header end-->

<div class="clearfix"></div>
<div class="col-md-12 col-sm-12 col-xs-12 mb2 xsmb4">
<div class="layout-box1 clearfix padding0 mailer-data">
<div class="box-padding">
<div class="col-xs-12 col-md-6 col-sm-6 mt1 mb2 xsmb3 w600 em11 smem11 xsem11 padding0">Manage Forms</div>
<div class="col-xs-12 col-md-6 col-sm-6 mb2 xsmb2 text-right template-search padding0">
<div class="col-md-offset-6 col-sm-offset-2"><input type="text" placeholder="Search for Form" class="search"><button type="button"><i class="fa fa-search"></i></button></div>
</div></div>

<div class="clearfix"></div>
<div class="table-responsive table-data table-fixed-header contentDiv forms-coluom">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
 <thead> <tr class="w600">
    <th class="text-center"><input name="" type="checkbox" value="all" id="select_all"/></th>
    <th>Form Name</th>
    <th class="text-left">Create Date</th>
    <th class="text-center">Action</th>
  </tr>
   <tr class="noresult" style="display:  <?php if(sizeof($all_record)==0) { ?> table-row-group <?php } else { ?>none <?php } ?>;">
		   <th colspan="6" class="text-center w3000">No Record Found</th>
		  </tr>
  </thead>
 <tbody> <?php foreach($all_record as $val) { ?>
  <tr class="text-center looptr">
    <td><input name="select_checkbox" type="checkbox" value="<?php echo $val->id?>" class="select_checkbox"/></td>
    <td class="text-left more-word1"><?php echo $val->form_name?></td>
    <td class="text-left"><?php echo date('M d, Y',$val->add_time)?></td>
     <td class="table-icon"><ul>
    <li><a data-toggle="modal" data-target="#preview" title="Preview" data-img="<?php echo site_url('assets/uploads/form_images/'.$val->image)?>" class="preview"><i class="fa fa-eye em12" aria-hidden="true"></i></a></li>
    <li><a href="<?php echo site_url('form-settings/'.$val->id)?>" title="Edit"><i class="fa fa-pencil-square-o em12" aria-hidden="true"></i></a></li>
    <li><a data-toggle="modal" data-target="#codeCopy" class="viewembed" title="Embed Code" data-id="<?php echo $val->id?>"><i class="fa fa-code em12" aria-hidden="true"></i></a></li>
    <li><a data-toggle="modal" data-target="#delete" class="deletelink" title="Delete" data-href="<?php echo site_url('delete-form/'.$val->id)?>" href="javascript:"><i class="fa fa-trash-o em12" aria-hidden="true"></i></a></li>
    </ul></td>
  </tr>
  <?php } ?>
  </tbody>
</table>
</div>
</div>


<div class="col-md-12 col-sm-12 col-xs-12 xstext-right xstext-center mt2 xsmt8 padding0">
<a data-toggle="modal" data-target="#delete" href="javascript:" class="activity-btn disableGreybtn deleteMultiLink multi_action_btn" title="Delete"> &nbsp; Delete &nbsp;</a>  <!--&nbsp;
<a href="<?php echo site_url('form-settings?add_type=new')?>" class="smart-btn" title="Create Forms">Create Forms</a>-->
</div>


</div>
</div>

<!-- copy embeded code modalpopup-->
<div id="codeCopy" class="modal fade codeCopy" role="dialog"></div>

<!-- form preview-->
<div id="preview" class="modal" role="dialog">
<div class="modal-dialog form-preview"  style="transform:none !important">
<div class="modal-content">
<button class="close-preview" data-dismiss="modal">&times;</button>
<div class="modal-body padding0 clearfix xsmb4 xsmt4">
<div class="col-xs-12"><img src="#" class="img-responsive previewimg" /></div>
</div>
</div>
</div>
</div>

<script>
/*-------------------------- delete single form starts here ------------------------------------*/
$('body').delegate(".deletelink", 'click', function() {
    
	deleteurl = $(this).attr('data-href');
	 $('#delete_yes_button').attr('href',deleteurl);
	 $('#delete_yes_button').removeClass('deleteMultiple');
});
/*-------------------------- delete single form ends here ------------------------------------*/

/*-------------------------- Show Preview starts here ------------------------------------*/
$('body').delegate(".preview", 'click', function() {
	imgurl = $(this).attr('data-img');
	$('.previewimg').attr('src',imgurl);
	 
});
/*-------------------------- Show Preview ends here ------------------------------------*/

/*-------------------------- check/uncheck multiple form starts here ------------------------------------*/
$('body').delegate("#select_all", 'change', function() {
      $(".select_checkbox").prop('checked', $(this).prop("checked"));
	  $(".select_checkbox").change();
      });
$('body').delegate(".select_checkbox", 'change', function() {
	  var length = $('.select_checkbox:checked').length
	  if(length==0)
	   {
		  $(".multi_action_btn").addClass('disableGreybtn');
	   } else {
		  $(".multi_action_btn").removeClass('disableGreybtn');
	   }
      });
/*-------------------------- check/uncheck multiple form ends here ------------------------------------*/

/*-------------------------- delete multiple form starts here ------------------------------------*/
$('body').delegate(".deleteMultiLink", 'click', function() {
    
	var length = $('.select_checkbox:checked').length;
	if(length==0)
	{ return false; }
	deleteurl = $(this).attr('data-href');
	 $('#delete_yes_button').attr('href','javascript:');
	 $('#delete_yes_button').addClass('deleteMultiple');
});
$('body').delegate(".deleteMultiple", 'click', function() {

    var posturl = site_url+'delete-multiple-form';
	var id = [];
	$(".select_checkbox:checkbox:checked").each(function (e) {
            id[e] = $(this).val();
        });
	var length = $('.select_checkbox:checked').length;
	if(length==0)
	{ return false; }

    $.ajax({
        url: posturl,
        dataType: 'json',
        type: "POST",
        data: {id: id},
        success: function(data) {
            if (data.success) {
			    $('#delete').modal('toggle');
				PNotify.removeAll();
				new PNotify({title: 'Success',text: 'Form Deleted successfully',type: 'success'});
				 $(".multi_action_btn").addClass('disableGreybtn');
                $(".contentDiv").load(location.href + " .contentDiv"); 
            }
        },
    });
});
/*-------------------------- delete multiple form ends here------------------------------------*/

/*-------------------------- view Embed multiple form starts here ------------------------------------*/
$('body').delegate(".viewembed", 'click', function() {

    var posturl = site_url+'view-form-embed';
	var id = $(this).attr('data-id');

    $.ajax({
        url: posturl,
        dataType: 'json',
        type: "POST",
        data: {id: id},
        success: function(data) {
            if (data.success) {
                $('.codeCopy').html(data.html);
            }
        },
    });
});
/*-------------------------- view Embed multiple form ends here ------------------------------------*/

/*-------------------------- Search function starts here ---------------------------------------------------*/
$('body').delegate(".search", 'keyup', function() {
	var searchVal = $(this).val();
	$( ".looptr:contains('"+searchVal+"')" ).show();
	$('.looptr:not(:contains('+ searchVal +'))').hide(); 
	$( ".noresult").hide();
         if($( ".looptr:contains('"+searchVal+"')" ).length === 0){ 
            $( ".noresult").show();
            $('#select_all').attr('disabled',true);
        }else{
            $('#select_all').attr('disabled',false);
        }
	
});
/*-------------------------- Search function ends here ---------------------------------------------------*/

/*-------------------------- copy code starts here ------------------------------------*/
(function() {

    'use strict';
  
  // click events
  document.body.addEventListener('click', copy, true);

    // event handler
    function copy(e) {

    // find target element
    var 
      t = e.target,
      c = t.dataset.copytarget,
      inp = (c ? document.querySelector(c) : null);
      
    // is element selectable?
    if (inp && inp.select) {
      
      // select text
      inp.select();

      try {
        // copy text
        document.execCommand('copy');
        inp.blur();
		PNotify.removeAll();
		new PNotify({title: 'Success',text: 'Copied Successfully', type: 'success'});
        
      }
      catch (err) {
        alert('please press Ctrl/Cmd+C to copy');
      }
      
    }
    
    }

})();
/*-------------------------- copy code ends here ------------------------------------*/

/*-------------------------- success message starts here---------------------------------------------------*/
<?php if($this->session->userdata('success_msg')) { ?>
new PNotify({
	  title: 'Success',
	  text: '<?php echo $this->session->userdata('success_msg')?>',
	  type: 'success'
	});
<?php $this->session->unset_userdata('success_msg'); } ?>
/*-------------------------- success message ends here---------------------------------------------------*/

/*---------------------------if none is checked start---------------------------*/
$(".select_checkbox").change(function(){
    if ($('.select_checkbox:checked').length != $('.select_checkbox').length) {
       $("#select_all").prop('checked', false);
       
    }else{
        $("#select_all").prop('checked', true);
    }
});
/*---------------------------if none is checked end---------------------------*/

</script>