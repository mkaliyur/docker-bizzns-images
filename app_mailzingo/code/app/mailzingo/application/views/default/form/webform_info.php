<!DOCTYPE html>
<html lang="en">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<!-- Tell the browser to be responsive to screen width -->
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
<!-- fonts-->
<link rel="stylesheet" href="<?php echo $this->config->item('templateAssetsPath')?>css/font-awesome.min.css" type="text/css" />
<link href='https://fonts.googleapis.com/css?family=Roboto:300,400,500,600,800' rel='stylesheet' type='text/css'>
<link href='https://fonts.googleapis.com/css?family=Raleway:400,500,600,800' rel='stylesheet' type='text/css'>
<link rel="icon" href="<?php echo $this->config->item('templateAssetsPath')?>images/favicon-icon.png" type="image/png"/>
<link rel="stylesheet" href="<?php echo $this->config->item('templateAssetsPath')?>css/bootstrap.min.css" type="text/css" />
<!-- smart mailer design css-->
<link rel="stylesheet" href="<?php echo $this->config->item('templateAssetsPath')?>css/style.css" type="text/css" />
<title>
<?php echo $this->config->item('site_title')?>
</title>

</head>
<body>
<div class="succes-box-area">
<div class="success_box text-center">
<?php if($success==true) { ?>
<img src="<?php echo $this->config->item('templateAssetsPath')?>images/success.png" class="img-responsive center-block">
<h1 class="smart-color em20 smem16 xsem14">Success</h1>
<p class="em12 smem12 xsem11">You have subscribed to list successfully.</p>
<?php } else { ?>
<img src="<?php echo $this->config->item('templateAssetsPath')?>images/error.png" class="img-responsive center-block">
<h1 class="smart-color em20 smem16 xsem14">Please Check Error</h1>
<p class="em12 smem12 xsem11"><?=$error?></p>
<?php } ?>



<div class="text-center"><a href="javascript:void(0)" onclick="window.history.back();" class="smart-btn">Back</a></div>

</div>
</div> 

</body>
</html>
