<!-- image hover-->
<script src="<?php echo $this->config->item('templateAssetsPath')?>js/modernizr.js"></script>
<script>
    $(document).ready(function(){
        if (Modernizr.touch) {
            // show the close overlay button
            $(".close-overlay").removeClass("hidden");
            // handle the adding of hover class when clicked
            $(".img").click(function(e){
                if (!$(this).hasClass("hover")) {
                    $(this).addClass("hover");
                }
            });
            // handle the closing of the overlay
            $(".close-overlay").click(function(e){
                e.preventDefault();
                e.stopPropagation();
                if ($(this).closest(".img").hasClass("hover")) {
                    $(this).closest(".img").removeClass("hover");
                }
            });
        } else {
            // handle the mouseenter functionality
            $(".img").mouseenter(function(){
                $(this).addClass("hover");
            })
            // handle the mouseleave functionality
            .mouseleave(function(){
                $(this).removeClass("hover");
            });
        }
    });
</script>

<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padding15 footer-height">
<!-- dash header-->
<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 mb2 xsmb4">
<div class="dash-header">
<div class="row">
<div class="col-md-12 col-sm-12"><h1 class="smart-color em22 smem18 xsem16">Forms</h1>
<p class="em11 smem10 xsem10">Create, manage and use forms for your subscribers easily</p></div>
<!--<div class="col-md-5 col-sm-5 xstext-right xstext-center mt2 xsmt4 mb0 xsmb2">
<a href="#" class="smart-btn">Create Forms</a></div> -->
</div>
</div>
</div>
<!-- dash header end-->

<div class="clearfix"></div>
<div class="col-md-12 col-sm-12 col-xs-12 mb2 xsmb4">
<div class="layout-box1 clearfix">
<div class="bs-wizard">
<div class="col-xs-3 bs-wizard-step complete">
<div class="progress"><div class="progress-bar"></div></div>
<a href="<?php echo site_url('form-settings')?>" class="bs-wizard-dot"></a>
<div class="text-center em8 smem9 xsem8 hidden-xs">setting</div>
</div>
                
<div class="col-xs-3 bs-wizard-step active">
<div class="progress"><div class="progress-bar"></div></div>
<a href="<?php if($form_session['step2']=='yes') { echo site_url('form-template'); } else { echo 'javascript:'; } ?>" class="bs-wizard-dot"></a>
<div class="text-center em8 smem9 xsem8 hidden-xs">Templates</div>
</div>
           
<div class="col-xs-3 bs-wizard-step <?php if($form_session['step3']!='yes') { ?> disabled <?php } ?>">
<div class="progress"><div class="progress-bar"></div></div>
<a href="<?php if($form_session['step3']=='yes') { echo site_url('form-editor'); } else { echo 'javascript:'; } ?>" class="bs-wizard-dot"></a>
<div class="text-center em8 smem9 xsem8 hidden-xs">Editor</div>
</div>
<div class="col-xs-3 bs-wizard-step <?php if($form_session['step4']!='yes') { ?> disabled <?php } ?>">
<div class="progress"><div class="progress-bar"></div></div>
<a href="<?php if($form_session['step4']=='yes') { echo site_url('form-publish'); } else { echo 'javascript:'; } ?>" class="bs-wizard-dot"></a>
<div class="text-center em8 smem9 xsem8 hidden-xs">Publish</div>
</div>
</div>
<div class="col-md-12 mt1 xsmt15 padding0">
<div class="col-xs-12 mt0 mb2 xsmb3 box-padding  w600 em11 smem11 xsem11">Form Templates </div>
<div class="col-xs-12 padding0 line-bottom"></div>
<div class="clearfix"></div>
<div class="col-xs-12 box-padding xsmb2 mb1 xsmt2 gallery data-scroll">
<form action="" method="post" id="templateform">
<div class="row">
<div id="effect-2" class="effects clearfix">

<?php foreach($template_list as $key=>$val) { $key++; ?>
<div class="form-preview-width mb2 xsmb4">
<p class="xsem8 em10 smem10"><?php echo $val->template_name?></p>
<div class="img tem-hover" id="element1">  
<?php if($template_id==$val->id) { ?>
<i class="fa fa-check template-active-icon" aria-hidden="true"></i>
<?php } ?>
<img src="<?php echo $this->config->item('templateAssetsPath')?>images/form-images/form-deafult<?php echo $key?>.jpg" class="img-responsive template-img center-block" title="<?php echo $val->template_name?>" />
<div class="overlay">
<div class="expand">
<a href="<?php echo $this->config->item('templateAssetsPath')?>images/form-images/form-deafult<?php echo $key?>.jpg" title="<?php echo $val->template_name?>"><i class="fa fa-search-plus" aria-hidden="true" title="<?php echo $val->template_name?>" ></i></a>
</div></div></div>
<div id="element2"><a href="javascript:" class="template-btn mt8 xsmt8 smem9 xsem8 em10 selectTemplate" title="Select Template" data-id="<?php echo $val->id?>">Select Template</a></div>
</div>
<?php } ?>



</div>
</div>
<input type="hidden" name="template_id" value="" id="template_input" />
<input type="hidden" name="step2" value="yes" />
</form>

</div>

</div>
</div>


<div class="col-xs-12 xstext-right xstext-center mt2 xsmt8 padding0">
<a href="<?php echo site_url('form-settings')?>" class="blank-btn" title="Back">Back</a> &nbsp;
</div>
</div>
</div>
<script>
$('body').delegate(".selectTemplate", 'click', function() {

	var template_id = $(this).attr('data-id');
	$('#template_input').val(template_id);
	$('#templateform').submit();
	
});

/*-------------------------- error message starts here---------------------------------------------------*/
<?php if(validation_errors()!='' || $error!='') { ?>
new PNotify({
	  title: 'Error',
	  text: <?php echo json_encode(validation_errors())?>,
	  type: 'error'
	});
<?php } ?>
/*-------------------------- error message ends here---------------------------------------------------*/
</script>