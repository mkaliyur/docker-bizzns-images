<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padding15 footer-height">
<!-- dash header-->
<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 mb2 xsmb4">
<div class="dash-header">
<div class="row">
<div class="col-md-12 col-sm-12"><h1 class="smart-color em22 smem18 xsem16">Forms</h1>
<p class="em11 smem10 xsem10">Create, manage and use forms for your subscribers easily</p></div>
<!--<div class="col-md-5 col-sm-5 xstext-right xstext-center mt2 xsmt4 mb0 xsmb2">
<a href="#" class="smart-btn">Create Forms</a></div> -->
</div>
</div>
</div>
<!-- dash header end-->

<div class="clearfix"></div>
<div class="col-md-12 col-sm-12 col-xs-12 mb2 xsmb4">
<form action="" method="post" >
<div class="layout-box1 clearfix xsmt4 mt1">
<div class="bs-wizard">
<div class="col-xs-3 bs-wizard-step complete">
<div class="progress"><div class="progress-bar"></div></div>
<a href="<?php echo site_url('form-settings')?>" class="bs-wizard-dot"></a>
<div class="text-center em8 smem9 xsem8 hidden-xs">setting</div>
</div>
<div class="col-xs-3 bs-wizard-step <?php if($form_session['step2']!='yes') { ?> disabled <?php } ?>">
<div class="progress"><div class="progress-bar"></div></div>
<a href="<?php if($form_session['step2']=='yes') { echo site_url('form-template'); } else { echo 'javascript:'; } ?>" class="bs-wizard-dot"></a>
<div class="text-center em8 smem9 xsem8 hidden-xs">Templates</div>
</div>
           
<div class="col-xs-3 bs-wizard-step <?php if($form_session['step3']!='yes') { ?> disabled <?php } ?>">
<div class="progress"><div class="progress-bar"></div></div>
<a href="<?php if($form_session['step3']=='yes') { echo site_url('form-editor'); } else { echo 'javascript:'; } ?>" class="bs-wizard-dot"></a>
<div class="text-center em8 smem9 xsem8 hidden-xs">Editor</div>
</div>
<div class="col-xs-3 bs-wizard-step <?php if($form_session['step4']!='yes') { ?> disabled <?php } ?>">
<div class="progress"><div class="progress-bar"></div></div>
<a href="jjavascript:" class="bs-wizard-dot"></a>
<div class="text-center em8 smem9 xsem8 hidden-xs">Publish</div>
</div>
</div>
<div class="col-md-12 mt1 xsmt15 padding0">
<div class="box-padding smart-field">
<div class="col-xs-12 xsmb2 mb0 padding0 xsmt2">
<div class="row">
<div class="col-xs-12 mt0 mb2 xsmb3 w600 em11 smem11 xsem11">Form Settings</div>
<div class="col-md-2 col-sm-3 col-xs-12 xsmb2 cloum-width mtt1">Form Name</div>
<div class="col-md-3 col-sm-5 col-xs-12">
<input name="form_name" type="text" placeholder="Enter Form Name" value="<?php echo $form_name?>"></div>
</div>
</div>
</div>
<div class="clearfix"></div>
<div class="table-responsive table-data table-fixed-header mt1">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
 <thead> <tr class="w500">
    <th class="text-center table-header-checkbox"><input name="" type="checkbox" value="" id="select_all"/></th>
    <th width="47%">Campaign Name</th>
    <th width="47%" class="text-center">Subscribers</th>
     </tr></thead>
<tbody> <?php foreach($campaign_list as $valCamp)	  { ?>
 <?php $contactCount = $this->common_model->getCountAllFromAnyTable('campaign_id',$valCamp->campaign_id,'tbl_contact'); ?>
  <tr class="text-center">
    <td class="table-header-checkbox"><input name="campaign_id[]" type="checkbox" value="<?php echo $valCamp->campaign_id?>" class="select_checkbox" <?php if(in_array($valCamp->campaign_id,$campaign_id)) { ?> checked="checked" <?php } ?> /></td>
    <td width="47%" class="text-left more-word1"><?php echo $valCamp->campaign_name?></td>
    <td width="47%"><?php echo $contactCount?></td>
  </tr>
 <?php } ?>
</tbody>
</table>
</div></div>
</div>

<div class="col-md-12 col-sm-12 col-xs-12 xstext-right xstext-center mt2 xsmt8 padding0">
<a href="<?php echo site_url('forms');?>" class="blank-btn" title="Back">Back</a>
&nbsp;
<button type="submit" class="smart-btn" title="Next">Next </button></div>


</form>
</div>
</div>
<script>
/*-------------------------- check/uncheck multiple campaign starts here ------------------------------------*/
$('body').delegate("#select_all", 'change', function() {
      $(".select_checkbox").prop('checked', $(this).prop("checked"));
	  $(".select_checkbox").change();
      });
$(".select_checkbox").change(function(){
    if ($('.select_checkbox:checked').length != $('.select_checkbox').length) {
       $("#select_all").prop('checked', false);
       
    }else{
        $("#select_all").prop('checked', true);
    }
});
/*-------------------------- check/uncheck multiple campaign ends here ------------------------------------*/

/*-------------------------- error message starts here---------------------------------------------------*/
<?php if(validation_errors()!='' || $error!='') { ?>
new PNotify({
	  title: 'Error',
	  text: <?php echo json_encode(validation_errors())?>,
	  type: 'error'
	});
<?php } ?>
/*-------------------------- error message ends here---------------------------------------------------*/
</script>