<!-------------------------------------------------Graph js Start------------------------------------------------->
<link href="<?php echo $this->config->item('templateAssetsPath')?>css/nv.d3.css" rel="stylesheet" type="text/css">
<script src="https://cdnjs.cloudflare.com/ajax/libs/d3/3.5.2/d3.min.js" charset="utf-8"></script>
<script src="<?php echo $this->config->item('templateAssetsPath')?>js/nv.d3.js"></script>
<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padding15">
  <!-- dash header-->
  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 mb2 xsmb4">
    <div class="dash-header">
      <h1 class="smart-color">Dashboard</h1>
      <p>Get a complete overview about your subscribers here   
	  
	  </p>
    </div>
  </div>
  <!-- dash header end-->
<div class="clearfix"></div>
<?php if($contact_info_msg) { ?>
<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
    <div class="alert smart-warning alert-dismissable">
    <div class="row">
    <div class="col-md-11 col-sm-11 col-xs-12"> <strong>Warning!</strong> <?php echo $contact_info_msg?></div>
    <div class="col-md-1 col-sm-1 col-xs-2 hidden-xs"> <a href="#" class="pull-right messsage-close" data-dismiss="alert" aria-label="close">×</a></div>
    </div>
    </div>
</div>
<?php } ?>
  
<div class="clearfix"></div>
  
  <div class="col-md-3 col-sm-3 mb2 xsmb4">
    <div class="layout-box3 text-center"> <img src="<?php echo $this->config->item('templateAssetsPath')?>images/campaign-icon.png" class="img-responsive center-block" alt="Create Campaign" /> <a href="<?php echo site_url().'campaign-list?camp_list_direct=1' ?>" class="dash-btn mt8 xsmt8 smem8 em10" title="Create Campaign">Create Campaign</a> </div>
  </div>
  <div class="col-md-3 col-sm-3 mb2 xsmb4">
    <div class="layout-box3 text-center"> <img src="<?php echo $this->config->item('templateAssetsPath')?>images/subscriber-icon.png" class="img-responsive center-block" alt="Create Subscriber" /> <a href="<?php echo site_url('add-contact'); ?>" class="dash-btn mt8 xsmt8 smem8 em10" title="Add Subscriber">Add Subscriber</a> </div>
  </div>
  <div class="col-md-3 col-sm-3 mb2 xsmb4 text-center">
    <div class="layout-box3"> <img src="<?php echo $this->config->item('templateAssetsPath')?>images/message-icon.png" class="img-responsive center-block" alt="Create Message" /> <a href="<?php echo site_url('add-newsletter?add_type=new'); ?>" class="dash-btn mt8 xsmt8 smem8 em10" title="Create Message">Create Message</a> </div>
  </div>
  <div class="col-md-3 col-sm-3 mb2 xsmb4 text-center">
    <div class="layout-box3"> <img src="<?php echo $this->config->item('templateAssetsPath')?>images/fourm-icon.png" class="img-responsive center-block" alt="Create Form" /> <a href="<?php echo site_url('form-settings?add_type=new'); ?>" class="dash-btn mt8 xsmt8 smem8 em10" title="Create Form">Create Form</a> </div>
  </div>
  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 mb2 xsmb4">
    <div class="layout-box2 clearfix">
      <div class="col-md-3 col-sm-3 col-xs-12 right-border text-center bottom-border">
        <div class="col-md-4 col-sm-12 col-xs-3 mt1 xsmt3 padding0"> <i class="icon-Subscribers-Today icon-round em25 smem18 xsem18 smart-color" aria-hidden="true"></i> </div>
        <div class="col-md-8 col-sm-12 col-xs-9 text-left xsmb2">
          <h1 class=" roboto em25 w300 mb0 xsmb0 stats_count" id="today_contacts">
            <?php echo $today_contacts?>
          </h1>
          <span class="em10 smem10 xsem10">Subscribers Today</span></div>
      </div>
      <div class="col-md-3 col-sm-3 col-xs-12 right-border bottom-border text-center">
        <div class="col-md-4 col-sm-12 col-xs-3 mt1 xsmt3 padding0"> <i class="icon-Subscribe-This-Month icon-round em25 smem18 xsem18 smart-blue" aria-hidden="true"></i> </div>
        <div class="col-md-8 col-sm-12 col-xs-9 text-left xsmb2">
          <h1 class=" roboto em25 w300 mb0 xsmb0 stats_count" id="contact_month">
            <?php echo $contact_month?>
          </h1>
          <span class="em10 smem10 xsem10">Subscribe This Month</span></div>
      </div>
      <div class="col-md-3 col-sm-3 col-xs-12 right-border bottom-border text-center">
        <div class="col-md-4 col-sm-12 col-xs-3 mt1 xsmt3 padding0"> <i class="icon-Unsubscribed-Today icon-round em25 smem18 xsem18 smart-red" aria-hidden="true"></i> </div>
        <div class="col-md-8 col-sm-12 col-xs-9 text-left xsmb2">
          <h1 class=" roboto em25 w300 mb0 xsmb0 stats_count" id="today_unsubscribe">
            <?php echo $today_unsubscribe?>
          </h1>
          <span class="em10 smem10 xsem10">Unsubscribed Today</span></div>
      </div>
      <div class="col-md-3 col-sm-3 col-xs-12 text-center bottom-border">
        <div class="col-md-4 col-sm-12 col-xs-3 mt1 xsmt3 padding0"> <i class="icon-Total-Unsubscribers icon-round em25 smem18 xsem18 smart-purple" aria-hidden="true"></i> </div>
        <div class="col-md-8 col-sm-12 col-xs-9 text-left xsmb2 ">
          <h1 class=" roboto em25 w300 mb0 xsmb0 stats_count" id="total_contacts">
            <?php echo $total_contacts?>
          </h1>
          <span class="em10 smem10 xsem10">Total Subscribers</span></div>
      </div>
    </div>
  </div>
  <div class="col-md-6 col-sm-6 col-xs-12 mb2 xsmb4">
    <div class="layout-box2">
      <!-- heading-->
      <div class="w600 pull-left em10 smem10 xsem9">Subscriber Meter</div>
      <div class="pull-right">
        <div class="radio radio-inline">
          <input type="radio" id="inlineRadio1" value="day" name="radioInline" class="subscriber_input" data-graph_type="subscriber">
          <label for="inlineRadio1" class="em9 smem9 xsem9"> Day </label>
        </div>
        <div class="radio radio-inline">
          <input type="radio" id="inlineRadio2" value="month" name="radioInline" class="subscriber_input" data-graph_type="subscriber">
          <label for="inlineRadio2" class="em9 smem9 xsem9"> Month </label>
        </div>
        <div class="radio radio-inline">
          <input type="radio" id="inlineRadio3" value="year" name="radioInline" class="subscriber_input" data-graph_type="subscriber">
          <label for="inlineRadio3" class="em9 smem9 xsem9"> Year </label>
        </div>
      </div>
      <div class="clearfix"></div>
      <hr />
      <!-- heading-->
      <div class="subscriberGDiv"></div>
    </div>
  </div>
  <div class="col-md-6 col-sm-6 col-xs-12 mb2 xsmb4">
    <div class="layout-box2 clearfix">
      <!-- heading-->
      <div class="w600 pull-left em10 smem10 xsem9">Unsubscribe Meter</div>
      <div class="pull-right">
        <div class="radio radio-inline">
          <input type="radio" id="inlineRadio1UN" value="day" name="radioInline1" class="subscriber_input" data-graph_type="unsubscribe">
          <label for="inlineRadio1UN" class="em9 smem9 xsem9"> Day </label>
        </div>
        <div class="radio radio-inline">
          <input type="radio" id="inlineRadio2UN" value="month" name="radioInline1" class="subscriber_input" data-graph_type="unsubscribe">
          <label for="inlineRadio2UN" class="em9 smem9 xsem9"> Month </label>
        </div>
        <div class="radio radio-inline">
          <input type="radio" id="inlineRadio3UN" value="year" name="radioInline1" class="subscriber_input" data-graph_type="unsubscribe">
          <label for="inlineRadio3UN" class="em9 smem9 xsem9"> Year </label>
        </div>
      </div>
      <div class="clearfix"></div>
      <hr />
      <div class="clearfix"></div>
      <!-- heading-->
      <div class="unsubscribeGDiv"></div>
    </div>
  </div>
  <div class="col-md-12 col-sm-12 col-xs-12 mb2 xsmb4">
    <div class="line-bg clearfix">
      <div class="col-lg-offset-2 col-lg-6 col-md-offset-1 col-md-6 col-sm-offset-1 col-sm-7 col-xs-offset-0 col-xs-12 smart-white">
        <h3 class="smart-white"><?php echo $this->upgrade_planname?></h3>
        Click here to upgrade to <?php echo $this->upgrade_planname?> version </div>
      <div class="col-lg-3 col-md-5 col-sm-4 col-xs-12 xsmt1"><a href="<?php echo $this->upgrade_link?>" title="Upgrade To Pro" target="_blank">Upgrade To <?php echo $this->upgrade_planname?></a></div>
    </div>
  </div>
 
</div>
<script>
/*-------------------------- graph starts here ------------------------------------*/
$('body').delegate(".subscriber_input", 'click', function() {

    var time_type = $(this).val();
	var graph_type = $(this).attr('data-graph_type');
	var posturl = site_url+'get-dashboard-graph';
	if(graph_type=='subscriber')
	var divname = '.subscriberGDiv';
	else
	var divname = '.unsubscribeGDiv';

    $.ajax({
        url: posturl,
        dataType: 'json',
        type: "POST",
        data: {time_type: time_type,graph_type:graph_type},
		beforeSend: function() { $(divname).html('<img src="<?php echo $this->config->item("templateAssetsPath")?>images/loader.gif" class="center-block big-loader" />'); },
        success: function(data) {
                $(divname).html('');
				$(divname).html(data.html);
        },
    });
});
$( document ).ready(function() { 
  $('#inlineRadio1').click();
  $('#inlineRadio1UN').click();
  $('#today_contacts').click();
  $('#contact_month').click();
  $('#today_unsubscribe').click();
  $('#total_contacts').click();
});
/*-------------------------- graph ends here ------------------------------------*/

/*-------------------------- stats Count starts here ------------------------------------*/
$('body').delegate(".stats_count", 'click', function() {

    var stats_type = $(this).attr('id');
	var posturl = site_url+'get-dashboard-stats-count';

    $.ajax({
        url: posturl,
        dataType: 'json',
        type: "POST",
        data: {stats_type: stats_type},
		beforeSend: function() { $('#'+stats_type).html('<img src="<?php echo $this->config->item("templateAssetsPath")?>images/loader.gif" class="small-loader" />'); },
        success: function(data) {
                  $('#'+stats_type).html('');
				  $('#'+stats_type).html(data.count_data);
        },
    });
});
/*-------------------------- stats Count ends here ------------------------------------*/


$(document).ready(function(){
	/* Counter Js Function */
	$('.count').each(function () {
		$(this).prop('Counter',0).animate({
			Counter: $(this).text()
		}, {
			duration: 500,
			easing: 'swing',
			step: function (now) {
				$(this).text(Math.ceil(now));
			}
		});
	});
	/* End Counter Js Function */
	
	/* Graph Resize Hack Script */
	$(".navbar-expand-toggle").click(function(){
		setTimeout(function(){ 
			if($(document).width() > 767){
				//window.dispatchEvent(new Event('resize'));
				
				$('input[name=radioInline]:checked').click();
				$('input[name=radioInline1]:checked').click();
			}
		}, 500);
	});
	/* Graph Resize Hack Script */
	
});
/*-------------------------- success message starts here---------------------------------------------------*/
<?php if($this->session->userdata('success_msg')) { ?>
new PNotify({
	  title: 'Success',
	  text: '<?php echo $this->session->userdata('success_msg')?>',
	  type: 'success'
	});
<?php $this->session->unset_userdata('success_msg'); } ?>
/*-------------------------- success message ends here---------------------------------------------------*/
</script>
