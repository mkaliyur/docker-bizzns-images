<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>
<?php echo $meta_title;?>
</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<!-- Tell the browser to be responsive to screen width -->
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
<!-- fonts-->
<link rel="stylesheet" href="<?php echo $this->config->item('templateAssetsPath')?>css/font-awesome.min.css" type="text/css" />
<link href='https://fonts.googleapis.com/css?family=Roboto:300,400,500,600,800' rel='stylesheet' type='text/css'>
<link href='https://fonts.googleapis.com/css?family=Raleway:400,500,600,800' rel='stylesheet' type='text/css'>
<link rel="icon" href="<?php echo $this->config->item('templateAssetsPath')?>images/favicon-icon.png" type="image/png"/>
<link rel="stylesheet" href="<?php echo $this->config->item('templateAssetsPath')?>css/bootstrap.min.css" type="text/css" />
<!-- left menu-->
<link rel="stylesheet" href="<?php echo $this->config->item('templateAssetsPath')?>css/left_menu.css" type="text/css" />
<!-- smart mailer design css-->
<link rel="stylesheet" href="<?php echo $this->config->item('templateAssetsPath')?>css/style.css" type="text/css" />
<!-- scroll css-->
<link rel="stylesheet" href="<?php echo $this->config->item('templateAssetsPath')?>css/CustomScrollbar.css" type="text/css" />
<script type='text/javascript' src="<?php echo $this->config->item('templateAssetsPath')?>js/jquery.min.js"></script>
<script type='text/javascript' src="<?php echo $this->config->item('templateAssetsPath')?>js/bootstrap.min.js"></script>
<!-- scroll js-->
<script type='text/javascript' src="<?php echo $this->config->item('templateAssetsPath')?>js/CustomScrollbar.min.js"></script>
<!-- left menu-->
<script type='text/javascript' src="<?php echo $this->config->item('templateAssetsPath')?>js/app.js"></script>
<!-- image lighbox css and js-->
<script>
var $gallery;
</script>
<script type='text/javascript' src="<?php echo $this->config->item('templateAssetsPath')?>js/simple-lightbox.js"></script>
<script src="<?php echo $this->config->item('templateAssetsPath')?>js/jquery.form.js"></script>
<script src="<?php echo $this->config->item('templateAssetsPath')?>js/modernizr.js"></script>
<!--[if lt IE 9]>
<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
<![endif]-->
<script>
var site_url = '<?php echo site_url()?>';
</script>
<!--- hide show div-->
<script type="text/javascript">
$( document ).ready(function() {
	$( "#mobile-menu-top" ).hide();
	$( "#opendiv" ).click(function() {
	$( "#mobile-menu-top" ).show();
});
	$( "#closediv" ).click(function() {
		$( "#mobile-menu-top" ).hide();
	});
});
</script>
<!--- hide show div end-->
</head>
<body>
<div class="myDropdown back_overlay"></div>
<div class="app-container">
  <div class="row content-container">
    <nav class="navbar navbar-default navbar-fixed-top">
      <div class="container-fluid">
        <div class="navbar-header">
          <button type="button" class="navbar-expand-toggle em14 smem14 xsem14"> </button>
          <div class="applogo"><a href="<?php echo site_url('dashboard')?>"><img src="<?php echo $this->config->item('templateAssetsPath')?>images/smart-logo.png" class="img-responsive"></a></div>
          <!----business name start -->
          <div class="col-xs-12 desktop-business-name text-center em15 smem15 xsem15 hidden-xs more-word">
            <?php echo $this->session->userdata('business_name');?>
          </div>
          <!----business name start-->
          <div class="accountlinks">
            <!-- Notification dropdown-->
            <ul class="inline margin0">
              <!-- question mark icon-->
              <li class="dropdown hidden-xs"> <a target="_blank" href="<?=site_url('faq')?>" class=" em20 mdem20 smem20 xsem20 questionmark" > <i class="fa fa-question em6 mdem6 smem6 xsem6" aria-hidden="true"></i> </a></li>
              <li class="dropdown visible-xs"> <a href="#" id="opendiv" class=" em20 mdem20 smem20 xsem20 questionmark"> <i class="icon-More em6 mdem6 smem6 xsem6" aria-hidden="true"></i> </a></li>
            </ul>
          </div>
          <!----profile dropdown -->
          <ul class="navbar-right inline padding00">
            <li class="dropdown"> <a href="#" class="dropdown-toggle padding00" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"> <img src="<?php if($this->session->userdata('profile_pic')){echo  site_url('assets/uploads/pics/profile/'.$this->session->userdata('profile_pic')); }else{echo $this->config->item('templateAssetsPath').'images/avatar.png'; }?> " class="img-responsive profile"></a>
              <ul class="dropdown-menu">
                <li><a href="<?php echo site_url('settings/user')?>" class="clearfix" >
                  <div class="col-md-2 col-sm-2 col-xs-2"><i class="fa fa-user" aria-hidden="true"></i></div>
                  <div class="col-md-10 col-sm-10 col-xs-9">Profile</div>
                  </a></li>
				 <li><a href="<?php echo site_url('update-log')?>" class="clearfix" >
                  <div class="col-md-2 col-sm-2 col-xs-2"><i class="fa fa-refresh" aria-hidden="true"></i></div>
                  <div class="col-md-10 col-sm-10 col-xs-9">Update Log</div>
                  </a></li> 
                <li><a href="<?php echo site_url('logout')?>" class="clearfix">
                  <div class="col-md-2 col-sm-2 col-xs-2 "><i class="fa fa-power-off" aria-hidden="true"></i></div>
                  <div class="col-md-10 col-sm-10 col-xs-9">Logout</div>
                  </a></li>
              </ul>
            </li>
          </ul>
        </div>
      </div>
    </nav>
	
    <!--- header global file--->
    <?php echo $this->load->view($this->config->item('templateName').'/left_menu')?>
    <!--- header global file--->
  </div>
  <!--  mobile-menu-top div start-->
  <div id="mobile-menu-top">
    <div class="row">
      <div class="col-xs-12 business-name text-center mobile-view-icon em13 smem13 xsem13"> <span class="business-name1">
        <?php echo $this->session->userdata('business_name');?> 
        </span>
        <ul class="pull-right">
          <li><a target="_blank" href="http://bizomart.kayako.com/"><i class="fa fa-question" aria-hidden="true"></i></a> </li>
          <li> <a href="javascript:void(0)" id="closediv">X</a></li>
        </ul>
      </div>
    </div>
  </div>
  <!-- mobile-menu-top div end -->
</div>



<div class="app-container">
<div class="side-body">
<div id="preloader" style="display:none"></div>
<!-- PNotify -->
<script type="text/javascript" src="<?php echo $this->config->item('templateAssetsPath')?>js/notify/pnotify.core.js"></script>
<script type="text/javascript" src="<?php echo $this->config->item('templateAssetsPath')?>js/notify/pnotify.buttons.js"></script>
