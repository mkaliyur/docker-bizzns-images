<html>
<head>

<script src="https://ajax.googleapis.com/ajax/libs/webfont/1.4.7/webfont.js"></script>
<script>
  // WebFont.load({
    // google: {
      // families: ['Alegreya', 'Alegreya Sans', 'Archivo Narrow', 'Arial', 'BioRhyme', 'Chivo','Cormorant Garamond', 'Crimson Text', 'Domine', 'Eczar', 'Georgia', 'Fira Sans', 'Handlee', 'Inconsolata', 'Lato', 'Libre Franklin', 'Lobster', 'Lora', 'Karla', 'Montserrat', 'Source Sans Pro', 'Neuton', 'Old Standard TT', 'Open Sans', 'Playfair Display', 'Poppins', 'Raleway', 'Roboto', 'Roboto Slab', 'Rubik', 'Space Mono', 'Source Sans Pro', 'Source Serif Pro', 'Times New Roman', 'Varela Round', 'Work Sans']
    // }
  // });
</script>

<link rel="stylesheet" href="<?php echo $this->config->item('templateAssetsPath')?>css/font-awesome.min.css" type="text/css" />
<script src="<?php echo  $this->config->item('templateAssetsPath').'editor/';?>js/jquery.min.js"></script>
<script src="<?php echo  $this->config->item('templateAssetsPath').'editor/';?>js/jquery-ui.js"></script>
<link rel="stylesheet" href="<?php echo  $this->config->item('templateAssetsPath').'editor/';?>css/jquery-ui.css" />
<!-- PNotify -->
<script type="text/javascript" src="<?php echo $this->config->item('templateAssetsPath')?>js/notify/pnotify.core.js"></script>
<script type="text/javascript" src="<?php echo $this->config->item('templateAssetsPath')?>js/notify/pnotify.buttons.js"></script>

<!--<link href="https://fonts.googleapis.com/css?family=Alegreya:400,300,500,700|Alegreya Sans:400,300,500,700|Archivo Narrow:400,300,500,700|Arial:400,300,500,700|BioRhyme:400,300,500,700|Chivo:400,300,500,700|Cormorant Garamond:400,300,500,700|Crimson Text:400,300,500,700|Domine:400,300,500,700|Eczar:400,300,500,700|Georgia:400,300,500,700|Fira Sans:400,300,500,700|Handlee:400,300,500,700|Inconsolata:400,300,500,700|Lato:400,300,500,700|Libre Franklin:400,300,500,700|Lobster:400,300,500,700|Lora:400,300,500,700|Karla:400,300,500,700|Montserrat:400,300,500,700|Source Sans Pro:400,300,500,700|Neuton:400,300,500,700|Old Standard TT:400,300,500,700|Open Sans:400,300,500,700|Playfair Display:400,300,500,700|Poppins:400,300,500,700|Raleway:400,300,500,700|Roboto:400,300,500,700|Roboto Slab:400,300,500,700|Rubik:400,300,500,700|Space Mono:400,300,500,700|Source Sans Pro:400,300,500,700|Source Serif Pro:400,300,500,700|Times New Roman:400,300,500,700|Varela Round:400,300,500,700|Work Sans:400,300,500,700" type="text/css" rel="stylesheet">

Georgia
Times New Roman
-->


<link href="https://fonts.googleapis.com/css?family=Alegreya:400,300,500,700|Alegreya Sans:400,300,500,700|Archivo Narrow:400,300,500,700|Arial:400,300,500,700|BioRhyme:400,300,500,700|Chivo:400,300,500,700|Cormorant Garamond:400,300,500,700|Crimson Text:400,300,500,700|Domine:400,300,500,700|Eczar:400,300,500,700|Fira Sans:400,300,500,700|Handlee:400,300,500,700|Inconsolata:400,300,500,700|Lato:400,300,500,700|Libre Franklin:400,300,500,700|Lobster:400,300,500,700|Lora:400,300,500,700|Karla:400,300,500,700|Montserrat:400,300,500,700|Source Sans Pro:400,300,500,700|Neuton:400,300,500,700|Old Standard TT:400,300,500,700|Open Sans:400,300,500,700|Playfair Display:400,300,500,700|Poppins:400,300,500,700|Raleway:400,300,500,700|Roboto:400,300,500,700|Roboto Slab:400,300,500,700|Rubik:400,300,500,700|Space Mono:400,300,500,700|Source Sans Pro:400,300,500,700|Source Serif Pro:400,300,500,700|Varela Round:400,300,500,700|Work Sans:400,300,500,700" type="text/css" rel="stylesheet">


<script type='text/javascript'>
$(window).load(function(){
$('input[type="range"]').change(function () {
    var val = ($(this).val() - $(this).attr('min')) / ($(this).attr('max') - $(this).attr('min'));
    $(this).css('background-image',
                '-webkit-gradient(linear, left top, right top, '
                + 'color-stop(' + val + ', #749ced), '
                + 'color-stop(' + val + ', #a8a8a8)'
                + ')'
                );
});
});
</script>

<script>
//To make spans with inline css
//document.execCommand("styleWithCSS", true, null);
$(document).on("click",".buttonoptions",function() {
	$(".list-options").hide();
	$(".font-options").hide();
	$(".align-options").hide();
	$(".style-options").hide();
	$(".media-options").hide();
	$(".personalize-options").hide();
	$(".personalizeoptions").removeClass("is-open");
	$(".mediaoptions").removeClass("is-open");
	$(".listoptions").removeClass("is-open");
	$(".fontoptions").removeClass("is-open");
	$(".alignoptions").removeClass("is-open");
	$(".styleoptions").removeClass("is-open");
	$(".button-options").toggle();
    $(".buttonoptions").toggleClass("is-open");
});
$(document).on("click",".alignoptions",function() {
	$(".list-options").hide();
	$(".font-options").hide();
	$(".button-options").hide();
	$(".style-options").hide();
	$(".media-options").hide();
	$(".personalize-options").hide();
	$(".personalizeoptions").removeClass("is-open");
	$(".mediaoptions").removeClass("is-open");
	$(".listoptions").removeClass("is-open");
	$(".fontoptions").removeClass("is-open");
	$(".buttonoptions").removeClass("is-open");
	$(".styleoptions").removeClass("is-open");
	$(".align-options").toggle();
    $(".alignoptions").toggleClass("is-open");
});
$(document).on("click",".listoptions",function() {
	$(".align-options").hide();
	$(".font-options").hide();
	$(".button-options").hide();
	$(".style-options").hide();
	$(".media-options").hide();
	$(".personalize-options").hide();
	$(".personalizeoptions").removeClass("is-open");
	$(".mediaoptions").removeClass("is-open");
	$(".alignoptions").removeClass("is-open");
	$(".fontoptions").removeClass("is-open");
	$(".buttonoptions").removeClass("is-open");
	$(".styleoptions").removeClass("is-open");
	$(".list-options").toggle();
    $(".listoptions").toggleClass("is-open");
});
$(document).on("click",".fontoptions",function() {
	$(".align-options").hide();
	$(".list-options").hide();
	$(".button-options").hide();
	$(".style-options").hide();
	$(".media-options").hide();
	$(".personalize-options").hide();
	$(".personalizeoptions").removeClass("is-open");
	$(".mediaoptions").removeClass("is-open");
	$(".alignoptions").removeClass("is-open");
	$(".listoptions").removeClass("is-open");
	$(".buttonoptions").removeClass("is-open");
	$(".styleoptions").removeClass("is-open");
	$(".font-options").toggle();
    $(".fontoptions").toggleClass("is-open");
});
$(document).on("click",".styleoptions",function() {
	$(".align-options").hide();
	$(".list-options").hide();
	$(".button-options").hide();
	$(".font-options").hide();
	$(".media-options").hide();
	$(".personalize-options").hide();
	$(".personalizeoptions").removeClass("is-open");
	$(".mediaoptions").removeClass("is-open");
	$(".alignoptions").removeClass("is-open");
	$(".listoptions").removeClass("is-open");
	$(".buttonoptions").removeClass("is-open");
	$(".fontoptions").removeClass("is-open");
	$(".style-options").toggle();
    $(".styleoptions").toggleClass("is-open");
});
$(document).on("click",".mediaoptions",function() {
	$(".align-options").hide();
	$(".list-options").hide();
	$(".button-options").hide();
	$(".font-options").hide();
	$(".style-options").hide();
	$(".personalize-options").hide();
	$(".personalizeoptions").removeClass("is-open");
	$(".alignoptions").removeClass("is-open");
	$(".listoptions").removeClass("is-open");
	$(".buttonoptions").removeClass("is-open");
	$(".fontoptions").removeClass("is-open");
	$(".styleoptions").removeClass("is-open");
	$(".media-options").toggle();
    $(".mediaoptions").toggleClass("is-open");
});
$(document).on("click",".personalizeoptions",function() {
	$(".align-options").hide();
	$(".list-options").hide();
	$(".button-options").hide();
	$(".font-options").hide();
	$(".style-options").hide();
	$(".media-options").hide();
	$(".mediaoptions").removeClass("is-open");
	$(".alignoptions").removeClass("is-open");
	$(".listoptions").removeClass("is-open");
	$(".buttonoptions").removeClass("is-open");
	$(".fontoptions").removeClass("is-open");
	$(".styleoptions").removeClass("is-open");
	$(".personalize-options").toggle();
    $(".personalizeoptions").toggleClass("is-open");
});
/*$(document).on("click",".customurl",function() {
	alert("functioncalled");
	$('.align-options').hide();
	$('.list-options').hide();
	$(".font-options").hide();
});*/
$(document).on("click",".g-body",function(event) {
	event.preventDefault();
      event.stopPropagation();
});

//is used to get select editing element;
var EditingElement;

$(document).on("click",".edit_template",function(event) {
	target = event.target || event.srcElement
	if($(target).closest("#ImageEditor").length==0 && $(target).attr('id') != "tabs-3") {
		EditingElement = event.target || event.srcElement;
		//EditingElement = document.activeElement;
	}
});

$(document).on("click",function(event) {
	resetedit();
});

function resetedit(){
	var focusElement = EditingElement;
	if (EditingElement.nodeType == 3) {
		focusElement = EditingElement.parentElement;
	}
	
	//console.log(hexcolor($(focusElement).css('background-color')));
	$(".fontsize").val(parseInt($(focusElement).css('font-size')));
	$(".fontspace").val(parseInt($(focusElement).css('line-height'))/parseInt($(focusElement).css('font-size')));
	$(".buttonradius").val(parseInt($(focusElement).css('borderBottomLeftRadius')));
	$(".textColor").css("background",$(focusElement).css('color'));
	$(".buttonbgcolor").css("background",$(focusElement).css('background-color'));

}
$(document).on("change","#imgBorderColorvalue",function(event) {
	$(EditingElement).css({"border-color": "#"+$("#imgBorderColorvalue").val()});
	parent.mobielPreviewUpdate(getTemplate());
});
$(document).on("click",".imgBorderColorvalue",function(event) {
	if($(EditingElement).outerWidth() == $(EditingElement).width()){
		$(EditingElement).css({"border-style": "solid"});
		$(EditingElement).css({"border-width": "1px"});
	}
	$(EditingElement).css({"border-color": "#"+$("#imgBorderColorvalue").val()});
	parent.mobielPreviewUpdate(getTemplate());
});
$(document).on("change",".img-border-width",function(event) {
	console.log(EditingElement);
	$(EditingElement).css({"border-style": "solid"});
	$(EditingElement).css({"border-width": $(".img-border-width").val() + "px"});
	parent.mobielPreviewUpdate(getTemplate());
});
$(document).on("change",".img-border-radius",function(event) {
	$(EditingElement).css({"border-style": "solid"});
	if($(EditingElement).outerWidth() == $(EditingElement).width()){
		$(EditingElement).css({"border-style": "solid"});
		$(EditingElement).css({"border-width": "1px"});
	}
	$(EditingElement).css({"border-radius": $(".img-border-radius").val() + "%"});
	parent.mobielPreviewUpdate(getTemplate());
});
$(document).on("click",".delete_img",function(event) {
	$(EditingElement).remove();
	parent.mobielPreviewUpdate(getTemplate());
});
</script>
<script>
function buttonupdate(jscolor) {
// 'jscolor' instance can be used as a string
document.getElementById('rect').style.backgroundColor = '#' + jscolor
}
/*function textcolor(jscolor) {
console.log(jscolor);
    document.execCommand('foreColor',false,jscolor.value);
}
function hilitecolor(jscolor) {
    document.execCommand( 'hiliteColor',false,jscolor);
}*/
</script>
<link href="<?php echo  $this->config->item('templateAssetsPath').'editor/';?>css/editor.css" rel="stylesheet">
<link href="<?php echo  $this->config->item('templateAssetsPath').'editor/';?>css/menu.css" rel="stylesheet">
<link href="<?php echo  $this->config->item('templateAssetsPath').'editor/';?>css/CustomScroll.css" rel="stylesheet">
<!--<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.6.3/css/font-awesome.min.css">-->
<script src="<?php echo  $this->config->item('templateAssetsPath').'editor/';?>js/jscolor.js"></script>
<script src="<?php echo  $this->config->item('templateAssetsPath').'editor/';?>js/CustomScrollbar.min.js"></script>
</head>
<body style="margin:0;font-family:roboto;background-color:#e9e9e9;">
<div style="width:600px; margin:0 auto;"> 
<div class="edit_template">
<?php echo $template_text; ?>	
</div>
</div>
<script type="text/javascript" src="<?php echo  $this->config->item('templateAssetsPath').'editor/';?>js/inline-editor.js"></script>
<script type="text/javascript">
lpb_inline_editor.bind(document.querySelectorAll("editarea"));
</script>
<script>
function getTemplate(){
	return $(".edit_template").html();
}

$(document).ready(function(){
	$(".editblock").removeClass("editblock");
	
	 var editable_elements = document.querySelectorAll("[contenteditable=false]");
    $(editable_elements).attr("contentEditable", true);
	 
	// $(document).find("[contenteditable='true']").each(function() {
		// $(this).removeAttr("contenteditable");
	// });
	
	// $(document).on("click","a",function(event) {
		// event.preventDefault();
	// });

});

$(document).on("click",".delete-img",function() {
    var r = confirm("Are you sure you want to delete this Image?")
    if(r == true)
    {
		$this = $(this);
        $.ajax({
          url: '<?php echo site_url()?>form-editor',
          data: {
			  'file' : $(this).prevAll('img').first().attr("src"),
			  "action" : "deleteImage"
		  },
          success: function (response) {
             $this.closest(".editor-delete-icon").remove();
			 PNotify.removeAll();
			 new PNotify({title: 'Success',text: 'Image Deleted Successfully.',type: 'success'});
          },
          error: function () {
             //alert("hello1");
          }
        });
		
    }
});
</script>





<div class="img-menu active">
<div id="ImageEditor">

<div class="img-options">
<span class="no-overflow">
<span class="ui-inputs">

<button class="styleoptions tabs">Style</button>
<button class="mediaoptions tabs">Media</button>
<button class="tabs1 delete_img" title="Delete Image"><i class="icon-delete"></i></button>

<div class="style-options">
<div class="no-overflow">
<span class="ui-inputs">
<div class="popupbox">Border-Thickness : <input class="img-border-width img-range" min="0" max="10" step="1" type="range"></div>
<div class="popupbox">Border-Radius : <input class="img-border-radius img-range" min="0" max="50" step="1" type="range"></div>

<div class="popupbox">
<button class="imgBorderColorvalue colorbutton jscolor {valueElement:'imgBorderColorvalue'}"></button>
<input id="imgBorderColorvalue" value="ff6699" class="textColor" type="hidden">
</div>

</span>
</div>
</div>




<div class="media-options menu_scroll mCustomScrollbar mCustomScrollbar2" id="tabs-3">
<div class="mediaimg">
<?php 
if(!is_dir("./assets/media_image"))
{
	mkdir("./assets/media_image");
}
foreach(scandir("./assets/media_image") as $key=>$image){
	if($key > 1){
	echo '<div class="editor-delete-icon"><img alt="" src="'.site_url().'/assets/media_image/'.$image.'"><a href="javascript:" class="delete-img"><span class="fa fa-trash" aria-hidden="true" ></span></a></div>';
	}
}	
?>


</div>


<input id="fileupload" type="file" onChange="uploadimage(this.files[0])"/>
<label for="fileupload" class="custom-file-upload">
     Upload
</label>
</div>




</span>
</span>
</div>



</div>
</div>






<script>
$(function(){
	$("#ImageEditor").tabs();
});

function uploadimage(file){
	var formdata = new FormData();
	var imageType = ['jpeg', 'jpg', 'png', 'gif'];  
	if (-1 == $.inArray(file.type.split('/')[1], imageType)){
		alert("not valid");
		return false;
	}
	formdata.append('newmediaimg', file);
	var ajax = new XMLHttpRequest();
	ajax.addEventListener("load", appendimage, false);
	ajax.open("POST", "<?php echo  site_url().'uploadmediaimg'; ?>");
	ajax.send(formdata);
}
function appendimage(){
	console.log("dsa");
	$(".mediaimg").load(location.href + " .mediaimg");
}
	
</script>
<style>
.img-menu{
	position: absolute;
	left: 100%;
	top: 100%;
	width: 250px;
	display:none;
}
</style>
		
</body>
</html>