<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padding15 footer-height">
<!-- dash header-->
<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 mb2 xsmb4">
<div class="dash-header">
<div class="row">
<div class="col-md-7 col-sm-7"><h1 class="smart-color em22 smem18 xsem16">Updates</h1>
<p class="em11 smem10 xsem10">Here is the list of updates we've done</p></div>
<div class="col-md-5 col-sm-5 xstext-right xstext-center mt2 xsmt4 mb0 xsmb2">
<a href="javascript:"  class="smart-btn export-btn">Export Logs</a></div> </div>
</div>
</div>
<!-- dash header end-->

<div class="clearfix"></div>
<div class="col-md-12 col-sm-12 col-xs-12 mb2 xsmb4">
<div class="layout-box1 clearfix padding0 mailer-data">

<?php if($update_log_categories){
	foreach($update_log_categories as $cat){
	?>
<div class="box-padding xsmb10 mb2 clearfix">
<div class="mt1 mb2 xsmb3 update-title col-xs-12 padding0">
    <ul>
	<li class="w600 em13 smem12 xsem12">
		<?php echo $cat->title;?>	<span class="w500 lightgrey em9 smem9 xsem9"> &nbsp;&nbsp;(Updated On <?php echo date('d/m/Y',$cat->created);?> at <?php echo date('h:i A',$cat->created);?>) </span>
    </li>
    </ul>
</div>
<?php
		$d = $cat->id;
 		if($update_log->$d){
			foreach($update_log->$d as $log){
		?>
<div class="col-xs-12 clearfix">
<div class="row">
    <div class="col-md-9 col-sm-9 col-xs-12 update-details">
        <ul>
        
		<li class="w600 em11 smem11 xsem11">
        <?php echo $log->log_title;?> 
        <div class="w300 em9 smem9 xsem9 update-des mt1 xsmt3">
         <?php echo $log->description;?>
        </div>
        </li>
			
	    </ul>
    </div>
    <div class="col-md-3 col-sm-3 col-xs-12 text-center mt0 xsmt1">
        <i class="fa fa-check smart-green" aria-hidden="true"></i>
        &nbsp; <?php echo $log->type;?>
    </div>
</div>
</div>

 <?php }
			}?>
</div>

<hr />


<div class="clearfix"></div>
<?php }}?>


</div>

</div>
</div>
<script>
var export_url = '<?php echo site_url()."export-log";?>';
$(document).ready(function(){
	 $(".export-btn").click(function(){
		  window.location =export_url;
	 });
});
</script>