<!------------------------------------------- tagsinput js start ------------------------------------------------------->
<script type="text/javascript" src="<?php echo $this->config->item('templateAssetsPath')?>js/jquery.tagsinput.js"></script>
<script type="text/javascript">

		function onAddTag(tag) {
			alert("Added a tag: " + tag);
		}
		function onRemoveTag(tag) {
			alert("Removed a tag: " + tag);
		}

		function onChangeTag(input,tag) {
			alert("Changed a tag: " + tag);
		}

		$(function() {

			$('#tags_1').tagsInput({width:'auto'});
		
		$('input.tags').tagsInput({interactive:false});
		});

</script>
<!-------------------------------------------- tagsinput js end  -------------------------------------------------------->
<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padding15 footer-height">
<!-- dash header-->
<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 mb2 xsmb4">
<div class="dash-header">
<div class="row">
<div class="col-md-12 col-sm-12"><h1 class="smart-color">Suppression List</h1>
<p>Add subscribers here that you wish to exclude from your mailing list</p></div>
<!--<div class="col-md-5 col-sm-5 xstext-right xstext-center mt2 xsmt4 mb0 xsmb2">
<a href="#" class="smart-btn" data-toggle="modal" data-target="#myModal">Create Campaign</a></div> --></div>
</div>
</div>
<!-- dash header end-->

<div class="clearfix"></div>
<div class="col-md-12 smart-tabs clearfix">

<ul>
<li><a href="<?php echo site_url('add-contact')?>"  title="Add Subscriber">
<span class="hidden-xs">Add Subscriber</span><i class="icon-Add-Subscriber-Responsive visible-xs"></i>
</a></li>
<li><a href="<?php echo site_url('contact-list')?>" title="Subscriber Management">
<span class="hidden-xs">Subscriber Management</span><i class="icon-Subscriber-Management-Responsive visible-xs"></i>
</a></li>
<li><a href="<?php echo site_url('suppression-list')?>" class="active" title="Suppression List">
<span class="hidden-xs">Suppression List</span><i class="icon-Suppression-List-Responsive visible-xs"></i>
</a></li>
</ul>
</div>


<div class="col-md-12 col-sm-12 col-xs-12 mb2 xsmb4">
<div class="layout-box1 clearfix mailer-data">
<div class="box-padding">
<div class="col-xs-12 col-md-6 col-sm-6 mt1 mb2 xsmb3 w600 em11 smem11 xsem11 padding0">Manage Suppression List</div>
<div class="col-xs-7 col-md-6 col-sm-6 mb2 xsmb2 text-right campaign-search padding0  hidden-xs">
<a data-toggle="modal" data-target="#importPc" class="suppression-grey-btn">Import From PC</a> &nbsp;<a data-toggle="modal" data-target="#manuallyUpload" class="suppression-grey-btn">Manually</a>
</div></div>


<div class="clearfix"></div>
<?php if(sizeof($allRecord)!=0){ ?>
<div class="table-responsive table-multi-data table-fixed-header contentDiv">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<thead> 
  <tr>
    <th width="58%">Supperssion List Name</th>
    <th width="40%" class="text-center">Action</th>
  </tr></thead>
<tbody>   <?php  foreach($allRecord as $value) { ?>
  <tr class="text-center">
   <td width="60%" class="text-left more-word1"><?php echo $value->list_name?></td>
    <td width="40%" class="table-icon"><ul>
    <li><a href="<?php echo site_url('edit-suppression-list/'.$value->id)?>" title="Edit"><i class="fa fa-pencil-square-o em12" aria-hidden="true"></i></a></li>
    <li><a data-toggle="modal" data-target="#delete" class="deletelink" data-href="<?php echo site_url('delete-suppression-list/'.$value->id)?>" href="javascript:" title="Delete"><i class="fa fa-trash-o em12" aria-hidden="true"></i></a></li>
    </ul></td>
  </tr>
   <?php } 
?>
</tbody>
</table>
</div>
<?php }else{ ?>
<div class="clearfix"></div>
<div class="text-center mt6 xsmt6 xsmb4 mb4">
<i class="fa fa-file-text-o smart-grey em45 smem35 xsem30" aria-hidden="true"></i>
<h4 class="em11 smem11 xsem11">Suppression List Not Found</h4>
<p class="em9 smem9 xsem9">Please Click Import From PC Or Manually For Create Suppression List </p></div>
<?php 
    } ?>
</div>

</div>

<div class="col-xs-12 text-center visible-xs xsmt6 xsmb4">
<a data-toggle="modal" data-target="#importPc" class="suppression-grey-btn">From Gallery</a> &nbsp;<a data-toggle="modal" data-target="#manuallyUpload" class="suppression-grey-btn">Manually</a>
</div>


</div>



<!-- import from pc modalpopup starts here-->
<div id="importPc" class="modal fade" role="dialog">
  <div class="modal-dialog campaign-modal">
    <div class="modal-content">
      <div class="modal-body padding0 clearfix xsmb6 xsmt4 mb2">
       <div class="col-md-5 col-sm-5 col-xs-12 mt7 xsmt0 mb4">
       <div class="col-xs-12 bottom-border">
      
       <img src="<?php echo $this->config->item('templateAssetsPath')?>images/import-pc.png" class="img-responsive center-block mobile-img" /></div>
       </div>
	   <form action="<?php echo site_url('import-suppression-list')?>" method="post" class="addSuppImpForm" enctype="multipart/form-data">
       <div class="col-md-7 col-sm-7 col-xs-12 left-border campaign-field xsmt4 mt0 ">
        <div class="col-xs-12">
       <h3 class="em16 smem16 xsem14 hidden-xs">Import List From PC</h3><h3 class="em16 smem16 xsem14 visible-xs">Import List From Device</h3>
        <div class="form-group mt10 xsmt10">
		<label class="w300">Suppression List Name</label>
		<input type="text" name="list_name" placeholder="Suppression List Name" class="form-control" />
		</div>
       
		<div class="col-md-6 col-sm-6 col-xs-12 mt1 xsmt0 text-left padding0">
		<!--<label class="custom-file-upload"><input type="file" name="csv_file"/>Choose File</label> 
<input type="file" id="browse" name="csv_file" style="display: none" onChange="Handlechange();"/>
<input type="button" class="custom-file-upload"  value="Choose File" onclick="HandleBrowseClick();"/><br />
<input type="text" id="filename" class="mt3 xsmt3 em9 field-hidden" readonly="true"/>-->
<input type="file" name="csv_file" id="file-6" class="inputfile" />
<label for="file-6"><span class="custom-file-upload" >Choose File</span>
<p class="w300 mt4 xsmt5 em9 xsem9 smem9"></p>
</label>
        </div>
        <div class="col-md-6 col-sm-6 col-xs-12 mt4 xsmt0 text-left padding0">
<a href="<?php echo site_url('assets/default/sample.csv')?>" class="w300 em9 xsem9 smem9 smart-color">Download Sample File</a>
</label>
        </div>
		
		
		
		
		<div class="clearfix"></div>
		 
		
		
		<div class="xstext-right xstext-center mt10 xsmt10 row">
        	<div class="col-md-8 col-sm-7 col-xs-6 btn-space">
            	<a href="#" class="blank-btn" data-dismiss="modal">Cancel</a> 
            </div>
            <div class="col-md-4 col-sm-5 col-xs-6">
            <img src="<?php echo $this->config->item("templateAssetsPath")?>images/loader.gif" class="center-block submitWait_div" style="width:35px; display:none; position:absolute;left:35%;top:2px" />	
            	<button type="submit" class="smart-btn submitButt" style="float:right;display:block">&nbsp;&nbsp;Save&nbsp;&nbsp;</button>
                  <!--  <div class="" style="position:absolute; right:35px;">		    
               </div>-->
            </div>
            
		
		</div>
		
		</div></div>
       </form>
</div>
</div>
</div>
</div>
<!-- import from pc modalpopup ends here-->


<!-- manually upload modalpopup starts here-->
<div id="manuallyUpload" class="modal fade" role="dialog">
  <div class="modal-dialog campaign-modal">
    <div class="modal-content">
      <div class="modal-body padding0 clearfix xsmb6 xsmt4 mb4">
       <div class="col-md-5 col-sm-5 col-xs-12 mt7 xsmt0 mb4">
       <div class="col-xs-12 bottom-border">
      
       <img src="<?php echo $this->config->item('templateAssetsPath')?>images/manually-icon.png" class="img-responsive center-block mobile-img" /></div>
       </div>
	   <form action="<?php echo site_url('add-suppression-list')?>" method="post" class="addSuppManForm">
       <div class="col-md-7 col-sm-7 col-xs-12 left-border campaign-field xsmt4 mt0 ">
        <div class="col-xs-12">
       <h3 class="em16 smem16 xsem14">Manually</h3>
        <div class="form-group mt10 xsmt10">
		<label class="w300">Suppression List Name</label>
		<input type="text" name="list_name" placeholder="Suppression List Name" class="form-control" />
		</div>
        <div class="form-group">
		<label class="w300">Add Email</label>
	<!--	<textarea name="emails" cols="" rows="" placeholder="Email Address"></textarea>-->
        <input id="tags_1" type="text" name="emails" class="tags"  />
		</div>
        <p class="em10 smem9 xsem9 lightgrey visible-md visible-lg">Note: Enter Contact By Comma Separated</p>
        <p class="em10 smem9 xsem9 lightgrey visible-xs visible-sm">Note: Enter Email Separated by Enter key.</p>
        <div class="text-right mt10 xsmt10 xsmb4 xstext-right xstext-center"><a href="#" class="blank-btn" data-dismiss="modal">Cancel</a>&nbsp;&nbsp;<button type="text" class="smart-btn">&nbsp; Save &nbsp;</button></div>
       </div></div>
	   </form>
      </div>
    </div>
  </div>
</div>
<!-- manually upload modalpopup ends here-->
<script>
/*-------------------------- delete single contact starts here ------------------------------------*/
$('body').delegate(".deletelink", 'click', function() {
    
	deleteurl = $(this).attr('data-href');
	 $('#delete_yes_button').attr('href',deleteurl);
	 $('#delete_yes_button').removeClass('deleteMultiple');
});
/*-------------------------- delete single contact ends here ------------------------------------*/

/*-------------------------- Add Suppression manually starts here ------------------------------------*/
$(document).ready(function() {
    $(".addSuppManForm").submit(function(event) {
        var posturl = $(this).attr('action');
        $(this).ajaxSubmit({
            url: posturl,
            dataType: 'json',
            success: function(response) {
				
                if (response.success) {
					
					if(response.model=='hide')
					$('#manuallyUpload').modal('toggle');
					PNotify.removeAll();
					new PNotify({title: 'Success',text: response.success_msg,type: 'success'});
					$('.addSuppManForm').trigger("reset");
					//$(".contentDiv").load(location.href + " .contentDiv"); 
					 window.location.reload(true); 
					
					 
                } else {
				   PNotify.removeAll();
				   new PNotify({title: 'Error',text: response.error,type: 'error'});
                }
            },
        });
        return false;
    });
});
/*-------------------------- Add Suppression manually ends here ------------------------------------*/

/*-------------------------- Add Suppression Import starts here ------------------------------------*/
$(document).ready(function() {
    $(".addSuppImpForm").submit(function(event) {
        var posturl = $(this).attr('action');
        $(this).ajaxSubmit({
            url: posturl,
            dataType: 'json',
			beforeSend: function() { 
		     $('.submitButt').css('display','none'); 
		     $('.submitWait_div').fadeIn(); 
		    },
            success: function(response) {
			  $('.submitWait_div').fadeOut();
			  $('.submitButt').css('display','block');
				
                if (response.success) {
					
					if(response.model=='hide')
					$('#importPc').modal('toggle');
					PNotify.removeAll();
					new PNotify({title: 'Success',text: response.success_msg,type: 'success'});
					$('.addSuppImpForm').trigger("reset");
					//$(".contentDiv").load(location.href + " .contentDiv"); 
					 window.location.reload(true); 
					
					 
                } else {
				   PNotify.removeAll();
				   new PNotify({title: 'Error',text: response.error,type: 'error'});
                }
            },
        });
        return false;
    });
});
/*-------------------------- Add Suppression Import ends here ------------------------------------*/

/*-------------------------- success message starts here---------------------------------------------------*/
<?php if($this->session->userdata('success_msg')) { ?>
new PNotify({
	  title: 'Success',
	  text: '<?php echo $this->session->userdata('success_msg')?>',
	  type: 'success'
	});
<?php $this->session->unset_userdata('success_msg'); } ?>
/*-------------------------- success message ends here---------------------------------------------------*/

</script>
<!----------------------file upload start js---------------------->
<script>
/*
	By Osvaldas Valutis, www.osvaldas.info
	Available for use under the MIT License
*/
(function(e,t,n){var r=e.querySelectorAll("html")[0];r.className=r.className.replace(/(^|\s)no-js(\s|$)/,"$1js$2")})(document,window,0);
        

'use strict';

;( function ( document, window, index )
{
	var inputs = document.querySelectorAll( '.inputfile' );
	Array.prototype.forEach.call( inputs, function( input )
	{
		var label	 = input.nextElementSibling,
			labelVal = label.innerHTML;

		input.addEventListener( 'change', function( e )
		{
			var fileName = '';
			if( this.files && this.files.length > 1 )
				fileName = ( this.getAttribute( 'data-multiple-caption' ) || '' ).replace( '{count}', this.files.length );
			else
				fileName = e.target.value.split( '\\' ).pop();

			if( fileName )
				label.querySelector( 'p' ).innerHTML = fileName;
			else
				label.innerHTML = labelVal;
		});

		// Firefox bug fix
		input.addEventListener( 'focus', function(){ input.classList.add( 'has-focus' ); });
		input.addEventListener( 'blur', function(){ input.classList.remove( 'has-focus' ); });
	});
}( document, window, 0 ));
</script>

<!----------------------file upload end js ---------------------->