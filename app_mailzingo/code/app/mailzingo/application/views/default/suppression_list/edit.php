<!------------------------------------------- tagsinput js start ------------------------------------------------------->
<script type="text/javascript" src="<?php echo $this->config->item('templateAssetsPath')?>js/jquery.tagsinput.js"></script>
<script type="text/javascript">

		function onAddTag(tag) {
			alert("Added a tag: " + tag);
		}
		function onRemoveTag(tag) {
			alert("Removed a tag: " + tag);
		}

		function onChangeTag(input,tag) {
			alert("Changed a tag: " + tag);
		}

		$(function() {

			$('#tags_1').tagsInput({width:'auto'});
		
		$('input.tags').tagsInput({interactive:false});
		});

</script>
<!-------------------------------------------- tagsinput js end  -------------------------------------------------------->
<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padding15 footer-height">
<!-- dash header-->
<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 mb1 xsmb4">
<div class="dash-header">
<div class="row">
<div class="col-md-12 col-sm-12"><h1 class="smart-color">Suppression List</h1>
<p>Add subscribers here that you wish to exclude from your mailing list</p></div>
<!--<div class="col-md-5 col-sm-5 xstext-right xstext-center mt2 xsmt4 mb0 xsmb2">
<a href="#" class="smart-btn" data-toggle="modal" data-target="#myModal">Create Campaign</a></div> --></div>
</div>
</div>
<!-- dash header end-->

<div class="clearfix"></div>
<div class="col-md-12 mb1 xsmb2 clearfix text-right">
<a href="<?php echo site_url('suppression-list')?>" class="back-link">
<i class="fa fa-arrow-left" aria-hidden="true"></i> &nbsp;Back</a>
</div>


<div class="col-md-12 col-sm-12 col-xs-12 mb2 xsmb4">
<div class="layout-box1 clearfix padding0 mailer-data">

<div class="box-padding">
<div class="col-xs-12 col-md-6 col-sm-6 mt1 mb2 xsmb3 w600 em11 smem11 xsem11 padding0"><?php echo $listDetail->list_name?></div>
<div class="col-xs-7 col-md-6 col-sm-6 mb2 xsmb2 text-right campaign-search padding0  hidden-xs">
<a data-toggle="modal" data-target="#importPc" class="suppression-grey-btn">Import From PC</a> &nbsp;<a data-toggle="modal" data-target="#manuallyUpload" class="suppression-grey-btn">Manually</a>
</div></div>


<div class="clearfix"></div>
<div class="table-responsive table-multi-data table-fixed-header contentDiv" id="autoscroll">
<table width="100%" border="0" cellspacing="0" cellpadding="0" id="tbl">
    <thead>
    <tr>
    <th width="60%">Subscribers</th>
    <th width="40%"class="text-center">Action</th>
  </tr>
   <tr class="noresult" style="display:  <?php if($total_data==0) { ?> table-row-group <?php } else { ?>none <?php } ?>;">
		   <th colspan="6" class="text-center w3000">No Record Found</th>
		  </tr>
    </thead>
    <tbody id="tbltbody1" style="max-height:350px;">
        
    </tbody>
</table>
</div>
</div>

<div class="col-xs-12 text-center visible-xs xsmt6 xsmb4">
<a data-toggle="modal" data-target="#importPc" class="suppression-grey-btn">From Gallery</a> &nbsp;<a data-toggle="modal" data-target="#manuallyUpload" class="suppression-grey-btn">Manually</a>
</div>


</div>
</div>

<!-- import from pc modalpopup starts here-->
<div id="importPc" class="modal fade" role="dialog">
  <div class="modal-dialog campaign-modal">
    <div class="modal-content">
      <div class="modal-body padding0 clearfix xsmb6 xsmt4 mb2">
       <div class="col-md-5 col-sm-5 col-xs-12 mt7 xsmt0 mb4">
       <div class="col-xs-12 bottom-border">
      
       <img src="<?php echo $this->config->item('templateAssetsPath')?>images/import-pc.png" class="img-responsive center-block mobile-img" /></div>
       </div>
	   <form action="<?php echo site_url('import-suppression-list')?>" method="post" class="addSuppImpForm" enctype="multipart/form-data">
       <div class="col-md-7 col-sm-7 col-xs-12 left-border campaign-field xsmt4 mt0 ">
        <div class="col-xs-12">
       <h3 class="em16 smem16 xsem14">Import List From PC</h3>
		<div class="col-md-6 col-sm-6 col-xs-12 mt10 xsmt0 text-left padding0">
	     <input type="hidden" name="list_id" value="<?php echo $listDetail->id?>" />
		<!--<label class="custom-file-upload"><input type="file" name="csv_file"/>Choose File</label> -->
        <input type="file" name="csv_file" id="file-6" class="inputfile" />
        <label for="file-6"><span class="custom-file-upload" >Choose File</span>
        <p class="w300 mt4 xsmt5 em9 xsem9 smem9"></p>
        </label>
        
        </div>
         <div class="col-md-6 col-sm-6 col-xs-12 mt12 xsmt0 text-left padding0">
<a href="<?php echo site_url('assets/default/sample.csv')?>" class="w300 em9 xsem9 smem9 smart-color">Download Sample File</a>
</label>
        </div>
		<div class="clearfix"></div>
		<div class="xstext-right xstext-center mt10 xsmt10 row">
        	<div class="col-md-8 col-sm-7 col-xs-6 btn-space">
            	<a href="#" class="blank-btn" data-dismiss="modal">Cancel</a> 
            </div>
            <div class="col-md-4 col-sm-5 col-xs-6">
            <img src="<?php echo $this->config->item("templateAssetsPath")?>images/loader.gif" class="center-block submitWait_div" style="width:35px; display:none; position:absolute;left:35%;top:2px" />	
            	<button type="submit" class="smart-btn submitButt" style="float:right;display:block">&nbsp;&nbsp;Save&nbsp;&nbsp;</button>
                  <!--  <div class="" style="position:absolute; right:35px;">		    
               </div>-->
            </div>
            
		
		</div>
		</div></div>
       </form>
</div>
</div>
</div>
</div>
<!-- import from pc modalpopup ends here-->


<!-- manually upload modalpopup starts here-->
<div id="manuallyUpload" class="modal fade" role="dialog">
  <div class="modal-dialog campaign-modal">
    <div class="modal-content">
      <div class="modal-body padding0 clearfix xsmb6 xsmt4 mb4">
       <div class="col-md-5 col-sm-5 col-xs-12 mt7 xsmt0 mb4">
       <div class="col-xs-12 bottom-border">
      
       <img src="<?php echo $this->config->item('templateAssetsPath')?>images/manually-icon.png" class="img-responsive center-block mobile-img" /></div>
       </div>
	   <form action="<?php echo site_url('add-suppression-list')?>" method="post" class="addSuppManForm">
       <div class="col-md-7 col-sm-7 col-xs-12 left-border campaign-field xsmt4 mt0 ">
        <div class="col-xs-12">
       <h3 class="em16 smem16 xsem14">Manually</h3>
        <div class="form-group">
		<label class="w300">Add Email</label>
		<input type="hidden" name="list_id" value="<?php echo $listDetail->id?>" />
	<!--	<textarea name="emails" cols="" rows="" placeholder="Email Address"></textarea>-->
        <input id="tags_1" type="text" name="emails" class="tags" />
		</div>
        <p class="em9 smem9 xsem9 visible-md visible-lg">Note: Enter Contact By Comma Separated</p>
         <p class="em10 smem9 xsem9 visible-xs visible-sm">Note: Enter Email Separated by Enter key.</p>
        <div class="text-right mt10 xsmt10 xsmb4 xstext-right xstext-center"><a href="#" class="blank-btn" data-dismiss="modal">Cancel</a> <button type="text" class="smart-btn">&nbsp; Save &nbsp;</button></div>
       </div></div>
	   </form>
      </div>
    </div>
  </div>
</div>
<!-- manually upload modalpopup ends here-->

<script>
/*-------------------------- delete single contact starts here ------------------------------------*/
$('body').delegate(".deletelink", 'click', function() {
    
	deleteurl = $(this).attr('data-href');
	 $('#delete_yes_button').attr('href',deleteurl);
	 $('#delete_yes_button').removeClass('deleteMultiple');
});
/*-------------------------- delete single contact ends here ------------------------------------*/

/*-------------------------- Add Suppression manually starts here ------------------------------------*/
$(document).ready(function() {
    $(".addSuppManForm").submit(function(event) {
        var posturl = $(this).attr('action');
        $(this).ajaxSubmit({
            url: posturl,
            dataType: 'json',
            success: function(response) {
				
                if (response.success) {
					
					if(response.model=='hide')
					$('#manuallyUpload').modal('toggle');
					//PNotify.removeAll();
					//new PNotify({title: 'Success',text: response.success_msg,type: 'success'});
					$('.addSuppManForm').trigger("reset");
//					$(".contentDiv").load(location.href + " .contentDiv");
                    window.location.reload(true); 
					
					 
                } else {
				   PNotify.removeAll();
				   new PNotify({title: 'Error',text: response.error,type: 'error'});
                }
            },
        });
        return false;
    });
});
/*-------------------------- Add Suppression manually ends here ------------------------------------*/

/*-------------------------- Add Suppression Import starts here ------------------------------------*/
$(document).ready(function() {
    $(".addSuppImpForm").submit(function(event) {
        var posturl = $(this).attr('action');
        $(this).ajaxSubmit({
            url: posturl,
            dataType: 'json',
			beforeSend: function() { 
		     $('.submitButt').css('display','none'); 
		     $('.submitWait_div').fadeIn(); 
		    },
            success: function(response) {
				  $('.submitWait_div').fadeOut();
			  $('.submitButt').css('display','block');
                if (response.success) {
					
					if(response.model=='hide')
					$('#importPc').modal('toggle');
					//PNotify.removeAll();
					//new PNotify({title: 'Success',text: response.success_msg,type: 'success'});
					$('.addSuppImpForm').trigger("reset");
					//$(".contentDiv").load(location.href + " .contentDiv"); 
					 window.location.reload(true); 
					
					 
                } else {
				   PNotify.removeAll();
				   new PNotify({title: 'Error',text: response.error,type: 'error'});
                }
            },
        });
        return false;
    });
});
/*-------------------------- Add Suppression Import ends here ------------------------------------*/
</script>

<script type="text/javascript">
/*-------------------------- Auto scrolling pagination starts here ------------------------------------*/
$(document).ready(function() {
var total_record = 0;
var total_groups = '<?php echo $total_data; ?>';
var id = '<?php echo $id; ?>';
var prestat = true;


$('#tbl > tbody').load("<?php echo base_url() ?>suppression-list-pagination",
 {'group_no': total_record,'id':id}, function() {total_record++;});
$('#tbltbody1').scroll(function() {       
    if($('#tbltbody1').scrollTop() + $('#tbl').height() > $('#tbltbody1').prop('scrollHeight') && prestat) 
    {       
        if(total_record <= total_groups)
        {
          loading = true; 
          $('.loader_image').show(); 
		    prestat = false;
          $.post('<?php echo site_url() ?>suppression-list-pagination',{'group_no': total_record,'id':id},
            function(data){ 
                if (data != "") {
                    $("#tbl > tbody").append(data);                 
                    $('.loader_image').hide();                  
                    total_record++;
					prestat = true;
                }
            });     
        }
    }
});
});
/*-------------------------- Auto scrolling pagination end here ------------------------------------*/

/*-------------------------- success message starts here---------------------------------------------------*/
<?php if($this->session->userdata('success_msg')) { ?>
new PNotify({
	  title: 'Success',
	  text: '<?php echo $this->session->userdata('success_msg')?>',
	  type: 'success'
	});
<?php $this->session->unset_userdata('success_msg'); } ?>
/*-------------------------- success message ends here---------------------------------------------------*/
</script>

<!----------------------file upload start js---------------------->
<script>
/*
	By Osvaldas Valutis, www.osvaldas.info
	Available for use under the MIT License
*/
(function(e,t,n){var r=e.querySelectorAll("html")[0];r.className=r.className.replace(/(^|\s)no-js(\s|$)/,"$1js$2")})(document,window,0);
        

'use strict';

;( function ( document, window, index )
{
	var inputs = document.querySelectorAll( '.inputfile' );
	Array.prototype.forEach.call( inputs, function( input )
	{
		var label	 = input.nextElementSibling,
			labelVal = label.innerHTML;

		input.addEventListener( 'change', function( e )
		{
			var fileName = '';
			if( this.files && this.files.length > 1 )
				fileName = ( this.getAttribute( 'data-multiple-caption' ) || '' ).replace( '{count}', this.files.length );
			else
				fileName = e.target.value.split( '\\' ).pop();

			if( fileName )
				label.querySelector( 'p' ).innerHTML = fileName;
			else
				label.innerHTML = labelVal;
		});

		// Firefox bug fix
		input.addEventListener( 'focus', function(){ input.classList.add( 'has-focus' ); });
		input.addEventListener( 'blur', function(){ input.classList.remove( 'has-focus' ); });
	});
}( document, window, 0 ));
</script>
<!----------------------file upload end js ---------------------->