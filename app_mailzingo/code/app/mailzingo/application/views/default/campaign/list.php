<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padding15 footer-height">
<!-- dash header-->
<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 mb2 xsmb4">
<div class="dash-header">
<div class="row">
<div class="col-md-7 col-sm-7"><h1 class="smart-color">Campaigns</h1>
<p>Create and manage your campaigns with ease</p></div>
<div class="col-md-5 col-sm-5 xstext-right xstext-center mt2 xsmt4 mb0 xsmb4">
<a href="#" class="smart-btn" data-toggle="modal" data-target="#campaignModal">Create Campaign</a></div></div>
</div>
</div>
<!-- dash header end-->

<div class="clearfix"></div>
<div class="col-md-7 col-sm-6 col-xs-12 text-left campaign-links mb1 xsmb4">
<ul>
<li><a href="javascript:" class="disable multi_action_btn copyMove" data-toggle="modal" data-target="#copyMoveModel" data-type="copy" title="Copy"><i class="fa fa-files-o em14 smem14 xsem14" aria-hidden="true"></i></a></li>
<li><a href="javascript:" class="disable multi_action_btn copyMove" data-toggle="modal" data-target="#copyMoveModel" data-type="move" title="Move"><i class="fa fa-arrows em14 smem14 xsem14" aria-hidden="true"></i></a></li>
<li><a data-toggle="modal" data-target="#delete" class="disable multi_action_btn deleteMultiLink" title="Delete"><i class="fa fa-trash-o em14 smem14 xsem14" aria-hidden="true"></i></a></li>
<li class="campaign-search"><input type="text" placeholder="Search for Campaign" name="search_by" class="search_by" value="<?php echo $this->input->get('search_by')?>"><button type="button" class="search_btn"><i class="fa fa-search"></i></button></li>
</ul>
</div>
<div class="col-md-5 col-sm-6 col-xs-6 text-right sorting mb1 hidden-xs">
Sort by <select class="sort_by" name="sort_by"> 
    <option value="add_time">Recent</option>
    <option value="campaign_name">Name</option>
</select>
</div>
<div class="col-md-12 col-sm-12 col-xs-12 mb2">
<div class="table-bordered">
<div class="table-responsive table-data table-fixed-header contentDiv" id="campaign-fixed-coloum">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
 <thead> <tr class="w600 text-center">
    <th class="text-center"><input name="" type="checkbox" value="" id="select_all" /></th>
    <th class="text-left">Campaigns</th>
    <th class="text-center">Create Date</th>
    <th class="text-center">Subscribers</th>
    <th class="text-center">Action</th>
  </tr>
   <tr class="noresult" style="display:<?php if(sizeof($allRecord)==0) { ?> table-row-group <?php } else { ?>none <?php } ?>;">
      <th colspan="5" class="text-center w3000">No Record Found</th>
  </tr>
  </thead>
 <tbody> <?php foreach($allRecord as $value) { ?>
  <?php $contactCount = $this->common_model->getCountAllFromAnyTable('campaign_id',$value->campaign_id,'tbl_contact','active'); ?>
  <tr class="text-center">
    <td><input name="select_checkbox" type="checkbox" value="<?php echo $value->campaign_id?>" class="select_checkbox"/></td>
    <td class="text-left more-word1"><?php echo $value->campaign_name?></td>
    <td class="text-center"><?php echo date('Y-m-d h:i:s',$value->add_time)?></td>
    <td class="text-center"><?php echo $contactCount?></td>
    <td class="table-icon text-center"><ul>
    <li><a href="<?php echo site_url('add-contact/'.$value->campaign_id)?>" title="Add"><i class="fa fa-user-plus em12" aria-hidden="true"></i></a></li>
    <li><a data-toggle="modal" data-target="#copyMoveModel" class="edit" title="Edit" data-href="<?php echo site_url('edit-campaign/'.$value->campaign_id)?>" href="javascript:"><i class="fa fa-pencil-square-o em12" aria-hidden="true"></i></a></li>
    <li><a data-toggle="modal" data-target="#delete" title="Delete" class="deletelink" data-href="<?php echo site_url('delete-campaign/'.$value->campaign_id)?>" href="javascript:"><i class="fa fa-trash-o em12" aria-hidden="true"></i></a></li>
	<li><a data-toggle="modal" data-target="#clear-camp" title="Delete Contacts" class="clearlink" data-href="<?php echo site_url('clear-campaign/'.$value->campaign_id)?>" href="javascript:"><i class="fa fa-eraser em12" aria-hidden="true"></i></a></li>
    </ul></td>
  </tr>
  <?php } ?></tbody>
</table>
</div>
</div>
</div>
</div>
<!-- create campaign modalpopup-->
<div id="campaignModal" class="modal fade addCampModel" role="dialog">
  <div class="modal-dialog campaign-modal">
    <div class="modal-content">
      <div class="modal-body padding0 clearfix xsmb6 xsmt4 mb4">
       <div class="col-md-5 col-sm-5 col-xs-12 mt7 xsmt0 mb4">
       <div class="col-xs-12 bottom-border">
      
       <img src="<?php echo $this->config->item('templateAssetsPath')?>images/create-popup-icon.png" class="img-responsive center-block mobile-img" /></div>
       </div>
	   <form action="<?php echo site_url('add-campaign')?>" method="post" class="addCampForm" >
       <div class="col-md-7 col-sm-7 col-xs-12 left-border campaign-field xsmt4 mt0 ">
        <div class="col-xs-12">
       <h3 class="em16 smem16 xsem14">Create Campaign</h3>
        <div class="form-group mt10 xsmt10">
		<label class="w300">Campaign Name</label>
		<input type="text" name="campaign_name" class="form-control" placeholder="Campaign Name" />
		</div>
        <div class="form-group">
		<label class="w300">Campaign Details</label>
		<textarea name="campaign_description" cols="" rows="" placeholder="Campaign Details"></textarea>
		</div>
        <div class="xstext-right xstext-center mt10 xsmt10 row">
        <div class="col-md-8 col-sm-7 col-xs-6 btn-space"><a href="javascript:" class="blank-btn" data-dismiss="modal">Cancel</a></div>
          <div class="col-md-4 col-sm-5 col-xs-6">
          <img src="<?php echo $this->config->item("templateAssetsPath")?>images/loader.gif" class="center-block addWait_div" style=" width:35px; display:none; position:absolute;left:35%;top:2px" />
           <button type="submit" style="float:right;" class="smart-btn addButt">Create</button>
          </div>
		</div>
        
        
       </div></div>
	   </form>
      </div>
    </div>
  </div>
</div>


<!-- Clear campaign modalpopup-->
<div id="clear-camp" class="modal fade" role="dialog">
  <div class="modal-dialog delete-modal">
    <div class="modal-content">
      <div class="modal-body clearfix padding0">
       
       <div class="mt10 xsmt10 text-center">
      
       <h4 class="mb6 col-md-10 col-md-offset-1 col-sm-offset-0 col-sm-12 col-xs-12 col-xs-offset-0">Are You Sure You Want To Delete All Contacts From This Campaign?</h4>
<div class="clearfix"></div>
<hr />
        <div class="text-center mt6 xsmt6 mb4 xsmb4"><a href="#" class="smart-blank-btn" data-dismiss="modal">No</a> <a href="#" class="yes-btn" id="clear_yes_button">Yes</a></div>
        </div>
      </div>
    </div>
  </div>
</div>

<!-------------- copy move model will come here ----------------------->
<div id="copyMoveModel" class="modal fade copyMoveModel" role="dialog"></div>
<!-------------- copy move model ends here ----------------------->
<script>
/*-------------------------- delete single campaign starts here ------------------------------------*/
$('body').delegate(".deletelink", 'click', function() {
    
	deleteurl = $(this).attr('data-href');
	 $('#delete_yes_button').attr('href',deleteurl);
	 $('#delete_yes_button').removeClass('deleteMultiple');
});
/*-------------------------- delete single campaign ends here ------------------------------------*/

/*-------------------------- Clear single campaign starts here ------------------------------------*/
$('body').delegate(".clearlink", 'click', function() {
    
	deleteurl = $(this).attr('data-href');
	 $('#clear_yes_button').attr('href',deleteurl);
	 $('#clear_yes_button').removeClass('deleteMultiple');
});
/*-------------------------- Clear single campaign ends here ------------------------------------*/

/*-------------------------- Searching/sorting/pagination function starts here ------------------------------------*/
$('body').delegate(".sort_by", 'change', function() {
	ssp();
});
$('body').delegate(".search_by", 'keyup', function() {
	ssp();
});
function ssp(){

	var posturl = site_url+'campaign-list';
	var sort_by = $('.sort_by').val();
	var search_by = $('.search_by').val();
	
     window.history.pushState('', '', posturl+'?sort_by='+sort_by+'&search_by='+search_by);
     $(".contentDiv").load(location.href + " .contentDiv"); 
}
/*-------------------------- Searching/sorting/pagination function ends here ------------------------------------*/


/*-------------------------- check/uncheck multiple campaign starts here ------------------------------------*/
$('body').delegate("#select_all", 'change', function() {
      $(".select_checkbox").prop('checked', $(this).prop("checked"));
	  $(".select_checkbox").change();
      });
$('body').delegate(".select_checkbox", 'change', function() {
	  var length = $('.select_checkbox:checked').length
	  if(length==0)
	   {
		  $(".multi_action_btn").addClass('disable');
	   } else {
		  $(".multi_action_btn").removeClass('disable');
	   }
      });
      
$(".select_checkbox").change(function(){
    if ($('.select_checkbox:checked').length != $('.select_checkbox').length) {
       $("#select_all").prop('checked', false);
    }else{
        $("#select_all").prop('checked', true);
    }
});
/*-------------------------- check/uncheck multiple campaign ends here ------------------------------------*/

/*-------------------------- Edit campaign starts here ------------------------------------*/
$('body').delegate(".edit", 'click', function() {

    var posturl = $(this).attr('data-href');
    $.ajax({
        url: posturl,
        dataType: 'json',
        type: "POST",
        success: function(data) {
            if (data.success) {
                $('.copyMoveModel').html(data.html);
            }
        },
    });
});
/*-------------------------- Edit campaign ends here ------------------------------------*/

/*-------------------------- copy/move multiple campaign starts here ------------------------------------*/
$('body').delegate(".copyMove", 'click', function() {

    var posturl = site_url+'copy-move-compaign';
	var id = [];
	$(".select_checkbox:checkbox:checked").each(function (e) {
            id[e] = $(this).val();
        });
	var type = $(this).attr('data-type');
	var actionType = 'view';
	var length = $('.select_checkbox:checked').length;
	if(length==0)
	{ return false; }

    $.ajax({
        url: posturl,
        dataType: 'json',
        type: "POST",
        data: {id: id,type: type,actionType: actionType},
        success: function(data) {
            if (data.success) {
                $('.copyMoveModel').html(data.html);
            }
        },
    });
    
});
/*-------------------------- copy/move multiple campaign ends here ------------------------------------*/

/*-------------------------- delete multiple campaign starts here ------------------------------------*/
$('body').delegate(".deleteMultiLink", 'click', function() {
    
	var length = $('.select_checkbox:checked').length;
	if(length==0)
	{ return false; }
	deleteurl = $(this).attr('data-href');
	 $('#delete_yes_button').attr('href','javascript:');
	 $('#delete_yes_button').addClass('deleteMultiple');
});
$('body').delegate(".deleteMultiple", 'click', function() {

    var posturl = site_url+'delete-multiple-campaign';
	var id = [];
	$(".select_checkbox:checkbox:checked").each(function (e) {
            id[e] = $(this).val();
        });
	var length = $('.select_checkbox:checked').length;
	if(length==0)
	{ return false; }

    $.ajax({
        url: posturl,
        dataType: 'json',
        type: "POST",
        data: {id: id},
        success: function(data) {
            if (data.success) {
			    $('#delete').modal('toggle');
				//PNotify.removeAll();
				//new PNotify({title: 'Success',text: 'Campaign Deleted successfully',type: 'success'});
               // $(".contentDiv").load(location.href + " .contentDiv"); 
			   $('input:checkbox').removeAttr('checked');
				location.reload(); 
				 $(".multi_action_btn").addClass('disable');
            } else {
			    $('#delete').modal('toggle');
				PNotify.removeAll();
				new PNotify({title: 'Error',text: data.error_msg,type: 'error'});
			}
        },
    });
});
/*-------------------------- delete multiple campaign ends here------------------------------------*/

/*-------------------------- success message starts here---------------------------------------------------*/
<?php if($this->session->userdata('success_msg')) { ?>
new PNotify({
	  title: 'Success',
	  text: '<?php echo $this->session->userdata('success_msg')?>',
	  type: 'success'
	});
<?php $this->session->unset_userdata('success_msg'); } ?>
/*-------------------------- success message ends here---------------------------------------------------*/

/*-------------------------- error message starts here---------------------------------------------------*/
<?php if($this->session->userdata('error_msg')) { ?>
new PNotify({
	  title: 'Error',
	  text: '<?php echo $this->session->userdata('error_msg')?>',
	  type: 'error'
	});
<?php $this->session->unset_userdata('error_msg'); } ?>
/*-------------------------- error message ends here---------------------------------------------------*/

/*-------------------------- Add Campaign starts here---------------------------------------------------*/
$(document).ready(function() {
    $(".addCampForm").submit(function(event) {
        var posturl = $(this).attr('action');
        $(this).ajaxSubmit({
            url: posturl,
            dataType: 'json',
			beforeSend: function() { 
		     $('.addButt').css('display','none'); 
		     $('.addWait_div').fadeIn(); 
		    },
            success: function(response) {
		        $('.addWait_div').fadeOut();
				$('.addButt').css('display','block');

                if (response.success) {
					
					if(response.model=='hide')
					$('.addCampModel').modal('toggle');
					window.location.href = site_url+'campaign-list';
                } else {
                   
				   PNotify.removeAll();
				   new PNotify({title: 'Error',text: response.error,type: 'error'});
				    
                }
            },
        });
        return false;
    });
});
/*-------------------------- Add Campaign ends here---------------------------------------------------*/
</script>

<?php 
if(isset($_REQUEST['camp_list_direct']) && $_REQUEST['camp_list_direct']==1 ){
    ?>
<script>
    $('#campaignModal').modal('show'); 
</script>
<?php 
}
?>

