<div class="modal-dialog campaign-modal">
    <div class="modal-content">
      <div class="modal-body padding0 clearfix xsmb4 xsmt4 mb4">
       <div class="col-md-5 col-sm-5 col-xs-12 mt7 xsmt4 mb4">
       <div class="col-xs-12 bottom-border">
      
       <img src="<?php echo $this->config->item('templateAssetsPath')?>images/create-popup-icon.png" class="img-responsive center-block mobile-img" /></div>
       </div>
	   <form action="<?php echo site_url('edit-campaign/'.$id)?>" method="post" class="editCampForm" >
       <div class="col-md-7 col-sm-7 col-xs-12 left-border campaign-field xsmt4 mt0 ">
        <div class="col-xs-12">
       <h3 class="em16 smem16 xsem14">Update Campaign</h3>
        <div class="form-group mt10 xsmt10">
		<label class="w300">Campaign Name</label>
		<input type="text" name="campaign_name" class="form-control" value="<?php echo $campaign_name?>" />
		</div>
        <div class="form-group">
		<label class="w300">Campaign Detail</label>
		<textarea name="campaign_description" cols="" rows="" placeholder="Campaign Detail"><?php echo $campaign_description?></textarea>
		</div>
        <div class="text-right mt10 xsmt10 xsmb4 xstext-right xstext-center"><a href="javascript:" class="blank-btn" data-dismiss="modal">Cancel</a> <button type="submit" class="smart-btn editButt">Update</button>
		<div class="editWait_div" style="display:none; position:absolute; margin-top:-35px; right:35px;"><img src="<?php echo $this->config->item("templateAssetsPath")?>images/loader.gif" class="center-block" style="width:50%;" /></div>
		</div>
       </div></div>
	   </form>
      </div>
    </div>
  </div>
 <script>
 /*-------------------------- Edit Campaign starts here---------------------------------------------------*/
$(document).ready(function() {
    $(".editCampForm").submit(function(event) {
        var posturl = $(this).attr('action');
        $(this).ajaxSubmit({
            url: posturl,
            dataType: 'json',
			beforeSend: function() { 
		     $('.editButt').fadeOut(); 
		     $('.editWait_div').fadeIn(); 
		    },
            success: function(response) {
		        $('.editWait_div').fadeOut();
				$('.editButt').fadeIn(); 

                if (response.success) {
					
					if(response.model=='hide')
					$('.copyMoveModel').modal('toggle');
					window.location.href = site_url+'campaign-list';
                } else {
                   
				   PNotify.removeAll();
				   new PNotify({title: 'Error',text: response.error,type: 'error'});
				    
                }
            },
        });
        return false;
    });
});
/*-------------------------- Edit Campaign ends here---------------------------------------------------*/

 </script>