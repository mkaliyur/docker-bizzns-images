<div class="modal-dialog campaign-modal">
    <div class="modal-content">
      <div class="modal-body padding0 clearfix xsmb4 xsmt4 mb4">
        <div class="col-md-5 col-sm-5 col-xs-12 mt7 xsmt4 mb4 right-border">
       <div class="col-xs-12 bottom-border">
       <div class="image-out-border"><div class="image-in-border">
       <img src="<?php echo $this->config->item('templateAssetsPath')?>images/<?php echo $type?>-icon.png" class="img-responsive center-block mobile-img" /></div></div></div>
       </div>
       <div class="col-md-7 col-sm-7 col-xs-12 campaign-field">
        <div class="col-xs-12">
       <h4><?php echo ucfirst($page_title)?> To Desired Campaigns</h4>
	    <form data-parsley-validate class="copyMoveForm" action="<?php echo site_url('copy-move-compaign')?>" method="post">
        <div class="form-group mt10 xsmt10">
		<label class="w300">Select Campaign</label>
        
<dl class="all-campaign"> 
<dt><a href="javascript:void(0)"><span class="hida em11 smem11 xsem11">Select</span><p class="multiSelP em11 smem11 xsem11"></p></a></dt>
<dd>
<div class="mutliSelect">
<ul>
 <?php foreach($campaign_list as $value) { ?>
<li><input type="checkbox" value="<?php echo $value->campaign_id?>" name="to_campaign[]" <?php if(in_array($value->campaign_id,explode(',',$from_campaign))){ ?> disabled="disabled" <?php } ?> data-title="<?php echo $value->campaign_name?>" class="campaignCheckbox"/>&nbsp; <span class="more-word2"><?php echo $value->campaign_name?></span></li>
<?php } ?>        
<input type="hidden" name="from_campaign" value="<?php echo $from_campaign?>" />
<input type="hidden" name="type" value="<?php echo $type?>" /> 
<input type="hidden" name="actionType" value="update" />    
</ul>
</div>
</dd>
</dl>

</div>
  
        <div class="xstext-right xstext-center mt10 xsmt10 row">
        	<div class="col-md-8 col-sm-7 col-xs-6 btn-space">
            	<a href="#" class="blank-btn" data-dismiss="modal">Cancel</a> 
            </div>
            <div class="col-md-4 col-sm-5 col-xs-6">
            <img src="<?php echo $this->config->item("templateAssetsPath")?>images/loader.gif" class="center-block copyWait_div" style="width:35px; display:none; position:absolute;left:35%;top:2px" />	
            	<button type="submit" class="smart-btn copyButt" style="float:right;display:block">&nbsp;&nbsp;<?php echo ucfirst($type)?>&nbsp;&nbsp;</button>
                  <!--  <div class="" style="position:absolute; right:35px;">		    
               </div>-->
            </div>
            
		
		</div>
		</form>
       </div></div>
      </div>
    </div>
  </div>
<script>
$(document).ready(function() {
    $(".copyMoveForm").submit(function(event) {
        var posturl = $(this).attr('action');
        $(this).ajaxSubmit({
            url: posturl,
            dataType: 'json',
			beforeSend: function() { 
		     $('.copyButt').css('display','none'); 
		     $('.copyWait_div').fadeIn(); 
		    },
            success: function(response) {
		        $('.copyWait_div').fadeOut();
				$('.copyButt').css('display','block');

                if (response.success) {
					
					if(response.model=='hide')
					$('.copyMoveModel').modal('toggle');
					//PNotify.removeAll();
					//new PNotify({title: 'Success',text: response.success_msg,type: 'success'});
					// $(".contentDiv").load(location.href + " .contentDiv");
					 window.location.href = site_url+'campaign-list';
					 $(".multi_action_btn").addClass('disable');
                } else {
                   
				   PNotify.removeAll();
				   new PNotify({title: 'Error',text: response.error,type: 'error'});
				    
                }
            },
        });
        return false;
    });
});

$('body').delegate(".campaignCheckbox", 'change', function() {
      
	  var length = $('.campaignCheckbox:checked').length
	  $('.multiSelP').text(length+' Selected');
      });
</script>
<script type='text/javascript' src="<?php echo $this->config->item('templateAssetsPath')?>js/index.js"></script>