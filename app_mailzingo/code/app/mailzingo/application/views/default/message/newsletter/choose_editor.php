<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padding15 footer-height">
<!-- dash header-->
<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 mb2 xsmb4">
<div class="dash-header">
<div class="row">
<div class="col-md-12 col-sm-12"><h1 class="smart-color">Newsletter</h1>
<p>Here You Can Create Regular Bulk Mails</p></div>
<!--<div class="col-md-5 col-sm-5 xstext-right xstext-center mt2 xsmt4 mb0 xsmb2">
<a href="javascript:void(0)" class="smart-btn" data-toggle="modal" data-target="javascript:void(0)myModal">Create Campaign</a></div> -->
</div>
</div>
</div>
<!-- dash header end-->

<div class="clearfix"></div>
<div class="col-md-12 col-sm-12 col-xs-12 mb2 xsmb4">
<div class="layout-box clearfix">
<div class="mt0 mb2 xsmb3 w500 em14 smem13 xsem11 padding0">Choose Editor Type</div>

<div class="col-xs-12 col-md-12 col-sm-12 col-lg-9 center-div">
<div class="row">
<a href="<?php echo site_url('choose-newsletter-editor?editor_type=inline')?>" class="upload-area">
<div class="col-md-6 col-lg-6 col-sm-6 xstext-center mt5 xsmt5 mb4">
<img src="<?php echo $this->config->item('templateAssetsPath')?>images/inline-ediotr-img.jpg" class="img-responsive center-block editor-img">
<span class="editor-btns em12 smem12 xsem12 mt5 xsmt5">Inline Editor</span>
<p class="em11 smem11 xsem11 mt3 xsmt3">
Create Comprehensive Newsletter And
Send Latest Information To Your Subscribers</p>
</div></a>

<a href="<?php echo site_url('choose-newsletter-editor?editor_type=plain')?>" class="upload-area">
<div class="col-md-6 col-lg-5 col-lg-offset-1 col-sm-6 xstext-center mt5 xsmt12 mb4">
<img src="<?php echo $this->config->item('templateAssetsPath')?>images/plan-editor-img.jpg" class="img-responsive center-block editor-img">
<span class="editor-btns em12 smem12 xsem12 mt6 xsmt6">Plain Text Editor</span>
<p class="em11 smem11 xsem11 mt4 xsmt4">
Automatically Send Email To Entire List Or
Selected Subscribers With This Service</p>
</div></a>

</div>
</div>
</div>
</div>
</div>