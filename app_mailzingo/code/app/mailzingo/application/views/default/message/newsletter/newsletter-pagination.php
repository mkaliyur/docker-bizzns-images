&nbsp;
<?php foreach ($allRecord as $value){ ?>
<tr class="text-center looptr">
    <td width="5%"><input name="select_checkbox" type="checkbox" value="<?php echo $value->id?>" class="select_checkbox" /></td>
    <td class="text-left"  width="23%">
    <div class="pull-left">
    <a href="<?php echo site_url('assets/uploads/newsletter_images/'.$value->image)?>"><img src="<?php echo site_url('assets/uploads/newsletter_images/'.$value->image)?>" class="img-responsive newsletter-imgwidth" title="Click to Zoom Image" style="min-height:92px; max-height:92px; min-width:75px;" /></a>
    </div>
    <div class="pull-left padding15 mt10 xsmt12 more-word3"> <?php echo $value->message_name?></div></td>
    <td  width="23%"><?php echo date('M d, Y',$value->add_time)?></td>    
	<?php if($status=='scheduled') { ?>
	<td  width="23%"><?php echo date('M d, Y h:i A',$value->schedule_timestamp)?></td> 
	<?php } ?>
    <td class="table-icon"  width="23%"><ul>
	<?php if($value->status=='sent') { ?>
    <li><a href="<?php echo site_url('newsletter-stats/'.$value->id.'/overview')?>" title="Report"><i class="icon-Report em12" aria-hidden="true" title="Report"></i></a></li>
	<li><a href="<?php echo site_url('use-newsletter/'.$value->id)?>" title="Use"><i class="icon-Use-Me em12" aria-hidden="true" title="Use"></i></a></li>
	<?php } else if($value->status=='scheduled' || $value->status=='draft'){ ?>
	<li><a href="<?php echo site_url('edit-newsletter/'.$value->id)?>" title="edit"><i class="fa fa-pencil-square-o em12" aria-hidden="true" title="Edit"></i></a></li>
    <li><a data-toggle="modal" data-target="#testMail"  title="Message" class="testmailA top-space" data-newsletter_id="<?php echo $value->id?>"><i class="icon-Message em12" aria-hidden="true" title="Message"></i></a></li>
    <?php } ?>

    
  <li><a data-toggle="modal" data-target="#delete" title="Delete" class="deletelink" data-href="<?php echo site_url('delete-newsletter/'.$value->id)?>" href="javascript:"><i class="fa fa-trash-o em12" aria-hidden="true" title="Delete"></i></a></li> 
    </ul></td>
  </tr>
<?php } ?>

<script>
      $(".select_checkbox").change(function(){ 
    if ($('.select_checkbox:checked').length != $('.select_checkbox').length) {
       $("#select_all").prop('checked', false);
  }else{
        $("#select_all").prop('checked', true);
    }
});
</script>