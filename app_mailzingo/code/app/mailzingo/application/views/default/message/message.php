<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padding15 footer-height">
<!-- dash header-->
<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 mb2 xsmb4">
<div class="dash-header">
<div class="row">
<div class="col-md-12 col-sm-12"><h1 class="smart-color em22 smem18 xsem16">Message</h1>
<p class="em11 smem10 xsem10">Here You Can Create Newsletters And Autoresponder Mesage.</p></div>
<!--<div class="col-md-5 col-sm-5 xstext-right xstext-center mt2 xsmt4 mb0 xsmb2">
<a href="#" class="smart-btn" data-toggle="modal" data-target="#myModal">Create Campaign</a></div> -->
</div>
</div>
</div>
<!-- dash header end-->

<div class="clearfix"></div>
<div class="col-md-12 col-sm-12 col-xs-12 mb2 xsmb4">
<div class="layout-box clearfix">

<a href="<?php echo site_url('add-newsletter')?>" class="upload-area">
<div class="col-md-6 col-sm-6 right-border xstext-center mt5 xsmt5 mb4">
<i class="icon-Message em45 smem35 hidden-xs"></i>
<p class="em10 smem10 xsem10 mt8 xsmt8 hidden-xs">
Create Comprehensive Newsletter and send latest information<br />
 to opt-in list of your subscribers.</p>
<h5 class="w600 hidden-xs">Newsletter</h5>
<h5 class="w600 visible-xs mobile-btn">Newsletter</h5>
</div></a>

<a href="#" class="upload-area xstext-center">
<div class="col-md-6 col-sm-6 mt5 xsmt5 mb4">
<i class="icon-Autoresponder em45 smem35 hidden-xs"></i>
<p class="em10 xsem10 mt8 xsmt8 hidden-xs">
Automatically send Emails to your entire list or desired contacts<br />
 by using Autoresponder service</p>
<h5 class="w600 mt4 xsmt4 hidden-xs">Autoresponder</h5>
<h5 class="w600 visible-xs mobile-btn">Autoresponder</h5>
</div></a>

</div>

</div>
</div>