<!--- donought graph css and js--->
<link href="<?php echo $this->config->item('templateAssetsPath')?>css/nv.d3.css" rel="stylesheet" type="text/css">
<script src="https://cdnjs.cloudflare.com/ajax/libs/d3/3.5.2/d3.min.js" charset="utf-8"></script>
<script src="<?php echo $this->config->item('templateAssetsPath')?>js/nv.d3.js"></script>
<script>
    var testdata = [
        {key: "One", y: 5},
        {key: "Two", y: 10},
        {key: "Three", y: 15},
        {key: "Four", y: 20},
        {key: "Five", y: 25},
		
    ];

    var width = 300;
    var height = 300;

   nv.addGraph(function() {
        var chart = nv.models.pie()
                .x(function(d) { return d.key; })
                .y(function(d) { return d.y; })
                .width(width)
                .height(height)
                .labelType('percent')
                .valueFormat(d3.format('%'))
                .donut(true);

        d3.select("#graphs")
                .datum([testdata])
                .transition().duration(1200)
                .attr('width', width)
                .attr('height', height)
                .call(chart);

        return chart;
    });

</script>

<!--- new donought graph css and js--->
<style type="text/css">
#chartdiv {width:100%;height:300px;font-size:11px;}							
</style>
<!-- Resources -->
<script src="<?php echo $this->config->item('templateAssetsPath')?>js/amcharts.js"></script>
<script src="<?php echo $this->config->item('templateAssetsPath')?>js/pie.js"></script>
<script>
var chart = AmCharts.makeChart( "chartdiv", {
  "type": "pie",
  "theme": "light",
  "dataProvider": [ {
    "title": "opened",
    "value": <?php echo $total_open?>
  }, {
    "title": "Unopened",
    "value": <?php echo $total_unopen?>
  } ],
  "titleField": "title",
  "valueField": "value",
  "labelRadius": 5,

  "radius": "42%",
  "innerRadius": "60%",
  "labelText": "[[title]]",
  "export": {
    "enabled": true
  }
} );
</script>
<!--- donought graph css and js end--->
<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padding15 footer-height">
<!-- dash header-->
<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 mb2 xsmb4">
<div class="dash-header">
<div class="row">
<div class="col-md-12 col-sm-12"><h1 class="smart-color em22 smem18 xsem16">Statistics</h1>
<p class="em11 smem10 xsem10">Here are the stats for the mails sent by autoresponder</p></div>
<!--<div class="col-md-5 col-sm-5 xstext-right xstext-center mt2 xsmt4 mb0 xsmb2">
<a href="#" class="smart-btn">Create Newsletter</a></div> --> </div>
</div>
</div>
<!-- dash header end-->

<div class="clearfix"></div>
<div class="col-md-10 col-sm-10 col-xs-8 smart-tabs clearfix">

<ul>
<li><a href="<?php echo site_url('autoresponder-stats/'.$newsletter_id.'/overview')?>" title="Overview" class="active">
<span class="hidden-xs">Overview </span><i class="icon-Overview visible-xs"></i>
</a></li>
<li><a href="<?php echo site_url('autoresponder-stats/'.$newsletter_id.'/technology')?>" title="Technology">
<span class="hidden-xs">Technology</span><i class="icon-Technology visible-xs"></i>
</a></li>
<li><a href="<?php echo site_url('autoresponder-stats/'.$newsletter_id.'/location')?>" title="Location">
<span class="hidden-xs">Location</span><i class="icon-Location visible-xs"></i>
</a></li>
</ul>
</div>
<div class="col-md-2 col-sm-2 col-xs-4 text-right xsmt2 mt0">
<a href="<?php echo site_url('autoresponder-stats')?>" class="back-link">
<i class="fa fa-arrow-left" aria-hidden="true"></i> &nbsp;Back</a></div>


<div class="col-md-12 col-sm-12 col-xs-12 mb2 xsmb4">
<div class="layout-box clearfix">

<div class="col-xs-12 col-md-12 col-sm-12 mt1 mb2 xsmb3 w600 em11 smem11 xsem11 padding0"><?php echo $newsletter_detail->auto_name?></div>


<div class="clearfix"></div>
<div class="col-xs-12 padding0">
<div class="row">

<div class="col-md-5 col-sm-12">
<div class="layout-border1 clearfix">
<!--<div class="donough-graph"><svg id="graphs"></svg></div> -->
<?php if($total_sent==0) { ?>
<div class="w600 chart-no-data text-center">No Data Available</div>
<?php } ?>
<div id="chartdiv"></div>	
</div>

<div class="col-md-6 col-sm-6 col-xs-6 padding0 mt1 xsmt2 mb1">
<div class="col-md-7 col-sm-8 col-xs-7 sent-mail-btn1 em12 smem10 xsem8 roboto w500">Sent Mail</div>
<div class="col-md-5 col-sm-4 col-xs-5 roboto sent-mail-btn2 em16 smem12 xsem10 w500 text-right"><?php echo $total_sent?></div>
</div>
<div class="col-md-6 col-sm-6 col-xs-6 delpadding mt1 xsmt2 mb1 pull-right">
<div class="col-md-7 col-sm-8 col-xs-8 delivered-btn1 em12 smem10 xsem8 roboto w500 ">Delivered</div>
<div class="col-md-5 col-sm-4 col-xs-4 roboto delivered-btn2 em16 smem12 xsem10 w500 text-right"><?php echo $total_deliverd?></div>
</div>
<div class="clearfix"></div>
</div>

<div class="col-md-7 col-sm-12 padding0">
<div class="col-md-6 col-sm-6 mb1 xsmb2 xsmt2 mt0">
<div class="layout-bordered clearfix">
<div class="col-xs-12 padding0">
<div class="row">
<div class="col-md-8 col-sm-8 col-xs-7">
<h4 class="mb0 xsmb0 xsmt0 mt0 mdem11 em12 smem12 xsem11 more-word">Total Sent Mail</h4>
<p class="roboto em20 smem20 xsem15 mt0 mb0 bluering"><?php echo $total_sent?></p>
<hr class="mt0 mb0 xsmb0 xsmt0" />
<div class="xsmt1 mt2">
<a href="<?php echo site_url('autoresponder-stats-report/'.$newsletter_id.'/sent')?>" class="bluering em9 smem9 xsem9 w600">View List</a></div>
</div>
<div class="col-md-4 col-sm-4 col-xs-5">
<div id="donut1" data-donut="<?php echo ($total_sent==0?0:100)?>">
</div></div>
</div></div></div>
</div>


<div class="col-md-6 col-sm-6 mb1 xsmb2 xsmt2 mt0">
<div class="layout-bordered clearfix">
<div class="col-xs-12 padding0">
<div class="row">
<div class="col-md-8 col-sm-8 col-xs-7">
<h4 class="mb0 xsmb0 xsmt0 mt0 mdem11 em12 smem12 xsem11">Opened Mail</h4>
<p class="roboto em20 smem20 xsem15 mt0 mb0 smart-color"><?php echo $total_open?></p>
<hr class="mt0 mb0 xsmb0 xsmt0" />
<div class="xsmt1 mt2">
<a href="<?php echo site_url('autoresponder-stats-report/'.$newsletter_id.'/open')?>" class="bluering em9 smem9 xsem9 w600">View List</a></div>
</div>
<div class="col-md-4 col-sm-4 col-xs-5">
<div id="donut2" data-donut="<?php echo $open_per?>">
</div></div>
</div></div></div></div>

<div class="col-md-6 col-sm-6 mb1 xsmb2 xsmt2 mt0">
<div class="layout-bordered clearfix">
<div class="col-xs-12 padding0">
<div class="row">
<div class="col-md-8 col-sm-8 col-xs-7">
<h4 class="mb0 xsmb0 xsmt0 mt0 mdem11 em12 smem12 xsem11">Clicked Mail</h4>
<p class="roboto em20 smem20 xsem15 mt0 mb0 text-yellow"><?php echo $total_click?></p>
<hr class="mt0 mb0 xsmb0 xsmt0" />
<div class="xsmt1 mt2">
<a href="<?php echo site_url('autoresponder-stats-report/'.$newsletter_id.'/click')?>" class="bluering em9 smem9 xsem9 w600">View List</a></div>
</div>
<div class="col-md-4 col-sm-4 col-xs-5">
<div id="donut3" data-donut="<?php echo $click_per?>">
</div></div>
</div></div></div></div>

<div class="col-md-6 col-sm-6 mb1 xsmb2 xsmt2 mt0">
<div class="layout-bordered clearfix">
<div class="col-xs-12 padding0">
<div class="row">
<div class="col-md-8 col-sm-8 col-xs-7">
<h4 class="mb0 xsmb0 xsmt0 mt0 mdem11 em12 smem12 xsem11">Unsubscribe</h4>
<p class="roboto em20 smem20 xsem15 mt0 mb0 smart-red"><?php echo $total_unsubscribe?></p>
<hr class="mt0 mb0 xsmb0 xsmt0" />
<div class="xsmt1 mt2">
<a href="<?php echo site_url('autoresponder-stats-report/'.$newsletter_id.'/unsubscribed')?>" class="bluering em9 smem9 xsem9 w600">View List</a></div>
</div>
<div class="col-md-4 col-sm-4 col-xs-5">
<div id="donut4" data-donut="<?php echo $unsubscribe_per?>">
</div></div>
</div></div></div></div>

<div class="col-md-6 col-sm-6 mb1 xsmb2 xsmt2 mt0">
<div class="layout-bordered clearfix">
<div class="col-xs-12 padding0">
<div class="row">
<div class="col-md-8 col-sm-8 col-xs-7">
<h4 class="mb0 xsmb0 xsmt0 mt0 mdem11 em12 smem12 xsem11">Bounced</h4>
<p class="roboto em20 smem20 xsem15 mt0 mb0 text-dark-purple"><?php echo $total_bounced?></p>
<hr class="mt0 mb0 xsmb0 xsmt0" />
<div class="xsmt1 mt2">
<a href="<?php echo site_url('autoresponder-stats-report/'.$newsletter_id.'/bounced')?>" class="bluering em9 smem9 xsem9 w600">View List</a></div>
</div>
<div class="col-md-4 col-sm-4 col-xs-5">
<div id="donut5" data-donut="<?php echo $bounced_per?>">
</div></div>
</div></div></div></div>



</div>

<div class="col-xs-12 mt3 xsmt4">
<div class="w500 pull-left em10 smem10 xsem9">Open & Click</div>
<div class="pull-right w500 em10 smem10 xsem9"><div class="radio radio-inline">
<input type="radio" id="inlineRadio1" value="day" name="radioInline" class="graph_input" >
<label for="inlineRadio1" class="em9 smem9 xsem9"> Day </label></div>
<div class="radio radio-inline">
<input type="radio" id="inlineRadio2" value="month" name="radioInline" class="graph_input" >
<label for="inlineRadio2" class="em9 smem9 xsem9"> Month </label></div>  
<div class="radio radio-inline">
<input type="radio" id="inlineRadio3" value="year" name="radioInline" class="graph_input" >
<label for="inlineRadio3" class="em9 smem9 xsem9"> Year </label></div>  </div>
<div class="clearfix"></div>
<hr class="double-line" />

<div class="graph_div"></div>


</div>
</div></div>




</div>
</div>
</div>
<!--- porgress loader-->
<script>

   var duration   = 500,
    transition = 200;

drawDonutChart(
  '#donut1',
  $('#donut1').data('donut'),
  75,
  75,
  ".45em"
);
drawDonutChart(
  '#donut2',
  $('#donut2').data('donut'),
 75,
  75,
  ".45em"
);
drawDonutChart(
  '#donut3',
  $('#donut3').data('donut'),
  75,
  75,
  ".45em"
);
drawDonutChart(
  '#donut4',
  $('#donut4').data('donut'),
  75,
  75,
  ".45em"
);
drawDonutChart(
  '#donut5',
  $('#donut5').data('donut'),
  75,
  75,
  ".45em"
);
drawDonutChart(
  '#donut6',
  $('#donut6').data('donut'),
  75,
 75,
  ".45em"
);

function drawDonutChart(element, percent, width, height, text_y) {
  width = typeof width !== 'undefined' ? width : 290;
  height = typeof height !== 'undefined' ? height : 290;
  text_y = typeof text_y !== 'undefined' ? text_y : "-.10em";

  var dataset = {
        lower: calcPercent(0),
        upper: calcPercent(percent)
      },
      radius = Math.min(width, height) / 2,
      pie = d3.layout.pie().sort(null),
      format = d3.format(".0%");

  var arc = d3.svg.arc()
        .innerRadius(radius - 6)
        .outerRadius(radius);

  var svg = d3.select(element).append("svg")
        .attr("width", width)
        .attr("height", height)
        .append("g")
        .attr("transform", "translate(" + width / 2 + "," + height / 2 + ")");

  var path = svg.selectAll("path")
        .data(pie(dataset.lower))
        .enter().append("path")
        .attr("class", function(d, i) { return "color" + i })
        .attr("d", arc)
        .each(function(d) { this._current = d; }); // store the initial values

  var text = svg.append("text")
        .attr("text-anchor", "middle")
        .attr("dy", text_y);

  if (typeof(percent) === "string") {
    text.text(percent);
  }
  else {
    var progress = 0;
    var timeout = setTimeout(function () {
      clearTimeout(timeout);
      path = path.data(pie(dataset.upper)); // update the data
      path.transition().duration(duration).attrTween("d", function (a) {
        // Store the displayed angles in _current.
        // Then, interpolate from _current to the new angles.
        // During the transition, _current is updated in-place by d3.interpolate.
        var i  = d3.interpolate(this._current, a);
        var i2 = d3.interpolate(progress, percent)
        this._current = i(0);
        return function(t) {
          text.text( format(i2(t) / 100) );
          return arc(i(t));
        };
      }); // redraw the arcs
    }, 200);
  }
};

function calcPercent(percent) {
  return [percent, 100-percent];
};     
</script>

<script>
/*-------------------------- graph starts here ------------------------------------*/
$('body').delegate(".graph_input", 'click', function() {

    var time_type = $(this).val();
	var newsletter_id = '<?php echo $newsletter_id?>';
	var posturl = site_url+'get-autoresponder-stats-graph';

    $.ajax({
        url: posturl,
        dataType: 'json',
        type: "POST",
        data: {time_type: time_type,newsletter_id:newsletter_id},
		beforeSend: function() { $('.graph_div').html('<img src="<?php echo $this->config->item("templateAssetsPath")?>images/loader.gif" class="center-block" />'); },
        success: function(data) {
                $('.graph_div').html('');
				$('.graph_div').html(data.html);
        },
    });
});
$( document ).ready(function() { 
  $('#inlineRadio1').click();
});
/*-------------------------- graph ends here ------------------------------------*/

</script>