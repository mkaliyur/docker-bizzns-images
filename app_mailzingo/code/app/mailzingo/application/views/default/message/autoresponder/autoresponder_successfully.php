<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padding15 footer-height">
<!-- dash header-->
<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 mb2 xsmb4">
<div class="dash-header">
<div class="row">
<div class="col-md-12 col-sm-12"><h1 class="smart-color em22 smem18 xsem16">Autoresponder</h1>
</div>
<!--<div class="col-md-5 col-sm-5 xstext-right xstext-center mt2 xsmt4 mb0 xsmb2">
<a href="#" class="smart-btn" data-toggle="modal" data-target="#myModal">Create Campaign</a></div> --></div>
</div>
</div>
<!-- dash header end-->

<div class="clearfix"></div>

<div class="col-md-12 col-sm-12 col-xs-12 mb2 mt1 xsmt1 xsmb4">
<div class="layout-box1 clearfix">

<div class="col-xs-12 xsmb2 mb1 xsmt15 mt1 text-center padding0">



<img src="<?php echo $this->config->item('templateAssetsPath')?>images/congratulation.jpg" class="img-responsive center-block" />
<p class="em13 smem13 xsem11">Your Autoresponder Has Been Saved Successfully  .</p>
<div class="curve-line-bg"></div>


<div class="col-md-offset-3 col-md-9">
<div class="col-md-4 col-sm-6 col-xs-6 right-border1 text-center mb4">
<i class="icon-Message center-block em45 smem30 xsem25 smart-grey"></i>
<div class="mt1 xsmt2"><a href="<?php echo site_url('add-autoresponder?add_type=new')?>" class="back-link em12 smem12 xsem9">Create Another Autoresponder</a></div>
</div>
<div class="col-md-4 col-sm-6 col-xs-6 text-center mb4">
<i class="icon-Dashboard center-block em42 smem30 xsem25 smart-grey"></i>
<div class="mt2 xsmt2"><a href="<?php echo site_url('dashboard')?>" class="back-link em12 smem12 xsem9">Go to Dashboard</a></div>
</div>

</div>

</div>
</div>


</div>
</div>