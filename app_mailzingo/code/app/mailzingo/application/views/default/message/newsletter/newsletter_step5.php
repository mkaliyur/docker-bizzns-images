<!-- calender css and js-->
<link rel="stylesheet" href="<?php echo $this->config->item('templateAssetsPath')?>css/daterangepicker.min.css" />
<script src="<?php echo $this->config->item('templateAssetsPath')?>js/jquery.daterangepicker.min.js"></script>
<script type="text/javascript">
var dateToday = new Date();  
$(function(){
$('.date-range0').dateRangePicker(
{
startOfWeek: 'monday',
format: 'DD.MM.YYYY HH:mm',
autoClose: false,
singleMonth: true,
singleDate : true,
startDate: new Date(),
time: {
enabled: true
}
});
});
</script>

<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padding15 footer-height">
<!-- dash header-->
<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 mb2 xsmb4">
<div class="dash-header">
<div class="row">
<div class="col-md-12 col-sm-12"><h1 class="smart-color em22 smem18 xsem16">Newsletter</h1>
<p class="em11 smem10 xsem10">Create comprehensive newsletter and send latest information to your subscribers</p></div>
<!--<div class="col-md-5 col-sm-5 xstext-right xstext-center mt2 xsmt4 mb0 xsmb2">
<a href="#" class="smart-btn" data-toggle="modal" data-target="#myModal">Create Campaign</a></div> --></div>
</div>
</div>
<!-- dash header end-->

<div class="clearfix"></div>
<form action="" method="post">
<div class="col-md-12 col-sm-12 col-xs-12 mb2 mt1 xsmt1 xsmb4">
<div class="layout-box clearfix">
<div class="bs-wizard">
<div class="col-xs-3 bs-wizard-step complete">
<div class="progress"><div class="progress-bar"></div></div>
<a href="<?php echo site_url('add-newsletter?step=1')?>" class="bs-wizard-dot"></a>
<div class="text-center em8 smem9 xsem8 hidden-xs">setting</div>
</div>
                
<div class="col-xs-3 bs-wizard-step complete">
<div class="progress"><div class="progress-bar"></div></div>
<a href="<?php echo site_url('add-newsletter?step=2')?>" class="bs-wizard-dot"></a>
<div class="text-center em8 smem9 xsem8 hidden-xs">Templates</div>
</div>
<div class="col-xs-3 bs-wizard-step complete">
<div class="progress"><div class="progress-bar"></div></div>
<a href="<?php echo site_url('add-newsletter?step=3')?>" class="bs-wizard-dot"></a>
<div class="text-center em8 smem9 xsem8 hidden-xs">Editor</div>
</div>
<div class="col-xs-3 bs-wizard-step complete">
<div class="progress"><div class="progress-bar"></div></div>
<a href="<?php echo site_url('add-newsletter?step=4')?>" class="bs-wizard-dot"></a>
<div class="text-center em8 smem9 xsem8 hidden-xs">Subscriber</div>
</div>
<div class="col-xs-3 bs-wizard-step active">
<div class="progress"><div class="progress-bar"></div></div>
<a href="javascript:" class="bs-wizard-dot"></a>
<div class="text-center em8 smem9 xsem8 hidden-xs">Send Option</div>
</div>
</div>

<div class="col-xs-12 xsmb2 mb1 xsmt15 mt1 padding0 smart-field">
<h1 class="w600 em11 smem11 xsem11 ">Send Option</h1>
<p class="em9 smem9 xsem9 hidden-xs">Here You Can Send Your Message Immediately Or Schedule</p>

<div class="col-xs-12 padding0">
<div class="row">
<div class="col-xs-12 xsmt2 xsmb4 mb0 padding0">
<div class="col-md-4 col-lg-2 col-sm-4 col-xs-5 xsmb2">Email Name</div>
<div class="col-md-4 col-lg-4 col-sm-5 col-xs-7 w600"><?php echo $message_name?></div></div>

<div class="col-xs-12 xsmt2 xsmb4 mb0 padding0">
<div class="col-md-4 col-lg-2 col-sm-4 col-xs-5 xsmb2">Subject</div>
<div class="col-md-4 col-lg-4 col-sm-5 col-xs-7 w600"><?php echo $subject?></div></div>

<?php if($template_name) { ?>
<div class="col-xs-12 xsmt2 xsmb4 mb0 padding0">
<div class="col-md-4 col-lg-2 col-sm-4 col-xs-5 xsmb2">Templates</div>
<div class="col-md-4 col-lg-4 col-sm-5 col-xs-7 w600"><?php echo $template_name?></div></div>
<?php } ?>

<div class="col-xs-12 xsmt2 xsmb4 mb0 padding0">
<div class="col-md-4 col-lg-2 col-sm-4 col-xs-5 xsmb2">Subscribers</div>
<div class="col-md-4 col-lg-4 col-sm-5 col-xs-7 w600" id="contact_count"></div></div>
<input type="hidden" name="contact_count" class="contact_count" value="<?php echo $contact_count?>" />

<div class="col-xs-12 xsmt2 xsmb4 mb0 padding0">
<div class="col-md-4 col-lg-2 col-sm-4 col-xs-12 xsmb2 mtt1">Sending Option</div>
<div class="col-md-4 col-lg-3 col-sm-5 col-xs-12"><select name="message_type" onchange="schedule(this.value)">
<option value="instant" <?php if($message_type=='instant') { ?> selected="selected" <?php } ?>>Immediately</option>
<option value="schedule" <?php if($message_type=='schedule') { ?> selected="selected" <?php } ?>>Schedule</option>
</select>
</div></div>

<div class="ScheduleDiv" style="display:<?php if($message_type=='schedule') { echo 'block'; } else { echo 'none'; } ?>">
<div class="col-xs-12 xsmt2 xsmb4 mb0 padding0">
<div class="col-md-4 col-lg-2 col-sm-4 col-xs-12 xsmb2 mtt1">Date & Time</div>
<div class="col-md-3 col-lg-2 col-sm-4 col-xs-10">
<input class="date-range0" type="text" size="60" value="<?php echo $schedule_time?>" placeholder="DD.MM.YYYY HH:MM" name="schedule_time" />
</div>
<div class="col-md-1 col-lg-1 col-sm-1 mtt1 xsmt2 col-xs-2 padding0">
<i class="fa fa-calendar em22 smem25 xsem20" aria-hidden="true"></i>
</div>
</div>
</div>


</div>
</div>

</div>
</div>

<div class="col-xs-12 col-md-12 col-sm-12 xstext-right xstext-center mt2 xsmt4 padding0">
<a href="<?php echo site_url('add-newsletter?step=4')?>" class="activity-btn" title="Back">Back </a> &nbsp;
<button type="submit" class="smart-btn submit_butt" title="Save To Draft" name="step5draft" value="draft">Save To Draft </button> &nbsp;
<button type="submit" class="smart-btn submit_butt send_butt" title="Send" name="step5" value="step5"><?php if($message_type=='schedule') { echo 'Schedule'; } else { echo 'Send'; } ?> </button> 

</div>
</div>
</form>
</div>
<script>
/*-------------------------- Schedule starts here ---------------------------------------------------*/
function schedule(type){
 
  if(type=='instant')
   { 
     $('.ScheduleDiv').fadeOut('slow');
	 $('.send_butt').text('send');
   }
  else 
   { 
     $('.ScheduleDiv').fadeIn('slow'); 
	 $('.send_butt').text('schedule');
   }
}
/*-------------------------- Schedule starts here ---------------------------------------------------*/

/*-------------------------- error message starts here---------------------------------------------------*/
<?php if(validation_errors()!='' || $error!='') { ?>
new PNotify({
	  title: 'Error',
	  text: <?php echo json_encode(validation_errors())?>,
	  type: 'error'
	});
<?php } ?>
/*-------------------------- error message ends here---------------------------------------------------*/

/*-------------------------- total subscriber starts here ------------------------------------*/
$( document ).ready(function() { 
        
	var posturl = site_url+'get-final-email-count';

	var in_campaign = '<?php echo implode(',',$in_campaign_id)?>';
	  in_campaign = in_campaign.split(',');
	var ex_campaign = '<?php echo implode(',',$ex_campaign_id)?>';
	  ex_campaign = ex_campaign.split(',');
	var supression = '<?php echo implode(',',$sep_list_id)?>';
	  supression = supression.split(',');
	  
	if('<?php echo $contact_count?>')
	 {
	  $('#contact_count').text('<?php echo $contact_count?> Contacts From '+in_campaign.length+' Campaigns');
	  $('.contact_count').val('<?php echo $contact_count?>');
	  return;
	 } 
		
	$.ajax({
	url: posturl,
	dataType: 'json',
	type: "POST",
	data: {in_campaign: in_campaign, ex_campaign: ex_campaign, supression: supression},
	beforeSend: function() {
	  $('#contact_count').html('<img src="<?php echo $this->config->item("templateAssetsPath")?>images/loader.gif" class="center-block" />'); 
	 },
	success: function(data) {
		if (data.success) {
			$('#contact_count').html('');
			$('#contact_count').text(data.contact_count+' Contacts From '+in_campaign.length+' Campaigns');
			$('.contact_count').val(data.contact_count);
		}
	},
  });
      
});
/*-------------------------- total subscriber ends here ------------------------------------*/
</script>				 
