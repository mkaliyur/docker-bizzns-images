<link href="<?php echo $this->config->item('templateAssetsPath')?>css/nv.d3.css" rel="stylesheet" type="text/css">
<script src="https://cdnjs.cloudflare.com/ajax/libs/d3/3.5.2/d3.min.js" charset="utf-8"></script>
<script src="<?php echo $this->config->item('templateAssetsPath')?>js/nv.d3.js"></script>
<script type="text/javascript">
var h = 350;
var r = h/2;
var arc = d3.svg.arc().outerRadius(r);
var data = [
    {"label":"Desktop ", "value":<?php echo $desktop_open?>},
    {"label":"Mobile", "value":<?php echo $mobile_open?>},
   
];
var data1 = [
    {"label":"Chrome ", "value":<?php echo $chrome_open?>},
    {"label":"Mozilla", "value":<?php echo $firefox_open?>},
    {"label":"Internet Explorer ", "value":<?php echo $ie_open?>},
	{"label":"Safari ", "value":<?php echo $safari_open?>},
    {"label":"Others", "value":<?php echo $others_open?>},
];
var colors = [
    '#ffce45',
    '#d56126',
    '#1ebbee',
	'#b8b9bb',
	'#76b476'
]

var count = 0;
function setPiegraph(chartid, chartdata){
	nv.addGraph(function() {
		var chart = nv.models.pieChart()
			.x(function(d) { return d.label })
			.y(function(d) { return d.value })
			.color(colors)
			.showLabels(true)
			.labelType("percent")
	//        .donut(true).donutRatio(0) /* Trick to make the labels go inside the chart*/
		;
		
		d3.select("#"+chartid+" svg")
			.datum(chartdata)
			.transition().duration(1200)
			.call(chart)
		;

		d3.selectAll(".nv-label text")
			/* Alter SVG attribute (not CSS attributes) */
			.attr("transform", function(d){
				d.innerRadius = -150;
				d.outerRadius = r;
				return "translate(" + arc.centroid(d) + ")";}
			)
			.attr("text-anchor", "middle")
			/* Alter CSS attributes */
			.style({"font-size": "1em"})
		;
		
		/* Replace bullets with blocks */
		d3.selectAll('.nv-series').each(function(d,i) {
			var group = d3.select(this);
				circle = group.select('circle');
			var color = circle.style('fill');
			
			count++;
			if(count> data.length){
			circle.remove();
			}
			
			var symbol = group.append('path')
				.attr('d', d3.svg.symbol().type('square'))
				.style('stroke', color)
				.style('fill', color)
				// ADJUST SIZE AND POSITION
				.attr('transform', 'scale(1.5) translate(-2,0)')
			//
		});
		return chart;
	});
}

setPiegraph("chart", data);
setPiegraph("chart1", data1);


$(document).ready(function(){
	
	/* Graph Resize Hack Script */
	$(".navbar-expand-toggle").click(function(){
		setTimeout(function(){ 
			if($(document).width() > 767){
				//alert('12')
				
				//alert($('#chart1').find('.nv-pieChart').html());
				
				//alert($('.nv-pieChart').html());
				//$('.nv-pieChart').attr('transform',"translate(0 5)");
				
				//window.dispatchEvent(new Event('resize'));
				setPiegraph("chart", data);
				setPiegraph("chart1", data1);
			}
		}, 700);
		
    });
	/* Graph Resive Hack Script */
	
});

</script>
<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padding15 footer-height">
<!-- dash header-->
<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 mb2 xsmb4">
<div class="dash-header">
<div class="row">
<div class="col-md-12 col-sm-12"><h1 class="smart-color em22 smem18 xsem16">Statistics</h1>
<p class="em11 smem10 xsem10">Here are the stats for the newsletter mailed</p></div>
<!--<div class="col-md-5 col-sm-5 xstext-right xstext-center mt2 xsmt4 mb0 xsmb2">
<a href="#" class="smart-btn">Create Newsletter</a></div> --> </div>
</div>
</div>
<!-- dash header end-->
<div class="clearfix"></div>
<div class="col-md-11 col-sm-10 col-xs-8 smart-tabs clearfix">
<ul>
<li><a href="<?php echo site_url('newsletter-stats/'.$newsletter_id.'/overview')?>" title="Overview">
<span class="hidden-xs">Overview </span><i class="icon-Overview visible-xs"></i>
</a></li>
<li><a href="<?php echo site_url('newsletter-stats/'.$newsletter_id.'/technology')?>" title="Technology" class="active">
<span class="hidden-xs">Technology</span><i class="icon-Technology visible-xs"></i>
</a></li>
<li><a href="<?php echo site_url('newsletter-stats/'.$newsletter_id.'/location')?>" title="Location">
<span class="hidden-xs">Location</span><i class="icon-Location visible-xs"></i>
</a></li>
</ul>
</div>
<div class="col-md-1 col-sm-2 col-xs-4 text-right">
<a href="<?php echo site_url('newsletter-stats')?>" class="back-link">
<i class="fa fa-arrow-left" aria-hidden="true"></i> &nbsp;Back</a></div>
<div class="col-md-12 col-sm-12 col-xs-12 mb2 xsmb4">
<div class="layout-box clearfix">
<div class="col-xs-12 col-md-12 col-sm-12 mt1 mb2 xsmb3 w600 em11 smem11 xsem11 padding0">
<?php echo $newsletter_detail->message_name?></div>
<div class="clearfix"></div>
<div class="col-xs-12 padding0">
<div class="row">
<div class="col-md-6 col-sm-6">
<div class="layout-border clearfix">
<h4 class="em15 smen14 xsem12 mt0 xsmt0">Reading Environment</h4>
<?php if($desktop_open==0 && $mobile_open==0) { ?>
<div>No Record Found</div>
<?php } else { ?>
<div id="chart">
  <svg></svg>
</div>
<?php } ?>
</div>
</div>
<div class="col-md-6 col-sm-6 mt0 xsmt2">
<div class="layout-border clearfix">
<h4 class="em15 smen14 xsem12 mt0 xsmt0">Browser Stats</h4>
<?php if($chrome_open==0 && $firefox_open==0 && $ie_open==0 && $safari_open==0 && $others_open==0) { ?>
<div>No Record Found</div>
<?php } else { ?>
<div id="chart1">
  <svg></svg>
</div>
<?php } ?>
</div>
</div>
</div>
</div>
</div>
</div>
</div>