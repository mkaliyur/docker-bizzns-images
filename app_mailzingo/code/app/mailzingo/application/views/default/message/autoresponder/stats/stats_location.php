<!--- jvector map--->
<link rel="stylesheet" href="<?php echo $this->config->item('templateAssetsPath')?>css/jquery-jvectormap-2.0.3.css" type="text/css" media="screen"/>
<script type="text/javascript" src="<?php echo $this->config->item('templateAssetsPath')?>js/map/jvectormap-2.0.3.min.js"></script>
<script type="text/javascript" src="<?php echo $this->config->item('templateAssetsPath')?>js/map/jvectormap-world-mill.js"></script>



<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padding15 footer-height">
<!-- dash header-->
<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 mb2 xsmb4">
<div class="dash-header">
<div class="row">
<div class="col-md-12 col-sm-12"><h1 class="smart-color em22 smem18 xsem16">Statistics</h1>
<p class="em11 smem10 xsem10">Here are the stats for the mails sent by autoresponder</p></div>
<!--<div class="col-md-5 col-sm-5 xstext-right xstext-center mt2 xsmt4 mb0 xsmb2">
<a href="#" class="smart-btn">Create Newsletter</a></div> --> </div>
</div>
</div>
<!-- dash header end-->

<div class="clearfix"></div>
<div class="col-md-11 col-sm-10 col-xs-8 smart-tabs clearfix">

<ul>
<li><a href="<?php echo site_url('autoresponder-stats/'.$newsletter_id.'/overview')?>" title="Overview">
<span class="hidden-xs">Overview </span><i class="icon-Overview visible-xs"></i>
</a></li>
<li><a href="<?php echo site_url('autoresponder-stats/'.$newsletter_id.'/technology')?>" title="Technology">
<span class="hidden-xs">Technology</span><i class="icon-Technology visible-xs"></i>
</a></li>
<li><a href="<?php echo site_url('autoresponder-stats/'.$newsletter_id.'/location')?>" title="Location" class="active">
<span class="hidden-xs">Location</span><i class="icon-Location visible-xs"></i>
</a></li>
</ul>
</div>
<div class="col-md-1 col-sm-2 col-xs-4 text-right">
<a href="<?php echo site_url('autoresponder-stats')?>" class="back-link">
<i class="fa fa-arrow-left" aria-hidden="true"></i> &nbsp;Back</a></div>

<div class="col-md-12 col-sm-12 col-xs-12 mb2 xsmb4">
<div class="layout-box clearfix">
<div class="col-xs-12 col-md-12 col-sm-12 mt1 mb2 xsmb3 w600 em11 smem11 xsem11 padding0">
Message Name <span class="smart-color"><?php echo $newsletter_detail->auto_name?></span></div>
<div class="clearfix"></div>
<div class="col-xs-12 padding0">
<h4 class="em15 smen14 xsem12 mt0">Geographic Stat</h4>
<figure>
<div id="world-map-gdp" class="world-map-width"></div>
<script type="text/javascript">
$(function(){
var gdpData = <?php echo $location_array?>;
$('#world-map-gdp').vectorMap({
map: 'world_mill',
series: {
regions: [{
values: gdpData,
scale: ['#addfc7', '#60ad60'],
normalizeFunction: 'polynomial'
}]
},

onRegionTipShow: function(e, el, code){
el.html(el.html()+' (Open - '+gdpData[code]+')');
}
});
});
</script>
</figure>

</div>
</div>

<div class="col-xs-12 visible-xs text-center xsem12 greysection w600">
<a href="<?php echo site_url('autoresponder-stats')?>" class="backbtn">Back</a>
</div>
</div>
</div>