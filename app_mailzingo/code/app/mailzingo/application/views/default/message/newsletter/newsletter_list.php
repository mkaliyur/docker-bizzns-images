<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padding15 footer-height">
<!-- dash header-->
<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 mb2 xsmb4">
<div class="dash-header">
<div class="row">
<div class="col-md-7 col-sm-7"><h1 class="smart-color em22 smem18 xsem16">Newsletter</h1>
<p class="em11 smem10 xsem10">Create comprehensive newsletter and send latest information to your subscribers</p></div>
<div class="col-md-5 col-sm-5 xstext-right xstext-center mt2 xsmt4 mb0 xsmb2">
<a href="<?php echo site_url('add-newsletter?add_type=new')?>" class="smart-btn">Create Newsletter</a></div> </div>
</div>
</div>
<!-- dash header end-->

<div class="clearfix"></div>
<div class="col-md-12 smart-tabs clearfix">
<ul>
<li><a href="<?php echo site_url('newsletter-list/sent')?>"  <?php if($status=='sent'){ ?> class="active" <?php } ?> title="Sent Mail">
<span class="hidden-xs">Sent Mail <span class="roboto">(<?php echo $sentCount?>)</span></span><i class="icon-Sent-Mail visible-xs"></i>
</a></li>
<li><a href="<?php echo site_url('newsletter-list/scheduled')?>"  <?php if($status=='scheduled'){ ?> class="active" <?php } ?>  title="Scheduled Mail">
<span class="hidden-xs">Scheduled Mail <span class="roboto">(<?php echo $scheduledCount?>)</span></span><i class="icon-Scheduled visible-xs"></i>
</a></li>
<li><a href="<?php echo site_url('newsletter-list/draft')?>"  <?php if($status=='draft'){ ?> class="active" <?php } ?>  title="Draft Mail">
<span class="hidden-xs">Draft Mail <span class="roboto">(<?php echo $draftCount?>)</span></span><i class="icon-Drafts visible-xs"></i>
</a></li>
<li><a href="<?php echo site_url('newsletter-list/process')?>"  <?php if($status=='process'){ ?> class="active" <?php } ?>  title="Processing Mail">
<span class="hidden-xs">Processing Mail <span class="roboto">(<?php echo $processCount?>)</span></span><i class="icon-Process visible-xs"></i>
</a></li>
</ul>
</div>


<div class="col-md-12 col-sm-12 col-xs-12 mb2 xsmb4">
<div class="layout-box1 clearfix mailer-data">
<div class="box-padding">
<div class="col-xs-12 col-md-6 col-sm-6 mt1 mb2 xsmb3 w600 em11 smem11 xsem11 padding0"><?php echo ucfirst($status)?> Mail</div>
<div class="col-xs-12 col-md-6 col-sm-6 mb2 xsmb2 text-right template-search padding0">
<div class="col-md-offset-6 col-sm-offset-2"><input type="text" placeholder="Message Name" class="search"><button type="button"><i class="fa fa-search"></i></button></div>
</div></div>



<div class="clearfix"></div>
<div class="table-responsive table-data table-fixed-header gallery contentDiv newsletter-coluom" id="autoscroll">
<table width="100%" border="0" cellspacing="0" cellpadding="0" id="tbl">
    <thead>
    <tr class="w600">
    <th class="text-center"><input name="" type="checkbox" value="all" id="select_all"/></th>
    <th>Newsletter</th>
    <th class="text-center">Created Date</th>
	<?php if($status=='scheduled') { ?>
	<th class="text-center" >Schedule Date</th>
	<?php } ?>

    <th class="text-center">Action</th>
	
  </tr>
       <tr class="noresult" style="display:  <?php if($total_data==0) { ?> <?php } else { ?>none <?php } ?>;">
      <th colspan="5" class="text-center w3000">No Record Found</th>
  </tr>

</thead>
<tbody id="tbltbody1">
 
  </tbody>
</table>
</div>
</div>

<div class="col-xs-12 xstext-right xstext-center mt2 xsmt8 padding0">
<a data-toggle="modal" data-target="#delete" class="disableGreybtn multi_action_btn deleteMultiLink activity-btn" title="Delete">Delete</a>
</div>


</div>
</div>

<!-- Test Mail modalpopup-->
<div id="testMail" class="modal fade" role="dialog">
  <div class="modal-dialog campaign-modal">
    <div class="modal-content">
      <div class="modal-body padding0 clearfix xsmb4 xsmt4 mt9 mb5">
       <div class="col-md-5 col-sm-5 col-xs-12 mt5 xsmt2">
       <div class="col-xs-12 bottom-border">
      
       <img src="<?php echo $this->config->item('templateAssetsPath')?>images/testmail.png" class="img-responsive center-block mobile-img" /></div>
       </div>
       <div class="col-md-7 col-sm-7 col-xs-12 left-border campaign-field xsmt4 mt0 ">
       
       <h3 class="em16 smem16 xsem14">Send Test Mail</h3>
        <div class="form-group mt18 xsmt12 mb25 xsmb0">
		<label class="w300 sr-only">Campaign Name</label>
		<input type="text" name="testemail" placeholder="abc@sitename.com"  class="form-control testemail" />
		<input type="hidden" name="newsletter_id" value="" id="newsletter_id" />
		
        <p class="em10 smem9 xsem9 mt4 xsmt2"><span class="smart-red">Note : </span> Test Your Message
By Delivering Them To Your Email Address.</p>
        </div>
        <div class="text-right mt12 xsmt12 xsmb4 xstext-right xstext-center"><a href="#" class="blank-btn" data-dismiss="modal">Cancel</a> <a href="javascript:" class="smart-btn sendTestmail">Send</a></div>
       </div>
      </div>
    </div>
  </div>
</div>

<script>
/*-------------------------- delete single newsletter starts here ------------------------------------*/
$('body').delegate(".deletelink", 'click', function() {
    
	deleteurl = $(this).attr('data-href');
	 $('#delete_yes_button').attr('href',deleteurl);
	 $('#delete_yes_button').removeClass('deleteMultiple');
});
/*-------------------------- delete single newsletter ends here ------------------------------------*/

/*-------------------------- check/uncheck multiple newsletter starts here ------------------------------------*/
$('body').delegate("#select_all", 'change', function() {
      $(".select_checkbox").prop('checked', $(this).prop("checked"));
	  $(".select_checkbox").change();
      });
$('body').delegate(".select_checkbox", 'change', function() {
	  var length = $('.select_checkbox:checked').length
	  if(length==0)
	   {
		  $(".multi_action_btn").addClass('disableGreybtn');
	   } else {
		  $(".multi_action_btn").removeClass('disableGreybtn');
	   }
      });
/*-------------------------- check/uncheck multiple newsletter ends here ------------------------------------*/

/*-------------------------- delete multiple newsletter starts here ------------------------------------*/
$('body').delegate(".deleteMultiLink", 'click', function() {
    
	var length = $('.select_checkbox:checked').length;
	if(length==0)
	{ return false; }
	deleteurl = $(this).attr('data-href');
	 $('#delete_yes_button').attr('href','javascript:');
	 $('#delete_yes_button').addClass('deleteMultiple');
});



$('body').delegate(".deleteMultiple", 'click', function() {

    var posturl = site_url+'delete-multiple-newsletter';
	var id = [];
	$(".select_checkbox:checkbox:checked").each(function (e) {
            id[e] = $(this).val();
        });
	var length = $('.select_checkbox:checked').length;
	if(length==0)
	{ return false; }

    $.ajax({
        url: posturl,
        dataType: 'json',
        type: "POST",
        data: {id: id},
        success: function(data) {
            if (data.success) {
			    $('#delete').modal('toggle');
				//PNotify.removeAll();
				//new PNotify({title: 'Success',text: 'Newsletter Deleted successfully',type: 'success'});
				location.reload(); 
                //$(".contentDiv").load(location.href + " .contentDiv"); 
            }
        },
    });
});
/*-------------------------- delete multiple newsletter ends here------------------------------------*/

/*-------------------------- Search function starts here ---------------------------------------------------*/
$('body').delegate(".search", 'keyup', function() {
	var searchVal = $(this).val();
	$( ".looptr:contains('"+searchVal+"')" ).show();
	$('.looptr:not(:contains('+ searchVal +'))').hide(); 
	$( ".noresult").hide();
         if($( ".looptr:contains('"+searchVal+"')" ).length === 0){ 
            $( ".noresult").show();
            $('#select_all').attr('disabled',true);
        }else{
            $('#select_all').attr('disabled',false);
        }
	
});
/*-------------------------- Search function ends here ---------------------------------------------------*/

/*-------------------------- Send test mail starts here ---------------------------------------------------*/
$('body').delegate(".testmailA", 'click', function() {
    var newsletter_id = $(this).attr('data-newsletter_id');
    $('#newsletter_id').val(newsletter_id);
});

$('body').delegate(".sendTestmail", 'click', function() {
        var posturl = site_url+'send-test-mail-out';
        var newsletter_id = $('#newsletter_id').val();
		var testemail = $('.testemail').val();
		
        $.ajax({
        url: posturl,
        dataType: 'json',
        type: "POST",
        data: {newsletter_id: newsletter_id,testemail: testemail},
        success: function(data) {
            if (data.error) {
			    PNotify.removeAll();
			    new PNotify({title: 'Error',text: data.error,type: 'error'});
            } else {
			    $('#testMail').modal('toggle');
			    PNotify.removeAll();
				new PNotify({title: 'Success',text: 'Mail Sent Successfully',type: 'success'});
			}
			
        },
      });
    
});
/*-------------------------- Send test mail ends here ---------------------------------------------------*/

/*-------------------------- success message starts here---------------------------------------------------*/
<?php if($this->session->userdata('success_msg')) { ?>
new PNotify({
	  title: 'Success',
	  text: '<?php echo $this->session->userdata('success_msg')?>',
	  type: 'success'
	});
<?php $this->session->unset_userdata('success_msg'); } ?>
/*-------------------------- success message ends here---------------------------------------------------*/

/*-------------------------- error message starts here---------------------------------------------------*/
<?php if($this->session->userdata('error_msg')) { ?>
new PNotify({
	  title: 'Error',
	  text: '<?php echo $this->session->userdata('error_msg')?>',
	  type: 'error'
	});
<?php $this->session->unset_userdata('error_msg'); } ?>
/*-------------------------- error message ends here---------------------------------------------------*/

$(document).ready(function() {
var total_record = 0;
var total_groups = '<?php echo $total_data; ?>';
//var id = '<?php // echo $id; ?>';
var status = '<?php echo $status; ?>';
var prestat = true;
$('#tbl > tbody').load("<?php echo base_url() ?>newsletter-pagination",
{
	'group_no': total_record,
	'status':status
}, 
function() { 
	total_record++;
	$gallery = $('.gallery a').simpleLightbox();
});
  
$('#tbltbody1').scroll(function() {       
     if($('#tbltbody1').scrollTop() + $('#tbl').height() > $('#tbltbody1').prop('scrollHeight') && prestat) 
    {       
        if(total_record <= total_groups)
        {
          loading = true; 
          $('.loader_image').show(); 
		   prestat = false;
		  
          $.post('<?php echo site_url() ?>newsletter-pagination',{'group_no': total_record,'status':status},
            function(data){ 
                if (data != "") {
                    $("#tbl > tbody").append(data);                 
                    $('.loader_image').hide();                  
                    total_record++;
					prestat = true;
					$gallery = $('.gallery a').simpleLightbox();
                }
            });     
        }
    }
});
});

/*-------------------------- success message starts here---------------------------------------------------*/
<?php if($this->session->userdata('success_msg')) { ?>
new PNotify({
	  title: 'Success',
	  text: '<?php echo $this->session->userdata('success_msg')?>',
	  type: 'success'
	});
<?php $this->session->unset_userdata('success_msg'); } ?>
/*-------------------------- success message ends here---------------------------------------------------*/

</script>