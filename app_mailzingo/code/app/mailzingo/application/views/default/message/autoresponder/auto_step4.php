<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padding15 footer-height">
<!-- dash header-->
<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 mb2 xsmb4">
<div class="dash-header">
<div class="row">
<div class="col-md-12 col-sm-12"><h1 class="smart-color em22 smem18 xsem16">Autoresponder</h1>
<p class="em11 smem10 xsem10">Send mails to your subscribers automatically</p></div>
<!--<div class="col-md-5 col-sm-5 xstext-right xstext-center mt2 xsmt4 mb0 xsmb2">
<a href="#" class="smart-btn" data-toggle="modal" data-target="#myModal">Create Campaign</a></div> --></div>
</div>
</div>
<!-- dash header end-->

<div class="clearfix"></div>
<form action="" method="post">
<div class="col-md-12 col-sm-12 col-xs-12 mb2 mt1 xsmt1 xsmb4">
<div class="layout-box clearfix">
<div class="bs-wizard">
<div class="col-xs-3 bs-wizard-step complete">
<div class="progress"><div class="progress-bar"></div></div>
<a href="<?php echo site_url('add-autoresponder?step=1')?>" class="bs-wizard-dot"></a>
<div class="text-center em8 smem9 xsem8 hidden-xs">setting</div>
</div>
                
<div class="col-xs-3 bs-wizard-step complete">
<div class="progress"><div class="progress-bar"></div></div>
<a href="<?php echo site_url('add-autoresponder?step=2')?>" class="bs-wizard-dot"></a>
<div class="text-center em8 smem9 xsem8 hidden-xs">Templates</div>
</div>
<div class="col-xs-3 bs-wizard-step complete">
<div class="progress"><div class="progress-bar"></div></div>
<a href="<?php echo site_url('add-autoresponder?step=3')?>" class="bs-wizard-dot"></a>
<div class="text-center em8 smem9 xsem8 hidden-xs">Editor</div>
</div>
<div class="col-xs-3 bs-wizard-step complete active">
<div class="progress"><div class="progress-bar"></div></div>
<a href="<?php if($auto_session['step4']=='yes') { echo site_url('add-autoresponder?step=4'); } else { echo 'javascript:'; } ?>" class="bs-wizard-dot"></a>
<div class="text-center em8 smem9 xsem8 hidden-xs">Subscriber</div>
</div>
<div class="col-xs-3 bs-wizard-step  <?php if($auto_session['step5']!='yes') { ?> disabled <?php } ?>">
<div class="progress"><div class="progress-bar"></div></div>
<a href="<?php if($auto_session['step5']=='yes') { echo site_url('add-autoresponder?step=5'); } else { echo 'javascript:'; } ?>" class="bs-wizard-dot"></a>
<div class="text-center em8 smem9 xsem8 hidden-xs">Send Option</div>
</div>
</div>

<div class="col-xs-12 xsmb2 mb1 xsmt15 mt1 padding0">
<h1 class="mb2 xsmb3 w600 em11 smem11 xsem11 hidden-xs">Choose Campaigns</h1>

<div class="col-xs-12 padding0">
<div class="row">
<div class="col-md-12 col-sm-12 w600 em11 smem11 xsem10 xsmt4 mt0">

<div class="campaign-heading">
<input name="all_campaign" type="checkbox" value="all" id="in_select_all"/> &nbsp; &nbsp;Campaign 
</div>
<div class="subscriber-list data-scroll">
<ul>
<?php foreach($campaign_list as $valCamp) { ?>
<li><input name="in_campaign_id[]" type="checkbox" value="<?php echo $valCamp->campaign_id?>" class="in_campaign_ckbox" <?php if(in_array($valCamp->campaign_id,$in_campaign_id)) { ?> checked="checked" <?php } ?>/> &nbsp; &nbsp;<?php echo $valCamp->campaign_name?> </li>
<?php } ?>
</ul>
</div>
</div>

</div>
</div>
</div>
</div>
<div class="col-xs-12 col-md-12 col-sm-12 xstext-right xstext-center mt2 xsmt4 padding0">
<a href="<?php echo site_url('add-autoresponder?step=3')?>" class="blank-btn" title="Back">Back </a> &nbsp;

<button type="submit" class="smart-btn" title="Next" name="step4" value="step4">Next </button>
</div>
</div>
</form>
</div>
<script>
$(document).ready(function(){
$("#in_select_all").change(function(){
      $(".in_campaign_ckbox").prop('checked', $(this).prop("checked"));
	  $(".in_campaign_ckbox").change();
});

$(document ).on( "change",".in_campaign_ckbox", function() {
	   if ($('.in_campaign_ckbox:checked').length != $('.in_campaign_ckbox').length) {
        $("#in_select_all").prop('checked', false);
      }else{
        $("#in_select_all").prop('checked', true);
      }
});

});
	  
/*-------------------------- error message starts here---------------------------------------------------*/
<?php if(validation_errors()!='' || $error!='') { ?>
new PNotify({
	  title: 'Error',
	  text: <?php echo json_encode(validation_errors())?>,
	  type: 'error'
	});
<?php } ?>
/*-------------------------- error message ends here---------------------------------------------------*/
</script>