<!-- image hover-->
<script src="<?php echo $this->config->item('templateAssetsPath')?>js/modernizr.js"></script>
<script>
    $(document).ready(function(){
        if (Modernizr.touch) {
            // show the close overlay button
            $(".close-overlay").removeClass("hidden");
            // handle the adding of hover class when clicked
            $(".img").click(function(e){
                if (!$(this).hasClass("hover")) {
                    $(this).addClass("hover");
                }
            });
            // handle the closing of the overlay
            $(".close-overlay").click(function(e){
                e.preventDefault();
                e.stopPropagation();
                if ($(this).closest(".img").hasClass("hover")) {
                    $(this).closest(".img").removeClass("hover");
                }
            });
        } else {
            // handle the mouseenter functionality
            $(".img").mouseenter(function(){
                $(this).addClass("hover");
            })
            // handle the mouseleave functionality
            .mouseleave(function(){
                $(this).removeClass("hover");
            });
        }
    });
</script>

<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padding15 footer-height">
<!-- dash header-->
<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 mb2 xsmb4">
<div class="dash-header">
<div class="row">
<div class="col-md-12 col-sm-12"><h1 class="smart-color em22 smem18 xsem16">Newsletter</h1>
<p class="em11 smem10 xsem10">Create comprehensive newsletter and send latest information to your subscribers</p></div>
</div>
</div>
</div>
<!-- dash header end-->

<div class="clearfix"></div>
<div class="col-md-12 multi-tabs-nav clearfix">

<ul class="nav nav-tabs">
<?php if(sizeof($defaultTemplate)!=0) { ?>
<li class="active">
<a data-toggle="tab" href="#templates"><span class="hidden-xs">Templates</span><i class="icon-Template xsem16 smem14 em14 visible-xs"></i></a>
</li>
<?php } ?>
<li <?php if(sizeof($defaultTemplate)==0) { ?> class="active" <?php } ?>>
<a data-toggle="tab" href="#draft"><span class="hidden-xs">Draft</span><i class="icon-Drafts xsem16 smem14 em14 visible-xs"></i></a>
</li>
</ul>
</div>


<div class="col-md-12 col-sm-12 col-xs-12 mb2 xsmb4">
<div class="layout-box1 clearfix">
<div class="bs-wizard">
<div class="col-xs-3 bs-wizard-step complete">
<div class="progress"><div class="progress-bar"></div></div>
<a href="<?php echo site_url('add-newsletter?step=1')?>" class="bs-wizard-dot"></a>
<div class="text-center em8 smem9 xsem8 hidden-xs">setting</div>
</div>
                
<div class="col-xs-3 bs-wizard-step active">
<div class="progress"><div class="progress-bar"></div></div>
<a href="<?php if($newsletter_session['step2']=='yes') { echo site_url('add-newsletter?step=2'); } else { echo 'javascript:'; } ?>" class="bs-wizard-dot"></a>
<div class="text-center em8 smem9 xsem8 hidden-xs">Templates</div>
</div>
<div class="col-xs-3 bs-wizard-step  <?php if($newsletter_session['step3']!='yes') { ?> disabled <?php } ?>">
<div class="progress"><div class="progress-bar"></div></div>
<a href="<?php if($newsletter_session['step3']=='yes') { echo site_url('add-newsletter?step=3'); } else { echo 'javascript:'; } ?>" class="bs-wizard-dot"></a>
<div class="text-center em8 smem9 xsem8 hidden-xs">Editor</div>
</div>
<div class="col-xs-3 bs-wizard-step  <?php if($newsletter_session['step4']!='yes') { ?> disabled <?php } ?>">
<div class="progress"><div class="progress-bar"></div></div>
<a href="<?php if($newsletter_session['step4']=='yes') { echo site_url('add-newsletter?step=4'); } else { echo 'javascript:'; } ?>" class="bs-wizard-dot"></a>
<div class="text-center em8 smem9 xsem8 hidden-xs">Subscriber</div>
</div>
<div class="col-xs-3 bs-wizard-step  <?php if($newsletter_session['step5']!='yes') { ?> disabled <?php } ?>">
<div class="progress"><div class="progress-bar"></div></div>
<a href="<?php if($newsletter_session['step5']=='yes') { echo site_url('add-newsletter?step=5'); } else { echo 'javascript:'; } ?>" class="bs-wizard-dot"></a>
<div class="text-center em8 smem9 xsem8 hidden-xs">Send Option</div>
</div>
</div>
<form action="" method="post" id="templateform">
<div class="tab-content">
<?php if(sizeof($defaultTemplate)!=0) { ?>
<div id="templates" class="tab-pane fade in active">
<div class="box-padding xsmt15 mt4">


<div class="col-xs-12 col-lg-12 col-md-12 col-sm-12 text-right xsmb2 mb2 padding0 template-search">
<div class="col-lg-3 col-md-5 col-sm-5 col-xs-12 pull-right padding0">
<input type="text" placeholder="Template Name" class="search"><button type="button"><i class="fa fa-search"></i></button></div>
</div>

</div>
<div class="col-xs-12 padding0 line-bottom"></div>
<div class="clearfix"></div>
<div class="col-xs-12 box-padding xsmb2 mb1 xsmt2 gallery data-scroll">
<div class="row">
<div id="effect-2" class="effects clearfix">
<?php foreach($defaultTemplate as $key=>$valDefault) { $key++; ?>
<div class="col-xs-6 col-md-3 col-sm-3 col-lg-2 mb2 xsmb4 defaultDiv">
<p class="xsem8 em10 smem10 more-word"><?php echo $valDefault->template_name?></p>
<div class="img tem-hover" id="element1">   
<?php if($template_id==$valDefault->id) { ?>
<i class="fa fa-check template-active-icon" aria-hidden="true"></i>
<?php } ?>
<img src="<?php echo site_url('assets/uploads/template_images/'.$valDefault->template_screenshot)?>" class="img-responsive template-img center-block" title="Beautiful Image" style="min-height:200px; height:200px" />
<div class="overlay">
<div class="expand">
<a href="<?php echo site_url('assets/uploads/template_images/'.$valDefault->template_screenshot)?>" title="<?php echo $valDefault->template_name?>"><i class="fa fa-search-plus" aria-hidden="true" title="<?php echo $valDefault->template_name?>" ></i></a>
</div></div></div>
<div id="element2"><a href="javascript:" class="template-btn mt8 xsmt8 smem9 xsem8 em10 selectTemplate" title="<?php echo $valDefault->template_name?>" data-id="<?php echo $valDefault->id?>">Select Template</a></div>
</div>
<?php } ?>

</div>
</div>


</div>
</div>
<div class="noresultdefault text-center col-xs-12 mb2 xsmb4" style="display:  <?php if(sizeof($defaultTemplate)==0) { ?> block <?php } else { ?>none <?php } ?>;">No Record Found</div>
<?php } ?>
<div id="draft" class="tab-pane fade <?php if(sizeof($defaultTemplate)==0) { ?> in active <?php } ?>">
<div class="box-padding xsmt15 mt4">
<div class="col-xs-12 col-lg-12 col-md-12 col-sm-12 col-md-offset-0 col-lg-offset-0 col-xs-offset-0 text-right xsmb2 mb0 padding0 template-search">
<div class="col-lg-3 col-md-5 col-sm-5 col-xs-12 pull-right padding0">
<input type="text" placeholder="Template Name" class="searchdraft"><button type="button"><i class="fa fa-search"></i></button></div>
</div>
</div>
<div class="col-xs-12 padding0 mt1 line-bottom"></div>
<div class="clearfix"></div>
<div class="col-xs-12 box-padding xsmb2 mb1 xsmt2 gallery data-scroll">
<div class="row">
<div id="effect-2" class="effects clearfix">
<?php foreach($draftTemplate as $valDraft) { ?>
<div class="col-xs-6 col-md-3 col-sm-3 col-lg-2 mb2 xsmb4 draftDiv">
<p class="xsem8 em10 smem10 more-word"><?php echo $valDraft->template_name?></p>

<div class="img tem-hover" id="element1"> 
<?php if($template_id==$valDraft->id) { ?>
<i class="fa fa-check template-active-icon" aria-hidden="true"></i>
<?php } ?>
<img src="<?php echo site_url('assets/uploads/template_images/'.$valDraft->template_screenshot)?>" class="img-responsive template-img center-block" title="<?php echo $valDraft->template_name?>"  style="min-height:200px; height:200px"/>
<div class="overlay">
<div class="expand">
<a href="<?php echo site_url('assets/uploads/template_images/'.$valDraft->template_screenshot)?>" title="<?php echo $valDraft->template_name?>"><i class="fa fa-search-plus" aria-hidden="true" title="<?php echo $valDraft->template_name?>" ></i></a>
</div></div></div>
<div id="element2"><a href="javascript:" class="template-btn mt8 xsmt8 smem9 xsem8 em10 selectTemplate" title="<?php echo $valDraft->template_name?>" data-id="<?php echo $valDraft->id?>">Select Template</a></div>
</div>
<?php } ?>
<div class="noresultdraft text-center col-xs-12 mb2 xsmb4" style="display:  <?php if(sizeof($draftTemplate)==0) { ?> block <?php } else { ?>none <?php } ?>;">No Record Found</div>
</div>
</div>
</div>
</div>
</div>
<input type="hidden" name="template_id" value="" id="template_input" />
<input type="hidden" name="step2" value="yes" />
</form>
</div>

<div class="col-xs-12 xstext-right xstext-center mt2 xsmt8 padding0">
<a href="<?php echo site_url('add-newsletter?step=1')?>" class="blank-btn" title="Back">Back </a>  
</div>
</div>

</div>
<script>
$('body').delegate(".selectTemplate", 'click', function() {

	var template_id = $(this).attr('data-id');
	$('#template_input').val(template_id);
	$('#templateform').submit();
	
});

/*-------------------------- Search function starts here ---------------------------------------------------*/
$('body').delegate(".search", 'keyup', function() {
	var searchVal = $(this).val();
	$( ".defaultDiv:contains('"+searchVal+"')" ).show();
	$('.defaultDiv:not(:contains('+ searchVal +'))').hide(); 
	 $( ".noresultdefault").hide();
         if($( ".defaultDiv:contains('"+searchVal+"')" ).length === 0){ 
            $( ".noresultdefault").show();
        }
	
});
$('body').delegate(".searchdraft", 'keyup', function() {
	var searchVal = $(this).val();
	$( ".draftDiv:contains('"+searchVal+"')" ).show();
	$('.draftDiv:not(:contains('+ searchVal +'))').hide();
	 $( ".noresultdraft").hide();
         if($( ".draftDiv:contains('"+searchVal+"')" ).length === 0){ 
            $( ".noresultdraft").show();
        } 
	
});
/*-------------------------- Search function ends here ---------------------------------------------------*/


/*-------------------------- error message starts here---------------------------------------------------*/
<?php if(validation_errors()!='' || $error!='') { ?>
new PNotify({
	  title: 'Error',
	  text: <?php echo json_encode(validation_errors())?>,
	  type: 'error'
	});
<?php } ?>
/*-------------------------- error message ends here---------------------------------------------------*/
</script>