<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padding15 footer-height">
<!-- dash header-->
<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 mb2 xsmb4">
<div class="dash-header">
<div class="row">
<div class="col-md-8 col-lg-9 col-sm-7"><h1 class="smart-color em22 smem18 xsem16">Autoresponder</h1>
<p class="em11 smem10 xsem10">Send mails to your subscribers automatically</p></div>
<div class="col-md-4 col-lg-3 col-sm-5 xstext-right xstext-center mt2 xsmt4 mb0 xsmb2">
<a href="<?php echo site_url('add-autoresponder?add_type=new')?>" class="smart-btn">Create Autoresponder</a></div> </div>
</div>
</div>
<!-- dash header end-->

<div class="clearfix"></div>

<div class="col-md-12 col-sm-12 col-xs-12 mb2 xsmb4">
<div class="layout-box1 clearfix mailer-data">
<div class="box-padding">
<div class="col-md-offset-7 col-md-5 col-sm-offset-5 col-sm-7 col-xs-offset-0 col-xs-12 mb2 xsmb2 template-search padding0">
<div class="col-md-5 col-sm-5 col-xs-5 mt2 xsmt3 xstext-right xstext-center"> </div>
<div class="col-md-7 col-sm-7 col-xs-7 padding0"><input type="text" placeholder="Autoresponder Name" class="search"><button type="button"><i class="fa fa-search"></i></button></div>
</div></div>


<div class="clearfix"></div>


<div class="table-responsive table-data table-fixed-header gallery contentDiv autoresponder-coluom" id="autoscroll">
<table width="100%" border="0" cellspacing="0" cellpadding="0" id="tbl">
<thead>
  <tr class="w600">
    <th class="text-center table-header-checkbox"><input name="" type="checkbox" value="all" id="select_all"  /></th>
    <th>Autoresponder</th>
    <th class="text-center">Created Date</th>
     <th class="text-center">Status</th>
    <th class="text-center">Action</th>
  </tr>
  </tr>
       <tr class="noresult" style="display:  <?php if($total_data==0) { ?> table-row-group <?php } else { ?>none <?php } ?>;">
      <th colspan="5" class="text-center w3000">No Record Found</th>
  </tr>
 </thead>
 <tbody id="tbltbody1"></tbody>
  
</table>
</div>
</div>

<div class="col-xs-12 xstext-right xstext-center mt2 xsmt8 padding0">
<a data-toggle="modal" data-target="#delete" class="activity-btn disableGreybtn multi_action_btn deleteMultiLink " title="Delete">Delete</a><!--&nbsp;
<a href="<?php echo site_url('add-autoresponder?add_type=new')?>" class="smart-btn hidden-xs" title="Create Autoresponder">Create Autoresponder</a>-->
</div>


</div>
</div>
<script>
/*-------------------------- delete single newsletter starts here ------------------------------------*/
$('body').delegate(".deletelink", 'click', function() {
    
	deleteurl = $(this).attr('data-href');
	 $('#delete_yes_button').attr('href',deleteurl);
	 $('#delete_yes_button').removeClass('deleteMultiple');
});
/*-------------------------- delete single newsletter ends here ------------------------------------*/

/*-------------------------- check/uncheck multiple newsletter starts here ------------------------------------*/
$("#select_all").change(function(){ 
      $(".select_checkbox").prop('checked', $(this).prop("checked"));
	  $(".select_checkbox").change();
      });
/*-------------------------- check/uncheck multiple newsletter ends here ------------------------------------*/

/*-------------------------- delete multiple newsletter starts here ------------------------------------*/
$('body').delegate(".deleteMultiLink", 'click', function() {
    
	var length = $('.select_checkbox:checked').length;
	if(length==0)
	{ return false; }
	deleteurl = $(this).attr('data-href');
	 $('#delete_yes_button').attr('href','javascript:');
	 $('#delete_yes_button').addClass('deleteMultiple');
});
$('body').delegate(".deleteMultiple", 'click', function() {

    var posturl = site_url+'delete-multiple-autoresponder';
	var id = [];
	$(".select_checkbox:checkbox:checked").each(function (e) {
            id[e] = $(this).val();
        });
	var length = $('.select_checkbox:checked').length;
	if(length==0)
	{ return false; }

    $.ajax({
        url: posturl,
        dataType: 'json',
        type: "POST",
        data: {id: id},
        success: function(data) {
            if (data.success) {
			    $('#delete').modal('toggle');
				//PNotify.removeAll();
				//new PNotify({title: 'Success',text: 'Newsletter Deleted successfully',type: 'success'});
                //$(".contentDiv").load(location.href + " .contentDiv"); 
				location.reload(); 
            }
        },
    });
});
/*-------------------------- delete multiple newsletter ends here------------------------------------*/

/*-------------------------- Search function starts here ---------------------------------------------------*/
$('body').delegate(".search", 'keyup', function() {
	var searchVal = $(this).val();
	$( ".looptr:contains('"+searchVal+"')" ).show();
	$('.looptr:not(:contains('+ searchVal +'))').hide(); 
	$( ".noresult").hide();
         if($( ".looptr:contains('"+searchVal+"')" ).length === 0){ 
            $( ".noresult").show();
            $('#select_all').attr('disabled',true);
        }else{
            $('#select_all').attr('disabled',false);
        }
	
});
/*-------------------------- Search function ends here ---------------------------------------------------*/

/*-------------------------- success message starts here---------------------------------------------------*/
<?php if($this->session->userdata('success_msg')) { ?>
new PNotify({
	  title: 'Success',
	  text: '<?php echo $this->session->userdata('success_msg')?>',
	  type: 'success'
	});
<?php $this->session->unset_userdata('success_msg'); } ?>
/*-------------------------- success message ends here---------------------------------------------------*/



$(document).ready(function() {
var total_record = 0;
var total_groups = '<?php echo $total_data; ?>';
//var id = '<?php // echo $id; ?>';
var prestat = true;
$('#tbl > tbody').load("<?php echo base_url() ?>autoresponder-pagination",
 {'group_no': total_record}, function() {  total_record++;
 $gallery = $('.gallery a').simpleLightbox();
 });
$('#tbltbody1').scroll(function() {       
     if($('#tbltbody1').scrollTop() + $('#tbl').height() > $('#tbltbody1').prop('scrollHeight') && prestat) 
    {       
        if(total_record <= total_groups)
        {
          loading = true; 
          $('.loader_image').show(); 
		   prestat = false;
		  
          $.post('<?php echo site_url() ?>autoresponder-pagination',{'group_no': total_record},
            function(data){ 
                if (data != "") {
                    $("#tbl > tbody").append(data);                 
                    $('.loader_image').hide();                  
                    total_record++;
					prestat = true;
					$gallery = $('.gallery a').simpleLightbox();
                }
            });     
        }
    }
});
});

/*-------------------------- Status Change starts here---------------------------------------------------*/
$(document ).on( "change",".status_change", function() {
 if ($(this).is(":checked")) {
     var status = 'active';
    } else {
	  var status = 'inactive';
	}
	var posturl = site_url+'autoresponder-change-status';
	var id = $(this).attr('data-id');
	
    $.ajax({
        url: posturl,
        dataType: 'json',
        type: "POST",
        data: {status: status,id:id},
        success: function(data) {
            if (data.success) {
				PNotify.removeAll();
				new PNotify({title: 'Success',text: 'Status Changes successfully',type: 'success'});
            }
        },
    });
	
});
/*-------------------------- Status Change ends here-----------------------------------------------------*/
</script>