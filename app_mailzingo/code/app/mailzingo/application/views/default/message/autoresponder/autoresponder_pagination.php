&nbsp;
<?php 	 foreach ($allRecord as $value){ ?>
<tr class="text-center looptr">
    <td class="table-header-checkbox"><input name="select_checkbox" type="checkbox" value="<?php echo $value->id?>" class="select_checkbox"/></td>
    <td class="text-left">
    <div class="pull-left">
    <a href="<?php echo site_url('assets/uploads/autoresponder_images/'.$value->image)?>"><img src="<?php echo site_url('assets/uploads/autoresponder_images/'.$value->image)?>" class="img-responsive newsletter-imgwidth" title="Click to Zoom Image" style="min-height:92px; max-height:92px; min-width:75px;" /></a>
    </div>
    <div class="pull-left padding15 mt6 xsmt6 more-word4"> <?php echo $value->auto_name?></div></td>
    <td ><?php echo date('M d, Y',$value->add_time)?></td>
    <td >
      <label class="switch">
						<input class="switch-input status_change" type="checkbox" <?php if($value->status=='active') { ?> checked="checked" <?php } ?> value="<?php echo $value->id?>" data-id="<?php echo $value->id?>"/>
						<span class="switch-label" data-on="On" data-off="Off"></span> <span class="switch-handle"></span> </label>
    </td>    
    <td class="table-icon"><ul>
    <li><a href="<?php echo site_url('autoresponder-stats/'.$value->id.'/overview')?>" class="top-space" title="Report"><i class="icon-Report em12" aria-hidden="true" title="Report"></i></a></li>
    <li><a href="<?php echo site_url('use-autoresponder/'.$value->id)?>" title="Use" class="top-space"><i class="icon-Use-Me em12" aria-hidden="true" title="Use"></i></a></li>
  <li><a data-toggle="modal" data-target="#delete" title="Delete" class="deletelink" data-href="<?php echo site_url('delete-autoresponder/'.$value->id)?>" href="javascript:"><i class="fa fa-trash-o em12" aria-hidden="true" title="Delete"></i></a></li> 
    </ul></td>
  </tr>
<?php } ?>
<script>
$(".select_checkbox").change(function(){ 
	  var length = $('.select_checkbox:checked').length
	  if(length==0)
	   {
		  $(".multi_action_btn").addClass('disableGreybtn');
	   } else {
		  $(".multi_action_btn").removeClass('disableGreybtn');
	   }
	   
	    if (length != $('.select_checkbox').length) {
       $("#select_all").prop('checked', false);
  }else{
        $("#select_all").prop('checked', true);
    }
	
      });

</script>
