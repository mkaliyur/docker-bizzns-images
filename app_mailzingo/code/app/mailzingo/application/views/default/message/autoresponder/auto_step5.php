<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padding15 footer-height">
<!-- dash header-->
<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 mb2 xsmb4">
<div class="dash-header">
<div class="row">
<div class="col-md-12 col-sm-12"><h1 class="smart-color em22 smem18 xsem16">Autoresponder</h1>
<p class="em11 smem10 xsem10">Send mails to your subscribers automatically</p></div>
<!--<div class="col-md-5 col-sm-5 xstext-right xstext-center mt2 xsmt4 mb0 xsmb2">
<a href="#" class="smart-btn" data-toggle="modal" data-target="#myModal">Create Campaign</a></div> --></div>
</div>
</div>
<!-- dash header end-->

<div class="clearfix"></div>
<form action="" method="post">
<div class="col-md-12 col-sm-12 col-xs-12 mb2 mt1 xsmt1 xsmb4">
<div class="layout-box clearfix">
<div class="bs-wizard">
<div class="col-xs-3 bs-wizard-step complete">
<div class="progress"><div class="progress-bar"></div></div>
<a href="<?php echo site_url('add-autoresponder?step=1')?>" class="bs-wizard-dot"></a>
<div class="text-center em8 smem9 xsem8 hidden-xs">setting</div>
</div>
                
<div class="col-xs-3 bs-wizard-step complete">
<div class="progress"><div class="progress-bar"></div></div>
<a href="<?php echo site_url('add-autoresponder?step=2')?>" class="bs-wizard-dot"></a>
<div class="text-center em8 smem9 xsem8 hidden-xs">Templates</div>
</div>
<div class="col-xs-3 bs-wizard-step complete">
<div class="progress"><div class="progress-bar"></div></div>
<a href="<?php echo site_url('add-autoresponder?step=3')?>" class="bs-wizard-dot"></a>
<div class="text-center em8 smem9 xsem8 hidden-xs">Editor</div>
</div>
<div class="col-xs-3 bs-wizard-step complete">
<div class="progress"><div class="progress-bar"></div></div>
<a href="<?php echo site_url('add-autoresponder?step=4')?>" class="bs-wizard-dot"></a>
<div class="text-center em8 smem9 xsem8 hidden-xs">Subscriber</div>
</div>
<div class="col-xs-3 bs-wizard-step active">
<div class="progress"><div class="progress-bar"></div></div>
<a href="javascript:" class="bs-wizard-dot"></a>
<div class="text-center em8 smem9 xsem8 hidden-xs">Send Option</div>
</div>
</div>

<div class="col-xs-12 xsmb2 mb1 xsmt15 mt1 padding0 smart-field">
<h1 class="w600 em11 smem11 xsem11 ">Send Option</h1>
<p class="em9 smem9 xsem9 hidden-xs">Here You Can Send Your Message Immediately Or Schedule</p>

<div class="col-xs-12 padding0">
<div class="row">
<div class="col-xs-12 xsmt2 xsmb4 mb0 padding0">
<div class="col-md-4 col-lg-2 col-sm-4 col-xs-5 xsmb2">Email Name</div>
<div class="col-md-4 col-lg-4 col-sm-5 col-xs-7 w600"><?php echo $auto_name?></div></div>

<div class="col-xs-12 xsmt2 xsmb4 mb0 padding0">
<div class="col-md-4 col-lg-2 col-sm-4 col-xs-5 xsmb2">Subject</div>
<div class="col-md-4 col-lg-4 col-sm-5 col-xs-7 w600"><?php echo $subject?></div></div>

<?php if($template_name) { ?>
<div class="col-xs-12 xsmt2 xsmb4 mb0 padding0">
<div class="col-md-4 col-lg-2 col-sm-4 col-xs-5 xsmb2">Templates</div>
<div class="col-md-4 col-lg-4 col-sm-5 col-xs-7 w600"><?php echo $template_name?></div></div>
<?php } ?>

<div class="col-xs-12 xsmt2 xsmb4 mb0 padding0">
<div class="col-md-4 col-lg-2 col-sm-4 col-xs-12 xsmb2">Send Email</div>
<div class="col-md-4 col-lg-3 col-sm-5 col-xs-12"><select name="mail_day" class="mail_day">
<option value="0" <?php if($mail_day==0) { ?> selected="selected" <?php } ?>>Instant</option>
<?php for($i=1;$i<=30;$i++) { ?>
<option value="<?php echo $i?>" <?php if($mail_day==$i) { ?> selected="selected" <?php } ?>>After <?php echo $i?> Day</option>
<?php } ?>
</select></div></div>

<div class="col-xs-12 xsmt2 xsmb4 mb0 padding0">
<div class="col-md-4 col-lg-2 col-sm-4 col-xs-12 xsmb2">Active Days</div>
<div class="col-md-8 col-lg-10 col-sm-8 col-xs-12">

<div class="checkbox col-md-2 col-lg-1 col-sm-3 col-xs-4 cloum-width">
 <input id="sunday" class="styled" type="checkbox" name="enable_on[]" value="sunday" <?php if(in_array('sunday',$enable_on)) { ?> checked="checked" <?php } ?>>
<label for="sunday">Sun</label>
</div>
<div class="checkbox col-md-2 col-lg-1 col-sm-3 col-xs-4 cloum-width">
 <input id="monday" class="styled " type="checkbox"  name="enable_on[]" value="monday" <?php if(in_array('monday',$enable_on)) { ?> checked="checked" <?php } ?>>
<label for="monday">Mon</label>
</div>
<div class="checkbox col-md-2 col-lg-1 col-sm-3 col-xs-4 cloum-width">
 <input id="tuesday" class="styled" type="checkbox" name="enable_on[]" value="tuesday" <?php if(in_array('tuesday',$enable_on)) { ?> checked="checked" <?php } ?>>
<label for="tuesday">Tue</label>
</div>
<div class="checkbox col-md-2 col-lg-1 col-sm-3 col-xs-4 cloum-width">
 <input id="wednesday" class="styled" type="checkbox" name="enable_on[]" value="wednesday" <?php if(in_array('wednesday',$enable_on)) { ?> checked="checked" <?php } ?>>
<label for="wednesday">Wed</label>
</div>
<div class="checkbox col-md-2 col-lg-1 col-sm-3 col-xs-4 cloum-width">
 <input id="thursday" class="styled" type="checkbox" name="enable_on[]" value="thursday" <?php if(in_array('thursday',$enable_on)) { ?> checked="checked" <?php } ?>>
<label for="thursday">Thu</label>
</div>
<div class="checkbox col-md-2 col-lg-1 col-sm-3 col-xs-4 cloum-width">
 <input id="friday" class="styled" type="checkbox" name="enable_on[]" value="friday" <?php if(in_array('friday',$enable_on)) { ?> checked="checked" <?php } ?>>
<label for="friday">Fri</label>
</div>
<div class="checkbox col-md-2 col-lg-1 col-sm-3 col-xs-4 cloum-width">
 <input id="saturday" class="styled" type="checkbox"  name="enable_on[]" value="saturday" <?php if(in_array('saturday',$enable_on)) { ?> checked="checked" <?php } ?>>
<label for="saturday">Sat</label>
</div>
 
</div>

</div>

<div class="col-xs-12 xsmt2 xsmb4 mb0 padding0 send_timediv" <?php if($mail_day==0) { ?> style="display:none;" <?php } ?>>
<div class="col-md-4 col-lg-2 col-sm-4 col-xs-12 xsmb2">Send Time</div>
<div class="col-md-4 col-lg-2 col-sm-4 col-xs-12">
<select name="send_time">
<option selected="selected" value="00:00:00" <?php if($send_time=='00:00:00'){ ?> selected="selected" <?php } ?>>12:00 am</option>
<?php for($i=1;$i<=11;$i++) { ?>
<option value="<?php echo $i?>:00:00" <?php if($send_time==$i){ ?> selected="selected" <?php } ?>><?php echo $i?>:00 am</option>
<?php } ?>
<option value="12:00:00" <?php if($send_time=='12:00:00'){ ?> selected="selected" <?php } ?>>12:00 pm</option>
<?php for($i=1;$i<=11;$i++) { ?>
<option value="<?php echo $i+12?>:00:00" <?php if($send_time==$i+12){ ?> selected="selected" <?php } ?>><?php echo $i?>:00 pm</option>
<?php } ?>

</select>
</div>

</div>


</div>
</div>

</div>
</div>

<div class="col-xs-12 col-md-12 col-sm-12 xstext-right xstext-center mt2 xsmt4 padding0">
<a href="<?php echo site_url('add-autoresponder?step=4')?>" class="activity-btn" title="Back"> &nbsp; Back &nbsp; </a> &nbsp;
<button type="submit" class="activity-btn" title="Save To Draft" name="step5" value="save">Save To Draft</button> &nbsp;
<button type="submit" class="smart-btn xsmt4 mt0" title="Set Autoresponder " name="step5" value="publish">Set Autoresponder </button> 

</div>
</div>
</form>
</div>
<script>
$('body').delegate(".mail_day", 'change', function() {
       
        var mail_day = $('.mail_day').val();
		if(mail_day==0)
		 {
		   $('.send_timediv').fadeOut();
		 } else {
		   $('.send_timediv').fadeIn();
		 }
		
    
});

/*-------------------------- error message starts here---------------------------------------------------*/
<?php if(validation_errors()!='' || $error!='') { ?>
new PNotify({
	  title: 'Error',
	  text: <?php echo json_encode(validation_errors())?>,
	  type: 'error'
	});
<?php } ?>
/*-------------------------- error message ends here---------------------------------------------------*/

</script>				 
