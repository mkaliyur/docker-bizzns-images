
<!---------------------------line graph start--------------------------------->
<script type="text/javascript">
var StatePiriod="Day";
	<?php if($time_type=='day') { ?>
	var datetime = '%d';
	<?php } else if($time_type=='month') { ?>
	var datetime = '%b';
	<?php } else if($time_type=='year') { ?>
	var datetime = '%Y';
	<?php } ?>
	
	var Xtitle = 'Time';
	var Ytitle = 'Count';
	var GraphID = 'mailgraph';
	
	var sin = [  <?php echo $open_data?> ];
	var sin1 = [ <?php echo $click_data?> ];
	
	var passdata = [ 
	                 {values: sin, key: 'Open Rate',color: '#4e72bb' },
					 {values: sin1, key: 'Click Rate',color: '#4e7200' },
				   ];

		nv.addGraph(function() {
		chart = nv.models.lineChart();
			// chart sub-models (ie. xAxis, yAxis, etc) when accessed directly, return themselves, not the parent chart, so need to chain separately
			chart.xScale(d3.time.scale());
			chart.xAxis
				.axisLabel(Xtitle)
				//.ticks(8)
				
			.tickFormat(function(d) {
				  //return d3.time.format('%d %b %y')(new Date(d));
			 return d3.time.format(datetime)(new Date(d));
					 
			});
				
			chart.legend.margin({top: 10, right: 0, left: 0, bottom: 20})
			chart.yAxis
				.axisLabel(Ytitle)
				.tickFormat(function(d){
					if (d == null) {
						return 'N/A';
					}
					//return d3.format('')(d);
					return d3.format(',.0d')(d);
				});

			
			//data = GrapsArray();
			d3.select('#'+GraphID).html('').append('svg')
				.datum(passdata)
				.call(chart);
			nv.utils.windowResize(chart.update);
			
			return chart;
		});



</script>
<!---------------------------line graph end--------------------------------->

<div id="mailgraph"><svg></svg></div>