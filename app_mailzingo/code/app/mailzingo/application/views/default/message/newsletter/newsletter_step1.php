<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padding15 footer-height">
<!-- dash header-->
<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 mb2 xsmb4">
<div class="dash-header">
<div class="row">
<div class="col-md-12 col-sm-12"><h1 class="smart-color em22 smem18 xsem16">Newsletter</h1>
<p class="em11 smem10 xsem10">Create comprehensive newsletter and send latest information to your subscribers</p></div>
<!--<div class="col-md-5 col-sm-5 xstext-right xstext-center mt2 xsmt4 mb0 xsmb2">
<a href="#" class="smart-btn" data-toggle="modal" data-target="#myModal">Create Campaign</a></div> --></div>
</div>
</div>
<!-- dash header end-->

<div class="clearfix"></div>
<form action="" method="post">
<div class="col-md-12 col-sm-12 col-xs-12 mb2 xsmb4">
<div class="layout-box clearfix">
				  
<div class="bs-wizard">
<div class="col-xs-3 bs-wizard-step ">
<div class="progress"><div class="progress-bar"></div></div>
<a href="#" class="bs-wizard-dot"></a>
<div class="text-center em8 smem9 xsem8 hidden-xs">setting</div>
</div>
                
<div class="col-xs-3 bs-wizard-step <?php if($newsletter_session['step2']!='yes') { ?> disabled <?php } ?>">
<div class="progress"><div class="progress-bar"></div></div>
<a href="<?php if($newsletter_session['step2']=='yes') { echo site_url('add-newsletter?step=2'); } else { echo 'javascript:'; } ?>" class="bs-wizard-dot"></a>
<div class="text-center em8 smem9 xsem8 hidden-xs">Templates</div>
</div>
<div class="col-xs-3 bs-wizard-step  <?php if($newsletter_session['step3']!='yes') { ?> disabled <?php } ?>">
<div class="progress"><div class="progress-bar"></div></div>
<a href="<?php if($newsletter_session['step3']=='yes') { echo site_url('add-newsletter?step=3'); } else { echo 'javascript:'; } ?>" class="bs-wizard-dot"></a>
<div class="text-center em8 smem9 xsem8 hidden-xs">Editor</div>
</div>
<div class="col-xs-3 bs-wizard-step  <?php if($newsletter_session['step4']!='yes') { ?> disabled <?php } ?>">
<div class="progress"><div class="progress-bar"></div></div>
<a href="<?php if($newsletter_session['step4']=='yes') { echo site_url('add-newsletter?step=4'); } else { echo 'javascript:'; } ?>" class="bs-wizard-dot"></a>
<div class="text-center em8 smem9 xsem8 hidden-xs">Subscriber</div>
</div>
<div class="col-xs-3 bs-wizard-step  <?php if($newsletter_session['step5']!='yes') { ?> disabled <?php } ?>">
<div class="progress"><div class="progress-bar"></div></div>
<a href="<?php if($newsletter_session['step5']=='yes') { echo site_url('add-newsletter?step=5'); } else { echo 'javascript:'; } ?>" class="bs-wizard-dot"></a>
<div class="text-center em8 smem9 xsem8 hidden-xs">Send Option</div>
</div>
</div>

<div class="col-md-12 xsmt15 mt1 padding0 smart-field">
<div class="mt1 mb2 xsmb3 padding0 w600 em11 smem11 xsem11">Here You Can Set Message Settings</div>

<div class="col-xs-12 xsmb2 mb0 padding0 xsmt2">
<div class="row">
<div class="col-md-2 col-sm-3 col-xs-12 xsmb2">Message Name</div>
<div class="col-md-4 col-sm-5 col-xs-12">
<input name="message_name" type="text" placeholder="Message Name" value="<?php echo $message_name?>"></div>
<p class="col-md-10 col-sm-9 col-xs-10 col-md-offset-2 col-sm-offset-3 col-xs-offset-0 em9 smem9 xsem9 mb0 xsmt1 mt0 xsmb0">
It Will Not Be Seen By Your Subscribers.</p>
</div>
</div>

<div class="col-xs-12 xsmb4 xsmt2 mb0 padding0">
<div class="row">
<div class="col-md-2 col-sm-3 col-xs-12 xsmb2">Subject</div>
<div class="col-md-4 col-sm-5 col-xs-12">
<input name="subject" type="text" placeholder="Subject" class="subject" value="<?php echo $subject?>"></div>
<div class="col-md-3 col-sm-4 col-xs-12 xsmt2 mt0">
<select name="personalization" class="personal">
<option value="">Personalization</option>
<option value="name">Name</option>
<option value="email">Email</option>
<option value="address">Address</option>
<option value="city">City</option>
<option value="state">State</option>
<option value="country">Country</option>
<option value="zip">Zip</option>
<option value="phone">Phone</option>
<option value="custom_1">Custom 1</option>
<option value="custom_2">Custom 2</option>
<option value="custom_3">Custom 3</option>
</select></div>
<p class="col-md-10 col-sm-9 col-xs-10 col-md-offset-2 col-sm-offset-3 col-xs-offset-0 em9 smem9 xsem9 mb0 mt0 xsmt1 xsmb0">
It Will Be Seen By Your Subscribers.</p>
</div>
</div>

<div class="col-xs-12 xsmt2 xsmb4 mb0 padding0">
<div class="row">
<div class="col-md-2 col-sm-3 col-xs-12 xsmb2">From Name</div>
<div class="col-md-4 col-sm-5 col-xs-12">
<input name="from_name" type="text" placeholder="From Name" value="<?php echo $from_name?>"></div>
</div>
</div>
<?php $mail_type = $this->common_model->getSingleFieldFromAnyTable('mail_type','member_id',$this->session->userdata('mem_id'),'tbl_website_settings'); ?>
<div class="col-xs-12 xsmt2 xsmb4 mb0 padding0">
<div class="row">
<div class="col-md-2 col-sm-3 col-xs-12 xsmb2">From Email</div>
<div class="col-md-4 col-sm-5 col-xs-12">
<select name="from_email" <?php if($mail_type!='smtp' && $mail_type!='default') { ?> disabled="disabled" <?php } ?>>
<?php foreach($from_emails as $valEmail) { ?>
<option value="<?php echo $valEmail->email?>" <?php if($from_email==$valEmail->email) { ?> selected="selected" <?php } ?>><?php echo $valEmail->email?></option>
<?php } ?>
</select>
<?php if($mail_type=='smtp' || $mail_type=='default') { ?> 
<p class="em9 smem9 xsem9 mt1 xsmt1 xstext-right"><a href="<?php echo site_url('settings/user')?>" class="back-link"><u>Add Another Email</u></a></p>
<?php } ?>
</div>
<?php if($mail_type!='smtp' && $mail_type!='default') { ?> 
<div class="col-md-6 col-sm-4 col-xs-12 xsmt2 mt1">
<p class="em9 smem9 xsem9 smart-blue">
From email address will managed by <?php echo ucfirst($mail_type)?> API</p>
</div>
<?php } ?>
</div>
</div>

<div class="col-xs-12 xsmt2 xsmb4 mb0 padding0">
<div class="row">
<div class="col-md-2 col-sm-3 col-xs-12 xsmb2">Reply Email</div>
<div class="col-md-4 col-sm-5 col-xs-12">
<select name="reply_to" <?php if($mail_type!='smtp' && $mail_type!='default') { ?> disabled="disabled" <?php } ?>>
<?php foreach($from_emails as $valEmail) { ?>
<option value="<?php echo $valEmail->email?>" <?php if($reply_to==$valEmail->email) { ?> selected="selected" <?php } ?>><?php echo $valEmail->email?></option>
<?php } ?>
</select>
<?php if($mail_type=='smtp' || $mail_type=='default') { ?> 
<p class="em9 smem9 xsem9 mt1 xsmt1 xstext-right"><a href="<?php echo site_url('settings/user')?>" class="back-link"><u>Add Another Email</u></a></p>
<?php } ?>
</div>
<?php if($mail_type!='smtp' && $mail_type!='default') { ?> 
<div class="col-md-6 col-sm-4 col-xs-12 xsmt2 mt1">
<p class="em9 smem9 xsem9 smart-blue">
Reply email address will managed by <?php echo ucfirst($mail_type)?> API</p>
</div>
<?php } ?>
</div>
</div>

</div>
</div>


<div class="col-xs-12 xstext-right xstext-center mt2 xsmt8 padding0">
<a href="<?php echo site_url('newsletter-list/sent')?>" class="blank-btn" title="Back">Back</a>  &nbsp;
<button type="submit" class="smart-btn" title="Next" name="step1" value="step1">Next</button></div>
</div>
</form>
</div>
<script>
$('body').delegate(".personal", 'change', function() {
    var subval = $('.subject').val();
	var perVal = $(this).val();
	$('.subject').val(subval+'{#'+perVal+'}');
	
});

/*-------------------------- error message starts here---------------------------------------------------*/
<?php if(validation_errors()!='' || $error!='') { ?>
new PNotify({
	  title: 'Error',
	  text: <?php echo json_encode(validation_errors())?>,
	  type: 'error'
	});
<?php } ?>
/*-------------------------- error message ends here---------------------------------------------------*/
</script>