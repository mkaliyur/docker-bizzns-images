<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title><?php echo  $meta_title?></title>
<!-- Tell the browser to be responsive to screen width -->
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
<link rel="icon" href="<?php echo $this->config->item('templateAssetsPath')?>images/favicon-icon.png" type="image/png"/>
<!-- fonts-->
<link rel="stylesheet" href="<?php echo $this->config->item('templateAssetsPath')?>css/font-awesome.min.css" type="text/css" />
<link href='https://fonts.googleapis.com/css?family=Roboto:300,400,600,800' rel='stylesheet' type='text/css'>
<link href='https://fonts.googleapis.com/css?family=Raleway:400,600,800' rel='stylesheet' type='text/css'>
<link rel="stylesheet" href="<?php echo $this->config->item('templateAssetsPath')?>css/bootstrap.min.css" type="text/css" />
<!-- smart mailer design css-->
<link rel="stylesheet" href="<?php echo $this->config->item('templateAssetsPath')?>css/style.css" type="text/css" />
<link rel="stylesheet" href="<?php echo $this->config->item('templateAssetsPath')?>css/inline-editor.css" type="text/css" />
<script type='text/javascript' src="<?php echo $this->config->item('templateAssetsPath')?>js/jquery.min.js"></script>
<script type='text/javascript' src="<?php echo $this->config->item('templateAssetsPath')?>js/bootstrap.min.js"></script>
<script src="<?php echo $this->config->item('templateAssetsPath')?>js/jquery.form.js"></script> 
<!-- PNotify -->
<script type="text/javascript" src="<?php echo $this->config->item('templateAssetsPath')?>js/notify/pnotify.core.js"></script>
<script type="text/javascript" src="<?php echo $this->config->item('templateAssetsPath')?>js/notify/pnotify.buttons.js"></script>
<script>
var site_url = '<?php echo site_url()?>';
</script>
<!--- hide show div-->
<script type="text/javascript">
$( document ).ready(function() {
	$( "#preview-temp" ).hide();
	$( "#opendiv" ).click(function() {
	$( "#preview-temp" ).show();
});
	$( "#closediv" ).click(function() {
		$( "#preview-temp" ).hide();
	});
});
</script>
<!--- hide show div end-->
<!--- iframe auto height start js-->
<script type="text/javascript">
$( document ).ready(function() {
$('.iframe-full-height').on('load', function(){
   this.style.height=this.contentDocument.body.scrollHeight +'px';
});
});
/*focus();
var listener = addEventListener('blur', function() {
	if(document.activeElement = document.getElementById('iframe')) {
		//message.innerHTML = 'Clicked';
		alert("clicked");
	}
    //removeEventListener(listener);
});*/

$('.iframe-full-height').on('click', function() { alert("clicked"); });

$($('#m-scroll')).scroll(function(){
       $('.iframe-full-height').get(0).style.height=$('.iframe-full-height').get(0).contentDocument.body.scrollHeight +'px';
    }); 
</script>
<!--- iframe auto height start js end-->
</head>
<body class="scroll-hidden" onResize="myScrollfunction()" onload="myScrollfunction()">
<div id="preloader" style="display:none"></div>
<!-------------------------header start-------------------------------------------------->
<div class="inline-editor-header inline-editor-tab clearfix">
<ul>
<?php $mail_type = $this->common_model->getSingleFieldFromAnyTable('mail_type','member_id',$this->session->userdata('mem_id'),'tbl_website_settings'); ?>
<?php if($mail_type=='smtp' || $mail_type=='default') { ?>
<li>
<a data-toggle="modal" data-target="#msgattach">
<i class="fa fa-paperclip em14 fa-rotate-90" aria-hidden="true"></i>
&nbsp; <span class="attatchCount"><?php echo ($newsletter_session['attatchment']?sizeof($newsletter_session['attatchment']):0)?></span> Attached</a>
</li>
<?php } ?>
<li><a data-toggle="modal" data-target="#testMail" class="xsmt2 mt0">Test Mail</a></li>
<li class="inline-btn-right"><a id="opendiv" class="btn-width xsmt2 mt0">Mobile Preview</a></li>
</ul>
</div>

<!-------------------------header end-------------------------------------------------->

<!-------------------------content section start-------------------------------------------------->
<div class="inline-editor-content">
<div class="content-section editordiv" id="m-scroll">
<iframe src="<?php echo site_url('inline-editor/'.$newsletter_session['template_id'].'/newsletter')?>" height="100%" frameborder="0" scrolling="yes" class="iframe-full-height" id="iframe"></iframe>
</div>
<div id="img_val" style="display:none; min-height:800px"></div>
</div>
<!-------------------------content section end-------------------------------------------------->
<form action="" method="post" style="display:none">
<input type="hidden" class="template_text" name="template_text" />
<input type="hidden" class="template_name" name="template_name" />
<input type="hidden" class="image_text" name="image_text" value="" />
<button type="submit" class="step3" name="step3"  value="step3">Next </button>
</form>

<!-------------------------footer start-------------------------------------------------->
<div class="inline-editor-footer inline-editor-tab clearfix">
<div class="row">
<div class="col-xs-12 col-md-6 col-sm-4 xstext-center xstext-left">
<ul>
<li><a href="<?php echo site_url('add-newsletter?step=2')?>" class="inline-editor-btn">Back</a></li>
</ul>
</div>

<div class="col-xs-12 col-xs-12 col-md-6 col-sm-8 mt0 xsmt2 text-right">
<ul>
<li><a href="javascript:" class="btn-width saveTemplate">Save Template</a></li>
<li><a href="javascript:" class="inline-editor-btn submitnext">Next</a></li>
</ul>
</div>

</div>
</div>
<!-------------------------footer end-------------------------------------------------->

<!--------------------------------  preview modal div start ------------------------------------------>
<div id="preview-temp" class="tempreview-modal">
<a href="javascript:void(0)" id="closediv" class="close-modal">X</a>
<div class="col-xs-12 mobile-editor-content">
<div class="mobile-editor-section">
<div class="mobile-preview">


<?php echo str_replace('{image_path}',$this->config->item('templateAssetsPath').'editor/',$newsletter_session['template_text'])?>
	


</div>
</div>
</div>


</div>
<!--------------------------------  preview modal div end ------------------------------------------>
</div>

<!-- message attachment modalpopup-->
<div id="msgattach" class="modal fade" role="dialog">
  <div class="modal-dialog campaign-modal">
    <div class="modal-content">
      <div class="modal-body padding0 clearfix xsmb4 xsmt0 mt4 mb2">
        <div class="col-md-5 col-sm-5 col-xs-12 mt7 xsmt4 mb4">
          <div class="col-xs-12 bottom-border"> <img src="<?php echo $this->config->item('templateAssetsPath')?>images/import-pc.png" class="img-responsive center-block mobile-img" /></div>
        </div>
        <div class="col-md-7 col-sm-7 col-xs-12 left-border campaign-field xsmt4 mt0 ">
          <div class="col-xs-12">
            <h3 class="em16 smem16 xsem12">Attachment</h3>
			<form action="<?php echo site_url('add-temp-attatchment')?>" enctype="multipart/form-data" class="attatchForm" method="post">
        <!-- <label class="custom-file-upload mt6 mb4">
            <input type="file" name="attatchment" class="attatchment"/>
            <span class="w300">Add Attachment </span></label>-->
              <input type="file" name="attatchment" id="file-6" class="inputfile attatchment" />
        <label for="file-6"><span class="custom-file-upload" >Choose File</span></label>
            
			<div class="aWait_div" style="display:none"><img src="<?php echo $this->config->item("templateAssetsPath")?>images/loader.gif" class="center-block" /></div>
			</form>
			<div class="attachedDiv">
			<?php $attatchment = $newsletter_session['attatchment']; 
			 foreach($attatchment as $valattach){ ?>
			<div><div class="col-md-10 col-sm-10 col-xs-10 mt1 xsmt0 mb1 xsmb1 text-left padding0 em9 smem9 xsem9 mt1 xsmt0 more-word"><?php echo $valattach['name'].' ('.$valattach['size'].' kb)'?></div><div class="col-md-2 col-sm-2 col-xs-2 mt2 xsmt2 text-right mb1 xsmb1"> <a href="javascript:" class="smart-grey w300 deleteAttatch" data-name="<?php echo $valattach['name']?>"><i class="fa fa-times" aria-hidden="true"></i></a></div></div>
			<?php } ?>
			</div>
            

            <div class="clearfix"></div>
            <div class="text-right mt14 xsmt6 xstext-right xstext-center"> <a href="#" class="blank-btn" data-dismiss="modal">Close</a> </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<!-- Test Mail modalpopup-->
<div id="testMail" class="modal fade" role="dialog">
  <div class="modal-dialog campaign-modal">
    <div class="modal-content">
      <div class="modal-body padding0 clearfix xsmb4 xsmt4 ">
       <div class="col-md-5 col-sm-5 col-xs-12 mt1 xsmt2">
       <div class="col-xs-12 bottom-border">
      
       <img src="<?php echo $this->config->item('templateAssetsPath')?>images/testmail.png" class="img-responsive center-block mobile-img" /></div>
       </div>
       <div class="col-md-7 col-sm-7 col-xs-12 left-border campaign-field xsmt4 mt0 ">
        <div class="col-xs-12 mb2">
       <h3 class="em16 smem16 xsem14">Sent Test Mail</h3>
        <div class="form-group mt12 xsmt12">
		<label class="w300 sr-only">Campaign Name</label>
		<input type="text" name="testemail" placeholder="abc@sitename.com" required class="form-control testemail" />
		</div>
        <p class="em9 smem9 xsem9"><span class="smart-red">Note : </span> Test Your Message
By Delivering Them To Your Email Address.</p>
        
        <div class="text-right mt12 xsmt12 xsmb4 xstext-right xstext-center"><a href="#" class="blank-btn" data-dismiss="modal">Cancel</a> <a href="javascript:" class="smart-btn sendTestmail">Send</a></div>
		<div class="testWait_div" style="display:none"><img src="<?php echo $this->config->item("templateAssetsPath")?>images/loader.gif" class="center-block" /></div>
       </div></div>
      </div>
    </div>
  </div>
</div>

<!--GET SCREENSHOT -->
<script type="text/javascript" src="<?php echo $this->config->item('templateAssetsPath')?>js/html2canvas.js"></script>
<script type="text/javascript" src="<?php echo $this->config->item('templateAssetsPath')?>js/jquery.plugin.html2canvas.js"></script>
<!--GET SCREENSHOT -->

<script>
/*-------------------------- Mobile Preview Update Starts Here ----------------------------------------------------*/
function mobielPreviewUpdate(data)
{
	var div = document.createElement("div"); 
	$(div).append(data);    
	$(div).find("[contenteditable=true]").attr("contenteditable",false);
	template_text = $(div).html();
	$('.mobile-preview').html(template_text);
	
	//Commented by Mukesh Goyal, its disturb submenu in inline-editor.
	$('.iframe-full-height').get(0).style.height=$('.iframe-full-height').get(0).contentDocument.body.scrollHeight +'px'; 
	
}
$( document ).ready(function() {
    var data = $('.mobile-preview').html();
    var div = document.createElement("div"); 
	$(div).append(data);    
    $(div).find("[contenteditable=true]").attr("contenteditable",false);
     template_text = $(div).html();
    $('.mobile-preview').html(template_text);
});
/*-------------------------- Mobile Preview Update Starts Here ----------------------------------------------------*/

/*-------------------------- Submit Form Starts Here ----------------------------------------------------*/
$('.submitnext').on('click', function() 
{ 
   var template_text = document.getElementById('iframe').contentWindow.getTemplate();
   
    $('#preloader').show();
	$('.editordiv').hide();
	$('#img_val').show();
	$('#img_val').html(template_text);
	$('#img_val').html2canvas({
		onrendered: function (canvas) {
			  $('.image_text').val(canvas.toDataURL("image/png"));
		}
	});
	
   $('.template_text').val(template_text);

   var delay=1000; //1 second
	setTimeout(function() {
	   $('.step3').click();
	}, delay);
   
});
/*-------------------------- Submit Form Ends Here ----------------------------------------------------*/

/*-------------------------- Attatch file starts here ---------------------------------------------------*/
$('.attatchment').on('change', function() 
{ 
  $('.attatchForm').submit();
});
$(document).ready(function() {
    $(".attatchForm").submit(function(event) {
        var posturl = $(this).attr('action');
		var valueOrg;
        $(this).ajaxSubmit({
            url: posturl,
            dataType: 'json',
			beforeSend: function() { $('.aWait_div').fadeIn(); },
            success: function(response) {
             if (response.success) {
			    $('.aWait_div').fadeOut();
				$('.attachedDiv').html('');
				$.each(response.attatchment, function (index, value) {
					$('.attachedDiv').append('<div><div class="col-md-10 col-sm-10 col-xs-10 mt1 xsmt0 mb1 xsmb1 text-left padding0 em9 smem9 xsem9 mt1 xsmt0 more-word">'+value.name+' ('+value.size+' kb)</div><div class="col-md-2 col-sm-2 col-xs-2 mt2 xsmt2 text-right mb1 xsmb1"> <a href="javascript:" class="smart-grey w300 deleteAttatch" data-name="'+value.name+'"><i class="fa fa-times" aria-hidden="true"></i></a></div></div>');
			     });
				 $('.attatchCount').html(response.attatchCount);
			} 
            },
        });
        return false;
    
});
});
/*-------------------------- Attatch file ends here ---------------------------------------------------*/

/*-------------------------- Delete Attatch file starts here ---------------------------------------------------*/
$('body').delegate(".deleteAttatch", 'click', function() {
        var posturl = site_url+'delete-attatchment';
		var name = $(this).attr('data-name');
		var $this = $(this);
        $.ajax({
        url: posturl,
        dataType: 'json',
        type: "POST",
        data: {name: name},
        success: function(data) {
            if (data.success) {
			    $this.parent().parent().remove();
				$('.attatchCount').html(data.attatchCount);
            }
        },
      });
    
});
/*-------------------------- Delete Attatch file starts here ---------------------------------------------------*/

/*-------------------------- Save Template starts her ---------------------------------------------------*/
$('body').delegate(".saveTemplate", 'click', function() {

	var posturl = site_url+'save-template';
	var template_text = document.getElementById('iframe').contentWindow.getTemplate();
	
	var div = document.createElement("div"); 
	$(div).append(template_text);    
	$(div).find("[contenteditable=true]").attr("contenteditable",false);
	template_text = $(div).html();
		
	$('#preloader').show();
	$('.editordiv').hide();
	$('#img_val').show();
	$('#img_val').html(template_text);
	$('#img_val').html2canvas({
		onrendered: function (canvas) {
			  $('.image_text').val(canvas.toDataURL("image/png"));
		}
	});
   
   setTimeout(function() {
        $.ajax({
        url: posturl,
        dataType: 'json',
        type: "POST",
        data: {template_text: template_text,image_text: $('.image_text').val()},
        success: function(data) {
		    $('#preloader').hide();
			$('.editordiv').show();
			$('#img_val').hide();
            if (data.success) {
			    PNotify.removeAll();
				new PNotify({title: 'Success',text: 'Template Saved Successfully',type: 'success'});
            }
        },
      });
	}, 1000);
    
});
/*-------------------------- Save Template starts here ---------------------------------------------------*/

/*-------------------------- Send test mail starts here ---------------------------------------------------*/
$('body').delegate(".sendTestmail", 'click', function() {
        var posturl = site_url+'send-test-mail';
		var template_text = document.getElementById('iframe').contentWindow.getTemplate();
        var testemail = $('.testemail').val();
		
		var div = document.createElement("div"); 
	    $(div).append(template_text);    
	    $(div).find("[contenteditable=true]").attr("contenteditable",false);
	    template_text = $(div).html();
		
        $.ajax({
        url: posturl,
        dataType: 'json',
        type: "POST",
        data: {template_text: template_text,testemail: testemail},
		beforeSend: function() { 
		  $('.sendTestmail').fadeOut(); 
		  $('.testWait_div').fadeIn(); 
		},
        success: function(data) {
		$('.testWait_div').fadeOut();
		$('.sendTestmail').fadeIn(); 
            if (data.error) {
			    PNotify.removeAll();
			    new PNotify({title: 'Error',text: data.error,type: 'error'});
            } else {
			    $('#testMail').modal('toggle');
				PNotify.removeAll();
				new PNotify({title: 'Success',text: 'Mail Sent Successfully',type: 'success'});
			}
			
        },
      });
    
});
/*-------------------------- Send test mail ends here ---------------------------------------------------*/

/*-------------------------- error message starts here---------------------------------------------------*/
<?php if(validation_errors()!='' || $error!='') { ?>
new PNotify({
	  title: 'Error',
	  text: <?php echo json_encode(validation_errors())?>,
	  type: 'error'
	});
<?php } ?>
/*-------------------------- error message ends here---------------------------------------------------*/

</script>

<!------------------------ height scroll js --------------------------------> 
<script type="text/javascript">
function myScrollfunction() {
    var w = document.innerWidth;
    var h = $(window).height();
	var h_head = $(".inline-editor-header").outerHeight();
	var h_foot = $(".inline-editor-footer").outerHeight();
//	var h_prev = $(".template-heading").outerHeight();
//	var h_tab = $(".template-tab-bg").outerHeight();
	
//console.log(h+":"+h_head+":"+h_foot+"="+(h-h_head-h_foot));
//document.getElementById("noti-scroll").innerHTML = "Width: " + w + "<br>Height: " + h;
document.getElementById("m-scroll").style.maxHeight = (h-h_head-h_foot)+"px";
document.getElementById("preview-temp").style.maxHeight = (h-h_head-h_foot)+"px";
//document.getElementById("pre-scroll").style.maxHeight = (h-h_prev-h_tab)+"px";
$(".inline-editor-content").css("padding-top",h_head+"px");
}
</script> 
<!------------------------ height scroll js end --------------------------------> 

<!----------------------file upload start js---------------------->
<script>
/*
	By Osvaldas Valutis, www.osvaldas.info
	Available for use under the MIT License
*/
(function(e,t,n){var r=e.querySelectorAll("html")[0];r.className=r.className.replace(/(^|\s)no-js(\s|$)/,"$1js$2")})(document,window,0);
        

'use strict';

;( function ( document, window, index )
{
	var inputs = document.querySelectorAll( '.inputfile' );
	Array.prototype.forEach.call( inputs, function( input )
	{
		var label	 = input.nextElementSibling,
			labelVal = label.innerHTML;

		input.addEventListener( 'change', function( e )
		{
			var fileName = '';
			if( this.files && this.files.length > 1 )
				fileName = ( this.getAttribute( 'data-multiple-caption' ) || '' ).replace( '{count}', this.files.length );
			else
				fileName = e.target.value.split( '\\' ).pop();

			if( fileName )
				label.querySelector( 'p' ).innerHTML = fileName;
			else
				label.innerHTML = labelVal;
		});

		// Firefox bug fix
		input.addEventListener( 'focus', function(){ input.classList.add( 'has-focus' ); });
		input.addEventListener( 'blur', function(){ input.classList.remove( 'has-focus' ); });
	});
}( document, window, 0 ));
</script>

<!----------------------file upload end js ---------------------->
</body>
</html>