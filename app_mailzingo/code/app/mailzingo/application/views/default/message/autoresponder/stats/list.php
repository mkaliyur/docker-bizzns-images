<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padding15 footer-height">
<!-- dash header-->
<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 mb2 xsmb4">
<div class="dash-header">
<div class="row">
<div class="col-md-12 col-lg-12 col-sm-12"><h1 class="smart-color em22 smem18 xsem16">Statistics</h1>
<p class="em11 smem10 xsem10">Here are the stats for the mails sent by autoresponder</p></div>
<!--<div class="col-md-4 col-lg-3 col-sm-5 xstext-right xstext-center mt2 xsmt4 mb0 xsmb2">
<a href="#" class="smart-btn">Create Autoresponder</a></div> -->
 </div>
</div>
</div>
<!-- dash header end-->

<div class="clearfix"></div>

<div class="col-md-12 col-sm-12 col-xs-12 mb2 xsmb4">
<div class="layout-box1 clearfix mailer-data">
<div class="box-padding">
<div class="col-xs-12 col-md-6 col-sm-6 mt1 mb2 xsmb3 w600 em11 smem11 xsem11 padding0">Autoresponder Statisics</div>
<div class="col-xs-12 col-md-6 col-sm-6 mb2 xsmb2 text-right template-search padding0">
<div class="col-md-offset-6 col-sm-offset-2">
    <input type="text" placeholder="Search Autoresponder"  class="search"><button type="button"><i class="fa fa-search"></i></button></div>
</div></div>


<div class="clearfix"></div>


<div class="table-responsive table-data table-fixed-header gallery autoresponder-stats" id="autoscroll">
    <table width="100%" border="0" cellspacing="0" cellpadding="0" id="tbl">
<thead>  <tr class="w600">
	<th class="text-center"><input name="" type="checkbox" value="" id="select_all" /></th>
    <th>Autoresponder Name</th>
    <th class="text-center">Add Date</th>
    <th class="text-center">Action</th>
  </tr>
  <tr class="noresult" style="display:  <?php if(sizeof($allRecord)==0) { ?> table-row-group <?php } else { ?>none <?php } ?>;">
		   <th colspan="6" class="text-center w3000">No Record Found</th>
		  </tr>
  </thead>
 <tbody>  <?php foreach($allRecord as $value) { ?>
  <tr class="text-center looptr">
  <td><input name="select_checkbox" type="checkbox" value="<?php echo $value->id?>" class="select_checkbox"/></td>
    <td class="text-left">
    <div class="pull-left">
    <a href="<?php echo site_url('assets/uploads/autoresponder_images/'.$value->image)?>"><img src="<?php echo site_url('assets/uploads/autoresponder_images/'.$value->image)?>" class="img-responsive newsletter-imgwidth" title="Newsletter" /></a>
    </div>
    <div class="pull-left padding15 mt8 xsmt8"> <?php echo $value->auto_name?></div></td>
    <td><?php echo date('M d, Y',$value->add_time)?></td>    
    <td class="table-icon"><ul>
   <li><a href="<?php echo site_url('autoresponder-stats/'.$value->id.'/overview')?>" title="Report"><i class="icon-Report em12" aria-hidden="true" title="Report"></i></a></li>
   <li><a data-toggle="modal" data-target="#delete" class="deletelink" data-href="<?php echo site_url('delete-autoresponder/'.$value->id)?>" href="javascript:"><i class="fa fa-trash-o em12" aria-hidden="true"></i></a></li>
    </ul></td>
  </tr>
   <?php } ?>
  
  </tbody>
 
</table>
</div>
</div>

</div>
  <div class="col-xs-12 xstext-right xstext-center mt0 xsmt5 mb2 xsmb5"> <a data-toggle="modal" data-target="#delete" class="multi_action_btn deleteMultiLink activity-btn ">Delete</a> </div>



</div>
<script>
/*-------------------------- Search function starts here ---------------------------------------------------*/
$('body').delegate(".search", 'keyup', function() {
	var searchVal = $(this).val();
	$( ".looptr:contains('"+searchVal+"')" ).show();
	$('.looptr:not(:contains('+ searchVal +'))').hide(); 
      $( ".noresult").hide();
         if($( ".looptr:contains('"+searchVal+"')" ).length === 0){ 
            $( ".noresult").show();
            $('#select_all').attr('disabled',true);
        }else{
            $('#select_all').attr('disabled',false);
        }
	
});
$('body').delegate("#select_all", 'change', function() {
      $(".select_checkbox").prop('checked', $(this).prop("checked"));
	  $(".select_checkbox").change();
      });
$('body').delegate(".select_checkbox", 'change', function() {
	  var length = $('.select_checkbox:checked').length
	  if(length==0)
	   {
		  $(".multi_action_btn").addClass('disableGreybtn');
	   } else {
		  $(".multi_action_btn").removeClass('disableGreybtn');
	   }
      });

/*---------------------------if none is checked start---------------------------*/
$(".select_checkbox").change(function(){
    if ($('.select_checkbox:checked').length != $('.select_checkbox').length) {
       $("#select_all").prop('checked', false);
       
    }else{
        $("#select_all").prop('checked', true);
    }
});
/*---------------------------if none is checked end---------------------------*/

if($('.select_all:checked')){
    $(".multi_action_btn").addClass('disableGreybtn');
}
/*-------------------------- Search function ends here ---------------------------------------------------*/

/*-------------------------- delete single autoresponder starts here ------------------------------------*/
$('body').delegate(".deletelink", 'click', function() {
    
	deleteurl = $(this).attr('data-href');
	 $('#delete_yes_button').attr('href',deleteurl);
	 $('#delete_yes_button').removeClass('deleteMultiple');
});
/*-------------------------- delete single autoresponder ends here ------------------------------------*/

/*-------------------------- delete multiple autoresponder starts here ------------------------------------*/
$('body').delegate(".deleteMultiLink", 'click', function() {
    
	var length = $('.select_checkbox:checked').length;
	if(length==0)
	{ return false; }
	deleteurl = $(this).attr('data-href');
	 $('#delete_yes_button').attr('href','javascript:');
	 $('#delete_yes_button').addClass('deleteMultiple');
});

$('body').delegate(".deleteMultiple", 'click', function() {

    var posturl = site_url+'delete-multiple-autoresponder';
	var id = [];
	$(".select_checkbox:checkbox:checked").each(function (e) {
            id[e] = $(this).val();
        });
	var length = $('.select_checkbox:checked').length;
	if(length==0)
	{ return false; }

    $.ajax({
        url: posturl,
        dataType: 'json',
        type: "POST",
        data: {id: id},
        success: function(data) {
            if (data.success) {
			    $('#delete').modal('hide');
				PNotify.removeAll();
				new PNotify({title: 'Success',text: 'Autoresponder Deleted successfully',type: 'success'});
                $(".contentDiv").load(location.href + " .contentDiv"); 
                window.location.reload(true);

            }
        },
    });
});
/*-------------------------- delete multiple newsletter ends here------------------------------------*/

/*-------------------------- success message starts here---------------------------------------------------*/
<?php if($this->session->userdata('success_msg')) { ?>
new PNotify({
	  title: 'Success',
	  text: '<?php echo $this->session->userdata('success_msg')?>',
	  type: 'success'
	});
<?php $this->session->unset_userdata('success_msg'); } ?>
/*-------------------------- success message ends here---------------------------------------------------*/
</script>
