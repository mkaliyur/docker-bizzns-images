<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padding15 footer-height">
<!-- dash header-->
<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 mb1 xsmb4">
<div class="dash-header">
<div class="row">
<div class="col-md-12 col-sm-12"><h1 class="smart-color em22 smem18 xsem16">Statistics</h1>
<p class="em11 smem10 xsem10">Here are the stats for the mails sent by autoresponder</p></div>
<!--<div class="col-md-5 col-sm-5 xstext-right xstext-center mt2 xsmt4 mb0 xsmb2">
<a href="#" class="smart-btn" data-toggle="modal" data-target="#myModal">Create Campaign</a></div> --></div>
</div>
</div>
<!-- dash header end-->

<div class="clearfix"></div>
<div class="col-md-12 mb1 xsmb2 clearfix text-right hidden-xs">
<a href="<?php echo site_url('autoresponder-stats/'.$newsletter_id.'/overview')?>" class="back-link">
<i class="fa fa-arrow-left" aria-hidden="true"></i> &nbsp;Back</a>
</div>


<div class="col-md-12 col-sm-12 col-xs-12 mb2 xsmb4">
<div class="layout-box1 clearfix padding0 mailer-data">

<div class="box-padding">
<div class="col-xs-7 col-md-6 col-sm-6 mt1 mb2 xsmb3 w600 em11 smem11 xsem11 padding0">Opened Emails</div>
<div class="col-xs-5 col-md-6 col-sm-6 mb2 xsmb2 text-right campaign-search padding0 ">
    <a id="export" href="<?php echo site_url('autoresponder-stats-export/'.$newsletter_id.'/'.$page_type)?>" class="suppression-grey-btn <?php if(sizeof($email_list)==0) { ?> disableGreybtn <?php } ?>">Export</a> 
</div></div>


<div class="clearfix"></div>
<div class="table-responsive table-multi-data data-scroll">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <th>Subscribers</th>
     </tr>
  <?php if($email_list){ foreach($email_list as $val) { ?>
  <tr class="text-center">
   <td class="text-left"><?php echo $val->email?></td>
  
  </tr>
  <?php }}else{
      ?>
  <tr class="text-center"><td>No record available</td></tr>
  <script>
      $('#export').removeAttr('href');
      </script>
      <?php 
  } ?>

</table>
</div>
</div>

<div class="col-xs-12 xstext-right xstext-center mt2 xsmt8 padding0 visible-xs">
<a href="<?php echo site_url('autoresponder-stats/'.$newsletter_id.'/overview')?>" class="blank-btn" title="Back">Back</a>

</div>

</div>
</div>