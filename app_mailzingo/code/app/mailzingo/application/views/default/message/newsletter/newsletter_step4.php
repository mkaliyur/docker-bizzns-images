<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padding15 footer-height">
<!-- dash header-->
<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 mb2 xsmb4">
<div class="dash-header">
<div class="row">
<div class="col-md-12 col-sm-12"><h1 class="smart-color em22 smem18 xsem16">Newsletter</h1>
<p class="em11 smem10 xsem10">Create comprehensive newsletter and send latest information to your subscribers</p></div>
<!--<div class="col-md-5 col-sm-5 xstext-right xstext-center mt2 xsmt4 mb0 xsmb2">
<a href="#" class="smart-btn" data-toggle="modal" data-target="#myModal">Create Campaign</a></div> --></div>
</div>
</div>
<!-- dash header end-->

<div class="clearfix"></div>
<form action="" method="post">
<div class="col-md-12 col-sm-12 col-xs-12 mb2 mt1 xsmt1 xsmb4">
<div class="layout-box clearfix">
<div class="bs-wizard">
<div class="col-xs-3 bs-wizard-step complete">
<div class="progress"><div class="progress-bar"></div></div>
<a href="<?php echo site_url('add-newsletter?step=1')?>" class="bs-wizard-dot"></a>
<div class="text-center em8 smem9 xsem8 hidden-xs">setting</div>
</div>
                
<div class="col-xs-3 bs-wizard-step complete">
<div class="progress"><div class="progress-bar"></div></div>
<a href="<?php echo site_url('add-newsletter?step=2')?>" class="bs-wizard-dot"></a>
<div class="text-center em8 smem9 xsem8 hidden-xs">Templates</div>
</div>
<div class="col-xs-3 bs-wizard-step complete">
<div class="progress"><div class="progress-bar"></div></div>
<a href="<?php echo site_url('add-newsletter?step=3')?>" class="bs-wizard-dot"></a>
<div class="text-center em8 smem9 xsem8 hidden-xs">Editor</div>
</div>
<div class="col-xs-3 bs-wizard-step complete active">
<div class="progress"><div class="progress-bar"></div></div>
<a href="<?php if($newsletter_session['step4']=='yes') { echo site_url('add-newsletter?step=4'); } else { echo 'javascript:'; } ?>" class="bs-wizard-dot"></a>
<div class="text-center em8 smem9 xsem8 hidden-xs">Subscriber</div>
</div>
<div class="col-xs-3 bs-wizard-step  <?php if($newsletter_session['step5']!='yes') { ?> disabled <?php } ?>">
<div class="progress"><div class="progress-bar"></div></div>
<a href="<?php if($newsletter_session['step5']=='yes') { echo site_url('add-newsletter?step=5'); } else { echo 'javascript:'; } ?>" class="bs-wizard-dot"></a>
<div class="text-center em8 smem9 xsem8 hidden-xs">Send Option</div>
</div>
</div>

<div class="col-xs-12 xsmb2 mb1 xsmt15 mt1 padding0">
<h1 class="mb2 xsmb3 w600 em11 smem11 xsem11 hidden-xs">Choose Campaigns</h1>

<div class="col-xs-12 padding0">
<div class="row">
<div class="col-md-4 col-sm-4 w600 em11 smem11 xsem10 xsmt4 mt0">
Include Subscriber <i class="fa fa-plus plus-icon em10" aria-hidden="true"></i>
<div class="campaign-heading mt4 xsmt6">
<input name="all_campaign" type="checkbox" value="all" id="in_select_all"/> &nbsp; &nbsp;Campaign 
</div>
<div class="subscriber-list data-scroll">
<ul id="in_campaign_ul">
<!--Campaign list will show here-->
</ul>
</div>
</div>

<div class="col-md-4 col-sm-4 w600 em11 smem11 xsem10 xsmt4 mt0">
Exclude Subscriber <i class="fa fa-times red-icon em10" aria-hidden="true"></i>
<div class="campaign-heading mt4 xsmt6">
<input name="all_campaign" type="checkbox" value="all" id="ex_select_all"  /> &nbsp; &nbsp;Campaign 
</div>
<div class="subscriber-list data-scroll">
<ul id="ex_campaign_ul">
<!--Campaign list will show here-->
</ul>
</div>
</div>
<div class="col-md-4 col-sm-4 w600 em11 smem11 xsem10 xsmt4 mt0">Suppression

<div class="campaign-heading mt5 xsmt6"><input name="all_seppression" type="checkbox" value="all" id="sep_select_all" /> &nbsp; &nbsp;Suppression List
</div>
<div class="subscriber-list data-scroll">
<ul id="sep_ul">
<!--Suppression list will show here-->
</ul>
</div>

</div>
</div>
</div>


</div>
</div>

<div class="col-xs-12 col-md-5 col-sm-6 mt2 xsmt8 padding0  xstext-center xstext-left mdem12 smem11 xsem10">
<p class="blank-btn w500">Select Subscribers: &nbsp;&nbsp;<span id="contact_count" class="roboto">0</span></p> 
</div>
<div class="col-xs-12 col-md-7 col-sm-6 xstext-right xstext-center mt2 xsmt4 padding0">
<a href="<?php echo site_url('add-newsletter?step=3')?>" class="blank-btn" title="Back">Back </a> &nbsp;

<button type="submit" class="smart-btn" title="Next" name="step4" value="step4">Next </button>
</div>
</div>
</form>
</div>
<script>
$(document).ready(function(){
/*-------------------------- campaign/select/unselect starts here ------------------------------------*/
$(document ).on( "change","#in_select_all", function() {
      $(".in_campaign_ckbox").prop('checked', $(this).prop("checked"));
	  $(".in_campaign_ckbox").change();

});

$(document ).on( "change",".in_campaign_ckbox", function() {
	  var value = $(this).val();
	  if($(this).is(":checked"))
	   {
		  $(".ex_campaign_ckbox:checkbox[value="+value+"]").prop('disabled', true);
		  $(".ex_campaign_ckbox:checkbox[value="+value+"]").parent().addClass('unactive');
		  $(".ex_campaign_ckbox:checkbox[value="+value+"]").prop('checked', false);
	   } else {
		  $(".ex_campaign_ckbox:checkbox[value="+value+"]").prop('disabled', false);
		   $(".ex_campaign_ckbox:checkbox[value="+value+"]").parent().removeClass('unactive');
	   }
	   if ($('.in_campaign_ckbox:checked').length != $('.in_campaign_ckbox').length) {
        $("#in_select_all").prop('checked', false);
      }else{
        $("#in_select_all").prop('checked', true);
      }
	
});

$(document ).on( "change","#ex_select_all", function() {
      $(".ex_campaign_ckbox:enabled").prop('checked', $(this).prop("checked"));
      });

$(document ).on( "change","#sep_select_all", function() {
      $(".sup_ckbox").prop('checked', $(this).prop("checked"));
});

$(document ).on( "change",".ex_campaign_ckbox", function() {
	   if ($('.ex_campaign_ckbox:checked').length != $('.ex_campaign_ckbox').length) {
        $("#ex_select_all").prop('checked', false);
      }else{
        $("#ex_select_all").prop('checked', true);
      }
});
$(document ).on( "change",".sup_ckbox", function() {
	   if ($('.sup_ckbox:checked').length != $('.sup_ckbox').length) {
        $("#sep_select_all").prop('checked', false);
      }else{
        $("#sep_select_all").prop('checked', true);
      }
});


});
/*-------------------------- campaign/select/unselect ends here ------------------------------------*/

/*-------------------------- total subscriber starts here ------------------------------------*/
$(document ).on( "change",".in_campaign_ckbox,.ex_campaign_ckbox,.sup_ckbox", function() {

        var posturl = site_url+'get-final-email-count';

		  var in_campaign = $('.in_campaign_ckbox:checked').map(function()
            {
                return $(this).val();
            }).get();
		  var ex_campaign = $('.ex_campaign_ckbox:checked').map(function()
            {
                return $(this).val();
            }).get();
		  var supression = $('.sup_ckbox:checked').map(function()
            {
                return $(this).val();
            }).get();
		
      if($('.in_campaign_ckbox:checked').length != 0){ 
            
        $.ajax({
        url: posturl,
        dataType: 'json',
        type: "POST",
        data: {in_campaign: in_campaign, ex_campaign: ex_campaign, supression: supression},
		beforeSend: function() { $('#contact_count').html('<img src="<?php echo $this->config->item("templateAssetsPath")?>images/loader.gif" class="center-block newslettter-loader" />'); },
        success: function(data) {
            if (data.success) {
			    $('#contact_count').html('');
			    $('#contact_count').text(data.contact_count);
            }
        },
      });
      
    }else{
        $('#contact_count').text('0');
    }
     
});
/*-------------------------- total subscriber ends here ------------------------------------*/

/*-------------------------- get Campaign List starts here ------------------------------------*/
$( document ).ready(function() { 
  getCampaignList('in_campaign_ul');
  getCampaignList('ex_campaign_ul');
  getCampaignList('sep_ul');
});

function getCampaignList(camp_type){

	var posturl = site_url+'get-campaign-list-for-newsletter';

    $.ajax({
        url: posturl,
        dataType: 'json',
        type: "POST",
        data: {camp_type: camp_type},
		beforeSend: function() { $('#'+camp_type).html('<img src="<?php echo $this->config->item("templateAssetsPath")?>images/loader.gif" class="center-block" />'); },
        success: function(data) {
                  $('#'+camp_type).html('');
				  $('#'+camp_type).html(data.html);
				   $('.in_campaign_ckbox').change();
        },
    });
}
/*-------------------------- get Campaign List ends here ------------------------------------*/

/*-------------------------- error message starts here---------------------------------------------------*/
<?php if(validation_errors()!='' || $error!='') { ?>
new PNotify({
	  title: 'Error',
	  text: <?php echo json_encode(validation_errors())?>,
	  type: 'error'
	});
<?php } ?>
/*-------------------------- error message ends here---------------------------------------------------*/
</script>