<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title><?php echo $this->config->item('site_title')?></title>
<!-- Tell the browser to be responsive to screen width -->
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
<link rel="icon" href="<?php echo $this->config->item('templateAssetsPath')?>images/favicon-icon.png" type="image/png"/>
<!-- fonts-->
<link rel="stylesheet" href="<?php echo $this->config->item('templateAssetsPath')?>css/font-awesome.min.css" type="text/css" />
<link href='https://fonts.googleapis.com/css?family=Roboto:300,400,600,800' rel='stylesheet' type='text/css'>
<link href='https://fonts.googleapis.com/css?family=Raleway:400,600,800' rel='stylesheet' type='text/css'>
<link rel="stylesheet" href="<?php echo $this->config->item('templateAssetsPath')?>css/bootstrap.min.css" type="text/css" />
<!-- smart mailer design css-->
<link rel="stylesheet" href="<?php echo $this->config->item('templateAssetsPath')?>css/style.css" type="text/css" />
<link rel="stylesheet" href="<?php echo $this->config->item('templateAssetsPath')?>css/inline-editor.css" type="text/css" />
<script type='text/javascript' src="<?php echo $this->config->item('templateAssetsPath')?>js/jquery.min.js"></script>
<script type='text/javascript' src="<?php echo $this->config->item('templateAssetsPath')?>js/bootstrap.min.js"></script>
<script src="<?php echo $this->config->item('templateAssetsPath')?>js/jquery.form.js"></script>
<!-- PNotify -->
<script type="text/javascript" src="<?php echo $this->config->item('templateAssetsPath')?>js/notify/pnotify.core.js"></script>
<script type="text/javascript" src="<?php echo $this->config->item('templateAssetsPath')?>js/notify/pnotify.buttons.js"></script>
<script>
var site_url = '<?php echo site_url()?>';
</script>
<!--- hide show div-->
<script type="text/javascript">
$( document ).ready(function() {
	$( "#preview-temp" ).hide();
	$( "#opendiv" ).click(function() {
	$( "#preview-temp" ).show();
});
	$( "#closediv" ).click(function() {
		$( "#preview-temp" ).hide();
	});
});
</script>
<!--- hide show div end-->


<!--- iframe auto height start js-->
<script type="text/javascript">
$( document ).ready(function() {
$('.iframe-full-height').on('load', function(){
    this.style.height=this.contentDocument.body.scrollHeight +'px';
});
});

/*-------------------------- Mobile Preview Update Starts Here ----------------------------------------------------*/
function mobielPreviewUpdate(data)
{
	
}

$($('#m-scroll')).scroll(function(){
       $('.iframe-full-height').get(0).style.height=$('.iframe-full-height').get(0).contentDocument.body.scrollHeight +'px';
    }); 
</script>
<!--- iframe auto height start js end-->

</head>
<body class="scroll-hidden" onResize="myScrollfunction()" onload="myScrollfunction()">
<div id="preloader" style="display:none"></div>
<!-------------------------header start-------------------------------------------------->
<div class="inline-editor-header inline-editor-tab clearfix">
<ul>
<li class="col-md-3 col-sm-5 col-xs-12">
<input name="template_name1" class="form-control template_name1" placeholder="Template Name" value="<?php echo $template_name?>" type="text" >
</li>
</ul>
</div>
<!-------------------------header end-------------------------------------------------->

<!-------------------------content section start-------------------------------------------------->
<div class="inline-editor-content">
<div class="content-section editordiv" id="m-scroll">
<iframe src="<?php echo site_url('inline-editor/'.$template_id.'/new')?>" height="100%" frameborder="0" scrolling="yes" class="iframe-full-height" id="iframe"></iframe>
</div>
<div id="img_val" style="display:none; min-height:800px"></div>
</div>
<!-------------------------content section end-------------------------------------------------->
<form action="" method="post" style="display:none">
<input type="hidden" class="template_text" name="template_text" />
<input type="hidden" class="template_name" name="template_name" />
<input type="hidden" name="image_text" value="" class="image_text" />
<button type="submit" class="step3" name="step3"  value="step3">Next </button>
</form>

<!-------------------------footer start-------------------------------------------------->
<div class="inline-editor-footer inline-editor-tab clearfix">
<div class="row">
<div class="col-xs-4 col-md-6 col-sm-4 xstext-center xstext-left">
<ul>
<li><a href="<?php echo site_url('template-list')?>" class="inline-editor-btn">Back</a></li>
</ul>
</div>

<div class="col-xs-8 col-md-6 col-sm-8 text-right">
<ul>
<li><a href="javascript:" class="btn-width submitnext">Save To Draft</a></li>
</ul>
</div>
</div>
</div>
<!-------------------------footer end-------------------------------------------------->


</div>

<!--GET SCREENSHOT -->
<script type="text/javascript" src="<?php echo $this->config->item('templateAssetsPath')?>js/html2canvas.js"></script>
<script type="text/javascript" src="<?php echo $this->config->item('templateAssetsPath')?>js/jquery.plugin.html2canvas.js"></script>
<!--GET SCREENSHOT -->

<script>
/*-------------------------- Submit Form Starts Here ----------------------------------------------------*/
$('.submitnext').on('click', function() 
{ 
   var template_text = document.getElementById('iframe').contentWindow.getTemplate();
    var div = document.createElement("div"); 
	$(div).append(template_text);    
    $(div).find("[contenteditable=true]").attr("contenteditable",false);
     template_text = $(div).html();
  
   $('.template_text').val(template_text);
   $('.template_name').val($('.template_name1').val());
  
    $('#preloader').show();
	$('.editordiv').hide();
	$('#img_val').show();
	$('#img_val').html(template_text);
	$('#img_val').html2canvas({
		onrendered: function (canvas) {
			  $('.image_text').val(canvas.toDataURL("image/png"));
		}
	});
	
	var delay=1000; //1 second
	setTimeout(function() {
	   $('.step3').click();
	}, delay);
	
   
   
});
/*-------------------------- Submit Form Ends Here ----------------------------------------------------*/




/*-------------------------- error message starts here---------------------------------------------------*/
$( document ).ready(function() {
<?php if(validation_errors()!='' || $error!='') { ?>
new PNotify({
	  title: 'Error',
	  text: <?php echo json_encode(validation_errors())?>,
	  type: 'error'
	});
<?php } ?>
});
/*-------------------------- error message ends here---------------------------------------------------*/

</script>

<!------------------------ height scroll js --------------------------------> 
<script type="text/javascript">
function myScrollfunction() {
    var w = document.innerWidth;
    var h = $(window).height();
	var h_head = $(".inline-editor-header").outerHeight();
	var h_foot = $(".inline-editor-footer").outerHeight();
//	var h_prev = $(".template-heading").outerHeight();
//	var h_tab = $(".template-tab-bg").outerHeight();
	
//console.log(h+":"+h_head+":"+h_foot+"="+(h-h_head-h_foot));
//document.getElementById("noti-scroll").innerHTML = "Width: " + w + "<br>Height: " + h;
document.getElementById("m-scroll").style.maxHeight = (h-h_head-h_foot)+"px";
//document.getElementById("preview-temp").style.maxHeight = (h-h_head-h_foot)+"px";
//document.getElementById("pre-scroll").style.maxHeight = (h-h_prev-h_tab)+"px";
$(".inline-editor-content").css("padding-top",h_head+"px");
}
</script> 
<!------------------------ height scroll js end --------------------------------> 

<!----------------------file upload start js---------------------->
<script>
/*
	By Osvaldas Valutis, www.osvaldas.info
	Available for use under the MIT License
*/
(function(e,t,n){var r=e.querySelectorAll("html")[0];r.className=r.className.replace(/(^|\s)no-js(\s|$)/,"$1js$2")})(document,window,0);
        

'use strict';

;( function ( document, window, index )
{
	var inputs = document.querySelectorAll( '.inputfile' );
	Array.prototype.forEach.call( inputs, function( input )
	{
		var label	 = input.nextElementSibling,
			labelVal = label.innerHTML;

		input.addEventListener( 'change', function( e )
		{
			var fileName = '';
			if( this.files && this.files.length > 1 )
				fileName = ( this.getAttribute( 'data-multiple-caption' ) || '' ).replace( '{count}', this.files.length );
			else
				fileName = e.target.value.split( '\\' ).pop();

			if( fileName )
				label.querySelector( 'p' ).innerHTML = fileName;
			else
				label.innerHTML = labelVal;
		});

		// Firefox bug fix
		input.addEventListener( 'focus', function(){ input.classList.add( 'has-focus' ); });
		input.addEventListener( 'blur', function(){ input.classList.remove( 'has-focus' ); });
	});
}( document, window, 0 ));
</script>

<!----------------------file upload end js ---------------------->
</body>
</html>