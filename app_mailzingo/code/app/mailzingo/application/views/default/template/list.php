<script>
    $(document).ready(function(){
        if (Modernizr.touch) {
            // show the close overlay button
            $(".close-overlay").removeClass("hidden");
            // handle the adding of hover class when clicked
            $(".img").click(function(e){
                if (!$(this).hasClass("hover")) {
                    $(this).addClass("hover");
                }
            });
            // handle the closing of the overlay
            $(".close-overlay").click(function(e){
                e.preventDefault();
                e.stopPropagation();
                if ($(this).closest(".img").hasClass("hover")) {
                    $(this).closest(".img").removeClass("hover");
                }
            });
        } else {
            // handle the mouseenter functionality
            $(".img").mouseenter(function(){
                $(this).addClass("hover");
            })
            // handle the mouseleave functionality
            .mouseleave(function(){
                $(this).removeClass("hover");
            });
        }
    });
</script>
<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padding15 footer-height">
  <!-- dash header-->
  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 mb2 xsmb4">
    <div class="dash-header">
      <div class="row">
        <div class="col-md-7 col-sm-7">
          <h1 class="smart-color em22 smem18 xsem16">Template</h1>
          <p class="em11 smem10 xsem10">Create and modify existing templates easily to get best results</p>
        </div>
        <div class="col-md-5 col-sm-5 xstext-right xstext-center mt2 xsmt4 mb0 xsmb2"> <a href="<?php echo site_url('add-template'); ?>" class="smart-btn">Create Template</a></div>
      </div>
    </div>
  </div>
  <!-- dash header end-->
  <div class="clearfix"></div>
  <div class="col-md-12 smart-tabs clearfix">
    <ul>
      <li><a href="<?php echo site_url('add-template'); ?>" title="Create Template"> <span class="hidden-xs">Create Template</span><i class="icon-Create-Template visible-xs"></i> </a></li>
      <li><a href="<?php echo site_url('template-list'); ?>" class="active" title="Template"> <span class="hidden-xs">Template</span><i class="icon-Template visible-xs"></i> </a></li>
      <li><a href="<?php echo site_url('template-list').'/draft'; ?>" title="Draft"> <span class="hidden-xs">Draft</span><i class="icon-Drafts visible-xs"></i> </a></li>
    </ul>
  </div>
  <div class="col-md-12 col-sm-12 col-xs-12 mb2 xsmb4">
    <div class="layout-box1 clearfix">
      <div class="box-padding">
        <div class="col-xs-12 col-md-6 col-sm-6 mt1 mb2 xsmb3 w600 em11 smem11 xsem11 padding0">Choose Your Default Template</div>
        <div class="col-xs-12 col-md-6 col-sm-6 mb2 xsmb2 text-right template-search padding0">
          <div class="col-md-offset-6 col-sm-offset-2">
            <input type="text" placeholder="Search Template" class="search">
            <button type="button"><i class="fa fa-search"></i></button>
          </div>
        </div>
      </div>
      <div class="col-xs-12 padding0 line-bottom"></div>
      <div class="clearfix"></div>
      <div class="col-xs-12 box-padding xsmb2 mb1 xsmt2 gallery data-scroll">
        <div class="row">
          <div id="effect-2" class="effects clearfix">
            <?php foreach($allRecord as $value) { ?>
            <div class="col-xs-6 col-md-3 col-sm-3 col-lg-2 mb2 xsmb4 my-class looptr">
              <p class="xsem8 em10 smem10 more-word"><?php echo $value->template_name; ?></p>
              <div class="img tem-hover" id="element1"> <img src="<?php echo site_url('assets/uploads/template_images/'.$value->template_screenshot)?>" class="img-responsive template-img center-block" title="<?php echo $value->template_name; ?>" style="min-height:180px; height:180px;" />
                <div class="overlay">
                 <div class="expand"><a href="<?php echo site_url('assets/uploads/template_images/'.$value->template_screenshot)?>" title="<?php echo $value->template_name; ?>"><i class="fa fa-search-plus" aria-hidden="true" title="<?php echo $value->template_name; ?>" ></i></a> </div>
                </div>
              </div>
              <div id="element2"><a href="<?php echo site_url('edit-template/'.$value->id); ?>" class="template-btn mt8 xsmt8 smem9 xsem8 em10" title="Select Template">Select Template</a></div>
            </div>
            <?php } ?>
			
			<div class="noresult text-center col-xs-12 mb2 xsmb4" style="display:  <?php if(sizeof($allRecord)==0) { ?> block <?php } else { ?>none <?php } ?>;">No Record Found</div>
		  
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<script>
/*-------------------------- Search starts here---------------------------------------------------*/
$('body').delegate(".search", 'keyup', function() {
	var searchVal = $(this).val();
	$( ".looptr:contains('"+searchVal+"')" ).show();
	$('.looptr:not(:contains('+ searchVal +'))').hide(); 
      $( ".noresult").hide();
         if($( ".looptr:contains('"+searchVal+"')" ).length === 0){ 
            $( ".noresult").show();
        }
	
});
/*-------------------------- Search ends here-----------------------------------------------------*/

/*-------------------------- success message starts here---------------------------------------------------*/
<?php if($this->session->userdata('success_msg')) { ?>
new PNotify({
	  title: 'Success',
	  text: '<?php echo $this->session->userdata('success_msg')?>',
	  type: 'success'
	});
<?php $this->session->unset_userdata('success_msg'); } ?>
/*-------------------------- success message ends here---------------------------------------------------*/

/*-------------------------- error message starts here---------------------------------------------------*/
<?php if(validation_errors()!='' || $error!='') { ?>
new PNotify({
	  title: 'Error',
	  text: <?php echo json_encode(validation_errors())?>,
	  type: 'error'
	});
<?php } ?>
<?php if($this->session->userdata('error_msg')) { ?>
new PNotify({
	  title: 'Error',
	  text: '<?php echo $this->session->userdata('error_msg')?>',
	  type: 'error'
	});
<?php $this->session->unset_userdata('error_msg'); } ?>
/*-------------------------- error message ends here---------------------------------------------------*/
</script>
