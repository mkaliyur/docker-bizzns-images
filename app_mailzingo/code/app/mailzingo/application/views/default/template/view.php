<div class="modal-dialog modal-lg">
  <div class="modal-content">
    <div class="modal-header">
      <h4 class="modal-title" id="myModalLabel">
        Template detail</h4>
    </div>
    <div class="modal-body">
      <div class="row">
        <div class="col-md-12 col-sm-6 col-xs-12">
          <div class="x_panel">
            <div class="x_content">
              <table class="table">
                <thead>
                  <tr>
                    <th>Template Name :</th>
                    <th><?php echo $detail->template_name?></th>
                  </tr>
                </thead>
                <thead>
                  <tr>
                    <th colspan="2"><?php echo $detail->template_text?></th>
                  </tr>
                </thead>
              </table>
            </div>
          </div>
        </div>
		
      </div>
    </div>
    <div class="modal-footer">
      <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
    </div>
  </div>
</div>