<script type='text/javascript'> var ckeditor_url = "<?php echo site_url().'assets/default/js/';?>";</script>
<script type='text/javascript' src="<?php echo $this->config->item('templateAssetsPath')?>js/ckeditor/ckeditor.js"></script>
<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padding15 footer-height">
  <!-- dash header-->
  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 mb2 xsmb4">
    <div class="dash-header">
      <div class="row">
        <div class="col-md-7 col-sm-7">
          <h1 class="smart-color em22 smem18 xsem16">Template</h1>
          <p class="em11 smem10 xsem10">Create and modify existing templates easily to get best results</p>
        </div>
        <div class="col-md-5 col-sm-5 xstext-right xstext-center mt2 xsmt4 mb0 xsmb2"> </div>
      </div>
    </div>
  </div>
  <!-- dash header end-->
  <div class="clearfix"></div>
  <div class="col-md-12 smart-tabs clearfix">
    <ul>
      <li><a href="<?php echo site_url('add-template');?>" class="active" title="Create Template"> <span class="hidden-xs">Create Template</span><i class="icon-Create-Template visible-xs"></i> </a></li>
      <li><a href="<?php echo site_url('template-list'); ?>" title="Template"> <span class="hidden-xs">Template</span><i class="icon-Template visible-xs"></i> </a></li>
      <li><a href="<?php echo site_url('template-list').'/draft'; ?>" title="Draft"> <span class="hidden-xs">Draft</span><i class="icon-Drafts visible-xs"></i> </a></li>
    </ul>
  </div>
  <form id="demo-form2" class="form-horizontal form-label-left form_editor" action="" method="post" name="form_editor">
    <div class="col-md-12 col-sm-12 col-xs-12 mb2 xsmb4">
      <div class="layout-box1 clearfix padding0 mailer-data">
        <div class="box-padding smart-field">
          <div class="col-xs-12 xsmb2 mb0 padding0 xsmt2">
            <div class="row">
              <div class="col-md-2 col-sm-3 col-xs-12 xsmb2 mtt1 xsmtt1">Template Name</div>
              <div class="col-md-3 col-sm-5 col-xs-12">
                <input type="text" name="template_name"  placeholder="Template Name" value="<?php echo $template_name?>" >
              </div>
            </div>
          </div>
        </div>
        <div class="clearfix"></div>
        <div class="mt2 xsmt2 editordiv">
          <textarea name="template_text" class="ckeditor form-control col-md-7 col-xs-12"  id="editor" rows="20"><?php echo $template_text?></textarea>
          <input type="hidden" name="image_text" value="" class="image_text" />
        </div>
		<div id="img_val" style="display:none; min-height:800px"></div>
      </div>
      <div class="col-xs-12 col-md-4 col-sm-4 mt2 xsmt8 padding0  xstext-left xstext-center">
        <!--<//?php if($page_title=='Edit Draft'){ ?><a href="#" class="smart-btn" title="Create Message">Create Message</a><//?php } ?>-->
      </div>
      <div class="col-xs-12 col-sm-8 col-md-8 xstext-right xstext-center mt2 xsmt8 padding0">
        <a href=" <?php echo site_url('template-list'); ?>" class="blank-btn" title="Back">Back</a>
        &nbsp; <button type="submit" name="submit" value="save" class="smart-btn savebutt1" style="display:none">Save To Draft</button>
		<button type="button" name="submit" value="save" class="smart-btn savebutt">Save To Draft</button>
		 </div>
    </div>
  </form>
</div>
<!--GET SCREENSHOT -->
<script type="text/javascript" src="<?php echo $this->config->item('templateAssetsPath')?>js/html2canvas.js"></script>
<script type="text/javascript" src="<?php echo $this->config->item('templateAssetsPath')?>js/jquery.plugin.html2canvas.js"></script>

<!--GET SCREENSHOT -->
<script>
/*-------------------------- Submit Form starts here---------------------------------------------------*/
$( ".savebutt" ).click(function() {

	$('#preloader').show();
	$('.editordiv').hide();
	$('#img_val').show();
	$('#img_val').html(CKEDITOR.instances.editor.getData());
	$('#img_val').html2canvas({
		onrendered: function (canvas) {
			  $('.image_text').val(canvas.toDataURL("image/png"));
		}
	});
	
	var delay=1000; //1 second
	setTimeout(function() {
	   $('.savebutt1').click();
	}, delay);

});
/*-------------------------- Submit Form ends here---------------------------------------------------*/


/*-------------------------- success message starts here---------------------------------------------------*/
<?php if($this->session->userdata('success_msg')) { ?>
new PNotify({
	  title: 'Success',
	  text: '<?php echo $this->session->userdata('success_msg')?>',
	  type: 'success'
	});
<?php $this->session->unset_userdata('success_msg'); } ?>
/*-------------------------- success message ends here---------------------------------------------------*/

/*-------------------------- error message starts here---------------------------------------------------*/
<?php if(validation_errors()!='' || $error!='') { ?>
new PNotify({
	  title: 'Error',
	  text: <?php echo json_encode(validation_errors())?>,
	  type: 'error'
	});
<?php } ?>
/*-------------------------- error message ends here---------------------------------------------------*/
</script>
