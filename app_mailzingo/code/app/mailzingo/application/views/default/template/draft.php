<script>
    $(document).ready(function(){
        if (Modernizr.touch) {
            // show the close overlay button
            $(".close-overlay").removeClass("hidden");
            // handle the adding of hover class when clicked
            $(".img").click(function(e){
                if (!$(this).hasClass("hover")) {
                    $(this).addClass("hover");
                }
            });
            // handle the closing of the overlay
            $(".close-overlay").click(function(e){
                e.preventDefault();
                e.stopPropagation();
                if ($(this).closest(".img").hasClass("hover")) {
                    $(this).closest(".img").removeClass("hover");
                }
            });
        } else {
            // handle the mouseenter functionality
            $(".img").mouseenter(function(){
                $(this).addClass("hover");
            })
            // handle the mouseleave functionality
            .mouseleave(function(){
                $(this).removeClass("hover");
            });
        }
    });
</script>
<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padding15 footer-height">
  <!-- dash header-->
  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 mb2 xsmb4">
    <div class="dash-header">
      <div class="row">
        <div class="col-md-7 col-sm-7">
          <h1 class="smart-color em22 smem18 xsem16">Template</h1>
          <p class="em11 smem10 xsem10">Create and modify existing templates easily to get best results</p>
        </div>
        <div class="col-md-5 col-sm-5 xstext-right xstext-center mt2 xsmt4 mb0 xsmb2"> <a href="<?php echo site_url('add-template'); ?>" class="smart-btn">Create Template</a></div>
      </div>
    </div>
  </div>
  <!-- dash header end-->
  <div class="clearfix"></div>
  <div class="col-md-12 smart-tabs clearfix">
    <ul>
      <li><a href="<?php echo site_url('add-template');?>" title="Create Template"> <span class="hidden-xs">Create Template</span><i class="icon-Create-Template visible-xs"></i> </a></li>
      <li><a href="<?php echo site_url('template-list'); ?>" title="Template"> <span class="hidden-xs">Template</span><i class="icon-Template visible-xs"></i> </a></li>
      <li><a href="<?php echo site_url('template-list').'/draft'; ?>" class="active" title="Draft"> <span class="hidden-xs">Draft</span><i class="icon-Drafts visible-xs"> </i> </a></li>
    </ul>
  </div>
  <div class="col-md-12 col-sm-12 col-xs-12 mb2 xsmb4">
    <div class="layout-box1 clearfix">
      <div class="box-padding">
        <div class="col-xs-12 mt1 mb1 xsmb3 w600 em11 smem11 xsem11 padding0">Draft Template</div>
        <div class="col-xs-12 col-lg-2 col-md-3 col-sm-5 padding0 campaign-links">
          <p>Delete Multiple Template</p>
          <ul>
            <li class="mt3 xsmt3 icon-space1">
              <input name="" type="checkbox"  value="" id="select_all"/>
              &nbsp;</li>
            <li class="xsmb4 mb0"><a data-href="<?php echo site_url('delete-multiple-template'); ?>" data-toggle="modal" class="disable multi_action_btn deleteMultiLink" data-target="#delete" title="Delete"><i class="fa fa-trash-o em14 smem14 xsem14" aria-hidden="true"></i></a></li>
          </ul>
        </div>
        <div class="col-xs-12 col-lg-10 col-md-9 col-sm-5 col-sm-offset-2 col-md-offset-0 col-lg-offset-0 col-xs-offset-0 text-right xsmb2 mb0 padding0 template-search">
          <div class="col-lg-3 col-md-5 col-xs-12 pull-right padding0">
            <input type="text" class="search" placeholder="Search Template">
            <button type="button"><i class="fa fa-search"></i></button>
          </div>
        </div>
      </div>
      <div class="col-xs-12 padding0 mt1 line-bottom"></div>
      <div class="clearfix"></div>
      <div class="col-xs-12 box-padding xsmb2 mb1 xsmt2 gallery data-scroll contentDiv">
        <div class="row">
          <div id="effect-2" class="effects clearfix">
            <?php foreach($allRecord as $value) { ?>
            <div class="col-xs-6 col-md-3 col-sm-3 col-lg-2 mb2 xsmb4 my-class looptr">
              <p class="xsem8 em10 smem10 more-text">
                <input name="" type="checkbox"  value="<?php echo $value->id; ?>" class="select_checkbox" />
                <?php echo $value->template_name; ?></p>
              <div class="img tem-hover" id="element1"> <img src="<?php if($value->template_screenshot){echo site_url('assets/uploads/template_images/'.$value->template_screenshot);}else { echo $this->config->item('templateAssetsPath').'images/newesleetr-dummy-big.jpg';} ?>" class="img-responsive template-img center-block" title="Beautiful Image" style="min-height:200px; height:200px;" />
                <div class="overlay">
                  <div class="expand"> <a href="<?php if($value->template_screenshot){echo site_url('assets/uploads/template_images/'.$value->template_screenshot);}else { echo $this->config->item('templateAssetsPath').'images/newesleetr-dummy-big.jpg';} ?>" title="Template Name"><i class="fa fa-search-plus" aria-hidden="true" title="Template Name" ></i></a> <a data-href="<?php echo site_url('delete-template/'.$value->id); ?>" data-toggle="modal" data-target="#delete" class="delete1"><i class="fa fa-trash-o em10 smem12 xsem12 delete-right" aria-hidden="true"></i></a> </div>
                </div>
              </div>
              <div id="element2"><a href="<?php echo site_url('edit-template/'.$value->id.'/draft-edit'); ?>" class="template-btn mt8 xsmt8 smem9 xsem8 em10" title="Select Template">Select Template</a></div>
            </div>
            <?php } ?>
			<div class="noresult text-center col-xs-12 mb2 xsmb4" style="display:  <?php if(sizeof($allRecord)==0) { ?> block <?php } else { ?>none <?php } ?>;">No Record Found</div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<script>
/*-------------------------- Search starts here---------------------------------------------------*/
$('body').delegate(".search", 'keyup', function() {
	var searchVal = $(this).val();
	$( ".looptr:contains('"+searchVal+"')" ).show();
	$('.looptr:not(:contains('+ searchVal +'))').hide(); 
      $( ".noresult").hide();
         if($( ".looptr:contains('"+searchVal+"')" ).length === 0){ 
            $( ".noresult").show();
            $('#select_all').attr('disabled',true);
        }else{
            $('#select_all').attr('disabled',false);
        }
	
});
/*-------------------------- Search ends here---------------------------------------------------*/

/*-------------------------- success message starts here---------------------------------------------------*/
<?php if($this->session->userdata('success_msg')) { ?>
new PNotify({
	  title: 'Success',
	  text: '<?php echo $this->session->userdata('success_msg')?>',
	  type: 'success'
	});
<?php $this->session->unset_userdata('success_msg'); } ?>
/*-------------------------- success message ends here---------------------------------------------------*/

/*-------------------------- error message starts here---------------------------------------------------*/
<?php if(validation_errors()!='' || $error!='') { ?>
new PNotify({
	  title: 'Error',
	  text: <?php echo json_encode(validation_errors())?>,
	  type: 'error'
	});
<?php } ?>
/*-------------------------- error message ends here---------------------------------------------------*/

/*-------------------------- check/uncheck multiple campaign starts here ------------------------------------*/
$('body').delegate("#select_all", 'change', function() {
      $(".select_checkbox").prop('checked', $(this).prop("checked"));
	  $(".select_checkbox").change();
      });
$('body').delegate(".select_checkbox", 'change', function() {
	  var length = $('.select_checkbox:checked').length
	  if(length==0)
	   {
		  $(".multi_action_btn").addClass('disable');
	   } else {
		  $(".multi_action_btn").removeClass('disable');
	   }
      });
$(".select_checkbox").change(function(){
    if ($('.select_checkbox:checked').length != $('.select_checkbox').length) {
       $("#select_all").prop('checked', false);
    }else{
        $("#select_all").prop('checked', true);
    }
});

/*-------------------------- check/uncheck multiple campaign ends here ------------------------------------*/

/*-------------------------- delete single campaign starts here ------------------------------------*/
$('body').delegate(".delete1", 'click', function() {
    
	var deleteurl = $(this).attr('data-href');
	 $('#delete_yes_button').attr('href',deleteurl);
	 $('#delete_yes_button').removeClass('deleteMultiple');
});
/*-------------------------- delete single campaign ends here ------------------------------------*/

/*-------------------------- delete multiple campaign starts here ------------------------------------*/
$('body').delegate(".deleteMultiLink", 'click', function() {
    
	var length = $('.select_checkbox:checked').length;
	if(length==0)
	{ return false; }
	deleteurl = $(this).attr('data-href');
	 $('#delete_yes_button').attr('href','javascript:');
	 $('#delete_yes_button').addClass('deleteMultiple');
});
$('body').delegate(".deleteMultiple", 'click', function() {

    var posturl = site_url+'delete-multiple-template';
	var id = [];
	$(".select_checkbox:checkbox:checked").each(function (e) {
            id[e] = $(this).val();
        });
	var length = $('.select_checkbox:checked').length;
	if(length==0)
	{ return false; }

    $.ajax({
        url: posturl,
        dataType: 'json',
        type: "POST",
        data: {id: id},
        success: function(data) {
            if (data.success) {
			    $('#delete').modal('toggle');
//				PNotify.removeAll();
//				new PNotify({title: 'Success',text: 'Draft Deleted successfully',type: 'success'});
//                $(".contentDiv").load(location.href + " .contentDiv");
                                window.location.reload(true);
            }
        },
    });
});
/*-------------------------- delete multiple campaign ends here------------------------------------*/

</script>
