<script type="text/javascript">
$( document ).ready(function() { 
	
	<?php if($time_type=='day') { ?>
	var datetime = '%d';
	<?php } else if($time_type=='month') { ?>
	var datetime = '%b';
	<?php } else if($time_type=='year') { ?>
	var datetime = '%Y';
	<?php } ?>


	<?php if($graph_type=='subscriber') { ?>
	var GraphID = 'subchart';
	var key =  'Subscriber';
	<?php } else { ?>
	var GraphID = 'unsubchart';
	var key =  'Unsubscriber';
	<?php } ?>
	
	var Xtitle = 'Time';
	var Ytitle = 'Count';
	 
	var sin = [ <?php echo $data?> ];
	var passdata = [ {
					  values: sin,
				      key: key,
					  color: '#4e72bb',
				    } ];
	
			nv.addGraph(function() {
			chart = nv.models.lineChart();
				// chart sub-models (ie. xAxis, yAxis, etc) when accessed directly, return themselves, not the parent chart, so need to chain separately
				chart.xScale(d3.time.scale());
				chart.xAxis
					.axisLabel(Xtitle)
					//.ticks(8)
					
					.tickFormat(function(d) {
					  // return d3.time.format('%d %b %y')(new Date(d));
						 return d3.time.format(datetime)(new Date(d));
						 
					});
				chart.showLegend(false);
				chart.yAxis
					.axisLabel(Ytitle)
					//.ticks(2)
					.tickFormat(function(d){
						if (d == null) {
							return 'N/A';
						}
						//return d3.format('')(d);
						return d3.format(',.0d')(d);
					});

				//data = GrapsArray();
				d3.select('#'+GraphID).html('').append('svg')
					.datum(passdata)
					.call(chart);
				nv.utils.windowResize(chart.update);
				
				return chart;
			});
	
});

<!----------------------------- graph js end----------------------------->
</script>
 <?php if($graph_type=='subscriber') { ?>
<div id="subchart"></div>
<?php } else { ?>
<div id="unsubchart"></div>
<?php } ?>
