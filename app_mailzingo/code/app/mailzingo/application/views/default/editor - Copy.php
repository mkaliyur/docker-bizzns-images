<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title><?php echo $this->config->item('site_title')?></title>
<!-- Tell the browser to be responsive to screen width -->
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
<!-- fonts-->
<link rel="stylesheet" href="<?php echo $this->config->item('templateAssetsPath')?>css/font-awesome.min.css" type="text/css" />
<link href='https://fonts.googleapis.com/css?family=Roboto:300,400,600,800' rel='stylesheet' type='text/css'>
<link href='https://fonts.googleapis.com/css?family=Raleway:400,600,800' rel='stylesheet' type='text/css'>
<link rel="stylesheet" href="<?php echo $this->config->item('templateAssetsPath')?>css/bootstrap.min.css" type="text/css" />
<!-- smart mailer design css-->
<link rel="stylesheet" href="<?php echo $this->config->item('templateAssetsPath')?>css/style.css" type="text/css" />
<link rel="stylesheet" href="<?php echo $this->config->item('templateAssetsPath')?>css/inline-editor.css" type="text/css" />
<!-- scroll css-->
<link rel="stylesheet" href="<?php echo $this->config->item('templateAssetsPath')?>css/ediotor-scroll.css" type="text/css" />
<script type='text/javascript' src="<?php echo $this->config->item('templateAssetsPath')?>js/jquery.min.js"></script>
<script type='text/javascript' src="<?php echo $this->config->item('templateAssetsPath')?>js/bootstrap.min.js"></script>
<!-- scroll js-->
<script type='text/javascript' src="<?php echo $this->config->item('templateAssetsPath')?>js/CustomScrollbar.min.js"></script>
<!--- hide show div-->
<script type="text/javascript">
$( document ).ready(function() {
	$( "#preview-temp" ).hide();
	$( "#opendiv" ).click(function() {
	$( "#preview-temp" ).show();
});
	$( "#closediv" ).click(function() {
		$( "#preview-temp" ).hide();
	});
});
</script>
<!--- hide show div end-->

<!--- iframe auto height start js-->
<script>
$( document ).ready(function() {
$('.iframe-full-height').on('load', function(){
    this.style.height=this.contentDocument.body.scrollHeight +'px';
});
});
</script>
<!--- iframe auto height start js end-->

</head>
<body class="scroll-hidden" onResize="myScrollfunction()" onload="myScrollfunction()">
<!-------------------------header start-------------------------------------------------->
<div class="inline-editor-header inline-editor-tab clearfix">
<ul>
<li>
<a data-toggle="modal" data-target="#msgattach">
<i class="fa fa-paperclip em14 fa-rotate-90" aria-hidden="true"></i>
&nbsp; 5 Attached</a>
</li>
<!--<li class="dropdown">
<a class="dropdown-toggle personalization-menu-width" type="button" data-toggle="dropdown">Personalize
<span class="caret"></span></a>
<ul class="dropdown-menu personalization-menu mCustomScrollbar" style="max-height:320px;"> 
<li><a href="#">[[name]]</a></li>
<li><a href="#">[[firstname]]</a></li>
<li><a href="#">[[lastname]]</a></li>
<li><a href="#">[[email]]</a></li>
<li><a href="#">[[ref]]</a></li>
<li><a href="#">[[geo_country]]</a></li>
<li><a href="#">[[geo_country_co...]]</a></li>
<li><a href="#">[[geo_city]]</a></li>
<li><a href="#">[[geo_region]]</a></li>
<li><a href="#">[[geo_postal]]</a></li>
<li><a href="#">[[ip]]</a></li>
<li><a href="#">[[responder]]</a></li>
<li><a href="#">[[myname]]</a></li>
<li><a href="#">[[myemail]]</a></li>
<li><a href="#">[[campaign]]</a></li>
</ul>
</li> -->
<li><a data-toggle="modal" data-target="#testMail" class="xsmt2 mt0">Test Mail</a></li>
<li class="inline-btn-right"><a id="opendiv" class="btn-width xsmt2 mt0">Preview</a></li>
</ul>
</div>

<!-------------------------header end-------------------------------------------------->

<!-------------------------content section start-------------------------------------------------->
<div class="inline-editor-content">
<div class="content-section">
<iframe src="http://localhost/smartmailer/Editor?id=<?php echo  $_GET['id']; ?>" height="100%" frameborder="0" scrolling="yes" id="m-scroll" class="iframe-full-height"></iframe>
</div>
</div>
<!-------------------------content section end-------------------------------------------------->

<!-------------------------footer start-------------------------------------------------->
<div class="inline-editor-footer inline-editor-tab clearfix">
<div class="row">
<div class="col-xs-12 col-md-6 col-sm-4 xstext-center xstext-left">
<ul>
<li><a href="#" class="inline-editor-btn">Back</a></li>
</ul>
</div>

<div class="col-xs-12 col-xs-12 col-md-6 col-sm-8 mt0 xsmt2 text-right">
<ul>
<li><a href="#" class="btn-width">Save Template</a></li>
<li><a href="#"  class="inline-editor-btn">Next</a></li>
</ul>
</div>

</div>
</div>
<!-------------------------footer end-------------------------------------------------->

<!--------------------------------  preview modal div start ------------------------------------------>
<div id="preview-temp" class="tempreview-modal">
<a href="javascript:void(0)" id="closediv" class="close-modal">X</a>
<div class="template-heading"><p>Tamplate Name</p></div>
<div class="template-tab-bg">
<div class="temp-tabs-nav">
<ul class="nav nav-tabs center-tabs centered border-hidden">
<li class="active"><a data-toggle="tab" href="#desktop">Desktop</a></li>
<li><a data-toggle="tab" href="#mobile">Mobile</a></li>
</ul>
</div>
</div>

<div class="tab-content">
<div id="desktop" class="tab-pane fade in active">
<div class="col-xs-12 editor-content">
<div class="inline-editor-section">
<iframe src="http://www.w3schools.com/bootstrap/" height="100%" frameborder="0" scrolling="yes" id="pre-scroll" class="iframe-full-height"></iframe>
</div>
</div>
</div>

<div id="mobile" class="tab-pane fade">
<div class="col-xs-12 mobile-editor-content">
<div class="mobile-editor-section">
<iframe src="http://www.w3schools.com/bootstrap/" scrolling="yes" width="100%" height="385px" frameborder="0"></iframe>
</div>
</div>
</div>

</div>
</div>
<!--------------------------------  preview modal div end ------------------------------------------>
</div>



<!-- message attachment modalpopup-->
<div id="msgattach" class="modal fade" role="dialog">
  <div class="modal-dialog campaign-modal">
    <div class="modal-content">
      <div class="modal-body padding0 clearfix xsmb4 xsmt0 mt4 mb2">
        <div class="col-md-5 col-sm-5 col-xs-12 mt7 xsmt4 mb4">
          <div class="col-xs-12 bottom-border"> <img src="<?php echo $this->config->item('templateAssetsPath')?>images/import-pc.png" class="img-responsive center-block mobile-img" /></div>
        </div>
        <div class="col-md-7 col-sm-7 col-xs-12 left-border campaign-field xsmt4 mt0 ">
          <div class="col-xs-12">
            <h3 class="em16 smem16 xsem12">Attachment</h3>
			<form action="<?php echo site_url('add-temp-attatchment')?>" enctype="multipart/form-data" class="attatchForm" method="post">
            <label class="custom-file-upload mt6 mb4">
            <input type="file" name="attatchment" class="attatchment"/>
            <span class="w300">Add Attachment </span></label>
			<div class="aWait_div" style="display:none"><img src="<?php echo $this->config->item("templateAssetsPath")?>images/loader.gif" class="center-block" /></div>
			</form>
			<div class="attachedDiv">
			<?php $attatchment = $newsletter_session['attatchment']; 
			 foreach($attatchment as $valattach){ ?>
			<div><div class="col-md-10 col-sm-10 col-xs-10 mt1 xsmt0 mb1 xsmb1 text-left padding0 em9 smem9 xsem9 mt1 xsmt0"><?php echo $valattach['name'].' ('.$valattach['size'].' kb)'?></div><div class="col-md-2 col-sm-2 col-xs-2 mt2 xsmt2 text-right mb1 xsmb1"> <a href="javascript:" class="smart-grey w300 deleteAttatch" data-name="<?php echo $valattach['name']?>"><i class="fa fa-times" aria-hidden="true"></i></a></div></div>
			<?php } ?>
			</div>
            

            <div class="clearfix"></div>
            <div class="text-right mt14 xsmt6 xstext-right xstext-center"> <a href="#" class="blank-btn" data-dismiss="modal">Close</a> </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<!-- Test Mail modalpopup-->
<div id="testMail" class="modal fade" role="dialog">
  <div class="modal-dialog campaign-modal">
    <div class="modal-content">
      <div class="modal-body padding0 clearfix xsmb4 xsmt4 ">
       <div class="col-md-5 col-sm-5 col-xs-12 mt1 xsmt2">
       <div class="col-xs-12 bottom-border">
      
       <img src="<?php echo $this->config->item('templateAssetsPath')?>images/testmail.png" class="img-responsive center-block mobile-img" /></div>
       </div>
       <div class="col-md-7 col-sm-7 col-xs-12 left-border campaign-field xsmt4 mt0 ">
        <div class="col-xs-12 mb2">
       <h3 class="em16 smem16 xsem14">Sent Test Mail</h3>
        <div class="form-group mt12 xsmt12">
		<label class="w300 sr-only">Campaign Name</label>
		<input type="text" name="testemail" placeholder="abc@sitename.com" required class="form-control testemail" />
		</div>
        <p class="em9 smem9 xsem9"><span class="smart-red">Note : </span> Test Your Message
By Delivering Them To Your Email Address.</p>
        
        <div class="text-right mt12 xsmt12 xsmb4 xstext-right xstext-center"><a href="#" class="blank-btn" data-dismiss="modal">Cancel</a> <a href="javascript:" class="smart-btn sendTestmail">Send</a></div>
		<div class="testWait_div" style="display:none"><img src="<?php echo $this->config->item("templateAssetsPath")?>images/loader.gif" class="center-block" /></div>
       </div></div>
      </div>
    </div>
  </div>
</div>

<!------------------------ height scroll js --------------------------------> 
<script type="text/javascript">
function myScrollfunction() {
    var w = document.innerWidth;
    var h = $(window).height();
	var h_head = $(".inline-editor-header").outerHeight();
	var h_foot = $(".inline-editor-footer").outerHeight();
	var h_prev = $(".template-heading").outerHeight();
	var h_tab = $(".template-tab-bg").outerHeight();
	
//console.log(h+":"+h_head+":"+h_foot+"="+(h-h_head-h_foot));
//document.getElementById("noti-scroll").innerHTML = "Width: " + w + "<br>Height: " + h;
document.getElementById("m-scroll").style.maxHeight = (h-h_head-h_foot)+"px";
document.getElementById("pre-scroll").style.maxHeight = (h-h_prev-h_tab)+"px";
$(".inline-editor-content").css("padding-top",h_head+"px");
}
</script> 
<!------------------------ height scroll js end --------------------------------> 
</body>
</html>