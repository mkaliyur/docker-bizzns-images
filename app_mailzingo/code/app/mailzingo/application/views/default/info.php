<!DOCTYPE html>
<html lang="en">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<!-- Tell the browser to be responsive to screen width -->
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
<!-- fonts-->
<link rel="stylesheet" href="<?php echo $this->config->item('templateAssetsPath')?>css/font-awesome.min.css" type="text/css" />
<link href='https://fonts.googleapis.com/css?family=Roboto:300,400,500,600,800' rel='stylesheet' type='text/css'>
<link href='https://fonts.googleapis.com/css?family=Raleway:400,500,600,800' rel='stylesheet' type='text/css'>
<link rel="icon" href="<?php echo $this->config->item('templateAssetsPath')?>images/favicon-icon.png" type="image/png"/>
<link rel="stylesheet" href="<?php echo $this->config->item('templateAssetsPath')?>css/bootstrap.min.css" type="text/css" />
<!-- smart mailer design css-->
<link rel="stylesheet" href="<?php echo $this->config->item('templateAssetsPath')?>css/style.css" type="text/css" />
<title>
<?php echo $this->config->item('site_title')?>
</title>
<style type="text/css">
footer{position:fixed;bottom:0;}
.dbfooter a.social{padding: 4px 0px; !important; width:27px !important;}
</style>
</head>
<body class="info-bg">
<img src="<?php echo $this->config->item('templateAssetsPath')?>images/smart-logo.png" class="img-responsive center-block">
<div class="info-box-width col-md-7 col-sm-10 col-xs-9" align="center">
<h1 class="smart-color em30 smem30 xsem25 roboto">SUCCESS !!</h1>
<p class="em16 mt3 xsmt4 w300 roboto">
<?php echo $this->session->userdata('success_msg')?>
</p>
<?php $this->session->unset_userdata('success_msg'); ?>
</div>

</body>
</html>
