<script type="text/javascript">
var AssetsURL = 'http://bigwigvideo.co/assets/front_end/dashboard/';
</script>

<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padding15 footer-height">
  <!-- dash header-->
  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 mb2 xsmb4">
    <div class="dash-header">
      <h1 class="smart-color em22 smem18 xsem16">MailZingo Video Training</h1>
      <p class="em11 smem10 xsem10">Let's see how MailZingo works</p>
    </div>
  </div>
  <!-- dash header end-->
  <div class="clearfix"></div>
  
<div class="col-md-12 smart-tabs clearfix">
<ul>
<li><a href="<?php echo site_url().'training';?>"  class="<?php if($page_name == 'Training Videos'){ echo "active";}?>" title="Training Videos">
<span class="hidden-xs">Training Videos</span><i class="icon-video visible-xs"></i>
</a></li>
<!--
<li><a href="<?php //echo site_url().'training-pdf';?>" class="<?php //if($page_name=='Training Pdf'){ echo "active";}?>" title="Pdf Documents">
<span class="hidden-xs">Pdf Documents </span><i class="fa fa-file-pdf-o visible-xs"></i>
</a></li>
-->
</ul>
</div>


<div class="col-md-12 col-sm-12 col-xs-12 mb2 xsmb4">
<div class="layout-box1 clearfix">
<div class="mt1 xsmt2">
<?php

if($fetch_data)
{
	foreach($fetch_data as $video1)
	{
		
		echo '<div class="col-md-12 col-sm-12 col-xs-12 mdem12 smem11 xsem10 text-center smart-color w600 greycolor mb2 xsmb4 text-capitalize">'.$video1[0]->category_title.'</div>
				<div class="col-md-12 col-sm-12 col-xs-12 mb2 xsmb4">';
		
		foreach($video1 as $video){
		if($video->type =='y')
		{
			if (strpos($video->url, 'watch?v=') !== false)
			$youtube_id = str_replace('https://www.youtube.com/watch?v=','',$video->url); 
			else
			$youtube_id = str_replace('https://youtu.be/','',$video->url);
			
			if($video->video_thumbnail && $video->video_thumbnail!='')
			{
				$video_thumbnail = $this->config->item('membersarea_assets').'uploads/training_video_image/'.$video->video_thumbnail;
			}
			else{
				$video_thumbnail ='https://img.youtube.com/vi/'.$youtube_id.'/hqdefault.jpg';
			}
			echo '<div class="col-md-3 col-sm-3 col-xs-12 mb2 xsmb4">
			<div class="traning-video">
			<a href="https://youtu.be/'.$youtube_id.'" onclick="_gaq.push([&#39;_trackEvent&#39;, &#39;outbound-article&#39;, &#39;https://youtu.be/'.$youtube_id.';, &#39;&#39;]);" class="wplightbox" data-group="gallery0" data-thumbnail="'.$this->config->item('assetsPath').'images/training-videos.png" title="">
			<img src="'.$video_thumbnail.'" class="img-responsive center-block"></a>
			<div class="mdem11 smem10 xsem10 channel mt4 xsmt4 mb2 xsmb2 text-center text-capitalize more-word">'.$video->title.'</div></div>
			</div>';
		}
		else if($video->type =='vw')
		{
			
		  $vimeo_id = str_replace('https://videowhizz.saglus.com/embed/','',$video->url);
		  if($video->video_thumbnail && $video->video_thumbnail!='')
			{
				$video_thumbnail = $this->config->item('membersarea_assets').'uploads/training_video_image/'.$video->video_thumbnail;
			}
			else{
				$video_thumbnail ='https://s3.amazonaws.com/videowhizz/UploadVideos/'.$vimeo_id.'/'.$vimeo_id.'.png';
			}
			echo '
			<div class="col-md-3 col-sm-3 col-xs-12 mb2 xsmb4">
			<div class="traning-video">
			<a href="https://videowhizz.saglus.com/embed/'.$vimeo_id.'" onclick="_gaq.push([&#39;_trackEvent&#39;, &#39;outbound-article&#39;, &#39;https://videowhizz.saglus.com/embed/njun1f5tmp?rel=0&amp;vq=hd1080&#39;, &#39;&#39;]);" class="wplightbox" data-group="gallery0" data-thumbnail="https://videowhizz.saglus.com/embed/'.$vimeo_id.'" title="">
			<img src="'.$video_thumbnail.'" class="img-responsive center-block"></a>
			<div class="mdem11 smem10 xsem10 channel  mt4 xsmt4 mb2 xsmb2 text-center text-capitalize more-word">'.$video->title.'</div>
			</div>
			</div>'; 
		}
		}
		echo '</div>'; 
	}
}	
else
{
	echo '<div class="col-xs-12"><div class="alert alert-record alert-dismissible col-xs-12" role="alert">Record Not Found.</div></div>';
}
?>

  </div>




<div class="clearfix"></div>



</div>




</div>
  
  
    
    </div>
 <script type='text/javascript' src='<?php echo $this->config->item('membersarea_url')?>assets/dashboard/js/wonderpluginlightbox.js'></script>

