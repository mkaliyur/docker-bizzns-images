<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padding15 footer-height">
  <!-- dash header-->
  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 mb2 xsmb4">
    <div class="dash-header">
      <h1 class="smart-color em22 smem18 xsem16">MailZingo PDF Documents</h1>
      <p class="em11 smem10 xsem10">Let's see how MailZingo PDF Documents</p>
    </div>
  </div>
  <!-- dash header end-->
  <div class="clearfix"></div>
  
<div class="col-md-12 smart-tabs clearfix">
<ul>
<li><a href="<?php echo site_url().'training';?>"  class="<?php if($page_name == 'Training Videos'){ echo "active";}?>" title="Training Videos">
<span class="hidden-xs">Training Videos</span><i class="icon-video visible-xs"></i>
</a></li>
<li><a href="<?php echo site_url().'training-pdf';?>" class="<?php if($page_name=='Training Pdf'){ echo "active";}?>" title="Pdf Documents">
<span class="hidden-xs">Pdf Documents</span><i class="fa fa-file-pdf-o visible-xs"></i>
</a></li>
</ul>
</div>


<div class="col-md-12 col-sm-12 col-xs-12 mb2 xsmb4">
<div class="layout-box1 clearfix">
<div class="mt1 xsmt2">
<?php

if($fetch_data)
{
	foreach($fetch_data as $pdf1)
	{
		
		echo '<div class="col-md-12 col-sm-12 col-xs-12 mdem12 smem11 xsem10 text-center smart-color w600 greycolor mb2 xsmb4 text-capitalize">'.$pdf1[0]->category_title.'</div>
				<div class="col-md-12 col-sm-12 col-xs-12 mb2 xsmb4">';
		
		foreach($pdf1 as $pdf){
			echo '<div class="col-md-2 col-sm-3 col-xs-12 mb2 xsmb4">
			<div class="traning-video">
			<a href="'.$this->config->item('membersarea_assets').'uploads/training_pdf/'.$pdf->pdf_file.'" target="_blank" class="wplightbox" data-group="gallery0" title="">
			<img src="'.$this->config->item('membersarea_assets').'dashboard/images/no_image.jpg" class="img-responsive center-block"></a>
			<div class="mdem11 smem10 xsem10 channel mt4 xsmt4 mb2 xsmb2 text-center text-capitalize more-word">'.$pdf->title.'</div></div>
			</div>';
		} 
		echo '</div>'; 
	}
}	
else
{
	echo '<div class="col-xs-12"><div class="alert alert-record alert-dismissible col-xs-12" role="alert">Record Not Found.</div></div>';
}
?>

  </div>




<div class="clearfix"></div>



</div>




</div>
  
  
    
    </div>
 <script type='text/javascript' src='<?php echo $this->config->item('membersarea_url')?>assets/dashboard/js/wonderpluginlightbox.js'></script>

