<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padding15 footer-height">
<!-- dash header-->
<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 mb2 xsmb4">
<div class="dash-header">
<div class="row">
<div class="col-md-12 col-sm-12"><h1 class="smart-color">Edit Subscriber</h1>
</div>
<!--<div class="col-md-5 col-sm-5 xstext-right xstext-center mt2 xsmt4 mb0 xsmb2">
<a href="#" class="smart-btn" data-toggle="modal" data-target="#myModal">Create Campaign</a></div> -->
</div>
</div>
</div>
<!-- dash header end-->

<div class="clearfix"></div>
<div class="col-md-12 col-sm-12 col-xs-12 mb2 xsmb4">
<form action="" method="post" >
<div class="col-md-12 col-sm-12 col-xs-12 mb2 xsmb4 padding0">
<div class="layout-box clearfix">
<!-- content start-->
<div class="col-md-12 mt2 mobile-margin padding0 smart-field">

<div class="col-xs-12 xsmb4 mb0 padding0">
<div class="row">
<div class="col-md-2 col-sm-3 col-xs-12 mt2 xsmt2">Email Address</div>
<div class="col-md-4 col-sm-5 col-xs-12 mt2 xsmt2">
<input name="email" type="text" placeholder="Email Address" value="<?php echo $email?>"></div></div>
</div>

<div class="col-xs-12 xsmb4 mb0 padding0">
<div class="row">
<div class="col-md-2 col-sm-3 col-xs-12 mt2 xsmt2">Name</div>
<div class="col-md-4 col-sm-5 col-xs-9 mt2 xsmt2">
<input name="name" type="text" placeholder="Name" value="<?php echo $name?>"></div>
</div>	
</div>	
<?php if(sizeof($customFieldArray)==0) { ?>
<div class="col-xs-12 padding0 fieldDiv">
<div class="row">
<div class="col-md-2 col-sm-3 col-xs-6 mt2 xsmt2"><select name="fieldname[]" class="fieldSelect">
    <option value="">Select Field</option>
	<option value="address">Address</option>
    <option value="city">City</option>
    <option value="state">State</option>
    <option value="zip">Zip</option>
    <option value="country">Country</option>
    <option value="phone">Phone</option>
    <option value="custom_1">Custom Field 1</option>
    <option value="custom_2">Custom Field 2</option>
    <option value="custom_3">Custom Field 3</option>
    </select></div>
<div class="col-md-4 col-sm-5 col-xs-6 mt2 xsmt2">
<input name="fieldvalue[]" type="text" ></div>

<div class="col-md-1 col-sm-2 col-xs-2 xstext-right1 xstext-center1 xsmt7 mtt2  cloum-width1 removeButDiv" style="display:none">
<a href="javascript:" class="cross-icon removeField"><i class="fa fa-remove" aria-hidden="true"></i></a></div>
<div class="col-md-1 col-sm-2 col-xs-2 xstext-right1 xstext-center1 xsmt7 mtt2  cloum-width1 addButDiv">
<a href="javascript:" class="cross-icon1 addField"><i class="fa fa-plus" aria-hidden="true"></i></a></div>
</div>
</div>
<?php } else { 
$keyArray = array();
foreach($customFieldArray as $keycK=>$valueCK)
$keyArray[] = $keycK;
$i=0;
  foreach($customFieldArray as $keyc=>$valueC) {  ?>

<div class="col-xs-12 padding0 fieldDiv">
<div class="row">
<div class="col-md-2 col-sm-3 col-xs-6 mt2 xsmt2"><select name="fieldname[]" class="fieldSelect">
    <option value="">Select Field</option>
	<option value="address" <?php if(in_array('address',$keyArray)) { ?> style="display:none" <?php } ?> <?php if($keyc=='address') { ?> selected="selected" <?php } ?> >Address</option>
    <option value="city" <?php if(in_array('city',$keyArray)) { ?> style="display:none" <?php } ?>  <?php if($keyc=='city') { ?> selected="selected" <?php } ?> >City</option>
    <option value="state" <?php if(in_array('state',$keyArray)) { ?> style="display:none" <?php } ?> <?php if($keyc=='state') { ?> selected="selected" <?php } ?> >State</option>
    <option value="zip" <?php if(in_array('zip',$keyArray)) { ?> style="display:none" <?php } ?>  <?php if($keyc=='zip') { ?> selected="selected" <?php } ?> >Zip</option>
    <option value="country" <?php if(in_array('country',$keyArray)) { ?> style="display:none" <?php } ?> <?php if($keyc=='country') { ?> selected="selected" <?php } ?>>Country</option> 
    <option value="phone" <?php if(in_array('phone',$keyArray)) { ?> style="display:none" <?php } ?>  <?php if($keyc=='phone') { ?> selected="selected" <?php } ?>>Phone</option>
    <option value="custom_1" <?php if(in_array('custom_1',$keyArray)) { ?> style="display:none" <?php } ?> <?php if($keyc=='custom_1') { ?> selected="selected" <?php } ?>>Custom Field 1</option> 
    <option value="custom_2" <?php if(in_array('custom_2',$keyArray)) { ?> style="display:none" <?php } ?> <?php if($keyc=='custom_2') { ?> selected="selected" <?php } ?>>Custom Field 2</option> 
    <option value="custom_3" <?php if(in_array('custom_3',$keyArray)) { ?> style="display:none" <?php } ?> <?php if($keyc=='custom_3') { ?> selected="selected" <?php } ?>>Custom Field 3</option> 
    </select></div>
<div class="col-md-4 col-sm-5 col-xs-6 mt2 xsmt2">
<input name="fieldvalue[]" type="text" value="<?php echo $valueC?>"></div>

<div class="col-md-1 col-sm-2 col-xs-2 text-center xsmt7 mtt2  cloum-width removeButDiv" style="display:<?php if($i>0){ echo 'block'; } else { echo 'none'; } ?>">
<a href="javascript:" class="cross-icon removeField"><i class="fa fa-remove" aria-hidden="true"></i></a></div>
<?php if($i==0){ ?>
<div class="col-md-1 col-sm-2 col-xs-2 text-center xsmt7 mtt2  cloum-width addButDiv">
<a href="javascript:" class="cross-icon smart-btn addField"  <?php if(sizeof($customFieldArray)>=9){ ?> style="display:none" <?php } ?>><i class="fa fa-plus" aria-hidden="true"></i></a></div>
<?php } ?>
</div>
</div>

<?php $i++; } } ?>


</div>
<!-- content end-->
</div>
</div>

<div class="col-md-12 col-sm-12 col-xs-12 xstext-right xstext-center xsmb2 mb2 padding0">
<a href="<?php echo site_url('contact-list')?>" class="blank-btn" title="Back"> Back</a>  &nbsp;
<button type="submit" name="submit_6" value="yes" class="smart-btn" title="Update"> Update </button> </div>
</form>


</div>
</div>
<script>
/*-------------------------- Add New Field starts here---------------------------------------------------*/
$(document ).on( "click",".removeField", function() {
     var selectedfield = $(this).parent().parent().find('.fieldSelect').val();
      if($('.fieldDiv').length>1)
     $(this).parent().parent().parent().remove();
	  if($('.fieldDiv').length<9)
	 $('.addField').show();
	 $('.fieldSelect').find('option[value="'+selectedfield+'"]').show();
 });
$('body').delegate(".addField", 'click', function() {
      var clonedata = $(".fieldDiv:first").clone().find("input:text").val("").end().find(':selected').removeAttr('selected').end();
	  clonedata.find('.addButDiv').remove();
	  clonedata.find('.removeButDiv').show();
	  $('.fieldDiv:last').after(clonedata);
	  setSelectBox();
	   if($('.fieldDiv').length>=9)
	  { $(this).hide(); }
 });
$(document ).on( "change",".fieldSelect", function() {
	  setSelectBox();
 });
function setSelectBox(){

 $('.fieldSelect').each(function(){
   //restore previously selected value
   var prevValue = $(this).data('previous');
   $('.fieldSelect').not(this).find('option[value="'+prevValue+'"]').show(); 
  
   //hide option selected now
   var value = $(this).val();
   //update previously selected data
   $(this).data('previous',value);
   $('.fieldSelect').not(this).find('option[value="'+value+'"]').hide();
  });
}
/*-------------------------- Add New Field ends here---------------------------------------------------*/

/*-------------------------- campaign multi select div starts here---------------------------------------------------*/
$(window).load(function(){
$('#multi-cam-list').focus(function() {
    $('div.multi-cam-list').show();
    $(document).bind('focusin.multi-cam-list click.example',function(e) {
        if ($(e.target).closest('.multi-cam-list, #multi-cam-list').length) return;
        $(document).unbind('.multi-cam-list');
        $('div.multi-cam-list').fadeOut('medium');
    });
});
$('div.multi-cam-list').hide();
});
/*-------------------------- campaign multi select div ends here---------------------------------------------------*/


/*-------------------------- error message starts here---------------------------------------------------*/
<?php if(validation_errors()!='' || $error!='') { ?>
new PNotify({
	  title: 'Error',
	  text: <?php echo json_encode(validation_errors())?>,
	  type: 'error'
	});
<?php } ?>
/*-------------------------- error message ends here---------------------------------------------------*/
</script>
