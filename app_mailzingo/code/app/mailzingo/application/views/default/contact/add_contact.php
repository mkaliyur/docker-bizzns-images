<!------------------------------------------- tagsinput js start ------------------------------------------------------->
<script type="text/javascript" src="<?php echo $this->config->item('templateAssetsPath')?>js/jquery.tagsinput.js"></script>
<script type="text/javascript">

		function onAddTag(tag) {
			alert("Added a tag: " + tag);
		}
		function onRemoveTag(tag) {
			alert("Removed a tag: " + tag);
		}

		function onChangeTag(input,tag) {
			alert("Changed a tag: " + tag);
		}

		$(function() {

			$('#tags_1').tagsInput({width:'auto'});
		
		$('input.tags').tagsInput({interactive:false});
		});

</script>
<style type="text/css">
div.tagsinput
{
	height:180px !important;
}
</style>
<!-------------------------------------------- tagsinput js end  -------------------------------------------------------->
<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padding15 footer-height">
<!-- dash header-->
<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 mb2 xsmb4">
<div class="dash-header">
<div class="row">
<div class="col-md-12 col-sm-12"><h1 class="smart-color">Add Subscribers</h1>
<p>Add your subscribers in 3 easy ways</p></div>
<!--<div class="col-md-5 col-sm-5 xstext-right xstext-center mt2 xsmt4 mb0 xsmb2">
<a href="javascript:void(0)" class="smart-btn" data-toggle="modal" data-target="javascript:void(0)myModal">Create Campaign</a></div> -->
</div>
</div>
</div>
<!-- dash header end-->

<div class="clearfix"></div>
<div class="col-md-12 col-sm-12 col-xs-12 mb2 xsmb4">
<div class="smart-tabs clearfix">
<ul>
<li><a href="<?php echo site_url('add-contact')?>" class="active" title="Add Subscriber">
<span class="hidden-xs">Add Subscriber</span><i class="icon-Add-Subscriber-Responsive visible-xs"></i>
</a></li>
<li><a href="<?php echo site_url('contact-list')?>" title="Subscriber Management">
<span class="hidden-xs">Subscriber Management</span><i class="icon-Subscriber-Management-Responsive visible-xs"></i>
</a></li>
<li><a href="<?php echo site_url('suppression-list')?>" title="Suppression List">
<span class="hidden-xs">Suppression List</span><i class="icon-Suppression-List-Responsive visible-xs"></i>
</a></li>
</ul>
</div>
<?php if($add_type=='') { ?>
<div class="layout-box clearfix">
<a href="<?php echo site_url('add-contact?add_type=import')?>" class="upload-area">
<div class="col-md-4 col-sm-4 right-border xstext-center mt5 xsmt5 mb4">
<i class="icon-upload-from-pc em45 smem35 hidden-xs"></i>
<p class="em10 smem10 xsem10 mt8 xsmt8 hidden-xs">
Upload subscribers directly from your PC</p>
<h5 class="w600 hidden-xs">Upload From PC</h5>
<h5 class="w600 visible-xs mobile-btn">Upload From Device</h5>
</div></a>

<a href="<?php echo site_url('add-contact?add_type=single')?>" class="upload-area xstext-center">
<div class="col-md-4 col-sm-4 mt5 xsmt5 mb4">
<i class="icon-Add-One-At-A-Time em45 smem35 hidden-xs"></i>
<p class="em10 xsem10 mt8 xsmt8 hidden-xs">
Add every subscriber detail one at a time</p>
<h5 class="w600 mt4 xsmt4 hidden-xs">One At A Time</h5>
<h5 class="w600 visible-xs mobile-btn">One At A Time</h5>
</div></a>

<a href="<?php echo site_url('add-contact?add_type=copy')?>" class="upload-area xstext-center">
<div class="col-md-4 col-sm-4 left-border mt5 xsmt5 mb4">
<i class="icon-Copy-n-Paste em45 smem35 hidden-xs"></i>
<p class="em10 xsem10 mt8 xsmt8 hidden-xs">Add more than 1 subscriber here with ease</p>
<h5 class="w600 mt4 xsmt4 hidden-xs">Copy & Paste</h5>
<h5 class="w600 visible-xs mobile-btn">Copy & Paste</h5>
</div></a>
</div>
<?php } ?> 

<?php if($add_type=='import' && $importData=='' && $importArray=='') { ?>
<script src="<?php echo $this->config->item('templateAssetsPath')?>js/drop_uploader.js"></script>
<div class="col-md-12 col-sm-12 col-xs-12 mb2 xsmb4 padding0">
<form action="" method="post" enctype="multipart/form-data">
<div class="layout-box clearfix">
<div class="bs-wizard">
<div class="col-xs-3 bs-wizard-step complete">
<div class="progress"><div class="progress-bar"></div></div>
<a href="javascript:void(0)" class="bs-wizard-dot"></a>
<div class="text-center em8 smem9 xsem8 hidden-xs">setting</div>
</div>
                
<div class="col-xs-3 bs-wizard-step active disabled">
<div class="progress"><div class="progress-bar"></div></div>
<a href="javascript:void(0)" class="bs-wizard-dot"></a>
<div class="text-center em8 smem9 xsem8 hidden-xs">Mapping</div>
</div>
           
<div class="col-xs-3 bs-wizard-step disabled">
<div class="progress"><div class="progress-bar"></div></div>
<a href="javascript:void(0)" class="bs-wizard-dot"></a>
<div class="text-center em8 smem9 xsem8 hidden-xs">Choose Campaign</div>
</div>
</div>

<div class="mt5 xsmt16 text-center mobile-margin" id="other_show">
<input type="hidden" name="test" value="test" />
<input type="file" class="singleFile" name="csv_file" data-layout="list" >
<!-- <input class="file-btn" type="submit" value="Submit">-->
<p class="xstext-center xstext-right em9 smem9 xsem8 xsmt2">Accepted File : CSV | XLS </p>
</div>
<!----------------------------safari and internet explorer dropbox start----------------------------------------------------->
<script type="text/javascript">
$(document).ready(function(){
	var isSafari = Object.prototype.toString.call(window.HTMLElement).indexOf('Constructor') > 0;
	var isIE = /*@cc_on!@*/false || !!document.documentMode;
	if(isSafari==true){
		$('#safari_show').css("display","block");
		$('#other_show').remove();
	}
	else if(isIE==true){
		$('#safari_show').css("display","block");
		$('#other_show').remove();
	}
	 else {
		$('#other_show').css("display","block");
		$('#safari_show').remove();
	}
});
</script>
<div class="col-xs-12 padding0" id="safari_show">
<div class="safaridrop-box mt5 xsmt16 text-center mobile-margin">
<i class="icon-Drag-n-Drop smart-grey em50 xsem50 smem50 center-block mt3"></i>
<input type="file" name="csv_file" id="file-6" class="inputfile" />
<label for="file-6" class="w300"><span class="custom-file-upload" >Choose File</span>
<p class="w300 mt4 xsmt5 em9 xsem9 smem9 file-layout" style="background-color:#ffffff;"></p>
</label>
</div>
<p class="xstext-center xstext-right em9 smem9 xsem8 xsmt2">Accepted File : CSV | XLS | XLSX</p>
</div>
<!----------------------------safari and internet explorer dropbox end----------------------------------------------------->
</div>

<div class="row">
<div class="col-md-7 col-sm-6 col-xs-12 mt2 xsmt2 xsem9 xsmb2 mb0">
<input type="checkbox" name="permission" value="yes" > &nbsp; I Have Permission To Add This Contact To Campaign</div>
<div class="col-md-5 col-sm-6 col-xs-12 xstext-right xstext-center mt2 xsmt2">
<a href="<?php echo site_url('add-contact')?>" class="blank-btn" title="Back"> Back</a>  &nbsp;
<button type="submit" class="smart-btn" title="Next" name="submit_1" value="yes"> Next </button> </div>
</div>

</form>
</div>
<?php } ?>

<?php if($add_type=='import' && $importData!='') { ?>
<form action="" method="post" >
<div class="col-md-12 col-sm-12 col-xs-12 mb2 xsmb4 padding0">
<div class="layout-box1 clearfix">
<div class="bs-wizard">
<div class="col-xs-3 bs-wizard-step complete">
<div class="progress"><div class="progress-bar"></div></div>
<a href="javascript:void(0)" class="bs-wizard-dot"></a>
<div class="text-center em8 smem9 xsem8 hidden-xs">setting</div>
</div>
                
<div class="col-xs-3 bs-wizard-step active">
<div class="progress"><div class="progress-bar"></div></div>
<a href="javascript:void(0)" class="bs-wizard-dot"></a>
<div class="text-center em8 smem9 xsem8 hidden-xs">Mapping</div>
</div>
           
<div class="col-xs-3 bs-wizard-step disabled">
<div class="progress"><div class="progress-bar"></div></div>
<a href="javascript:void(0)" class="bs-wizard-dot"></a>
<div class="text-center em8 smem9 xsem8 hidden-xs">Choose Campaign</div>
</div>
</div>
<div class="col-md-12 mt1 xsmt15 padding0">
<div class="mt0 mb2 xsmb3 box-padding w600 em11 smem11 xsem11">Field Mapping</div>
<div class="table-responsive table-multi-data data-scroll">

<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
  <?php $i=0;
   foreach($importData[0] as $val){  ?>
    <th class="text-left"><select name="field[]" class="fields tag">
    <option value="no" selected="selected">Do not import</option>
    <option value="email" <?php if($field[$i]=='email'){?> selected="selected"<?php } ?>>Email</option>
    <option value="name" <?php if($field[$i]=='name'){?> selected="selected"<?php } ?>>Name</option>
    <option value="address" <?php if($field[$i]=='address'){?> selected="selected"<?php } ?>>Address</option>
    <option value="city" <?php if($field[$i]=='city'){?> selected="selected"<?php } ?>>City</option>
    <option value="state" <?php if($field[$i]=='state'){?> selected="selected"<?php } ?>>State</option>
    <option value="zip" <?php if($field[$i]=='zip'){?> selected="selected"<?php } ?>>Zip</option>
    <option value="country" <?php if($field[$i]=='country'){?> selected="selected"<?php } ?>>Country</option>
    <option value="phone" <?php if($field[$i]=='phone'){?> selected="selected"<?php } ?>>Phone</option>
    <option value="custom_1" <?php if($field[$i]=='custom_1'){?> selected="selected"<?php } ?>>Custom Field 1</option>
    <option value="custom_2" <?php if($field[$i]=='custom_2'){?> selected="selected"<?php } ?>>Custom Field 2</option>
    <option value="custom_3" <?php if($field[$i]=='custom_3'){?> selected="selected"<?php } ?>>Custom Field 3</option>
    </select></th>
	<?php $i++; } ?>
     </tr>
<?php 
 $field_val = array();
  foreach($importData as $key=>$valI) { ?>
  <tr class="text-center">
  <?php foreach($valI as $valvalI) { ?>
    <td class="text-left"><?php echo $valvalI?>
	<?php $field_val[$key][] = $valvalI; ?>
	</td>
<?php } ?>
  </tr>
  <?php } ?>
 </table>
  <input type="hidden" name="file_path" value="<?php echo $file_path;?>">
</div></div>
</div>
<div class="col-xs-12 xstext-right xstext-center mt2 xsmt8 padding0">
<a href="javascript:void(0)" onclick="window.history.back();" class="blank-btn" title="Back">Back</a>  &nbsp;
<button type="submit" class="smart-btn" title="Next" name="submit_2" value="yes"> Next </button> </div>
</div>
</form>
<script>
$(document).ready(function(){
   $('.fields').on('change', function(event ) {
       //restore previously selected value
       var prevValue = $(this).data('previous');
       $('.fields').not(this).find('option[value="'+prevValue+'"]').show();    

       //hide option selected now
       var value = $(this).val();
       //update previously selected data
       $(this).data('previous',value);
	   if(value!='no')
       $('.fields').not(this).find('option[value="'+value+'"]').hide();
   });

});
</script>
<?php } ?> 

<?php if($add_type=='import' && $importArray!='') { ?>
<form action="" method="post" >
<div class="col-md-12 col-sm-12 col-xs-12 mb2 xsmb4 padding0">
<div class="layout-box1 clearfix">
<div class="bs-wizard">
<div class="col-xs-3 bs-wizard-step complete">
<div class="progress"><div class="progress-bar"></div></div>
<a href="javascript:void(0)" class="bs-wizard-dot"></a>
<div class="text-center em8 smem9 xsem8 hidden-xs">setting</div>
</div>
                
<div class="col-xs-3 bs-wizard-step complete">
<div class="progress"><div class="progress-bar"></div></div>
<a href="javascript:void(0)" class="bs-wizard-dot"></a>
<div class="text-center em8 smem9 xsem8 hidden-xs">Mapping</div>
</div>
           
<div class="col-xs-3 bs-wizard-step complete">
<div class="progress"><div class="progress-bar"></div></div>
<a href="javascript:void(0)" class="bs-wizard-dot"></a>
<div class="text-center em8 smem9 xsem8 hidden-xs">Choose Campaign</div>
</div>
</div>
<div class="col-md-12 mt1 xsmt15 padding0">
<div class="mt0 mb2 xsmb3 box-padding w600 em11 smem11 xsem11">Choose Campaign</div>
<div class="table-responsive table-data data-scroll">

<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr class="w500">
    <th width="50px" class="text-center"><input name="" type="checkbox" value="" id="select_all"/></td>
    <th>Campaigns</td>
    <th class="text-center">Contacts</td>
     </tr>
<?php foreach($campaign_list as $valcamp) { ?>
  <tr class="text-center">
    <td><input name="select_checkbox[]" type="checkbox" value="<?php echo $valcamp->campaign_id?>" class="select_checkbox" /></td>
    <td class="text-left"><?php echo $valcamp->campaign_name?></td>
    <td><?php echo $this->common_model->getCountAllFromAnyTable('campaign_id',$valcamp->campaign_id,'tbl_contact'); ?></td>
  </tr>
 <?php } ?>
	<?php
	foreach($keyArray as $k)
	{
		?>
			<input type="hidden" name="field[]" value="<?php echo $k;?>"> 
		<?php
	}
	?>
</table>
 <input type="hidden" name="file_path" value="<?php echo $file_path;?>">
 
</div></div>
</div>


<div class="col-xs-12 xstext-right xstext-center mt2 xsmt8 padding0">
<a href="javascript:void(0)" onclick="window.history.back();" class="blank-btn" title="Back">Back</a>  &nbsp;
<button class="smart-btn submitCamp" title="Import" type="submit" name="submit_3" value="yes">Import </button></div>
</div>
</form>
<?php } ?> 

<?php if($add_type=='copy' && $emailList=='') { ?>
<form action="" method="post" >
<div class="col-md-12 col-sm-12 col-xs-12 mb2 xsmb4 padding0">
<div class="layout-box clearfix">
<div class="bs-wizard">
<div class="col-xs-3 bs-wizard-step complete ">
<div class="progress"><div class="progress-bar"></div></div>
<a href="javascript:void(0)" class="bs-wizard-dot"></a>
<div class="text-center em8 smem9 xsem8 hidden-xs">setting</div>
</div>
                
<div class="col-xs-3 bs-wizard-step active disabled">
<div class="progress"><div class="progress-bar"></div></div>
<a href="javascript:void(0)" class="bs-wizard-dot"></a>
<div class="text-center em8 smem9 xsem8 hidden-xs">Choose Campaign</div>
</div>
</div>
<!-- content start-->
<div class="col-md-12 mt2 mobile-margin padding0 smart-field">
<div class="mt0 mb2 mdem12 smem11 xsem10 w500">Copy And Paste</div>

<div class="col-xs-12 xsmb4 mb0 padding0">
<div class="row">
<div class="col-md-2 col-sm-3 col-xs-12 mt1 xsmt1">Paste Here Your Contact</div>
<div class="col-md-4 col-sm-5 col-xs-12 mt1 xsmt1">
<!--<textarea name="emails" cols="" rows="7" ><?php echo $emails?></textarea>-->
<input id="tags_1" type="text" name="emails" class="tags" value="<?php echo $emails?>" />
<p class="em9 smem9 xsem9 mt1 xsmt1 visible-md visible-lg">Note: Enter Email by comma separated.</p>
<p class="em9 smem9 xsem9 mt1 xsmt1 visible-xs visible-sm">Note: Enter Email Separated by Enter key.</p>
</div>
</div>
</div>
</div>
<!-- content end-->
</div>
</div>
<div class="row">
<div class="col-md-7 col-sm-6 col-xs-12 xsem9 xsmb2 mb2">
<input type="checkbox" name="permission" value="yes" > &nbsp; I Have Permission To Add This Contact To Campaign</div>
<div class="col-md-5 col-sm-6 col-xs-12 xstext-right xstext-center xsmb2 mb2 ">
<a href="<?php echo site_url('add-contact')?>"  class="blank-btn" title="Back"> Back</a>  &nbsp;
<button type="submit" name="submit_4" value="yes" class="smart-btn" title="Next"> Next </button> </div>
</div>
</form>
<?php } ?> 

<?php if($add_type=='copy' && $emailList!='') { ?>
<form action="" method="post" >
<div class="col-md-12 col-sm-12 col-xs-12 mb2 xsmb4 padding0">
<div class="layout-box1 clearfix">

<div class="bs-wizard">
<div class="col-xs-3 bs-wizard-step complete">
<div class="progress"><div class="progress-bar"></div></div>
<a href="javascript:void(0)" class="bs-wizard-dot"></a>
<div class="text-center em8 smem9 xsem8 hidden-xs">setting</div>
</div>
                
<div class="col-xs-3 bs-wizard-step active">
<div class="progress"><div class="progress-bar"></div></div>
<a href="javascript:void(0)" class="bs-wizard-dot"></a>
<div class="text-center em8 smem9 xsem8 hidden-xs">Choose Campaign</div>
</div>
</div>

<div class="col-md-12 mt1 xsmt15 padding0">
<div class="mt0 mb2 xsmb3 box-padding w600 em11 smem11 xsem11">Choose Campaign</div>
<div class="table-responsive table-data data-scroll">

<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr class="w500">
    <th class="text-center"><input name="" type="checkbox" value="" id="select_all" /></td>
    <th>Campaigns</td>
    <th class="text-center">Contacts</td>
     </tr>
<?php foreach($campaign_list as $valcamp) { ?>
  <tr class="text-center">
    <td><input name="select_checkbox[]" type="checkbox" value="<?php echo $valcamp->campaign_id?>" class="select_checkbox" /></td>
    <td class="text-left"><?php echo $valcamp->campaign_name?></td>
    <td><?php echo $this->common_model->getCountAllFromAnyTable('campaign_id',$valcamp->campaign_id,'tbl_contact'); ?></td>
  </tr>
 <?php } ?></table>
 <input type="hidden" name="emailList" value="<?php echo $emailList?>" />
</div></div>
</div>


<div class="col-xs-12 xstext-right xstext-center mt2 xsmt8 padding0">
<a href="javascript:void(0)" onclick="window.history.back();" class="blank-btn" title="Back">&nbsp;&nbsp;Back &nbsp;&nbsp;</a>  &nbsp;
<button type="submit" name="submit_5" value="yes" class="smart-btn" title="Add Contact">Add Contact</button></div>
</div>
</form>
<?php } ?> 

<?php if($add_type=='single' && $postArray=='') { ?>
<form action="" method="post" >
<div class="col-md-12 col-sm-12 col-xs-12 mb2 xsmb4 padding0">
<div class="layout-box clearfix">
<div class="bs-wizard">
<div class="col-xs-3 bs-wizard-step complete ">
<div class="progress"><div class="progress-bar"></div></div>
<a href="javascript:void(0)" class="bs-wizard-dot"></a>
<div class="text-center em8 smem9 xsem8 hidden-xs">setting</div>
</div>
                
<div class="col-xs-3 bs-wizard-step active disabled">
<div class="progress"><div class="progress-bar"></div></div>
<a href="javascript:void(0)" class="bs-wizard-dot"></a>
<div class="text-center em8 smem9 xsem8 hidden-xs">Choose Campaign</div>
</div>
</div>
<!-- content start-->
<div class="col-md-12 mt2 mobile-margin padding0 smart-field">
<h4 class="mt0 mb2 xsmt0">Add One At A Time</h4>

<div class="col-xs-12 xsmb4 mb0 padding0">
<div class="row">
<div class="col-md-2 col-sm-3 col-xs-12 mt2 xsmt2">Email Address</div>
<div class="col-md-4 col-sm-5 col-xs-12 mt2 xsmt2">
<input name="email" type="text" placeholder="Email Address" value="<?php echo $email?>"></div></div>
</div>

<div class="col-xs-12 xsmb4 mb0 padding0">
<div class="row">
<div class="col-md-2 col-sm-3 col-xs-12 mt2 xsmt2">Name</div>
<div class="col-md-4 col-sm-5 col-xs-12 mt2 xsmt2">
<input name="name" type="text" placeholder="Name" value="<?php echo $name?>"></div>
</div>	
</div>	

<?php if(sizeof($customFieldArray)==0) { ?>
<div class="col-xs-12 padding0 fieldDiv">
<div class="row">
<div class="col-md-2 col-sm-3 col-xs-6 mt2 xsmt2"><select name="fieldname[]" class="fieldSelect">
    <option value="">Select Field</option>
	<option value="address">Address</option>
    <option value="city">City</option>
    <option value="state">State</option>
    <option value="zip">Zip</option>
    <option value="country">Country</option>
    <option value="phone">Phone</option>
    <option value="custom_1">Custom Field 1</option>
    <option value="custom_2">Custom Field 2</option>
    <option value="custom_3">Custom Field 3</option>
    </select></div>
<div class="col-md-4 col-sm-5 col-xs-6 mt2 xsmt2">
<input name="fieldvalue[]" type="text" ></div>

<div class="col-md-1 col-sm-2 col-xs-2 xstext-right1 xstext-center1 xsmt7 mtt2  cloum-width1 removeButDiv" style="display:none">
<a href="javascript:" class="cross-icon removeField"><i class="fa fa-remove" aria-hidden="true"></i></a></div>
<div class="col-md-1 col-sm-2 col-xs-2 xstext-right1 xstext-center1 xsmt7 mtt2  cloum-width1 addButDiv">
<a href="javascript:" class="cross-icon1 addField"><i class="fa fa-plus" aria-hidden="true"></i></a></div>
</div>
</div>
<?php } else { 
$keyArray = array();
foreach($customFieldArray as $keycK=>$valueCK)
$keyArray[] = $keycK;
$i=0;
  foreach($customFieldArray as $keyc=>$valueC) {  ?>

<div class="col-xs-12 padding0 fieldDiv">
<div class="row">
<div class="col-md-2 col-sm-3 col-xs-6 mt2 xsmt2">
    <select name="fieldname[]" class="fieldSelect">
    <option value="">Select Field</option>
	<option value="address" <?php if(in_array('address',$keyArray)) { ?> style="display:none" <?php } ?> <?php if($keyc=='address') { ?> selected="selected" <?php } ?>>Address</option>
    <option value="city" <?php if(in_array('city',$keyArray)) { ?> style="display:none" <?php } ?>  <?php if($keyc=='city') { ?> selected="selected" <?php } ?>>City</option>
    <option value="state" <?php if(in_array('state',$keyArray)) { ?> style="display:none" <?php } ?> <?php if($keyc=='state') { ?> selected="selected" <?php } ?>>State</option>
    <option value="zip" <?php if(in_array('zip',$keyArray)) { ?> style="display:none" <?php } ?>  <?php if($keyc=='zip') { ?> selected="selected" <?php } ?>>Zip</option>
    <option value="country" <?php if(in_array('country',$keyArray)) { ?> style="display:none" <?php } ?> <?php if($keyc=='country') { ?> selected="selected" <?php } ?>>Country</option>
    <option value="phone" <?php if(in_array('phone',$keyArray)) { ?> style="display:none" <?php } ?>  <?php if($keyc=='phone') { ?> selected="selected" <?php } ?>>Phone</option>
    <option value="custom_1" <?php if(in_array('custom_1',$keyArray)) { ?> style="display:none" <?php } ?> <?php if($keyc=='custom_1') { ?> selected="selected" <?php } ?>>Custom Field 1</option>
    <option value="custom_2" <?php if(in_array('custom_2',$keyArray)) { ?> style="display:none" <?php } ?> <?php if($keyc=='custom_2') { ?> selected="selected" <?php } ?>>Custom Field 2</option>
    <option value="custom_3" <?php if(in_array('custom_3',$keyArray)) { ?> style="display:none" <?php } ?> <?php if($keyc=='custom_3') { ?> selected="selected" <?php } ?>>Custom Field 3</option>
    </select></div>
<div class="col-md-4 col-sm-5 col-xs-4 mt2 xsmt2">
<input name="fieldvalue[]" type="text" value="<?php echo $valueC?>"></div>

<div class="col-md-1 col-sm-2 col-xs-2 text-center xsmt7 mtt2  cloum-width removeButDiv" style="display:<?php if($i>0){ echo 'block'; } else { echo 'none'; } ?>">
<a href="javascript:" class="cross-icon removeField"><i class="fa fa-remove" aria-hidden="true"></i></a></div>
<?php if($i==0){ ?>
<div class="col-md-1 col-sm-2 col-xs-2 text-center xsmt7 mtt2  cloum-width addButDiv">
<a href="javascript:" class="cross-icon smart-btn addField"  <?php if(sizeof($customFieldArray)>=9){ ?> style="display:none" <?php } ?>><i class="fa fa-plus" aria-hidden="true"></i></a></div>
<?php } ?>
</div>
</div>

<?php $i++; } } ?>


</div>
<!-- content end-->
</div>
</div>
<div class="row">
<div class="col-md-7 col-sm-6 col-xs-12 xsem9 xsmb2 mb2">
<input type="checkbox" name="permission" value="yes"> &nbsp; I Have Permission To Add This Contact To Campaign</div>
<div class="col-md-5 col-sm-6 col-xs-12 xstext-right xstext-center xsmb2 mb2 ">
<a href="<?php echo site_url('add-contact')?>" class="blank-btn" title="Back"> Back</a>  &nbsp;
<button type="submit" name="submit_6" value="yes" class="smart-btn" title="Next"> Next </button> </div>
</div>
</form>
<script>
$('body').delegate(".removeField", 'click', function() {
  var selectedfield = $(this).parent().parent().find('.fieldSelect').val();
      if($('.fieldDiv').length>1)
     $(this).parent().parent().parent().remove();
	  if($('.fieldDiv').length<9)
	 $('.addField').show();
	  $('.fieldSelect').find('option[value="'+selectedfield+'"]').show();
 });
$('body').delegate(".addField", 'click', function() {
      var clonedata = $(".fieldDiv:first").clone().find("input:text").val("").end();
	  clonedata.find('.addButDiv').remove();
	  clonedata.find('.removeButDiv').show();
	  $('.fieldDiv:last').after(clonedata);
	  setSelectBox();
	  if($('.fieldDiv').length>=9)
	  { $(this).hide(); }
 });
$(document ).on( "change",".fieldSelect", function() {
	  setSelectBox();
 });
function setSelectBox(){

 $('.fieldSelect').each(function(){
   //restore previously selected value
   var prevValue = $(this).data('previous');
   $('.fieldSelect').not(this).find('option[value="'+prevValue+'"]').show(); 
  
   //hide option selected now
   var value = $(this).val();
   //update previously selected data
   $(this).data('previous',value);
   $('.fieldSelect').not(this).find('option[value="'+value+'"]').hide();
  });
}
</script>
<?php } ?> 

<?php if($add_type=='single' && $postArray!='') { ?>
<form action="" method="post" >
<div class="col-md-12 col-sm-12 col-xs-12 mb2 xsmb4 padding0">
<div class="layout-box1 clearfix">
<div class="bs-wizard">
<div class="col-xs-3 bs-wizard-step complete">
<div class="progress"><div class="progress-bar"></div></div>
<a href="javascript:" class="bs-wizard-dot"></a>
<div class="text-center em8 smem9 xsem8 hidden-xs">setting</div>
</div>
                
<div class="col-xs-3 bs-wizard-step active">
<div class="progress"><div class="progress-bar"></div></div>
<a href="javascript:" class="bs-wizard-dot"></a>
<div class="text-center em8 smem9 xsem8 hidden-xs">Choose Campaign</div>
</div>
</div>
<div class="col-md-12 mt1 xsmt15 padding0">
<div class="mt0 mb2 xsmb3 box-padding w600 em11 smem11 xsem11">Choose Campaign</div>
<div class="table-responsive table-data data-scroll">

<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr class="w500">
    <th width="50px"  class="text-center"><input name="" type="checkbox" value="" id="select_all" /></td>
    <th>Campaigns</td>
    <th class="text-center">Contacts</td>
     </tr>
 <?php foreach($campaign_list as $valcamp) { ?>
  <tr class="text-center">
    <td><input name="select_checkbox[]" type="checkbox" value="<?php echo $valcamp->campaign_id?>" class="select_checkbox" /></td>
    <td class="text-left"><?php echo $valcamp->campaign_name?></td>
    <td><?php echo $this->common_model->getCountAllFromAnyTable('campaign_id',$valcamp->campaign_id,'tbl_contact'); ?></td>
  </tr>
 <?php } ?>
  
</table>
 <input type="hidden" name="postArray" value='<?php echo $postArray?>' />
</div></div>
</div>


<div class="col-xs-12 xstext-right xstext-center mt2 xsmt8 padding0">
<a href="javascript:void(0)" onclick="window.history.back();" class="blank-btn" title="Back">&nbsp;&nbsp;Back &nbsp;&nbsp;</a>  &nbsp;
<button type="submit" name="submit_7" value="yes" class="smart-btn" title="Add Contact">Add Contact</button></div>
</div>
</form>
<?php } ?>
</div>
</div>
<script>

/*-------------------------- check/uncheck multiple campaign starts here ------------------------------------*/
$('body').delegate("#select_all", 'change', function() {
      $(".select_checkbox").prop('checked', $(this).prop("checked"));
	  $(".select_checkbox").change();
      });
$('body').delegate(".select_checkbox", 'change', function() {
	  var length = $('.select_checkbox:checked').length
	 if (length != $('.select_checkbox').length) {
       $("#select_all").prop('checked', false);
    }else{
        $("#select_all").prop('checked', true);
    }
      });
/*-------------------------- check/uncheck multiple campaign ends here ------------------------------------*/

/*-------------------------- success message starts here---------------------------------------------------*/
<?php if($this->session->userdata('success_msg')) { ?>
new PNotify({
	  title: 'Success',
	  text: '<?php echo $this->session->userdata('success_msg')?>',
	  type: 'success'
	});
<?php $this->session->unset_userdata('success_msg'); } ?>
/*-------------------------- success message ends here---------------------------------------------------*/

/*-------------------------- error message starts here---------------------------------------------------*/
<?php if(validation_errors()!='' || $error!='') { ?>
new PNotify({
	  title: 'Error',
	  text: <?php echo (validation_errors()?json_encode(validation_errors()):json_encode($error))?>,
	  type: 'error'
	});
<?php } ?>

<?php if($this->session->userdata('error_msg')) { ?>
new PNotify({
	  title: 'Error',
	  text: '<?php echo $this->session->userdata('error_msg')?>',
	  type: 'error'
	});
<?php $this->session->unset_userdata('error_msg'); } ?>
/*-------------------------- error message ends here---------------------------------------------------*/
</script>

<!----------------------file upload start js---------------------->
<script>
/*
	By Osvaldas Valutis, www.osvaldas.info
	Available for use under the MIT License
*/
(function(e,t,n){var r=e.querySelectorAll("html")[0];r.className=r.className.replace(/(^|\s)no-js(\s|$)/,"$1js$2")})(document,window,0);
        
'use strict';

;( function ( document, window, index )
{
	var inputs = document.querySelectorAll( '.inputfile' );
	Array.prototype.forEach.call( inputs, function( input )
	{
		var label	 = input.nextElementSibling,
			labelVal = label.innerHTML;

		input.addEventListener( 'change', function( e )
		{var size1=this.files[0].size;
		
		var sizeInMB = (size1 / (1024*1024)).toFixed(2);

			var fileName = '';
			if( this.files && this.files.length > 1 )
				fileName = ( this.getAttribute( 'data-multiple-caption' ) || '' ).replace( '{count}', this.files.length );
			else
				fileName = e.target.value.split( '\\' ).pop();

			if( fileName )
				//label.querySelector( 'p' ).innerHTML = fileName+' '+sizeInMB+' MB';
				label.querySelector( 'p' ).innerHTML = fileName;

				
			else
				label.innerHTML = labelVal;
		});

		// Firefox bug fix
		input.addEventListener( 'focus', function(){ input.classList.add( 'has-focus' ); });
		input.addEventListener( 'blur', function(){ input.classList.remove( 'has-focus' ); });
	});
}( document, window, 0 ));
</script>

<!----------------------file upload end js ---------------------->
