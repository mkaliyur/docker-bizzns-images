&nbsp;
<?php foreach($allRecord as $value) { ?>
<tr class="text-center">
            <td class="table-header-checkbox1"><input name="select_checkbox" type="checkbox" value="<?php echo $value->id?>" class="select_checkbox" /></td>
            <td class="text-left more-word1"><?php echo $value->email?></td>
            <td class="text-center more-word1"><?php echo $value->name?></td>
            <td class="text-center more-word1"><?php echo $this->common_model->getSingleFieldFromAnyTable('campaign_name','campaign_id',$value->campaign_id,'tbl_campaign')?></td>
            <td class="text-center "><?php echo date('Y-m-d h:i:s',$value->add_time)?></td>
            <td class="table-icon"><ul>
                <li><a href="<?php echo site_url('edit-contact/'.$value->id)?>" title="Edit"><i class="fa fa-pencil-square-o em12" aria-hidden="true"></i></a></li>
                <li><a data-toggle="modal" data-target="#delete" class="deletelink" data-href="<?php echo site_url('delete-contact/'.$value->id)?>" href="javascript:" title="Delete"><i class="fa fa-trash-o em12" aria-hidden="true"></i></a></li>
              </ul></td>
          </tr>
          
<?php } ?>
<script>
      $(".select_checkbox").change(function(){ 
    if ($('.select_checkbox:checked').length != $('.select_checkbox').length) {
       $("#select_all").prop('checked', false);
  }else{
        $("#select_all").prop('checked', true);
    }
});
</script>