<div class="right_col" role="main">
        <div class="">
          <div class="clearfix"></div>
          <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
              <div class="x_panel">
                <div class="x_title">
                  <h2><?php echo $page_title?></h2>
                  <ul class="nav navbar-right panel_toolbox">
                    <li><a data-toggle="tooltip" data-placement="top" title="contact List" href="<?php echo site_url('contact-list')?>"><i class="fa fa-backward"></i></a>
                    </li>
                  </ul>
                  <div class="clearfix"></div>
                </div>
				<?php if(!$importData && !$importArray) { ?>
                <div class="x_content">
				<?php if(validation_errors() || $error) { ?>
				<div class="alert alert-danger alert-dismissible fade in" role="alert">
                  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">x</span>
                  </button><?php echo validation_errors()?><?php echo $error?></div>
				<?php } ?>
                  <br />
                  <form id="demo-form2" data-parsley-validate class="form-horizontal form-label-left" action="" method="post" enctype="multipart/form-data">

					<div class="form-group">
                      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Import File<span class="required">*</span>
                      </label>
                      <div class="col-md-6 col-sm-6 col-xs-12">
					  <input type="hidden" name="test" value="test" />
                        <input type="file" id="csv_file" required="required" class="form-control col-md-7 col-xs-12" name="csv_file" value="">
                      </div>
                    </div>
					
                    <div class="form-group">
                      <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                        <button type="submit" class="btn btn-success" name="submit_1" value="import">Upload</button>
                      </div>
                    </div>

                  </form>
                </div>
				<?php } ?>
				 
				<?php if($importData) { ?>
				
				<div class="x_content">
				<form id="demo-form2" data-parsley-validate class="form-horizontal form-label-left" action="" method="post">
                <ul class="list-unstyled timeline">
				<?php foreach($importData as $val) { ?>
                  <li>
                    <div class="block" style="margin-left:160px">
                      <div class="tags">
                         <select class="form-control fields tag" tabindex="-1" name="field[]" style="width:140px; border:none">
						   <option value="no" selected="selected">Do not import</option>
						   <option value="name">Name</option>
						   <option value="email">Email</option>
                        </select>
                      </div>
                      <div class="block_content">
                        <h2 class="title" style="text-align:justify"><?php echo $val?>
							<input type="hidden" name="field_val[]" value="<?php echo $val?>" />
                                    </h2>
                      </div>
                    </div>
                  </li>
                <?php } ?>
                </ul>
				<div class="form-group">
                      <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                        <button type="submit" class="btn btn-success" name="submit_2" value="Next">Next</button>
                      </div>
                    </div>
				</form>

              </div>
			  
			   <?php } ?>
			   
			   <?php if($importArray) { ?>
                <div class="x_content">
				<?php if(validation_errors()) { ?>
				<div class="alert alert-danger alert-dismissible fade in" role="alert">
                  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">x</span>
                  </button><?php echo validation_errors()?></div>
				<?php } ?>
                  <br />
                  <form id="demo-form2" data-parsley-validate class="form-horizontal form-label-left" action="" method="post" >

					<div class="form-group">
                      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="campaign_name">Campaign Name:<span class="required">*</span>
                      </label>
                      <div class="col-md-6 col-sm-6 col-xs-12">
                        <select class="select2_single form-control" tabindex="-1" name="campaign_id">
						<?php foreach($campaign_list as $valCamp) { ?>
                          <option value="<?php echo $valCamp->campaign_id?>" <?php if($campaign_id==$valCamp->campaign_id) { ?> selected="selected" <?php } ?>><?php echo $valCamp->campaign_name?></option>
						<?php } ?>
                        </select>
						<?php foreach($importArray as $key=>$val){ ?>
						<input type="hidden" name="<?php echo $key?>" value="<?php echo $val?>" />
						<?php } ?>
                      </div>
                    </div>
					
                    <div class="form-group">
                      <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                        <button type="submit" class="btn btn-success" name="submit_3" value="Finish">Finish</button>
                      </div>
                    </div>

                  </form>
                </div>
				<?php } ?>
              </div>
            </div>
          </div>
        </div>
        <!-- /page content -->
<script>
$(document).ready(function(){
   $('.fields').on('change', function(event ) {
       //restore previously selected value
       var prevValue = $(this).data('previous');
       $('.fields').not(this).find('option[value="'+prevValue+'"]').show();    

       //hide option selected now
       var value = $(this).val();
       //update previously selected data
       $(this).data('previous',value);
	   if(value!='no')
       $('.fields').not(this).find('option[value="'+value+'"]').hide();
   });

});

</script>
	<!-- form validation -->
  <script type="text/javascript" src="<?php echo $this->config->item('templateAssetsPath')?>js/parsley/parsley.min.js"></script>

		  <!-- form validation -->
  <script type="text/javascript">

    $(document).ready(function() {
      $.listen('parsley:field:validate', function() {
        validateFront();
      });
      $('#demo-form2 .btn').on('click', function() {
        $('#demo-form2').parsley().validate();
        validateFront();
      });
      var validateFront = function() {
        if (true === $('#demo-form2').parsley().isValid()) {
          $('.bs-callout-info').removeClass('hidden');
          $('.bs-callout-warning').addClass('hidden');
        } else {
          $('.bs-callout-info').addClass('hidden');
          $('.bs-callout-warning').removeClass('hidden');
        }
      };
    });
    try {
      hljs.initHighlightingOnLoad();
    } catch (err) {}

$('body').delegate(".managerLi", 'click', function() {
    $(this).parent().find('.submanagersUl').toggle('slow');
});
$('body').delegate(".icheckbox_flat-green", 'click', function() {
    $(this).parent().find('.permissionUl').toggle('slow');
});
$('.submanagerCheckbox').on('ifChecked', function(event){
  $(this).parent().parent().find('.permissionUl').show('slow');
});
$('.submanagerCheckbox').on('ifUnchecked', function(event){
  $(this).parent().parent().find('.permissionUl').hide('slow');
  $(this).parent().parent().find('input').iCheck('uncheck');
});
  </script>
  <!-- /form validation -->
   <!-- select2 -->
  <link href="<?php echo $this->config->item('templateAssetsPath')?>css/select/select2.min.css" rel="stylesheet">
  <!-- select2 -->
  <script src="<?php echo $this->config->item('templateAssetsPath')?>js/select/select2.full.js"></script>
   <script>
    $(document).ready(function() {
      $(".select2_single").select2({});
    });
  </script>
