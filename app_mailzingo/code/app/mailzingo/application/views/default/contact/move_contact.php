<div class="modal-dialog campaign-modal">
    <div class="modal-content">
      <div class="modal-body padding0 clearfix xsmb4 xsmt4 mb4">
        <div class="col-md-5 col-sm-5 col-xs-12 mt7 xsmt4 mb4 right-border">
       <div class="col-xs-12 bottom-border">
       <div class="image-out-border"><div class="image-in-border">
       <img src="<?php echo $this->config->item('templateAssetsPath')?>images/<?php echo $type?>-icon.png" class="img-responsive center-block mobile-img" /></div></div></div>
       </div>
       <div class="col-md-7 col-sm-7 col-xs-12 campaign-field">
        <div class="col-xs-12">
       <h4><?php echo ucfirst($page_title)?> To Desired Campaigns</h4>
	    <form data-parsley-validate class="copyMoveForm" action="<?php echo site_url('copy-move-contact')?>" method="post">
        <div class="form-group mt10 xsmt10">
		<label class="w300">Select Campaign</label>
        
<dl class="all-campaign"> 
<dt><a href="javascript:void(0)"><span class="hida">Select</span><p class="multiSelP1"></p></a></dt>
<dd>
<div class="mutliSelect">
<ul>
 <?php foreach($campaign_list as $value) { ?>
<li><input type="checkbox" value="<?php echo $value->campaign_id?>" name="to_campaign[]" data-title="<?php echo $value->campaign_name?>" class="campaignCheckbox1"/>&nbsp;<span class="more-word2"> <?php echo $value->campaign_name?></span></li>
<?php } ?>        
<input type="hidden" name="contact_id" value="<?php echo $contact_id?>" />
<input type="hidden" name="type" value="<?php echo $type?>" /> 
<input type="hidden" name="actionType" value="update" />    
</ul>
</div>
</dd>
</dl>

</div>
  
        <div class="xstext-right xstext-center mt10 xsmt10"><a href="#" class="blank-btn" data-dismiss="modal">Cancel</a> <button type="submit" class="smart-btn">&nbsp;&nbsp;<?php echo ucfirst($type)?>&nbsp;&nbsp;</button></div>
		</form>
       </div></div>
      </div>
    </div>
  </div>
<script>
$(document).ready(function() {
    $(".copyMoveForm").submit(function(event) {
        var posturl = $(this).attr('action');
        $(this).ajaxSubmit({
            url: posturl,
            dataType: 'json',
            success: function(response) {
				
                if (response.success) {
					
					if(response.model=='hide')
					$('.copyMoveModel').modal('toggle');
					//PNotify.removeAll();
					//new PNotify({title: 'Success',text: response.success_msg,type: 'success'});
					//$(".contentDiv").load(location.href + " .contentDiv"); 
					location.reload(); 
					 
                } else {
				   PNotify.removeAll();
				   new PNotify({title: 'Error',text: response.error,type: 'error'});
                }
            },
        });
        return false;
    });
});
$('body').delegate(".campaignCheckbox1", 'change', function() {
      
	  var length = $('.campaignCheckbox1:checked').length
	  $('.multiSelP1').text(length+' selected');
      });

</script>
<script type='text/javascript' src="<?php echo $this->config->item('templateAssetsPath')?>js/index.js"></script>