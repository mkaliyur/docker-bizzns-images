<!-- calender css and js starts here -->
<link rel="stylesheet" href="<?php echo $this->config->item('templateAssetsPath')?>css/jquery.datetimepicker.css" />
<script src="<?php echo $this->config->item('templateAssetsPath')?>js/jquery.datetimepicker.js" type="text/javascript"></script>
<script type="text/javascript">
$(document).ready(function(){
	$('.from_date').datetimepicker({
		format:'Y/m/d',
		timepicker: false,
		lang: 'en',
		maxDate:  0,
	});
	$('.from_date1').datetimepicker({
		format:'Y/m/d',
		timepicker: false,
		lang: 'en',
		maxDate:  0,
	});
	$('#from_time').datetimepicker({
		format:	'H:i',
		datepicker: false,
		lang: 'en',		
	});
	
	$(document).on('change','.from_date',function(){
		$('.from_date1').datetimepicker({
			format:'Y/m/d',
			timepicker: false,
			lang: 'en',
			maxDate:  new Date($('.from_date').val()),
		});
	});
	$(document).on('change','.from_date1',function(){
		$('.from_date').datetimepicker({
			format:'Y/m/d',
			timepicker: false,
			lang: 'en',
			minDate:  new Date($('.from_date1').val()),
			maxDate:  0,
		});
	});
	

});

</script>
<!-- calender css and js ends here -->
<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padding15 footer-height"> 
  <!-- dash header-->
  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 mb2 xsmb4">
    <div class="dash-header">
      <div class="row">
        <div class="col-md-12 col-sm-12">
          <h1 class="smart-color">Subscriber Management</h1>
          <p>Search as well as manage your subscribers here</p>
        </div>
        <!--<div class="col-md-5 col-sm-5 xstext-right xstext-center mt2 xsmt4 mb0 xsmb2">
<a href="#" class="smart-btn" data-toggle="modal" data-target="#myModal">Create Campaign</a></div> --> 
      </div>
    </div>
  </div>
  <!-- dash header end-->
  <div class="clearfix"></div>
  <div class="col-md-12 smart-tabs clearfix">
    <ul>
      <li><a href="<?php echo site_url('add-contact')?>"  title="Add Subscriber"> <span class="hidden-xs">Add Subscriber</span><i class="icon-Add-Subscriber-Responsive visible-xs"></i> </a></li>
      <li><a href="<?php echo site_url('contact-list')?>" class="active" title="Subscriber Management"> <span class="hidden-xs">Subscriber Management</span><i class="icon-Subscriber-Management-Responsive visible-xs"></i> </a></li>
      <li><a href="<?php echo site_url('suppression-list')?>" title="Suppression List"> <span class="hidden-xs">Suppression List</span><i class="icon-Suppression-List-Responsive visible-xs"></i> </a></li>
    </ul>
  </div>
  <div class="col-md-12 col-sm-12 col-xs-12 mb2 xsmb4">
    <div class="layout-box1 clearfix smart-field">
      <div class="box-padding">
        <div class="col-xs-12 padding0">
          <?php if($this->search_subscriber=='yes') { ?>
          <form action="" method="get" class="searchform">
            <div class="row">
              <div class="col-md-8 col-lg-9 col-sm-8 padding0">
                <div class="col-xs-2 col-xs-15">
                  <input id="multi-cam-list" type="text" placeholder="Select Campaign List" value="<?php if(sizeof($campaign_id)>=1 && !empty($campaign_id)) { echo sizeof($campaign_id).' selected'; } else { echo 'All Selected'; } ?>" readonly=""/>
                  <div class="multi-cam-list listdisplay">
                    <ul>
                      <li>
                        <input name="selectAllCamp" type="checkbox" id="edit-node-types-forum" value="all" class="selectAllCamp" <?php if($_GET['selectAllCamp']=='all'){ ?> checked="checked" <?php } ?>/>
                        &nbsp;<span class="more-word2">All Campaign</span></li>
                      <?php foreach($campaign_list as $val1) { ?>
                      <li>
                        <input name="campaign_id[]" type="checkbox" id="edit-node-types-forum" value="<?php echo $val1->campaign_id?>" class="campaignCheckbox" <?php if(in_array($val1->campaign_id,$campaign_id)) { ?> checked="checked" <?php } ?> />
                        &nbsp;<span class="more-word2">
                        <?php echo $val1->campaign_name?>
                        </span></li>
                      <?php } ?>
                    </ul>
                  </div>
                </div>
                <div class="col-xs-15">
                  <select name="subscribe_type" class="subscribe_type">
                    <option value="all_subscriber" <?php if($subscribe_type=='all_subscriber') { ?> selected="selected" <?php } ?>>All Subscribers</option>
                    <?php if($this->search_by_unsubscriber=='yes') { ?>
                    <option value="unsubscriber" <?php if($subscribe_type=='unsubscriber') { ?> selected="selected" <?php } ?>>Unsubscribers</option>
                    <?php } ?>
                    <?php if($this->search_by_bounce=='yes') { ?>
                    <option value="bounced" <?php if($subscribe_type=='bounced') { ?> selected="selected" <?php } ?>>Bounced</option>
                    <?php } ?>
                    <?php if($this->search_by_message=='yes') { ?>
                    <option value="message" <?php if($subscribe_type=='message') { ?> selected="selected" <?php } ?>>Messages</option>
                    <?php } ?>
                  </select>
                </div>
                <div class="messageDiv" style="display:<?php if($subscribe_type=='message') { echo 'block'; } else { echo 'none'; } ?>">
                  <div class="col-xs-15">
                    <select name="message_type" class="message_type">
                      <option value="newsletter" <?php if($message_type=='newsletter') { ?> selected="selected" <?php } ?>>Newsletters</option>
                      <option value="autoresponder" <?php if($message_type=='autoresponder') { ?> selected="selected" <?php } ?>>Autoresponder</option>
                    </select>
                  </div>
                  <div class="col-xs-15">
                    <select name="newsletter_id" class="newsletter_type">
                      <?php foreach($newsletter_list as $valnews) { ?>
                      <option value="<?php echo $valnews->id?>" <?php if($newsletter_id==$valnews->id) { ?> selected="selected" <?php } ?>>
                      <?php echo $valnews->message_name?>
                      </option>
                      <?php } ?>
                      <?php if(sizeof($newsletter_list)==0) { ?>
                      <option value="" >No Record Found</option>
                      <?php } ?>
                    </select>
                  </div>
                  <div class="col-xs-15">
                    <select name="stat_type">
                      <option value="open" <?php if($stat_type=='open') { ?> selected="selected" <?php } ?>>Open</option>
                      <option value="click" <?php if($stat_type=='click') { ?> selected="selected" <?php } ?>>Click</option>
                      <option value="unopen" <?php if($stat_type=='unopen') { ?> selected="selected" <?php } ?>>Unopen</option>
                      <option value="bounce" <?php if($stat_type=='bounce') { ?> selected="selected" <?php } ?>>Bounce</option>
                      <option value="unsubsribe" <?php if($stat_type=='unsubsribe') { ?> selected="selected" <?php } ?>>Unsubscribe</option>
                    </select>
                  </div>
                </div>
                <div class="col-xs-15">
                  <select name="date_time" class="date_time">
                    <option value="all_time" <?php if($date_time=='all_time') { ?> selected="selected" <?php } ?>>All Time</option>
                    <option value="today" <?php if($date_time=='today') { ?> selected="selected" <?php } ?>>Today</option>
                    <option value="yesterday" <?php if($date_time=='yesterday') { ?> selected="selected" <?php } ?>>Yesterday</option>
                    <option value="last_week" <?php if($date_time=='last_week') { ?> selected="selected" <?php } ?>>Last Week</option>
                    <option value="this_month" <?php if($date_time=='this_month') { ?> selected="selected" <?php } ?>>This Month</option>
                    <option value="last_month" <?php if($date_time=='last_month') { ?> selected="selected" <?php } ?>>Last Month</option>
                    <option value="custom" <?php if($date_time=='custom') { ?> selected="selected" <?php } ?>>Custom</option>
                  </select>
                </div>
                <div class="customDateDiv" style="display:<?php if($date_time=='custom') { echo 'block'; } else { echo 'none'; } ?>">
                  <div class="col-xs-15">
                   
                      <div class="col-xs-12 col-md-6 col-sm-6 xsmb2 padding0">
                        <input class="eventStartDate newEventStart eventEditDate startTime eventEditMetaEntry from_date1"  name="from_date" placeholder="Start Date" readonly="readonly" type="text" value="<?php echo $from_date?>"/>
                      </div>
                      <div class="col-xs-12 col-md-6 col-sm-6 xsmb2 calender_padding padding0">
                        <input class="eventStartDate newEventStart eventEditDate startTime eventEditMetaEntry from_date" name="to_date" placeholder="End Date" readonly="readonly" type="text" value="<?php echo $to_date?>" />
                      </div>
                   
                  </div>
                </div>

                  <div class="col-xs-15">
                    <div class="row">
                      <div class="col-xs-12 col-md-12 col-sm-6 xsmb2">
                       <button type="submit" class="filter-btn" value="search" name="submittype">Filter</button>
                      </div>
                    </div>
                  </div>
              </div>
              <div class="col-md-4 col-lg-3 col-sm-4">
                <div class="mailer-search ">
                  <input type="text" placeholder="Search for Email" name="keyword" value="<?php echo $keyword?>">
                  <button type="submit" class="search--btn-color" value="search" name="submittype"><i class="fa fa-search"></i></button>
                </div>
              </div>
            </div>
          </form>
          <?php } ?>
        </div>
      </div>
      <div class="clearfix"></div>
      <div class="table-responsive table-data contentDiv table-fixed-header table-fixed-coluom" id="autoscroll">
        <table width="100%" border="0" cellspacing="0" cellpadding="0" id="tbl">
          <thead>
            <tr>
              <th class="text-center table-header-checkbox1"><input name="" type="checkbox" value="" id="select_all"/></th>
              <th class="break-word">Email</th>
              <th class="text-center break-word">Name</th>
              <th class="text-center break-word">Campaign</th>
              <th class="text-center break-word">Date</th>
              <th class="text-center break-word">Action</th>
            </tr>
            <tr class="noresult" style="display:  <?php if($total_data==0) { ?> table-row-group <?php } else { ?>none <?php } ?>;">
              <th colspan="6" class="text-center w3000">No Record Found</th>
            </tr>
          </thead>
          <tbody id="tbltbody1">
          </tbody>
        </table>
      </div>
    </div>
    <div class="col-xs-12 xstext-right xstext-center mt2 xsmt8 padding0"> 
	<a href="javascript:" class="exportallbtn smart-btn" title="Export">Export All</a>&nbsp;
	<a data-toggle="modal" data-target="#delete" title="Delete" class="disableGreybtn multi_action_btn deleteMultiLink activity-btn">Delete</a> &nbsp; <a data-toggle="modal" data-target="#copyMoveModel" title="Copy" class="disableGreybtn multi_action_btn copyMove activity-btn" data-type="copy">Copy</a> &nbsp; <a data-toggle="modal" data-target="#copyMoveModel" title="Move" class="disableGreybtn multi_action_btn copyMove activity-btn" data-type="move">Move</a> &nbsp; <a href="javascript:" class="disableGreybtn multi_action_btn exportbtn activity-btn" title="Export">Export Selected</a> </div>
  </div>
</div>
<!-------------- copy move model will come here ----------------------->
<div id="copyMoveModel" class="modal fade copyMoveModel" role="dialog"></div>
<!-------------- copy move model ends here -----------------------> 
<script>
/*-------------------------- delete single contact starts here ------------------------------------*/
$('body').delegate(".deletelink", 'click', function() {
    
	deleteurl = $(this).attr('data-href');
	 $('#delete_yes_button').attr('href',deleteurl);
	 $('#delete_yes_button').removeClass('deleteMultiple');
});
/*-------------------------- delete single contact ends here ------------------------------------*/

/*-------------------------- check/uncheck multiple campaign starts here ------------------------------------*/
$('body').delegate("#select_all", 'change', function() {
      $(".select_checkbox").prop('checked', $(this).prop("checked"));
	  $(".select_checkbox").change();
      });
$('body').delegate(".select_checkbox", 'change', function() {
	  var length = $('.select_checkbox:checked').length
	  if(length==0)
	   {
		  $(".multi_action_btn").addClass('disableGreybtn');
	   } else {
		  $(".multi_action_btn").removeClass('disableGreybtn');
	   }
      });
      
$(".select_checkbox").change(function(){
    if ($('.select_checkbox:checked').length != $('.select_checkbox').length) {
       $("#select_all").prop('checked', false);
    }else{
        $("#select_all").prop('checked', true);
    }
});   
/*-------------------------- check/uncheck multiple campaign ends here ------------------------------------*/

/*-------------------------- copy/move multiple contact starts here ------------------------------------*/
$('body').delegate(".copyMove", 'click', function() {

    var posturl = site_url+'copy-move-contact';
	var id = [];
	$(".select_checkbox:checkbox:checked").each(function (e) {
            id[e] = $(this).val();
        });
	var type = $(this).attr('data-type');
	var actionType = 'view';
	var length = $('.select_checkbox:checked').length;
	if(length==0)
	{ return false; }

    $.ajax({
        url: posturl,
        dataType: 'json',
        type: "POST",
        data: {id: id,type: type,actionType: actionType},
        success: function(data) {
            if (data.success) {
                $('.copyMoveModel').html(data.html);
            }
        },
    });
});
/*-------------------------- copy/move multiple contact ends here ------------------------------------*/

/*-------------------------- delete multiple contact starts here ------------------------------------*/
$('body').delegate(".deleteMultiLink", 'click', function() {
    
	var length = $('.select_checkbox:checked').length;
	if(length==0)
	{ return false; }
	deleteurl = $(this).attr('data-href');
	 $('#delete_yes_button').attr('href','javascript:');
	 $('#delete_yes_button').addClass('deleteMultiple');
});
$('body').delegate(".deleteMultiple", 'click', function() {

    var posturl = site_url+'delete-multiple-contact';
	var id = [];
	$(".select_checkbox:checkbox:checked").each(function (e) {
            id[e] = $(this).val();
        });
	var length = $('.select_checkbox:checked').length;
	if(length==0)
	{ return false; }

    $.ajax({
        url: posturl,
        dataType: 'json',
        type: "POST",
        data: {id: id},
        success: function(data) {
            if (data.success) {
				$('#delete').modal('toggle');
				PNotify.removeAll();
				new PNotify({title: 'Success',text: 'Contacts Deleted successfully',type: 'success'});
                window.location.reload(true);
            } else {
			    $('#delete').modal('toggle');
				PNotify.removeAll();
				new PNotify({title: 'Error',text: data.error_msg,type: 'error'});
			}
        },
    });
});
/*-------------------------- delete multiple contact ends here------------------------------------*/

/*-------------------------- export multiple contact starts here ------------------------------------*/
$('body').delegate(".exportbtn", 'click', function() {

    var posturl = site_url+'export-contact';
	var id = [];
	$(".select_checkbox:checkbox:checked").each(function (e) {
            id[e] = $(this).val();
        });
	var length = $('.select_checkbox:checked').length;
	if(length==0)
	{ return false; }
    
	post(posturl, {id: id});
});

function post(path, params, method) {
    method = method || "post"; // Set method to post by default if not specified.

    // The rest of this code assumes you are not using a library.
    // It can be made less wordy if you use one.
    var form = document.createElement("form");
    form.setAttribute("method", method);
    form.setAttribute("action", path);

    for(var key in params) {
        if(params.hasOwnProperty(key)) {
            var hiddenField = document.createElement("input");
            hiddenField.setAttribute("type", "hidden");
            hiddenField.setAttribute("name", key);
            hiddenField.setAttribute("value", params[key]);

            form.appendChild(hiddenField);
         }
    }

    document.body.appendChild(form);
    form.submit();
}
/*-------------------------- export multiple contact ends here ------------------------------------*/

/*-------------------------- export all contact starts here ------------------------------------*/
$('body').delegate(".exportallbtn", 'click', function() {

    $('.searchform').submit();
	
});
/*-------------------------- export all contact starts here ------------------------------------*/

/*-------------------------- success message starts here---------------------------------------------------*/
<?php if($this->session->userdata('success_msg')) { ?>
new PNotify({
	  title: 'Success',
	  text: '<?php echo $this->session->userdata('success_msg')?>',
	  type: 'success'
	});
<?php $this->session->unset_userdata('success_msg'); } ?>
/*-------------------------- success message ends here---------------------------------------------------*/

/*-------------------------- error message starts here---------------------------------------------------*/
<?php if($this->session->userdata('error_msg')) { ?>
new PNotify({
	  title: 'Error',
	  text: '<?php echo $this->session->userdata('error_msg')?>',
	  type: 'error'
	});
<?php $this->session->unset_userdata('error_msg'); } ?>
/*-------------------------- error message ends here---------------------------------------------------*/

/*-------------------------- search related starts here---------------------------------------------------*/
$('body').delegate(".campaignCheckbox", 'click', function() {
	  var length = $('.campaignCheckbox:checked').length
	  $('#multi-cam-list').val(length+' selected');
      });
$('body').delegate(".selectAllCamp", 'click', function() {
      $(".campaignCheckbox").prop('checked', $(this).prop("checked"));
	  var length = $('.campaignCheckbox:checked').length
	  $('#multi-cam-list').val(length+' selected');
      });	
$(".campaignCheckbox").change(function(){
    if ($('.campaignCheckbox:checked').length != $('.campaignCheckbox').length) {
       $(".selectAllCamp").prop('checked', false);
    }else{
        $(".selectAllCamp").prop('checked', true);
    }
});

$('body').delegate(".subscribe_type", 'change', function() {
	   if($(this).val()=='message')
	    {
	     $('.messageDiv').fadeIn('slow');
	    } else {
	     $('.messageDiv').fadeOut('slow');
		}
});	

$('body').delegate(".message_type", 'change', function() {

    var type = $(this).val();
	var posturl = site_url+'get-newsletter-list'

    $.ajax({
        url: posturl,
        dataType: 'json',
        type: "POST",
        data: {type: type},
        success: function(data) {
            if (data.success) {
			    $('.newsletter_type').html('');
                $.each(data.newsletter_list, function (index, value) {
					$('.newsletter_type').append($('<option/>', { 
						value: value.id,
						text : value.message_name 
					}));
				}); 
				if(data.newsletter_list=='')
				{
				   $('.newsletter_type').append($('<option/>', { 
						value: '',
						text : 'No Record Found' 
					}));
				}
            }
        },
    });
});

$('body').delegate(".date_time", 'change', function() {
	   if($(this).val()=='custom')
	    {
	     $('.customDateDiv').fadeIn('slow');
	    } else {
	     $('.customDateDiv').fadeOut('slow');
		}
});	

/*-------------------------- search related ends here---------------------------------------------------*/

/*-------------------------- campaign multi select div starts here---------------------------------------------------*/
$(window).load(function(){
$('#multi-cam-list').focus(function() {
    $('div.multi-cam-list').show();
    $(document).bind('focusin.multi-cam-list click.example',function(e) {
        if ($(e.target).closest('.multi-cam-list, #multi-cam-list').length) return;
        $(document).unbind('.multi-cam-list');
        $('div.multi-cam-list').fadeOut('medium');
    });
});
$('div.multi-cam-list').hide();
});
/*-------------------------- campaign multi select div ends here---------------------------------------------------*/
</script> 
<script type="text/javascript">
$(document).ready(function() {
var total_record = 0;
var total_groups = <?php echo $total_data; ?>;
var campaign_id = <?php echo json_encode($_GET['campaign_id']);?>;  
var subscribe_type = '<?php echo $_GET['subscribe_type'];?>';  
var message_type = '<?php echo $_GET['message_type'];?>';  
var newsletter_id = '<?php echo $_GET['newsletter_id'];?>';  
var stat_type = '<?php echo $_GET['stat_type'];?>';  
var date_time = '<?php echo $_GET['date_time'];?>';  
var from_date = '<?php echo $_GET['from_date'];?>';  
var to_date = '<?php echo $_GET['to_date'];?>';  
var keyword = '<?php echo $_GET['keyword'];?>';  
var prestat = true;
$('#tbl > tbody').load("<?php echo base_url() ?>contact-list-pagination",
 {'group_no': total_record,'campaign_id': campaign_id,'subscribe_type': subscribe_type
                                                                ,'message_type': message_type,'newsletter_id': newsletter_id,'stat_type': stat_type
                                                                    ,'date_time':date_time,'from_date' :from_date,'to_date':to_date, 'keyword': keyword}, function() {total_record++;});
$('#tbltbody1').scroll(function() { 
   
 //  console.log($('#tbltbody1').scrollTop());
//   console.log($('#tbltbody1').prop('scrollHeight'));
//   console.log($('#tbl').height());

    if($('#tbltbody1').scrollTop() + $('#tbl').height() > $('#tbltbody1').prop('scrollHeight') && prestat)  
    {   
        if(total_record <= total_groups)
        {
          loading = true; 
          $('.loader_image').show(); 
		   
		  prestat = false;
		  
          $.post('<?php echo site_url() ?>contact-list-pagination',{'group_no': total_record,'campaign_id': campaign_id,'subscribe_type': subscribe_type
                                                                ,'message_type': message_type,'newsletter_id': newsletter_id,'stat_type': stat_type
                                                                    ,'date_time':date_time,'from_date' :from_date,'to_date':to_date, 'keyword': keyword },
            function(data){ 
                if (data != "") {
                    $("#tbl > tbody").append(data);                 
                    $('.loader_image').hide();                  
                    total_record++;
					prestat = true;
                }
            });     
        }
    }
});
});
</script> 
