<?php if($this->update_title) { ?>
<div class="col-xs-12 updatediv">
<div class="update-notification">
<div class="box-area clearfix">
<a href="javascript:" onclick="$('.updatediv').toggle();" class="update-close" >X</a>
<div class="padding15"><img src="<?php echo $this->config->item('templateAssetsPath')?>images/update-icon.png" class="center-block mt4 xsmt4" />
<p class="text-center mt3 xsmt4 w500">Update Available (<?php echo $this->update_title?>)</p></div>
<p class="em9 smem9 xsem9 padding15 descriptiondiv" style="display:none"><?php echo $this->update_description?></p>
<div class="padding0 col-xs-12">
<div class="col-xs-6 padding0"><a href="javascript:" class="accept text-center" onclick="$('.descriptiondiv').toggle();"><i class="fa fa-info-circle" aria-hidden="true"></i>
 info</a></div>
<div class="col-xs-6 padding0"><a href="<?php echo site_url('update-version')?>" class="infomation text-center"><i class="fa fa-check-circle" aria-hidden="true"></i>
 Update</a></div>
</div>
</div>
</div>

</div>
<?php } ?>
<script>
$(document).ready(function() {
    $(".licenseKeyForm").submit(function(event) {
        var posturl = $(this).attr('action');
        $(this).ajaxSubmit({
            url: posturl,
            dataType: 'json',
			beforeSend: function() { 
		     $('#preloader').fadeIn(); 
		    },
            success: function(response) {
		        $('#preloader').fadeOut();

                if (response.success) {
					PNotify.removeAll();
				     new PNotify({title: 'Success',text: response.success_msg,type: 'success'});
					window.location.href = site_url+'update-licensing/'+response.license_key+'/yes';
                } else {
                   
				   PNotify.removeAll();
				   new PNotify({title: 'Error',text: response.error_msg,type: 'error'});
				    
                }
            },
        });
        return false;
    });
});
</script>
<footer class="dbfooter">
  <div class="col-xs-12  hidden-xs">
    <div class="col-md-6 col-sm-5 padding0">
      All Rights Reserved 2017 @ <?php echo $this->config->item('PoweredBy');?> </div>
    <div class="col-md-6 col-sm-7 padding0" align="right"> <a href="<?php echo $this->config->item('contact_url');?>" target="_blank">Contact</a>&nbsp; | &nbsp; <a href="<?php echo $this->config->item('privacy');?>" target="_blank">Privacy</a>&nbsp;| &nbsp;<a href="<?php echo $this->config->item('terms');?>" target="_blank">Terms & Conditions</a> &nbsp; | &nbsp; <a href="<?php echo $this->config->item('disclaimer_url');?>" target="_blank">Disclaimer</a> </div>
  </div>
  <div class="col-md-8 col-sm-9 col-xs-12 visible-xs">
  <div class="text-center xsem9 xsmt2"> All Rights Reserved 2017 @ <?php echo $this->config->item('PoweredBy');?></div>
  <div class="text-center xsem9"> <a href="<?php echo $this->config->item('contact_url');?>" target="_blank">Contact</a>&nbsp; | &nbsp; <a href="<?php echo $this->config->item('privacy');?>" target="_blank">Privacy</a>&nbsp;| &nbsp;<a href="<?php echo $this->config->item('terms');?>" target="_blank">Terms & Conditions</a> &nbsp; | &nbsp; <a href="<?php echo $this->config->item('disclaimer_url');?>" target="_blank">Disclaimer</a> </div>
  </div>
</footer>
</div>
</div>
<!--- common modal popup file--->
<?php echo $this->load->view($this->config->item('templateName').'/common_modal')?>
<!--- common modal popup file--->
</body></html>