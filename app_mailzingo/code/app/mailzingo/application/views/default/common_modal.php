<!-- delete modalpopup-->
<div id="delete" class="modal fade" role="dialog">
  <div class="modal-dialog delete-modal">
    <div class="modal-content">
      <div class="modal-body clearfix padding0">
       
       <div class="mt10 xsmt10 text-center">
      
       <h4 class="mb6 col-md-10 col-md-offset-1 col-sm-offset-0 col-sm-12 col-xs-12 col-xs-offset-0">Are You Sure You Want To Delete
This ?</h4>
<div class="clearfix"></div>
<hr />
        <div class="text-center mt6 xsmt6 mb4 xsmb4"><a href="#" class="smart-blank-btn" data-dismiss="modal">No</a> <a href="#" class="yes-btn" id="delete_yes_button">Yes</a></div>
        </div>
      </div>
    </div>
  </div>
</div>





