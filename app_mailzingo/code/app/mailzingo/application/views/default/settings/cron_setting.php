<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padding15 footer-height">
  <!-- dash header-->
  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 mb2 xsmb4">
    <div class="dash-header">
      <div class="row">
        <div class="col-md-12 col-sm-12">
          <h1 class="smart-color em22 smem18 xsem16">Settings</h1>
          <p class="em11 smem10 xsem10">Manage your settings here to automate your mailing process</p>
        </div>
      </div>
    </div>
  </div>
  <!-- dash header end-->
  <div class="clearfix"></div>
  <div class="col-md-12 smart-tabs clearfix">
    <ul>
      <li><a href="<?php echo site_url('settings/user')?>" class="<?php if($page=='user'){echo 'active';} ?>" title="User Settings"> <span class="hidden-xs">User Settings</span><i class="icon-User-Settings visible-xs"></i> </a></li>
      <li><a href="<?php echo site_url('settings/app')?>" title="App Setting" class="<?php if($page=='app'){echo 'active';} ?>" > <span class="hidden-xs">App Setting</span><i class="icon-App-Settings visible-xs"></i> </a></li>
      <li><a href="<?php echo site_url('settings/mail')?>" title="Mail Settings" class="<?php if($page=='mail'){echo 'active';} ?>" > <span class="hidden-xs">Mail Settings</span><i class="icon-Mail-Settings visible-xs"></i> </a></li>
      <li><a href="<?php echo site_url('settings/bounce')?>" title="Bounce Settings" class="<?php if($page=='bounce'){echo 'active';} ?>" > <span class="hidden-xs">Bounce Settings</span><i class="icon-Bounce-Settings visible-xs"></i> </a></li>
        <li><a href="<?php echo site_url('settings/cron')?>" title="Cron Settings" class="<?php if($page=='cron'){echo 'active';} ?>" > <span class="hidden-xs">Cron Settings</span><i class="icon-Corn-Seting visible-xs"></i> </a></li>
		 <li><a href="<?php echo site_url('settings/change-password')?>" title="Change Password" class="<?php if($page=='change_password'){echo 'active';} ?>" > <span class="hidden-xs">Change password</span><i class="icon-change-password visible-xs"></i> </a></li>
		<li><a href="<?php echo site_url('settings/social-settings')?>" title="Social Settings" class="<?php if($page=='social_settings'){echo 'active';} ?>" > <span class="hidden-xs">Social Settings</span><i class="icon-social-setting visible-xs"></i> </a></li>
		<li><a href="<?php echo site_url('settings/api-settings')?>" title="API Settings" class="<?php if($page=='api_settings'){echo 'active';} ?>" > <span class="hidden-xs">API Settings</span><i class="icon-social-setting visible-xs"></i> </a></li>
  <!--   <li><a href="<?php echo site_url('settings/label')?>" title="Label Settings" class="<?php if($page=='label'){echo 'active';} ?>" > <span class="hidden-xs">Label Settings</span><i class="icon-Label-Settings visible-xs"></i> </a></li> -->
    </ul>
  </div>
  <div class="col-md-12 col-sm-12 col-xs-12 mb2 xsmb4">
<div class="layout-box clearfix padding0 smart-field">
<div class="col-xs-12 mt2 padding0">
<div class="row">
<div class="col-md-12 col-sm-12 col-xs-12">
<p>Cron</p>
<textarea name="" cols="" rows="10" class="jstextarea" id="croncopycode"  readonly="readonly">
/usr/bin/curl <?php echo site_url('global-cron')?>
</textarea>
<div class="row">
<div class="col-md-9 mt2 xsmt3"><p class="em9 smem9 xsem9">Copy code and set cron </p></div>
<div class="col-md-3 text-right mt2 xsmt0 mb0 xsmb5"><a href="javascript:" class="smart-btn" data-copytarget="#croncopycode" title="Copy">Copy</a></div>
</div>
</div>

</div>
<div class="clearfix"></div>
</div>
</div>
      
  </div>
</div>
<script>
/*
  Copy text from any appropriate field to the clipboard
  By Craig Buckler, @craigbuckler
  use it, abuse it, do whatever you like with it!
*/
(function() {

    'use strict';
  
  // click events
  document.body.addEventListener('click', copy, true);

    // event handler
    function copy(e) {

    // find target element
    var 
      t = e.target,
      c = t.dataset.copytarget,
      inp = (c ? document.querySelector(c) : null);
      
    // is element selectable?
    if (inp && inp.select) {
      
      // select text
      inp.select();

      try {
        // copy text
        document.execCommand('copy');
        inp.blur();
		PNotify.removeAll();
		new PNotify({title: 'Success',text: 'Copied Successfully', type: 'success'});
        
      }
      catch (err) {
        alert('please press Ctrl/Cmd+C to copy');
      }
      
    }
    
    }

})();
</script>