<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padding15 footer-height">
<!-- dash header-->
<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 mb2 xsmb4">
<div class="dash-header">
<div class="row">
<div class="col-md-12 col-sm-12"><h1 class="smart-color em22 smem18 xsem16">Settings</h1>
<p class="em11 smem10 xsem10">Manage your settings here to automate your mailing process</p></div>
 </div>
</div>
</div>
<!-- dash header end-->

<div class="clearfix"></div>
<div class="col-md-12 smart-tabs clearfix">
<ul>
<li><a href="<?php echo site_url('settings/user')?>" class="<?php if($page=='user'){echo 'active';} ?>" title="User Settings">
<span class="hidden-xs">User Settings</span><i class="icon-User-Settings visible-xs"></i>
</a></li>
<li><a href="<?php echo site_url('settings/app')?>" title="App Setting" class="<?php if($page=='app'){echo 'active';} ?>" >
<span class="hidden-xs">App Setting</span><i class="icon-App-Settings visible-xs"></i>
</a></li>
<li><a href="<?php echo site_url('settings/mail')?>" title="Mail Settings" class="<?php if($page=='mail'){echo 'active';} ?>" >
<span class="hidden-xs">Mail Settings</span><i class="icon-Mail-Settings visible-xs"></i>
</a></li>
<li><a href="<?php echo site_url('settings/bounce')?>" title="Bounce Settings" class="<?php if($page=='bounce'){echo 'active';} ?>" >
<span class="hidden-xs">Bounce Settings</span><i class="icon-Bounce-Settings visible-xs"></i>
</a></li>
<li><a href="<?php echo site_url('settings/cron')?>" title="Cron Settings" class="<?php if($page=='cron'){echo 'active';} ?>" > <span class="hidden-xs">Cron Settings</span><i class="icon-Corn-Seting visible-xs"></i> </a></li>
<li><a href="<?php echo site_url('settings/label')?>" title="Label Settings" class="<?php if($page=='label'){echo 'active';} ?>" >
<span class="hidden-xs">Label Settings</span><i class="icon-Label-Settings visible-xs"></i>
</a></li>
 <li><a href="<?php echo site_url('settings/change-password')?>" title="Change Password" class="<?php if($page=='change_password'){echo 'active';} ?>" > <span class="hidden-xs">Change password</span><i class="icon-change-password visible-xs"></i> </a></li>
</ul>
</div>

<div class="col-md-12 col-sm-12 col-xs-12 mb2 xsmb4">
    <form action="" method="post" enctype="multipart/form-data" id="label_form" >
<div class="layout-box clearfix padding0 smart-field">
<div class="col-xs-12 mt2 padding0">
<div class="row">

<div class="col-md-12 col-sm-12 col-xs-12 padding0 mb2 xsmb3 clearfix">
<div class="col-md-3 col-sm-4 col-xs-12 xsmb2 em10 smem10 xsem10 mt1 xsmt2">Application Name</div>
<div class="col-md-4 col-sm-6 col-xs-12">
    <input name="app_name" value="<?php echo $app_name; ?>" type="text" placeholder="Application Name"></div>
</div>

<div class="col-md-12 col-sm-12 col-xs-12 padding0 mb2 xsmb3 clearfix">
<div class="col-md-3 col-sm-4 col-xs-12 xsmb2 em10 smem10 xsem10 mt1 xsmt2">Application Footer</div>
<div class="col-md-4 col-sm-6 col-xs-12">
<textarea name="app_footer" cols="" rows="4" id="htmlcopycode"><?php echo $app_footer?></textarea>
</div>
</div>

<div class="col-md-12 col-sm-12 col-xs-12 padding0 mb2 xsmb3 clearfix">
<div class="col-md-4 col-sm-6 col-xs-12 col-md-offset-3 col-sm-offset-4 text-right">
    <button type="button" class="suppression-grey-btn" data-copytarget="#htmlcopycode" id="copyButton" > &nbsp; Copy &nbsp; </button></div>
</div>

<div class="col-md-12 col-sm-12 col-xs-12 padding0 clearfix">
<div class="col-md-3 col-sm-4 col-xs-12 xsmb2 em10 smem10 xsem10 mb0 mt1 xsmt2">Application Logo</div>
<div class="col-md-4 col-sm-6 col-xs-12 clearfix">
<!--<input type="file" name="file1" id="file1" class="inputfile inputfile-1" data-multiple-caption="{count} files uploaded" multiple />
<label for="file1"><span>Upload From PC</span></label>-->
<input type="file" name="file1" id="file-6" class="inputfile" />
<label for="file-6" class="w300"><span class="custom-file-upload font-normal">Upload From PC</span>
<p class="w300 mt4 xsmt5 em9 xsem9 smem9"></p>
</label>
</div>
</div>

<div class="col-md-12 col-sm-12 col-xs-12 padding0 mb2 mt0 xsmb3 clearfix">
<div class="col-md-2 col-sm-6 col-xs-12 col-md-offset-3 col-sm-offset-4">
<div class="layout-border"><img src="<?php if($app_logo){echo site_url('assets/uploads/pics/logo/'.$app_logo); }else{ echo $this->config->item('templateAssetsPath').'images/favicon-icon.png'; } ?>" class="img-responsive center-block" /></div></div>
</div>


<div class="col-md-12 col-sm-12 col-xs-12 padding0 clearfix">
<div class="col-md-3 col-sm-4 col-xs-12 xsmb2 em10 smem10 xsem10 mb0 mt1 xsmt2">Favicon</div>
<div class="col-md-4 col-sm-6 col-xs-12 clearfix">
<!--   <input type="hidden" name="h2" value="<?php echo $favicon; ?>" />
<input type="file" name="file2" id="file2" class="inputfile inputfile-1" data-multiple-caption="{count} files uploaded" multiple />
<label for="file2"><span>Upload From PC</span></label> -->
<input type="file" name="file2" id="file-7" class="inputfile" value="<?php echo $favicon; ?>" />
<label for="file-7" class="w300"><span class="custom-file-upload font-normal">Upload From PC</span>
<p class="w300 mt4 xsmt5 em9 xsem9 smem9"></p>
</label>
</div>
</div>

<div class="col-md-12 col-sm-12 col-xs-12 padding0 mb2 mt0 xsmb3 clearfix">
<div class="col-md-2 col-sm-6 col-xs-12 col-md-offset-3 col-sm-offset-4">
<div class="layout-border"><img src="<?php if($favicon){echo site_url('assets/uploads/pics/favicon/'.$favicon);}else{ echo $this->config->item('templateAssetsPath').'images/favicon-icon.png'; } ?>" class="img-responsive center-block" /></div></div>
</div>
</div>
</div>
<div class="clearfix"></div>
</div>
<div class="col-xs-12 xstext-right xstext-center mt2 xsmt8 mb0 padding0">
    <a href="javascript:" onclick="javascript: document.getElementById('label_form').submit();" class="smart-btn" title="Save">Save</a>
</div>
</form>
</div>

</div>

<!-- select file option-->
<script type="text/javascript">
(function() {

    'use strict';
  
  // click events
  document.body.addEventListener('click', copy, true);

    // event handler
    function copy(e) {

    // find target element
    var 
      t = e.target,
      c = t.dataset.copytarget,
      inp = (c ? document.querySelector(c) : null);
    // is element selectable?
    if (inp && inp.select) {
      
      // select text
      inp.select();

      try {
        // copy text
        document.execCommand('copy');
        inp.blur();
		PNotify.removeAll();
		new PNotify({title: 'Success',text: 'Copied Successfully', type: 'success'});
        
      }
      catch (err) {
        alert('please press Ctrl/Cmd+C to copy');
      }
      
    }
    
    }

})();

/*--------------------disable copy button if textarea empty----------------*/
$('#htmlcopycode').keyup(function() {
if (!$.trim($("#htmlcopycode").val())) {
    $('#copyButton').attr("disabled", "disabled");
}else{
    $('#copyButton').removeAttr("disabled");
}
});
/*--------------------disable copy button if textarea empty----------------*/

/*-------------------------- error message starts here---------------------------------------------------*/
<?php if(validation_errors()!='' || $error!='') { ?>
new PNotify({
	  title: 'Error',
	  text: <?php echo json_encode(validation_errors())?>,
	  type: 'error'
	});
<?php } ?>
/*-------------------------- error message ends here---------------------------------------------------*/

/*-------------------------- success message starts here---------------------------------------------------*/
<?php if($this->session->userdata('success_msg')) { ?>
new PNotify({
	  title: 'Success',
	  text: '<?php echo $this->session->userdata('success_msg')?>',
	  type: 'success'
	});
<?php $this->session->unset_userdata('success_msg'); } ?>
/*-------------------------- success message ends here---------------------------------------------------*/
</script>


<!----------------------file upload start js---------------------->
<script>
/*
	By Osvaldas Valutis, www.osvaldas.info
	Available for use under the MIT License
*/
(function(e,t,n){var r=e.querySelectorAll("html")[0];r.className=r.className.replace(/(^|\s)no-js(\s|$)/,"$1js$2")})(document,window,0);
        
'use strict';

;( function ( document, window, index )
{
	var inputs = document.querySelectorAll( '.inputfile' );
	Array.prototype.forEach.call( inputs, function( input )
	{
		var label	 = input.nextElementSibling,
			labelVal = label.innerHTML;

		input.addEventListener( 'change', function( e )
		{var size1=this.files[0].size;
		
		var sizeInMB = (size1 / (1024*1024)).toFixed(2);

			var fileName = '';
			if( this.files && this.files.length > 1 )
				fileName = ( this.getAttribute( 'data-multiple-caption' ) || '' ).replace( '{count}', this.files.length );
			else
				fileName = e.target.value.split( '\\' ).pop();

			if( fileName )
				label.querySelector( 'p' ).innerHTML = fileName+' '+sizeInMB+' MB';

				
			else
				label.innerHTML = labelVal;
		});

		// Firefox bug fix
		input.addEventListener( 'focus', function(){ input.classList.add( 'has-focus' ); });
		input.addEventListener( 'blur', function(){ input.classList.remove( 'has-focus' ); });
	});
}( document, window, 0 ));
</script>

<!----------------------file upload end js ---------------------->

