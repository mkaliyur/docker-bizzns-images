<!--- Tool Tip Code--->
<script>
$(document).ready(function(){
    $('[data-toggle="tooltip"]').tooltip();
});
</script>

<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padding15 footer-height">
  <!-- dash header-->
  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 mb2 xsmb4">
    <div class="dash-header">
      <div class="row">
        <div class="col-md-12 col-sm-12">
          <h1 class="smart-color em22 smem18 xsem16">Settings</h1>
          <p class="em11 smem10 xsem10">Manage your settings here to automate your mailing process</p>
        </div>
      </div>
    </div>
  </div>
  <!-- dash header end-->
  <div class="clearfix"></div>
  <div class="col-md-12 smart-tabs clearfix">
    <ul>
      <li><a href="<?php echo site_url('settings/user')?>" class="<?php if($page=='user'){echo 'active';} ?>" title="User Settings"> <span class="hidden-xs">User Settings</span><i class="icon-User-Settings visible-xs"></i> </a></li>
      <li><a href="<?php echo site_url('settings/app')?>" title="App Setting" class="<?php if($page=='app'){echo 'active';} ?>" > <span class="hidden-xs">App Setting</span><i class="icon-App-Settings visible-xs"></i> </a></li>
      <li><a href="<?php echo site_url('settings/mail')?>" title="Mail Settings" class="<?php if($page=='mail'){echo 'active';} ?>" > <span class="hidden-xs">Mail Settings</span><i class="icon-Mail-Settings visible-xs"></i> </a></li>
      <li><a href="<?php echo site_url('settings/bounce')?>" title="Bounce Settings" class="<?php if($page=='bounce'){echo 'active';} ?>" > <span class="hidden-xs">Bounce Settings</span><i class="icon-Bounce-Settings visible-xs"></i> </a></li>
      <li><a href="<?php echo site_url('settings/cron')?>" title="Cron Settings" class="<?php if($page=='cron'){echo 'active';} ?>" > <span class="hidden-xs">Cron Settings</span><i class="icon-Corn-Seting visible-xs"></i> </a></li>
	   <li><a href="<?php echo site_url('settings/change-password')?>" title="Change Password" class="<?php if($page=='change_password'){echo 'active';} ?>" > <span class="hidden-xs">Change password</span><i class="icon-change-password visible-xs"></i> </a></li>
		<li><a href="<?php echo site_url('settings/social-settings')?>" title="Social Settings" class="<?php if($page=='social_settings'){echo 'active';} ?>" > <span class="hidden-xs">Social Settings</span><i class="icon-social-setting visible-xs"></i> </a></li>
		<li><a href="<?php echo site_url('settings/api-settings')?>" title="API Settings" class="<?php if($page=='api_settings'){echo 'active';} ?>" > <span class="hidden-xs">API Settings</span><i class="icon-social-setting visible-xs"></i> </a></li>
	<!-- <li><a href="<?php echo site_url('settings/label')?>" title="Label Settings" class="<?php if($page=='label'){echo 'active';} ?>" > <span class="hidden-xs">Label Settings</span><i class="icon-Label-Settings visible-xs"></i> </a></li> -->
    </ul>
  </div>
  <div class="col-md-12 col-sm-12 col-xs-12 mb2 xsmb4">
    <form method="post" action="" id="app_form" >
      <div class="layout-box clearfix padding0 smart-field">
        <div class="col-xs-12 mt2 padding0">
          <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12 padding0 mb2 xsmb3 clearfix">
              <div class="col-md-3 col-sm-4 col-xs-12 xsmb2 em10 smem10 xsem10 mt1 xsmt2">Application URL</div>
              <div class="col-md-4 col-sm-6 col-xs-10">
                <input name="app_url" value="<?php echo $app_details->application_URL; ?>" type="text" placeholder="naved.com/flyemail" readonly="">
              </div>
              <div class="col-md-1 col-sm-1 col-xs-2 mt1 xsmt4"> <a href="#" data-toggle="tooltip" data-placement="top" class="red-tooltip visible-xs visible-sm" title="Url where your application installed"><i class="fa fa-info-circle smart-grey em11" aria-hidden="true"></i></a> <a href="#" data-toggle="tooltip" data-placement="right" class="red-tooltip hidden-sm hidden-xs" title="Url where your application installed"><i class="fa fa-info-circle smart-grey em11" aria-hidden="true"></i></a> </div>
            </div>
            <div class="col-md-12 col-sm-12 col-xs-12 padding0 mb2 xsmb3 clearfix">
              <div class="col-md-3 col-sm-4 col-xs-12 xsmb2 em10 smem10 xsem10 mt1 xsmt2">Database Name</div>
              <div class="col-md-4 col-sm-6 col-xs-10">
                <input name="database" value="<?php echo $app_details->database_name; ?>" type="text" placeholder="FlyEmail" readonly="">
              </div>
              <div class="col-md-1 col-sm-1 col-xs-2 mt1 xsmt4"> <a href="#" data-toggle="tooltip" data-placement="top" class="red-tooltip visible-xs visible-sm" title="database name where your application data store"><i class="fa fa-info-circle smart-grey em11" aria-hidden="true"></i></a> <a href="#" data-toggle="tooltip" data-placement="right" class="red-tooltip hidden-sm hidden-xs" title="database name where your application data store"><i class="fa fa-info-circle smart-grey em11" aria-hidden="true"></i></a> </div>
            </div>
            <div class="col-md-12 col-sm-12 col-xs-12 padding0 mb2 xsmb3 clearfix">
              <div class="col-md-3 col-sm-4 col-xs-12 xsmb2 em10 smem10 xsem10 mt1 xsmt2">License Key</div>
              <div class="col-md-4 col-sm-6 col-xs-10">
                <input name="license_key" value="<?php echo $license_key; ?>" type="text" placeholder="3113wdw dwd22e22e 2e 2e2e2e2">
              </div>
              <div class="col-md-1 col-sm-1 col-xs-2 mt1 xsmt4"> <a href="#" data-toggle="tooltip" class="red-tooltip visible-xs visible-sm" data-placement="top" title="the license key generated when you purchased product"><i class="fa fa-info-circle smart-grey em11" aria-hidden="true"></i></a> <a href="#" data-toggle="tooltip" class="red-tooltip hidden-sm hidden-xs" data-placement="right" title="the license key generated when you purchased product"><i class="fa fa-info-circle smart-grey em11" aria-hidden="true"></i></a> </div>
            </div>
          </div>
          <div class="clearfix"></div>
        </div>
      </div>
      <div class="col-xs-12 xstext-right xstext-center mt2 xsmt8 mb0 padding0"> <a href="javascript:void(0)" onClick="javascript: document.getElementById('app_form').submit();" class="smart-btn" title="Save">Save</a> </div>
    </form>
  </div>
</div>
<script>
/*-------------------------- success message starts here---------------------------------------------------*/
<?php if($this->session->userdata('success_msg')) { ?>
new PNotify({
	  title: 'Success',
	  text: '<?php echo $this->session->userdata('success_msg')?>',
	  type: 'success'
	});
<?php $this->session->unset_userdata('success_msg'); } ?>
/*-------------------------- success message ends here---------------------------------------------------*/
/*-------------------------- error message starts here---------------------------------------------------*/
<?php if(validation_errors()!='' || $error!='') { ?>
new PNotify({
	  title: 'Error',
	  text: <?php echo json_encode(validation_errors())?>,
	  type: 'error'
	});
<?php } ?>
/*-------------------------- error message ends here---------------------------------------------------*/
</script>
