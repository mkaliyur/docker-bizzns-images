<!--- Tool Tip Code--->
<script>
$(document).ready(function(){
<!------------ mail setting tab active on radio button js--------------------->
	<?php if($external_active_option == 'sendgrid'){?>
		 $('#sg11').parent().find('a').trigger('click')
	<?php }?>
	<?php if($external_active_option == 'sparkpost'){?>
		 $('#s11').parent().find('a').trigger('click')
	<?php }?>
	
	<?php if($external_active_option == 'postmark'){?>
		 $('#m11').parent().find('a').trigger('click')
	<?php }?>
	
	<?php if($external_active_option == 'elasticmail'){?>
		 $('#r11').parent().find('a').trigger('click')
	<?php }?>
	
	<?php if($external_active_option == 'amazon_ses'){?>
		 $('#as11').parent().find('a').trigger('click')
	<?php }?>
	
	<?php if($external_active_option == 'mailgun'){?>
		 $('#mg11').parent().find('a').trigger('click')
	<?php }?>
	
	$('#sg11').on('click', function(){
		$(this).parent().find('a').trigger('click')
	})
	
	$('#r11').on('click', function(){
		$(this).parent().find('a').trigger('click')
	})

	$('#m11').on('click', function(){
		$(this).parent().find('a').trigger('click')
	})

	$('#s11').on('click', function(){
		$(this).parent().find('a').trigger('click')
	})
	
	$('#as11').on('click', function(){
		$(this).parent().find('a').trigger('click')
	})
	
	$('#mg11').on('click', function(){
		$(this).parent().find('a').trigger('click')
	})
<!------------ mail setting tab active on radio button js end--------------------->
    $('[data-toggle="tooltip"]').tooltip();
	
	$(function() {
  $(".expand").on( "click", function() {
    // $(this).next().slideToggle(200);
    $expand = $(this).find(">:first-child");
    
    if($expand.text() == "+") {
      $expand.text("-");
    } else {
      $expand.text("+");
    }
  });
});
});
</script>

<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padding15 footer-height">
  <!-- dash header-->
  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 mb2 xsmb4">
    <div class="dash-header">
      <div class="row">
        <div class="col-md-12 col-sm-12">
          <h1 class="smart-color em22 smem18 xsem16">Settings</h1>
          <p class="em11 smem10 xsem10">Manage your settings here to automate your mailing process</p>
        </div>
      </div>
    </div>
  </div>
  <!-- dash header end-->
  <div class="clearfix"></div>
  <div class="col-md-12 smart-tabs clearfix">
    <ul>
      <li><a href="<?php echo site_url('settings/user')?>" class="<?php if($page=='user'){echo 'active';} ?>" title="User Settings"> <span class="hidden-xs">User Settings</span><i class="icon-User-Settings visible-xs"></i> </a></li>
      <li><a href="<?php echo site_url('settings/app')?>" title="App Setting" class="<?php if($page=='app'){echo 'active';} ?>" > <span class="hidden-xs">App Setting</span><i class="icon-App-Settings visible-xs"></i> </a></li>
      <li><a href="<?php echo site_url('settings/mail')?>" title="Mail Settings" class="<?php if($page=='mail'){echo 'active';} ?>" > <span class="hidden-xs">Mail Settings</span><i class="icon-Mail-Settings visible-xs"></i> </a></li>
      <li><a href="<?php echo site_url('settings/bounce')?>" title="Bounce Settings" class="<?php if($page=='bounce'){echo 'active';} ?>" > <span class="hidden-xs">Bounce Settings</span><i class="icon-Bounce-Settings visible-xs"></i> </a></li>
      <li><a href="<?php echo site_url('settings/cron')?>" title="Cron Settings" class="<?php if($page=='cron'){echo 'active';} ?>" > <span class="hidden-xs">Cron Settings</span><i class="icon-Corn-Seting visible-xs"></i> </a></li>
      <li><a href="<?php echo site_url('settings/change-password')?>" title="Change Password" class="<?php if($page=='change_password'){echo 'active';} ?>" > <span class="hidden-xs">Change password</span><i class="icon-change-password visible-xs"></i> </a></li>
      <li><a href="<?php echo site_url('settings/social-settings')?>" title="Social Settings" class="<?php if($page=='social_settings'){echo 'active';} ?>" > <span class="hidden-xs">Social Settings</span><i class="icon-social-setting visible-xs"></i> </a></li>
		<li><a href="<?php echo site_url('settings/api-settings')?>" title="API Settings" class="<?php if($page=='api_settings'){echo 'active';} ?>" > <span class="hidden-xs">API Settings</span><i class="icon-social-setting visible-xs"></i> </a></li>
      
      <!-- <li><a href="<?php echo site_url('settings/label')?>" title="Label Settings" class="<?php if($page=='label'){echo 'active';} ?>" > <span class="hidden-xs">Label Settings</span><i class="icon-Label-Settings visible-xs"></i> </a></li> -->
    </ul>
  </div>
  <div class="col-md-12 col-sm-12 col-xs-12 mb2 xsmb4">
    <form action="" method="post" id="mail_form" >
      <div class="layout-box clearfix padding0 smart-field">
        <div class="col-xs-12 mt2 padding0">
          <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12 padding0 mb2 xsmb3 clearfix">
              <div class="col-md-3 col-sm-4 col-xs-12 xsmb2 em10 smem10 xsem10 mt1 xsmt2">Mail Server</div>
              <div class="col-md-4 col-sm-6 col-xs-10 em10 smem10 xsem9">
                <div class="mb2">
                  <input id="default1"  type="radio" <?php if($mail_type=='default'){ echo 'checked';} ?> name="mail_type" value="default" checked>
                  Use inbuild Mailzingo SMTP</div>
                <div class="mb2">
                  <input type="radio" id="smtp1" name="mail_type" <?php if($mail_type=='smtp'){ echo 'checked';} ?> value="smtp">
                  Let Me Specify My Own SMTP Server Details<br>
                </div>
                <input type="radio" name="mail_type" id="smtp_api"  <?php if($mail_type=='external'){ echo 'checked';} ?> value="external">
                External Web Services<br>
              </div>
              <div class="col-md-1 col-sm-1 col-xs-2"> <a href="#" data-toggle="tooltip" data-placement="top" class="red-tooltip visible-xs visible-sm" title="By Default You can send php mail"> <i class="fa fa-info-circle smart-grey em11" aria-hidden="true"></i></a> <a href="#" data-toggle="tooltip" data-placement="right" class="red-tooltip hidden-sm hidden-xs" title="Use default SMTP hosted by your server."> <i class="fa fa-info-circle smart-grey em11" aria-hidden="true"></i></a> </div>
            </div>
            <div class="default1" style="display:<?php if($mail_type=='default'){ echo 'block'; } else { echo 'none'; } ?>" >
              <div class="col-md-12 col-sm-12 col-xs-12 padding0 mb2 xsmb3 clearfix">
                <div class="col-md-3 col-sm-4 col-xs-12 xsmb2 em10 smem10 xsem10 mt1 xsmt2">Mail Per Hour</div>
                <div class="col-md-4 col-sm-6 col-xs-10">
                  <input name="mail_per_hour" value="<?php echo $mail_per_hour; ?>" type="text" placeholder="Mail per hour" class="MAIL_PER_HOPUR">
                </div>
                <div class="col-md-1 col-sm-1 col-xs-2 mt1 xsmt4 icon-space"> <a href="#" data-toggle="tooltip" data-placement="top" class="red-tooltip visible-xs visible-sm" title="Enter Mail per hour"><i class="fa fa-info-circle smart-grey em11" aria-hidden="true"></i></a> <a href="#" data-toggle="tooltip" data-placement="right" class="red-tooltip hidden-sm hidden-xs" title="Enter Mail per hour"><i class="fa fa-info-circle smart-grey em11" aria-hidden="true"></i></a> </div>
              </div>
            </div>
            <div class="smtpdiv" style="display:<?php if($mail_type=='smtp'){ echo 'block'; } else { echo 'none'; } ?>" >
              <div class="col-md-12 col-sm-12 col-xs-12 padding0 mb2 xsmb3 clearfix">
                <div class="col-md-3 col-sm-4 col-xs-12 xsmb2 em10 smem10 xsem10 mt1 xsmt2">SMTP Hostname</div>
                <div class="col-md-4 col-sm-6 col-xs-10">
                  <input name="SMTP_hostname" value="<?php echo $SMTP_hostname; ?>" type="text" placeholder="Hostname" class="SMTP_hostname">
                </div>
                <div class="col-md-1 col-sm-1 col-xs-2 mt1 xsmt4 icon-space"> <a href="#" data-toggle="tooltip" data-placement="top" class="red-tooltip visible-xs visible-sm" title="Enter SMTP Host Name"><i class="fa fa-info-circle smart-grey em11" aria-hidden="true"></i></a> <a href="#" data-toggle="tooltip" data-placement="right" class="red-tooltip hidden-sm hidden-xs" title="Enter SMTP Host Name"><i class="fa fa-info-circle smart-grey em11" aria-hidden="true"></i></a> </div>
              </div>
              <div class="col-md-12 col-sm-12 col-xs-12 padding0 mb2 xsmb3 clearfix" >
                <div class="col-md-3 col-sm-4 col-xs-12 xsmb2 em10 smem10 xsem10 mt1 xsmt2">SMTP Username</div>
                <div class="col-md-4 col-sm-6 col-xs-10">
                  <input name="SMTP_username" value="<?php echo $SMTP_username; ?>" type="text" placeholder="user name" class="SMTP_username">
                </div>
                <div class="col-md-1 col-sm-1 col-xs-2 mt1 xsmt4 icon-space"> <a href="#" data-toggle="tooltip" data-placement="top" class="red-tooltip visible-xs visible-sm" title="Enter SMTP User Name"><i class="fa fa-info-circle smart-grey em11" aria-hidden="true"></i></a> <a href="#" data-toggle="tooltip" data-placement="right" class="red-tooltip hidden-sm hidden-xs" title="Enter SMTP User Name"><i class="fa fa-info-circle smart-grey em11" aria-hidden="true"></i></a> </div>
              </div>
              <div class="col-md-12 col-sm-12 col-xs-12 padding0 mb2 xsmb3 clearfix">
                <div class="col-md-3 col-sm-4 col-xs-12 xsmb2 em10 smem10 xsem10 mt1 xsmt2">SMTP Password</div>
                <div class="col-md-4 col-sm-6 col-xs-10">
                  <input name="SMTP_password" value="<?php echo $SMTP_password; ?>" type="password" placeholder="*********" class="SMTP_password">
                </div>
                <div class="col-md-1 col-sm-1 col-xs-2 mt1 xsmt4 icon-space"> <a href="#" data-toggle="tooltip" class="red-tooltip visible-xs visible-sm" data-placement="top" title="Enter SMTP Password "><i class="fa fa-info-circle smart-grey em11" aria-hidden="true"></i></a> <a href="#" data-toggle="tooltip" class="red-tooltip hidden-sm hidden-xs" data-placement="right" title="Enter SMTP Password"><i class="fa fa-info-circle smart-grey em11" aria-hidden="true"></i></a> </div>
              </div>
              <div class="col-md-12 col-sm-12 col-xs-12 padding0 mb1 xsmb3 clearfix">
                <div class="col-md-3 col-sm-4 col-xs-12 xsmb2 em10 smem10 xsem10 mt1 xsmt2">SMTP Port</div>
                <div class="col-md-4 col-sm-6 col-xs-10">
                  <input name="SMTP_port" value="<?php echo $SMTP_port; ?>" type="text" placeholder="587" class="SMTP_port">
                </div>
                <div class="col-md-1 col-sm-1 col-xs-2 mt1 xsmt4 icon-space"> <a href="#" data-toggle="tooltip" class="red-tooltip visible-xs visible-sm" data-placement="top" title="Enter SMTP Port (Example : 25 )"><i class="fa fa-info-circle smart-grey em11" aria-hidden="true"></i></a> <a href="#" data-toggle="tooltip" class="red-tooltip hidden-sm hidden-xs" data-placement="right" title="Enter SMTP Port (Example : 25 )"><i class="fa fa-info-circle smart-grey em11" aria-hidden="true"></i></a> </div>
              </div>
              <div class="col-md-12 col-sm-12 col-xs-12 padding0 mb2 xsmb3 clearfix" id="test_div" >
                <div class="col-md-4 col-sm-6 col-xs-10 col-md-offset-3 col-sm-offset-4 text-right"> <a data-toggle="modal" data-target="#testMail" class="suppression-grey-btn">Test Mail</a></div>
              </div>
            </div>
          </div>
        </div>
        <div class="clearfix"></div>
        <!-- Added for external web service start  -->
        <div class="col-xs-12 mt2 padding0 smtp_api_div" style="display:<?php if($mail_type=='external'){ echo 'block'; } else { echo 'none'; } ?>">
          <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
			
			
			
			<div class="panel panel-default faq-border-radius">
              <div class="panel-heading" role="tab" id="headingOne">
                <h4 class="panel-title mail-seeting-tab">
				
				<label for='as11'>
				<input type="radio" id='as11' name="option_name" value="amazon_ses" <?php if($external_active_option=='amazon_ses'){ echo "checked";}?>>
           		
                <span class="w300"> &nbsp; Amazon SES </span> <i class="more-less glyphicon glyphicon-plus pull-right"></i>
                <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">  </a>
            </label>
				
				
                </h4>
              </div>
              <div id="collapseOne" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne">
                <div class="panel-body multi-tabs-nav faq-tabs-content1 smart-pill-tab">
                  <div class="col-md-12 col-sm-12 col-xs-12 padding0 mb2 xsmb3 clearfix">
                    <div class="col-md-3 col-sm-4 col-xs-12 xsmb2 em10 smem10 xsem10 mt1 xsmt2">Amazon Sending Email</div>
                    <div class="col-md-4 col-sm-6 col-xs-10">
                      <input name="amazon_ses[sending_mail]" value="<?php echo $amazon_sending_email; ?>" type="text" placeholder="Amazon Sending Email" class="AMAZON_SECRET_KEY">
                    </div>
                    <div class="col-md-1 col-sm-1 col-xs-2 mt1 xsmt4 icon-space"> <a href="#" data-toggle="tooltip" data-placement="top" class="red-tooltip visible-xs visible-sm" title="Enter Amazon Sending Email"><i class="fa fa-info-circle smart-grey em11" aria-hidden="true"></i></a> <a href="#" data-toggle="tooltip" data-placement="right" class="red-tooltip hidden-sm hidden-xs" title="Enter Amazon Sending Email"><i class="fa fa-info-circle smart-grey em11" aria-hidden="true"></i></a> </div>
                  </div>
                  <div class="col-md-12 col-sm-12 col-xs-12 padding0 mb2 xsmb3 clearfix">
                    <div class="col-md-3 col-sm-4 col-xs-12 xsmb2 em10 smem10 xsem10 mt1 xsmt2">Amazon Secret Key</div>
                    <div class="col-md-4 col-sm-6 col-xs-10">
                      <input name="amazon_ses[secret_key]" value="<?php echo $amazon_api_key; ?>" type="text" placeholder="Amazon Secret Key" class="AMAZON_SECRET_KEY">
                    </div>
                    <div class="col-md-1 col-sm-1 col-xs-2 mt1 xsmt4 icon-space"> <a href="#" data-toggle="tooltip" data-placement="top" class="red-tooltip visible-xs visible-sm" title="Enter Amazon Secret Key"><i class="fa fa-info-circle smart-grey em11" aria-hidden="true"></i></a> <a href="#" data-toggle="tooltip" data-placement="right" class="red-tooltip hidden-sm hidden-xs" title="Enter Amazon Secret Key"><i class="fa fa-info-circle smart-grey em11" aria-hidden="true"></i></a> </div>
                  </div>
                  <div class="col-md-12 col-sm-12 col-xs-12 padding0 mb2 xsmb3 clearfix">
                    <div class="col-md-3 col-sm-4 col-xs-12 xsmb2 em10 smem10 xsem10 mt1 xsmt2">Amazon Secret Password</div>
                    <div class="col-md-4 col-sm-6 col-xs-10">
                      <input name="amazon_ses[secret_password]" value="<?php echo $amazon_api_secret; ?>" type="text" placeholder="Amazon Secret Password" class="AMAZON_SECRET_KEY">
                    </div>
                    <div class="col-md-1 col-sm-1 col-xs-2 mt1 xsmt4 icon-space"> <a href="#" data-toggle="tooltip" data-placement="top" class="red-tooltip visible-xs visible-sm" title="Enter Amazon Secret Password"><i class="fa fa-info-circle smart-grey em11" aria-hidden="true"></i></a> <a href="#" data-toggle="tooltip" data-placement="right" class="red-tooltip hidden-sm hidden-xs" title="Enter Amazon Secret Password"><i class="fa fa-info-circle smart-grey em11" aria-hidden="true"></i></a> </div>
                  </div>
				   <div class="col-md-12 col-sm-12 col-xs-12 padding0 mb2 xsmb3 clearfix">
                    <div class="col-md-3 col-sm-4 col-xs-12 xsmb2 em10 smem10 xsem10 mt1 xsmt2">Amazon SES Region</div>
                    <div class="col-md-4 col-sm-6 col-xs-10">
                      <select name="amazon_ses[region]"  class="AMAZON_SECRET_KEY">	
					  <option value="" <?php if($amazon_region == ''){ echo "selected";} ?>>Select Region</option>
					  <option value="us_east_1" <?php if($amazon_region == 'us_east_1'){ echo "selected";} ?>>us_east_1</option>
					  <option value="us_west_1" <?php if($amazon_region == 'eu_west_1'){ echo "selected";} ?>>eu_west_1</option>
					  <option value="us_west_2" <?php if($amazon_region == 'us_west_2'){ echo "selected";} ?>>us_west_2</option>
					  </select>
					</div>
                    <div class="col-md-1 col-sm-1 col-xs-2 mt1 xsmt4 icon-space"> <a href="#" data-toggle="tooltip" data-placement="top" class="red-tooltip visible-xs visible-sm" title="Enter Amazon SES Region"><i class="fa fa-info-circle smart-grey em11" aria-hidden="true"></i></a> <a href="#" data-toggle="tooltip" data-placement="right" class="red-tooltip hidden-sm hidden-xs" title="Enter Amazon SES Region"><i class="fa fa-info-circle smart-grey em11" aria-hidden="true"></i></a> </div>
                  </div>
                </div>
              </div>
            </div>
        	
			<div class="panel panel-default faq-border-radius">
              <div class="panel-heading" role="tab" id="headingOne">
                <h4 class="panel-title mail-seeting-tab">
                <label for='r11'>
                 <input type="radio" id='r11' name="option_name" value="elasticmail" <?php if($external_active_option=='elasticmail'){ echo "checked";}?>>
           		
                <span class="w300"> &nbsp; Elastic Mail</span> <i class="more-less glyphicon glyphicon-plus pull-right"></i>
                <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapsefive" aria-expanded="true" aria-controls="collapsetwo">  </a>
            </label>
                </h4>
              </div>
              <div id="collapsefive" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne">
                <div class="panel-body multi-tabs-nav faq-tabs-content1 smart-pill-tab">
                  <div class="col-md-12 col-sm-12 col-xs-12 padding0 mb2 xsmb3 clearfix">
                    <div class="col-md-3 col-sm-4 col-xs-12 xsmb2 em10 smem10 xsem10 mt1 xsmt2">Elasticmail Verifed Email</div>
                    <div class="col-md-4 col-sm-6 col-xs-10">
                      <input name="elasticmail[sending_email]" value="<?php echo $elastic_sending_email; ?>" type="text" placeholder="Elasticmail Sending Email" class="POSTMARK_API">
                    </div>
                    <div class="col-md-1 col-sm-1 col-xs-2 mt1 xsmt4 icon-space"> <a href="#" data-toggle="tooltip" data-placement="top" class="red-tooltip visible-xs visible-sm" title="Enter ElasticMail Verifed Email"><i class="fa fa-info-circle smart-grey em11" aria-hidden="true"></i></a> <a href="#" data-toggle="tooltip" data-placement="right" class="red-tooltip hidden-sm hidden-xs" title="Enter ElasticMail Sending Email"><i class="fa fa-info-circle smart-grey em11" aria-hidden="true"></i></a> </div>
                  </div>
                  <div class="col-md-12 col-sm-12 col-xs-12 padding0 mb2 xsmb3 clearfix">
                    <div class="col-md-3 col-sm-4 col-xs-12 xsmb2 em10 smem10 xsem10 mt1 xsmt2">ElasticMail Api Key</div>
                    <div class="col-md-4 col-sm-6 col-xs-10">
                      <input name="elasticmail[api_key]" value="<?php echo $elastic_api_key; ?>" type="text" placeholder="ElasticMail Api" class="ELASTICMAIL_API">
                    </div>
                    <div class="col-md-1 col-sm-1 col-xs-2 mt1 xsmt4 icon-space"> <a href="#" data-toggle="tooltip" data-placement="top" class="red-tooltip visible-xs visible-sm" title="Enter ElasticMail Api"><i class="fa fa-info-circle smart-grey em11" aria-hidden="true"></i></a> <a href="#" data-toggle="tooltip" data-placement="right" class="red-tooltip hidden-sm hidden-xs" title="Enter ElasticMail Api Key"><i class="fa fa-info-circle smart-grey em11" aria-hidden="true"></i></a> </div>
                  </div>
                </div>
              </div>
            </div>
			
			   
			<div class="panel panel-default faq-border-radius">
              <div class="panel-heading" role="tab" id="headingOne">
                <h4 class="panel-title mail-seeting-tab">
				
				<label for='mg11'>
				 <input type="radio" id='mg11'  name="option_name" value="mailgun" <?php if($external_active_option=='mailgun'){ echo "checked";}?>>
           		
                <span class="w300"> &nbsp; MailGun</span> <i class="more-less glyphicon glyphicon-plus pull-right"></i>
                <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapsefour" aria-expanded="true" aria-controls="collapsetwo">  </a>
            </label>
				
			</h4>
              </div>
              <div id="collapsefour" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne">
                <div class="panel-body multi-tabs-nav faq-tabs-content1 smart-pill-tab">
                  <div class="col-md-12 col-sm-12 col-xs-12 padding0 mb2 xsmb3 clearfix">
                    <div class="col-md-3 col-sm-4 col-xs-12 xsmb2 em10 smem10 xsem10 mt1 xsmt2">Mailgun Api Key</div>
                    <div class="col-md-4 col-sm-6 col-xs-10">
                      <input name="mailgun[api_key]" value="<?php echo $mailgun_api_key; ?>" type="text" placeholder="MailGun Api Key" class="MAILGUN_API">
                    </div>
                    <div class="col-md-1 col-sm-1 col-xs-2 mt1 xsmt4 icon-space"> <a href="#" data-toggle="tooltip" data-placement="top" class="red-tooltip visible-xs visible-sm" title="Enter MailGun Api"><i class="fa fa-info-circle smart-grey em11" aria-hidden="true"></i></a> <a href="#" data-toggle="tooltip" data-placement="right" class="red-tooltip hidden-sm hidden-xs" title="Enter MailGun Api Key"><i class="fa fa-info-circle smart-grey em11" aria-hidden="true"></i></a> </div>
                  </div>
                
				<div class="col-md-12 col-sm-12 col-xs-12 padding0 mb2 xsmb3 clearfix">
                    <div class="col-md-3 col-sm-4 col-xs-12 xsmb2 em10 smem10 xsem10 mt1 xsmt2">Mailgun Sending Domain</div>
                    <div class="col-md-4 col-sm-6 col-xs-10">
                      <input name="mailgun[sending_domain]" value="<?php echo $mailgun_sending_domain; ?>" type="text" placeholder="MailGun Sending Domain" class="MAILGUN_API">
                    </div>
                    <div class="col-md-1 col-sm-1 col-xs-2 mt1 xsmt4 icon-space"> <a href="#" data-toggle="tooltip" data-placement="top" class="red-tooltip visible-xs visible-sm" title="Enter MailGun Api"><i class="fa fa-info-circle smart-grey em11" aria-hidden="true"></i></a> <a href="#" data-toggle="tooltip" data-placement="right" class="red-tooltip hidden-sm hidden-xs" title="Enter MailGun Api Key"><i class="fa fa-info-circle smart-grey em11" aria-hidden="true"></i></a> </div>
                  </div>
				 
				<div class="col-md-12 col-sm-12 col-xs-12 padding0 mb2 xsmb3 clearfix">
                    <div class="col-md-3 col-sm-4 col-xs-12 xsmb2 em10 smem10 xsem10 mt1 xsmt2">Mailgun Sending Email</div>
                    <div class="col-md-4 col-sm-6 col-xs-10">
                      <input name="mailgun[sending_email]" value="<?php echo $mailgun_sending_email; ?>" type="text" placeholder="MailGun Sending Email" class="MAILGUN_API">
                    </div>
                    <div class="col-md-1 col-sm-1 col-xs-2 mt1 xsmt4 icon-space"> <a href="#" data-toggle="tooltip" data-placement="top" class="red-tooltip visible-xs visible-sm" title="Enter MailGun Api"><i class="fa fa-info-circle smart-grey em11" aria-hidden="true"></i></a> <a href="#" data-toggle="tooltip" data-placement="right" class="red-tooltip hidden-sm hidden-xs" title="Enter MailGun Api Key"><i class="fa fa-info-circle smart-grey em11" aria-hidden="true"></i></a> </div>
                  </div>	
                
				
				</div>
              </div>
            </div>
			
			
			 <div class="panel panel-default faq-border-radius">
              <div class="panel-heading" role="tab" id="headingOne">
                <h4 class="panel-title mail-seeting-tab">
                <label for='m11'>
                <input type="radio" id='m11'  name="option_name" value="postmark" <?php if($external_active_option=='postmark'){ echo "checked";}?>>
                
           <span class="w300"> &nbsp; PostMark</span> <i class="more-less glyphicon glyphicon-plus pull-right"></i>
           <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapsethree" aria-expanded="true" aria-controls="collapsetwo"> </a>
            </label>
                </h4>
              </div>
              <div id="collapsethree" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne">
                <div class="panel-body multi-tabs-nav faq-tabs-content1 smart-pill-tab">
                  <div class="col-md-12 col-sm-12 col-xs-12 padding0 mb2 xsmb3 clearfix">
                    <div class="col-md-3 col-sm-4 col-xs-12 xsmb2 em10 smem10 xsem10 mt1 xsmt2">PostMark Verifed Email</div>
                    <div class="col-md-4 col-sm-6 col-xs-10">
                      <input name="postmark[sending_email]" value="<?php echo $postmark_sending_email; ?>" type="text" placeholder="PostMark Sending Email" class="POSTMARK_API">
                    </div>
                    <div class="col-md-1 col-sm-1 col-xs-2 mt1 xsmt4 icon-space"> <a href="#" data-toggle="tooltip" data-placement="top" class="red-tooltip visible-xs visible-sm" title="Enter PostMark Verifed Email"><i class="fa fa-info-circle smart-grey em11" aria-hidden="true"></i></a> <a href="#" data-toggle="tooltip" data-placement="right" class="red-tooltip hidden-sm hidden-xs" title="Enter Postmark Sending Email"><i class="fa fa-info-circle smart-grey em11" aria-hidden="true"></i></a> </div>
                  </div>
                  <div class="col-md-12 col-sm-12 col-xs-12 padding0 mb2 xsmb3 clearfix">
                    <div class="col-md-3 col-sm-4 col-xs-12 xsmb2 em10 smem10 xsem10 mt1 xsmt2">PostMark Api Key</div>
                    <div class="col-md-4 col-sm-6 col-xs-10">
                      <input name="postmark[api_key]" value="<?php echo $postmark_api_key; ?>" type="text" placeholder="PostMark Api" class="POSTMARK_API">
                    </div>
                    <div class="col-md-1 col-sm-1 col-xs-2 mt1 xsmt4 icon-space"> <a href="#" data-toggle="tooltip" data-placement="top" class="red-tooltip visible-xs visible-sm" title="Enter PostMark Api"><i class="fa fa-info-circle smart-grey em11" aria-hidden="true"></i></a> <a href="#" data-toggle="tooltip" data-placement="right" class="red-tooltip hidden-sm hidden-xs" title="Enter Postmark Api"><i class="fa fa-info-circle smart-grey em11" aria-hidden="true"></i></a> </div>
                  </div>
                </div>
              </div>
            </div>
           
		   
            <div class="panel panel-default faq-border-radius">
              <div class="panel-heading" role="tab" id="headingOne">
                <h4 class="panel-title mail-seeting-tab">
                <label for='s11'>
                 <input type="radio"  id='s11' name="option_name" value="sparkpost" <?php if($external_active_option=='sparkpost'){ echo "checked";}?>> 
               
           <span class="w300"> &nbsp; SparkPost</span> <i class="more-less glyphicon glyphicon-plus pull-right"></i>
           <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapsetwo" aria-expanded="true" aria-controls="collapsetwo"> </a>
            </label>
                </h4>
              </div>
              <div id="collapsetwo" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne">
                <div class="panel-body multi-tabs-nav faq-tabs-content1 smart-pill-tab">
                  <div class="col-md-12 col-sm-12 col-xs-12 padding0 mb2 xsmb3 clearfix">
                    <div class="col-md-3 col-sm-4 col-xs-12 xsmb2 em10 smem10 xsem10 mt1 xsmt2">SparkPost Sending EMail</div>
                    <div class="col-md-4 col-sm-6 col-xs-10">
                      <input name="sparkpost[sending_email]" value="<?php echo $sparkpost_sending_email; ?>" type="text" placeholder="SparkPost Sending Email" class="SPARKPOST_API">
                    </div>
                    <div class="col-md-1 col-sm-1 col-xs-2 mt1 xsmt4 icon-space"> <a href="#" data-toggle="tooltip" data-placement="top" class="red-tooltip visible-xs visible-sm" title="Enter Sparkpost Sending Email"><i class="fa fa-info-circle smart-grey em11" aria-hidden="true"></i></a> <a href="#" data-toggle="tooltip" data-placement="right" class="red-tooltip hidden-sm hidden-xs" title="Enter Sparkpost Sending Email"><i class="fa fa-info-circle smart-grey em11" aria-hidden="true"></i></a> </div>
                  </div>
                  <div class="col-md-12 col-sm-12 col-xs-12 padding0 mb2 xsmb3 clearfix">
                    <div class="col-md-3 col-sm-4 col-xs-12 xsmb2 em10 smem10 xsem10 mt1 xsmt2">SparkPost Api Key</div>
                    <div class="col-md-4 col-sm-6 col-xs-10">
                      <input name="sparkpost[api_key]" value="<?php echo $sparkpost_api_key; ?>" type="text" placeholder="SparkPost Api" class="SPARKPOST_API">
                    </div>
                    <div class="col-md-1 col-sm-1 col-xs-2 mt1 xsmt4 icon-space"> <a href="#" data-toggle="tooltip" data-placement="top" class="red-tooltip visible-xs visible-sm" title="Enter Sparkpost Api"><i class="fa fa-info-circle smart-grey em11" aria-hidden="true"></i></a> <a href="#" data-toggle="tooltip" data-placement="right" class="red-tooltip hidden-sm hidden-xs" title="Enter Sparkpost Api"><i class="fa fa-info-circle smart-grey em11" aria-hidden="true"></i></a> </div>
                  </div>
                </div>
              </div>
            </div>
          
			<div class="panel panel-default faq-border-radius">
              <div class="panel-heading" role="tab" id="headingOne">
                <h4 class="panel-title mail-seeting-tab">
				
				<label for='sg11'>
				<input type="radio" id='sg11' name="option_name" value="sendgrid" <?php if($external_active_option=='sendgrid'){ echo "checked";}?>>
           		
                <span class="w300"> &nbsp; Sendgrid </span> <i class="more-less glyphicon glyphicon-plus pull-right"></i>
                <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseSg" aria-expanded="true" aria-controls="collapseOne">  </a>
            </label>
				
				
                </h4>
              </div>
              <div id="collapseSg" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne">
                <div class="panel-body multi-tabs-nav faq-tabs-content1 smart-pill-tab">
                  <div class="col-md-12 col-sm-12 col-xs-12 padding0 mb2 xsmb3 clearfix">
                    <div class="col-md-3 col-sm-4 col-xs-12 xsmb2 em10 smem10 xsem10 mt1 xsmt2">Sendgrid Sending Email</div>
                    <div class="col-md-4 col-sm-6 col-xs-10">
                      <input name="sendgrid[sending_mail]" value="<?php echo $sendgrid_sending_email; ?>" type="text" placeholder="Sendgrid Sending Email" class="SENDGRID_SECRET_KEY">
                    </div>
                    <div class="col-md-1 col-sm-1 col-xs-2 mt1 xsmt4 icon-space"> <a href="#" data-toggle="tooltip" data-placement="top" class="red-tooltip visible-xs visible-sm" title="Enter Sendgrid Sending Email"><i class="fa fa-info-circle smart-grey em11" aria-hidden="true"></i></a> <a href="#" data-toggle="tooltip" data-placement="right" class="red-tooltip hidden-sm hidden-xs" title="Enter Sendgrid Sending Email"><i class="fa fa-info-circle smart-grey em11" aria-hidden="true"></i></a> </div>
                  </div>
                  <div class="col-md-12 col-sm-12 col-xs-12 padding0 mb2 xsmb3 clearfix">
                    <div class="col-md-3 col-sm-4 col-xs-12 xsmb2 em10 smem10 xsem10 mt1 xsmt2">Sendgrid Api Key</div>
                    <div class="col-md-4 col-sm-6 col-xs-10">
                      <input name="sendgrid[api_key]" value="<?php echo $sendgrid_api_key; ?>" type="text" placeholder="Sendgrid Api Key" class="AMAZON_SECRET_KEY">
                    </div>
                    <div class="col-md-1 col-sm-1 col-xs-2 mt1 xsmt4 icon-space"> <a href="#" data-toggle="tooltip" data-placement="top" class="red-tooltip visible-xs visible-sm" title="Enter Sendgrid Secret Key"><i class="fa fa-info-circle smart-grey em11" aria-hidden="true"></i></a> <a href="#" data-toggle="tooltip" data-placement="right" class="red-tooltip hidden-sm hidden-xs" title="Enter Sendgrid Secret Key"><i class="fa fa-info-circle smart-grey em11" aria-hidden="true"></i></a> </div>
                  </div>
                 </div>
              </div>
            </div>
			 
		  </div>
        </div>
        <!-- Added for external web service end  -->
        <div class="clearfix"></div>
      </div>
      <div class="col-xs-12 xstext-right xstext-center mt2 xsmt8 mb0 padding0"> <a href="#" onclick="javascript: document.getElementById('mail_form').submit();" class="smart-btn" title="Save">Save</a> </div>
    </form>
  </div>
</div>
<!-- Test Mail modalpopup-->
<div id="testMail" class="modal fade" role="dialog">
  <div class="modal-dialog campaign-modal">
    <div class="modal-content">
      <div class="modal-body padding0 clearfix xsmb4 xsmt4">
        <div class="col-md-5 col-sm-5 col-xs-12 mt1 xsmt2">
          <div class="col-xs-12 bottom-border"> <img src="<?php echo $this->config->item('templateAssetsPath')?>images/testmail.png" class="img-responsive center-block mobile-img" /></div>
        </div>
        <div class="col-md-7 col-sm-7 col-xs-12 left-border campaign-field xsmt4 mt0 ">
          <div class="col-xs-12 mb2">
            <h3 class="em16 smem16 xsem12">Sent Test Mail</h3>
            <div class="form-group mt12 xsmt6">
              <label class="w300 sr-only">Campaign Name</label>
              <input type="text" name="testemail" placeholder="email@sitename.com" class="form-control testemail"/>
            </div>
            <p class="em9 smem9 xsem9"><span class="smart-red">Note : </span> Test Your SMTP settings.</p>
            <div class="text-right mt12 xsmt6 xsmb4 xstext-right xstext-center"><a href="#" class="blank-btn" data-dismiss="modal">Cancel</a> <a href="javascript:" class="smart-btn sendTestmail">Send</a></div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<script>
/*-------------------------- accordian tabs start here ---------------------------------------------------*/
  function toggleIcon(e) {
        $(e.target)
        .prev('.panel-heading')
        .find(".more-less")
        .toggleClass('glyphicon-plus glyphicon-minus');
        }
        $('.panel-group').on('hidden.bs.collapse', toggleIcon);
        $('.panel-group').on('shown.bs.collapse', toggleIcon);
/*-------------------------- accordian tabs end here ---------------------------------------------------*/
		
/*-------------------------- show hide smtp div starts here ---------------------------------------------------*/
$('#default1').click(function() {
	$(".default1").fadeIn();
    $(".smtpdiv").fadeOut();
	 $(".smtp_api_div").fadeOut();
});
$('#smtp1').click(function() {
$(".default1").fadeOut();
    $(".smtpdiv").fadeIn()
	 $(".smtp_api_div").fadeOut();
});
$('#smtp_api').click(function() {
$(".default1").fadeOut();
	$(".smtpdiv").fadeOut();
    $(".smtp_api_div").fadeIn();
});
/*-------------------------- show hide smtp div ends here ---------------------------------------------------*/

/*-------------------------- Send test mail starts here ---------------------------------------------------*/
$('body').delegate(".sendTestmail", 'click', function() {
        var posturl = site_url+'send-smtp-test-mail';
		var SMTP_hostname = $('.SMTP_hostname').val();
		var SMTP_username = $('.SMTP_username').val();
		var SMTP_password = $('.SMTP_password').val();
		var SMTP_port = $('.SMTP_port').val();
        var testemail = $('.testemail').val();
		
        $.ajax({
        url: posturl,
        dataType: 'json',
        type: "POST",
        data: {SMTP_hostname: SMTP_hostname,SMTP_username: SMTP_username,SMTP_password: SMTP_password,SMTP_port: SMTP_port,testemail: testemail},
        success: function(data) {
            if (data.error) {
			    PNotify.removeAll();
			    new PNotify({title: 'Error',text: data.error,type: 'error'});
            } else {
			    $('#testMail').modal('toggle');
				PNotify.removeAll();
				new PNotify({title: 'Success',text: 'Mail Sent Successfully',type: 'success'});
			}
			
        },
      });
    
});
/*-------------------------- Send test mail ends here ---------------------------------------------------*/


/*-------------------------- error message starts here---------------------------------------------------*/
<?php if(validation_errors()!='' || $error!='') { ?>
new PNotify({
	  title: 'Error',
	  text: <?php echo json_encode(validation_errors().$error)?>,
	  type: 'error'
	});
<?php } ?>
/*-------------------------- error message ends here---------------------------------------------------*/
/*-------------------------- success message starts here---------------------------------------------------*/
<?php if($this->session->userdata('success_msg')) { ?>
new PNotify({
	  title: 'Success',
	  text: '<?php echo $this->session->userdata('success_msg')?>',
	  type: 'success'
	});
<?php $this->session->unset_userdata('success_msg'); } ?>
/*-------------------------- success message ends here---------------------------------------------------*/
</script>
