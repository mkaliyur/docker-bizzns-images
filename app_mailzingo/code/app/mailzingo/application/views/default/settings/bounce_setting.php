<!--- Tool Tip Code--->
<script>
$(document).ready(function(){
    $('[data-toggle="tooltip"]').tooltip();
});
</script>

<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padding15 footer-height"> 
  <!-- dash header-->
  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 mb2 xsmb4">
    <div class="dash-header">
      <div class="row">
        <div class="col-md-12 col-sm-12">
          <h1 class="smart-color em22 smem18 xsem16">Settings</h1>
          <p class="em11 smem10 xsem10">Manage your settings here to automate your mailing process</p>
        </div>
      </div>
    </div>
  </div>
  <!-- dash header end-->
  
  <div class="clearfix"></div>
  <div class="col-md-12 smart-tabs clearfix">
    <ul>
      <li><a href="<?php echo site_url('settings/user')?>" class="<?php if($page=='user'){echo 'active';} ?>" title="User Settings"> <span class="hidden-xs">User Settings</span><i class="icon-User-Settings visible-xs"></i> </a></li>
      <li><a href="<?php echo site_url('settings/app')?>" title="App Setting" class="<?php if($page=='app'){echo 'active';} ?>" > <span class="hidden-xs">App Setting</span><i class="icon-App-Settings visible-xs"></i> </a></li>
      <li><a href="<?php echo site_url('settings/mail')?>" title="Mail Settings" class="<?php if($page=='mail'){echo 'active';} ?>" > <span class="hidden-xs">Mail Settings</span><i class="icon-Mail-Settings visible-xs"></i> </a></li>
      <li><a href="<?php echo site_url('settings/bounce')?>" title="Bounce Settings" class="<?php if($page=='bounce'){echo 'active';} ?>" > <span class="hidden-xs">Bounce Settings</span><i class="icon-Bounce-Settings visible-xs"></i> </a></li>
      <li><a href="<?php echo site_url('settings/cron')?>" title="Cron Settings" class="<?php if($page=='cron'){echo 'active';} ?>" > <span class="hidden-xs">Cron Settings</span><i class="icon-Corn-Seting visible-xs"></i> </a></li>
	   <li><a href="<?php echo site_url('settings/change-password')?>" title="Change Password" class="<?php if($page=='change_password'){echo 'active';} ?>" > <span class="hidden-xs">Change password</span><i class="icon-change-password visible-xs"></i> </a></li>
		<li><a href="<?php echo site_url('settings/social-settings')?>" title="Social Settings" class="<?php if($page=='social_settings'){echo 'active';} ?>" > <span class="hidden-xs">Social Settings</span><i class="icon-social-setting visible-xs"></i> </a></li>
		<li><a href="<?php echo site_url('settings/api-settings')?>" title="API Settings" class="<?php if($page=='api_settings'){echo 'active';} ?>" > <span class="hidden-xs">API Settings</span><i class="icon-social-setting visible-xs"></i> </a></li>
	 <!--<li><a href="<?php echo site_url('settings/label')?>" title="Label Settings" class="<?php if($page=='label'){echo 'active';} ?>" >
<span class="hidden-xs">Label Settings</span><i class="icon-Label-Settings visible-xs"></i>
</a></li> -->
    </ul>
  </div>
  <div class="col-md-12 col-sm-12 col-xs-12 mb2 xsmb4">
    <form action="" method="post" id="bounce_form">
      <div class="layout-box clearfix padding0 smart-field">
        <div class="col-md-12 mt2 padding0">
          <div class="row">
            <div class="col-md-8 col-xs-12 col-sm-12">
              <div class="col-xs-12 padding0">
                <div class="row">
                  <div class="col-md-12 col-sm-12 col-xs-12 padding0 mb2 xsmb3 clearfix">
                    <div class="col-md-3 col-sm-4 col-xs-12 xsmb2 em10 smem10 xsem10 mt1 xsmt2">Bounce Setting</div>
                    <div class="col-md-6 col-sm-6 col-xs-10 em10 smem10 xsem9">
                      <div class="mb2">
                        <input type="radio" id="default1" <?php if($bounce_type=='default'){echo 'checked';} ?> name="bounce_type" value="default">
                        &nbsp;&nbsp; Use My Default Bounce Mail</div>
                      <input type="radio" id="custom1" name="bounce_type" <?php if($bounce_type=='custom'){echo 'checked';} ?> value="custom">
                      &nbsp;&nbsp; Let Me Specify My Own Bounce Details </div>
                    <div class="col-md-1 col-sm-1 col-xs-2"> <a href="#" data-toggle="tooltip" data-placement="top" class="red-tooltip visible-xs visible-sm" title="By default bounce mail will be returned to from email"><i class="fa fa-info-circle smart-grey em11" aria-hidden="true"></i></a> <a href="#" data-toggle="tooltip" data-placement="right" class="red-tooltip hidden-sm hidden-xs" title="By default bounce mail will be returned to from email"><i class="fa fa-info-circle smart-grey em11" aria-hidden="true"></i></a> </div>
                  </div>
                  <div class="col-md-12 col-sm-12 col-xs-12 padding0 mb2 xsmb3 clearfix" id="address_div">
                    <div class="col-md-3 col-sm-4 col-xs-12 xsmb2 em10 smem10 xsem10 mt1 xsmt2">Email Address </div>
                    <div class="col-md-6 col-sm-6 col-xs-10">
                      <input name="bounce_address" type="text" value="<?php echo $bounce_address; ?>" placeholder="Bounce Address">
                    </div>
                    <div class="col-md-1 col-sm-1 col-xs-2 mt1 xsmt4"> <a href="#" data-toggle="tooltip" data-placement="top" class="red-tooltip visible-xs visible-sm" title="This is the email address where emails will returned if they bounced"><i class="fa fa-info-circle smart-grey em11" aria-hidden="true"></i></a> <a href="#" data-toggle="tooltip" data-placement="right" class="red-tooltip hidden-sm hidden-xs" title="This is the email address where emails will returned if they bounced"><i class="fa fa-info-circle smart-grey em11" aria-hidden="true"></i></a> </div>
                  </div>
                  <div class="col-md-12 col-sm-12 col-xs-12 padding0 mb2 xsmb3 clearfix" id="server_div">
                    <div class="col-md-3 col-sm-4 col-xs-12 xsmb2 em10 smem10 xsem10 mt1 xsmt2">Server</div>
                    <div class="col-md-6 col-sm-6 col-xs-10">
                      <input name="bounce_server" type="text" value="<?php echo $bounce_server; ?>" placeholder="Bounce Server">
                    </div>
                    <div class="col-md-1 col-sm-1 col-xs-2 mt1 xsmt4"> <a href="#" data-toggle="tooltip" data-placement="top" class="red-tooltip visible-xs visible-sm" title="Email server name for bounce process"><i class="fa fa-info-circle smart-grey em11" aria-hidden="true"></i></a> <a href="#" data-toggle="tooltip" data-placement="right" class="red-tooltip hidden-sm hidden-xs" title="Email server name for bounce process"><i class="fa fa-info-circle smart-grey em11" aria-hidden="true"></i></a> </div>
                  </div>
                  <div class="col-md-12 col-sm-12 col-xs-12 padding0 mb2 xsmb3 clearfix" id="pass_div">
                    <div class="col-md-3 col-sm-4 col-xs-12 xsmb2 em10 smem10 xsem10 mt1 xsmt2">Email Password</div>
                    <div class="col-md-6 col-sm-6 col-xs-10">
                      <input name="bounce_password" type="Password" value="<?php echo $bounce_password; ?>" placeholder=" Bounce Password">
                    </div>
                    <div class="col-md-1 col-sm-1 col-xs-2 mt1 xsmt4"> <a href="#" data-toggle="tooltip" class="red-tooltip visible-xs visible-sm" data-placement="top" title="Enter bounce password for bounce process"><i class="fa fa-info-circle smart-grey em11" aria-hidden="true"></i></a> <a href="#" data-toggle="tooltip" class="red-tooltip hidden-sm hidden-xs" data-placement="right" title="Enter bounce password for bounce process"><i class="fa fa-info-circle smart-grey em11" aria-hidden="true"></i></a> </div>
                  </div>
                  <div class="col-md-12 col-sm-12 col-xs-12 padding0 mb2 xsmb3 clearfix" id="address_div">
                    <div class="col-md-3 col-sm-4 col-xs-12 xsmb2 em10 smem10 xsem10 mt1 xsmt2">Port</div>
                    <div class="col-md-6 col-sm-6 col-xs-10">
                      <input name="bounce_port" type="text" value="<?php echo $bounce_port; ?>" placeholder="Bounce Port">
                    </div>
                    <div class="col-md-1 col-sm-1 col-xs-2 mt1 xsmt4"> <a href="#" data-toggle="tooltip" data-placement="top" class="red-tooltip visible-xs visible-sm" title="This is server port."><i class="fa fa-info-circle smart-grey em11" aria-hidden="true"></i></a> <a href="#" data-toggle="tooltip" data-placement="right" class="red-tooltip hidden-sm hidden-xs" title="This is server port."><i class="fa fa-info-circle smart-grey em11" aria-hidden="true"></i></a> </div>
                  </div>
                  <div class="col-md-12 col-sm-12 col-xs-12 padding0 mb1 xsmb3 clearfix" id="account_div">
                    <div class="col-md-3 col-sm-4 col-xs-12 xsmb2 em10 smem10 xsem10 mt1 xsmt2">Account Type</div>
                    <div class="col-md-6 col-sm-6 col-xs-10 mt1 xsmt1">
					<input type="hidden" name="account_type" value="imap" />
                      IMAP Account
                    </div>
                    <div class="col-md-1 col-sm-1 col-xs-2 mt1 xsmt4"> <a href="#" data-toggle="tooltip" class="red-tooltip visible-xs visible-sm" data-placement="top" title="Select email account type for bounce process"><i class="fa fa-info-circle smart-grey em11" aria-hidden="true"></i></a> <a href="#" data-toggle="tooltip" class="red-tooltip hidden-sm hidden-xs" data-placement="right" title="Enable IMAP serverice for your account"><i class="fa fa-info-circle smart-grey em11" aria-hidden="true"></i></a> </div>
                  </div>
                  
                  <!--<div class="col-md-12 col-sm-12 col-xs-12 padding0 mb2 xsmb3 clearfix" id="test_div">
<div class="col-md-4 col-sm-6 col-xs-10 col-md-offset-3 col-sm-offset-4 text-right">
<button type="button" class="suppression-grey-btn">Test Bounce </button></div>
</div>--> 
                  
                </div>
              </div>
            </div>
            
            <div class="col-md-4 col-xs-12 col-sm-12 mt0 xsmt6">
              <p class="w600">Cron setup for bounce emails</p>
              <textarea name="" cols="" rows="10" class="jstextarea" id="croncopycode"  readonly="readonly">
/usr/bin/curl <?php echo site_url('check-bounce')?>
</textarea>
              <div class="row">
                <div class="col-md-6 col-sm-6 mt2 xsmt3">
                  <p class="em9 smem9 xsem9">Copy code and set cron for bounce emails</p>
                </div>
                <div class="col-md-6 col-sm-6 text-right mt2 xsmt0 mb0 xsmb5">
                <a href="javascript:" class="smart-btn" data-copytarget="#croncopycode" title="Copy">Copy</a></div>
              </div>
            </div>
            <div class="clearfix"></div>
          </div>
        </div>
      </div>
      <div class="col-xs-12 xstext-right xstext-center mt2 xsmt8 mb0 padding0"> <a href="javascript:" onclick="javascript: document.getElementById('bounce_form').submit();" class="smart-btn" title="Save">Save</a> </div>
    </form>
  </div>
</div>
<script>
$(document).ready(function(){
  
    if ($("#default1:checked").val()) {
      $("#address_div,#server_div,#user_div,#pass_div,#account_div,#test_div").hide()
        return false;
    }
    else {
      $("#address_div,#server_div,#user_div,#pass_div,#account_div,#test_div").show()
    }
  
}); 

$('#default1').click(function() {
    $("#address_div,#server_div,#user_div,#pass_div,#account_div,#test_div").hide()
});
$('#custom1').click(function() {
    $("#address_div,#server_div,#user_div,#pass_div,#account_div,#test_div").show()
});

/*-------------------------- error message starts here---------------------------------------------------*/
<?php if(validation_errors()!='' || $errormsg!='') { ?>
new PNotify({
	  title: 'Error',
	  text: <?php echo json_encode(validation_errors().$errormsg)?>,
	  type: 'error'
	});
<?php } ?>
/*-------------------------- error message ends here---------------------------------------------------*/
/*-------------------------- success message starts here---------------------------------------------------*/
<?php if($this->session->userdata('success_msg')) { ?>
new PNotify({
	  title: 'Success',
	  text: '<?php echo $this->session->userdata('success_msg')?>',
	  type: 'success'
	});
<?php $this->session->unset_userdata('success_msg'); } ?>
/*-------------------------- success message ends here---------------------------------------------------*/


/*
  Copy text from any appropriate field to the clipboard
  By Craig Buckler, @craigbuckler
  use it, abuse it, do whatever you like with it!
*/
(function() {

    'use strict';
  
  // click events
  document.body.addEventListener('click', copy, true);

    // event handler
    function copy(e) {

    // find target element
    var 
      t = e.target,
      c = t.dataset.copytarget,
      inp = (c ? document.querySelector(c) : null);
      
    // is element selectable?
    if (inp && inp.select) {
      
      // select text
      inp.select();

      try {
        // copy text
        document.execCommand('copy');
        inp.blur();
		PNotify.removeAll();
		new PNotify({title: 'Success',text: 'Copied Successfully', type: 'success'});
        
      }
      catch (err) {
        alert('please press Ctrl/Cmd+C to copy');
      }
      
    }
    
    }

})();
</script>