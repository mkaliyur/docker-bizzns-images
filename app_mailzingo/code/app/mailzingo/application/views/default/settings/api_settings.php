<!--- Tool Tip Code--->
<script>
$(document).ready(function(){
    $('[data-toggle="tooltip"]').tooltip();
});
</script>

<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padding15 footer-height">
  <!-- dash header-->
  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 mb2 xsmb4">
    <div class="dash-header">
      <div class="row">
        <div class="col-md-12 col-sm-12">
          <h1 class="smart-color em22 smem18 xsem16">Settings</h1>
          <p class="em11 smem10 xsem10">Manage your settings here to automate your mailing process</p>
        </div>
      </div>
    </div>
  </div>
  <!-- dash header end-->
  <div class="clearfix"></div>
  <div class="col-md-12 smart-tabs clearfix">
    <ul>
	 
      <li><a href="<?php echo site_url('settings/user')?>" class="<?php if($page=='user'){echo 'active';} ?>" title="User Settings"> <span class="hidden-xs">User Settings</span><i class="icon-User-Settings visible-xs"></i> </a></li>
      <li><a href="<?php echo site_url('settings/app')?>" title="App Setting" class="<?php if($page=='app'){echo 'active';} ?>" > <span class="hidden-xs">App Setting</span><i class="icon-App-Settings visible-xs"></i> </a></li>
      <li><a href="<?php echo site_url('settings/mail')?>" title="Mail Settings" class="<?php if($page=='mail'){echo 'active';} ?>" > <span class="hidden-xs">Mail Settings</span><i class="icon-Mail-Settings visible-xs"></i> </a></li>
      <li><a href="<?php echo site_url('settings/bounce')?>" title="Bounce Settings" class="<?php if($page=='bounce'){echo 'active';} ?>" > <span class="hidden-xs">Bounce Settings</span><i class="icon-Bounce-Settings visible-xs"></i> </a></li>
      <li><a href="<?php echo site_url('settings/cron')?>" title="Cron Settings" class="<?php if($page=='cron'){echo 'active';} ?>" > <span class="hidden-xs">Cron Settings</span><i class="icon-Corn-Seting visible-xs"></i> </a></li>
	   <li><a href="<?php echo site_url('settings/change-password')?>" title="Change Password" class="<?php if($page=='change_password'){echo 'active';} ?>" > <span class="hidden-xs">Change password</span><i class="icon-change-password visible-xs"></i> </a></li>
		<li><a href="<?php echo site_url('settings/social-settings')?>" title="Social Settings" class="<?php if($page=='social_settings'){echo 'active';} ?>" > <span class="hidden-xs">Social Settings</span><i class="icon-social-setting visible-xs"></i> </a></li>
		<li><a href="<?php echo site_url('settings/api-settings')?>" title="API Settings" class="<?php if($page=='api_settings'){echo 'active';} ?>" > <span class="hidden-xs">API Settings</span><i class="icon-api visible-xs"></i> </a></li>
        
	<!-- <li><a href="<?php echo site_url('settings/label')?>" title="Label Settings" class="<?php if($page=='label'){echo 'active';} ?>" > <span class="hidden-xs">Label Settings</span><i class="icon-Label-Settings visible-xs"></i> </a></li> -->
    </ul>
  </div>
  <div class="col-md-12 col-sm-12 col-xs-12 mb2 xsmb4">
<form method="post" action="" id="app_form">
      <div class="layout-box clearfix padding0 smart-field">
        <div class="col-xs-12 mt2 padding0">
          <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12 padding0 mb2 xsmb3 clearfix">
              <div class="col-md-2 col-sm-2 col-xs-12 xsmb2 em10 smem10 xsem10 mt1 xsmt2">API Key</div>
              <div class="col-md-4 col-sm-5 col-xs-12">
                <input name="api_key" value="<?=$api_key?>" placeholder="egweggkejbjk252n2jklsnbfkjfb3jk45b4fbjkebfgrgr2332efwesvvd" type="text" id="keycopy">
              </div>
              <div class="col-md-6 col-sm-5 col-xs-12 mt0 xsmt2"> 
              <?php if($api_key=='') { ?>
              <input type="submit" name="generate" value="Generate" class="reset-btn"/>
              <?php } else { ?>
              <input type="submit" name="reset" value="Reset" class="reset-btn"/>
              <?php } ?>
              &nbsp;&nbsp;
              <a href="javascript:" class="generate-btn"  data-copytarget="#keycopy">Copy</a>
              </div>
            </div>
            <div class="col-md-12 col-sm-12 col-xs-12 padding0 mb2 xsmb3 clearfix">
              <div class="col-md-2 col-sm-2 col-xs-12 xsmb2 em10 smem10 xsem10 mt1 xsmt2">URL</div>
              <div class="col-md-4 col-sm-5 col-xs-12">
                <input name="api_url" value="<?=$api_url?>" placeholder="<?=$api_url?>" type="text" readonly="readonly" id="linkcopy">
              </div>
              <div class="col-md-6 col-sm-5 col-xs-12 mt0 xsmt2">  <a href="javascript:" class="generate-btn"  data-copytarget="#linkcopy">Copy</a> </div>
            </div>
            
          </div>
          <div class="clearfix"></div>
        </div>
      </div>
      
    </form>
</div>

<div class="col-md-12 col-sm-12 col-xs-12 mb2 xsmb4">
<p class="em11 smem10 xsem10 w600">API Instructions</p>

    <div class="panel-group mrgn-left" id="accordion">
        <div class="panel panel-default api-format">
            <li class="heading-title">
            <a class="accordion-toggle em11 smem10 xsem10" data-toggle="collapse" data-parent="#accordion" href="#collapseOne">
            How to use
            </a>
            </li>
            <div id="collapseOne" class="panel-collapse collapse in">
            <p class="em9 smem9 xsem10">
            We accept both GET and POST method. Every request must have api key in api_key named field.
            </p>
            </div>
        </div>
        <div class="panel panel-default api-format">
            <li class="heading-title">
            <a class="accordion-toggle em11 smem10 xsem10" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo">
            Authentication check
            </a>
            </li>
            <div id="collapseTwo" class="panel-collapse collapse">
            <p class="em9 smem9 xsem10">
            For check api key authentication you have to send request to given api url in which <b>api_key</b> must be there and if you want to check authentication you have to send <b>authentication_check</b> value in action field.<br />
             <b>Example :- </b>
             <pre>
             &lt;form action="<?=$api_url?>" &gt;
                &lt;input name="action" value="authentication_check"&gt;
                &lt;input name="api_key" value="<?=$api_key?>"&gt;
                &lt;input type="submit" value="submit"&gt;
             &lt;/form&gt;
            </pre>
            
            </p>
            </div>
        </div>
        <div class="panel panel-default api-format">
            <li class="heading-title">
            <a class="accordion-toggle em11 smem10 xsem10" data-toggle="collapse" data-parent="#accordion" href="#collapseThree">
            Campaign List
            </a>
            </li>
            <div id="collapseThree" class="panel-collapse collapse">
            <p class="em9 smem9 xsem10">
            Get all campaign list you have to send request to given api url in which <b>api_key</b> must be there and if you want to list campaigns you have to send <b>campaign_list</b> value in action field.
            <b>Example :- </b>
             <pre>
             &lt;form action="<?=$api_url?>" &gt;
                &lt;input name="action" value="campaign_list"&gt;
                &lt;input name="api_key" value="<?=$api_key?>"&gt;
                &lt;input type="submit" value="submit"&gt;
             &lt;/form&gt;
            </pre>
            </p>
            </div>
        </div>
        <div class="panel panel-default api-format">
            <li class="heading-title">
            <a class="accordion-toggle em11 smem10 xsem10" data-toggle="collapse" data-parent="#accordion" href="#collapseFour">
            Add Subscriber
            </a>
            </li>
            <div id="collapseFour" class="panel-collapse collapse">
            <p class="em9 smem9 xsem10">
            Add subscriber in any campaign there must be <b>campaign_id</b> in request with action value <b>add_subscriber</b>.
            you can add given field in subscriber add option <br />
            campaign_id (required)<br />
            email (required)<br />
            name (optional)<br />
            address (optional)<br />
            city (optional)<br />
            state (optional)<br />
            country (optional)<br />
            zip (optional)<br />
            phone (optional)<br />
            <b>Example :- </b>
             <pre>
             &lt;form action="<?=$api_url?>" &gt;
                &lt;input name="action" value="add_subscriber"&gt;
                &lt;input name="campaign_id" value="{required}"&gt;
                &lt;input name="api_key" value="<?=$api_key?>"&gt;
                &lt;input name="email" value="yourmail@domain.com"&gt;
                &lt;input name="name" value="Your Name"&gt;
                &lt;input type="submit" value="submit"&gt;
             &lt;/form&gt;
            </pre>
            </p>
            </div>
        </div>
    </div>

</div>
</div>
<script>
/*-------------------------- success message starts here---------------------------------------------------*/
<?php if($this->session->userdata('success_msg')) { ?>
new PNotify({
	  title: 'Success',
	  text: '<?php echo $this->session->userdata('success_msg')?>',
	  type: 'success'
	});
<?php $this->session->unset_userdata('success_msg'); } ?>
/*-------------------------- success message ends here---------------------------------------------------*/
/*-------------------------- error message starts here---------------------------------------------------*/
<?php if(validation_errors()!='' || $error!='') { ?>
new PNotify({
	  title: 'Error',
	  text: <?php echo json_encode(validation_errors())?>,
	  type: 'error'
	});
<?php } ?>
/*-------------------------- error message ends here---------------------------------------------------*/

/*
  Copy text from any appropriate field to the clipboard
  By Craig Buckler, @craigbuckler
  use it, abuse it, do whatever you like with it!
*/
(function() {

    'use strict';
  
  // click events
  document.body.addEventListener('click', copy, true);

    // event handler
    function copy(e) {

    // find target element
    var 
      t = e.target,
      c = t.dataset.copytarget,
      inp = (c ? document.querySelector(c) : null);
      
    // is element selectable?
    if (inp && inp.select) {
      
      // select text
      inp.select();

      try {
        // copy text
        document.execCommand('copy');
        inp.blur();
		PNotify.removeAll();
		new PNotify({title: 'Success',text: 'Copied Successfully', type: 'success'});
        
      }
      catch (err) {
        alert('please press Ctrl/Cmd+C to copy');
      }
      
    }
    
    }

})();
</script>