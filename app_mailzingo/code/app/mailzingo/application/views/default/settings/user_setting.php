<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padding15 footer-height">
  <!-- dash header-->
  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 mb2 xsmb4">
    <div class="dash-header">
      <div class="row">
        <div class="col-md-12 col-sm-12">
          <h1 class="smart-color em22 smem18 xsem16">Settings</h1>
          <p class="em11 smem10 xsem10">Manage your settings here to automate your mailing process</p>
        </div>
      </div>
    </div>
  </div>
  <!-- dash header end-->
  <div class="clearfix"></div>
  <div class="col-md-12 smart-tabs clearfix">
    <ul>
      <li><a href="<?php echo site_url('settings/user')?>" class="<?php if($page=='user'){echo 'active';} ?>" title="User Settings"> <span class="hidden-xs">User Settings</span><i class="icon-User-Settings visible-xs"></i> </a></li>
      <li><a href="<?php echo site_url('settings/app')?>" title="App Setting" class="<?php if($page=='app'){echo 'active';} ?>" > <span class="hidden-xs">App Setting</span><i class="icon-App-Settings visible-xs"></i> </a></li>
      <li><a href="<?php echo site_url('settings/mail')?>" title="Mail Settings" class="<?php if($page=='mail'){echo 'active';} ?>" > <span class="hidden-xs">Mail Settings</span><i class="icon-Mail-Settings visible-xs"></i> </a></li>
      <li><a href="<?php echo site_url('settings/bounce')?>" title="Bounce Settings" class="<?php if($page=='bounce'){echo 'active';} ?>" > <span class="hidden-xs">Bounce Settings</span><i class="icon-Bounce-Settings visible-xs"></i> </a></li>
      <li><a href="<?php echo site_url('settings/cron')?>" title="Cron Settings" class="<?php if($page=='cron'){echo 'active';} ?>" > <span class="hidden-xs">Cron Settings</span><i class="icon-Corn-Seting visible-xs"></i> </a></li>
	   <li><a href="<?php echo site_url('settings/change-password')?>" title="Change Password" class="<?php if($page=='change_password'){echo 'active';} ?>" > <span class="hidden-xs">Change password</span><i class="icon-change-password visible-xs"></i> </a></li>
      <li><a href="<?php echo site_url('settings/social-settings')?>" title="Social Settings" class="<?php if($page=='social_settings'){echo 'active';} ?>" > <span class="hidden-xs">Social Settings</span><i class="icon-social-setting visible-xs"></i> </a></li>
		<li><a href="<?php echo site_url('settings/api-settings')?>" title="API Settings" class="<?php if($page=='api_settings'){echo 'active';} ?>" > <span class="hidden-xs">API Settings</span><i class="icon-social-setting visible-xs"></i> </a></li>
	  <!--<li><a href="<?php echo site_url('settings/label')?>" title="Label Settings" class="<?php if($page=='label'){echo 'active';} ?>" > <span class="hidden-xs">Label Settings</span><i class="icon-Label-Settings visible-xs"></i> </a></li> -->
    </ul>
  </div>
  <div class="col-md-12 col-sm-12 col-xs-12 mb2 xsmb4">
    <form action="" method="post" enctype="multipart/form-data" >
      <div class="layout-box clearfix padding0 smart-field">
        <div class="col-xs-12 mt2 padding0">
          <div class="row">
            <div class="col-md-6 col-sm-6 col-xs-12 padding0 mb2 xsmb3 clearfix">
              <div class="col-md-4 col-sm-4 col-xs-12 xsmb2 em10 smem10 xsem10 mt1 xsmt2">Name</div>
              <div class="col-md-6 col-sm-8 col-xs-12">
                <input name="name" type="text" value="<?php echo $name?>" placeholder="Name">
              </div>
            </div>
            <div class="col-md-6 col-sm-6 col-xs-12 padding0 mb2 xsmb3 clearfix">
              <div class="col-md-4 col-sm-4 col-xs-12 xsmb2 em10 smem10 xsem10 mt1 xsmt2">Country</div>
              <div class="col-md-6 col-sm-8 col-xs-12">
                <input name="country" type="text" value="<?php echo $country; ?>" placeholder="Country">
              </div>
            </div>
            <div class="col-md-6 col-sm-6 col-xs-12 padding0 mb2 xsmb3 clearfix">
              <div class="col-md-4 col-sm-4 col-xs-12 xsmb2 em10 smem10 xsem10 mt1 xsmt2">Address</div>
              <div class="col-md-6 col-sm-8 col-xs-12">
                <input name="address" type="text" value="<?php echo $address; ?>" placeholder="Address">
              </div>
            </div>
            <div class="col-md-6 col-sm-6 col-xs-12 padding0 mb2 xsmb3 clearfix">
              <div class="col-md-4 col-sm-4 col-xs-12 xsmb2 em10 smem10 xsem10 mt1 xsmt2">State</div>
              <div class="col-md-6 col-sm-8 col-xs-12">
                <input name="state" type="text" value="<?php echo $state; ?>" placeholder="State">
              </div>
            </div>
            <div class="col-md-6 col-sm-6 col-xs-12 padding0 mb2 xsmb3 clearfix">
              <div class="col-md-4 col-sm-4 col-xs-12 xsmb2 em10 smem10 xsem10 mt1 xsmt2">Post/Zip Code</div>
              <div class="col-md-6 col-sm-8 col-xs-12">
                <input name="pin_code" type="text" value="<?php echo $pin_code; ?>" placeholder="Pin Code">
              </div>
            </div>
            <div class="col-md-6 col-sm-6 col-xs-12 padding0 mb2 xsmb3">
              <div class="col-md-4 col-sm-4 col-xs-12 xsmb2 em10 smem10 xsem10 mt1 xsmt2">City</div>
              <div class="col-md-6 col-sm-8 col-xs-12">
                <input name="city" type="text" value="<?php echo $city; ?>" placeholder="City">
              </div>
            </div>
              <div class="col-md-6 col-sm-6 col-xs-12 padding0 mb2 xsmb3">
              <div class="col-md-4 col-sm-4 col-xs-12 xsmb2 em10 smem10 xsem10 mt1 xsmt2">Business Name</div>
              <div class="col-md-6 col-sm-8 col-xs-12">
                <input name="business_name" type="text" value="<?php echo $business_name; ?>" placeholder="Business Name">
              </div>
            </div>
            <!--<div class="col-md-6 col-sm-6 col-xs-12 padding0 mb2 xsmb3 clearfix">
              <div class="col-md-4 col-sm-4 col-xs-12 xsmb2 em10 smem10 xsem10 mt1 xsmt2">Time Zone</div>
              <div class="col-md-6 col-sm-8 col-xs-12">
                <select name="time_zone">
                  <option <?php if($time_zone=='delhi'){echo 'selected'; } ;  ?> value="delhi" >Delhi</option>
                  <option <?php if($time_zone=='chennai'){echo 'selected'; } ;  ?> value="chennai" >Chennai</option>
                </select>
              </div>
            </div> -->
            <div class="col-md-6 col-sm-6 col-xs-12 padding0 mb2 xsmb3" clearfix>
              <div class="col-md-4 col-sm-4 col-xs-12 xsmb2 em10 smem10 xsem10 mt1 xsmt2">Profile Pic</div>
              <div class="col-md-6 col-sm-8 col-xs-12">
                <input name="pic" type="file" value="" >
                <p class="em9 smem9 xsem9 mt3 xsmt3 smart-color">Allowed File Types: gif, jpg, png, jpeg  </p>
              </div>
            </div>
            <div class="col-xs-12 padding0 mb2 xsmb3" clearfix>
             <div class="col-md-2 col-sm-4 col-xs-12 col-md-offset-8 col-sm-offset-8">
                <div class="layout-border"><img src="<?php if($profile_pic){echo site_url('assets/uploads/pics/profile/'.$profile_pic); }else{ echo $this->config->item('templateAssetsPath').'images/avatar.png'; } ?>" class="img-responsive center-block" /></div>
              </div>
           
            </div>
                     
          </div>
          <div class="clearfix"></div>
        </div>
      </div>
      <div class="col-xs-12 xstext-right xstext-center mt2 xsmt8 xsmb5 mb0 padding0">
        <button type="submit" name="user_form" value="user_form" class="smart-btn" title="Save">Save</button>
      </div>
    </form>
    <div class="mt6 mb2 xsmb3 w600 em11 smem11 xsem11 padding0">From And Reply Email</div>
    <div class="table-responsive table-bordered table-data table-fixed-header user-setting-coloum">
      <table width="100%" border="0" cellspacing="0" cellpadding="0">
      <thead> 
      <tr class="w600">
          <th>Email Address </th>
          <th class="text-center">Status</th>
          <th class="text-center">Action</th>
        </tr>
        </thead>
        <tbody>
        <?php foreach ($mail_details as $mail){  ?>
        <tr class="text-center">
          <td class="text-left"><?php echo $mail->email; ?></td>
          <td><?php echo ($mail->status=='varify'?'Verify':'Unverify')?></td>
          <td class="table-icon"><ul>
              <li><a data-toggle="modal" data-target="#delete" class="deletelink" data-href="<?php echo site_url('delete-mail/'.$mail->id)?>" ><i class="fa fa-trash-o em12" aria-hidden="true"></i></a></li>
            </ul></td>
        </tr>
        <?php } ?></tbody>
      </table>
    </div>
    
    <div class="col-xs-12 padding0 mt2 xsmt4">
      <div class="layout-box clearfix">
        <div class="col-xs-12 padding0">
          <div class="row">
            <form method="post" action="" id="email_form1">
              <div class="col-md-6 col-sm-6 col-xs-12 smart-field">
                <input name="mail" type="email" placeholder="email@sitename.com" />
              </div>
              <div class="col-md-4 col-sm-3 col-xs-12 xsmt4 mt0">
                <button type="submit" name="email_submit" value="email_submit" class="smart-btn" title="Verify Email" >Verify Email</button>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<script>
/*-------------------------- delete single mail starts here ------------------------------------*/
$('body').delegate(".deletelink", 'click', function() {
    
	deleteurl = $(this).attr('data-href');
	 $('#delete_yes_button').attr('href',deleteurl);
	 $('#delete_yes_button').removeClass('deleteMultiple');
});
/*-------------------------- delete single mail ends here ------------------------------------*/

/*-------------------------- success message starts here---------------------------------------------------*/
<?php if($this->session->userdata('success_msg')) { ?>
new PNotify({
	  title: 'Success',
	  text: '<?php echo $this->session->userdata('success_msg')?>',
	  type: 'success'
	});
<?php $this->session->unset_userdata('success_msg'); } ?>
/*-------------------------- success message ends here---------------------------------------------------*/

/*-------------------------- error message starts here---------------------------------------------------*/
<?php if(validation_errors()!='' || $error!='') { ?>
new PNotify({
	  title: 'Error',
	  text: <?php echo json_encode(validation_errors().$error)?>,
	  type: 'error'
	});
<?php } ?>
<?php if($this->session->userdata('error_msg')) { ?>
new PNotify({
	  title: 'Error',
	  text: '<?php echo $this->session->userdata('error_msg')?>',
	  type: 'error'
	});
<?php $this->session->unset_userdata('error_msg'); } ?>
/*-------------------------- error message ends here---------------------------------------------------*/

</script>
