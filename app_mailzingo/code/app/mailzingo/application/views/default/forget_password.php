<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title><?php echo $this->config->item('site_title')?></title>

<!-- Tell the browser to be responsive to screen width -->
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
<!-- fonts--><link rel="icon" href="<?php echo $this->config->item('templateAssetsPath')?>images/favicon-icon.png" type="image/png"/>
<link rel="stylesheet" href="<?php echo $this->config->item('templateAssetsPath')?>css/font-awesome.min.css" type="text/css" />
<link href='https://fonts.googleapis.com/css?family=Roboto:300,400,600,800' rel='stylesheet' type='text/css'>
<link href='https://fonts.googleapis.com/css?family=Raleway:400,600,800' rel='stylesheet' type='text/css'>
<link rel="stylesheet" href="<?php echo $this->config->item('templateAssetsPath')?>css/bootstrap.min.css" type="text/css" />
<!-- left menu-->
<link rel="stylesheet" href="<?php echo $this->config->item('templateAssetsPath')?>css/left_menu.css" type="text/css" />
<!-- smart mailer design css-->
<link rel="stylesheet" href="<?php echo $this->config->item('templateAssetsPath')?>css/style.css" type="text/css" />
<!-- scroll css-->
<link rel="stylesheet" href="<?php echo $this->config->item('templateAssetsPath')?>css/CustomScrollbar.css" type="text/css" />
<script type='text/javascript' src="<?php echo $this->config->item('templateAssetsPath')?>js/jquery.min.js"></script>
<script type='text/javascript' src="<?php echo $this->config->item('templateAssetsPath')?>js/bootstrap.min.js"></script>
<!-- scroll js-->
<script type='text/javascript' src="<?php echo $this->config->item('templateAssetsPath')?>js/CustomScrollbar.min.js"></script>
<!-- left menu-->
<script type='text/javascript' src="<?php echo $this->config->item('templateAssetsPath')?>js/app.js"></script>
<!-- image lighbox css and js-->
<link rel="stylesheet" href="<?php echo $this->config->item('templateAssetsPath')?>css/simplelightbox.css" type="text/css" />
<script type='text/javascript' src="<?php echo $this->config->item('templateAssetsPath')?>js/simple-lightbox.js"></script>
<script type="text/javascript" src="<?php echo $this->config->item('templateAssetsPath')?>js/notify/pnotify.core.js"></script>
<script type="text/javascript" src="<?php echo $this->config->item('templateAssetsPath')?>js/notify/pnotify.buttons.js"></script>

</head>
<body>
<div class="login-bg">
<div class="container">
<div class="smart-login">
<div align="center">
<img src="<?php echo $this->config->item('templateAssetsPath')?>images/smart_logo.png" class="login-logo img-responsive" alt="smart mailer" /></div>
<div class="login-box">
<?php if($error_msg) { ?>
	  <span class="text-danger"><?php echo $error_msg?></span>
	  <?php } ?>
<form method="post" action="" role="login">
                    <div class="form-group">
                        <label class="w300">Email</label>
                        <input type="email" name="email" class="form-control" placeholder="Enter Your Email " value="<?php echo $email?>" />
                    </div>

                    <p class="text-right"><a href="<?php echo site_url('login'); ?>">Login</a></p>
                                    <input type="submit" name="go" value="SUBMIT" class="btn btn-block w600">
</form>
</div>
</div>
</div>
</div>
    <script>
        /*-------------------------- success message starts here---------------------------------------------------*/
        $(document).ready(function() {
<?php if($this->session->userdata('success_msg')) { ?>
new PNotify({
	  title: 'Success',
	  text: '<?php echo $this->session->userdata('success_msg'); ?>',
	  type: 'success'
	});
<?php $this->session->unset_userdata('success_msg'); } ?>
});
/*-------------------------- success message ends here---------------------------------------------------*/

/*-------------------------- error message starts here---------------------------------------------------*/
$(document).ready(function() {
<?php if(validation_errors()!='' || $error!='') { ?>
new PNotify({
	  title: 'Error',
	  text: <?php echo json_encode(validation_errors().$error)?>,
	  type: 'error'
	});
<?php } ?>
});
/*-------------------------- error message ends here---------------------------------------------------*/
</script>

</body>
</html>
