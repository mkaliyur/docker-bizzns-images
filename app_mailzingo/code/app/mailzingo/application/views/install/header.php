<!DOCTYPE html>
<html>
<head>
<?php
header('Cache-Control: no-cache, no-store, must-revalidate');
header('Pragma: no-cache');
header('Expires: 0');
?>
<!-- Device & IE Compatibility Meta -->
<meta http-equiv="X-UA-Compatible" content="IE=9"/>
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0"/>
	<!--Load Google Fonts -->
<link href='https://fonts.googleapis.com/css?family=Roboto:300,400,600,700,800' rel='stylesheet' type='text/css'/>
<link href='https://fonts.googleapis.com/css?family=Raleway:400,300,300italic,400italic,500,500italic,600,600italic,700,800' rel='stylesheet' type='text/css'/>
	 <!--Load External CSS -->


 
<link rel="stylesheet" href="<?php echo $assets.'installation/css/bootstrap.min.css';?>" type="text/css" />
<link rel="stylesheet" href="<?php echo $assets.'installation/css/font-awesome.min.css';?>" type="text/css" />
<link rel="stylesheet" href="<?php echo $assets.'installation/css/style.css';?>" type="text/css" />
<link rel="stylesheet" href="<?php echo $assets.'installation/css/icomoon.css';?>" type="text/css" />
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<script type="text/javascript" src="<?php echo $assets.'installation/js/bootstrap.min.js';?>"></script>
</head>