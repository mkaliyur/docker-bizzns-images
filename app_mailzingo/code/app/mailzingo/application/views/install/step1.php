<?php 
include("header.php");?>
<title>MailZingo Installer-Step 1</title>
<link rel="icon" href="<?php echo $assets.'installation/images/favicon-icon.png'?>" type="image/png"/>
<body>
<div class="container-fluid">
<div class="row">
<style>
.error
{
	color:#900;
}
.padding5{padding:5px;}
</style>
<div class="col-xs-12 logosection visible-xs"><img src="<?php echo $assets.'installation/images/logo.png'?>" alt="" class="img-responsive center-block xsmt4 padding5"></div>
<div class="col-md-3 col-sm-3 col-xs-2 leftsection padding0 mt0 xsmt0">
<div class="mt5 xsmt2 hidden-xs"><img src="<?php echo $assets."installation/images/logo.png";?>" alt="" class="img-responsive center-block padding5"></div>
<?php $this->load->view("install/side-bar");?>
</div>
<div class="col-md-3 col-sm-3 col-xs-2"></div>
<div class="col-md-9 col-sm-9 col-xs-10 padding0 mt0 xsmt0">
<div class="col-md-12 col-sm-12 col-xs-12 progress_align">
<div class="progress mt2 xsmt2">
<div class="progress-bar" role="progressbar" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100" style="width:25%">25%
</div>
</div>
</div>

<div class="col-md-12 col-sm-12 col-xs-12 whitesection">
<div class="col-md-12 col-sm-12 col-xs-12 mdem15 smem13 xsem12 blacktext w500 padding0"><span class="orange">Step 1 -</span> Requirements</div>
<div class="col-md-12 col-sm-12 col-xs-12 mdem11 smem10 xsem9 mt2 xsmt5 padding0">
To ensure smooth functioning, we need the following requirements. To proceed further, you must be aware of the requisite credentials. Contact the webhost ASAP if you don't have.
</div>
<form action="" method="POST">
 <div class="col-md-12 col-sm-12 col-xs-12  table-responsive mt3 xsmt7">  
 <?php
	$version = explode('.', PHP_VERSION);
	$yes ="<button class='btn btn-success btn-block btn-sm'>&#10004</button>";
	$no ="<button class='btn btn-danger btn-block btn-sm'> &#10006;</button>";
	 
 ?>
   
	<table class="col-md-12 col-sm-12 col-xs-12 table-bordered table-striped table-condensed cf">
        		<thead class="cf mdem12 smem11 xsem10 thfont">
        			<tr>
        				<th>Add On</th>
        				<th>Requirement</th>
        				<th>Status</th>
        			
        			</tr>
        		</thead>
        		<tbody class="mdem10 smem9 xsem8 w400">
        			<tr>
        				<td>PHP</td>
        				<td>>5.4</td>
        				<td><?php 
						if($version[0] == 5 && $version[1] > 4){  
						echo '<i class="fa fa-check tick mdem15 smem13 xsem12"></i>';
                  	echo "<input type='hidden' class='check' id='php' name='php_check' value='1'>";
							  
                  }
				  else if($version[0] == 7)
				  {
					echo '<i class="fa fa-check tick mdem15 smem13 xsem12"></i>'; 
                    echo "<input type='hidden' class='check' id='php' name='php_check' value='1'>";	  
				  }
				  else{ 
				   
				  	echo '<i class="fa fa-times tick1 mdem15 smem13 xsem12"></i>'; 
                    	echo "<input type='hidden' class='check' id='php' name='php_check' value='0'>";	
                   
                  } 
				  ?>
                 <?php echo form_error('php'); ?>
                  </td>
        			
        			</tr>
        			<tr>
        				<td data-title="Code">MySQL</td>
        				<td data-title="Company">-</td>
        				<td><?php 
					if($version[0]==5){	
					if(function_exists('mysql_connect')){ 
						echo '<i class="fa fa-check tick mdem15 smem13 xsem12"></i>'; 
                    	echo "<input type='hidden' class='check' id='mysql' name='mysql_check' value='1'>";		  
                  }
				  else{ 
				  	echo '<i class="fa fa-times tick1 mdem15 smem13 xsem12"></i>';
                  	echo "<input type='hidden' class='check' id='mysql' name='mysql_check' value='0'>";
					} }
					if($version[0] == 7)
					{
						if(function_exists('mysqli_connect')){ 
						echo '<i class="fa fa-check tick mdem15 smem13 xsem12"></i>'; 
                    	echo "<input type='hidden' class='check' id='mysql' name='mysql_check' value='1'>";		  
						  }
						  else{ 
							echo '<i class="fa fa-times tick1 mdem15 smem13 xsem12"></i>';
							echo "<input type='hidden' class='check' id='mysql' name='mysql_check' value='0'>";
							}
					}
					?>
                      <?php echo '<p class="error">'.form_error('mysql').'</p>'; ?>
                    </td>
        			
        			</tr>
        			<tr>
        				<td data-title="Code">file_get_contents</td>
        				<td data-title="Company">Enable</td>
        				<td>
                        <?php 
                  if(ini_get('allow_url_fopen'))
                        {
                                echo '<i class="fa fa-check tick mdem15 smem13 xsem12"></i>';
                                 echo "<input type='hidden' id='get_content' name='get_content' class='check' value='1'>";
                        }
                        else
                        { 
                            echo '<i class="fa fa-times tick1 mdem15 smem13 xsem12"></i>';
                            echo "<input type='hidden' id='get_content' name='get_content' class='check' value='0'>";
                        }
                  ?>
                  <?php echo '<p class="error">'.form_error('get_content').'</p>'; ?>
                        </td>
        	
        			</tr>
					<tr>
        				<td data-title="Code">CURL</td>
        				<td data-title="Company">Enable</td>
        				<td>
                        <?php 
                  if(in_array('curl', get_loaded_extensions()))
                        {
                                echo '<i class="fa fa-check tick mdem15 smem13 xsem12"></i>';
                                 echo "<input type='hidden' id='curl' name='curl_check' class='check' value='1'>";
                        }
                        else
                        { 
                            echo '<i class="fa fa-times tick1 mdem15 smem13 xsem12"></i>';
                            echo "<input type='hidden' id='curl' name='curl_check' class='check' value='0'>";
                        }
                  ?>
                  <?php echo '<p class="error">'.form_error('curl1').'</p>'; ?>
                        </td>
        	
        			</tr>
        			<tr>
        				<td data-title="Code">Database File (database.php)</td>
        				<td data-title="Company">Writable(<?php echo form_error("check_database");?>)</td>
        				<td>
                        <?php
                  	if(is_writable('./application/config/database.php')){
						echo '<i class="fa fa-check tick mdem15 smem13 xsem12"></i>';
                 		echo "<input type='hidden' id='databse' name='check_database' class='check' value='1'>";
                  }else{ 
				   echo '<i class="fa fa-times tick1 mdem15 smem13 xsem12"></i>';
				  echo "<input type='hidden' id='databse' name='check_database' class='check' value='0'>"; } ?>
                        <?php echo '<p class="error">'.form_error('check_database').'</p>'; ?>
                        </td>
        			
        			</tr>
					<tr>
        				<td data-title="Code">Config File (config.php)</td>
        				<td data-title="Company">Writable(<?php echo form_error("check_config");?>)</td>
        				<td>
                        <?php
                  	if(is_writable('./application/config/config.php')){
						echo '<i class="fa fa-check tick mdem15 smem13 xsem12"></i>';
                 		echo "<input type='hidden' id='config' name='check_config' class='check' value='1'>";
                  }else{ 
				   echo '<i class="fa fa-times tick1 mdem15 smem13 xsem12"></i>';
				  echo "<input type='hidden' id='config' name='check_config' class='check' value='0'>"; } ?>
                        <?php echo '<p class="error">'.form_error('check_config').'</p>'; ?>
                        </td>
        			
        			</tr>
					<tr>
        				<td data-title="Code">Autoload File (autoload.php)</td>
        				<td data-title="Company">Writable(<?php echo form_error("check_autoload");?>)</td>
        				<td>
                        <?php
                  	if(is_writable('./application/config/autoload.php')){
						echo '<i class="fa fa-check tick mdem15 smem13 xsem12"></i>';
                 		echo "<input type='hidden' id='autoload' name='check_autoload' class='check' value='1'>";
                  }else{ 
				   echo '<i class="fa fa-times tick1 mdem15 smem13 xsem12"></i>';
				  echo "<input type='hidden' id='autoload' name='check_autoload' class='check' value='0'>"; } ?>
                        <?php echo '<p class="error">'.form_error('check_autoload').'</p>'; ?>
                        </td>
        			
        			</tr>
        			<tr>
        				<td data-title="Code">Max File Upload Size (<?php echo $maxUpload = (int)(ini_get('upload_max_filesize')); ?>M)</td>
        				<td data-title="Company">> = 2M</td>
        				<td>
                        <?php if($maxUpload >= 2){
				  echo '<i class="fa fa-check tick mdem15 smem13 xsem12"></i>';
                  echo "<input type='hidden' class='check' name='maxupload_check' id='maxupload' value='1'>";
                  }
				  else{ 
				  echo '<i class="fa fa-times tick1 mdem15 smem13 xsem12"></i>';
                  echo "<input type='hidden' class='check' id='maxupload' name='maxupload_check' value='0'>"; } ?>
                   <?php echo '<p class="error">'.form_error('maxupload_check').'</p>'; ?>
                        </td>
        	
        			</tr>
        			<tr>
        				<td data-title="Code">Max Post Size (<?php echo $maxPost = (int)(ini_get('post_max_size')); ?>M)</td>
        				<td data-title="Company">> = 8M</td>
                        <td>
                        <?php if($maxPost >= 8){
                                        echo '<i class="fa fa-check tick mdem15 smem13 xsem12"></i>';
                                        echo "<input type='hidden' class='check' name='maxpost_check' id='max_post' value='1'>";
                                        }else{ 
                                        echo '<i class="fa fa-times tick1 mdem15 smem13 xsem12"></i>';
                                        echo "<input type='hidden' id='max_post' name='maxpost_check' class='check' value='0'>"; } ?>
                        <?php echo '<p class="error">'.form_error('maxpost_check').'</p>'; ?>
                        </td>
        		
        			</tr>
        			<tr>
        				<td data-title="Code">Max Execution Time (<?php ini_set('max_execution_time', 30);  echo $maxExe = (int)(ini_get('max_execution_time')); ?> Sec)</td>
        				<td data-title="Company">> = 30 Second</td>
                        <td>
                        <?php if($maxExe >= 30){
                                        echo '<i class="fa fa-check tick mdem15 smem13 xsem12"></i>';
                                        echo "<input type='hidden' class='check' name='get_exe_time' id='max_post' value='1'>";
                                        }else{ 
                                        echo '<i class="fa fa-times tick1 mdem15 smem13 xsem12"></i>';
                                        echo "<input type='hidden' id='max_post' name='get_exe_time' class='check' value='0'>"; } ?>
                        <?php echo '<p class="error">'.form_error('get_exe_time').'</p>'; ?>
                        </td>
        		
        			</tr>
              
                    
        		</tbody>
        	</table>
         
    </div>    

<div class="col-md-12 col-sm-12 col-xs-12 text-right mt4 xsmt2 button">
<input type="submit" value="Next Step" name="step1" class="nxt-button mdem12 smem11 xsem10" >
</div>
  </form> 

</div>
</div>
</div>
</div>
 
<style>
/* Pnotify by Hunter Perrin :: 2.0.1 */
.ui-pnotify {
	top: 25px;
	right: 25px;
	position: absolute;
	height: auto;/* Ensures notices are above everything */
	z-index: 9999;
}
/* Hides position: fixed from IE6 */
html > body > .ui-pnotify {
	position: fixed;
}
.ui-pnotify .ui-pnotify-shadow {
	-webkit-box-shadow: 0px 2px 10px rgba(50, 50, 50, 0.5);
	-moz-box-shadow: 0px 2px 10px rgba(50, 50, 50, 0.5);
	box-shadow: 0px 2px 10px rgba(50, 50, 50, 0.5);
}
.ui-pnotify-container {
	background-position: 0 0;
	padding: .8em;
	height: 100%;
	margin: 0;
}
.ui-pnotify-sharp {
	-webkit-border-radius: 0;
	-moz-border-radius: 0;
	border-radius: 0;
}
.ui-pnotify-title {
	display: block;
	margin-bottom: .4em;
	margin-top: 0;
}
.ui-pnotify-text {
	display: block;
}
.ui-pnotify-icon, .ui-pnotify-icon span {
	display: block;
	float: left;
	margin-right: .2em;
}
/* Alternate stack initial positioning. */

.ui-pnotify.stack-topleft, .ui-pnotify.stack-bottomleft {
	left: 25px;
	right: auto;
}
.ui-pnotify.stack-bottomright, .ui-pnotify.stack-bottomleft {
	bottom: 25px;
	top: auto;
}
.ui-pnotify-closer, .ui-pnotify-sticker {
	float: right;
	margin-left: .2em;
}
/* theming */
.alert-success {
	color: #ffffff;
	background-color: rgba(38, 185, 154, 0.88);
	border-color: rgba(38, 185, 154, 0.88);
}
.alert-info {
	color: #E9EDEF;
	background-color: rgba(52, 152, 219, 0.88);
	border-color: rgba(52, 152, 219, 0.88);
}
.alert-warning {
	color: #E9EDEF;
	background-color: rgba(243, 156, 18, 0.88);
	border-color: rgba(243, 156, 18, 0.88);
}
.alert-danger, .alert-error {
	color: #E9EDEF;
	background-color: rgba(231, 76, 60, 0.88);
	border-color: rgba(231, 76, 60, 0.88);
}
.alert-dark, .btn-dark {
	color: #E9EDEF;
	background-color: rgba(52, 73, 94, 0.88);
	border-color: rgba(52, 73, 94, 0.88);
}
.btn-dark:hover {
	color: #F7F7F7;
}
/* /theming */
/* /Pnotify by Hunter Perrin :: 2.0.1 */
</style>
 
 
 
<script type="text/javascript" src="<?php echo $assets;?>/assets/default/js/notify/pnotify.core.js"></script>
<?php
$error ='';
if($version[0]==5 && $version[1]<=4)
{
	$error = "Update PHP VERSION.<br>";
}
if($version[0]==5){
if(!function_exists('mysql_connect'))
{
	$error.= "Mysql Not Enable.<br>";
}}
if($version[0]==7){
if(!function_exists('mysqli_connect'))
{
	$error.= "Mysql Not Enable.<br>";
}	
}
if(!ini_get('allow_url_fopen'))
{
	$error.= "Allow Url Open is Not Enabled.<br>";
}
if(!in_array('curl', get_loaded_extensions()))
{
	$error.= "CURL NOT ENABLED.<br>";
}
if(!is_writable('./application/config/database.php'))
{
	$error.= "Database file not writable.<br>";
}
if(!is_writable('./application/config/config.php'))
{
	$error.= "Config file not writable.<br>";
}
if(!is_writable('./application/config/autoload.php'))
{
	$error.= "Autoload file not writable.<br>";
}
if($maxUpload < 2)
{
	$error.= "Max File Upload Size Is not greater than 2MB.<br>";
}
if($maxPost < 8)
{
	$error.= "Max POST  Size Is not greater than 8MB.<br>";
}
if($maxExe < 30)
{
	$error.= "Max Execution time must be greater than 30 sec.";
}
if($error!='')
{
?>
<script>
new PNotify({title: 'Error',text: '<?php echo $error;?>',type: 'error'});
</script>
<?php }?>
</body>
</html>