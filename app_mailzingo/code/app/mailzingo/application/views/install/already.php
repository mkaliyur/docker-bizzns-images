<?php $this->load->view("install/header");?>

<title>MailZingo Installer-Step 4</title>
<link rel="icon" href="<?php echo $assets.'installation/images/favicon-icon.png'?>" type="image/png"/>
<body>
<div class="container-fluid">
<div class="row">
<div class="col-xs-12 logosection visible-xs"><img src="<?php echo $assets.'installation/images/logo.png'?>" alt="" class="img-responsive center-block xsmt4"></div>




<div class="col-md-12 col-sm-12 col-xs-12 padding0 mt0 xsmt0">

<div class="col-md-12 col-sm-12 col-xs-12 progress_align">
<div class="progress mt2 xsmt2">
<div class="progress-bar" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width:100%">100%
</div>
</div>
</div>

<div class="col-md-12 col-sm-12 col-xs-12 whitesection">


<div class="mdem35 smem30 xsem25 text-center mt10 xsmt12 orange"><i class="icon icon-check-congrates"></i></div>
<div class="mdem20 smem18 xsem15 text-center mt0 xsmt0 blacktext">Hello <span class="w600"><?php echo $adminname;?>!</span></div>
<div class="mdem12 smem11 xsem10 text-center mt0 xsmt0 blacktext w500">MailZingo already installed. You are now ready to get started.</div>
<div class="line"></div>

<div class="clear text-center">
<a href="<?php echo  site_url();?>" class="homebutton mdem12 smem11 xsem9">Homepage</a>&nbsp;
<!--<a href="<?//= site_url().'store-admin';?>" target="_blank" class="adminbutton mdem12 smem11 xsem9">Admin</a></div>-->

</div>

</div>    


</div>
</div>
</div>
</div>

<script>
$(document).ready(function(){
    $('[data-toggle="tooltip"]').tooltip();
});
</script>

</body>
</html>
