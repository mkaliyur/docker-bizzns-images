<?php  $this->load->view("install/header");
?>
<title>MailZingo Installer-Step 2</title>
<link rel="icon" href="<?php echo $assets.'installation/images/favicon-icon.png'?>" type="image/png"/>
<body>
<style>
.error{color:#900;}
.fields input[type="text"], .fields input[type="password"]{margin-bottom:1% !important; }
.padding5{padding:5px;}
</style>
 
<script type="text/javascript"> 
    if(navigator.userAgent.indexOf("MSIE") > 0 )
    {
        $(function()
        {
            var input = document.createElement("input");
            if(('placeholder' in input) == false)
            {
                $('[placeholder]').focus(function()
                {
                    var i = $(this);
                    if(i.val() == i.attr('placeholder'))
                    {
                        i.val('').removeClass('placeholder');
                        if(i.hasClass('password'))
                        {
                            i.removeClass('password'); this.type='password';
                        }
                    }
                }).blur(function()
                {
                    var i = $(this);
                    if(i.val() == '' || i.val() == i.attr('placeholder'))
                    {
                        if(this.type=='password')
                        {
                            i.addClass('password'); this.type='text';
                        }
                        i.addClass('placeholder').val(i.attr('placeholder'));
                    }
                }).blur().parents('form').submit(function()
                {
                    $(this).find('[placeholder]').each(function()
                    {
                        var i = $(this);
                        if(i.val() == i.attr('placeholder')) i.val('');
                    });
                });
            }
        });
    }

</script>
 

<script type="text/javascript" src="<?php echo site_url();?>assets/default/js/notify/pnotify.core.js"></script> 
 
<div class="container-fluid">
<div class="row">

<div class="col-xs-12 logosection visible-xs"><img src="<?php echo $assets.'installation/images/logo.png'?>" alt="" class="img-responsive center-block xsmt4"></div>

<div class="col-md-3 col-sm-3 col-xs-2 leftsection padding0 mt0 xsmt0">
<div class="mt5 xsmt2 hidden-xs"><img src="<?php echo $assets."installation/images/logo.png";?>" alt="" class="img-responsive center-block padding5"></div>

<?php $this->load->view("install/side-bar");?>

</div>
<div class="col-md-3 col-sm-3 col-xs-2"></div>

<div class="col-md-9 col-sm-9 col-xs-10 padding0 mt0 xsmt0">

<div class="col-md-12 col-sm-12 col-xs-12 progress_align">
<div class="progress mt2 xsmt2">
<div class="progress-bar" role="progressbar" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100" style="width:50%">50%
</div>
</div>
</div>

<div class="col-md-12 col-sm-12 col-xs-12 whitesection">
<div class="mdem15 smem13 xsem12 blacktext w500 padding0"><span class="orange">Step 2 - </span>Database Connectivity</span></div>
<div class="mdem11 smem10 xsem9 mt2 xsmt5 padding0">
Enter the valid credentials  about your MySQL database here and proceed smoothly to the next step.
</div>

<form action="" method="POST" id="form_data">
<?php  echo "<h3 style='font-size:14px !important; color:red; margin-top:10px; font-weight:600;'>".$this->session->userdata("con_error")."</h3>";$this->session->unset_userdata("con_error")?>
	<div class="col-md-12 col-sm-12 col-xs-12 mt2 xsmt7 fields padding0">  

<div class="col-md-2 col-sm-3 col-xs-12 em11 mdem10 smem10 xsem10 w500 padding0 mt1 xsmt3">Hostname</div>

<div class="col-md-10 col-sm-9 col-xs-12 mt0 xsmt2 fields xs_padding fieldstep1 ">
    <div class="col-md-8 col-sm-8 col-xs-12 xs_padding"><input type="text" data-tip="" id="hostname" value="<?php echo $hostname; ?>" name="hostname" placeholder="Hostname" >
    <span class="error" id="hostname_error"><?php echo  strip_tags(form_error('hostname'));?></span>
</div>
<div class="col-md-4 col-sm-4 col-xs-4 padding0 mt1 xsmt1 mdem12 smem11 xsem10 hidden-xs"><a href="#" data-toggle="tooltip" data-placement="right" title="Enter Hostname Here" class="ticon"><i class="icon icon-information orange"></i></a></div>
</div>  

<div class="col-md-2 col-sm-3 col-xs-12 em11 mdem10 smem10 xsem10 w500 padding0 mt1 xsmt3">Database Name</div>

<div class="col-md-10 col-sm-9 col-xs-12 fields xs_padding fieldstep1 mt0 xsmt3">
<div class="col-md-8 col-sm-8 col-xs-12 xs_padding"><input type="text" data-tip="" id="database" name="database" value="<?php echo $database; ?>" autocomplete="off" placeholder="Database Name">
<span class="error" id="database_error"><?php echo strip_tags(form_error('database'));?></span>
</div>
<div class="col-md-4 col-sm-4 col-xs-4 padding0 mt1 xsmt1 mdem12 smem11 xsem10 hidden-xs"><a href="#" data-toggle="tooltip" data-placement="right" title="Enter Database Name Here" class="ticon"><i class="icon icon-information orange"></i></a></div>
</div>

<div class="col-md-2 col-sm-3 col-xs-12 em11 mdem10 smem10 xsem10 w500 padding0 mt1 xsmt3 clear">MySQL Username</div>

<div class="col-md-10 col-sm-9 col-xs-12 fields xs_padding fieldstep1 mt0 xsmt3">
<div class="col-md-8 col-sm-8 col-xs-12 xs_padding"><input type="text" data-tip="" id="username" value="<?php echo $username; ?>" autocorrect="off" autocapitalize="off" name="username" autocomplete="off" placeholder="MySQL Username">
<span class="error" id="username_error"><?php echo strip_tags(form_error('username'));?></span>
</div>
<div class="col-md-4 col-sm-4 col-xs-4 padding0 mt1 xsmt1 mdem12 smem11 xsem10 hidden-xs"><a href="#" data-toggle="tooltip" data-placement="right" title="Enter Mysql Username Here" class="ticon"><i class="icon icon-information orange"></i></a></div>
</div>

<div class="col-md-2 col-sm-3 col-xs-12 em11 mdem10 smem10 xsem10 w500 padding0 mt1 xsmt3 clear">MySQL Password</div>

<div class="col-md-10 col-sm-9 col-xs-12 fields xs_padding fieldstep1 mt0 xsmt3">
    <div class="col-md-8 col-sm-8 col-xs-12 xs_padding"><input type="password" data-tip="" autocorrect="off" value="<?php echo $password; ?>" autocapitalize="off" id="password" name="password" autocomplete="off" placeholder="MySQL Password">
<span class="error" id="password_error"><?php echo strip_tags(form_error('password'));?></span>
</div>
<div class="col-md-4 col-sm-4 col-xs-4 padding0 mt1 xsmt1 mdem12 smem11 xsem10 hidden-xs"><a href="#" data-toggle="tooltip" data-placement="right" title="Enter Mysql Password Here" class="ticon"><i class="icon icon-information orange"></i></a></div>
</div>
 

<div class="col-md-2 col-sm-3 col-xs-12 em11 mdem10 smem10 xsem10 w500 padding0 mt1 xsmt3 clear">Site URL</div>

<div class="col-md-10 col-sm-9 col-xs-12 fields xs_padding mt0 xsmt3">
<div class="col-md-8 col-sm-8 col-xs-12 xs_padding"><input type="text" data-tip="" id="site_url" name="site_url" readonly value="<?php echo str_replace('/step2','',site_url()); ?>" autocomplete="off" placeholder="Site URL"></div>
<div class="col-md-4 col-sm-4 col-xs-4 padding0 mt1 xsmt1 mdem12 smem11 xsem10 hidden-xs"><a href="#" data-toggle="tooltip" data-placement="right" title="Enter Site Url Here" class="ticon"><i class="icon icon-information orange"></i></a></div>
</div>

<div class="col-md-2 col-sm-1 col-xs-12 mdem11 smem10 xsem10 w500 padding0 mt1 xsmt0"></div>

<div class="col-md-10 col-sm-10 col-xs-12 xs_padding">
<div class="col-md-8 col-sm-8 col-xs-12 xs_padding xstext-center button mt4 xsmt7">
<input type="submit" name="step2" value="Next Step" class="nxt-button mdem12 smem11 xsem10">
</div>
<div class="col-md-4 col-sm-4 col-xs-4"></div>
</div>

</div>
</form>


</div>
</div>
</div>
</div>
<style>
</style>
 
 
<script>
$(document).ready(function(){
    $('[data-toggle="tooltip"]').tooltip();
	$("#form_data" ).submit(function( event ) {
		    // event.preventDefault();
		 	 var hostname=$("#hostname").val();
			 var database=$("#database").val();
			 var username=$("#username").val();
			 if(hostname.length>0)
			 {
				$("#hostname_error").html("");
			 }
			 else
			 {
			 	$("#hostname_error").html("Hostname is required");
			 	
			 }
			 if(database.length>0)
			 {
				 $("#database_error").html("");
			 }
			 else
			 {
				$("#database_error").html("Database Name  is required");
				 
			 }
			 if(username.length>0)
			 {
				  $("#username_error").html("");
			 }
			 else
			 {
				$("#username_error").html("Username is required");
			 }
			 if(hostname.length>0 && database.length>0 && username.length>0)
			 {
				 $( "#form_data" ).submit();
			 }
			 else
			 {
				return false;
			 }
		});
});
</script>
 
 

 
 
<?php
if($this->session->flashdata('in')){?>
<script>
new PNotify({title: 'Error',text: '<?php echo $this->session->flashdata('in');?>',type: 'error'});
</script>
<?php } 
if(isset($error) && $error!=''){?>
<script>
new PNotify({title: 'Error',text: '<?php echo $error;?>',type: 'error'});
</script>
<?php }
if(validation_errors())
{
	?>
<script>
new PNotify({title: 'Error',text: '<?php echo validation_errors();?>',type: 'error'});
</script>
	<?php
}
?>
</body>
</html>