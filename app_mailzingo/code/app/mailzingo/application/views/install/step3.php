<?php 
$this->load->view("install/header");
?>

<title>MailZingo Installer-Step 3</title>
<link rel="icon" href="<?php echo $assets.'installation/images/favicon-icon.png'?>" type="image/png"/>
<body>
<style>
	.error_msg
	{
		color:#990000;
	}
	.success_msg
	{
	  color:#006600;	
	}
    .fields input[type="text"],.fields input[type="password"]{ margin-bottom:1% !important;}
	.padding5{padding:5px;}
        .error{
            color: red;
        }
 

</style>
<script type="text/javascript" src="<?php echo site_url();?>assets/default/js/notify/pnotify.core.js"></script> 
<!-- IE Placeholder Script -->
<script type="text/javascript"> 
    if(navigator.userAgent.indexOf("MSIE") > 0 )
    {
        $(function()
        {
            var input = document.createElement("input");
            if(('placeholder' in input) == false)
            {
                $('[placeholder]').focus(function()
                {
                    var i = $(this);
                    if(i.val() == i.attr('placeholder'))
                    {
                        i.val('').removeClass('placeholder');
                        if(i.hasClass('password'))
                        {
                            i.removeClass('password'); this.type='password';
                        }
                    }
                }).blur(function()
                {
                    var i = $(this);
                    if(i.val() == '' || i.val() == i.attr('placeholder'))
                    {
                        if(this.type=='password')
                        {
                            i.addClass('password'); this.type='text';
                        }
                        i.addClass('placeholder').val(i.attr('placeholder'));
                    }
                }).blur().parents('form').submit(function()
                {
                    $(this).find('[placeholder]').each(function()
                    {
                        var i = $(this);
                        if(i.val() == i.attr('placeholder')) i.val('');
                    });
                });
            }
        });
    }

</script>
<!--[if lt IE 9]>
<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
<![endif]-->
<div class="container-fluid">
<div class="row">

<div class="col-xs-12 logosection visible-xs"><img src="<?php echo $assets.'installation/images/logo.png'?>" alt="" class="img-responsive center-block xsmt4"></div>



<div class="col-md-3 col-sm-3 col-xs-2 leftsection padding0 mt0 xsmt0">
<div class="mt5 xsmt2 hidden-xs"><img src="<?php echo $assets."installation/images/logo.png";?>" alt="" class="img-responsive center-block padding5"></div>

 
<?php $this->load->view("install/side-bar");?>
</div>

<div class="col-md-3 col-sm-3 col-xs-2"></div>

<div class="col-md-9 col-sm-9 col-xs-10 padding0 mt0 xsmt0">

<div class="col-md-12 col-sm-12 col-xs-12 progress_align">
<div class="progress mt2 xsmt2">
<div class="progress-bar" role="progressbar" aria-valuenow="75" aria-valuemin="0" aria-valuemax="100" style="width:75%">75%
</div>
</div>
</div>

<div class="col-md-12 col-sm-12 col-xs-12 whitesection">
<div class="col-md-12 col-sm-12 col-xs-12 mdem15 smem13 xsem12 blacktext w500 padding0"><span class="orange">Step 3 - </span>Admin Credentials</span></div>
<div class="col-md-12 col-sm-12 col-xs-12 mdem11 smem10 xsem9 mt2 xsmt5 padding0">
Create your username and password here so that you can login to your MailZingo dashboard ahead
</div>
<form action="" method="POST" id="form_data" class="AdminForm">
<div class="col-md-12 col-sm-12 col-xs-12 mt2 xsmt7 fields padding0">  
<div class="row">
<div class="col-md-6 col-sm-6 col-xs-12 em11 mdem10 smem10 xsem10 w500 mt1 xsmt2">Administrator Name
<div class="mt2 xsmt2 fieldstep2">
<input type="text" data-tip="" Placeholder="Administrator Name" value="<?php if(set_value('adminname')){echo set_value('adminname');} else{ echo $adminname;} ?>" name="adminname"  autocomplete="off" id="admin_username">
<input type="hidden" value="" id="admin_username1">
<label class="error" id="username_error"><span class='error_msg'><?php echo strip_tags(form_error('adminname'));?></span></label>
</div></div>

<div class="col-md-6 col-sm-6 col-xs-12 em11 mdem10 smem10 xsem10 w500 mt1 xsmt2">Site URL
<div class="mt2 xsmt2 fieldstep2"><input type="text" id="adminSiteUrl" data-tip="" Placeholder="Site Url" value="<?php echo site_url();?>" name="" readonly>
<input type="hidden" value="" id="adminSiteUrl1">
<label class="error" id="username_Siteurl"><span class='error_msg'><?php echo strip_tags(form_error('adminSiteUrl'));?></span></label>
</div></div>

<div class="col-md-6 col-sm-6 col-xs-12 em11 mdem10 smem10 xsem10 w500 mt1 xsmt2 clear">Administrator Email
<div class="mt2 xsmt2 fieldstep2"><input type="text" data-tip="" id="admin_email" name="adminemail" value="<?php if(set_value('adminemail')){echo set_value('adminemail');} else{ echo $adminemail;} ?>" Placeholder="Administrator Email"   autocomplete="off">
 <input type="hidden" value="" id="admin_email1">
<label class="error" id="email_error"><span class='error_msg'><?php echo strip_tags(form_error('adminemail'));?></span></label>
</div></div>
<div class="col-md-6 col-sm-6 col-xs-12 mdem11 smem10 xsem10 w500 mt1 xsmt2">Company Name
<div class="mt2 xsmt2 fieldstep2">
<input type="text"  Placeholder="Company Name" data-tip="" id="company_name" name="company_name" ="" value="<?php echo set_value('company_name');?>"  autocomplete="off">
 <input type="hidden" value="" id="company_name1">
<label class="error" id="company_error"><span class='error_msg'><?php  echo form_error('company_name');?></span></label>
</div></div>

<div class="col-md-6 col-sm-6 col-xs-12 em11 mdem10 smem10 xsem10 w500 mt1 xsmt2 clear">Administrator Password
<div class="mt2 xsmt2 fieldstep2"><input type="password" data-tip=""  Placeholder="Administrator Password" value="<?php echo set_value('adminpass'); ?>" id="admin_password" name="adminpass"   autocomplete="off">
 <input type="hidden" value="" id="admin_pass1">
 <label class="error" id="admin_error"><span class='error_msg'><?php echo strip_tags(form_error('adminpass'));?></span></label>
</div></div>
<div class="col-md-6 col-sm-6 col-xs-12 em11 mdem10 smem10 xsem10 w500 mt1 xsmt2">Confirm Password
<div class="mt2 xsmt2 fieldstep2">
<input type="password" data-tip=""  Placeholder="Confirm Administrator Password" id="confirm_password" value="<?php echo set_value('confirm_password');?>" name="confirm_password"   autocomplete="off">
<label class="error" id="confirm_error"><span class='error_msg'><?php echo strip_tags(form_error('confirm_password'));?></span></label>
<input type="hidden" value="" id="confirm_password1">
</div></div>

<div class="col-md-12 col-sm-12 col-xs-12 mt4 xsmt0 xstextcenter text-right button">
<input type="submit" name="step3" Value="Next Step" class="nxt-button mdem12 smem11 xsem9">
</div>

</div>    

</form>
</div>
</div>
</div>
</div>
</div>

<script>
$(document).ready(function(){
	var ua = window.navigator.userAgent;
    var msie = ua.indexOf("MSIE ");

    if (msie > 0 || !!navigator.userAgent.match(/Trident.*rv\:11\./))  // If Internet Explorer, return version number
    {
        var ie_enable =1;
    }
    else  // If another browser, return 0
    {
        var ie_enable =0;
    }
    $('[data-toggle="tooltip"]').tooltip();
	$( "#admin_password" ).on("keyup change blur",function(event){
			var pass=$("#admin_password").val();
			if(ie_enable==1)
			{
				if(pass.length==0 || pass=='Administrator Password'){
					$("#admin_error").html("<span class='error_msg'>Enter Password</span>");	
					$("#confirm_password").prop('disabled', true);
					$("#admin_pass1").val(0);
				}
				else if(!pass.match(/^.{6,}$/))
				{
					$("#admin_error").html("<span class='error_msg'>Minimum 6 characters</span>");	
					$("#confirm_password").prop('disabled', true);
					$("#admin_pass1").val(0);
				}	
				else
				{
					$("#admin_error").html("<span class='success_msg'>Password correct</span>");	
					$("#confirm_password").prop('disabled', false);
					$("#admin_pass1").val(1);
				}	
			}
			else{
			if(pass.length==0){
					$("#admin_error").html("<span class='error_msg'>Enter Password</span>");	
					$("#confirm_password").prop('disabled', true);
					$("#admin_pass1").val(0);
				}
			else if(!pass.match(/^.{6,}$/))
			{
				$("#admin_error").html("<span class='error_msg'>Minimum 6 characters</span>");	
				$("#confirm_password").prop('disabled', true);
				$("#admin_pass1").val(0);
			}	
			else
			{
				$("#admin_error").html("<span class='success_msg'>Password correct</span>");	
				$("#confirm_password").prop('disabled', false);
				$("#admin_pass1").val(1);
			}
			
			}
		});
	$( "#confirm_password" ).on("keyup change blur",function(event){
			var pass=$("#confirm_password").val();
			var pass1=$("#admin_password").val();
			if(!pass.match(/^.{6,}$/) && pass!=pass1)
			{
				$("#confirm_error").html("<span class='error_msg'>Password not matched</span>");	
				$("#confirm_password1").val(0);
			}
			if(pass==pass1 && pass1!="")
			{
				$("#confirm_error").html("<span class='success_msg'>Password matched</span>");	
				$("#confirm_password1").val(1);
			}
			
		});	
	$( "#admin_username" ).on("keyup change blur",function(event){
			var username=$("#admin_username").val();
			if(ie_enable==1)
			{
				if(!username.match(/^[a-zA-Z ]*$/) || username=="" || username.length<3 || username=='Administrator Name')
				{
					$("#username_error").html("<span class='error_msg'>Enter your full name</span>");	
					$("#admin_username1").val(0);
				}	
				else
				{
					$("#username_error").html("<span class='success_msg'>Administrator name correct</span>");	
					$("#admin_username1").val(1);
				}
			}
			else{
				if(!username.match(/^[a-zA-Z ]*$/) || username=="" || username.length<3)
				{
					$("#username_error").html("<span class='error_msg'>Enter your full name</span>");	
					$("#admin_username1").val(0);
				}	
				else
				{
					$("#username_error").html("<span class='success_msg'>Administrator name correct</span>");	
					$("#admin_username1").val(1);
				}
			}
			
			
			
		});
	$( "#admin_email" ).on("keyup change blur",function(event){
			var email=$("#admin_email").val();
			if(!email.match(/^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,6}$/) || email=="")
			{
				$("#email_error").html("<span class='error_msg'>Enter valid email</span>");	
				$("#admin_email1").val(0);
			}	
			else
			{
				$("#email_error").html("<span class='success_msg'>Email correct</span>");	
				$("#admin_email1").val(1);
			}
			
		});
	$( "#company_name" ).on("keyup change blur",function(event){
			var username1=$("#company_name").val();
			if(ie_enable==1)
			{
				if(!username1.match(/^[a-zA-Z0-9 ]*$/) || username1.length<3 || username1=='' || username1=='Company Name')
				{
					$("#company_error").html("<span class='error_msg'>Enter valid company name</span>");	
					$("#company_name1").val(0);
				}	
				else
				{
					$("#company_error").html("<span class='success_msg'>Company name correct</span>");	
					$("#company_name1").val(1);
				}	
			}
			else{
			
				if(!username1.match(/^[a-zA-Z0-9 ]*$/) || username1.length<3 || username1=='')
				{
					$("#company_error").html("<span class='error_msg'>Enter valid company name</span>");	
					$("#company_name1").val(0);
				}	
				else
				{
					$("#company_error").html("<span class='success_msg'>Company name correct</span>");	
					$("#company_name1").val(1);
				}
			} 
		});
	 
                
	$( "#form_data" ).submit(function( event ) {
			 if(("#admin_pass1").val()==1 && ("#admin_username1").val()==1 && ("#admin_email1").val()==1 && ("#company_name1").val()==1 && ("#confirm_password1").val()==("#admin_pass1").val())
			 {
			 	return true;
			 }
			 else
			 {
			 	$("#form_error").html("Please fill correct details");
				return false;
			 }
		}); 
});
 
</script>
<?php
if($this->session->flashdata('in')){?>
<script>
new PNotify({title: 'Error',text: '<?php echo $this->session->flashdata('in');?>',type: 'error'});
</script>
<?php } 
?>
</body>
</html>