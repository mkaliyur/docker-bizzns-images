<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	http://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There area two reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router what URI segments to use if those provided
| in the URL cannot be matched to a valid route.
|
*/



$route['default_controller'] = "welcome";
$route['404_override'] = 'error/_404';

$route['login'] = 'login/index';
$route['logout'] = 'login/logout';
$route['forget-password'] = 'login/forget_password';
$route['new-password/(:any)']='login/new_password/$1';
$route['create-password']='login/new_password';
$route['update-licensing/(:any)/(:any)']='app/updateLicensing/$1/$2';

$route['editor'] = 'welcome/editor';

/*---------- Install route starts --------------------*/
$route['install'] = 'install/step1';
/*---------- Install route ends--------------------*/

$route['info'] = 'welcome/info';
$route['user/change-password'] = "login/changePassword";

/*---------- dashboard graph stars --------------------*/
$route['get-dashboard-graph'] = "dashboard/getGraph";
$route['get-dashboard-stats-count'] = "dashboard/getStatsCount";
/*---------- dashboard graph ends ---------------------*/

/*---------- update version starts here --------------------*/
$route['update-version'] = "update_version/index";
/*---------- update version ends here ----------------------*/

/*---------- campaign starts --------------------------*/
$route['add-campaign'] = 'campaign/addCampaign';
$route['campaign-list'] = 'campaign/index';
$route['edit-campaign/(:num)'] = 'campaign/editCampaign/$1';
$route['delete-campaign/(:num)'] = 'campaign/deleteCampaign/$1';
$route['clear-campaign/(:num)'] = 'campaign/clearCampaign/$1';
$route['delete-multiple-campaign'] = 'campaign/deleteMultiCampaign';
$route['copy-move-compaign'] = 'campaign/copyMoveCampaign';
/*---------- campaign ends ----------------------------*/

/*---------- contact starts ---------------------------*/
$route['add-contact/(:num)'] = 'contact/addContact/$1';
$route['add-contact'] = 'contact/addContact';
$route['contact-list'] = 'contact/index';
$route['contact-list-pagination'] = 'contact/load_more';
$route['edit-contact/(:num)'] = 'contact/editContact/$1';
$route['delete-contact/(:num)'] = 'contact/deleteContact/$1';
$route['delete-multiple-contact'] = 'contact/deleteMultiContact';
$route['copy-move-contact'] = 'contact/copyMoveContact';
$route['import-contact'] = 'contact/importContact';
$route['export-contact'] = 'contact/exportContact';
$route['get-newsletter-list'] = 'contact/getNewsletterList';
/*---------- contact ends -----------------------------*/

/*---------- suppression list starts ------------------*/
$route['suppression-list'] = 'suppression_list/index';
$route['add-suppression-list'] = 'suppression_list/addList';
$route['delete-suppression-list/(:num)'] = 'suppression_list/deleteList/$1';
$route['import-suppression-list'] = 'suppression_list/importSuppression';
$route['edit-suppression-list/(:num)'] = 'suppression_list/editList/$1';
$route['suppression-list-pagination'] = 'suppression_list/load_more';
$route['delete-suppression/(:num)'] = 'suppression_list/deleteSuppression/$1';
/*---------- suppression list ends --------------------*/

/*---------- Template starts --------------------------*/
$route['template-list'] = 'template/index';
$route['template-list/(:any)'] = 'template/index/$1';
$route['add-template'] = 'template/addTemplate';
$route['edit-template/(:num)'] = 'template/editTemplate/$1';
$route['edit-template/(:num)/(:any)'] = 'template/editTemplate/$1/$2';
$route['delete-template/(:num)'] = 'template/deleteTemplate/$1';
$route['delete-multiple-template'] = 'template/deleteMultiTemplate';
/*---------- Template ends ----------------------------*/

/*---------- Newsletter starts ------------------------*/
$route['message'] = 'newsletter/message';
$route['choose-newsletter-editor'] = 'newsletter/chooseEditor';
$route['inline-editor/(:num)/(:any)'] = 'editor/index/$1/$2';
$route['delete-attatchment'] = 'newsletter/deleteAttatchment';
$route['save-template'] = 'newsletter/saveTemplate';
$route['send-test-mail'] = 'newsletter/sendTestMail';
$route['send-test-mail-out'] = 'newsletter/sendTestMailOut';
$route['add-temp-attatchment'] = 'newsletter/addTempAttatchment';
$route['get-final-email-count'] = 'newsletter/getFinalEmailCount';
$route['add-newsletter'] = 'newsletter/addNewsletter';
$route['use-newsletter/(:num)'] = 'newsletter/addNewsletter/$1';
$route['edit-newsletter/(:num)'] = 'newsletter/addNewsletter/$1';
$route['newsletter-list/(:any)'] = 'newsletter/index/$1';
$route['delete-newsletter/(:num)'] = 'newsletter/deleteNewsletter/$1';
$route['delete-multiple-newsletter'] = 'newsletter/deleteMultiNewsletter';
$route['newsletter-pagination'] = 'newsletter/load_more';
$route['get-campaign-list-for-newsletter'] = "newsletter/getCampaignList";
$route['newsletter-successfull'] = "newsletter/success";
/*---------- Newsletter ends --------------------------*/

/*---------- Autoresponder starts ---------------------*/
$route['choose-autoresponder-editor'] = 'autoresponder/chooseEditor';
$route['delete-attatchment-autoresponder'] = 'autoresponder/deleteAttatchment';
$route['save-template-autoresponder'] = 'autoresponder/saveTemplate';
$route['send-test-mail-autoresponder'] = 'autoresponder/sendTestMail';
$route['add-temp-attatchment-autoresponder'] = 'autoresponder/addTempAttatchment';
$route['add-autoresponder'] = 'autoresponder/addAutoresponder';
$route['use-autoresponder/(:num)'] = 'autoresponder/addAutoresponder/$1';
$route['edit-autoresponder/(:num)'] = 'autoresponder/addAutoresponder/$1';
$route['autoresponder-list'] = 'autoresponder/index/';
$route['delete-autoresponder/(:num)'] = 'autoresponder/deleteAutoresponder/$1';
$route['delete-multiple-autoresponder'] = 'autoresponder/deleteMultiAutoresponder';
$route['autoresponder-successfull'] = "autoresponder/success";
$route['autoresponder-pagination'] = 'autoresponder/load_more';
$route['autoresponder-change-status'] = 'autoresponder/changeStatus';
/*---------- Autoresponder ends -----------------------*/

/*---------- Newsletter stats starts ------------------*/
$route['newsletter-stats1'] = 'stats/index1';
$route['newsletter-stats'] = 'stats/index';
$route['newsletter-stats/(:num)/overview'] = 'stats/statsByNewsletter/$1/overview';
$route['newsletter-stats/(:num)/technology'] = 'stats/statsByNewsletter/$1/technology';
$route['newsletter-stats/(:num)/location'] = 'stats/statsByNewsletter/$1/location';
$route['newsletter-stats-report/(:num)/sent'] = 'stats/statsReportByNewsletter/$1/sent';
$route['newsletter-stats-report/(:num)/open'] = 'stats/statsReportByNewsletter/$1/open';
$route['newsletter-stats-report/(:num)/click'] = 'stats/statsReportByNewsletter/$1/click';
$route['newsletter-stats-report/(:num)/unsubscribed'] = 'stats/statsReportByNewsletter/$1/unsubscribed';
$route['newsletter-stats-report/(:num)/bounced'] = 'stats/statsReportByNewsletter/$1/bounced';
$route['newsletter-stats-report/(:num)/complaints'] = 'stats/statsReportByNewsletter/$1/complaints';
$route['newsletter-stats-export/(:num)/sent'] = 'stats/export/$1/sent';
$route['newsletter-stats-export/(:num)/open'] = 'stats/export/$1/open';
$route['newsletter-stats-export/(:num)/click'] = 'stats/export/$1/click';
$route['newsletter-stats-export/(:num)/unsubscribed'] = 'stats/export/$1/unsubscribed';
$route['newsletter-stats-export/(:num)/bounced'] = 'stats/export/$1/bounced';
$route['newsletter-stats-export/(:num)/complaints'] = 'stats/export/$1/complaints';
$route['get-newsletter-stats-graph'] = "stats/getGraph";
/*---------- Newsletter stats ends --------------------*/

/*---------- Autoresponder stats starts ---------------*/
$route['autoresponder-stats'] = 'autoresponder_stats/index';
$route['autoresponder-stats/(:num)/overview'] = 'autoresponder_stats/statsByAutoresponder/$1/overview';
$route['autoresponder-stats/(:num)/technology'] = 'autoresponder_stats/statsByAutoresponder/$1/technology';
$route['autoresponder-stats/(:num)/location'] = 'autoresponder_stats/statsByAutoresponder/$1/location';
$route['autoresponder-stats-report/(:num)/sent'] = 'autoresponder_stats/statsReportByAutoresponder/$1/sent';
$route['autoresponder-stats-report/(:num)/open'] = 'autoresponder_stats/statsReportByAutoresponder/$1/open';
$route['autoresponder-stats-report/(:num)/click'] = 'autoresponder_stats/statsReportByAutoresponder/$1/click';
$route['autoresponder-stats-report/(:num)/unsubscribed'] = 'autoresponder_stats/statsReportByAutoresponder/$1/unsubscribed';
$route['autoresponder-stats-report/(:num)/bounced'] = 'autoresponder_stats/statsReportByAutoresponder/$1/bounced';
$route['autoresponder-stats-report/(:num)/complaints'] = 'autoresponder_stats/statsReportByAutoresponder/$1/complaints';
$route['autoresponder-stats-export/(:num)/sent'] = 'autoresponder_stats/export/$1/sent';
$route['autoresponder-stats-export/(:num)/open'] = 'autoresponder_stats/export/$1/open';
$route['autoresponder-stats-export/(:num)/click'] = 'autoresponder_stats/export/$1/click';
$route['autoresponder-stats-export/(:num)/unsubscribed'] = 'autoresponder_stats/export/$1/unsubscribed';
$route['autoresponder-stats-export/(:num)/bounced'] = 'autoresponder_stats/export/$1/bounced';
$route['autoresponder-stats-export/(:num)/complaints'] = 'autoresponder_stats/export/$1/complaints';
$route['get-autoresponder-stats-graph'] = "autoresponder_stats/getGraph";
/*---------- Autoresponder stats ends -----------------*/

/*---------- web form starts --------------------------*/
$route['forms'] = 'form/index';
$route['form-settings'] = 'form/formSettings';
$route['form-template'] = 'form/formTemplate';
$route['form-editor'] = 'form/formEditor';
$route['form-settings/(:num)'] = 'form/formSettings/$1';
$route['form-template/(:num)'] = 'form/formTemplate/$1';
$route['form-editor/(:num)'] = 'form/formEditor/$1';
$route['form-publish/(:num)'] = 'form/formPublish/$1';
$route['delete-form/(:num)'] = 'form/deleteForm/$1';
$route['delete-multiple-form'] = 'form/deleteMultiForm';
$route['view-form-embed'] = 'form/viewFormEmbed';
$route['view-webform(:num).js'] = 'webform/viewWebformJS/$1';
$route['view-webform/(:num)'] = 'webform/index/$1';
$route['webform-submit'] = 'webform/webformSubmit';
$route['webform-submit-html'] = 'webform/webformSubmitHtml';
$route['subscriber-verify/(:any)'] = 'webform/subscriberVerify/$1';
/*---------- web form ends ----------------------------*/


/*---------- API System starts -----------------------*/
//$route['api'] = 'api/index';
/*---------- API System ennds -----------------------*/


/*---------- Stats Track starts -----------------------*/
$route['trackmail'] = 'trackmail/index';
$route['open-(:any)mail'] = 'trackmail/openEmail/$1';
$route['unsubscribe-(:any)email'] = 'unsubscribe/index/$1';
$route['click-(:any)email(:any)'] = 'trackmail/clickEmail/$1/$2';
$route['bounce-(:any)email'] = 'trackmail/bounceEmail/$1';
$route['check-bounce'] = 'bounce/index';
/*---------- Stats Track ends -------------------------*/

/*---------- For Cron ---------------------------------*/
$route['global-cron'] = 'cron/globalCron';
$route['run-cron'] = 'cron/index';
$route['autoresponder-cron'] = 'cron/sendAutoresponder';
$route['add-in-queue-cron'] = 'cron/addDataInQueue';
$route['send-newsletter-cron/(:any)'] = 'cron/sendNewsletter/$1';
/*---------- Cron Ends --------------------------------*/

/*---------- Settings ---------------------------------*/
$route['send-newsletter-cron/(:any)'] = 'cron/sendNewsletter/$1';
$route['settings/user'] = 'settings/user_setting';
$route['from-email-verify/(:any)'] = 'welcome/fromEmailVerify/$1';
$route['delete-mail/(:any)'] = 'settings/delete_mail/$1';
$route['send-smtp-test-mail'] = 'settings/sendTestMail';
$route['settings/app'] = 'settings/app_setting';
$route['settings/mail'] = 'settings/mail_setting';
$route['settings/bounce'] = 'settings/bounce_setting';
$route['settings/label'] = 'settings/label_setting';
$route['settings/cron'] = 'settings/cron_setting';
$route['settings/change-password'] = 'settings/change_password';
$route['settings/social-settings'] = 'settings/social_settings';
$route['settings/api-settings'] = 'settings/api_settings';
/*---------- Settings Ends ----------------------------*/

/*---------- Editor By mukesh -------------------------*/
$route['uploadmediaimg'] = 'editor/uploadmediaimg';
/*---------- Editor By mukesh -------------------------*/

/*---------- training pdf --------------------*/
$route['training-pdf'] = 'training_pdf/index';
/*---------- Install route ends--------------------*/

/*---------- Faq By trilok -------------------------*/
$route['faq'] = 'faq/index';
$route['faq/(:any)'] = 'faq/index/$1';
/*---------- Faq By trilok -------------------------*/

/*---------- Change Log By trilok -------------------------*/
$route['update-log'] = 'update_version/updateLog';
$route['export-log'] = 'update_version/export_log';
/*---------- Change Log By trilok end-------------------------*/


/* End of file routes.php */
/* Location: ./application/config/routes.php */