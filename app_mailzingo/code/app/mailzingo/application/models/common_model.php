<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Common_Model extends CI_Model {

	function __construct()
	{ 
	
		global $URI, $CFG, $IN;
        $ci = get_instance(); 
        $ci->load->config('config');

        //$ci->config->set_item('facebook_url',$row->facebook_url); 
		
		if($this->session->userdata('SAG_mem_id')!='')
		  {
			$this->db->where('parent_id','0');
			$this->db->where('status','Active');
			$this->db->order_by('display_order','ASC');
			$query = $this->db->get('tbl_sag_manager');
			$managers	=	$query->result();

			for($i=0;isset($managers[$i]);$i++)
				$managers[$i]->submanagers	=	$this->getSubmanagers($managers[$i]->mng_id);
				$ci->config->set_item('memberManagers',$managers);
		  } 
 	
	}
	protected function getSubmanagers($id = null)
  	 {
		$this->db->where('parent_id',$id);
		$this->db->where('status','active');
		$this->db->order_by('display_order','ASC');
		$query = $this->db->get('tbl_sag_manager');
		$managers	=	$query->result();
		return $managers;
	 } 
	public function checkSagPanelLogin(){
		if($this->session->userdata('SAG_mem_id')=='')
		 {
		   redirect($this->config->item('adminName').'/login');
		 }
	}
    public function checkForPageAccess($mng_id=null,$permission=null,$redirect=null)
	 {
	   $SAG_mem_id = $this->session->userdata('SAG_mem_id');

	   $this->db->select('privileges,permission');
	   $this->db->where('id',$SAG_mem_id);
	   $this->db->where('status','active');
	   $query = $this->db->get('tbl_sag_user');
	   $result = $query->row();
	   
	   $privileges = explode(',',$result->privileges);
	   if(in_array($mng_id,$privileges))
	    {
		  $access = 'yes';
		}
	   else
	    { $access = 'no'; }
	   if($permission)
	    {
		  $pr = unserialize($result->permission);
		  if(in_array($permission,$pr[$mng_id]))
		   {
		     $access = 'yes';
		   } else { $access = 'no'; }

		}
		if($redirect=='redirect' && $access=='no')
		 { redirect($this->config->item('adminName').'/access-denied'); }
		return $access;
	   

	 }
	
	function getSingleFieldFromAnyTable($field_name=null, $condition_coloum=null, $condition_value=null, $table_name=null) {
        $this->db->select($field_name);
        $this->db->where($condition_coloum, $condition_value);
        $query = $this->db->get($table_name);
        $data = $query->row();
        return $data->$field_name;
    }
	function getCountAllFromAnyTable($condition_coloum=null,$condition_value=null,$table_name=null,$status=null)
	{
		$this->db->where($condition_coloum,$condition_value);
		if($status)
		$this->db->where('status',$status);
		$query = $this->db->get($table_name); 
	 	$nums = $query->num_rows();
		return $nums;
	}
	
	
}
