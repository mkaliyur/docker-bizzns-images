<?php
defined('BASEPATH') OR exit('No direct script access allowed');
 
class Settings_Model extends CI_Model {

	var $tableWeb = 'tbl_website_settings';
	var $tableMember = 'tbl_member';
	var $tableFrom_email = 'tbl_from_email';
	var $tableExternalApi = 'tbl_external_mail_api';
	
        function getUser($member_id){

            $this->db->from($this->tableMember);
            $this->db->where('mem_id', $member_id);
            $query = $this->db->get();
            return $query->row();
	 }
        
         function getFrom_email($member_id){

            $this->db->from($this->tableFrom_email);
            $this->db->where('member_id', $member_id);
            $query = $this->db->get();
            return $query->result();
	 }
         
         
         function updateUser($member_id,$filename=null){
		     
			 if($this->input->post('name'))
			 $this->db->set('name',$this->input->post('name'));
			 if($this->input->post('country'))
			 $this->db->set('country',$this->input->post('country'));
			 if($this->input->post('address'))
			 $this->db->set('address',$this->input->post('address'));
			 if($this->input->post('state'))
			 $this->db->set('state',$this->input->post('state'));
			 if($this->input->post('pin_code'))
			 $this->db->set('pin_code',$this->input->post('pin_code'));
			 if($this->input->post('city'))
			 $this->db->set('city',$this->input->post('city'));
			 if($filename)
			 $this->db->set('profile_pic',$filename);
			 if($this->input->post('business_name'))
			 $this->db->set('business_name',$this->input->post('business_name'));
			 $this->db->where('mem_id',$member_id);
             $this->db->update($this->tableMember);
                     }
        function insertMail($mail,$member_id=null,$verify_code=null){
            $this->db->insert($this->tableFrom_email,array('member_id'=> $member_id ,'email'=>$mail,'verify_code'=>$verify_code,'status'=>'unvarify','add_time'=>time()));
			return $this->db->insert_id();
            
        }
		function verifyEmail($verify_code)
		{
		   $this->db->set('status','varify');
		   $this->db->set('verify_code','');
		   $this->db->where('verify_code',$verify_code);
		   $this->db->update($this->tableFrom_email);
		}
        function getCountMail($mail){
            $this->db->select('email');
            $this->db->from($this->tableFrom_email);
            $this->db->where('email', $mail);
            $query = $this->db->get();
            return $query->num_rows;
        }
#END User

#START App

    function getWeb($member_id){
        $this->db->select('*');
        $this->db->from($this->tableWeb);
        $this->db->where('member_id', $member_id);
        $query = $this->db->get();
        return $query->row();
    }
            
    function updateApp($member_id,$update_array=null){
        $this->db->update($this->tableWeb, $update_array, "member_id = $member_id");
    }

    
#START Mail settings
    
    function updateMailSettings($member_id){
//        $this->db->update($this->tableWeb, $update_array, "member_id = $member_id");
    
            
            $this->db->set('mail_type',$this->input->post('mail_type'));
            if($this->input->post('SMTP_hostname'))
            $this->db->set('SMTP_hostname',$this->input->post('SMTP_hostname'));
            if($this->input->post('SMTP_username'))
            $this->db->set('SMTP_username',$this->input->post('SMTP_username'));
            if($this->input->post('SMTP_password'))
            $this->db->set('SMTP_password',$this->input->post('SMTP_password'));
            if($this->input->post('SMTP_port'))
            $this->db->set('SMTP_port',$this->input->post('SMTP_port'));
			if($this->input->post('mail_per_hour'))
			{
				$this->db->set('mail_hour_limit',$this->input->post('mail_per_hour'));
				if($this->input->post('mail_per_hour')>60){
					$d = round($this->input->post('mail_per_hour')/60);
				}
				else{
					$d = 1;
				}
				$this->db->set('mail_minute_limit',$d);
			}
			
            $this->db->where('member_id',$member_id);
            $this->db->update($this->tableWeb);
//        echo $this->db->last_query();die;
        
    }

#START Bounce settings
    function updateBounceSettings($member_id){
//        $this->db->update($this->tableWeb, $update_array, "member_id = $member_id");
        
        if($this->input->post('bounce_type'))
		$this->db->set('bounce_type',$this->input->post('bounce_type'));
        if($this->input->post('bounce_address'))
		$this->db->set('bounce_address',$this->input->post('bounce_address'));
		if($this->input->post('bounce_server'))
		$this->db->set('bounce_server',$this->input->post('bounce_server'));
       // if($this->input->post('bounce_username'))
		//$this->db->set('bounce_username',$this->input->post('bounce_username'));
        if($this->input->post('bounce_password'))
		$this->db->set('bounce_password',$this->input->post('bounce_password'));
        if($this->input->post('account_type'))
		$this->db->set('account_type',$this->input->post('account_type'));
		if($this->input->post('bounce_port'))
		$this->db->set('bounce_port',$this->input->post('bounce_port'));
		$this->db->where('member_id',$member_id);
		$this->db->update($this->tableWeb);
        
        
        
    }

    function updateLabelSettings($member_id,$app_logo=null,$favicon=null){
       
	   $this->db->set('app_name',$this->input->post('app_name'));
	   $this->db->set('app_footer',$this->input->post('app_footer'));
	   if($app_logo)
	   $this->db->set('app_logo',$app_logo);
	   if($favicon)
	   $this->db->set('favicon',$favicon);
	   $this->db->where('member_id',$member_id);
	   $this->db->update($this->tableWeb);
    }
    

                     
    public function getCampaignList($member_id,$status=null,$sort_by=null,$search_by=null)
	{
		if($member_id)
		$this->db->where('member_id',$member_id);
		if($status)
		$this->db->where('status',$status);
		if($sort_by=='add_time')
		$this->db->order_by($sort_by,'desc');
		if($sort_by=='settings_name')
		$this->db->order_by($sort_by,'asc');
		if($search_by)
		$this->db->like('settings_name',$search_by);
		$query = $this->db->get($this->tableName);
		return $query->result();
	}
        
        public function deleteMail($id){
                $this->db->where('id',$id);
		$this->db->delete($this->tableFrom_email);
        }

	function updatePassword(){
	    $this->db->set('password',md5($this->input->post('new_password')));
		$this->db->where('mem_id',$this->session->userdata('mem_id'));
		$this->db->update($this->tableMember);
	}
	function updateAPIKey($api_key,$member_id){
	    $this->db->set('api_key',$api_key);
		$this->db->where('member_id',$member_id);
		$this->db->update($this->tableWeb);
	}














        public function addCampaign($member_id)
	{
		$this->db->set('member_id',$member_id);
		$this->db->set('settings_name',$this->input->post('settings_name'));
		if($this->input->post('settings_description'))
		$this->db->set('settings_description',$this->input->post('settings_description'));
		$this->db->set('add_time',time());
		$this->db->set('ip',$_SERVER['REMOTE_ADDR']);
		$this->db->insert($this->tableName);
	}
	public function updateCampaign($id)
	{
		if($this->input->post('settings_name'))
		$this->db->set('settings_name',$this->input->post('settings_name'));
		if($this->input->post('settings_description'))
		$this->db->set('settings_description',$this->input->post('settings_description'));
		$this->db->where('settings_id',$id);
		$this->db->update($this->tableName);
	}
	public function getCampaignById($id)
	{
		$this->db->where('settings_id',$id);
		$query = $this->db->get($this->tableName);
		return $query->row();
	}
	public function deleteCampaign($id)
	{
		$this->db->where('settings_id',$id);
		$this->db->delete($this->tableName);
		
		$this->db->where('settings_id',$id);
		$this->db->delete($this->tableContact);
	}
	public function removeMultipleCampaign($settings_ids)
	{
		$this->db->where_in('settings_id',$settings_ids);
		$this->db->delete($this->tableName);
		$this->removeMultipleContact($settings_ids);
	}
	public function removeMultipleContact($settings_ids)
	{
		$this->db->where_in('settings_id',$settings_ids);
		$this->db->delete($this->tableContact);
	}
	public function copyCampaign($from_settings,$to_settings)
	{
		$this->db->where('settings_id',$from_settings);
		$query = $this->db->get($this->tableContact);
		$data = $query->result();
		
		foreach($data as $val)
		{
			$this->db->set('member_id',$val->member_id);
			$this->db->set('settings_id',$to_settings);
			$this->db->set('name',$val->name);
			$this->db->set('email',$val->email);
			$this->db->set('add_time',time());
			$this->db->set('ip',$val->ip);
			$this->db->insert($this->tableContact);
	  }
	}
	public function getCampaignListMove($member_id,$settings_id=null)
	{
		if($member_id)
		$this->db->where('member_id',$member_id);
		if($settings_id)
		$this->db->where('settings_id !=',$settings_id);
		$this->db->where('status','active');
		$query = $this->db->get($this->tableName);
		return $query->result();
	}
	public function getSocialOptions($member_id)
	{
		if($member_id)
		$this->db->where('member_id',$member_id);
		$query = $this->db->get($this->tableWeb);
		return $query->row($this->tableWeb);
	}
	public function updateSocialSettings($member_id)
	{
		$this->db->set('fb_url',$this->input->post('fb_url'));
		$this->db->set('tw_url',$this->input->post('tw_url'));
		$this->db->set('insta_url',$this->input->post('insta_url'));
		$this->db->set('gplus_url',$this->input->post('gplus_url'));
		if($member_id)
		$this->db->where('member_id',$member_id);
		$this->db->update($this->tableWeb);
		 
	}
	function updateExternalMailSettings($member_id,$option_name=null,$credentials=null)
	{
		//update in web_settings
		$this->db->set('mail_type',$option_name); 
		$this->db->update($this->tableWeb);
		 
		//Update on option table
		$this->db->select("*");
		$this->db->where('api_name',$option_name);
		$query = $this->db->get($this->tableExternalApi);
		if($query->num_rows>0){
			$this->db->where('api_name',$option_name);
			$this->db->set('api_value',serialize($credentials));
			$this->db->update($this->tableExternalApi);
		}
		else
		{
			$this->db->set('api_name',$option_name);
			$this->db->set('api_value',serialize($credentials));
			$this->db->insert($this->tableExternalApi);
		}
		 
	}
	function getExternalApi($option_name)
	{
		$this->db->select("api_value");
		$this->db->where('api_name',$option_name);
		$query = $this->db->get($this->tableExternalApi);
		if($query->num_rows>0){
			$data = $query->row();
			return $values = unserialize($data->api_value);
		}
		return 0;
	}
	
	
}
