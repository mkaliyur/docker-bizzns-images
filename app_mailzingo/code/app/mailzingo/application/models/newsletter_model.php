<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Newsletter_Model extends CI_Model {

	var $tableName = 'tbl_newsletter';
	var $tableContact = 'tbl_contact';
	var $tableStats = 'tbl_statistics';
	var $tableQueue = 'tbl_email_queue';
    var $tableUnsubscribe = 'tbl_unsubscribe_list'; 
	var $tableCronURL = 'tbl_cron_urls';
	var $tableFromEmail = 'tbl_from_email';
	var $tableTemplate = 'tbl_template';
	var $tableAttatch = 'tbl_file_attatch';
	
	public function getAllCount($member_id,$status=null){
	 	
		if($status)
		$this->db->where('status',$status);
		if($member_id)
		$this->db->where('member_id',$member_id);
		$query = $this->db->get($this->tableName);
		return $query->num_rows();
	 }
	public function getNewsletterList($member_id=null,$status=null,$start=null,$content_per_page=null,$index=null)
	{       
		if($member_id)
		$this->db->where('member_id',$member_id);
		if($status)
		$this->db->where('status',$status);
		$this->db->order_by('id','desc');
		$query = $this->db->get($this->tableName,$content_per_page,$start);
                if(!$index)
                return $query->result();
                if($index)
		return $query->num_rows();
	}
        
        public function get_all_content($start,$content_per_page=null)
    {
        $sql = "SELECT * FROM  tbl_newsletter where member_id=1 LIMIT $start,$content_per_page";       
        $result = $this->db->query($sql)->result();
        return $result;
    }
        
	public function getNewsletterById($member_id,$id=null)
	{
		if($member_id)
		$this->db->where('member_id',$member_id);
		if($id)
		$this->db->where('id',$id);
		$query = $this->db->get($this->tableName);
		return $query->row();
	}
	public function addNewsletter($insertArray)
	{
		$this->db->set('member_id',$insertArray['member_id']);
		$this->db->set('from_name',$insertArray['from_name']);
		$this->db->set('from_email',$insertArray['from_email']);
		$this->db->set('reply_to',$insertArray['reply_to']);
		$this->db->set('message_name',$insertArray['message_name']);
		$this->db->set('subject',$insertArray['subject']);
		$this->db->set('template_id',$insertArray['template_id']);
		$this->db->set('template_text',$insertArray['template_text']);
		$this->db->set('message_type',$insertArray['message_type']);
		if($insertArray['in_campaign_id'])
		$this->db->set('in_campaign_id',$insertArray['in_campaign_id']);
		if($insertArray['ex_campaign_id'])
		$this->db->set('ex_campaign_id',$insertArray['ex_campaign_id']);
		if($insertArray['sep_list_id'])
		$this->db->set('sep_list_id',$insertArray['sep_list_id']);
		if($insertArray['contact_id'])
		$this->db->set('contact_id',$insertArray['contact_id']);
		if($insertArray['image'])
		$this->db->set('image',$insertArray['image']);
		if($insertArray['schedule_time'])
		$this->db->set('schedule_date',$insertArray['schedule_time']);
		if($insertArray['schedule_time'])
		$this->db->set('schedule_timestamp',strtotime($insertArray['schedule_time']));
		if($insertArray['status'])
		$this->db->set('status',$insertArray['status']);
		$this->db->set('add_time',time());
		$this->db->set('ip',$_SERVER['REMOTE_ADDR']);
		$this->db->insert($this->tableName);
		return $this->db->insert_id();
	}
	public function updateNewsletter($insertArray,$newsletter_id=null)
	{
		$this->db->set('member_id',$insertArray['member_id']);
		$this->db->set('from_name',$insertArray['from_name']);
		$this->db->set('from_email',$insertArray['from_email']);
		$this->db->set('reply_to',$insertArray['reply_to']);
		$this->db->set('message_name',$insertArray['message_name']);
		$this->db->set('subject',$insertArray['subject']);
		$this->db->set('template_id',$insertArray['template_id']);
		$this->db->set('template_text',$insertArray['template_text']);
		$this->db->set('message_type',$insertArray['message_type']);
		if($insertArray['in_campaign_id'])
		$this->db->set('in_campaign_id',$insertArray['in_campaign_id']);
		if($insertArray['ex_campaign_id'])
		$this->db->set('ex_campaign_id',$insertArray['ex_campaign_id']);
		if($insertArray['sep_list_id'])
		$this->db->set('sep_list_id',$insertArray['sep_list_id']);
		if($insertArray['contact_id'])
		$this->db->set('contact_id',$insertArray['contact_id']);
		if($insertArray['image'])
		$this->db->set('image',$insertArray['image']);
		if($insertArray['schedule_time'])
		$this->db->set('schedule_date',$insertArray['schedule_time']);
		if($insertArray['schedule_time'])
		$this->db->set('schedule_timestamp',strtotime($insertArray['schedule_time']));
		if($insertArray['status'])
		$this->db->set('status',$insertArray['status']);
		$this->db->set('add_time',time());
		$this->db->set('ip',$_SERVER['REMOTE_ADDR']);
		$this->db->where('id',$newsletter_id);
		$this->db->update($this->tableName);
	}
	public function deleteNewsletter($id)
	{
	   $this->db->where('id',$id);
	   $this->db->delete($this->tableName);
	   
	   $this->db->where('newsletter_id',$id);
	   $this->db->delete($this->tableStats);
	   
	   $this->db->where('newsletter_id',$id);
	   $this->db->delete($this->tableQueue);
	   
	   $this->db->where('newsletter_id',$id);
	   $this->db->delete($this->tableAttatch);
	   
	   $this->db->where('newsletter_id',$id);
	   $this->db->delete($this->tableCronURL);
	}
	public function removeMultipleNewsletter($id)
	{
	   $this->db->where_in('id',$id);
	   $this->db->delete($this->tableName);
		
	   $this->db->where_in('newsletter_id',$id);
	   $this->db->delete($this->tableStats);
	   
	   $this->db->where_in('newsletter_id',$id);
	   $this->db->delete($this->tableQueue);
	   
	   $this->db->where_in('newsletter_id',$id);
	   $this->db->delete($this->tableAttatch);
	   
	   $this->db->where_in('newsletter_id',$id);
	   $this->db->delete($this->tableCronURL);
	}
	public function getQueueNewsletter($cron_id)
	{
		$this->db->select($this->tableQueue.'.id as queue_id,'.$this->tableQueue.'.newsletter_id,'.$this->tableContact.'.email,name,address,city,state,country,zip,phone,'.$this->tableContact.'.id as contactid');
		$this->db->where($this->tableQueue.'.cron_id',$cron_id);
		$this->db->join($this->tableContact,$this->tableContact.'.id='.$this->tableQueue.'.contact_id','left');
		$this->db->order_by($this->tableQueue .'.id','desc');
		$query = $this->db->get($this->tableQueue);
		return $query->result();
	}
	public function getTemplateDataByNewsletter($newsletter_id)
	{
		$this->db->select('subject,from_name,from_email,reply_to,template_text,member_id');
		$this->db->where($this->tableName.'.id',$newsletter_id);
		$query = $this->db->get($this->tableName);
		return $query->row();
	}
	public function addEmailQueue($newsletter_id,$contact_id=null,$cron_id=null)
	{
		$this->db->set('cron_id',$cron_id);
		$this->db->set('newsletter_id',$newsletter_id);
		$this->db->set('contact_id',$contact_id);
		$this->db->insert($this->tableQueue);
	}
	public function removeQueueById($id)
	{
	   $this->db->where('id',$id);
	   $this->db->delete($this->tableQueue);
	}
	public function chanegNewsletterStatus($newsletter_id)
	{
	   $this->db->where('newsletter_id',$newsletter_id);
	   $query = $this->db->get($this->tableQueue);
	   $count = $query->num_rows();
	   if($count==0)
	    {
			$this->db->set('status','sent');
			$this->db->set('sent_time',time());
			$this->db->where('id',$newsletter_id);
			$this->db->update($this->tableName);
		}
	}
	public function checkQueueNewsletter($cron_id)
	{
		$this->db->where($this->tableQueue.'.cron_id',$cron_id);
		$query = $this->db->get($this->tableQueue);
		return $query->num_rows();
	}
	public function removeCronURL($cron_id)
	{
		$this->db->where('url',site_url('send-newsletter-cron/'.$cron_id));
		$query = $this->db->delete($this->tableCronURL);
	}
	public function getFromEmailList($member_id)
	{
	    $this->db->where($this->tableFromEmail.'.member_id',$member_id);
		$this->db->where($this->tableFromEmail.'.status','varify');
		$query = $this->db->get($this->tableFromEmail);
		return $query->result();
	}
	public function saveTemplate($member_id,$template_name=null,$template_text=null,$editor_type=null,$template_screenshot=null)
	{
		$this->db->set('member_id',$member_id);
		$this->db->set('template_name',$template_name);
		$this->db->set('template_text',$template_text);
		$this->db->set('template_type','draft');
		$this->db->set('editor_type',$editor_type);
		$this->db->set('template_screenshot',$template_screenshot);
		$this->db->set('add_time',time());
		$this->db->set('ip',$_SERVER['REMOTE_ADDR']);
		$this->db->insert($this->tableTemplate);
	}
	public function addAttatchFile($file_name,$newsletter_id=null)
	{
		$this->db->set('file_name',$file_name);
		$this->db->set('newsletter_id',$newsletter_id);
		$this->db->set('type','newsletter');
		$this->db->insert($this->tableAttatch);
	}
	public function getAttatchFile($newsletter_id)
	{
		$this->db->select('file_name');
		$this->db->where('newsletter_id',$newsletter_id);
		$this->db->where('type','newsletter');
		$query = $this->db->get($this->tableAttatch);
		return $query->result();
	}
	public function deleteAttatchFile($newsletter_id)
	{
		$this->db->where('newsletter_id',$newsletter_id);
		$this->db->where('type','autoresponder');
		$this->db->delete($this->tableAttatch);
	}
	public function deleteFromCronTable($newsletter_id)
	{
		$this->db->where('newsletter_id',$newsletter_id);
		$this->db->delete($this->tableCronURL);
	}
	public function removeQueueByNewsletterId($newsletter_id)
	{
	   $this->db->where('newsletter_id',$newsletter_id);
	   $this->db->delete($this->tableQueue);
	}
	public function getSMTPDetail($member_id)
	{
       $this->db->select('mail_type,SMTP_hostname,SMTP_username,SMTP_password,SMTP_port,bounce_type,bounce_address');
	   $this->db->where('member_id',$member_id);
	   $query = $this->db->get('tbl_website_settings');
	   return $data = $query->row();	
	}
	public function getSocialLinks($member_id)
	{
	   $this->db->select('fb_url,tw_url,insta_url,gplus_url');
	   $this->db->where('member_id',$member_id);
	   $query = $this->db->get('tbl_website_settings');
	   return $data = $query->row();	
	}
	public function getUserAddress($member_id)
	{
	   $this->db->select('city,state,country,address,pin_code');
	   $this->db->where('mem_id',$member_id);
	   $query = $this->db->get('tbl_member');
	   return $data = $query->row();	
	}
	public function checkForHourLimit($member_id)
	{ 
	   $this->db->select('mail_minute_limit,current_hour_mail,last_mail_time');
	   $this->db->where('member_id',$member_id);
	   $query = $this->db->get('tbl_website_settings');
	   $data = $query->row();	
	   
	   $current_hour_mail = $data->current_hour_mail;
	   $mail_minute_limit = $data->mail_minute_limit;
	   $last_mail_time = date('d-m-y H i',$data->last_mail_time);
	   $current_time = date('d-m-y H i',time()); 
	   $limit = 'not done';

	   if($current_time>$last_mail_time)
	   { 
	     $this->db->set('last_mail_time',time());
		 $this->db->set('current_hour_mail',1);
	     $this->db->where('member_id',$member_id);
	     $this->db->update('tbl_website_settings');
		 
	   } else if($current_hour_mail<$mail_minute_limit){

	      $this->db->set('current_hour_mail', 'current_hour_mail + 1', FALSE);
		  $this->db->set('last_mail_time',time());
		  $this->db->where('member_id',$member_id);
	      $this->db->update('tbl_website_settings');
		  
	   } else {

		  $limit = 'done';
	   }
	   
	   return $limit;
	}
}
