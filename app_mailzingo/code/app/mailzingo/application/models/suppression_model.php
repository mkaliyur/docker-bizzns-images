<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Suppression_Model extends CI_Model {

	var $tableName = 'tbl_suppression';
	
	
	public function addSuppression($member_id,$list_id,$email)
	{
		
		$this->db->set('member_id',$member_id);
		$this->db->set('list_id',$list_id);
		$this->db->set('email',$email);
		$this->db->set('add_time',time());
		$this->db->set('ip',$_SERVER['REMOTE_ADDR']);
		$this->db->insert($this->tableName);
	}
	public function getSuppressionList($list_id,$start,$content_per_page,$index)
	{
		if($list_id)
		$this->db->where('list_id',$list_id);
		$query = $this->db->get($this->tableName,$content_per_page,$start);
                if(!$index)
		return $query->result();
                if($index)
		return $query->num_rows();
	}
	public function deleteSuppression($id)
	{
		$this->db->where('id',$id);
		$query = $this->db->delete($this->tableName);
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	public function getSuppressionById($id)
	{
		$this->db->where('id',$id);
		$query = $this->db->get($this->tableName);
		return $query->row();
	}
	public function copySuppression($id,$list_id)
	{
		$detail = $this->getSuppressionById($id);
		
		$this->db->set('member_id',$detail->member_id);
		$this->db->set('list_id',$list_id);
		$this->db->set('email',$detail->email);
		$this->db->set('add_time',time());
		$this->db->set('ip',$_SERVER['REMOTE_ADDR']);
		$this->db->insert($this->tableName);
	}
	public function moveSuppression($id,$to_list)
	{
		if($to_list)
		$this->db->set('list_id',$to_list);
		$this->db->where('id',$id);
		$this->db->update($this->tableName);
	}
	public function getSuppressionByListId($list_id)
	{
		$this->db->where_in('list_id',$list_id);
		$query = $this->db->get($this->tableName);
		return $query->result();
	}
	public function getSuppressionByListIdForNewsletter($list_id)
	{
		$this->db->select('email');
		$this->db->where_in('list_id',$list_id);
		$query = $this->db->get($this->tableName);
		return $query->result_array();
	}
	
}
