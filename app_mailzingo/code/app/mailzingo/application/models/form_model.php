<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Form_Model extends CI_Model {

	var $tableName = 'tbl_webform';
	var $tableTemplate = 'tbl_form_template';
	var $tableContact = 'tbl_contact';

	public function getFormlist($member_id)
	{
		$this->db->where('member_id',$member_id);
		$this->db->order_by('id','desc');
		$query = $this->db->get($this->tableName);
		return $query->result();
	}
	function getTemplateList($member_id,$limit=null){
	 	
		if($member_id)
		$this->db->where('member_id',$member_id);
		if($limit)
		$this->db->limit($limit);
		$query = $this->db->get($this->tableTemplate);
		return $query->result();
	 }
	public function getTemplateById($id)
	{
		$this->db->where('id',$id);
		$query = $this->db->get($this->tableTemplate);
		return $query->row();
	}
	public function addForm($member_id,$form_session)
	{
		$this->db->set('member_id',$member_id);
		$this->db->set('form_name',$form_session['form_name']);
		$this->db->set('campaign_ids',implode(',',$form_session['campaign_id']));
		$this->db->set('form_html',$this->input->post('template_text'));
		$this->db->set('template_id',$form_session['template_id']);
		$this->db->set('thankyou_page',$this->input->post('thankyou_page'));
		if($this->input->post('thankyou_custom'))
		$this->db->set('thankyou_custom',$this->input->post('thankyou_custom'));
		$this->db->set('already_subscribe',$this->input->post('already_subscribe'));
		if($this->input->post('alreadysub_custom'))
		$this->db->set('alreadysub_custom',$this->input->post('alreadysub_custom'));
		if($this->input->post('double_otp'))
		$this->db->set('double_otp',$this->input->post('double_otp'));
		$this->db->set('add_time',time());
		$this->db->insert($this->tableName);
		return $this->db->insert_id();
	}
	public function updateForm($member_id,$form_session=null,$form_id=null)
	{
		$this->db->set('member_id',$member_id);
		$this->db->set('form_name',$form_session['form_name']);
		$this->db->set('campaign_ids',implode(',',$form_session['campaign_id']));
		$this->db->set('template_id',$form_session['template_id']);
		$this->db->set('form_html',$this->input->post('template_text'));
		$this->db->set('thankyou_page',$this->input->post('thankyou_page'));
		if($this->input->post('thankyou_custom'))
		$this->db->set('thankyou_custom',$this->input->post('thankyou_custom'));
		$this->db->set('already_subscribe',$this->input->post('already_subscribe'));
		if($this->input->post('alreadysub_custom'))
		$this->db->set('alreadysub_custom',$this->input->post('alreadysub_custom'));
		if($this->input->post('double_otp'))
		$this->db->set('double_otp',$this->input->post('double_otp'));
		$this->db->where('id',$form_id);
		$this->db->update($this->tableName);
	}
	public function getFormById($id)
	{
		$this->db->where('id',$id);
		$query = $this->db->get($this->tableName);
		return $query->row();
	}
	public function addSubscriber($member_id,$campaign_id,$varification_code,$status)
	{
		$subscribercheck = $this->checkSubscriber($campaign_id,$this->input->post('email'));
		if($subscribercheck!=0)
		{ return 'notdone'; }
		
		$this->db->set('member_id',trim($member_id));
		$this->db->set('campaign_id',trim($campaign_id)); 
		$this->db->set('email',$this->input->post('email')); 
		if($this->input->post('name'))
		$this->db->set('name',$this->input->post('name')); 
		if($this->input->post('address'))
		$this->db->set('address',$this->input->post('address')); 
		if($this->input->post('city'))
		$this->db->set('city',$this->input->post('city')); 
		if($this->input->post('state'))
		$this->db->set('state',$this->input->post('state')); 
		if($this->input->post('country'))
		$this->db->set('country',$this->input->post('country')); 
		if($this->input->post('zip'))
		$this->db->set('zip',$this->input->post('zip')); 
		if($this->input->post('phone'))
		$this->db->set('phone',$this->input->post('phone')); 
		$this->db->set('add_from','form');
		if($varification_code)
		$this->db->set('varification_code',$varification_code);
		$this->db->set('status',$status);
		$this->db->set('add_time',time());
		$this->db->set('ip',$_SERVER['REMOTE_ADDR']);
		$this->db->insert($this->tableContact);
		return $this->db->insert_id();
	}
	public function checkSubscriber($campaign_id,$email=null)
	{
		$this->db->where('campaign_id',$campaign_id);
		$this->db->where('email',$email);
		$query = $this->db->get($this->tableContact);
		return $query->num_rows();
	}
	public function getAllCount($member_id,$status=null){
	 	
		if($status)
		$this->db->where('status',$status);
		if($member_id)
		$this->db->where('member_id',$member_id);
		$query = $this->db->get($this->tableContact);
		return $query->num_rows();
	 }
	public function deleteForm($id)
	{
	   $this->db->where('id',$id);
	   $this->db->delete($this->tableName);
	}
	public function removeMultipleForm($id)
	{
	   $this->db->where_in('id',$id);
	   $this->db->delete($this->tableName);
	}
	public function verifySubscriber($varification_code)
	{
	  $this->db->select('email');
	  $this->db->where('varification_code',$varification_code);
	  $query = $this->db->get($this->tableContact);
	  $email = $query->row()->email;
	  
	  $this->removeFromUnsubscribeList($email);
	  $this->db->set('status','active');
	  $this->db->set('varification_code','');
	  $this->db->where('varification_code',$varification_code);
	  $this->db->update($this->tableContact);
	}
	public function updateImage($image,$form_id=null)
	{
		$formdetail = $this->getFormById($form_id);
		unlink('./assets/uploads/form_images/'.$formdetail->image);
		$this->db->set('image',$image);
		$this->db->where('id',$form_id);
		$this->db->update($this->tableName);
	}
	public function getSubscriberDetail($id)
	{
		$this->db->where('id',$id);
		$query = $this->db->get($this->tableContact);
		return $query->row();
	}
	public function removeFromUnsubscribeList($email)
	{ 
		$this->db->where('email',trim($email));
		$this->db->delete('tbl_unsubscribe_list');
	}
}
