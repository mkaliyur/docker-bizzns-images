<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Version_Model extends CI_Model {

	var $tableName = 'tbl_versions';

	public function getAllCount($member_id,$status){
	 	
		if($status)
		$this->db->where('status',$status);
		if($member_id)
		$this->db->where('member_id',$member_id);
		$query = $this->db->get($this->tableName);
		return $query->num_rows();
	 }
}
