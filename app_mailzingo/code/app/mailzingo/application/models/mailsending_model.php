<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mailsending_Model extends CI_Model {
  
	function sendmail($mailArray) 
	{
		if($mailArray['mail_type']!='')
		 {
		    $mail_type = $mailArray['mail_type'];
			$SMTP_hostname = $mailArray['SMTP_hostname'];
			$SMTP_port = $mailArray['SMTP_port'];
			$SMTP_username = $mailArray['SMTP_username'];
			$SMTP_password = $mailArray['SMTP_password'];
			$bounce_type = $mailArray['bounce_type'];
			$bounce_address = $mailArray['bounce_address'];
			
		 } else {
		 
			$this->db->select('mail_type,SMTP_hostname,SMTP_username,SMTP_password,SMTP_port,bounce_type,bounce_address');
			$this->db->where('member_id',$mailArray['member_id']);
			$query = $this->db->get('tbl_website_settings');
			$data = $query->row();
			
		    $mail_type = $data->mail_type;
			$SMTP_hostname = $data->SMTP_hostname;
			$SMTP_port = $data->SMTP_port;
			$SMTP_username = $data->SMTP_username;
			$SMTP_password = $data->SMTP_password;
			$bounce_type = $smtpDetail->bounce_type;
			$bounce_address = $smtpDetail->bounce_address;
		 }
			if($mail_type=='smtp')
			{
				$emailConfig = array(
					'protocol' => 'smtp',
					'smtp_host' => $SMTP_hostname,
					'smtp_port' => $SMTP_port,
					'smtp_user' => $SMTP_username,
					'smtp_pass' => $SMTP_password,
					'mailtype'  => 'html',
					'charset'   => 'iso-8859-1'
				);
		   }
			
			// Set your email information
			$stats_code = $mailArray['stats_code'];
			$unsubscribe_link = $mailArray['unsubscribe_link'];
			if($bounce_type=='default')
			$return_path = $SMTP_username;
			else
			$return_path = $bounce_address;

			$from = array('email' => $mailArray['from_email'], 'name' => $mailArray['from_name']);
			if($mailArray['reply_to']=='')
			$reply_to = array('email' => '', 'name' => '');
			else
			$reply_to = array('email' => $mailArray['reply_to'], 'name' => $mailArray['from_name']);
			$to = array('email' => trim($mailArray['to']));
			$subject = $mailArray['subject'];
			$message = $mailArray['message'].'<div style="display:none">{emailpro}'.$stats_code.'{emailpro}</div>';
			$attatchFiles = $mailArray['attatchFiles'];
			$newsletter_id = $mailArray['newsletter_id'];

			$this->load->library('email');
			$this->email->initialize($emailConfig);
			$this->email->set_newline("rn");
			
			if($mail_type=='smtp')
			$this->email->from($from['email'], $from['name'], $return_path);
			else 
			$this->email->from($from['email'], $from['name'], $return_path);

			$this->email->reply_to($reply_to['email'],$reply_to['name']);
			$this->email->to($to);
			$this->email->subject($subject);
			$this->email->message($message);
			if($mailArray['email_type']=='autoresponder')
			{
			  foreach($attatchFiles as $valA)
			  $this->email->attach('./assets/uploads/email_attatchment/auto/'.$newsletter_id.'/'.$valA->file_name);
			} else {
			  foreach($attatchFiles as $valA)
			  $this->email->attach('./assets/uploads/email_attatchment/'.$newsletter_id.'/'.$valA->file_name);
			}
			//$this->email->_set_header('List-Unsubscribe','<mailto:'.$smtpdata->smtp_user.' ?subject=Unsubscribe>, <'.$unsubscribe_link.'>');
			$this->email->set_header('List-Unsubscribe','<mailto:'.$SMTP_username.' ?subject=Unsubscribe>, <'.$unsubscribe_link.'>');
			$this->email->set_header('Feedback-ID', rand());
			
			
			if(!$this->email->send()) {
				show_error($this->email->print_debugger());
			}
			else{
				return true;
			}
	}
	function sendTestMail($mailArray)
	{
			
			$this->db->select('mail_type,SMTP_hostname,SMTP_username,SMTP_password,SMTP_port');
			$this->db->where('member_id',$mailArray['member_id']);
			$query = $this->db->get('tbl_website_settings');
			$data = $query->row();
			if($data->mail_type=='smtp')
			{
				$emailConfig = array(
					'protocol' => 'smtp',
					'smtp_host' => $data->SMTP_hostname,
					'smtp_port' => $data->SMTP_port,
					'smtp_user' => $data->SMTP_username,
					'smtp_pass' => $data->SMTP_password,
					'mailtype'  => 'html',
					'charset'   => 'iso-8859-1'
				);
		   }

			
			// Set your email information
			$from = array('email' => $mailArray['from_email'], 'name' => $mailArray['from_name']);
			$reply_to = array('email' => $mailArray['reply_to'], 'name' => $mailArray['from_name']);
			$to = array('email' => trim($mailArray['contact_email']));
			$subject = $mailArray['subject'];
			$message = $mailArray['message'];
			$attatchFiles = $mailArray['attatchFiles'];
			$attatchmentPath = $mailArray['attatchmentPath'];

			$this->load->library('email');
			$this->email->initialize($emailConfig);
			$this->email->set_newline("rn");

			if($data->mail_type=='smtp')
			$this->email->from($from['email'], $from['name'], $data->SMTP_username);
			else 
			$this->email->from($from['email'], $from['name']);

			$this->email->reply_to($reply_to['email'],$reply_to['name']);
			$this->email->to($to);
			$this->email->subject($subject);
			$this->email->message($message);
			foreach($attatchFiles as $valA)
			 {
			   $this->email->attach('./assets/uploads/'.$attatchmentPath.'/'.$valA);
			  }
			
			$this->email->set_header('Feedback-ID', rand());
			if(!$this->email->send()) {
				show_error($this->email->print_debugger());
			}
			else{
				return true;
			}
	}
	function sendSMTPTestMail($mailArray)
	{
			
			$emailConfig = array(
			    'protocol' => 'smtp',
				'smtp_host' => $mailArray['SMTP_hostname'],
				'smtp_port' => $mailArray['SMTP_port'],
				'smtp_user' => $mailArray['SMTP_username'],
				'smtp_pass' => $mailArray['SMTP_password'],
				'mailtype'  => 'html',
				'charset'   => 'iso-8859-1'
			);
			
			// Set your email information
			$from = array('email' => $mailArray['from_email'], 'name' => $mailArray['from_name']);
			$reply_to = array('email' => $mailArray['reply_to'], 'name' => $mailArray['from_name']);
			$to = array('email' => trim($mailArray['contact_email']));
			$subject = $mailArray['subject'];
			$message = $mailArray['message'];

			$this->load->library('email');
			$this->email->initialize($emailConfig);
			$this->email->set_newline("rn");
			$this->email->from($from['email'], $from['name'], $mailArray['SMTP_username']);
			$this->email->reply_to($reply_to['email'],$reply_to['name']);
			$this->email->to($to);
			$this->email->subject($subject);
			$this->email->message($message);
			
			$this->email->set_header('Feedback-ID', rand());			
			if(!$this->email->send()) {
				return 'error';
			}
			else{
				return 'success';
			}
	}
	
	
	
	function sendMailUsingAmazon($mailArray)
	{
		$m = new SimpleEmailServiceMessage();
		
		$api_value = unserialize($mailArray['api_value']);
		$from_email = $api_value['sending_mail'];
		$secret_key = $api_value['secret_key'];
		$secret_password = $api_value['secret_password'];
		$amazon_region = $api_value['region'];
		$stats_code = $mailArray['stats_code'];
		$subject = $mailArray['subject'];
		$message = $mailArray['message'].'<div style="display:none">{emailpro}'.$stats_code.'{emailpro}</div>';

		$m->addTo(trim($mailArray['to']));
		$m->setFrom($from_email);
		$m->setSubject($subject);
		$html = $message;
		$m->setMessageFromString($text, $html);
		if($amazon_region == 'us_east_1'){
			$region_endpoint = SimpleEmailService::AWS_US_EAST_1;
		}
		else if($amazon_region == 'eu_west_1'){
				$region_endpoint = SimpleEmailService::AWS_US_WEST_1;
		}
		else if($amazon_region == 'us_west_2'){
			$region_endpoint = SimpleEmailService::AWS_US_WEST_2;
		}
		try
		{
			$ses = new SimpleEmailService($secret_key,$secret_password,$region_endpoint);
			$response = $ses->sendEmail($m);
			if($response['MessageId'])
			{
				return 'correct';
			}
		}
		catch(Exception $r)
		{
			return 'error';
		}
		return 'error';
	}
	function sendMailUsingSparkpost($mailArray,$sparky=null)
	{
		
		$api_value = unserialize($mailArray['api_value']);
		$from_email = $api_value['sending_email'];
		$to_email = trim($mailArray['to']);
		$stats_code = $mailArray['stats_code'];
		$subject = $mailArray['subject'];
		$message = $mailArray['message'].'<div style="display:none">{emailpro}'.$stats_code.'{emailpro}</div>';
		
		$sparky->setOptions(['async' => false]);
		if (strpos($sending_email, 'sparkpostbox') !== false) {
		try {
		$results = $sparky->transmissions->post([
		  'options' => [
			'sandbox' => true
		  ],
		  'content' => [
			'from' => $from_email,
			'subject' => $subject,
			'html' => $message
		  ],
		  'recipients' => [
			['address' => ['email'=>$to_email]]
		  ]
		]);
			return "correct"; 
		} catch (Exception $e) {
			return "error";
		}
		}
		else{	
		try {
		$results = $sparky->transmissions->post([
		 'content' => [
			'from' => $from_email,
			'subject' => $subject,
			'html' => $message
		  ],
		  'recipients' => [
			['address' => ['email'=>$to_email]]
		  ]
		]);
			return "correct"; 
		} catch (Exception $e) {
			return "error";
		}
		}
		 
	}
	function sendMailUsingPostmark($mailArray,$client=null)
	{ 
		$api_value = unserialize($mailArray['api_value']);
		$from_email = $api_value['sending_email'];
		$to_email = trim($mailArray['to']);
		$stats_code = $mailArray['stats_code'];
		$subject = $mailArray['subject'];
		$message = $mailArray['message'].'<div style="display:none">{emailpro}'.$stats_code.'{emailpro}</div>';
		
		try {
			$sendResult = $client->sendEmail(
			  $from_email,
			  $to_email,
			  $subject,
			  $message
			);
			return "correct";
		}
		catch (Exception $e) {
			return "error";
		}
		 
	}
	function sendMailUsingMailgun($mailArray,$mgClient=null,$domain=null,$sending_email=null)
	{
		$from_name = $mailArray['from_name'];
		$to_email = trim($mailArray['to']);
		$stats_code = $mailArray['stats_code'];
		$subject = $mailArray['subject'];
		$message = $mailArray['message'].'<div style="display:none">{emailpro}'.$stats_code.'{emailpro}</div>';
		
		try{
		$result = $mgClient->sendMessage($domain,
        array('from'    => $from_name.' <'.$sending_email.'>',
                'to'      => '<'.$to_email.'>',
                'subject' => $subject,
                'html'    => $message));
		return "correct";	
			
		}
		catch (Exception $e) {
			return "error";
		}
	}
	function sendMailUsingElastic($mailArray,$elasticEmail=null)
	{
		$api_value = unserialize($mailArray['api_value']);
		$from_email = $api_value['sending_email'];
		$to_email = trim($mailArray['to']);
		$stats_code = $mailArray['stats_code'];
		$subject = $mailArray['subject'];
		$message = $mailArray['message'].'<div style="display:none">{emailpro}'.$stats_code.'{emailpro}</div>';
		
		try{
			$elasticEmail->email()->send([
				'to'      => $to_email,
				'subject' => $subject,
				'from'    => $from_email,
				'bodyHtml' => $message 
			]);
			return "correct";	
		}
		catch(Exception $e)
		{
			return "error";
		}
	}
	
	
	
	function sendAmazonTestMail($contact_email=null,$sending_mail=null,$amazon_key=null,$amazon_password=null,$amazon_region=null)
	{
		
		$m = new SimpleEmailServiceMessage();
		$m->addTo($contact_email);
		$m->setFrom($sending_mail);
		$m->setSubject('Testing Credentials');
		$m->setMessageFromString('Credentials Correct');
		if($amazon_region == 'us_east_1'){
			$region_endpoint = SimpleEmailService::AWS_US_EAST_1;
		}
		else if($amazon_region == 'eu_west_1'){
				$region_endpoint = SimpleEmailService::AWS_US_WEST_1;
		}
		else if($amazon_region == 'us_west_2'){
			$region_endpoint = SimpleEmailService::AWS_US_WEST_2;
		}
		
		try{ 
		
		    $ses = new SimpleEmailService($amazon_key,$amazon_password,$region_endpoint);
			$response = $ses->sendEmail($m);
			if($response['MessageId'])
			{
				return 'correct';
			}	
		}
		catch(Exception $r)
		{
			return 'error';
		}
		return 'error';
	}
	function sendSparkpostTestMail($httpClient=null,$sparky=null,$contact_email=null,$sending_email=null)
	{
		
		$sparky->setOptions(['async' => false]);
		if (strpos($sending_email, 'sparkpostbox') !== false) {
			try {
				$results = $sparky->transmissions->post([
				  'options' => [
					'sandbox' => true
				  ],
				  'content' => [
					'from' => $sending_email,
					'subject' => 'MailZingo Tested',
					'html' => '<html><body><p>Mailzingo tested!</p></body></html>'
				  ],
				  'recipients' => [
					['address' => ['email'=>$contact_email]]
				  ]
				]);
					return "correct"; 
				} catch (Exception $e) {
					return "error";
				}
		  
		}
		else{
		try {
		$results = $sparky->transmissions->post([
		 'content' => [
			'from' => $sending_email,
			'subject' => 'MailZingo Tested',
			'html' => '<html><body><p>Mailzingo tested!</p></body></html>'
		  ],
		  'recipients' => [
			['address' => ['email'=>$contact_email]]
		  ]
		]);
			return "correct"; 
		} catch (Exception $e) {
			return "error";
		}
		}
	}
	function sendPostmarkTestMail($client=null,$sending_email=null,$contact_email=null)
	{
		try {
			$sendResult = $client->sendEmail(
			  $sending_email,
			  $contact_email,
			  "Mailzingo Postmark Tested",
			  "Mailzingo Tested"
			);
			return "correct";
		}
		catch (Exception $e) {
			return "error";
		}
		 
	}
	function sendMailgunTestMail($mgClient=null,$domain=null,$sending_email=null,$contact_email=null)
	{
		try{
		$result = $mgClient->sendMessage($domain,
        array('from'    => 'Mailgun Sandbox <'.$sending_email.'>',
                'to'      => 'trilok <'.$contact_email.'>',
                'subject' => 'Mailzingo mailgun tested',
                'text'    => 'Mailzingo Tested'));
		return "correct";	
			
		}
		catch (Exception $e) {
			return "error";
		}
	}
	function sendElasticTestMail($elasticEmail=null,$contact_email=null,$sending_email=null)
	{
		 
		try{
			$elasticEmail->email()->send([
				'to'      => $contact_email,
				'subject' => 'Mailzingo Elastic mail Testing',
				'from'    => $sending_email
			]);
			return "correct";	
		}
		catch(Exception $e)
		{
			return "error";
		}
	}
	
	function sendSendgridTestMail($api_key,$sending_email=null,$contact_email=null)
	{
		 $request_body = json_decode('{
		  "personalizations": [
			{
			  "to": [
				{
				  "email": "'.$contact_email.'"
				}
			  ],
			  "subject": "MailZingo Sendgrid Testing Mail"
			}
		  ],
		  "from": {
			"email": "'.$sending_email.'"
		  },
		  "content": [
			{
			  "type": "text/html",
			  "value": "<h1>MailZingo SendGrid Tested!</h1>"
			}
		  ]
		}');

		$sg = new \SendGrid($api_key);
		try{
			$response = $sg->client->mail()->send()->post($request_body);
			 $code = $response->statusCode();
			if($code==202) 
			return "correct";	
		}
		catch(Exception $e)
		{
			return "error";
		}
	}
	function sendMailUsingSendgrid($mailArray=null,$api_key=null,$sending_mail=null)
	{
		$api_value = unserialize($mailArray['api_value']);
		$from_email = $sending_mail;
		$to_email = trim($mailArray['to']);
		$stats_code = $mailArray['stats_code'];
		$subject = $mailArray['subject'];
		$message = $mailArray['message'].'<div style="display:none">{emailpro}'.$stats_code.'{emailpro}</div>';
		  
		$request_body = json_decode('{
		  "personalizations": [
			{
			  "to": [
				{
				  "email": "'.$to_email.'"
				}
			  ],
			  "subject": "'.$subject.'"
			}
		  ],
		  "from": {
			"email": "'.$from_email.'"
		  },
		  "content": [
			{
			  "type": "text/html",
			  "value": "<h2>ssssssss</h2>"
			}
		  ]
		}');
		
		 
		
		$request_body->content[0]->value = $message; 
		
		
		$sg = new \SendGrid($api_key);
		try{
			$response = $sg->client->mail()->send()->post($request_body);
			$code = $response->statusCode();
			if($code==202) 
			return "correct";	
		}
		catch(Exception $e)
		{
			return "error";
		}
		
	}
	
	
}