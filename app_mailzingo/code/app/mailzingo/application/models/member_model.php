<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Member_Model extends CI_Model {

	var $tableName = 'tbl_member';
	public function checkLogin()
	{
		
		$this->db->where('email',$this->input->post('email'));
		$this->db->where('password',md5($this->input->post('password')));
		$this->db->where('status','active');
		$query = $this->db->get($this->tableName);
		return $query->row();
	}
	function updatePassword(){
	    $this->db->set('password',md5($this->input->post('new_pass')));
		$this->db->where('mem_id',$this->session->userdata('mem_id'));
		$this->db->update($this->tableName);
	}
        
    function checkEmailExist(){
                $this->db->select('mem_id,email,business_name');
                $this->db->where('email',$this->input->post('email'));
		$query = $this->db->get($this->tableName);
		return $query->row();
        }
        
        function insert_forget_details($r,$id){
            $this->db->where('mem_id',$id);
            $this->db->set('forget_pass_code',$r);
            $this->db->set('forget_pass_exptime',strtotime('+1 day',time()) );
            $this->db->update($this->tableName);
            
        }
        
        function update_forget_details1($id){
            $this->db->where('mem_id',$id);
            $this->db->set('forget_pass_code',0);
            $this->db->set('forget_pass_exptime',0 );
            $this->db->update($this->tableName);
        }
        
        function getTableRow($id){
                $this->db->select('mem_id,forget_pass_code');
                $this->db->where('mem_id',$id);
		$query = $this->db->get($this->tableName);
		return $query->row();
        }
        function updateForgetPassword($id){
                
                if($this->input->post('pass1'))
                $this->db->set('password',md5($this->input->post('pass1')));
                if($this->session->userdata('mem_id'))
		$this->db->where('mem_id',$id);
		$this->db->update($this->tableName);
	}
}
