<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Unsubscribe_Model extends CI_Model {

	var $tableName = 'tbl_unsubscribe_list';
	var $tableContact = 'tbl_contact';
	var $tableStats = 'tbl_statistics';
	public function addUnsubscribe($member_id,$contact_id,$contact_email)
	{
		$this->db->set('member_id',$member_id);
		$this->db->set('contact_id',$contact_id);
		$this->db->set('email',$contact_email);
		$this->db->set('add_time',time());
		$this->db->set('ip',$_SERVER['REMOTE_ADDR']);
		$this->db->insert($this->tableName);
	}
	public function checkUnsubscriber($member_id,$contact_id,$contact_email)
	{
		if($member_id)
		$this->db->where('member_id',$member_id);
		if($contact_id)
		$this->db->where('contact_id',$contact_id);
		if($contact_email)
		$this->db->where('email',$contact_email);
		$query = $this->db->get($this->tableName);
		return $query->num_rows();
	}
	public function getUnsubscriberList($member_id)
	{
		$this->db->select('email');
		$this->db->where('member_id',$member_id);
		$query = $this->db->get($this->tableName);
		return $query->result_array();
	}
	
	
}
