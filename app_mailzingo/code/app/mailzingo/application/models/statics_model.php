<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Statics_Model extends CI_Model {

	var $tableName = 'tbl_statistics';
	var $tableContact = 'tbl_contact';
	public function addRecord($newsletter_id,$type,$contact_id,$email)
	{
		$this->db->set('type',$type);
		$this->db->set('newsletter_id',$newsletter_id);
		$this->db->set('contact_id',$contact_id);
		$this->db->set('email',$email);
		$this->db->set('add_time',time());
		$this->db->set('ip',0);
		$this->db->insert($this->tableName);
		return $this->db->insert_id();
	}
	public function setOpen($id,$device)
	{
		$browserdata = $_SERVER['HTTP_USER_AGENT'];
        if (strlen(strstr($browserdata, 'Firefox')) > 0) {
         $browser = 'firefox';
		} else if (strlen(strstr($browserdata, 'Chrome')) > 0) {
         $browser = 'chrome';
		} else if (strlen(strstr($browserdata, 'MSIE')) > 0) {
         $browser = 'IE';
		} else if (strlen(strstr($browserdata, 'Safari')) > 0) {
         $browser = 'safari';
		} else {
		 $browser = 'others';
		}
		$ip = $_SERVER['REMOTE_ADDR'];
        $geodata=unserialize(file_get_contents('http://www.geoplugin.net/php.gp?ip='.$ip));
	//	$country_code = $geodata['']
		$this->db->set('open','yes');
		$this->db->set('ip',$ip);
		$this->db->set('device',$device);
		$this->db->set('browser',$browser);
		$this->db->set('country_code',$geodata['geoplugin_countryCode']);
		$this->db->set('open_time',time());
		$this->db->where('id',$id);
		$this->db->update($this->tableName);
	}
	public function setClick($id,$device)
	{
		$this->db->set('click','yes');
		$this->db->set('click_time',time());
		$this->db->where('id',$id);
		$this->db->update($this->tableName);
		$this->setOpen($id,$device);
	}
	public function setUnsubscribe($id)
	{
		$this->db->set('unsubscribed','yes');
		$this->db->where('id',$id);
		$this->db->update($this->tableName);
	}
	public function setBounce($id)
	{
		$this->db->set('bounced','yes');
		$this->db->where('id',$id);
		$this->db->update($this->tableName);
	}
	public function checkStatsIdExists($id)
	{
		$this->db->where('id',$id);
		$query = $this->db->get($this->tableName);
		return $query->num_rows();
	}
	public function getNewsletterStatByFieldType($newsletter_id,$field_type,$condition,$type)
	{
		if($newsletter_id)
		$this->db->where('newsletter_id',$newsletter_id);
		if($field_type)
		$this->db->where($field_type,$condition);
		if($type)
		$this->db->where('type',$type);
		$query = $this->db->get($this->tableName);
		return $query->num_rows();
	}
	public function getNewsletterStatByDeviceType($newsletter_id,$device_type,$type)
	{ 
		if($newsletter_id)
		$this->db->where('newsletter_id',$newsletter_id);
		if($device_type)
		$this->db->where('device',$device_type);
		if($type)
		$this->db->where('type',$type);
		$query = $this->db->get($this->tableName);
		return $query->num_rows();
	}
	public function getNewsletterStatByBrowserType($newsletter_id,$browser)
	{ 
		if($newsletter_id)
		$this->db->where('newsletter_id',$newsletter_id);
		if($browser)
		$this->db->where('browser',$browser);
		$query = $this->db->get($this->tableName);
		return $query->num_rows();
	}
	public function getStatsdetail($id)
	{
		$this->db->select($this->tableContact.'.member_id,'.$this->tableName.'.*');
		$this->db->where($this->tableName.'.id',$id);
		$this->db->join($this->tableContact,$this->tableContact.'.id='.$this->tableName.'.contact_id');
		$query = $this->db->get($this->tableName);
		return $query->row();
	}
	public function getCountryArray($newsletter_id,$type)
	{ 
		$this->db->select('country_code');
		if($newsletter_id)
		$this->db->where('newsletter_id',$newsletter_id);
		if($type)
		$this->db->where('type',$type);
		$this->db->group_by('country_code');
		$query = $this->db->get($this->tableName);
		$country_codes = $query->result();
		$finalArray = array();
		foreach($country_codes as $val)
		 {
		   if($newsletter_id)
		   $this->db->where('newsletter_id',$newsletter_id);
		   if($type)
		   $this->db->where('type',$type);
		   $this->db->where('country_code',$val->country_code);
		   $query = $this->db->get($this->tableName);
		   $finalArray[$val->country_code] = $query->num_rows();
		 }
		return $finalArray;
	}
	public function getStatsEmailByNewsletter($newsletter_id,$field_type,$type)
	{
		$this->db->select('email');
		if($newsletter_id)
		$this->db->where('newsletter_id',$newsletter_id);
		if($field_type)
		$this->db->where($field_type,'yes');
		if($type)
		$this->db->where('type',$type);
		$query = $this->db->get($this->tableName);
		return $query->result();
	}
	 function getOpenGraph($time_type,$newsletter_id,$member_id,$type)
	 {
		
		if($time_type=='day')
		{
			$this->db->select("date_format(from_unixtime(open_time), ('%d-%m-%Y')) as new_date");
			$this->db->group_by("date_format(from_unixtime(open_time), ('%d-%m-%Y'))");
			$this->db->where("date_format(from_unixtime(open_time), ('%m-%Y')) =", date('m-Y'));
		} else if($time_type=='month')
		{
			$this->db->select("date_format(from_unixtime(open_time), ('%m-%Y')) as new_date");
			$this->db->group_by("date_format(from_unixtime(open_time), ('%m-%Y'))");
			$this->db->where("date_format(from_unixtime(open_time), ('%Y')) =", date('Y'));
		} else if($time_type=='year')
		{
			$this->db->select("date_format(from_unixtime(open_time), ('%Y')) as new_date");
			$this->db->group_by("date_format(from_unixtime(open_time), ('%Y'))");
		}
		$this->db->where('newsletter_id',$newsletter_id);
		if($type)
		$this->db->where('type',$type);
		$this->db->where('open','yes');
		$this->db->order_by('open_time','asc');
		$query = $this->db->get($this->tableName);
		$result = $query->result();
		$allresult = array();
		foreach($result as $val)
		{
			if($time_type=='day')
			{
			  $this->db->select("count(id) as total_record,date_format(from_unixtime(open_time), ('%d-%m-%Y')) as added_date");
			  $this->db->where("date_format(from_unixtime(open_time), ('%d-%m-%Y')) =", $val->new_date);
			} else if($time_type=='month')
			{
			  $this->db->select("count(id) as total_record,date_format(from_unixtime(open_time), ('%m-%Y')) as added_date");
			  $this->db->where("date_format(from_unixtime(open_time), ('%m-%Y')) =", $val->new_date);
			} else if($time_type=='year')
			{
			  $this->db->select("count(id) as total_record,date_format(from_unixtime(open_time), ('%Y')) as added_date");
			  $this->db->where("date_format(from_unixtime(open_time), ('%Y')) =", $val->new_date);
			}
		      $this->db->where('newsletter_id',$newsletter_id);
			  $this->db->where('open','yes');
			  if($type)
			  $this->db->where('type',$type);
			  $query1 = $this->db->get($this->tableName);
			  $allresult[] = $query1->row();
		}

	    if($time_type=='day')
		 {
		   for($i=1;$i<=date('d');$i++)
		    {
			   $ii = sprintf("%02d", $i);
			   $data[$ii] = '{ x: new Date("'.date('m').', '.$ii.', '.date('Y').'"), y: 0 },';
			}
		 }
		 else if($time_type=='month') {
		    for($i=1;$i<=date('m');$i++)
		    {
			   $ii = sprintf("%02d", $i);
			   $data[$ii] = '{ x: new Date("'.$ii.', 01, '.date('Y').'"), y: 0 },';
			}
		 }
		 else if($time_type=='year') {
		    $lastyear = date('Y')-5;
		    for($i=$lastyear;$i<=date('Y');$i++)
		    {
			   $data[$i] = '{ x: new Date("01, 01, '.$i.'"), y: 0 },';
			}
		 }
		 
		foreach($allresult as $value)
		{
		  if($time_type=='month')
		   { 
		     $added_date = '1-'.$value->added_date; 
		     $key = date('m',strtotime($added_date));
		   }
		  else if($time_type=='year')
		   { 
		     $added_date = '1-1-'.$value->added_date; 
			 $key = date('Y',strtotime($added_date));
		   }
		  else 
		   { 
		     $added_date = $value->added_date; 
			 $key = date('d',strtotime($added_date));
		   }
		 
		 $date = date('m',strtotime($added_date)).', '.date('d',strtotime($added_date)).', '.date('Y',strtotime($added_date));
		 $data[$key] = '{ x: new Date("'.$date.'"), y: '.$value->total_record.' },';
		}
		foreach($data as $valueData)
		 {
		   $newdata.= $valueData;
		 }
		//echo $data; die;
		return $newdata;
	 }
	 function getClickGraph($time_type,$newsletter_id,$member_id,$type)
	 {
		
		if($time_type=='day')
		{
			$this->db->select("date_format(from_unixtime(open_time), ('%d-%m-%Y')) as new_date");
			$this->db->group_by("date_format(from_unixtime(open_time), ('%d-%m-%Y'))");
			$this->db->where("date_format(from_unixtime(open_time), ('%m-%Y')) =", date('m-Y'));
		} else if($time_type=='month')
		{
			$this->db->select("date_format(from_unixtime(open_time), ('%m-%Y')) as new_date");
			$this->db->group_by("date_format(from_unixtime(open_time), ('%m-%Y'))");
			$this->db->where("date_format(from_unixtime(open_time), ('%Y')) =", date('Y'));
		} else if($time_type=='year')
		{
			$this->db->select("date_format(from_unixtime(open_time), ('%Y')) as new_date");
			$this->db->group_by("date_format(from_unixtime(open_time), ('%Y'))");
		}
		$this->db->where('newsletter_id',$newsletter_id);
		$this->db->where('click','yes');
		if($type)
		$this->db->where('type',$type);
		$this->db->order_by('open_time','asc');
		$query = $this->db->get($this->tableName);
		$result = $query->result();
		$allresult = array();
		foreach($result as $val)
		{
			if($time_type=='day')
			{
			  $this->db->select("count(id) as total_record,date_format(from_unixtime(open_time), ('%d-%m-%Y')) as added_date");
			  $this->db->where("date_format(from_unixtime(open_time), ('%d-%m-%Y')) =", $val->new_date);
			} else if($time_type=='month')
			{
			  $this->db->select("count(id) as total_record,date_format(from_unixtime(open_time), ('%m-%Y')) as added_date");
			  $this->db->where("date_format(from_unixtime(open_time), ('%m-%Y')) =", $val->new_date);
			} else if($time_type=='year')
			{
			  $this->db->select("count(id) as total_record,date_format(from_unixtime(open_time), ('%Y')) as added_date");
			  $this->db->where("date_format(from_unixtime(open_time), ('%Y')) =", $val->new_date);
			}
		      $this->db->where('newsletter_id',$newsletter_id);
			  $this->db->where('click','yes');
		      if($type)
		      $this->db->where('type',$type);
			  $query1 = $this->db->get($this->tableName);
			  $allresult[] = $query1->row();
		}
        
		 if($time_type=='day')
		 {
		   for($i=1;$i<=date('d');$i++)
		    {
			   $ii = sprintf("%02d", $i);
			   $data[$ii] = '{ x: new Date("'.date('m').', '.$ii.', '.date('Y').'"), y: 0 },';
			}
		 }
		 else if($time_type=='month') {
		    for($i=1;$i<=date('m');$i++)
		    {
			   $ii = sprintf("%02d", $i);
			   $data[$ii] = '{ x: new Date("'.$ii.', 01, '.date('Y').'"), y: 0 },';
			}
		 }
		 else if($time_type=='year') {
		    $lastyear = date('Y')-5;
		    for($i=$lastyear;$i<=date('Y');$i++)
		    {
			   $data[$i] = '{ x: new Date("01, 01, '.$i.'"), y: 0 },';
			}
		 }
		 
		foreach($allresult as $value)
		{
		  if($time_type=='month')
		   { 
		     $added_date = '1-'.$value->added_date;
			 $key = date('m',strtotime($added_date));
		   }
		  else if($time_type=='year')
		   { 
		     $added_date = '1-1-'.$value->added_date; 
			 $key = date('Y',strtotime($added_date));
		   }
		  else 
		   {
		     $added_date = $value->added_date; 
			 $key = date('d',strtotime($added_date));
		   }
		  
		  $date = date('m',strtotime($added_date)).', '.date('d',strtotime($added_date)).', '.date('Y',strtotime($added_date));
		  $data[$key] = '{ x: new Date("'.$date.'"), y: '.$value->total_record.' },';
		}
		foreach($data as $valueData)
		 {
		   $newdata.= $valueData;
		 }
		//echo $data; die;
		return $newdata;
	 }
}