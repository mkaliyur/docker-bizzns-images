<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Api_Model extends CI_Model {

	var $tableName = 'tbl_website_settings';
	var $tableCampaign = 'tbl_campaign';
	var $tableContact = 'tbl_contact';

	public function checkForAPI($api_key){
	 	
		$this->db->where('api_key',$api_key);
		$query = $this->db->get($this->tableName);
		if($query->num_rows()!=0)
		return true;
		else
		return false;
		
	 }
	public function getCampaignList()
	{
		$this->db->select('campaign_name,campaign_id');
		$this->db->order_by('campaign_name','asc');
		$query = $this->db->get($this->tableCampaign);
		return $query->result();
	}
	public function checkSubscriber($campaign_id,$email)
	{
		$this->db->where('campaign_id',$campaign_id);
		$this->db->where('email',$email);
		$query = $this->db->get($this->tableContact);
		return $query->num_rows();
	}
	public function addSubscriber($member_id)
	{
		$campaign_id = trim($_REQUEST['campaign_id']);
		$email = trim($_REQUEST['email']);
		$name = trim($_REQUEST['name']);
		$address = trim($_REQUEST['address']);
		$city = trim($_REQUEST['city']);
		$state = trim($_REQUEST['state']);
		$country = trim($_REQUEST['country']);
		$zip = trim($_REQUEST['zip']);
		$phone = trim($_REQUEST['phone']);
		
		$subscribercheck = $this->checkSubscriber($campaign_id,$email);
		if($subscribercheck!=0)
		{ return 'notdone'; }
		
		$this->db->set('member_id',trim($member_id));
		$this->db->set('campaign_id',trim($campaign_id)); 
		$this->db->set('email',$email); 
		if($name)
		$this->db->set('name',$name); 
		if($address)
		$this->db->set('address',$address); 
		if($city)
		$this->db->set('city',$city); 
		if($state)
		$this->db->set('state',$state); 
		if($country)
		$this->db->set('country',$country); 
		if($zip)
		$this->db->set('zip',$zip); 
		if($phone)
		$this->db->set('phone',$phone); 
		$this->db->set('add_from','form');
		$this->db->set('status','active');
		$this->db->set('add_time',time());
		$this->db->set('ip',$_SERVER['REMOTE_ADDR']);
		$this->db->insert($this->tableContact);
		return $this->db->insert_id();
	}
	public function removeFromUnsubscribeList($email)
	{ 
		$this->db->where('email',trim($email));
		$this->db->delete('tbl_unsubscribe_list');
	}
	public function getSubscriberDetail($id)
	{
		$this->db->where('id',$id);
		$query = $this->db->get($this->tableContact);
		return $query->row();
	}
}
