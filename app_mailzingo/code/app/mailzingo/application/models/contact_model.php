<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Contact_Model extends CI_Model {

	var $tableName = 'tbl_contact';
	var $tableCampaign = 'tbl_campaign';
	var $tableNewsletter = 'tbl_newsletter';
	var $tableStatistics = 'tbl_statistics';
	var $tableUnsubscribe = 'tbl_unsubscribe_list';
	
	public function getAllCount($member_id,$status=null){
	 	
		if($status)
		$this->db->where('status',$status);
		if($member_id)
		$this->db->where('member_id',$member_id);
		$query = $this->db->get($this->tableName);
		return $query->num_rows();
	 }
	public function addSingleContact($member_id,$fieldArray=null,$campaign_id=null)
	{
		foreach($campaign_id as $valCamp)
		{
			$this->db->set('member_id',trim($member_id));
			$this->db->set('campaign_id',trim($valCamp)); 
			$this->db->set('email',trim($fieldArray['email'])); 
			$this->db->set('name',trim($fieldArray['name']));
			foreach($fieldArray['fieldname'] as $key=>$val)
			{
			  if(trim($val)!='')
			  $this->db->set($val,trim($fieldArray['fieldvalue'][$key]));
			}
			$this->db->set('add_time',time());
			$this->db->set('ip',$_SERVER['REMOTE_ADDR']);
			$this->db->insert($this->tableName);
		}
	}
	public function addImportContact($member_id,$fieldArray=null,$campaign_id=null)
	{
		 
		foreach($campaign_id as $valCamp)
		{
			if(filter_var(trim($fieldArray['email']), FILTER_VALIDATE_EMAIL))
			{
				$this->db->set('member_id',trim($member_id));
				$this->db->set('campaign_id',trim($valCamp));
				foreach($fieldArray as $key=>$val)
				{
				  if($key!='no')
				  $this->db->set($key,trim($val));
				}
				$this->db->set('add_time',time());
				$this->db->set('ip',$_SERVER['REMOTE_ADDR']);
				$this->db->insert($this->tableName);
			}
		}
	}
	public function addCopyContact($member_id,$email=null,$campaign_id=null)
	{
		foreach($campaign_id as $valCamp)
		{
			$this->db->set('member_id',trim($member_id));
			$this->db->set('campaign_id',trim($valCamp));
			$this->db->set('email',trim($email));
			$this->db->set('add_time',time());
			$this->db->set('ip',$_SERVER['REMOTE_ADDR']);
			$this->db->insert($this->tableName);
		}
	}
	public function updateContact($id,$address=null,$city=null,$state=null,$country=null,$zip=null,$phone=null,$custom_1=null,$custom_2=null,$custom_3=null)
	{

		if($this->input->post('name'))
		$this->db->set('name',$this->input->post('name'));
		if($this->input->post('email'))
		$this->db->set('email',$this->input->post('email'));
		$this->db->set('address',trim($address));
		$this->db->set('city',trim($city));
		$this->db->set('state',trim($state));
		$this->db->set('country',trim($country));
		$this->db->set('zip',trim($zip));
		$this->db->set('phone',trim($phone));
		$this->db->set('custom_1',trim($custom_1));
		$this->db->set('custom_2',trim($custom_2));
		$this->db->set('custom_3',trim($custom_3));
		$this->db->where('id',$id);
		$this->db->update($this->tableName);

	}
	public function getContactList($campaign_id,$status=null,$member_id=null)
	{
		if($member_id)
		$this->db->where('member_id',$member_id);
		if($campaign_id)
		$this->db->where('campaign_id',$campaign_id);
		if($status)
		$this->db->where('status',$status);
		$query = $this->db->get($this->tableName);
		return $query->result();
	}
	public function getContactRecord($member_id,$campaign_id=null,$subscribe_type=null,$message_type=null,$newsletter_id=null,$stat_type=null,$date_time=null,$from_date=null,$to_date=null,$keyword=null,$start=null,$content_per_page=null,$index=null)
	{
		$this->db->select($this->tableName.'.*');
		if($member_id)
		$this->db->where($this->tableName.'.member_id',$member_id);
		if($campaign_id)
		$this->db->where_in($this->tableName.'.campaign_id',$campaign_id);
		if($keyword)
		$this->db->like($this->tableName.'.email',$keyword);
		
		if($date_time!='all_time')
		 {
		   if($date_time=='today')
		    $add_time = strtotime('-1 day');
		   else if($date_time=='yesterday')
		    $add_time = strtotime('-2 day');
		   else if($date_time=='last_week')
		    $add_time = strtotime('-1 week');
		   else if($date_time=='this_month')
		    $add_time = strtotime('-1 month');
		   else if($date_time=='last_month')
		   {
			$from_date = strtotime('-2 month');
			$to_date = strtotime('-1 month');
		   }
		   else if($date_time=='custom')
		   {
		    $from_date = strtotime($from_date);
			$to_date = strtotime('+1 day',strtotime($to_date));
		   }
		 }

		if($subscribe_type!='' && $subscribe_type!='all_subscriber')
		{ 
		 $this->db->join($this->tableStatistics,$this->tableStatistics.'.contact_id='.$this->tableName.'.id');
		 $this->db->join($this->tableNewsletter,$this->tableNewsletter.'.id='.$this->tableStatistics.'.newsletter_id');
		 $this->db->group_by($this->tableStatistics.'.contact_id');
		 
		 if($subscribe_type=='unsubscriber')
		  {
		    $this->db->where($this->tableStatistics.'.unsubscribed','yes');
			$this->db->join($this->tableUnsubscribe,$this->tableStatistics.'.contact_id='.$this->tableUnsubscribe.'.contact_id');
			if($add_time)
			  $this->db->where($this->tableUnsubscribe.'.add_time >',$add_time);
			if($from_date)
			  $this->db->where($this->tableUnsubscribe.'.add_time >',$from_date);
			if($to_date)
			  $this->db->where($this->tableUnsubscribe.'.add_time <',$to_date);
		  
		  } else if($subscribe_type=='bounced') {		  
		    $this->db->where($this->tableStatistics.'.bounced','yes');
			if($add_time)
			  $this->db->where($this->tableStatistics.'.add_time >',$add_time);
			if($from_date)
			  $this->db->where($this->tableStatistics.'.add_time >',$from_date);
			if($to_date)
			  $this->db->where($this->tableStatistics.'.add_time <',$to_date);
		 }
		 else if($subscribe_type=='message')
		  {
		      if($message_type)
			  $this->db->where($this->tableNewsletter.'.newsletter_type',$message_type);
			  if($newsletter_id)
			  $this->db->where($this->tableNewsletter.'.id',$newsletter_id);
			  if($stat_type=='open')
			  {
			    $this->db->where($this->tableStatistics.'.open','yes');
				if($add_time)
				  $this->db->where($this->tableStatistics.'.open_time >',$add_time);
				if($from_date)
				  $this->db->where($this->tableStatistics.'.open_time >',$from_date);
				if($to_date)
				  $this->db->where($this->tableStatistics.'.open_time <',$to_date);
			  }
			  else if($stat_type=='click')
			  {
			    $this->db->where($this->tableStatistics.'.click','yes');
				if($add_time)
				  $this->db->where($this->tableStatistics.'.click_time >',$add_time);
				if($from_date)
				  $this->db->where($this->tableStatistics.'.click_time >',$from_date);
				if($to_date)
				  $this->db->where($this->tableStatistics.'.click_time <',$to_date);
			  }
			  else if($stat_type=='unopen')
			  {
			    $this->db->where($this->tableStatistics.'.open','no');
				if($add_time)
				  $this->db->where($this->tableStatistics.'.add_time >',$add_time);
				if($from_date)
				  $this->db->where($this->tableStatistics.'.add_time >',$from_date);
				if($to_date)
				  $this->db->where($this->tableStatistics.'.add_time <',$to_date);
			  }
			  else if($stat_type=='bounce')
			  {
			    $this->db->where($this->tableStatistics.'.bounced','yes');
				if($add_time)
				  $this->db->where($this->tableStatistics.'.add_time >',$add_time);
				if($from_date)
				  $this->db->where($this->tableStatistics.'.add_time >',$from_date);
				if($to_date)
				  $this->db->where($this->tableStatistics.'.add_time <',$to_date);
			  }
			  else if($stat_type=='unsubsribe')
			  {
			    $this->db->where($this->tableStatistics.'.unsubscribed','yes');
				$this->db->join($this->tableUnsubscribe,$this->tableStatistics.'.contact_id='.$this->tableUnsubscribe.'.contact_id');
				if($add_time)
				  $this->db->where($this->tableUnsubscribe.'.add_time >',$add_time);
				if($from_date)
				  $this->db->where($this->tableUnsubscribe.'.add_time >',$from_date);
				if($to_date)
				  $this->db->where($this->tableUnsubscribe.'.add_time <',$to_date);
			  }
		  }
		}
		
		if($date_time!='all_time' && $subscribe_type!='')
		 {		
		  if($add_time)
		  $this->db->where($this->tableName.'.add_time >',$add_time);
		  if($from_date)
		  $this->db->where($this->tableName.'.add_time >',$from_date);
		  if($to_date)
		  $this->db->where($this->tableName.'.add_time <',$to_date);
		}
		
		$this->db->where($this->tableName.'.status','active');
		$query = $this->db->get($this->tableName,$content_per_page,$start);
                if(!$index)
		return $query->result();
                if($index)
		return $query->num_rows();
	}
	public function getContactRecordExport($member_id=null,$campaign_id=null,$subscribe_type=null,$message_type=null,$newsletter_id=null,$stat_type=null,$date_time=null,$from_date=null,$to_date=null,$keyword=null)
	{
		$this->db->select('tbl_contact.name as Name,tbl_contact.email as Email,tbl_campaign.campaign_name as Campaign_Name,tbl_contact.address as Address,tbl_contact.city as City,tbl_contact.state State,tbl_contact.country as Country,tbl_contact.zip as Zip,tbl_contact.phone as Phone,tbl_contact.custom_1 as Custom_1,tbl_contact.custom_2 as Custom_2,tbl_contact.custom_3 as Custom_3');
		if($member_id)
		$this->db->where($this->tableName.'.member_id',$member_id);
		if($campaign_id)
		$this->db->where_in($this->tableName.'.campaign_id',$campaign_id);
		if($keyword)
		$this->db->like($this->tableName.'.email',$keyword);
		$this->db->join($this->tableCampaign,$this->tableName.'.campaign_id='.$this->tableCampaign.'.campaign_id');
		
		if($date_time!='all_time')
		 {
		   if($date_time=='today')
		    $add_time = strtotime('-1 day');
		   else if($date_time=='yesterday')
		    $add_time = strtotime('-2 day');
		   else if($date_time=='last_week')
		    $add_time = strtotime('-1 week');
		   else if($date_time=='this_month')
		    $add_time = strtotime('-1 month');
		   else if($date_time=='last_month')
		   {
			$from_date = strtotime('-2 month');
			$to_date = strtotime('-1 month');
		   }
		   else if($date_time=='custom')
		   {
		    $from_date = strtotime($from_date);
			$to_date = strtotime('+1 day',strtotime($to_date));
		   }
		 }

		if($subscribe_type!='' && $subscribe_type!='all_subscriber')
		{ 
		 $this->db->join($this->tableStatistics,$this->tableStatistics.'.contact_id='.$this->tableName.'.id');
		 $this->db->join($this->tableNewsletter,$this->tableNewsletter.'.id='.$this->tableStatistics.'.newsletter_id');
		 $this->db->group_by($this->tableStatistics.'.contact_id');
		 
		 if($subscribe_type=='unsubscriber')
		  {
		    $this->db->where($this->tableStatistics.'.unsubscribed','yes');
			$this->db->join($this->tableUnsubscribe,$this->tableStatistics.'.contact_id='.$this->tableUnsubscribe.'.contact_id');
			if($add_time)
			  $this->db->where($this->tableUnsubscribe.'.add_time >',$add_time);
			if($from_date)
			  $this->db->where($this->tableUnsubscribe.'.add_time >',$from_date);
			if($to_date)
			  $this->db->where($this->tableUnsubscribe.'.add_time <',$to_date);
		  
		  } else if($subscribe_type=='bounced') {		  
		    $this->db->where($this->tableStatistics.'.bounced','yes');
			if($add_time)
			  $this->db->where($this->tableStatistics.'.add_time >',$add_time);
			if($from_date)
			  $this->db->where($this->tableStatistics.'.add_time >',$from_date);
			if($to_date)
			  $this->db->where($this->tableStatistics.'.add_time <',$to_date);
		 }
		 else if($subscribe_type=='message')
		  {
		      if($message_type)
			  $this->db->where($this->tableNewsletter.'.newsletter_type',$message_type);
			  if($newsletter_id)
			  $this->db->where($this->tableNewsletter.'.id',$newsletter_id);
			  if($stat_type=='open')
			  {
			    $this->db->where($this->tableStatistics.'.open','yes');
				if($add_time)
				  $this->db->where($this->tableStatistics.'.open_time >',$add_time);
				if($from_date)
				  $this->db->where($this->tableStatistics.'.open_time >',$from_date);
				if($to_date)
				  $this->db->where($this->tableStatistics.'.open_time <',$to_date);
			  }
			  else if($stat_type=='click')
			  {
			    $this->db->where($this->tableStatistics.'.click','yes');
				if($add_time)
				  $this->db->where($this->tableStatistics.'.click_time >',$add_time);
				if($from_date)
				  $this->db->where($this->tableStatistics.'.click_time >',$from_date);
				if($to_date)
				  $this->db->where($this->tableStatistics.'.click_time <',$to_date);
			  }
			  else if($stat_type=='unopen')
			  {
			    $this->db->where($this->tableStatistics.'.open','no');
				if($add_time)
				  $this->db->where($this->tableStatistics.'.add_time >',$add_time);
				if($from_date)
				  $this->db->where($this->tableStatistics.'.add_time >',$from_date);
				if($to_date)
				  $this->db->where($this->tableStatistics.'.add_time <',$to_date);
			  }
			  else if($stat_type=='bounce')
			  {
			    $this->db->where($this->tableStatistics.'.bounced','yes');
				if($add_time)
				  $this->db->where($this->tableStatistics.'.add_time >',$add_time);
				if($from_date)
				  $this->db->where($this->tableStatistics.'.add_time >',$from_date);
				if($to_date)
				  $this->db->where($this->tableStatistics.'.add_time <',$to_date);
			  }
			  else if($stat_type=='unsubsribe')
			  {
			    $this->db->where($this->tableStatistics.'.unsubscribed','yes');
				$this->db->join($this->tableUnsubscribe,$this->tableStatistics.'.contact_id='.$this->tableUnsubscribe.'.contact_id');
				if($add_time)
				  $this->db->where($this->tableUnsubscribe.'.add_time >',$add_time);
				if($from_date)
				  $this->db->where($this->tableUnsubscribe.'.add_time >',$from_date);
				if($to_date)
				  $this->db->where($this->tableUnsubscribe.'.add_time <',$to_date);
			  }
		  }
		}
		
		if($date_time!='all_time' && $subscribe_type!='')
		 {		
		  if($add_time)
		  $this->db->where($this->tableName.'.add_time >',$add_time);
		  if($from_date)
		  $this->db->where($this->tableName.'.add_time >',$from_date);
		  if($to_date)
		  $this->db->where($this->tableName.'.add_time <',$to_date);
		}
		
		$this->db->where($this->tableName.'.status','active');
		$query = $this->db->get($this->tableName);
		return $query;
	}
	public function deleteContact($id=null)
	{
		$this->db->where('id',$id);
		$query = $this->db->delete($this->tableName);
	}
	public function removeMultipleContact($id)
	{
		$this->db->where_in('id',$id);
		$this->db->delete($this->tableName);
	}
	public function getContactById($id)
	{
		$this->db->where('id',$id);
		$query = $this->db->get($this->tableName);
		return $query->row();
	}
	public function copyContact($id,$campaign_id=null)
	{
		$detail = $this->getContactById($id);
		
		$this->db->set('member_id',$detail->member_id);
		$this->db->set('campaign_id',$campaign_id);
		$this->db->set('name',$detail->name);
		$this->db->set('email',$detail->email);
		$this->db->set('add_time',time());
		$this->db->set('ip',$_SERVER['REMOTE_ADDR']);
		$this->db->insert($this->tableName);
	}
	public function getContactByCampaignId($campaign_id)
	{
		$this->db->select('id,email');
		$this->db->where_in('campaign_id',$campaign_id);
		$this->db->where('status','active');
		$query = $this->db->get($this->tableName);
		return $query->result();
	}
	public function getContactByCampaignIdEx($campaign_id)
	{
		$this->db->select('email');
		$this->db->where_in('campaign_id',$campaign_id);
		$this->db->where('status','active');
		$query = $this->db->get($this->tableName);
		return $query->result_array();
	}
	public function getContactByContactId($id)
	{
		$this->db->where_in('id',$id);
		$query = $this->db->get($this->tableName);
		return $query->result();
	}
	public function getNewsletterListByType($type)
	{
		$this->db->select('message_name,id');
		$this->db->where('newsletter_type',$type);
		$query = $this->db->get($this->tableNewsletter);
		return $query->result();
	}
	
}
