<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Suppression_list_Model extends CI_Model {

	var $tableName = 'tbl_suppression_list';
	var $tableSuppression = 'tbl_suppression';
	
	public function getSuppressionList($member_id,$status)
	{
		if($member_id)
		$this->db->where('member_id',$member_id);
		if($status)
		$this->db->where('status',$status);
		$query = $this->db->get($this->tableName);
		return $query->result();
	}
	public function addList($member_id)
	{
		$this->db->set('member_id',$member_id);
		$this->db->set('list_name',$this->input->post('list_name'));
		$this->db->set('add_time',time());
		$this->db->set('ip',$_SERVER['REMOTE_ADDR']);
		$this->db->insert($this->tableName);
		return $this->db->insert_id();
	}
	public function getListById($id)
	{
		$this->db->where('id',$id);
		$query = $this->db->get($this->tableName);
		return $query->row();
	}
	public function deleteList($id)
	{
		$this->db->where('id',$id);
		$query = $this->db->delete($this->tableName);
		
		$this->db->where('list_id',$id);
		$query = $this->db->delete($this->tableSuppression);
	}
}
