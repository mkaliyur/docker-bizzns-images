<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Template_Model extends CI_Model {

	var $tableName = 'tbl_template';

	public function getAllCount($member_id,$status){
	 	
		if($status)
		$this->db->where('status',$status);
		if($member_id)
		$this->db->where('member_id',$member_id);
		$query = $this->db->get($this->tableName);
		return $query->num_rows();
	 }
	public function getTemplateList($member_id,$page=null,$status=null,$limit=null)
	{
		if($member_id)
		$this->db->where('member_id',$member_id);
		if($status)
		$this->db->where('status',$status);
        if($page)
        $this->db->where('template_type',$page);
		if($limit)
		$this->db->limit($limit);
		$this->db->order_by('id','asc');
		$query = $this->db->get($this->tableName);
		return $query->result();
	}
	public function addTemplate($member_id,$editor_type)
	{
		$this->db->set('member_id',$member_id);
		$this->db->set('template_type','draft');
		$this->db->set('template_name',$this->input->post('template_name'));
		$this->db->set('template_text',$this->input->post('template_text'));
		$this->db->set('editor_type',$editor_type);
		$this->db->set('add_time',time());
		$this->db->set('ip',$_SERVER['REMOTE_ADDR']);
		$this->db->insert($this->tableName);
		return $this->db->insert_id();
	}
	public function updateTemplate($id,$editor_type)
	{
		if($this->input->post('template_name'))
		$this->db->set('template_name',$this->input->post('template_name'));
		if($this->input->post('template_text'))
		$this->db->set('template_text',$this->input->post('template_text'));
		$this->db->set('template_type','draft');
		$this->db->set('editor_type',$editor_type);
		$this->db->where('id',$id);
		$this->db->update($this->tableName);
	}
	public function getTemplateById($id)
	{
		$this->db->where('id',$id);
		$query = $this->db->get($this->tableName);
		return $query->row();
	}
	public function deleteTemplate($id)
	{
		$this->db->where('id',$id);
		$query = $this->db->delete($this->tableName);
	}
        public function removeMultipleTemplate($template_ids)
	{
		$this->db->where_in('id',$template_ids);
		$this->db->delete($this->tableName);
	}
	public function set_status($task,$id){
	    $this->db->set('status',$task);
	    $this->db->where('id',$id);
		$this->db->update($this->tableName);
	}
	public function getTemplateListByType($member_id,$type,$editor_type,$limit)
	{
		if($member_id)
		$this->db->where('member_id',$member_id);
		if($type)
		$this->db->where('template_type',$type);
		if($editor_type)
		$this->db->where('editor_type',$editor_type);
		if($limit)
		$this->db->limit($limit);
		$this->db->order_by('id','asc');
		$query = $this->db->get($this->tableName);
		return $query->result();
	}
	public function getTemplateScreenshots($template_id){
		$this->db->select('template_screenshot');
		$this->db->from($this->tableName);
		$this->db->where_in('id',$template_id);
		$query=$this->db->get();
		return $query->result();
		}
	public function updateImage($template_screenshot,$temp_id)
	{
		$tempdetail = $this->getTemplateById($temp_id);

		unlink('./assets/uploads/template_images/'.$tempdetail->template_screenshot);
		$this->db->set('template_screenshot',$template_screenshot);
		$this->db->where('id',$temp_id);
		$this->db->update($this->tableName);
	}
}
