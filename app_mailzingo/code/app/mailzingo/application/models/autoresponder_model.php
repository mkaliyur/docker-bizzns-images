<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Autoresponder_Model extends CI_Model {

	var $tableName = 'tbl_autoresponder';
	var $tableContact = 'tbl_contact';
	var $tableStats = 'tbl_statistics';
	var $tableQueue = 'tbl_email_queue';
    var $tableUnsubscribe = 'tbl_unsubscribe_list'; 
	var $tableCronURL = 'tbl_cron_urls';
	var $tableFromEmail = 'tbl_from_email';
	var $tableTemplate = 'tbl_template';
	var $tableAttatch = 'tbl_file_attatch';
	
	public function getAllCount($member_id,$status=null){
	 	
		if($status)
		$this->db->where('status',$status);
		if($member_id)
		$this->db->where('member_id',$member_id);
		$query = $this->db->get($this->tableName);
		return $query->num_rows();
	 }
	public function getAutoList($member_id,$status=null,$start=null,$content_per_page=null)
	{
		if($member_id)
		$this->db->where('member_id',$member_id);
		if($status)
		$this->db->where('status',$status);
		$this->db->order_by('id','desc');
		$query = $this->db->get($this->tableName,$content_per_page,$start);
		return $query->result();
	}
	public function getAutoById($member_id,$id=null)
	{
		if($member_id)
		$this->db->where('member_id',$member_id);
		if($id)
		$this->db->where('id',$id);
		$query = $this->db->get($this->tableName);
		return $query->row();
	}
	public function getFromEmailList($member_id=null)
	{
	    $this->db->where($this->tableFromEmail.'.member_id',$member_id);
		$this->db->where($this->tableFromEmail.'.status','varify');
		$query = $this->db->get($this->tableFromEmail);
		return $query->result();
	}
	public function getAttatchFile($newsletter_id=null)
	{
		$this->db->where('newsletter_id',$newsletter_id);
		$this->db->where('type','autoresponder');
		$query = $this->db->get($this->tableAttatch);
		return $query->result();
	}
	public function deleteAttatchFile($newsletter_id)
	{
		$this->db->where('newsletter_id',$newsletter_id);
		$this->db->where('type','autoresponder');
		$this->db->delete($this->tableAttatch);
	}
	public function addAutoresponder($insertArray)
	{
		$this->db->set('member_id',$insertArray['member_id']);
		$this->db->set('from_name',$insertArray['from_name']);
		$this->db->set('from_email',$insertArray['from_email']);
		$this->db->set('reply_to',$insertArray['reply_to']);
		$this->db->set('auto_name',$insertArray['auto_name']);
		$this->db->set('subject',$insertArray['subject']);
		$this->db->set('template_id',$insertArray['template_id']);
		$this->db->set('template_text',$insertArray['template_text']);
		if($insertArray['image'])
		$this->db->set('image',$insertArray['image']);
		if($insertArray['in_campaign_id'])
		$this->db->set('in_campaign_id',$insertArray['in_campaign_id']);
		$this->db->set('mail_day',$insertArray['mail_day']);
		$this->db->set('enable_on',$insertArray['enable_on']);
		$this->db->set('send_time',$insertArray['send_time']);
		$this->db->set('status',$insertArray['status']);
		$this->db->set('add_time',time());
		$this->db->set('ip',$_SERVER['REMOTE_ADDR']);
		$this->db->insert($this->tableName);
		return $this->db->insert_id();
	}
	public function updateAutoresponder($insertArray,$auto_id=null)
	{
		$this->db->set('member_id',$insertArray['member_id']);
		$this->db->set('from_name',$insertArray['from_name']);
		$this->db->set('from_email',$insertArray['from_email']);
		$this->db->set('reply_to',$insertArray['reply_to']);
		$this->db->set('auto_name',$insertArray['auto_name']);
		$this->db->set('subject',$insertArray['subject']);
		$this->db->set('template_id',$insertArray['template_id']);
		$this->db->set('template_text',$insertArray['template_text']);
		if($insertArray['image'])
		$this->db->set('image',$insertArray['image']);
		if($insertArray['in_campaign_id'])
		$this->db->set('in_campaign_id',$insertArray['in_campaign_id']);
		$this->db->set('mail_day',$insertArray['mail_day']);
		$this->db->set('enable_on',$insertArray['enable_on']);
		$this->db->set('send_time',$insertArray['send_time']);
		$this->db->set('status',$insertArray['status']);
		$this->db->set('add_time',time());
		$this->db->set('ip',$_SERVER['REMOTE_ADDR']);
		$this->db->where('id',$auto_id);
		$this->db->update($this->tableName);
	}
	public function addAttatchFile($file_name,$newsletter_id=null)
	{
		$this->db->set('file_name',$file_name);
		$this->db->set('newsletter_id',$newsletter_id);
		$this->db->set('type','autoresponder');
		$this->db->insert($this->tableAttatch);
	}
	public function saveTemplate($member_id,$template_name=null,$template_text=null,$editor_type=null,$template_screenshot=null)
	{
		$this->db->set('member_id',$member_id);
		$this->db->set('template_name',$template_name);
		$this->db->set('template_text',$template_text);
		$this->db->set('template_type','draft');
		$this->db->set('template_screenshot',$template_screenshot);
		$this->db->set('editor_type',$editor_type);
		$this->db->set('add_time',time());
		$this->db->set('ip',$_SERVER['REMOTE_ADDR']);
		$this->db->insert($this->tableTemplate);
	}
	public function deleteAutoresponder($id)
	{
	   $this->db->where('id',$id);
	   $this->db->delete($this->tableName);
	   
	}
	public function removeMultipleAutoresponder($id)
	{
	   $this->db->where_in('id',$id);
	   $this->db->delete($this->tableName);
	}
	public function statusChange($status,$id)
	{
	   $this->db->set('status',$status);
	   $this->db->where('id',$id);
	   $this->db->update($this->tableName);
	}
	public function getAutoresponderListToSend($time,$day=null)
	{
	  $this->db->where('mail_day !=',0);
	  $this->db->where('send_time',$time);
	  $where = "FIND_IN_SET('".$day."', enable_on)";  
      $this->db->where( $where ); 
      $query = $this->db->get($this->tableName);
	  return $query->result();
	}
	public function getAutoContactList($send_date,$campaign_id)
	{
	  $send_date = date('d-m-Y',strtotime('-'.$send_date.' day'));
	  $this->db->where("date_format(from_unixtime(add_time), ('%d-%m-%Y')) <=", $send_date);
      $this->db->where_in('campaign_id',explode(',',$campaign_id)); 
	  $this->db->where('add_from','form');
	 // $this->db->where('autoresponder_sent_time','');
	  $query = $this->db->get($this->tableContact);
	  return $query->result();
	}
	public function setAutoSentTime($id)
	{
	  $this->db->set('autoresponder_sent_time',time());
	  $this->db->where('id',$id);
	  $this->db->update($this->tableContact);
	}
	public function getInstantAutoresponderListToSend($campaign_id)
	{
	  $day = strtolower(date('l'));
	  $this->db->where('mail_day',0);
	  $where = "FIND_IN_SET('".$campaign_id."', in_campaign_id)";  
      $this->db->where( $where ); 
	  $where = "FIND_IN_SET('".$day."', enable_on)";  
      $this->db->where( $where ); 
      $query = $this->db->get($this->tableName);
	  return $query->result();
	}
	public function checkAlreadyMailed($email,$newsletter_id=null)
	{
	   $this->db->where('email',$email);
	   $this->db->where('type','autoresponder');
	   $this->db->where('newsletter_id',$newsletter_id);
	   $qt = $this->db->get($this->tableStats);
	   return $qt->num_rows();
	}


	
	
	
	
	
	
	public function getQueueNewsletter($cron_id)
	{
		$this->db->select($this->tableQueue.'.id as queue_id,'.$this->tableContact.'.email,name,address,city,state,country,zip,phone,'.$this->tableContact.'.id as contactid,'.$this->tableName.'.message_type,'.$this->tableName.'.id as newsletter_id');
		$this->db->where($this->tableQueue.'.cron_id',$cron_id);
		$this->db->join($this->tableName,$this->tableName.'.id='.$this->tableQueue.'.newsletter_id','left');
		$this->db->join($this->tableContact,$this->tableContact.'.id='.$this->tableQueue.'.contact_id','left');
		$this->db->order_by($this->tableQueue .'.id','desc');
		$query = $this->db->get($this->tableQueue);
		return $query->result();
	}
	public function getTemplateDataByCron($cron_id)
	{
		$this->db->select($this->tableName.'.subject,from_name,from_email,reply_to,'.$this->tableName.'.template_text');
		$this->db->where($this->tableQueue.'.cron_id',$cron_id);
		$this->db->join($this->tableName,$this->tableName.'.id='.$this->tableQueue.'.newsletter_id','left');
		$this->db->limit(1);
		$query = $this->db->get($this->tableQueue);
		return $query->row();
	}
	public function addEmailQueue($newsletter_id,$contact_id,$cron_id)
	{
		$this->db->set('cron_id',$cron_id);
		$this->db->set('newsletter_id',$newsletter_id);
		$this->db->set('contact_id',$contact_id);
		$this->db->insert($this->tableQueue);
	}
	public function removeQueueById($id,$newsletter_id)
	{
	   $this->db->where('id',$id);
	   $this->db->delete($this->tableQueue);
	   
	   $this->db->where('newsletter_id',$newsletter_id);
	   $query = $this->db->get($this->tableQueue);
	   $count = $query->num_rows();
	   if($count==0)
	    {
			$this->db->set('status','sent');
			$this->db->where('id',$newsletter_id);
			$this->db->update($this->tableName);
		}
	}
	public function checkQueueNewsletter($cron_id)
	{
		$this->db->where($this->tableQueue.'.cron_id',$cron_id);
		$query = $this->db->get($this->tableQueue);
		return $query->num_rows();
	}
	public function removeCronURL($cron_id)
	{
		$this->db->where('url',site_url('send-newsletter-cron/'.$cron_id));
		$query = $this->db->delete($this->tableCronURL);
	}
}
