<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Campaign_Model extends CI_Model {

	var $tableName = 'tbl_campaign';
	var $tableContact = 'tbl_contact';
	

	function getAllCount($member_id,$status=null){

		if($status)
		$this->db->where('status',$status);
		if($member_id)
		$this->db->where('member_id',$member_id);
		$query = $this->db->get($this->tableName);
		return $query->num_rows();
	 }
	public function getCampaignList($member_id,$status=null,$sort_by=null,$search_by=null)
	{
		if($member_id)
		$this->db->where('member_id',$member_id);
		if($status)
		$this->db->where('status',$status);
		if($sort_by=='campaign_name')
		$this->db->order_by($sort_by,'asc');
		else
		$this->db->order_by('add_time','desc');
		if($search_by)
		$this->db->like('campaign_name',$search_by);
		$query = $this->db->get($this->tableName);
		return $query->result();
	}
	public function addCampaign($member_id = null)
	{
		$this->db->set('member_id',$member_id);
		$this->db->set('campaign_name',$this->input->post('campaign_name'));
		if($this->input->post('campaign_description'))
		$this->db->set('campaign_description',$this->input->post('campaign_description'));
		$this->db->set('add_time',time());
		$this->db->set('ip',$_SERVER['REMOTE_ADDR']);
		$this->db->insert($this->tableName);
	}
	public function updateCampaign($id=null)
	{
		if($this->input->post('campaign_name'))
		$this->db->set('campaign_name',$this->input->post('campaign_name'));
		if($this->input->post('campaign_description'))
		$this->db->set('campaign_description',$this->input->post('campaign_description'));
		$this->db->where('campaign_id',$id);
		$this->db->update($this->tableName);
	}
	public function getCampaignById($id=null)
	{
		$this->db->where('campaign_id',$id);
		$query = $this->db->get($this->tableName);
		return $query->row();
	}
	public function deleteCampaign($id=null)
	{
		$this->db->where('campaign_id',$id);
		$this->db->delete($this->tableName);
		
		$this->db->where('campaign_id',$id);
		$this->db->delete($this->tableContact);
	}
	public function clearCampaign($id=null)
	{
		$this->db->where('campaign_id',$id);
		$this->db->delete($this->tableContact);
	}
	public function removeMultipleCampaign($campaign_ids=null)
	{
		$this->db->where_in('campaign_id',$campaign_ids);
		$this->db->delete($this->tableName);
		$this->removeMultipleContact($campaign_ids);
	}
	public function removeMultipleContact($campaign_ids=null)
	{
		$this->db->where_in('campaign_id',$campaign_ids);
		$this->db->delete($this->tableContact);
	}
	public function copyCampaign($from_campaign,$to_campaign=null)
	{
		$this->db->where('campaign_id',$from_campaign);
		$query = $this->db->get($this->tableContact);
		$data = $query->result();
		
		foreach($data as $val)
		{
			$this->db->set('member_id',$val->member_id);
			$this->db->set('campaign_id',$to_campaign);
			$this->db->set('name',$val->name);
			$this->db->set('email',$val->email);
			$this->db->set('add_time',time());
			$this->db->set('ip',$val->ip);
			$this->db->insert($this->tableContact);
	  }
	}
	public function getCampaignListMove($member_id,$campaign_id=null)
	{
		if($member_id)
		$this->db->where('member_id',$member_id);
		if($campaign_id)
		$this->db->where('campaign_id !=',$campaign_id);
		$this->db->where('status','active');
		$query = $this->db->get($this->tableName);
		return $query->result();
	}
}
