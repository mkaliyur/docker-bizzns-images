<?php
class Dashboard_Model extends CI_Model
{ 
	
	var $tableContact='tbl_contact';	
	var $tableTemplate='tbl_template';	
	var $tableNewsletter='tbl_newsletter';	
	var $tableUnsubscribe='tbl_unsubscribe_list';	
	var $tableStatics='tbl_statistics';

	function getAllContactCount($status,$member_id=null,$type=null){
	 	
		if($type=='day')
		{
		  $typetime = strtotime(date('d-m-Y').'00:00:00');
		} else if($type=='month')
		{
		  $typetime = strtotime('01-'.date('m-Y').'00:00:00');
		}
		if($type)
		$this->db->where('add_time >=',$typetime);
		if($status)
		$this->db->where('status',$status);
		if($member_id)
		$this->db->where('member_id',$member_id);
		$query = $this->db->get($this->tableContact);
		return $query->num_rows();
	 }
	function getAllUnsubscribeCount($status,$member_id=null,$type=null){
	 	
		if($type=='day')
		{
		  $typetime = strtotime(date('d-m-Y').'00:00:00');
		} else if($type=='month')
		{
		  $typetime = strtotime('01-'.date('m-Y').'00:00:00');
		}
		if($type)
		$this->db->where('add_time >=',$typetime);
		if($status)
		$this->db->where('status',$status);
		if($member_id)
		$this->db->where('member_id',$member_id);
		$query = $this->db->get($this->tableUnsubscribe);
		return $query->num_rows();
	 }
	function getAllTemplateCount($status,$member_id=null){
	 	
		if($status)
		$this->db->where('status',$status);
		if($member_id)
		$this->db->where('member_id',$member_id);
		$query = $this->db->get($this->tableTemplate);
		return $query->num_rows();
	 }
	function getAllNewsletterCount($status,$member_id=null){
	 	
		if($status)
		$this->db->where('status',$status);
		if($member_id)
		$this->db->where('member_id',$member_id);
		$query = $this->db->get($this->tableNewsletter);
		return $query->num_rows();
	 }
	function getAllStatsRateCount($fieldName,$fieldValue=null,$member_id=null){
	
	   $this->db->where($this->tableNewsletter.'.status','sent');
	   $this->db->where($this->tableNewsletter.'.member_id',$member_id);
	   $this->db->join($this->tableNewsletter,$this->tableNewsletter.'.id',$this->tableStatics.'.newsletter_id');
	   $query = $this->db->get($this->tableStatics);
	   $totalEmail = $query->num_rows();
		   
	   $this->db->where($this->tableStatics.'.'.$fieldName,$fieldValue);
	   $this->db->where($this->tableNewsletter.'.status','sent');
	   $this->db->where($this->tableNewsletter.'.member_id',$member_id);
	   $this->db->join($this->tableNewsletter,$this->tableNewsletter.'.id',$this->tableStatics.'.newsletter_id');
	   $query = $this->db->get($this->tableStatics);
	   $totalRate = $query->num_rows();
		   
		$allnewsletterRate += $totalRate/$totalEmail*100;

		return round($allnewsletterRate);
	 }
	 function getSubscriberGraph($time_type=null,$graph_type=null,$member_id=null)
	 {
	   if($graph_type=='subscriber')
	   $table = $this->tableContact;
	   else 
	   $table = $this->tableUnsubscribe;
		
		if($time_type=='day')
		{
			$this->db->select("date_format(from_unixtime(add_time), ('%d-%m-%Y')) as new_date");
			$this->db->group_by("date_format(from_unixtime(add_time), ('%d-%m-%Y'))");
			$this->db->where("date_format(from_unixtime(add_time), ('%m-%Y')) =", date('m-Y'));
		} else if($time_type=='month')
		{
			$this->db->select("date_format(from_unixtime(add_time), ('%m-%Y')) as new_date");
			$this->db->group_by("date_format(from_unixtime(add_time), ('%m-%Y'))");
			$this->db->where("date_format(from_unixtime(add_time), ('%Y')) =", date('Y'));
		} else if($time_type=='year')
		{
			$this->db->select("date_format(from_unixtime(add_time), ('%Y')) as new_date");
			$this->db->group_by("date_format(from_unixtime(add_time), ('%Y'))");
		}
		$this->db->where('member_id',$member_id);
		$this->db->order_by('add_time','asc');
		$query = $this->db->get($table);
		$result = $query->result();
		$allresult = array();
		foreach($result as $val)
		{
			if($time_type=='day')
			{
			  $this->db->select("count(id) as total_record,date_format(from_unixtime(add_time), ('%d-%m-%Y')) as added_date");
			  $this->db->where("date_format(from_unixtime(add_time), ('%d-%m-%Y')) =", $val->new_date);
			} else if($time_type=='month')
			{
			  $this->db->select("count(id) as total_record,date_format(from_unixtime(add_time), ('%m-%Y')) as added_date");
			  $this->db->where("date_format(from_unixtime(add_time), ('%m-%Y')) =", $val->new_date);
			} else if($time_type=='year')
			{
			  $this->db->select("count(id) as total_record,date_format(from_unixtime(add_time), ('%Y')) as added_date");
			  $this->db->where("date_format(from_unixtime(add_time), ('%Y')) =", $val->new_date);
			}
			$this->db->where('member_id',$member_id);
			  $query1 = $this->db->get($table);
			  $allresult[] = $query1->row();
		}
		
		 if($time_type=='day')
		 {
		   for($i=1;$i<=date('d');$i++)
		    {
			   $ii = sprintf("%02d", $i);
			   $data[$ii] = '{ x: new Date("'.date('m').', '.$ii.', '.date('Y').'"), y: 0 },';
			}
		 }
		 else if($time_type=='month') {
		    for($i=1;$i<=date('m');$i++)
		    {
			   $ii = sprintf("%02d", $i);
			   $data[$ii] = '{ x: new Date("'.$ii.', 01, '.date('Y').'"), y: 0 },';
			}
		 }
		 else if($time_type=='year') {
		    $lastyear = date('Y')-5;
		    for($i=$lastyear;$i<=date('Y');$i++)
		    {
			   $data[$i] = '{ x: new Date("01, 01, '.$i.'"), y: 0 },';
			}
		 }

		foreach($allresult as $value)
		{
		  if($time_type=='month')
		   { 
		     $added_date = '1-'.$value->added_date; 
			 $key = date('m',strtotime($added_date));
		   }
		  else if($time_type=='year')
		   { 
		     $added_date = '1-1-'.$value->added_date; 
			 $key = date('Y',strtotime($added_date));
		   }
		  else 
		   { 
		     $added_date = $value->added_date; 
			 $key = date('d',strtotime($added_date));
		   }
		  
		  $date = date('m',strtotime($added_date)).', '.date('d',strtotime($added_date)).', '.date('Y',strtotime($added_date));
		 $data[$key] = '{ x: new Date("'.$date.'"), y: '.$value->total_record.' },';
		}
		foreach($data as $valueData)
		 {
		   $newdata.= $valueData;
		 }
		//echo $data; die;
		return $newdata;
	 }
}