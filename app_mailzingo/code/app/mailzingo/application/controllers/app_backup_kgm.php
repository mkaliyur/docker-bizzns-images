<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
 
class App extends CI_Controller {

    function __construct() {
        parent::__construct();
		$this->checkSiteUrl();
		$this->checkDatabase();
		
		if($this->session->userdata('mem_id'))
		{
		  if($this->session->userdata('login_update')=='yes')
		   { 
		     $this->updateLicensing();
		   }
		  $this->session->unset_userdata('login_update');
		  $this->checkLicensing();
		}
		//$this->campaign_count = $this->checkAccess('campaign.campaign_count');
		$this->campaign_count = 1000;			
		//$this->copy_campaign = $this->checkAccess('campaign.copy_campaign');			
		$this->copy_campaign = yes;
		
		//$this->move_campaign = $this->checkAccess('campaign.move_campaign');
		$this->move_campaign = yes;
		
		
		//$this->delete_campaign = $this->checkAccess('campaign.delete_campaign');
		$this->delete_campaign = yes;
		
		
		
		//$this->subscriber_count = $this->checkAccess('subscriber.subscriber_count');
		$this->subscriber_count = unlimited;
		
		
		
		
		
				
		$this->import_subscriber = $this->checkAccess('subscriber.import_subscriber');
		$this->copy_paste_subscriber = $this->checkAccess('subscriber.copy_paste_subscriber');
		$this->add_one_at_time_subscriber = $this->checkAccess('subscriber.add_one_at_time_subscriber');		
		$this->delete_subscriber = $this->checkAccess('subscriber.delete_subscriber');
		$this->search_subscriber = $this->checkAccess('subscriber.search_subscriber');
		$this->export_subscriber = $this->checkAccess('subscriber.export_subscriber');
		$this->copy_subscriber = $this->checkAccess('subscriber.copy_subscriber');
		$this->move_subscriber = $this->checkAccess('subscriber.move_subscriber');
		$this->search_by_bounce = $this->checkAccess('subscriber.search_by_bounce');
		$this->search_by_message = $this->checkAccess('subscriber.search_by_message');
		$this->search_by_unsubscriber = $this->checkAccess('subscriber.search_by_unsubscriber');
		
		$this->supprssion_management = $this->checkAccess('suppression.supprssion_management');
		
		$this->instant_newsletter = $this->checkAccess('newsletter.instant_newsletter');
		$this->schedule_newsletter = $this->checkAccess('newsletter.schedule_newsletter');
		$this->use_newsletter = $this->checkAccess('newsletter.use_newsletter');		
		
		//$this->email_template_count = $this->checkAccess('template.email_template_count');
		$this->email_template_count = 14;
		
		
		
		
		
		//$this->create_template = $this->checkAccess('template.create_template');
		$this->create_template = yes;
		
		//$this->license_count = $this->checkAccess('settings.license_count');
		$this->license_count = unlimited;
		
		
		
		
		//$this->php_mail = $this->checkAccess('settings.php_mail');
		$this->php_mail = yes;
		
		
		//$this->smtp_setup = $this->checkAccess('settings.smtp_setup');
		$this->smtp_setup = yes;
		
		
		//$this->bounce_setup = $this->checkAccess('settings.bounce_setup');
		$this->bounce_setup = yes;
		
		//$this->form_template_count = $this->checkAccess('form.form_template_count');
		$this->form_template_count = 15;
		
		
		
		
		//$this->autoresponder_management = $this->checkAccess('autoresponder.autoresponder_management');
		$this->autoresponder_management = yes;
		
		//$this->statistics = $this->checkAccess('statistics.statistics');
		$this->statistics = yes;
		
		
	    $this->upgrade_link = $this->session->userdata('upgrade_link');
		$this->upgrade_planname = $this->session->userdata('upgrade_planname');
	    $this->update_title = $this->session->userdata('update_title');
		$this->update_description = $this->session->userdata('update_description');
		$this->update_file = $this->session->userdata('update_file');
		$this->update_id = $this->session->userdata('update_id');
    }
	private function checkSiteUrl()
	{
		$pageURL= (@$_SERVER["HTTPS"] == "on") ? "https://" : "http://";
		$root = $pageURL.$_SERVER['HTTP_HOST'];
		$root .= str_replace(basename($_SERVER['SCRIPT_NAME']),"",$_SERVER['SCRIPT_NAME']);
		$current_site = str_replace($pageURL,'',site_url());
		$current_site = str_replace('www.','',$current_site);
		$root1 = str_replace($pageURL,'',$root);
		$root1 = str_replace('www.','',$root1);
		if($root1!=$current_site)
		{
			$template_path 	= './installation/config.php';
			$output_path 	= './application/config/config.php';
			$config_file = file_get_contents($template_path);
			$new  = str_replace("%BASEURL%",$root,$config_file);
			$handle = fopen($output_path,'w+');
			@chmod($output_path,0777);
			if(is_writable($output_path)) {
			if(fwrite($handle,$new)) {
				return true;
			} else {
				return false;
			}

			} else {
				return false;
			}
			
		}
	}
	private function checkDatabase()
	{
	
		if(!$this->session->userdata('mem_id') && isset($this->db)){
		$get =$this->db->query('SHOW TABLES');
		$result = $get->result();
		if($result){
			$tab = 'Tables_in_'.$this->db->database;
			foreach($result as $res){
				 
				$arr[] = $res->$tab;
				 
			}
		}
		
		if(in_array('ci_sessions',$arr) && in_array('tbl_autoresponder',$arr) && in_array('tbl_campaign',$arr) && in_array('tbl_contact',$arr) && in_array('tbl_cron_urls',$arr) && in_array('tbl_email_queue',$arr) && in_array('tbl_file_attatch',$arr) && in_array('tbl_form_template',$arr) && in_array('tbl_from_email',$arr) && in_array('tbl_member',$arr) && in_array('tbl_newsletter',$arr) && in_array('tbl_statistics',$arr) && in_array('tbl_suppression',$arr) && in_array('tbl_suppression_list',$arr) && in_array('tbl_template',$arr) && in_array('tbl_unsubscribe_list',$arr) && in_array('tbl_webform',$arr) && in_array('tbl_website_settings',$arr) )
		{
			$this->db->select('mem_id');
			$query = $this->db->get('tbl_member');
			if($query->num_rows()==0)
			{
				$this->session->set_flashdata('in', 'Please Fill Signup Details');
				$this->session->set_userdata('step1','complete');
				$this->session->set_userdata('step2','complete');
				redirect(site_url().'install/step3');
			}
			else
			{
				if($this->config->item('sess_use_database')==0){
				$pageURL= (@$_SERVER["HTTPS"] == "on") ? "https://" : "http://";
				$root = $pageURL.$_SERVER['HTTP_HOST'];
				$root .= str_replace(basename($_SERVER['SCRIPT_NAME']),"",$_SERVER['SCRIPT_NAME']);
				$template_path 	= './installation/include/config.php';
				$output_path 	= './application/config/config.php';
				$config_file = file_get_contents($template_path);
				$new  = str_replace("%BASEURL%",$root,$config_file);
				$handle = fopen($output_path,'w+');
				@chmod($output_path,0777);
				if(is_writable($output_path)) {
				if(fwrite($handle,$new)) {
					return true;
				} else {
					return false;
				}

				} else {
					return false;
				}}
			}
		}
		else
		{
			
			$this->session->set_flashdata('in', 'Database Tables Not Created Properly');
			redirect(site_url().'install/step1');
		}		
		}
		else if(!$this->db)
		{
			redirect(site_url().'install/step1');
		}
	}
	 
	 
	private function checkLicensing()
	{
	  
	  $license_key = $this->common_model->getSingleFieldFromAnyTable('license_key','member_id',$this->session->userdata('mem_id'),'tbl_website_settings');

	  if(!$license_key) {	  
	    $this->load->view($this->config->item('templateName').'/license_div');
	  }
	}
	public function updateLicensing($license_key,$redirect)
	{ 
		$mem_id = $this->session->userdata('mem_id');

		if($license_key=='')
		 {
		   $license_key = $this->common_model->getSingleFieldFromAnyTable('license_key','member_id',$mem_id,'tbl_website_settings');
		 }

		if($license_key)
		 { 
		   $membersarea_url = $this->config->item('membersarea_url').'check-license-key';

		    $postdata = array('license_key' => $license_key, 'site_url' => site_url(), 'type' => 'check_license');
			
			for($i=0;$i<=2;$i++) {
			
				$curlpost = curl_init($membersarea_url);
				curl_setopt($curlpost, CURLOPT_HEADER, 0);
				curl_setopt($curlpost, CURLOPT_SSL_VERIFYHOST, 0);
				curl_setopt($curlpost, CURLOPT_SSL_VERIFYPEER, 0);
				curl_setopt($curlpost, CURLOPT_RETURNTRANSFER, 1);
				curl_setopt($curlpost, CURLOPT_BINARYTRANSFER,1);
				curl_setopt($curlpost,CURLOPT_MAXREDIRS,30); 
				curl_setopt($curlpost,CURLOPT_FOLLOWLOCATION,1); 
				curl_setopt($curlpost, CURLOPT_POST, 1);
				curl_setopt($curlpost, CURLOPT_POSTFIELDS, $postdata);
				curl_setopt($curlpost, CURLOPT_RETURNTRANSFER, true);
				$result = curl_exec($curlpost);
				curl_close($curlpost);
				
		       if($result)
			    {
			      break;
				}
		   }

			$result = json_decode($result); 
			 
			$plan_array = $result->plan_array; 
			$upgrade_link = $result->upgrade_link; 
			$upgrade_planname = $result->upgrade_planname; 
			$update_title = $result->update_title; 
			$update_description = $result->update_description; 
			$update_file = $result->update_file; 
			$update_id = $result->update_id;
			$sql_update = $result->sql_update;
			if($sql_update=='1')
			$json_file = $result->json_file;

		   if($result->success==true)
			{
			   $this->db->set('license_key',$license_key);
			   $this->db->where('member_id',$mem_id);
			   $this->db->update('tbl_website_settings');
			   $this->session->set_userdata('plan_data',$plan_array);
			   $this->session->set_userdata('upgrade_link',$upgrade_link);
			   $this->session->set_userdata('upgrade_planname',$upgrade_planname);
			   $this->session->set_userdata('update_title',$update_title);
			   $this->session->set_userdata('update_description',$update_description);
			   $this->session->set_userdata('update_file',$update_file);
			   $this->session->set_userdata('update_id',$update_id);
			   $this->session->set_userdata('sql_update',$sql_update);
			   $this->session->set_userdata('json_file',$json_file);
			   
			} else {
			  
			   $this->db->set('license_key','');
			   $this->db->where('member_id',$mem_id);
			   $this->db->update('tbl_website_settings');
			}
		 }
	  if($redirect=='yes') {
	     $this->session->set_userdata('success_msg','Licensing has been successfully updated');
	     redirect('dashboard');
	   }

	}
	public function checkAccess($permission_name)
	{ 
	   $permission_name = explode('.',$permission_name);
	   $plan_data = unserialize($this->session->userdata('plan_data'));
	   //echo '<pre>'; print_r($plan_data); die;
	   return $plan_data[$permission_name[0]][$permission_name[1]];
	}
}

