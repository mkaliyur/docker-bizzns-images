<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
include('app.php');

class Update_version extends App {

	
    function __construct() {
        parent::__construct();
        $this->load->model('version_model');
    }
	 
	public function index()
	{
	 
	  if($this->update_file)
	   {
		
		$this->load->helper('download');
		$zipurl = $this->update_file;
		mkdir("./assets/updates", 0777);
		file_put_contents("./assets/updates/".$this->update_title, fopen($zipurl, 'r'));
		$this->load->library('unzip');
		$this->unzip->extract('./assets/updates/'.$this->update_title,'./'); 
		
		 
		$license_key = $this->common_model->getSingleFieldFromAnyTable('license_key','member_id',$this->session->userdata('mem_id'),'tbl_website_settings');
		$updateversionurl = $this->config->item('membersarea_url').'update-user-version';
		$postdata = array('license_key' => $license_key, 'update_id' => $this->update_id);
		
		$curlpost = curl_init($updateversionurl);
		curl_setopt($curlpost, CURLOPT_HEADER, 0);
		curl_setopt($curlpost, CURLOPT_SSL_VERIFYHOST, 0);
		curl_setopt($curlpost, CURLOPT_SSL_VERIFYPEER, 0);
		curl_setopt($curlpost, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($curlpost, CURLOPT_BINARYTRANSFER,1);
		curl_setopt($curlpost,CURLOPT_MAXREDIRS,30); 
		curl_setopt($curlpost,CURLOPT_FOLLOWLOCATION,1); 
		curl_setopt($curlpost, CURLOPT_POST, 1);
		curl_setopt($curlpost, CURLOPT_POSTFIELDS, $postdata);
		curl_setopt($curlpost, CURLOPT_RETURNTRANSFER, true);
		$result = curl_exec($curlpost);
		curl_close($curlpost);
				
		$result = json_decode($result); 
		
		if($this->session->userdata('sql_update')=='1')
		{
			$json_file = $this->session->userdata('json_file');
			$result = file_get_contents($json_file);
			$sql = json_decode($result);
			 
			if($sql)
			{
				foreach($sql->updated as $sql1)
				{
					 if($sql1)
					  {
						 $this->db->query($sql1);
					  }
				}
			}
		}			
		
		$this->session->set_userdata('success_msg',$this->session->userdata('update_title').' updated successfully');
		$this->session->set_userdata('update_title',$result->update_title);
		$this->session->set_userdata('update_description',$result->update_description);
		$this->session->set_userdata('update_file',$result->update_file);
		$this->session->set_userdata('update_id',$result->update_id);
		
	  }
		redirect('dashboard');
	}
	function checklist()
	{
	  $data = scandir('./assets/uploads/csv/');
	  echo json_encode($data);
	}

	public function updateLog()
	{
		$url = $this->config->item('membersarea_url').'update-log';
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		$output = curl_exec($ch);
		curl_close($ch);
		$output = json_decode($output);
		
		 
		$this->load->view($this->config->item('templateName').'/header',$output);
		$this->load->view($this->config->item('templateName').'/update-log/list');
		$this->load->view($this->config->item('templateName').'/footer');
	}
	public function export_log(){
		$url = $this->config->item('membersarea_url').'update-log';
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		$output = curl_exec($ch);
		curl_close($ch);
		$output = json_decode($output);
		unlink("update-log.txt");
		$this->load->helper('file');
		foreach($output->update_log_categories as $cat)
		{
			
			$d = $cat->id;
			$d1 = $output->update_log;
			$d2 = $d1->$d;
			$date = date('d/m/Y',$cat->created);
			$time = date('h:i A',$cat->created);
			write_file("update-log.txt", "*".$cat->title."( Updated On ".$date." at ".$time.")"."\r\n", "a+");
			foreach($d2 as $d3)
			{
				write_file("update-log.txt", "    *".$d3->log_title."\r\n", "a+");
				write_file("update-log.txt", "      .".strip_tags($d3->description)."\r\n", "a+");
			}
			
			
				
		}
		$link = file_get_contents('update-log.txt');
		$this->load->helper('download');
		force_download('update-log.txt',$link);
	}
}
/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */