<?php
defined('BASEPATH') OR exit('No direct script access allowed');
include('app.php');

require 'spark_post/autoload.php'; 
use SparkPost\SparkPost;
use GuzzleHttp\Client; 
use Http\Adapter\Guzzle6\Client as GuzzleAdapter;

require_once('postmark/autoload.php');
use Postmark\PostmarkClient;

require 'mailgun/autoload.php';
use Mailgun\Mailgun;
require 'elastic_mail/autoload.php';
require 'sendgrid/autoload.php';
class Newsletter extends App {

	function __construct() 
	{ 
	    parent::__construct();				
		$this->load->model('newsletter_model');
		$this->load->model('template_model');
		$this->load->model('campaign_model');
		$this->load->model('contact_model');
		$this->load->model('suppression_list_model');
		$this->load->model('suppression_model');
		$this->load->model('unsubscribe_model');
		$this->load->model('mailsending_model');
		$this->load->model('statics_model');
		$this->checkLogin();
		$this->member_id = $this->session->userdata('mem_id');
	}
	function checkLogin()
	{
	  if($this->session->userdata('mem_id')=='')
	  { redirect('login'); }
	  
	}
	public function message()
	{

		$this->load->view($this->config->item('templateName').'/header',$output);
		$this->load->view($this->config->item('templateName').'/message/message');
		$this->load->view($this->config->item('templateName').'/footer');
	}
	public function index($status)
	{
		if($status == 'sent')
		$output['meta_title'] = "Newsletter Sent Mail";
		if($status == 'scheduled')
		$output['meta_title'] = "Newsletter Scheduled Mail";
		if($status == 'scheduled')
		$output['meta_title'] = "Newsletter Draft Mail";
		if($status == 'process')
		$output['meta_title'] = "Newsletter Processing Mail";
		
		$output['total_data'] = $this->newsletter_model->getnewsletterList($this->member_id,$status,$start,$content_per_page,$index=1);
		$output['sentCount'] = $this->newsletter_model->getAllCount($this->member_id,'sent');
		$output['scheduledCount'] = $this->newsletter_model->getAllCount($this->member_id,'scheduled');
		$output['draftCount'] = $this->newsletter_model->getAllCount($this->member_id,'draft');
		$output['processCount'] = $this->newsletter_model->getAllCount($this->member_id,'process');
        $output['status'] = $status;
		$this->session->unset_userdata('newsletter_session');
		$this->load->view($this->config->item('templateName').'/header',$output);
		$this->load->view($this->config->item('templateName').'/message/newsletter/newsletter_list');
		$this->load->view($this->config->item('templateName').'/footer');
	}
	public function load_more()
	{
			$group_no = $this->input->post('group_no');
			$output['status']=$status = $this->input->post('status');
			$content_per_page = 8;
			$start = ceil($group_no * $content_per_page);
			$output['allRecord'] = $allRecord = $this->newsletter_model->getnewsletterList($this->member_id,$status,$start,$content_per_page,$index);
			
		echo $this->load->view($this->config->item('templateName').'/message/newsletter/newsletter-pagination',$output,true);
	}
	public function addNewsletter($newsletter_id)
	{
		$output['meta_title'] = "Create Newsletter";
		if($_GET['add_type']=='new')
		$this->session->unset_userdata('newsletter_session');
		
		$newsletter_session = $this->session->userdata('newsletter_session');
		
		if($newsletter_id && $newsletter_session['newsletter_id']=='')
		  { 
		    $newsletter_detail = $this->newsletter_model->getNewsletterById($this->member_id,$newsletter_id);
			$newsletter_session['in_campaign_id'] = explode(',',$newsletter_detail->in_campaign_id);
			$newsletter_session['ex_campaign_id'] = explode(',',$newsletter_detail->ex_campaign_id);
			$newsletter_session['sep_list_id'] = explode(',',$newsletter_detail->sep_list_id);
			$newsletter_session['step5'] = 'yes';
			$newsletter_session['template_text'] = $newsletter_detail->template_text;
			$newsletter_session['image'] = $newsletter_detail->image;
			$newsletter_session['step4'] = 'yes';
			$newsletter_session['template_id'] = $newsletter_detail->template_id;
			$newsletter_session['step3'] = 'yes';
			$newsletter_session['message_name'] = $newsletter_detail->message_name;
		    $newsletter_session['subject'] = $newsletter_detail->subject;
			$newsletter_session['from_name'] = $newsletter_detail->from_name;
			$newsletter_session['from_email'] = $newsletter_detail->from_email;
			$newsletter_session['reply_to'] = $newsletter_detail->reply_to;
		    $newsletter_session['step2'] = 'yes';
			$newsletter_session['message_type'] = $newsletter_detail->message_type;
			if($newsletter_detail->message_type=='schedule')
			$newsletter_session['schedule_time'] = $newsletter_detail->schedule_date;
			if($newsletter_session['dirname']=='')
			{ 
				$attatchment = $this->newsletter_model->getAttatchFile($newsletter_id);
				$attatchmentArr = array();
				$dirname = time();
				$newsletter_session['dirname'] = $dirname;
				foreach($attatchment as $valA)
				{
				 $size = filesize('./assets/uploads/email_attatchment/'.$newsletter_id.'/'.$valA->file_name)/1024;
				 $attatchmentArr[] = array('name'=>$valA->file_name,'size'=>round($size,2));
				 mkdir("./assets/uploads/temp_attatchment/".$dirname, 0777);
				 copy('./assets/uploads/email_attatchment/'.$newsletter_id.'/'.$valA->file_name,'./assets/uploads/temp_attatchment/'.$dirname.'/'.$valA->file_name);
				}
				$newsletter_session['attatchment'] = $attatchmentArr;
		    }
			$newsletter_session['newsletter_id'] = $newsletter_id;
			$newsletter_session['newsletter_status'] = $newsletter_detail->status;
			if($newsletter_session['template_id']=='' || $newsletter_session['template_id']==0)
		    $newsletter_session['editor_type'] = 'plain';
			else
		    $newsletter_session['editor_type'] = 'inline';
			$this->session->set_userdata('newsletter_session',$newsletter_session);
	     }
		if($newsletter_detail->status=='sent' && $this->use_newsletter!='yes')
			 {
			   $this->session->set_userdata('error_msg','Please upgrade to use newsletter feature. <a target="_blank" href="'.$this->upgrade_link.'" class="upgradelink">Click here to upgrade </a>');
	            redirect('newsletter-list/sent');
			 }
		$output['newsletter_session'] = $newsletter_session;
		if($_GET['step']) { $step = $_GET['step']; } else { $step = ''; }
		/* for step 1 */
		if($step=='' || $step==1)
		 {
			$output['meta_title'] = 'Newsletter Setting'; 
		   if($newsletter_session['editor_type']=='')
		   redirect('choose-newsletter-editor');

		   $output['message_name'] = $newsletter_session['message_name'];
		   $output['subject'] = $newsletter_session['subject'];
		   $output['from_name'] = $newsletter_session['from_name'];
		   $output['from_email'] = $newsletter_session['from_email'];
		   $output['reply_to'] = $newsletter_session['reply_to'];

		   $output['from_emails'] = $from_emails = $this->newsletter_model->getFromEmailList($this->member_id);
		   $step = 1;
		 }
		if($_POST['step1'])
		 {
		   $output['message_name'] = $this->input->post('message_name');
		   $output['subject'] = $this->input->post('subject');
		   $output['from_name'] = $this->input->post('from_name');
		   $output['from_email'] = $this->input->post('from_email');
		   $output['reply_to'] = $this->input->post('reply_to');
		   $mail_type = $this->common_model->getSingleFieldFromAnyTable('mail_type','member_id',$this->session->userdata('mem_id'),'tbl_website_settings');

		   $this->form_validation->set_rules('message_name','Message Name','trim|required');
		   $this->form_validation->set_rules('subject','Subject','trim|required');
		   $this->form_validation->set_rules('from_name','From Name','trim|required');
		   if($mail_type=='smtp' && $mail_type=='default') {
		   $this->form_validation->set_rules('from_email','From Email','trim|required');
		   $this->form_validation->set_rules('reply_to','Reply To','trim|required');
		   }
		  
		   if($this->form_validation->run())
		    {
			   $newsletter_session['message_name'] = $this->input->post('message_name');
			   $newsletter_session['subject'] = $this->input->post('subject');
			   $newsletter_session['from_name'] = $this->input->post('from_name');
			   $newsletter_session['from_email'] = $this->input->post('from_email');
			   $newsletter_session['reply_to'] = $this->input->post('reply_to');
			   $newsletter_session['step2'] = 'yes';
			   $this->session->set_userdata('newsletter_session',$newsletter_session);
			   redirect('add-newsletter?step=2');
			}
		}
		/* step 1 ends here */
		
		/* for step 2 */
		if($step==2)
		 {
			$output['meta_title'] = 'Newsletter Templates';
		   if($newsletter_session['step2']=='yes')
		    {
			  if($newsletter_session['editor_type']=='inline')
			  $output['defaultTemplate'] = $this->template_model->getTemplateListByType($this->member_id,'default','',$this->email_template_count);
			  $output['draftTemplate'] = $draftTemplate = $this->template_model->getTemplateListByType($this->member_id,'draft',$newsletter_session['editor_type']);
			  $output['template_id'] = $newsletter_session['template_id'];
			  if(sizeof($draftTemplate)==0 && $newsletter_session['editor_type']=='plain')
			   {
				 //$newsletter_session['template_text'] = '';
			     $newsletter_session['template_id'] = '';
			     $newsletter_session['step3'] = 'yes';
				 $this->session->set_userdata('newsletter_session',$newsletter_session);
				 redirect('add-newsletter?step=3');
			   }
			}
		   else {
		     redirect('add-newsletter');
		   }
		 }
		 
		 if($_POST['step2']){
		    
			if($newsletter_session['step2']=='yes')
		    {
			   $this->form_validation->set_rules('template_id','Choose Template','trim|required');
			   
			   if($this->form_validation->run())
				{
				  $template_detail = $this->template_model->getTemplateById($this->input->post('template_id'));
			      $newsletter_session['template_text'] = str_replace('{image_path}',$this->config->item('templateAssetsPath').'editor/',$template_detail->template_text);
			      $newsletter_session['template_id'] = $this->input->post('template_id');
			      $newsletter_session['step3'] = 'yes';
				  $this->session->set_userdata('newsletter_session',$newsletter_session);
				  redirect('add-newsletter?step=3');
				}
			}
			 else {
		     redirect('add-newsletter');
		   }
		 }
		/* step 2 ends here */
		
		/* for step 3 */
		if($step==3)
		 {
			 $output['meta_title'] = 'Newsletter Editor';
		   if($newsletter_session['step3']=='yes')
		    {
			   $output['template_text'] = $newsletter_session['template_text']; 
			}
			else {
		     redirect('add-newsletter?step=2');
		   }
		 }

		  if($_POST['step3']){
		    
			if($newsletter_session['step3']=='yes')
		    {
			   
			   $output['template_text'] = $this->input->post('template_text');
			   $this->form_validation->set_rules('template_text','Edit Template','trim|required');

			   if($this->form_validation->run())
				{
			      $newsletter_session['template_text'] = $this->input->post('template_text');
				  $newsletter_session['image_text'] = $this->input->post('image_text');
				  $newsletter_session['step4'] = 'yes';
				  $this->session->set_userdata('newsletter_session',$newsletter_session);
				  redirect('add-newsletter?step=4');
				}
			}
			else {
		     redirect('add-newsletter?step=2');
		   }
		 }
		/* step 3 ends here */
		
		/* for step 4 */
		if($step==4)
		 { 
			$output['meta_title'] = 'Newsletter Subscriber';
		   if($newsletter_session['step4']=='yes')
		    {
			   $output['in_campaign_id']  = $newsletter_session['in_campaign_id'];
			   $output['ex_campaign_id']  = $newsletter_session['ex_campaign_id'];
			   $output['sep_list_id']  = $newsletter_session['sep_list_id'];
			}
			else { 
		     redirect('add-newsletter?step=3');
		   }
		 }
		 
		 if($_POST['step4']){
		    
			if($newsletter_session['step4']=='yes')
		    {
			  $output['in_campaign_id']  = $this->input->post('in_campaign_id');
			  $output['ex_campaign_id']  = $this->input->post('ex_campaign_id');
			  $output['sep_list_id']  = $this->input->post('sep_list_id');

			  $this->form_validation->set_rules('in_campaign_id[]','Choose Campaign','trim|required');
			   
			   if($this->form_validation->run())
				{
			        $newsletter_session['in_campaign_id'] = $this->input->post('in_campaign_id');
			        $newsletter_session['ex_campaign_id'] = $this->input->post('ex_campaign_id');
			        $newsletter_session['sep_list_id'] = $this->input->post('sep_list_id');
					$newsletter_session['step5'] = 'yes';
					$this->session->set_userdata('newsletter_session',$newsletter_session);
					redirect('add-newsletter?step=5');
			    }
			}
			else {
		     redirect('add-newsletter?step=3');
		   }
		 }
		/* step 4 ends here */
		
		/* for step 5 */
		if($step==5 && $_POST['step5']=='')
		 { 
			$output['meta_title'] = 'Newsletter Send';
		   if($newsletter_session['step5']=='yes')
		    {
			  $output['message_name'] = $newsletter_session['message_name'];
			  $output['subject'] = $newsletter_session['subject'];
			  $output['template_name'] = $this->common_model->getSingleFieldFromAnyTable('template_name','id',$newsletter_session['template_id'],'tbl_template');
			  $output['message_type'] = $newsletter_session['message_type'];
			  if($newsletter_session['schedule_time'])
			  $output['schedule_time'] = date('d-m-Y H:i',strtotime($newsletter_session['schedule_time']));
			  $output['in_campaign_id'] = $in_campaign_id = $newsletter_session['in_campaign_id'];
			  $output['ex_campaign_id'] = $ex_campaign_id = $newsletter_session['ex_campaign_id'];
			  $output['sep_list_id'] = $sep_list_id = $newsletter_session['sep_list_id'];
			}
			else { 
		     redirect('add-newsletter?step=4');
		   }
		 }

		 if($_POST['step5draft']=='draft')
		 {  
			if($newsletter_session['step5']=='yes')
		    {
			  
			  if($this->input->post('message_type')=='schedule')
			  {
				  $this->form_validation->set_rules('schedule_time','Schedule Time','trim|required|callback_validateScheduleTime');
			  }
             $output['message_type'] = $message_type = $this->input->post('message_type');
			 if($this->input->post('schedule_time'))
			 $output['schedule_time'] = date('d-m-Y H:i',strtotime($this->input->post('schedule_time')));
			 $output['message_name'] = $newsletter_session['message_name'];
			 $output['subject'] = $subject = $newsletter_session['subject'];
			 $output['template_name'] = $this->common_model->getSingleFieldFromAnyTable('template_name','id',$newsletter_session['template_id'],'tbl_template');
             $output['contact_count'] = $this->input->post('contact_count');
			 
			  $this->form_validation->set_rules('message_type','Message Type','trim|required|callback_validateMessageType');
			   if($this->form_validation->run())
				{
					$message = $newsletter_session['template_text'];
					$from_name = $newsletter_session['from_name'];
					$from_email = $newsletter_session['from_email'];
					$reply_to = $newsletter_session['reply_to'];
					$contact_id = $this->getFinalContactIds($newsletter_session);
					$schedule_time = date('Y-m-d H:i:s',strtotime($this->input->post('schedule_time')));
					$status = 'draft';
					
					if($newsletter_session['editor_type']=='inline')
					$template_id = $newsletter_session['template_id'];
					else
					$template_id = 0;
					
					if($newsletter_session['image_text'])
					{ 
						$filteredData=substr($newsletter_session['image_text'], strpos($newsletter_session['image_text'], ",")+1);
						$unencodedData=base64_decode($filteredData);
						$filename = $this->member_id.time().'.png';
						file_put_contents('./assets/uploads/newsletter_images/'.$filename, $unencodedData);
					} else {
						$filename = $this->member_id.time().'.png';
						copy('./assets/uploads/newsletter_images/'.$newsletter_session['image'], './assets/uploads/newsletter_images/'.$filename);
					}

					$insertArray = array(
									"member_id" => $this->member_id,
									"message_type" => $message_type,
									"from_name" => $from_name,
									"from_email" => $from_email,
									"reply_to" => $reply_to,
									"message_name" => $newsletter_session['message_name'],
									"subject" => $subject,
									"template_id" => $template_id,
									"template_text" => $message,
									"in_campaign_id" => implode(",",$newsletter_session['in_campaign_id']),
									"ex_campaign_id" => implode(",",$newsletter_session['ex_campaign_id']),
									"sep_list_id" => implode(",",$newsletter_session['sep_list_id']),
									"contact_id" => $contact_id,
									"image" => $filename,
									"schedule_time" => $schedule_time,
									"status" => $status
									);
					if($newsletter_session['newsletter_id']!='' && $newsletter_session['newsletter_status']!='sent')
					{ 
					 unlink('./assets/uploads/newsletter_images/'.$newsletter_session['image']);
					 $newsletter_id = $newsletter_session['newsletter_id'];
					 $this->newsletter_model->updateNewsletter($insertArray,$newsletter_id);
					 $attatchment = $this->newsletter_model->getAttatchFile($newsletter_id);
					 foreach($attatchment as $valA1)
					 {
					  unlink('./assets/uploads/email_attatchment/'.$newsletter_id.'/'.$valA1->file_name);
					 }
				     $this->newsletter_model->deleteAttatchFile($newsletter_id);
					 $this->newsletter_model->deleteFromCronTable($newsletter_id);
					 $this->newsletter_model->removeQueueByNewsletterId($newsletter_id);
 					} else {
					 $newsletter_id = $this->newsletter_model->addNewsletter($insertArray);
					}
					$attatchArray = $newsletter_session['attatchment'];
					foreach($attatchArray as $valAttatch)
					 {
					  $this->newsletter_model->addAttatchFile($valAttatch['name'],$newsletter_id);
					  $dirname = $newsletter_session['dirname'];
					  mkdir("./assets/uploads/email_attatchment", 0777);
					  mkdir("./assets/uploads/email_attatchment/".$newsletter_id, 0777);
					  copy('./assets/uploads/temp_attatchment/'.$dirname.'/'.$valAttatch['name'], './assets/uploads/email_attatchment/'.$newsletter_id.'/'.$valAttatch['name']);
					  unlink('./assets/uploads/temp_attatchment/'.$dirname.'/'.$valAttatch['name']);
					 }
					 rmdir('./assets/uploads/temp_attatchment/'.$dirname);
					 $this->session->unset_userdata('newsletter_session');
	                 redirect('newsletter-successfull?type=draft');
			  }

			}
			else {
		     redirect('add-newsletter?step=4');
		   }
		  } 
		 else if($_POST['step5']=='step5'){ 
			if($newsletter_session['step5']=='yes')
		    { 
			  if($this->input->post('message_type')=='schedule')
			  {
				  $this->form_validation->set_rules('schedule_time','Schedule Time','trim|required|callback_validateScheduleTime');
			  }
			  $this->form_validation->set_rules('message_type','Message Type','trim|required|callback_validateMessageType');
			  
			 $output['message_type'] = $message_type = $this->input->post('message_type');
			 if($this->input->post('schedule_time'))
			 $output['schedule_time'] = date('d-m-Y H:i',strtotime($this->input->post('schedule_time')));
			 $output['message_name'] = $newsletter_session['message_name'];
			 $output['subject'] = $subject = $newsletter_session['subject'];
			 $output['template_name'] = $this->common_model->getSingleFieldFromAnyTable('template_name','id',$newsletter_session['template_id'],'tbl_template');
             $output['contact_count'] = $this->input->post('contact_count');
			 
			   if($this->form_validation->run())
				{
					$message = $newsletter_session['template_text'];
					$from_name = $newsletter_session['from_name'];
					$from_email = $newsletter_session['from_email'];
					$reply_to = $newsletter_session['reply_to'];
					$contact_id = $this->getFinalContactIds($newsletter_session);
					$schedule_time = date('Y-m-d H:i:s',strtotime($this->input->post('schedule_time')));
					if($message_type=='schedule')
					$status = 'scheduled';
					else 
					$status = 'process';
					
					if($newsletter_session['editor_type']=='inline')
					$template_id = $newsletter_session['template_id'];
					else
					$template_id = 0;
					
					if($newsletter_session['image_text'])
					{
						$filteredData=substr($newsletter_session['image_text'], strpos($newsletter_session['image_text'], ",")+1);
						$unencodedData=base64_decode($filteredData);
						$filename = $this->member_id.time().'.png';
						file_put_contents('./assets/uploads/newsletter_images/'.$filename, $unencodedData);
				    }else {
						$filename = $this->member_id.time().'.png';
						copy('./assets/uploads/newsletter_images/'.$newsletter_session['image'], './assets/uploads/newsletter_images/'.$filename);
					}
					
					$insertArray = array(
									"member_id" => $this->member_id,
									"message_type" => $message_type,
									"from_name" => $from_name,
									"from_email" => $from_email,
									"reply_to" => $reply_to,
									"message_name" => $newsletter_session['message_name'],
									"subject" => $subject,
									"template_id" => $template_id,
									"template_text" => $message,
									"in_campaign_id" => implode(",",$newsletter_session['in_campaign_id']),
									"ex_campaign_id" => implode(",",$newsletter_session['ex_campaign_id']),
									"sep_list_id" => implode(",",$newsletter_session['sep_list_id']),
									"contact_id" => $contact_id,
									"image" => $filename,
									"schedule_time" => $schedule_time,
									"status" => $status
									);
					if($newsletter_session['newsletter_id']!='' && $newsletter_session['newsletter_status']!='sent')
					{ 
					 unlink('./assets/uploads/newsletter_images/'.$newsletter_session['image']);
					 $newsletter_id = $newsletter_session['newsletter_id'];
					 $this->newsletter_model->updateNewsletter($insertArray,$newsletter_id);
					 $attatchment = $this->newsletter_model->getAttatchFile($newsletter_id);
					 foreach($attatchment as $valA1)
					 {
					  unlink('./assets/uploads/email_attatchment/'.$newsletter_id.'/'.$valA1->file_name);
					 }
				     $this->newsletter_model->deleteAttatchFile($newsletter_id);
					 $this->newsletter_model->deleteFromCronTable($newsletter_id);
					 $this->newsletter_model->removeQueueByNewsletterId($newsletter_id);
 					} else {  
					 $newsletter_id = $this->newsletter_model->addNewsletter($insertArray);
					}
					$attatchArray = $newsletter_session['attatchment'];
					foreach($attatchArray as $valAttatch)
					 {
					  $this->newsletter_model->addAttatchFile($valAttatch['name'],$newsletter_id);
					  $dirname = $newsletter_session['dirname'];
					  mkdir("./assets/uploads/email_attatchment", 0777);	
					  mkdir("./assets/uploads/email_attatchment/".$newsletter_id, 0777);
					  copy('./assets/uploads/temp_attatchment/'.$dirname.'/'.$valAttatch['name'], './assets/uploads/email_attatchment/'.$newsletter_id.'/'.$valAttatch['name']);
					  unlink('./assets/uploads/temp_attatchment/'.$dirname.'/'.$valAttatch['name']);
					 }
					 rmdir('./assets/uploads/temp_attatchment/'.$dirname);
					
					 $this->session->unset_userdata('newsletter_session');
	                 redirect('newsletter-successfull?type='.$status);
			  }

			}
			else {
		     redirect('add-newsletter?step=4');
		   }
		 }
		/* step 5 ends here */
		$output['step'] = $step;
		
		if($step==3 && $newsletter_session['editor_type']=='inline')
		 {
		   $this->load->view($this->config->item('templateName').'/message/newsletter/inline_editor',$output);
		 } else {
		   $this->load->view($this->config->item('templateName').'/header',$output);
		   $this->load->view($this->config->item('templateName').'/message/newsletter/newsletter_step'.$step);
		   $this->load->view($this->config->item('templateName').'/footer');
		}
	}
	function validateMessageType($type)
	{
	   if($type=='instant')
	    {  $condition = $this->instant_newsletter; }
	   else { $condition = $this->schedule_newsletter; }   

	   if($condition=='yes')
		 { return true; }
	   else 
		 { 
		   $this->form_validation->set_message('validateMessageType','Please upgrade to use '.$type.' newsletter feature. <a target="_blank" href="'.$this->upgrade_link.'" class="upgradelink">Click here to upgrade </a>');
		   return FALSE;
		 }
	}
	
	public function getCampaignList()
	{
	  $newsletter_session = $this->session->userdata('newsletter_session');
	  $output['camp_type'] = $camp_type = $this->input->post('camp_type');
	  $output['in_campaign_id']  = $newsletter_session['in_campaign_id'];
	  $output['ex_campaign_id']  = $newsletter_session['ex_campaign_id'];
	  $output['sep_list_id']  = $newsletter_session['sep_list_id'];
			   
	  if($camp_type=='sep_ul')
	  $output['suppression_list'] = $this->suppression_list_model->getSuppressionList($this->member_id);
	  else
	  $output['campaign_list'] = $this->campaign_model->getCampaignList($this->member_id);
	  $response['html'] = $this->load->view($this->config->item('templateName').'/message/newsletter/campaign_li',$output,true);
	  $response['success'] = true;
	  $this->output
		   ->set_content_type('application/json')
		   ->set_output(json_encode($response));
	}
	public function addTempAttatchment()
	{
     $newsletter_session = $this->session->userdata('newsletter_session');
	 if($newsletter_session['dirname']=='')
     $dirname = time();
	 else
	 $dirname = $newsletter_session['dirname'];
	 mkdir("./assets/uploads/temp_attatchment", 0777);	
	 mkdir("./assets/uploads/temp_attatchment/".$dirname, 0777);	
	 $config['upload_path'] = './assets/uploads/temp_attatchment/'.$dirname.'/';
	 $config['allowed_types'] = '*';
	 $this->load->library('upload', $config);				
	  if(!$this->upload->do_upload('attatchment'))
	  {
	     $this->upload->display_errors();	
	  } else {
	     $data1 = array('upload_data' => $this->upload->data());
	  }
	 $attatchment = $newsletter_session['attatchment'];
	 if($attatchment=='')
	 $attatchment = array();
	 
	 $attatchment[] = array('name' => $data1['upload_data']['file_name'], 'size' => round($_FILES['attatchment']['size']/1024,2));
	 $newsletter_session['attatchment'] = $attatchment;	
	 $newsletter_session['dirname'] = $dirname;	
	 $this->session->set_userdata('newsletter_session',$newsletter_session);	
	 $data['success'] = true;
	 $data['attatchment'] = $attatchment;
	 $data['attatchCount'] = sizeof($attatchment);
	 echo json_encode($data); die;

	}
	public function deleteAttatchment()
	{
	$newsletter_session = $this->session->userdata('newsletter_session');	  
	 $name = $this->input->post('name');
	 $dirname = $newsletter_session['dirname'];
	 unlink('./assets/uploads/temp_attatchment/'.$dirname.'/'.$name);
	 $attatchment = $newsletter_session['attatchment'];
	 foreach($attatchment as $key=>$val)
	  {
	   if($val['name']==$name)
	   unset($attatchment[$key]);
	  }
	 $newsletter_session['attatchment'] = $attatchment;
	 $this->session->set_userdata('newsletter_session',$newsletter_session);	
	 
	 $data['success'] = true;
	 $data['attatchCount'] = sizeof($attatchment);
	 echo json_encode($data); die;

	}
	public function saveTemplate()
	{
	   if($_POST)
	   {
	     $newsletter_session = $this->session->userdata('newsletter_session');
		 $template_name = $newsletter_session['message_name'];
		 $template_text = $this->input->post('template_text');
		 
		 $filteredData=substr($_POST['image_text'], strpos($_POST['image_text'], ",")+1);
	     $unencodedData=base64_decode($filteredData);
		 $filename = $this->member_id.time().'.png';
		 file_put_contents('./assets/uploads/template_images/'.$filename, $unencodedData);

		 $this->newsletter_model->saveTemplate($this->member_id,$template_name,$template_text,$newsletter_session['editor_type'],$filename);
	   }
	 
	 $data['success'] = true;
	 echo json_encode($data); die;

	}
	public function sendTestMail()
	{
	  if($_POST)
	   {
		   $this->form_validation->set_rules('template_text','Template','trim|required');
		   $this->form_validation->set_rules('testemail','Test Email','trim|required|valid_email');

		  if($this->form_validation->run())
		  {    
		       $newsletter_session = $this->session->userdata('newsletter_session');
			   
			   $mailArray = array();
			   $mailArray['from_name'] = $newsletter_session['from_name'];
			   $mailArray['from_email'] = $newsletter_session['from_email'];
			   $mailArray['reply_to'] = $newsletter_session['reply_to'];
			   $mailArray['message'] = $this->input->post('template_text');
			   $mailArray['contact_email'] = $this->input->post('testemail');
			   $mailArray['attatchmentPath'] = 'temp_attatchment/'.$newsletter_session['dirname'];
			   $mailArray['member_id'] = $this->member_id;
			   
			   $mail_type = $this->common_model->getSingleFieldFromAnyTable('mail_type','member_id',$this->member_id,'tbl_website_settings');
			   if($mail_type!='default' && $mail_type!='smtp')
				 {
				   $api_value = $this->common_model->getSingleFieldFromAnyTable('api_value','api_name',$mail_type,'tbl_external_mail_api');
				   $api_data = unserialize($api_value);
				   $mailArray['from_email'] = $from_email = $api_data['sending_email'];
				 }
			   
			   $Mailfooter = '<br/><br/><br/><br/>'.$this->footerText($this->member_id);
			   $Mailfooter = str_replace("{#to_email}",$mailArray['contact_email'],$Mailfooter);
			   $Mailfooter = str_replace("{#from_email}",$mailArray['from_email'],$Mailfooter);
			   $Mailfooter = str_replace("{#unsubscribe_link}",'#',$Mailfooter);
			   
			   $subject=str_replace("{#name}",ucfirst($val->name),$newsletter_session['subject']);
		       $subject=str_replace("{#email}",$val->email,$subject);
			   $subject=str_replace("{#address}",$val->address,$subject);
			   $subject=str_replace("{#city}",$val->city,$subject);
			   $subject=str_replace("{#state}",$val->state,$subject);
			   $subject=str_replace("{#country}",$val->country,$subject);
			   $subject=str_replace("{#zip}",$val->zip,$subject);
			   $subject=str_replace("{#phone}",$val->phone,$subject);
			   $mailArray['subject'] = $subject;
			   
			   $message=str_replace("{#name}",ucfirst($val->name),$this->input->post('template_text'));
		       $message=str_replace("{#email}",$val->email,$message);
			   $message=str_replace("{#address}",$val->address,$message);
			   $message=str_replace("{#city}",$val->city,$message);
			   $message=str_replace("{#state}",$val->state,$message);
			   $message=str_replace("{#country}",$val->country,$message);
			   $message=str_replace("{#zip}",$val->zip,$message);
			   $message=str_replace("{#phone}",$val->phone,$message);
			   $mailArray['message'] = $message.$Mailfooter;
			   $attatchFiles = array();
			   foreach($newsletter_session['attatchment'] as $val)
			   $attatchFiles[] = $val['name'];
			   
			   $mailArray['attatchFiles'] = $attatchFiles;
			   $this->sendMail($mailArray);
			   $data['error'] = false;
		   } else { 
			   $data['error'] = validation_errors();
			   
		   }
	   }
	   $data['success'] = true;
	   echo json_encode($data); die;
	}
	public function sendTestMailOut()
	{
	  if($_POST)
	   {
	        $this->form_validation->set_rules('testemail','Test Email','trim|required|valid_email');
		if($this->form_validation->run())
		  {
			   $newsletter_id = $this->input->post('newsletter_id');
		       $newsletter_detail = $this->newsletter_model->getNewsletterById($this->member_id,$newsletter_id);
               $mailArray = array();
			   $mailArray['from_name'] = $newsletter_detail->from_name;
			   $mailArray['from_email'] = $newsletter_detail->from_email;
			   $mailArray['reply_to'] = $newsletter_detail->reply_to;
			   $mailArray['message'] = $newsletter_detail->template_text;
			   $mailArray['contact_email'] = $this->input->post('testemail');
			   $mailArray['attatchmentPath'] = 'email_attatchment/'.$newsletter_id;
			   $mailArray['member_id'] = $this->member_id;
			   
			   $mail_type = $this->common_model->getSingleFieldFromAnyTable('mail_type','member_id',$this->member_id,'tbl_website_settings');
			   if($mail_type!='default' && $mail_type!='smtp')
				 {
				   $api_value = $this->common_model->getSingleFieldFromAnyTable('api_value','api_name',$mail_type,'tbl_external_mail_api');
				   $api_data = unserialize($api_value);
				   $mailArray['from_email'] = $from_email = $api_data['sending_email'];
				 }
				 
			   $Mailfooter = '<br/><br/><br/><br/>'.$this->footerText($this->member_id);
			   $Mailfooter = str_replace("{#to_email}",$mailArray['contact_email'],$Mailfooter);
			   $Mailfooter = str_replace("{#from_email}",$mailArray['from_email'],$Mailfooter);
			   $Mailfooter = str_replace("{#unsubscribe_link}",'#',$Mailfooter);

			   $subject=str_replace("{#name}",ucfirst($val->name),$newsletter_detail->subject);
		       $subject=str_replace("{#email}",$val->email,$subject);
			   $subject=str_replace("{#address}",$val->address,$subject);
			   $subject=str_replace("{#city}",$val->city,$subject);
			   $subject=str_replace("{#state}",$val->state,$subject);
			   $subject=str_replace("{#country}",$val->country,$subject);
			   $subject=str_replace("{#zip}",$val->zip,$subject);
			   $subject=str_replace("{#phone}",$val->phone,$subject);
			   $mailArray['subject'] = $subject;
			   $message=str_replace("{#name}",ucfirst($val->name),$newsletter_detail->template_text);
		       $message=str_replace("{#email}",$val->email,$message);
			   $message=str_replace("{#address}",$val->address,$message);
			   $message=str_replace("{#city}",$val->city,$message);
			   $message=str_replace("{#state}",$val->state,$message);
			   $message=str_replace("{#country}",$val->country,$message);
			   $message=str_replace("{#zip}",$val->zip,$message);
			   $message=str_replace("{#phone}",$val->phone,$message);
			   $mailArray['message'] = $message.$Mailfooter;
			   $attatchment = $this->newsletter_model->getAttatchFile($newsletter_id);
			   // print_r($attatchment); die;
			   $attatchFiles = array();
			   foreach($attatchment as $val)
			   $attatchFiles[] = $val->file_name;
			   $mailArray['attatchFiles'] = $attatchFiles;
			   
			   $this->sendMail($mailArray);
			   $data['error'] = false;
			 } else { 
			   $data['error'] = validation_errors();
		   }
	   }
	   $data['success'] = true;
	   echo json_encode($data); die;
	}
	public function getFinalEmailCount()
	{
	  $in_campaign_id = $this->input->post('in_campaign');
	  $ex_campaign_id = $this->input->post('ex_campaign');
	  $sep_list_id = $this->input->post('supression');
			 
	  $in_contact_list = $this->contact_model->getContactByCampaignId($in_campaign_id);
	  if($ex_campaign_id)
	   {
	     $ex_contact_list = $this->contact_model->getContactByCampaignIdEx($ex_campaign_id);
	     $ex_contact_list = array_column($ex_contact_list, 'email');
	   }
	  
	  if($sep_list_id)
	   {
	     $sep_contact_list = $this->suppression_model->getSuppressionByListIdForNewsletter($sep_list_id);
	     $sep_contact_list = array_column($sep_contact_list, 'email');
	   }
	   $unsubscribeList = $this->unsubscribe_model->getUnsubscriberList($this->member_id);
	   $unsubscribeList = array_column($unsubscribeList, 'email');
	   
	  $contact_id = array();
	  
	  foreach($in_contact_list as $key=>$in_val)
	   {
		 $email = $in_val->email;
		 $match = 'no';
		 
		   if(in_array($email,$ex_contact_list))
			 { $match = 'yes'; continue; }

		  if($match=='no')
		  {
				if(in_array($email,$sep_contact_list))
				 { $match = 'yes'; continue; }
		  }
		  if($match=='no')
		  {
			 if(in_array($email,$unsubscribeList))
			  { $match = 'yes'; }
				
		  }
		 if($match=='no')
		   {
			 $contact_id[] = $in_val->id;
		   }
	   }

	    $data['success'] = true;
		$data['contact_count'] = sizeof($contact_id);
	   echo json_encode($data); die;
	}
	public function deleteNewsletter($id)
	{
	  $status = $this->common_model->getSingleFieldFromAnyTable('status','id',$id,'tbl_newsletter');
	  $this->newsletter_model->deleteNewsletter($id);
	  $this->session->set_userdata('success_msg','Newsletter Deleted Successfully');
	 // redirect('newsletter-list/'.$status);
	  redirect($_SERVER['HTTP_REFERER']);
	}
	public function deleteMultiNewsletter()
	{ 
	  if($_POST)
	   {
		 $id = $this->input->post('id');
		 $this->newsletter_model->removeMultipleNewsletter($id);
		 $this->session->set_userdata('success_msg','Newsletter Deleted Successfully');
		 $data['success'] = true;
		}
	  $this->output
		   ->set_content_type('application/json')
		   ->set_output(json_encode($data));
	   }
	public function success()
	{
		$this->load->view($this->config->item('templateName').'/header',$output);
		$this->load->view($this->config->item('templateName').'/message/newsletter/newsletter_successfully');
		$this->load->view($this->config->item('templateName').'/footer');
	}
	public function getFinalContactIds($newsletter_session)
	{
	  $in_campaign_id = $newsletter_session['in_campaign_id'];
	  $ex_campaign_id = $newsletter_session['ex_campaign_id'];
	  $sep_list_id = $newsletter_session['sep_list_id'];
	  
	  $in_contact_list = $this->contact_model->getContactByCampaignId($in_campaign_id);
	  if($ex_campaign_id)
	   {
	     $ex_contact_list = $this->contact_model->getContactByCampaignIdEx($ex_campaign_id);
	     $ex_contact_list = array_column($ex_contact_list, 'email');
	   }
	  
	  if($sep_list_id)
	   {
	     $sep_contact_list = $this->suppression_model->getSuppressionByListIdForNewsletter($sep_list_id);
	     $sep_contact_list = array_column($sep_contact_list, 'email');
	   }
	   $unsubscribeList = $this->unsubscribe_model->getUnsubscriberList($this->member_id);
	   $unsubscribeList = array_column($unsubscribeList, 'email');

	  $contact_id = '';

	  foreach($in_contact_list as $key=>$in_val)
	   { 
		 $email = $in_val->email;
		 $match = 'no';
		 
		 if(in_array($email,$final_email))
		 { $match = 'yes'; continue; }
		 
		 if(in_array($email,$ex_contact_list))
		   { $match = 'yes'; continue; }

		  if($match=='no')
		  {
			if(in_array($email,$sep_contact_list))
			 { $match = 'yes'; continue; }
		  }
		  if($match=='no')
		  {
			 if(in_array($email,$unsubscribeList))
			  { $match = 'yes'; }
		  }
		 if($match=='no')
		   {
			 $contact_id.= $in_val->id.',';
		   }
	   } 
		return $contact_id;
	}
	public function chooseEditor()
	{
		
		if(!empty($_GET))
		 {
		   $newsletter_session = $this->session->userdata('newsletter_session');
		   $newsletter_session['editor_type'] = $_GET['editor_type'];
		   $this->session->set_userdata('newsletter_session',$newsletter_session);
		   redirect('add-newsletter');
		 }
		$this->load->view($this->config->item('templateName').'/header',$output);
		$this->load->view($this->config->item('templateName').'/message/newsletter/choose_editor');
		$this->load->view($this->config->item('templateName').'/footer');
	}
	public function validateScheduleTime($schedule_time)
	{

	   if(strtotime($schedule_time)>time())
		 { return true; }
	   else 
		 { 
		   $this->form_validation->set_message('validateScheduleTime','Schedule time must greater than current time.');
		   return FALSE;
		 }
	}
	public function sendMail($mailArray)
	{
	  $mailArray['mail_type'] = $mail_type = $this->common_model->getSingleFieldFromAnyTable('mail_type','member_id',$mailArray['member_id'],'tbl_website_settings');
	  if($mailArray['mail_type']!='default' && $mailArray['mail_type']!='smtp')
		 {
		   $mailArray['api_value'] = $this->common_model->getSingleFieldFromAnyTable('api_value','api_name',$mail_type,'tbl_external_mail_api');
		   $mailArray['to'] = $mailArray['contact_email'];
		 }
		 
	  if($mailArray['mail_type']=='default' || $mailArray['mail_type']=='smtp')
	   {
	     $this->mailsending_model->sendTestMail($mailArray);
	   } 
	  else if($mailArray['mail_type']=='amazon_ses')
	   {
	     require_once 'amazon_ses/autoload.php';
	     $this->mailsending_model->sendMailUsingAmazon($mailArray);
	   }
	  else if($mailArray['mail_type']=='sparkpost')
	   {
	     $httpClient = new GuzzleAdapter(new Client());
		 $api_value = unserialize($mailArray['api_value']);
		 $api_key = $api_value['api_key'];
		 $sparky = new SparkPost($httpClient, ['key'=>$api_key]);
		 $response = $this->mailsending_model->sendMailUsingSparkpost($mailArray,$sparky);
	   }
	  else if($mailArray['mail_type']=='postmark')
	   {
		 $httpClient = new GuzzleAdapter(new Client());
		 $api_value = unserialize($mailArray['api_value']);
		 $api_key = $api_value['api_key'];
		 $client = new PostmarkClient($api_key);
		 $response = $this->mailsending_model->sendMailUsingPostmark($mailArray,$client);
	   }
	  else if($mailArray['mail_type']=='mailgun')
	   {
		 $api_value = unserialize($mailArray['api_value']);
		 $api_key = $api_value['api_key'];
		 $sending_domain = $api_value['sending_domain'];
		 $sending_email = $api_value['sending_email'];
		 $mgClient = new Mailgun($api_key);
		 $response = $this->mailsending_model->sendMailUsingMailgun($mailArray,$mgClient,$sending_domain,$sending_email);
	   }
	  else if($mailArray['mail_type']=='elasticmail')
	   {
		 $api_value = unserialize($mailArray['api_value']);
		 $api_key = $api_value['api_key'];
		 try{
		     $elasticEmail = new \ElasticEmail\ElasticEmailV2($api_key);
			 $response = $this->mailsending_model->sendMailUsingElastic($mailArray,$elasticEmail);
		 }
		 catch(Exception $e){
							$response = 'error';	
						}
		 
	   }
	   else if($mailArray['mail_type']=='sendgrid'){
	 	 $api_value = unserialize($mailArray['api_value']);
		 $api_key = $api_value['api_key'];
		 $sending_mail = $api_value['sending_mail'];
	  	 try{
				$response = $this->mailsending_model->sendMailUsingSendgrid($mailArray,$api_key,$sending_mail);
			}
			catch(Exception $e){
				$response = 'error';	
			}
	  
	  }
	}
	public function footerText($member_id)
	{
		$social_links = $this->newsletter_model->getSocialLinks($member_id);
		$useraddress = $this->newsletter_model->getUserAddress($member_id);
		 
		$fb_url = $social_links->fb_url;
		$tw_url = $social_links->tw_url;
		$insta_url = $social_links->insta_url;
		$gplus_url = $social_links->gplus_url;
		
		if($fb_url)
		 {
		   $social_liks.=  '<a href="'.$fb_url.'"><img src="'.site_url().'assets/default/images/email_footer/images/fb.png" alt=""/></a>';
		 }
		if($tw_url)
		 {
		   $social_liks.=  '<a href="'.$tw_url.'"><img src="'.site_url().'assets/default/images/email_footer/images/tw.png" alt=""/></a>';
		 }
		if($insta_url)
		 {
		   $social_liks.=  '<a href="'.$insta_url.'"><img src="'.site_url().'assets/default/images/email_footer/images/gplus.png" alt=""/></a>';
		 }
		if($gplus_url)
		 {
		   $social_liks.=  '<a href="'.$gplus_url.'"><img src="'.site_url().'assets/default/images/email_footer/images/in.png" alt=""/></a>';
		 }
		 if($social_liks)
		  {
		     $social_box = '<p style="margin:0; padding:0; line-height:20px;"><span style="position: relative;margin-top:-1px; padding-right:10px;">Follow us </span>'.$social_liks.'</p>';
		  }
	 	
		
		$footer_text = '<div style="width:600px; background-color:#f5f5f5;color:#4a4949; padding:15px 15px 10px 15px; margin:0 auto; height:auto; font-family: "Roboto", sans-serif; font-weight:400; font-size:13px;">
			<div style="float:left; width:70%;">
			<p style="margin:0; margin-top:-1px; padding-right:10px; line-height:20px;">This email was sent to {#to_email} by {#from_email}</p>
			'.$social_box.'
			<p style="margin:0; padding-bottom:8px; line-height:20px;">'.$useraddress->address.', '.$useraddress->city.', '.$useraddress->pin_code.', '.$useraddress->state.', '.$useraddress->country.'</p>
			<p style="background:url('.site_url().'assets/default/images/email_footer/images/line.jpg) repeat-x top; width:auto; display:inline-block; font-size:11px; margin:0; padding:5px 0px 0px 0px;"> Getting too many emails from us, click here to <a href="{#unsubscribe_link}" style="color:#1155cc; ">Unsubscribe Now.</a></p>
			</div>

			<div style="float:left; width:30%; text-align:right">
			<p style="margin:0; padding:0px 0px 4px 0px; text-align:center; width:95px; display:inline-block;">Powered by</p>
			<a href="https://www.mailzingo.com/"><img src="'.site_url().'assets/default/images/email_footer/images/email-logo.png" alt=""/></a></div>
			<div style="clear:both"></div>
			</div>';
		return $footer_text; 
	}
	
}
