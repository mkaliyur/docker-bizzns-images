<?php
defined('BASEPATH') OR exit('No direct script access allowed');
include('app.php');

class Unsubscribe extends App {

	function __construct()
	{ 
	    parent::__construct();				
		$this->load->model('unsubscribe_model');
		$this->load->model('statics_model');
		$this->member_id = $this->session->userdata('mem_id');
		
	}
	
	public function index($un_data)
	{
		$stats_id = base64_decode(base64_decode($un_data));
		$detail = $this->statics_model->getStatsdetail($stats_id);
		$member_id = $detail->member_id;
		$contact_id = $detail->contact_id;
		$contact_email = $detail->email;

		$count = $this->unsubscribe_model->checkUnsubscriber($member_id,$contact_id,$contact_email);
		if($count==0)
		 {
		   $this->unsubscribe_model->addUnsubscribe($member_id,$contact_id,$contact_email);
		 }
		 $this->statics_model->setUnsubscribe($stats_id);
		 $this->session->set_userdata('success_msg','Unsubscribed Successfully');
		 redirect('info');
	
	}
	
}