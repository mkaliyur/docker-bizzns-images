<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
include('app.php');

class Welcome extends App {

	
    function __construct() {
        parent::__construct();
        $this->load->model('settings_model');
    }
	 
	public function index()
	{
		redirect('login');
	}
	public function info()
	{
		$this->load->view($this->config->item('templateName').'/info');
		$this->load->view($this->config->item('templateName').'/footer');
	}
	public function editor()
	{
		$this->load->view($this->config->item('templateName').'/editor');
	}
	public function fromEmailVerify($verification_code) {
	
	  $this->settings_model->verifyEmail($verification_code);
	  $this->session->set_userdata('success_msg', 'Email Verified Successfully.');
      redirect('info');

	}
	public function php_info() {
	
	  echo phpinfo(); die;

	}
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */