<?php
defined('BASEPATH') OR exit('No direct script access allowed');
include('app.php');

class Suppression_list extends App {

	function __construct()
	{  
	    parent::__construct();				
		$this->load->model('suppression_list_model');
		$this->load->model('suppression_model');
		$this->checkLogin();
		$this->member_id = $this->session->userdata('mem_id');
		
	}
	function checkLogin()
	{
	  if($this->session->userdata('mem_id')=='')
	  { redirect('login'); }
	  
	  if($this->supprssion_management!='yes')
	  {
	    $this->session->set_userdata('error_msg','Please upgrade to use suppression feature. <a target="_blank" href="'.$this->upgrade_link.'" class="upgradelink">Click here to upgrade </a>');
	    redirect('contact-list');
	  }
	  
	}
	public function index()
	{
		$output['meta_title'] = "Supperssion List";
		$output['allRecord'] = $this->suppression_list_model->getSuppressionList($this->member_id,null);
		
		$this->load->view($this->config->item('templateName').'/header',$output);
		$this->load->view($this->config->item('templateName').'/suppression_list/list');
		$this->load->view($this->config->item('templateName').'/footer');
	}
	public function addList()
	{
	  if($_POST)
	   {
			 if($this->input->post('list_id')=='')
			 $this->form_validation->set_rules('list_name','List Name','trim|required');
			 $this->form_validation->set_rules('emails','Email','trim|required|callback_validateEmails');
			 $output['emails'] = $this->input->post('emails');
			 
			 if($this->form_validation->run())
			  {
				 $emails = explode(',',$this->input->post('emails'));
				 if($this->input->post('list_id')=='')
				 $list_id = $this->suppression_list_model->addList($this->member_id);
				 else 
				 $list_id = $this->input->post('list_id');
				 foreach($emails as $val)
				 $this->suppression_model->addSuppression($this->member_id,$list_id,$val);
				 $data['success'] = true;
			  }
			if(validation_errors())
			 {
			   $data['success'] = false;
			   $data['error'] = validation_errors();
			 } else {
			   $data['success'] = true;
			   $data['success_msg'] = 'Suppression List Added Successfully';
			   if($this->input->post('list_id')=='')
			    $this->session->set_userdata('success_msg','Suppression List Added Successfully');
			   else
			    $this->session->set_userdata('success_msg','Email Added Successfully');
			   $data['model'] = 'hide';
			 }
			echo json_encode($data); die;
	   }
	}
	public function importSuppression()
	{
	  
	  if($_POST)
	   {
			 $this->load->library('csvimport');
			 if($this->input->post('list_id')=='')
			 $this->form_validation->set_rules('list_name','List Name','trim|required');
			 else
			 $this->form_validation->set_rules('list_id','List Name','trim|required');
			 if($_FILES['csv_file']['name']=='')
			 $this->form_validation->set_rules('csv_file','File','trim|required');
			 
			 if($this->form_validation->run())
			  {
				
				$config['upload_path'] = './assets/uploads/csv';
			    $config['allowed_types'] = 'csv';
				$this->load->library('upload', $config);
			   // If upload failed, display error
				if (!$this->upload->do_upload('csv_file')) {
					$output['error'] = $error = $this->upload->display_errors();
				} else {
					$file_data = $this->upload->data();
					$file_path =  './assets/uploads/csv/'.$file_data['file_name'];
					
					if ($this->csvimport->get_array($file_path)) {
						$csv_array = $this->csvimport->get_array($file_path);
						
						$final = array();
						array_walk_recursive($csv_array, function($item, $key) use (&$final){
						  $final[$key] = isset($final[$key]) ?  $item .', '. $final[$key] : $item;
						});
					}

				 foreach($final as $val1)
				 {
				   $emailIds = $val1;
				   break; 
				 }
				 $emails = explode(',',$emailIds);
				 if($this->input->post('list_id')=='')
				 $list_id = $this->suppression_list_model->addList($this->member_id);
				 else 
				 $list_id = $this->input->post('list_id');
				 foreach($emails as $val)
				 {
				   if(filter_var(trim($val), FILTER_VALIDATE_EMAIL)) 
				   $this->suppression_model->addSuppression($this->member_id,$list_id,$val);
				 }
				 $data['success'] = true;
			  }
			 }
			if(validation_errors() || $error)
			 {
			   $data['success'] = false;
			   $data['error'] = validation_errors().$error;
			 } else {
			   $data['success'] = true;
			   $data['success_msg'] = 'Suppression List Added Successfully';
			   if($this->input->post('list_id')=='')
			    $this->session->set_userdata('success_msg','Suppression List Added Successfully');
			   else
			    $this->session->set_userdata('success_msg','Email Added Successfully');
			   $data['model'] = 'hide';
			 }
			echo json_encode($data); die;
	   }
    }
	function validateEmails($str)
	{
	   $emailArray = explode(',',$str);
	   foreach($emailArray as $val)
	   {
		   if (!filter_var($val, FILTER_VALIDATE_EMAIL) === true) {
		    $this->form_validation->set_message('validateEmails','<b>'.$val.'</b> is not a valid email address');
			   return FALSE;
			}
       }
	   return true;
	}
	public function editList($id)
	{
		$output['total_data']= $this->suppression_model->getSuppressionList($id,$start,$content_per_page,$index=1);
                $output['id']=$id;
		$listDetail = $this->suppression_list_model->getListById($id);
		$output['supDetail'] = $supDetail;
		$output['listDetail'] = $listDetail;
		$this->load->view($this->config->item('templateName').'/header',$output);
		$this->load->view($this->config->item('templateName').'/suppression_list/edit');
		$this->load->view($this->config->item('templateName').'/footer');
	}
        
        public function load_more()
        { 
        $group_no = $this->input->post('group_no');
        $id = $this->input->post('id');
        $content_per_page = 8;
        $start = ceil($group_no * $content_per_page);
        $output['allRecord'] = $this->suppression_model->getSuppressionList($id,$start,$content_per_page,$index);

        echo $this->load->view($this->config->item('templateName').'/suppression_list/suppression-pagination',$output,true);
        
    }
        
	public function deleteSuppression($id)
	{
	  $list_id = $this->common_model->getSingleFieldFromAnyTable('list_id','id',$id,'tbl_suppression');
	  $this->suppression_model->deleteSuppression($id);
	  $this->session->set_userdata('success_msg','Record Deleted Successfully');
	  redirect('edit-suppression-list/'.$list_id);
	}
	public function deleteList($id)
	{
	  $this->suppression_list_model->deleteList($id);
	  $this->session->set_userdata('success_msg','List Deleted Successfully');
	  redirect('suppression-list');
	}
}