<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Install extends CI_Controller {
	public function __construct() {
        parent::__construct();
	}
    
	/* Step 1 for checkinng php and mysql requirements*/ 
	
	function step1()
	{ 
		$this->UpdateSiteUrl(); 
		sleep(5);	
		$this->load->config('config');
	 
		$data = '';
		$data['step'] = 'step1';
		if($this->session->userdata('step1'))
		$this->session->unset_userdata("step1");
		if($this->session->userdata('step2'))
		$this->session->unset_userdata("step2");
		if($this->session->userdata('step3'))
		$this->session->unset_userdata("step3");
		if(isset($_POST['step1']))
		{
			$this->form_validation->set_rules('mysql_check', 'Mysql', 'trim|callback_mysql_check|xss_clean');
			$this->form_validation->set_rules('php_check', 'Php', 'trim|callback_php_check|xss_clean');
			$this->form_validation->set_rules('check_database', 'Database', 'trim|callback_database_check|xss_clean');
			$this->form_validation->set_rules('check_config', 'Config', 'trim|callback_config_check|xss_clean');
			$this->form_validation->set_rules('check_autoload', 'Autoload', 'trim|callback_autoload_check|xss_clean');
			$this->form_validation->set_rules('curl_check', 'Curl', 'trim|callback_curl_check|xss_clean');
			$this->form_validation->set_rules('maxupload_check', 'Maximum', 'trim|callback_maxupload_check|xss_clean');
			$this->form_validation->set_rules('maxpost_check', 'Maximum Post Size', 'trim|callback_maxpost_check|xss_clean');
			$this->form_validation->set_rules('get_content', 'File get content', 'trim|callback_get_content_check|xss_clean');
			$this->form_validation->set_rules('get_exe_time', 'File get content', 'trim|callback_get_exe_check|xss_clean');
			
			
			if ($this->form_validation->run()) {
					$this->session->set_userdata("step1",'complete');
					redirect(site_url().'install/step2');
				}
			}
		if(site_url()!='%BASEURL%/')
		$data['assets']=site_url();
		else
		$data['assets']='./';
		$this->load->view('install/step1',$data);
	}
	function updateSiteUrl()
	{
		$pageURL= (@$_SERVER["HTTPS"] == "on") ? "https://" : "https://";
		$root = $pageURL.$_SERVER['HTTP_HOST'];
		$root .= str_replace(basename($_SERVER['SCRIPT_NAME']),"",$_SERVER['SCRIPT_NAME']);
		if(site_url()!=$root)
		{
			
			$this->config->set_item('base_url',$root);
			$template_path 	= './installation/config.php';
			$output_path 	= './application/config/config.php';
			$config_file = file_get_contents($template_path);
			//$new  = $autoload_file;
			$new  = str_replace("%BASEURL%",$root,$config_file);
			$handle = fopen($output_path,'w+');
			@chmod($output_path,0777);
			if(is_writable($output_path)) {

				
				if(fwrite($handle,$new)) {
					return true;
				} else {
					return false;
				}

			} else {
				return false;
			}
		}			
		$this->databaseConnection();
	}
	/* check mysql connection*/
	function mysql_check()
	{
		$version = explode('.', PHP_VERSION);
		if($version[0] == 5){
		if (function_exists('mysql_connect')) {
			return TRUE;
		  } else {
			$this->form_validation->set_message('mysql_check', 'The %s rquirement not fullfill');
			return FALSE;
		  }
		}
		if($version[0] == 7){
		if (function_exists('mysqli_connect')) {
			return TRUE;
		  } else {
			$this->form_validation->set_message('mysql_check', 'The %s rquirement not fullfill');
			return FALSE;
		  }
		}
			
	}
	function php_check()
	{
		$version = explode('.', PHP_VERSION);
		if($version[0] == 5 && $version[1] > 4){ 
			return TRUE;
		  } 
		else if($version[0] == 7)
		{
			return TRUE;
		}			
		else {
			$this->form_validation->set_message('php_check', 'PHP Version must be 5.4 or greater');
			return FALSE;
		  }
	}
	function database_check()
	{
		if(is_writable('./application/config/database.php'))
		{
			return TRUE;
		  } else {
			$this->form_validation->set_message('database_check', 'Database File Not Writtable');
			return FALSE;
		  }
	}
	function config_check()
	{
		if(is_writable('./application/config/config.php'))
		{
			return TRUE;
		  } else {
			$this->form_validation->set_message('config_check', 'Config File Not Writtable');
			return FALSE;
		  }
	}
	function autoload_check()
	{
		if(is_writable('./application/config/autoload.php'))
		{
			return TRUE;
		  } else {
			$this->form_validation->set_message('autoload_check', 'Autoload File Not Writtable');
			return FALSE;
		  }
	}
	function curl_check()
	{
		if(in_array('curl', get_loaded_extensions()))
		{
			return TRUE;
		  } else {
			$this->form_validation->set_message('curl_check', 'Curl requirements not full fill');
			return FALSE;
		  }
	}
	function maxupload_check()
	{
		if((int)(ini_get('upload_max_filesize')) >= 2)
		{
			return TRUE;
		  } else {
			$this->form_validation->set_message('maxupload_check', 'Max upload size not full fill');
			return FALSE;
		  }
	}
	function maxpost_check()
	{
		if((int)(ini_get('post_max_size')) >= 2)
		{
			return TRUE;
		  } else {
			$this->form_validation->set_message('maxpost_check', 'Max upload size not full fill');
			return FALSE;
		  }
	}
	function get_content_check()
	{
		if((ini_get('allow_url_fopen')))
		{
			return TRUE;
		  } else {
			$this->form_validation->set_message('get_content_check', 'Please Enable file_get_content');
			return FALSE;
		  }
	  
		
	}
	function get_exe_check()
	{
		ini_set('max_execution_time', 30); $maxPost = (int)(ini_get('max_execution_time'));
		if ($maxPost >= 30) {
			return TRUE;
		  } else {
			$this->form_validation->set_message('max_execution_time_check', 'The %s rquirement not fullfill');
			return FALSE;
		  }
	}
	/* Database connection check and then create database tables*/
	function step2()
	{
		$this->load->config("config");
		if(site_url()!='%BASEURL%/')
			$data['assets']=site_url();
		else
			$data['assets']='./';
		 if($this->session->userdata("step1")=="complete")
		 {	 
			if($this->session->userdata("step2")=="complete")
			{
				 redirect(site_url().'install/step3');	
			}
			else
			{
			 
					$data['error'] = '';
					$data['step']='step2';
					$data['value']=20;
					if(isset($this->db)){
						$data['hostname'] = $this->db->hostname;
						$data['database'] = $this->db->database;
						$data['username'] = $this->db->username;
						$data['password'] = $this->db->password;
					}
					else
					{
						$data['hostname'] = '';
						$data['database'] = '';
						$data['username'] = '';
						$data['password'] = '';
					}
					if(isset($_POST['step2']))
					{
						$this->form_validation->set_rules('hostname', 'Hostname', 'trim|required|xss_clean');
						$this->form_validation->set_rules('database', 'Database name', 'trim|required|xss_clean');
						$this->form_validation->set_rules('username', 'Username', 'trim|required|xss_clean');
						$this->form_validation->set_rules('password', 'Password', 'trim|xss_clean');
						$this->form_validation->set_rules('site_url', 'Site Url', 'trim|required|xss_clean');
						$data['hostname'] = $this->input->post('hostname');
						$data['database'] = $this->input->post('database');
						$data['username'] = $this->input->post('username');
						$data['password'] = $this->input->post('password');
						if ($this->form_validation->run()) {
							 
							$val=$this->check_connection();
							if($val==1)
							{
								$d = $this->write_database();
								$d1 = $this->create_tables();
								$data['error'] = $d1;
								if($data['error']=='')
								{

									$this->write_autoload(); 
									$this->session->set_userdata("step2",'complete');
									redirect(site_url().'install/step3');
								}	
							}
							else
							{
								
								$data['error'] = str_replace("'",'',$val);
								
							} 
						}
					}
					$this->load->view('install/step2',$data);
			}		 
		 }
			else
			{
				$this->session->unset_userdata("step1");
				redirect(site_url().'install/step1');
			}
		
	}
	
	/* Check Password combination */
	public function check_password1()
		{
			  $pass=$this->input->post('adminpass');
			  
			  $this->form_validation->set_message('adminpass', 'Password not a valid combination.'); 
			   if (preg_match('/^.{6,}$/', $pass))
					{
						 return true;
					}
					else
					{
						$this->form_validation->set_message('check_password1', 'Minimum 6 characters');
						return false;	
					}
		}
	function check_password2()
	{
			
		$pass=$this->input->post('adminpass');
		$pass1=$this->input->post('confirm_password');
		if($pass==$pass1)
		{
			return true;
		}
		else
		{
			$this->form_validation->set_message('check_password2', 'Password and confirm password not matched');
			return false;
		}
	}
	/* step 3 for Administrator login details */
	function step3()
	{
		if(site_url()!='%BASEURL%/')
		$data['assets']=site_url();
		else
		$data['assets']='./';
		if($this->session->userdata("step1") && $this->session->userdata("step2"))
		{
			$data['adminname'] = '';
			$data['adminemail'] = '';
			$data['step']='step3';
			if($this->db){
			$this->db->select("name,email");
			$get =$this->db->get('tbl_member');
			if($get->num_rows()>0)
			{
				$result = $get->row();	
				$data['adminname'] = $result->name;
				$data['adminemail'] = $result->email;
			}}
			else
			{
				$data['adminname'] = '';
				$data['adminemail'] = '';
			}
			if(isset($_POST['step3']))
			{
				$this->form_validation->set_rules('adminname', 'Administrator name', 'trim|required|regex_match[/^[a-zA-Z ]*$/]|xss_clean');
				$this->form_validation->set_rules('adminemail', 'Email', 'trim|required|valid_email|xss_clean');
				$this->form_validation->set_rules('adminpass', 'Password', 'trim|required|callback_check_password1');
				$this->form_validation->set_rules('confirm_password', 'Password', 'trim|required|callback_check_password2');
				$this->form_validation->set_rules('company_name', 'Company Name', 'trim|required|xss_clean');
				if ($this->form_validation->run()) {
					$d=$this->app_settings();
					$mem_id = $this->website_settings();
					if($mem_id!=0){
						$this->formTemplates($mem_id);
						$this->Templates($mem_id);
					}
					$this->session->set_userdata("adminname",$this->input->post('adminname'));
					$this->session->set_userdata("adminemail",$this->input->post('adminemail'));
					$this->session->set_userdata("step3",'complete');
				 	redirect(site_url()."install/complete");
				}
			}
			
			$this->load->view("install/step3",$data);
		}
		else
		{
			redirect(site_url());
		}
	}
	/*Congratulation step*/
	/*Congratulation step*/
	function complete()
	{ 
	
		
		$pageURL= (@$_SERVER["HTTPS"] == "on") ? "https://" : "http://";
		$root = $pageURL.$_SERVER['HTTP_HOST'];
		$root .= str_replace(basename($_SERVER['SCRIPT_NAME']),"",$_SERVER['SCRIPT_NAME']);
		
		if(site_url()!='%BASEURL%/')
		$data['assets']=site_url();
		else
		$data['assets']='./';
		if($this->session->userdata("step1") && $this->session->userdata("step2") && $this->session->userdata("step3"))
		{
			
			
			$ch = curl_init();
			curl_setopt($ch, CURLOPT_URL, $this->config->item('membersarea_url').'sendmail/welcome?e='.$this->session->userdata("adminemail").'&n='.$this->session->userdata('adminname').'&url='.urlencode(site_url()));
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
			$output = curl_exec($ch);
			curl_close($ch);
			$this->session->unset_userdata('step1');
			$this->session->unset_userdata('step2');
			$this->session->unset_userdata('step3');
		 	$this->session->unset_userdata("adminemail");
			$this->configSessionUpdate();
			sleep(5);
			$data['step']='step4';
			$this->load->helper('url');
		 	$this->load->view("install/complete",$data);
		}
		else if(site_url()==$root)
		{
			$this->databaseConnection();
			$this->load->view("install/complete",$data);
		}
		else
		{
			$this->session->set_flashdata("in","Something Goes Wrong Please Try Again");
			redirect(site_url());
		}
		
	
	}
	
	function already()
	{
		$pageURL= (@$_SERVER["HTTPS"] == "on") ? "https://" : "http://";
		$root = $pageURL.$_SERVER['HTTP_HOST'];
		$root .= str_replace(basename($_SERVER['SCRIPT_NAME']),"",$_SERVER['SCRIPT_NAME']);
		
		//$this->databaseConnection();
		if(site_url()!='%BASEURL%/')
		$data['assets']=site_url();
		else
		$data['assets']='./';
		if($this->db){
		$this->db->select('*');
		$get = $this->db->get('tbl_member');
		if($get->num_rows()>0){
			$result = $get->row();
			$data['adminname'] = $result->name;
		}
		else{
			redirect($root.'install/');
		}}
		else{
			redirect($root.'install/');
		}		
		$this->load->view("install/already",$data);
	}
	
	/* Insert Into Database Tables*/
	function app_settings(){
			$adminname = $this->input->post('adminname');
		 	$adminpass = md5($this->input->post('adminpass'));//md5 Generate
			$adminemail = $this->input->post('adminemail');
			$sitetitle = $this->input->post('company_name'); //currently not insert into database
			$company=$_POST['company_name'];
			$arr['site_title']=$company;
			$ar=serialize($arr);
			$this->db->select('mem_id');
			$this->db->where('email',$adminemail);
			$res = $this->db->get('tbl_member');
			
			if($res->num_rows() >0)
			{
				$result = $res->row();
				$this->db->set('name',$adminname);
				$this->db->set('email',$adminemail);
				$this->db->set('password',$adminpass);
				$this->db->set('city','');
				$this->db->set('state','');
				$this->db->set('country','');
				$this->db->set('address','');
				$this->db->set('pin_code','');
				$this->db->set('business_name',$sitetitle);
				$this->db->set('add_time',time());
				$this->db->where('mem_id',$resut->id);
				$this->db->update('tbl_member');
			}
			else{
				
				$User_arr=array("name"=>$adminname,'email'=>$adminemail,'password'=>$adminpass,'city'=>'','state'=>'','country'=>'','address'=>'','pin_code'=>'','business_name'=>$sitetitle,'add_time'=>time());
				$this->db->insert("tbl_member",$User_arr);	//User Login Credentials
			}
			return ($this->db->affected_rows() != 1) ? 0 : 1;
			
	}
	/* Check Step1 Reuirements correct or not*/
	function sometext_check($str) {
		if ($str == 1) {
		return TRUE;
	  } else {
	  	$this->form_validation->set_message('sometext_check', 'The %s rquirement not fullfill');
		return FALSE;
	  }
	}
	/* Check Database Connection*/
	function check_connection(){
 	 	$database=$this->input->post('database'); 
	 	$hostname=$this->input->post('hostname');
 		$username=$this->input->post('username');
	 	$password=$this->input->post('password');
		$mysqli = new mysqli($hostname,$username,$password,$database);
		if(mysqli_connect_error())
		{
			return mysqli_connect_error();
		}
		else
		{
			$mysqli->query("CREATE DATABASE IF NOT EXISTS ".$database);
			return 1;
		}
	}
	
	 
	
	/* Change In Config File According Url*/
	function write_database() {
		$database=$this->input->post('database'); 
	 	$hostname=$this->input->post('hostname');
 		$username=$this->input->post('username');
	 	$password=$this->input->post('password'); 
	 	$template_path 	= './installation/database.php';
		$output_path 	= './application/config/database.php';
		$database_file = file_get_contents($template_path);
		$new  = str_replace("%HOSTNAME%",$hostname,$database_file);
		$new  = str_replace("%USERNAME%",$username,$new);
		$new  = str_replace("%PASSWORD%",$password,$new);
		$new  = str_replace("%DATABASE%",$database,$new);
		
		$version = explode('.', PHP_VERSION);
		if($version[0] == 5)
		$new  = str_replace("%MYSQL_TYPE%",'mysql',$new);
		else
		$new  = str_replace("%MYSQL_TYPE%",'mysqli',$new);	
		$handle = fopen($output_path,'w+');
		@chmod($output_path,0777);
		if(is_writable($output_path)) {
		if(fwrite($handle,$new)) {
				//return true;
			} else {
				//return false;
			}

		} else {
			$this->session->unset_userdata("step2");
			redirect(site_url().'install/step1');
		}
	}
	
	/* AFter Database Creation Autoload database*/
	function write_autoload() {
	 	$template_path 	= './installation/include/autoload.php';
		$output_path 	= './application/config/autoload.php';
		$autoload_file = file_get_contents($template_path);
		$new  = $autoload_file;

	
		$handle = fopen($output_path,'w+');

	
		@chmod($output_path,0777);

		
		if(is_writable($output_path)) {

			
			if(fwrite($handle,$new)) {
				return true;
			} else {
				return false;
			}

		} else {
			return false;
		}
	}
	/*After Installation Complete Create Route file*/
	
	 function website_settings(){
                        $member_details=$this->db->get('tbl_member')->row();
                        $member_id=$member_details->mem_id;
                        $this->session->set_userdata("member_id",$member_id);
                        $footer_text='<div style="width:600px;color:#4a4949; padding:15px 15px 10px 15px; margin:0 auto; height:auto; font-family: \'Roboto\', sans-serif; font-weight:400; font-size:13px;">
                        <div style="float:left; width:70%;">
                        <p style="margin:0; padding:0;"><span style="position: relative;margin-top:-1px; padding-right:10px;float: left;">Follow us </span>
                        <a href="{#fb_link}"><img src="'.site_url().'assets/default/images/email_footer/images/fb.png" /></a>
                        <a href="{#tw_link}"><img src="'.site_url().'assets/default/images/email_footer/images/tw.png" /></a>
                        <a href="{#gplus_link}"><img src="'.site_url().'assets/default/images/email_footer/images/gplus.png" /></a>
                        <a href="{#in_link}"><img src="'.site_url().'assets/default/images/email_footer/images/in.png" /></a></p>
                        <p style="margin:0; padding-bottom:8px; ">{#address}</p>
                        <p style="background:url(images/line.jpg) repeat-x top; width:auto; display:inline-block; font-size:11px; margin:0; padding:5px 0px 0px 0px;"> Getting too many emails from us, click here to <a href="{#unsubscribe_link}" style="color:#1155cc; ">Unsubscribe</a></p>
                        {#open_link}
                        </div>

                        <div style="float:left; width:30%; text-align:right">
                        <p style="margin:0; padding:0px 0px 4px 0px; text-align:center; width:95px; display:inline-block;">Powered by</p>
                        <img src="{#logo_link}" /></div>
                        <div style="clear:both"></div>
                        </div>';
                        $application_URL=str_replace(array('step1','step2','step3','complete'),'',site_url());
                        $database_name=$this->db->database;
                        $website_array_values=array('member_id'=>$member_id,'footer_text'=>$footer_text,'application_URL'=>$application_URL,'database_name'=>$database_name,'mail_hour_limit'=>60,'mail_minute_limit'=>3);
                        
//                        print_r($User_arr);die;
			$this->db->insert("tbl_website_settings",$website_array_values);	//User Login Credentials
//			echo $this->db->last_query();	die;

			return ($this->db->affected_rows() != 1) ? 0 : $member_id;
        }
	/* Create Database Tables*/
	
	function formTemplates($mem_id)
	{
		$this->db->select('id');
		$query = $this->db->get('tbl_form_template');
		if($query->num_rows()==0){
		$values = array(
					array(
						  'id' => 1 ,
						  'template_name' => 'Template 1' ,
						  'template_text' => '<div class="form1-bg" id="demo-preview">
<div class="row"> 
<div class="col-md-12 col-sm-12 col-xs-12">
<div class="col-md-12 col-sm-12 col-xs-12 form_area1">
<div class="mdem17 smem15 xsem12 blue text-center" contenteditable="true">TAKE THE BEST</div>
<div class="mdem17 smem15 xsem12 blue text-center" contenteditable="true"><b>ONLINE COURSES</b> AVAILABLE</div>
<div class="col-md-10 col-md-offset-1 col-sm-10 col-sm-offset-1 col-xs-10 col-xs-offset-1 mt7 xsmt5">
<div class="namefield">
<div class="mdem11 smem11 xsem10 blacktext w500" contenteditable="true">Name</div>
<div class="mdem11 smem11 xsem10"><input type="text"  name="name" class="fieldinput" placeholder="Name"/></div>
</div>
<div class="copydiv">
<div class="mdem11 smem11 xsem10 blacktext mt2 xsmt4 w500 fieldname" contenteditable="true">Email Address</div>
<div class="mdem12 smem11 xsem10"><input type="text" name="email" class="fieldinput" placeholder="Email" /></div>
</div>
<div class="mdem12 smem12 xsem11 mt15 xsmt10 w400">
<a href="#" class="form-btn1" contenteditable="true" id="rect">Sign Up Today</a></div>
</div>
</div>
</div>				
</div>
</div>',
						  'member_id' => $mem_id,
						  'add_time' =>1470394007,
						  'ip' => '',
						  'status' => 'active'
					   ),
					array(
						  'id' => 2,
						  'template_name' => 'Template 2' ,
						  'template_text' => '<div class="form2-bg" id="demo-preview">
<div class="row"> 
<div class="col-md-12 col-sm-12 col-xs-12">
<div class="col-md-12 col-sm-12 col-xs-12 form_area2">
<div class="mdem17 smem15 xsem13 heading w500 text-center robotoslab" contenteditable="true">EXPERIENCE ADVANTURE <br> OF A LIFE TIME</div>
<div class="mdem13 smem12 xsem12 preheading text-center w300 mt1 xsmt1" contenteditable="true">For discount, fill in the form</div>
<div class="col-md-10 col-md-offset-1 col-sm-10 col-sm-offset-1 col-xs-10 col-xs-offset-1 mt9 xsmt10">
<div class="namefield">
<div class="mdem10 smem10 xsem10 whitetext w300" contenteditable="true">Full Name</div>
<div class="mdem10 smem10 xsem10"><input type="text" name="name" class="fieldinput" placeholder="Name"></div>
</div>
<div class="copydiv">
<div class="mdem10 smem10 xsem10 whitetext mt2 xsmt4 w300 fieldname" contenteditable="true">Email Address</div>
<div class="mdem10 smem10 xsem10"><input type="text"  name="email" class="fieldinput" placeholder="Email"></div>
</div>
<div class="mdem11 smem11 xsem11 mt10 xsmt10 w300"><a href="#" class="form-btn2" contenteditable="true"  id="rect">Send My Offer</a></div>
</div>
</div>
</div>				
</div>
</div>',
						  'member_id' => $mem_id,
						  'add_time' =>1470394007,
						  'ip' => '',
						  'status' => 'active'
					   ),
					array(
						  'id' => 3,
						  'template_name' => 'Template 3' ,
						  'template_text' => '<div class="form3-bg" id="demo-preview">
<div class="row"> 
<div class="form_area3">
<div class="col-md-12 col-sm-12 col-xs-12 innerform">
<div class="mdem17 smem15 xsem13 whitetext text-center w600" contenteditable="true">Want To Get Smart Tips <br> From Our Experts</div>
<div class="mdem11 smem11 xsem11 whitetext text-center w300 mt2 xsmt2" contenteditable="true">For Details, Fill In The Form</div>
<div class="col-md-10 col-md-offset-1 col-sm-10 col-sm-offset-1 col-xs-10 col-xs-offset-1 mt9 xsmt10">
<div class="namefield">
<div class="mdem10 smem10 xsem10 whitetext w300" contenteditable="true">Full Name</div>
<div class="mdem10 smem10 xsem10"><input type="text"  name="name" class="fieldinput" placeholder="Name"></div>
</div>
<div class="copydiv">
<div class="mdem10 smem10 xsem10 whitetext mt2 xsmt4 w300 fieldname" contenteditable="true">Email Address</div>
<div class="mdem10 smem10 xsem10"><input type="text"  name="email" class="fieldinput" placeholder="Email"></div>
</div>
<div class="mdem11 smem10 xsem10 mt10 xsmt10 w300"><a href="#" class="form-btn3" contenteditable="true"  id="rect">Send Me Details</a></div>
</div>
</div>
</div>				
</div>
</div>',
						  'member_id' => $mem_id,
						  'add_time' =>1470394007,
						  'ip' => '',
						  'status' => 'active'
					   ),
					array(
						  'id' => 4,
						  'template_name' => 'Template 4' ,
						  'template_text' => '<div class="form4-bg"  id="demo-preview">
<div class="row"> 
<div class="col-md-12 col-sm-12 col-xs-12">
<div class="col-md-12 col-sm-12 col-xs-12 form_area4">
<div class="mdem18 smem15 xsem12 whitetext w700 text-center" contenteditable="true">CONCERTS</div>
<div class="mdem10 smem12 xsem11 whitetext text-center w500 mt3 xsmt3" contenteditable="true">DON\'T MISS ANY OF OUR AWESOME CONCERTS</div>
<div class="mdem9 smem12 xsem11 whitetext text-center w300 mt1 xsmt1" contenteditable="true">Limited amount of passes, reserve your ticket today!</div>
<div class="col-md-8 col-md-offset-2 col-sm-10 col-sm-offset-1 col-xs-10 col-xs-offset-1 mt7 xsmt10">
<div class="namefield">
<div class="mdem10 smem10 xsem10 whitetext w300" contenteditable="true">Full Name</div>
<div class="mdem10 smem10 xsem10"><input type="text" name="name" class="fieldinput" placeholder="Name"></div>
</div>
<div class="copydiv">
<div class="mdem10 smem10 xsem10 whitetext w300 fieldname" contenteditable="true">Email Address</div>
<div class="mdem10 smem10 xsem10"><input type="text" name="email" class="fieldinput" placeholder="Email"></div>
</div>
<div class="mdem11 smem11 xsem11 mt5 xsmt5 w600"><a href="#" class="form-btn4" contenteditable="true"  id="rect">Reserve Today</a></div>
</div>
</div>
</div>				
</div>
</div>',
						  'member_id' => $mem_id,
						  'add_time' =>1470394007,
						  'ip' => '',
						  'status' => 'active'
					   ),
					array(
						  'id' => 5,
						  'template_name' => 'Template 5' ,
						  'template_text' => '<div class="form5-bg" id="demo-preview">
<div class="row"> 
<div class="col-md-12 col-sm-12 col-xs-12">
<div class="col-md-12 col-sm-12 col-xs-12 form_area5">
<div class="mdem32 smem30 xsem22 greytext text-center breeserif" contenteditable="true">CAR EXHIBITION</div>
<div class="col-md-10 col-md-offset-1 col-sm-10 col-sm-offset-1 col-xs-10 col-xs-offset-1 mt25 xsmt25">
<div class="namefield">
<div class="mdem10 smem9 xsem9"><input type="text" placeholder="Full Name"  name="name" class="fieldinput"></div>
</div>
<div class="phonefield">
<div class="mdem10 smem9 xsem9"><input type="text"  placeholder="Phone Number"  name="phone" class="fieldinput"></div>
</div>
<div class="copydiv">
<div class="mdem10 smem9 xsem9"><input type="text" placeholder="Email Address"   name="email" class="fieldinput"></div>
</div>
<div class="mdem15 smem13 xsem13 mt10 xsmt10 w600"><a href="#" class="form-btn5" contenteditable="true"  id="rect">Join Now</a></div>
</div>
</div>	
</div>			
</div>
</div>',
						  'member_id' => $mem_id,
						  'add_time' =>1470394007,
						  'ip' => '',
						  'status' => 'active'
					   ),
					array(
						  'id' => 6,
						  'template_name' => 'Template 6' ,
						  'template_text' => '<div class="form6-bg"  id="demo-preview">
<div class="row"> 
<div class="col-md-12 col-sm-12 col-xs-12 form_area6">
<div class="col-md-8 col-md-offset-2 col-sm-8 col-sm-offset-2 col-xs-10 col-xs-offset-1 arrow_box">
<div class="mdem17 smem16 xsem15 whitetext w600 text-center letterspace mt13 xsmt5"  contenteditable="true">FREE COURSE</div>
<div class="mdem10 smem10 xsem9 whitetext text-center w300 mt3 xsmt2"  contenteditable="true">Inroduction to computer science <br> For join TODAY, Fill in the form</div>
</div>
<div class="col-md-10 col-md-offset-1 col-sm-10 col-sm-offset-1 col-xs-10 col-xs-offset-1">
<br><br>
<div class="namefield">
<div class="mdem10 smem10 xsem9 whitetext w300"  contenteditable="true">Full Name</div>
<div class="mdem10 smem10 xsem9"><input type="text"  name="name" class="fieldinput" placeholder="Name"></div>
</div>
<div class="copydiv">
<div class="mdem10 smem10 xsem9 whitetext mt2 xsmt0 w300 fieldname"  contenteditable="true">Email Address</div>
<div class="mdem10 smem10 xsem9"><input type="text"  name="email" class="fieldinput" placeholder="Email"></div>
</div>
<div class="mdem13 smem12 xsem11 mt10 xsmt10 w500"><a href="#" class="form-btn6" contenteditable="true"  id="rect">Join Now</a></div>
</div>
</div>				
</div>
</div>',
						  'member_id' => $mem_id,
						  'add_time' =>1470394007,
						  'ip' => '',
						  'status' => 'active'
					   ),
					array(
						  'id' => 7,
						  'template_name' => 'Template 7' ,
						  'template_text' => '<div class="form7-bg"  id="demo-preview">
<div class="row"> 
<div class="col-md-12 col-sm-12 col-xs-12">
<div class="col-md-12 col-sm-12 col-xs-12 form_area7">
<div class="mdem17 smem15 xsem15 redtext w700 text-center" contenteditable="true">LETS KEEP IN TOUCH !</div>
<div class="redarrow_box"><div class="mdem12 smem12 xsem11 whitetext text-center w300" contenteditable="true">Sign up below to get <br> regular updat from our company</div></div>
<div class="col-md-10 col-md-offset-1 col-sm-10 col-sm-offset-1 col-xs-10 col-xs-offset-1 mt5 xsmt5">
<div class="namefield">
<div class="mdem10 smem10 xsem10 black w500" contenteditable="true">Full Name</div>
<div class="mdem10 smem10 xsem10"><input type="text" name="name" class="fieldinput" placeholder="Name"></div>
</div>
<div class="copydiv">
<div class="mdem10 smem10 xsem10 black w500 fieldname" contenteditable="true">Email Address</div>
<div class="mdem10 smem10 xsem10"><input type="text" name="email" class="fieldinput" placeholder="Email"></div>
</div>
<div class="mdem13 smem11 xsem11 mt5 xsmt5 w600"><a href="#" class="form-btn7" contenteditable="true"  id="rect">Join Now</a></div>
</div>
</div>
</div>				
</div>
</div>',
						  'member_id' => $mem_id,
						  'add_time' =>1470394007,
						  'ip' => '',
						  'status' => 'active'
					   ),
					array(
						  'id' => 8,
						  'template_name' => 'Template 8' ,
						  'template_text' => '<div class="form8-bg" id="demo-preview">
<div class="row"> 
<div class="col-md-12 col-sm-12 col-xs-12">
<div class="col-md-12 col-sm-12 col-xs-12 form_area8 whitetext">
<div class="mdem12 smem11 xsem10 text-center" contenteditable="true">Want to recieve the best offers of the weak ? </div>
<div class="mdem22 smem15 xsem14 text-center w700 xsmb5" contenteditable="true">Join Today </div>
<div class="col-md-10 col-md-offset-1 col-sm-10 col-sm-offset-1 col-xs-10 col-xs-offset-1 mt10 xsmt10">
<div class="namefield">
<div class="mdem10 smem9 xsem9"><input type="text" placeholder="Full Name"  name="name" class="fieldinput"></div>
</div>
<div class="phonefield">
<div class="mdem10 smem9 xsem9"><input type="text"  placeholder="Phone Number"  name="phone" class="fieldinput"></div>
</div>
<div class="copydiv">
<div class="mdem10 smem9 xsem9"><input type="text" placeholder="Email Address"  name="email" class="fieldinput"></div>
</div>
<div class="mdem14 smem13 xsem13 mt10 xsmt10 w500"><a href="#" class="form-btn8" contenteditable="true"  id="rect">Join Now</a></div>
</div>
</div>				
</div>
</div>
</div>',
						  'member_id' => $mem_id,
						  'add_time' =>1470394007,
						  'ip' => '',
						  'status' => 'active'
					   ),
					array(
						  'id' => 9,
						  'template_name' => 'Template 9' ,
						  'template_text' => '<div class="form9-bg" id="demo-preview">
<div class="row"> 
<div class="col-md-12 col-sm-12 col-xs-12">
<div class="col-md-12 col-sm-12 col-xs-12 form_area9">
<div class="ribbon"><span  contenteditable="true">&nbsp;Hurry UP!&nbsp;</span></div>
<div class="mdem25 smem15 xsem12 text-center w600 mt16 xsmt15 whitetext" contenteditable="true">Start Your Fitness</div>
<div class="col-md-10 col-md-offset-1 col-sm-10 col-sm-offset-1 col-xs-10 col-xs-offset-1 mt7 xsmt5">
<div class="namefield">
<div class="mdem11 smem11 xsem10 w300 whitetext" contenteditable="true">Full Name</div>
<div class="mdem11 smem11 xsem10"><input type="text" name="name" class="fieldinput" placeholder="Name"></div>
</div>
<div class="copydiv">
<div class="mdem11 smem11 xsem10 w300 whitetext fieldname" contenteditable="true">Email Address</div>
<div class="mdem12 smem11 xsem10"><input type="text" name="email" class="fieldinput" placeholder="Email"></div>
</div>
<div class="mdem12 smem12 xsem11 mt5 xsmt5 w400"><a href="#" class="form-btn9" contenteditable="true"  id="rect">Sign Up</a></div>
</div>
</div>
</div>				
</div>
</div>',
						  'member_id' => $mem_id,
						  'add_time' =>1470394007,
						  'ip' => '',
						  'status' => 'active'
					   ),
					array(
						  'id' => 10,
						  'template_name' => 'Template 10' ,
						  'template_text' => '<div class="form10-bg"  id="demo-preview">
<div class="row"> 
<div class="col-md-12 col-sm-12 col-xs-12 segoe">
<div class="col-md-12 col-sm-12 col-xs-12 form_area10 whitetext">
<div class="mdem15 smem13 xsem12 w600 text-center" contenteditable="true">ONLY TODAY !</div>
<div class="mdem42 smem35 xsem30 yellowtext text-center w700" contenteditable="true">50% OFF</div>
<div class="mdem11 smem10 xsem9 text-center" contenteditable="true">Strength is the product of struggle, You <br> must do what others don\'t to acheive <br> what others won\'t</div>
<div class="mdem10 smem9 xsem9 text-center mt5 xsmt8" contenteditable="true">If so, enter your detail below</div>
<div class="col-md-10 col-md-offset-1 col-sm-10 col-sm-offset-1 col-xs-10 col-xs-offset-1 mt2 xsmt4">
<div class="namefield">
<div class="mdem9 smem9 xsem9"><input type="text" placeholder="Name" name="name" class="fieldinput"></div>
</div>
<div class="copydiv">
<div class="mdem9 smem9 xsem9"><input type="text" placeholder="Email" name="email" class="fieldinput"></div>
</div>
<div class="mdem11 smem11 xsem11 w500"><a href="#" class="form-btn10" contenteditable="true"  id="rect">Hurry! Register Today</a></div>
<div class="mdem8 smem8 xsem8 text-center mt4 xsmt5" contenteditable="true"> <img src="./assets/default/images/form-images/icon.png">&nbsp;&nbsp;Your information will never be shared.</div>

</div>
</div>
</div>				
</div>
</div>',
						  'member_id' => $mem_id,
						  'add_time' =>1470394007,
						  'ip' => '',
						  'status' => 'active'
					   )	
						
		);
		$this->db->insert_batch('tbl_form_template', $values); 
	}
	}
	
	function Templates($mem_id)
	{
		$this->db->select('id');
		$query = $this->db->get('tbl_template');
		if($query->num_rows()==0){
		$values = array(
					array(
						  'id' => 1 ,
						  'template_type' => 'default' ,
						  'editor_type' => 'inline',
						  'template_name' => 'Template 1',
						  'template_text' =>'<div style="width:100%; max-width:600px; margin: auto;"><table style="border-collapse:collapse;height:100%;margin:0;padding:0;background-color:#e9e9e9; width:100%;max-width:600px; margin:0 auto;" width="100%" cellspacing="0" cellpadding="0" border="0" align="center" height="100%">
<tbody>
<tr>
<td style="height:100%;margin:0;padding:0;width:100%;border-top:0" valign="top" align="center">

<table style="border-collapse:collapse;width:100%;max-width:600px;" width="100%" cellspacing="0" cellpadding="0" border="0">
<tbody>
<tr><td valign="top" align="center">

<table style="border-collapse:collapse; background:#3a617e;width:100%;max-width:600px;" width="600" cellspacing="0" cellpadding="0" border="0">
<tbody>
<tr>
<td style="padding:9px;text-align:center" valign="top">
<img alt="" src="{image_path}images/logo.png" style="box-sizing: border-box;max-width:163px;padding-bottom:0;display:inline!important;vertical-align:bottom;border:0;height:auto;outline:none;text-decoration:none" width="163" align="middle"></td></tr></tbody></table>
                                    
                                </td>
                            </tr>
                            <tr>
                                <td valign="top" align="center">
                                    
<table style="border-collapse:collapse;border-top:0;border-bottom:0;width:100%;max-width:600px;" width="100%" cellspacing="0" cellpadding="0" border="0">
<tbody><tr><td valign="top" align="center">
                                                
<table style="border-collapse:collapse;width:100%;max-width:600px;" width="600" cellspacing="0" cellpadding="0" border="0">
<tbody><tr><td valign="top" align="center">

<table style="border-collapse:collapse;background-color:#ffffff;border:0px solid #d5d5d5;width:100%;max-width:600px;" width="100%" cellspacing="0" cellpadding="0" border="0">
<tbody><tr><td valign="top">


<table style="min-width:100%;border-collapse:collapse;width:100%;max-width:600px;" width="100%" cellspacing="0" cellpadding="0" border="0" align="left">
<tbody><tr><td valign="top">
<img alt="" src="{image_path}images/banner.png" style="box-sizing: border-box;max-width:100%;padding-bottom:0;display:inline!important;vertical-align:bottom;border:0;height:auto;outline:none;text-decoration:none" tabindex="0" width="600" align="middle"></td></tr>
</tbody></table>


<table style="max-width:100%;min-width:100%;border-collapse:collapse;  clear:both;width:100%;max-width:600px;" width="100%" cellspacing="0" cellpadding="0" border="0" align="left"><tbody><tr><td style="padding-top:0; padding-right:18px; padding-bottom:9px; padding-left:18px; word-break:break-word; color:#606060; font-size:15px; text-align:left;" valign="top">
<div class="editblock" contenteditable="true">
<div style="display:block; margin:15px 0 0 0; padding:0; font-size:40px; font-style:normal; line-height:100%; color:#d95433;" align="center"><b>Apply Online</b></div>
</div>
<div class="editblock" contenteditable="true">
<p style="display:block; margin:15px 0 0 0; padding:0; font-size:20px; font-style:normal; line-height:100%; text-align:center; color:#d95433;">for your Loan and get faster approval!</p>
</div>
<div class="editblock" contenteditable="true">
<p style="display:block; margin:5px 0 0 0; font-size:15px; text-align:center; color:#353535!important;">Explore Loan offers from leading Banks now. Rates from 11.99% available.</p>
</div>
<hr style="background:#d6d6d6; height:1px; border:0; margin:15px 0 5px 0;"></td></tr></tbody>
</table>



	<table style="width:100%;max-width:600px;min-width:100%;border-collapse:collapse; clear:both;" width="100%" cellspacing="0" cellpadding="15" border="0" align="left">
    <tbody>
	
	<tr>
	<td width="40%"><img alt="" src="{image_path}images/education-loan.png" style="box-sizing: border-box;max-width:100%;padding-bottom:0;display:inline!important;vertical-align:bottom;border:1px solid #d6d6d6;height:auto;outline:none;text-decoration:none" tabindex="0" width="200" align="middle"></td>
    <td style="word-break:break-word;color:#4a4949;text-align:left" valign="top">
	<div class="editblock" contenteditable="true">
	<p style="display:block; margin:0; padding:0; line-height:100%; font-family:Helvetica; font-size:20px; font-style:normal; line-height:100%;">
	<b>Education Loan</b>
	</p>
	</div>
	<div class="editblock" contenteditable="true">
	<p style="display:block; margin-top:15px; margin-right:0px; margin-bottom:0px; margin-left:0px; padding:0; font-family:Helvetica; font-size:14px; font-style:normal; line-height:150%;">
Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
	</p>
	</div>
	
	<div class="editblock" contenteditable="true">
	<a href="#" style="font-family:Helvetica; border-radius:03px; background-color:#d95433; padding:9px 18px; text-align:center; text-decoration:none; color:#ffffff; display:inline-block; margin-top:6px;" class="sm-button">Apply Now</a>
	</div>
	
	<!--<a style="font-weight:bold; font-family:Helvetica; letter-spacing:normal; border-radius:3px; background-color:#d95433; padding:12px; line-height:100%; text-align:center; text-decoration:none; color:#ffffff; display:inline-block; margin-top:6px;"><div contenteditable="true">Apply Now</div></a>-->
    </td>
    </tr>
	
	
	<tr>
	<td><img alt="" src="{image_path}images/home-loan.png" style="box-sizing: border-box;max-width:100%;padding-bottom:0;display:inline!important;vertical-align:bottom;border:1px solid #d6d6d6;height:auto;outline:none;text-decoration:none" tabindex="0" width="200" align="middle"></td>
    <td style="word-break:break-word;color:#4a4949;text-align:left" valign="top">
	<div class="editblock" contenteditable="true"><p style="display:block; margin:0; padding:0; line-height:100%; font-family:Helvetica; font-size:20px; font-style:normal; line-height:100%;"><b>Home Loan</b></p></div>
	<div class="editblock" contenteditable="true">
	<p style="display:block; margin:15px 0 0 0; padding:0; font-family:Helvetica; font-size:14px; font-style:normal; line-height:150%;">Lorem ipsum <span>dolor</span> sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
	</p>
	
	</div>
	
	<div class="editblock" contenteditable="true">
	<a href="#" style="font-family:Helvetica; border-radius:03px; background-color:#d95433; padding:9px 18px; text-align:center; text-decoration:none; color:#ffffff; display:inline-block; margin-top:6px;" class="sm-button">Apply Now</a></div>
	
    </td>
    </tr>
	
	
	<tr style="border-bottom:15px solid #3a617e;">
	<td><img alt="" src="{image_path}images/car-loan.png" style="box-sizing: border-box;max-width:100%;padding-bottom:0;display:inline!important;vertical-align:bottom;border:1px solid #d6d6d6;height:auto;outline:none;text-decoration:none" tabindex="0" width="200" align="middle"></td>
    <td style="word-break:break-word;color:#4a4949;text-align:left" valign="top">
	<div class="editblock" contenteditable="true"><p style="display:block; margin:0; padding:0; line-height:100%; font-family:Helvetica; font-size:20px; font-style:normal; line-height:100%;"><b>Loan Loan</b></p></div>
	<div class="editblock" contenteditable="true"><p style="display:block; margin:15px 0 0 0; padding:0; font-family:Helvetica; font-size:14px; font-style:normal; line-height:150%;">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p></div>
	
	<div class="editblock" contenteditable="true">
	<a href="#" style="font-family:Helvetica; border-radius:03px; background-color:#d95433; padding:9px 18px; text-align:center; text-decoration:none; color:#ffffff; display:inline-block; margin-top:6px;" class="sm-button">Apply Now</a></div>
    
    
    
    </td>
    </tr>
	
	
	
	
	
	
</tbody>
</table>				

				


</td>
                                                    </tr>

                                                </tbody></table>
                                            </td>
                                        </tr>
                                    </tbody></table>
                                    
                                </td>
                            </tr>
                           
                        </tbody></table>
                        
                    </td>
</tr>

</tbody>
</table>
			
	</td>
    </tr>
    </tbody>
    </table>	
    </div>',
						  'template_screenshot' => 'template1.jpg',
						  'member_id' => $mem_id,
						  'add_time' => 1470394007,
						  'ip' => '',
						  'status' => 'active'
					   ),
					array(
						  'id' => 2 ,
						  'template_type' => 'default' ,
						  'editor_type' => 'inline',
						  'template_name' => 'Template 2',
						  'template_text' =>'<div style="width:100%; max-width:600px; margin: auto;"><table style="border-collapse:collapse;height:100%;margin:0 auto;padding:0;width:100%;background-color:#e9e9e9;max-width:600px;" width="100%" cellspacing="0" cellpadding="0" border="0" align="center" height="100%">
<tbody>
<tr>
<td style="height:100%;margin:0;padding:0;width:100%;border-top:0" valign="top" align="center">

<table style="border-collapse:collapse;max-width:600px;width:100%;" width="100%" cellspacing="0" cellpadding="0" border="0">
<tbody>
<tr><td valign="top" align="center">

<table style="border-collapse:collapse; background:#41aa9a;max-width:600px;width:100%;" width="100%" cellspacing="0" cellpadding="0" border="0">
<tbody>
<tr>
<td style="padding:18px;text-align:center" valign="top">
<img alt="" src="{image_path}images/logo2.png" style="box-sizing: border-box;height:70px;max-height:70px;padding-bottom:0;display:inline!important;vertical-align:bottom;border:0;width:auto;outline:none;text-decoration:none" align="middle"></td></tr></tbody></table>
                                    
                                </td>
                            </tr>
                            <tr>
                                <td valign="top" align="center">
                                    
<table style="border-collapse:collapse;border-top:0;border-bottom:0;max-width:600px;width:100%;" width="100%" cellspacing="0" cellpadding="0" border="0">
<tbody><tr><td valign="top" align="center">
                                                
<table style="border-collapse:collapse;max-width:600px;width:100%;" width="100%" cellspacing="0" cellpadding="0" border="0">
<tbody><tr><td valign="top" align="center">

<table style="border-collapse:collapse;background-color:#41aa9a;border:1px solid #41aa9a;max-width:600px;width:100%;" width="100%" cellspacing="0" cellpadding="0" border="0">
<tbody><tr><td valign="top">
<table style="border-collapse:collapse;background-color:#fff;border:1px solid #fff;margin:0 auto 25px auto;max-width:550px;width:100%;" width="92%" cellspacing="0" cellpadding="0" border="0">
<tbody><tr><td valign="top">


<table style="max-width:600px;width:100%;min-width:100%;border-collapse:collapse;  clear:both;" width="100%;" cellspacing="0" cellpadding="0" border="0" align="left"><tbody><tr><td style="color:#4a4949; font-size:15px; text-align:left;" valign="top">
<div  contenteditable="true">
<div style="display:block; margin:5px 15px 0 0; padding:0; font-size:13px; text-align:right;line-height:100%; color:#dadada;" align="center;"><b>*T &amp; C Apply</b></div></div>
<div contenteditable="true">
<div style="display:block; margin:15px 0 0 0; padding:0; font-size:58px; text-align:center;line-height:100%; color:#4a4949;font-weight:100;" align="center;">Summer</div>
</div>

<div contenteditable="true">
<div style="display:block; margin:15px 0 0 0; padding:0; font-size:100px; font-style:normal; line-height:100%; color:#4a4949;" align="center"><b>SALE</b></div>
</div>

<div contenteditable="true">
<div style="display:block; margin:15px 0 15px 0;font-size:45px; padding: 0% 15%; font-style:normal; line-height:100%; color:#fff;" align="center"><div style="background-color:#41aa9a;padding: 2.5% 0%;">40% OFF</div></div>
</div>

<div contenteditable="true">
<div style="display:block; margin:10px 0 10px 0;font-size:23px; padding: 0; font-style:normal; line-height:100%; color:#4a4949;font-weight:400;" align="center">ALL PURCHASES OVER $ 100</div>
</div>

<div contenteditable="true">
<div style="display:block; margin:20px 0 20px 0;font-size:50px; padding: 0; font-style:normal; line-height:100%; color:#41aa9a;" align="center">LAST WEEK!</div>
</div>

<div contenteditable="true">
<div style="display:block; margin:20px 0 20px 0;font-size:15px; padding: 0; font-style:normal; line-height:150%; color:#4a4949;padding:0 5%;" align="center">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</div>
</div>

<div style="text-align:center;" contenteditable="true">
<a href="#" style="border-radius:03px; background-color:#3d455c; padding: 6px 30px; text-align:center; text-decoration:none; color:#ffffff; display:inline-block;font-size:30px;margin:15px 0 15px 0;" class="sm-button">Shop Now</a></div>

</td>
</tr>
</tbody>
</table>
</td>
</tr>
</tbody></table>
</td>
</tr>
</tbody></table>



 </td>
</tr>
</tbody></table>
</td>
</tr>


</tbody></table>
</td>
</tr>
</tbody>
</table>
</td></tr>
</tbody>
</table>
</div>',
						  'template_screenshot' => 'template3.jpg',
						  'member_id' => $mem_id,
						  'add_time' => 1470394007,
						  'ip' => '',
						  'status' => 'active'
					   ),
					array(
						  'id' => 3 ,
						  'template_type' => 'default' ,
						  'editor_type' => 'inline',
						  'template_name' => 'Template 3',
						  'template_text' =>'<div style="width:100%; max-width:600px; margin: auto;"><table style="border-collapse:collapse;height:100%;margin:0;padding:0;width:100%;background-color:#e9e9e9; max-width:600px; margin:0 auto;" width="100%" cellspacing="0" cellpadding="0" border="0" align="center" height="100%">
<tbody>
<tr>
<td style="height:100%;margin:0;padding:0;width:100%;border-top:0" valign="top" align="center">

<table style="border-collapse:collapse;width:100%;max-width:600px;" width="100%" cellspacing="0" cellpadding="0" border="0">
<tbody>
<tr>
<td valign="top" align="center">

<table style="border-collapse:collapse; background:#405292;width:100%;max-width:600px;" width="600" cellspacing="0" cellpadding="0" border="0">
<tbody>
<tr>
<td style="padding:9px;text-align:center;word-break:break-word; color:#606060; font-size:15px;" valign="top">

<div contenteditable="true">
<div style="display:block; margin:15px 0; padding:0; font-size:28px; font-style:normal; line-height:100%; color:#fff;" align="center"><b>Providing Total Health Care Solution</b></div>
</div>

</td>
</tr>
</tbody>
</table>
                                    
</td>
</tr>

<tr>
<td valign="top" align="center">
                                    
<table style="border-collapse:collapse;border-top:0;border-bottom:0;background-color:#ffffff;width:100%;max-width:600px;" width="100%" cellspacing="0" cellpadding="0" border="0">
<tbody><tr><td valign="top" align="center">
                                                



<table style="min-width:100%;border-collapse:collapse;width:100%;max-width:600px;" width="100%" cellspacing="0" cellpadding="0" border="0" align="left">
<tbody><tr><td valign="top">
<img alt="" src="{image_path}images/temp3.png" style="box-sizing: border-box;max-height:310px;max-width:100%;padding-bottom:0;display:inline!important;vertical-align:bottom;border:0;height:auto;outline:none;text-decoration:none" tabindex="0" width="600" align="middle"></td></tr>
</tbody></table>


<table style="max-width:100%;min-width:100%;border-collapse:collapse;  clear:both;width:100%;max-width:600px;" width="100%;" cellspacing="0" cellpadding="0" border="0" align="left"><tbody><tr><td style="padding-top:0; padding-right:18px; padding-bottom:9px; padding-left:18px; word-break:break-word; color:#606060; font-size:15px; text-align:left;" valign="top">

<div contenteditable="true">
<p style="display:block; margin:20px 0 10px 0; padding:0; font-size:25px; font-weight:500;  text-align:center; color:#4a4949; line-height:30px;">Lorem ipsum dolor sit amet, no quodsi definitiones vis. Verterem postulant intellegat </p>
</div>

<hr style="background:#d6d6d6; height:1px; border:0; margin:15px 0 15px 0;">


<div contenteditable="true">
<p style="display:block; margin:15px 0 10px 0; padding:0; font-size:25px; font-weight:400; text-align:center; color:#000000; line-height:30px;">Description</p>
</div>


<div contenteditable="true">
<p style="display:block; margin:2px 0 10px 0; padding:10px 25px ; font-size:15px; font-weight:400; text-align:center; color:#818080; line-height:25px;">Lorem ipsum dolor sit amet, no quodsi definitiones vis. Verterem postulant intellegat est et, an partiendo argumentum elaboraret qui. Diam salutatus cu est, choro sententiae nam id. An sint meliore conceptam qui, eum dicit corpora consequuntur no. Aperiam repudiandae pri an. Vidisse torquatos mea ne, debet ludus duo ne, in quodsi fuisset theophrastus qui.
</p>
</div>

<div style="text-align:center;" contenteditable="true">
<a href="#" style="font-family:Roboto; border-radius:03px; background-color:#ff4747; padding:2% 20%; text-align:center; text-decoration:none; color:#ffffff; display:inline-block; margin:10px 0 15px 0;font-size:20px;" class="sm-button">Get Appointment</a>
</div>


</td>
</tr>
</tbody>
</table>




</td>
</tr>
</tbody></table>
</td>
</tr>
</tbody>
</table>
</td>
</tr>
</tbody>
</table>
</div>',
						  'template_screenshot' => 'template9.jpg',
						  'member_id' => $mem_id,
						  'add_time' => 1470394007,
						  'ip' => '',
						  'status' => 'active'
					   ),
					array(
						  'id' => 4 ,
						  'template_type' => 'default' ,
						  'editor_type' => 'inline',
						  'template_name' => 'Template 4',
						  'template_text' =>'<div style="width:100%; max-width:600px; margin: auto;"><table style="border-collapse:collapse;height:100%;margin:0;padding:0;width:100%;background-color:#e9e9e9; max-width:600px; margin:0 auto;" width="100%" cellspacing="0" cellpadding="0" border="0" align="center" height="100%">
<tbody>
<tr>
<td style="height:100%;margin:0;padding:0;width:100%;border-top:0" valign="top" align="center">

<table style="border-collapse:collapse;width:100%;max-width:600px;" width="100%" cellspacing="0" cellpadding="0" border="0">
<tbody>
<tr>
<td valign="top" align="center">
<table style="border-collapse:collapse;background:#fff;max-width:600px;width:100%;" width="600" cellspacing="0" cellpadding="0" border="0">
<tbody>
<tr>
<td style="text-align:center;word-break:break-word;padding:0 9px;" valign="top">

<div contenteditable="true">
<div style="display:block; margin:30px 0 10px 0; padding:0; font-size:26px; line-height:40px; color:#65a3ca;" align="center">This Area Is Home Sweet Home For A Sweet,Catchy Headline</div>
</div>

<div contenteditable="true">
<div style="display:block; margin:8px 0 35px 0; padding:0; font-size:16px; font-style:normal; line-height:40px; color:#4a4949;" align="center">Maybe A Little Description Section Too, In Case There’s More To Say!</div>
</div>

</td>
</tr>
</tbody>
</table>                                  
</td>
</tr>

<tr>
<td valign="top" align="center">
                                    
<table style="border-collapse:collapse;border-top:0;border-bottom:0;background-color:#ffffff;width:100%;max-width:600px;" width="100%" cellspacing="0" cellpadding="0" border="0">
<tbody><tr><td valign="top" align="center">
                                                



<table style="min-width:100%;border-collapse:collapse;max-width:600px;width:100%;" width="100%" cellspacing="0" cellpadding="0" border="0" align="left">
<tbody><tr><td valign="top">
<img alt="" src="{image_path}images/temp4.png" style="box-sizing: border-box;max-height:390px;max-width:100%;padding-bottom:0;display:inline!important;vertical-align:bottom;border:0;height:auto;outline:none;text-decoration:none" tabindex="0" width="600" align="middle"></td></tr>
</tbody></table>


<table style="min-width:100%;border-collapse:collapse;  clear:both;max-width:600px;width:100%;" width="100%;" cellspacing="0" cellpadding="0" border="0" align="left"><tbody><tr><td style="padding-top:0; padding-right:18px; padding-bottom:9px; padding-left:18px; word-break:break-word; color:#606060; font-size:15px; text-align:left;" valign="top">

<div style="text-align:center;margin:15px 0 15px 0;padding:2% 0;" contenteditable="true">
<a href="#" style="font-family:Roboto; border-radius:03px; background-color:#65a3ca; padding:2% 13%; text-align:center; text-decoration:none; color:#ffffff; display:inline-block;font-size:22px;" class="sm-button">Call To Action</a>
</div>

<div contenteditable="true">
<p style="display:block; margin:15px 0 10px 0; padding:0; font-size:25px; font-weight:400; text-align:center; color:#606060; line-height:30px;">Company Name</p>
</div>

<div contenteditable="true">
<p style="display:block; margin:2px 0 10px 0; padding:10px 30px ; font-size:15px; font-weight:400; text-align:center; color:#818080; line-height:25px;">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.
</p>
</div>

</td>
</tr>
</tbody>
</table>

</td>
</tr>
</tbody></table>
</td>
</tr>
</tbody>
</table>
</td>
</tr>
</tbody>
</table>
</div>',
						  'template_screenshot' => 'template7.jpg',
						  'member_id' => $mem_id,
						  'add_time' => 1470394007,
						  'ip' => '',
						  'status' => 'active'
					   ),
					array(
						  'id' => 5 ,
						  'template_type' => 'default' ,
						  'editor_type' => 'inline',
						  'template_name' => 'Template 5',
						  'template_text' =>'<div style="width:100%; max-width:600px; margin: auto;"><table style="border-collapse:collapse;height:100%;margin:0;padding:0;width:100%;background-color:#fff; max-width:600px; margin:0 auto;border-left:20px solid #75caed;" width="100%" cellspacing="0" cellpadding="0" border="0" align="center" height="100%">
<tbody>
<tr>
<td style="height:100%;margin:0;padding:0;width:100%;border-top:0" valign="top" align="center">


                                                




<table style="max-width:100%;min-width:100%;border-collapse:collapse;  clear:both;width:100%;max-width:600px;" width="100%;" cellspacing="0" cellpadding="0" border="0" align="left"><tbody><tr><td style="padding:20px 30px; word-break:break-word; color:#606060; font-size:15px; text-align:left;" valign="top">




<div contenteditable="true">
<div style="display:block; margin:15px 0; padding:0; font-size:28px; font-style:normal; line-height:100%; color:#75caed;" align="left"><b>Converting Your Dreams Into Reality Is Our Passion</b></div>
</div>



<hr style="background:#75caed; height:1px; border:0; margin:15px 0 15px 0;">


<div contenteditable="true">
<p style="display:block; margin:10px 0 10px 0; padding:0; font-size:18px; font-weight:500;  text-align:left; color:#4a4949; line-height:30px;">Dear Customers,</p>
</div>




<div contenteditable="true">
<p style="display:block; margin:2px 0 5px 0; padding:10px 0px ; font-size:14px; font-weight:400; text-align:left; color:#000; line-height:25px;">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Verum tamen cum de rebus grandioribus dicas, ipsae res verba rapiunt; Luxuriam non reprehendit, modo sit vacua infinita cupiditate et timore. Apud imperitos tum illa dicta sunt, aliquid etiam coronae datum; Quod non faceret, si in voluptate summum bonum poneret. Quid ei reliquisti, nisi te, quoquo modo loqueretur, intellegere, quid diceret? Ego vero volo in virtute vim esse quam maximam
</p>
</div>

<div contenteditable="true">
<p style="display:block; margin:2px 0 5px 0; padding:10px 0px ; font-size:14px; font-weight:400; text-align:left; color:#000; line-height:25px;">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Verum tamen cum de rebus grandioribus dicas, ipsae res verba rapiunt; Luxuriam non reprehendit, modo sit vacua infinita cupiditate et timore. Apud imperitos tum illa dicta sunt, aliquid etiam coronae datum; Quod non faceret, si in voluptate summum bonum poneret. Quid ei reliquisti, nisi te, quoquo modo loqueretur, intellegere, quid diceret? Ego vero volo in virtute vim esse quam maximam
</p>
</div>

<div contenteditable="true">
<p style="display:block; margin:2px 0 5px 0; padding:10px 0px ; font-size:14px; font-weight:400; text-align:left; color:#000; line-height:25px;">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Verum tamen cum de rebus grandioribus dicas, ipsae res verba rapiunt; Luxuriam non reprehendit, modo sit vacua infinita cupiditate et timore. Apud imperitos tum illa dicta sunt, aliquid etiam coronae datum; Quod non faceret, si in voluptate summum bonum poneret. Quid ei reliquisti, nisi te, quoquo modo loqueretur, intellegere, quid diceret? Ego vero volo in virtute vim esse quam maximam
</p>
</div>


<div contenteditable="true">
<p style="display:block; margin:2px 0 5px 0; padding:10px 0px ; font-size:14px; font-weight:400; text-align:left; color:#000; line-height:25px;">Thanks for a great year.
</p>
</div>

<div contenteditable="true">
<p style="display:block; margin:2px 0 5px 0; padding:10px 0px ; font-size:14px; font-weight:400; text-align:left; color:#000; line-height:18px;">
Sincerely<br>
Your Name
</p>
</div>

<div style="text-align:center;" contenteditable="true">
<a href="#" style="font-family:Roboto; border-radius:03px; background-color:#656e72; padding:2% 20%; text-align:center; text-decoration:none; color:#ffffff; display:inline-block; margin:20px 0 15px 0;font-size:20px;" class="sm-button">Contact Our Business Experts Now</a>
</div>


</td>
</tr>
</tbody>
</table>


                                       
          
</td>
</tr>

</tbody>
</table>
</div>',
						  'template_screenshot' => 'template15.jpg',
						  'member_id' => $mem_id,
						  'add_time' => 1470394007,
						  'ip' => '',
						  'status' => 'active'
					   ),
					array(
						  'id' => 6 ,
						  'template_type' => 'default' ,
						  'editor_type' => 'inline',
						  'template_name' => 'Template 6',
						  'template_text' =>'<div style="width:100%; max-width:600px; margin: auto;"><table style="border-collapse:collapse;height:100%;margin:0;padding:0;width:100%;background-color:#fff; max-width:600px; margin:0 auto;border:20px solid #d6d6d6;" width="100%" cellspacing="0" cellpadding="0" border="0" align="center" height="100%">
<tbody>
<tr>
<td style="height:100%;margin:0;padding:0;width:100%;border-top:0" valign="top" align="center">



<table style="max-width:100%;min-width:100%;border-collapse:collapse;  clear:both;width:100%;max-width:600px;" width="100%;" cellspacing="0" cellpadding="0" border="0"><tbody><tr><td style="padding:20px 30px; word-break:break-word; color:#606060; font-size:15px;" valign="top">


<div contenteditable="true">
<div style="display:block; margin:15px 0; padding:0; font-size:40px; font-style:normal; line-height:100%; color:#fb6735;" align="center"><b>Explore Our Music App!</b></div>
</div>

<table style="min-width:100%;border-collapse:collapse;max-width:600px;width:100%;margin:10% 0; " width="100%" cellspacing="0" cellpadding="0" border="0" align="center">
<tbody><tr><td valign="top" align="center">
<img alt="" src="{image_path}images/temp6.png" style="box-sizing: border-box;padding-bottom:0;display:inline!important;vertical-align:bottom;border:0;height:auto;outline:none;text-decoration:none; max-width:500px;" tabindex="0" align="middle"></td></tr>
</tbody></table>


<div contenteditable="true">
<div style="display:block; margin:9% 0 5% 0; padding:0; font-size:25px; font-style:normal; line-height:100%; color:#fb6735;" align="center">Our Product Will Amaze You</div>
</div>

<div contenteditable="true">
<p style="display:block; margin:2px 0 10px 0; padding:10px 30px ; font-size:15px; font-weight:400; text-align:center; color:#818080; line-height:25px;">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.
</p>
</div>

<div style="text-align:center;margin:15px 0 15px 0;padding:2% 0;" contenteditable="true">
<a href="#" style="font-family:Roboto; border-radius:03px; background-color:#232c3b; padding:2% 13%; text-align:center; text-decoration:none; color:#ffffff; display:inline-block;font-size:22px;" class="sm-button">Explore Now</a>
</div>


</td>
</tr>
</tbody>
</table>
                        
          
</td>
</tr>

</tbody>
</table>

</div>',
						  'template_screenshot' => 'template11.jpg',
						  'member_id' => $mem_id,
						  'add_time' => 1470394007,
						  'ip' => '',
						  'status' => 'active'
					   ),
					array(
						  'id' => 7 ,
						  'template_type' => 'default' ,
						  'editor_type' => 'inline',
						  'template_name' => 'Template 7',
						  'template_text' =>'<div style="width:100%; max-width:600px; margin: auto;">

<table align="center" border="0" cellpadding="0" cellspacing="0" height="100%" width="100%" style="border-collapse:collapse;height:100%;margin:0;padding:0;width:100%;background-color:#e9e9e9; max-width:600px; margin:0 auto;">
<tbody>
<tr>
<td align="center" valign="top" style="height:100%;margin:0;padding:0;width:100%;border-top:0">

<table border="0" cellpadding="0" cellspacing="0" width="100%" style="border-collapse:collapse;width:100%;max-width:600px;">
<tbody>
<tr>
<td align="center" valign="top">
<table border="0" cellpadding="0" cellspacing="0" width="600" style="border-collapse:collapse;background:#ff854f;max-width:600px;width:100%;">
<tbody>
<tr>
<td valign="top" style="text-align:center;word-break:break-word;padding:0 9px;">
<div contenteditable="true">
<div style="display:block; margin:15px 0 0 0; padding:0; font-size:125px; font-style:normal; line-height:100%; color:#fff;    letter-spacing: 15px;" align="center"><b>OUR</b></div></div>


<div style=" margin:5px auto;"><hr style="background:#d8612d; height:2px; border:0; margin:0 auto;width:50%;"></div>

<div contenteditable="true">
<div style="display:block; margin:15px 0 25px 0; padding:0; font-size:95px; text-align:center;line-height:100%; color:#fff;" align="center;">EVENT</div>
</div>
</td>
</tr>

</tbody>
</table>                                  
</td>
</tr>

<tr>
<td align="center" valign="top">
<table border="0" cellpadding="0" cellspacing="0" width="600" style="border-collapse:collapse;background:#d8612d;max-width:600px;width:100%;">
<tbody>
<tr>
<td valign="top" style="text-align:center;word-break:break-word;padding:9px;">
</td>
</tr>
</tbody>
</table>                                  
</td>
</tr>


<tr>
<td align="center" valign="top">
                                    
<table border="0" cellpadding="0" cellspacing="0" width="100%" style="border-collapse:collapse;border-top:0;border-bottom:0;background-color:#ffffff;width:100%;max-width:600px;">
<tbody><tr><td align="center" valign="top">
                                                

<table align="left" border="0" cellpadding="0" cellspacing="0" style="min-width:100%;border-collapse:collapse;  clear:both;max-width:600px;width:100%;" width="100%;">
<tbody>
<tr>
<td valign="top" style="padding-top:0; padding-right:18px; padding-bottom:9px; padding-left:18px;">

<div contenteditable="true">
<p style="display:block; margin:18px 0 15px 0; padding:0; font-size:23px; font-weight:400; text-align:center; color:#000; line-height:30px;">Your Awesome Event Discriotion Right Here</p>
</div>

<div contenteditable="true">
<p style="display:block; margin:20px 0 18px 0; padding:0; font-size:60px; font-weight:400; text-align:center; color:#4a4949; line-height:30px;"><b>31.5.2016</b></p>
</div>

<div style=" margin:5px auto;"><hr style="background:#d6d6d6; height:2px; border:0; margin:0 auto;width:70%;"></div>

<div contenteditable="true">
<p style="display:block; margin:17px 0 10px 0; padding:0; font-size:21px; font-weight:400; text-align:center; color:#4a4949; line-height:30px;">And Where It All Gonna Happen?</p>
</div>

<div contenteditable="true">
<div style="display:block; margin:15px 0 10px 0; padding:0; font-size:60px; font-style:normal; line-height:100%; color:#ff854f;" align="center"><b>YOUR CITY</b></div>
</div>

<div contenteditable="true">
<div style="display:block; margin:15px 0 15px 0; padding:0; font-size:40px; text-align:center;line-height:100%; color:#ff854f;font-weight:100;" align="center;">AWESOME STREET</div>
</div>

<div contenteditable="true" style="text-align:center;">
<a href="#" style="border-radius:05px; background-color:#d8612d; padding: 6px 30px; text-align:center; text-decoration:none; color:#ffffff; display:inline-block;font-size:23px;margin:30px 0 30px 0;" class="sm-button">Join Now</a></div>

</td>
</tr>

<tr>
<td align="center" valign="top">
<table border="0" cellpadding="0" cellspacing="0" width="600" style="border-collapse:collapse;background:#d8612d;max-width:600px;width:100%;">
<tbody>
<tr>
<td valign="top" style="padding:2px;">
</td>
</tr>
</tbody>
</table>                                  
</td>
</tr>


</tbody>
</table>


</td>
</tr>
</tbody></table>
</td>
</tr>

</tbody>
</table>


</td>
</tr>
</tbody>
</table>


</div>',
						  'template_screenshot' => 'template4.jpg',
						  'member_id' => $mem_id,
						  'add_time' => 1470394007,
						  'ip' => '',
						  'status' => 'active'
					   ),
					array(
						  'id' => 8 ,
						  'template_type' => 'default' ,
						  'editor_type' => 'inline',
						  'template_name' => 'Template 8',
						  'template_text' =>'<div style="width:100%; max-width:600px; margin: auto;">

<table align="center" border="0" cellpadding="0" cellspacing="0" height="100%" width="100%" style="border-collapse:collapse;height:100%;margin:0;padding:0;background-color:#e9e9e9; width:100%;max-width:600px; margin:0 auto;">
<tbody>
<tr>
<td align="center" valign="top" style="height:100%;margin:0;padding:0;width:100%;border-top:0">

<table border="0" cellpadding="0" cellspacing="0" width="100%" style="border-collapse:collapse;width:100%;max-width:600px;">
<tbody>
<tr>
<td align="left" valign="top">

<table border="0" cellpadding="0" cellspacing="0" width="600" style="border-collapse:collapse; background:#fe546b;width:100%;max-width:600px;">
<tbody>
<tr>
<td valign="top" style="padding:9px;text-align:left">
<img align="left" alt="" src="{image_path}images/logo8.png" style="height:33px;max-height:33px;padding-bottom:0;display:inline!important;vertical-align:bottom;border:0;height:auto;outline:none;text-decoration:none"></td></tr></tbody></table>
                                    
</td>
</tr>

<tr>
<td align="center" valign="top">
                                    
<table border="0" cellpadding="0" cellspacing="0" width="100%" style="border-collapse:collapse;border-top:0;border-bottom:0;width:100%;max-width:600px;">
<tbody><tr><td align="center" valign="top">
                                                
<table border="0" cellpadding="0" cellspacing="0" width="600" style="border-collapse:collapse;width:100%;max-width:600px;">
<tbody><tr><td align="center" valign="top">

<table border="0" cellpadding="0" cellspacing="0" width="100%" style="border-collapse:collapse;background-color:#ffffff;border:0px solid #d5d5d5;width:100%;max-width:600px;">
<tbody><tr><td valign="top">


<table align="left" width="100%" border="0" cellpadding="0" cellspacing="0" style="min-width:100%;border-collapse:collapse;width:100%;max-width:600px;background-color:#eaede2;">
<tbody><tr>

<td valign="top" width="60%">
<div contenteditable="true">
<div style="display:block; padding:0; font-size:40px; color:#fe546b;font-weight:300;margin-left:50px;margin-top:60px;">Look and feel Your Best.</div>
</div>
<div contenteditable="true">
<p style="display:block;padding:0; font-size:16px;color:#4a4949;margin-left:50px;margin-top:20px;">We have you covered from head to toe Hair, Body & facial Treatments</p>
</div>
</td>

<td valign="top">
<img align="center" alt="" src="{image_path}images/temp8.png" style="max-width:240px;max-height:273px;padding-bottom:0;display:inline!important;vertical-align:bottom;border:0;height:auto;outline:none;text-decoration:none" tabindex="0"></td>
</tr>
</tbody></table>


<table align="left" border="0" cellpadding="0" cellspacing="0" style="max-width:100%;min-width:100%;border-collapse:collapse;  clear:both;width:100%;max-width:600px;" width="100%"><tbody><tr><td valign="top" style="padding-top:0; padding-right:18px; padding-bottom:9px; padding-left:18px; word-break:break-word; color:#606060; font-size:15px; text-align:left;">
<div contenteditable="true">
<div style="display:block; margin:15px 0 0 0; padding:0; font-size:40px; font-weight:300;color:#fe546b;" align="center">Beauty Zone</div>
</div>

<div contenteditable="true">
<p style="display:block; margin:15px 0 5px 0; font-size:15px; text-align:center; color:#353535!important;padding:0 5%;">Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus.</p>
</div>
</td></tr></tbody>
</table>



<table border="0" cellpadding="15" cellspacing="0" style="width:100%;max-width:600px;min-width:100%;border-collapse:collapse; clear:both;text-align:center;" width="100%">
<tbody>

<tr>
<td>
<img alt="" src="{image_path}images/icon1.png" tabindex="0" style="border:1px solid #dbdbdb;border-radius:50%;width:94px;height:94px;margin:5px auto;">
<div contenteditable="true">
<p style="display:block; margin:15px 0 5px 0; font-size:18px;color:#4a4949;font-weight:500;">Cutting & Styling</p>
</div>
</td>

<td>
<img alt="" src="{image_path}images/icon2.png" tabindex="0" style="border:1px solid #dbdbdb;border-radius:50%;background:#fff;width:94px;height:94px;margin:5px auto;">
<div contenteditable="true">
<p style="display:block; margin:15px 0 5px 0; font-size:18px;color:#4a4949;font-weight:500;">Spa & Relaxing</p>
</div>
</td>

<td>
<img alt="" src="{image_path}images/icon3.png" tabindex="0" style="border:1px solid #dbdbdb;border-radius:50%;width:94px;height:94px;margin:5px auto;">
<div contenteditable="true">
<p style="display:block; margin:15px 0 5px 0; font-size:18px;color:#4a4949;font-weight:500;">Nail Treatment</p>
</div>
</td>

</tr>
</tbody>
</table>

<table align="left" border="0" cellpadding="0" cellspacing="0" style="max-width:100%;min-width:100%;border-collapse:collapse;  clear:both;width:100%;max-width:600px;" width="100%">
<tbody><tr>
<td valign="top" style="padding-top:0; padding-right:18px; padding-bottom:9px; padding-left:18px;">
<div contenteditable="true" style="text-align:center;">
<a href="#" style="border-radius:05px; background-color:#fe546b; padding: 1.8% 25%; text-align:center; text-decoration:none; color:#ffffff; display:inline-block;font-size:18px;margin:30px 0 30px 0;" class="sm-button">Get A Appoitment</a></div>
</td></tr></tbody>
</table>
				

</td>
</tr>
</tbody></table>
</td>
</tr>
</tbody></table>
</td>
</tr>
</tbody></table>
</td>
</tr>

</tbody>
</table>
			
	</td>
    </tr>
    </tbody>
    </table>


</div>',
						  'template_screenshot' => 'template13.jpg',
						  'member_id' => $mem_id,
						  'add_time' => 1470394007,
						  'ip' => '',
						  'status' => 'active'
					   ),
					array(
						  'id' => 9 ,
						  'template_type' => 'default' ,
						  'editor_type' => 'inline',
						  'template_name' => 'Template 9',
						  'template_text' =>'<div style="width:100%; max-width:600px; margin: auto;">


<table align="center" border="0" cellpadding="0" cellspacing="0" height="100%" width="100%" style="border-collapse:collapse;height:100%;margin:0;padding:0;width:100%;background-color:#e9e9e9; max-width:600px; margin:0 auto;">
<tbody>
<tr>
<td align="center" valign="top" style="height:100%;margin:0;padding:0;width:100%;border-top:0">

<table border="0" cellpadding="0" cellspacing="0" width="100%" style="border-collapse:collapse;width:100%;max-width:600px;">
<tbody>
<tr>
<td align="left" valign="top">

<table border="0" cellpadding="0" cellspacing="0" width="600" style="border-collapse:collapse; background:#fff;width:100%;max-width:600px;">
<tbody>
<tr>
<td valign="top" style="padding:9px;">
<img align="left" alt="" src="{image_path}images/logo9.png" width="163" style="max-width:163px;padding-bottom:0;display:inline!important;vertical-align:bottom;border:0;height:auto;outline:none;
text-decoration:none;margin:10px 0 5px 15px;"></td></tr></tbody></table>                                 
</td>
</tr>

<tr>
<td align="center" valign="top">
                                    
<table border="0" cellpadding="0" cellspacing="0" width="100%" style="border-collapse:collapse;border-top:0;border-bottom:0;background-color:#ffffff;width:100%;max-width:600px;">
<tbody><tr><td align="center" valign="top">
                                                

<table align="left" width="100%" border="0" cellpadding="0" cellspacing="0" style="min-width:100%;border-collapse:collapse;width:100%;max-width:600px;">
<tbody><tr><td valign="top">
<img align="center" alt="" src="{image_path}images/temp9.png" width="600" style="max-width:100%;padding-bottom:0;display:inline!important;vertical-align:bottom;border:0;height:auto;outline:none;text-decoration:none;max-height:203px;" tabindex="0"></td></tr>
</tbody></table>


<table align="left" border="0" cellpadding="0" cellspacing="0" style="max-width:100%;min-width:100%;border-collapse:collapse;  clear:both;width:100%;max-width:600px;" width="100%;"><tbody><tr><td valign="top" style="padding-top:0; padding-right:18px; padding-bottom:9px; padding-left:18px; word-break:break-word; color:#606060; font-size:15px; text-align:left;">


<div contenteditable="true">
<p style="display:block; margin:20px 0 10px 0; padding:0; font-size:25px;text-align:center; color:#4a4949;">Email Confirmation</p>
</div>


<div contenteditable="true">
<p style="display:block; margin:2px 0 10px 0; padding:10px 25px ; font-size:15px; font-weight:400; text-align:center; color:#818080; line-height:25px;">Lorem ipsum dolor sit amet, no quodsi definitiones vis. Verterem postulant intellegat est et, an partiendo argumentum elaboraret qui. Diam salutatus cu est, choro sententiae nam id. An sint meliore conceptam qui, eum dicit corpora consequuntur no.
</p>
</div>

<div contenteditable="true" style="text-align:center;">
<a href="#" style="font-family:Roboto; border-radius:04px; background-color:#ffb200; padding:1.7% 6%; text-align:center; text-decoration:none; color:#ffffff; display:inline-block; margin:10px 0 20px 0;font-size:23px;" class="sm-button">Verify Email Address</a>
</div>

</td>
</tr>
</tbody>
</table>
                                 
                                    
</td>
</tr>
</tbody></table>
</td>
</tr>
</tbody>
</table>
</td>
</tr>
</tbody>
</table>


</div>',
						  'template_screenshot' => 'template10.jpg',
						  'member_id' => $mem_id,
						  'add_time' => 1470394007,
						  'ip' => '',
						  'status' => 'active'
					   ),
					array(
						  'id' => 10 ,
						  'template_type' => 'default' ,
						  'editor_type' => 'inline',
						  'template_name' => 'Template 10',
						  'template_text' =>'<div style="width:100%; max-width:600px; margin: auto;">


<table align="center" border="0" cellpadding="0" cellspacing="0" height="100%" width="100%" style="border-collapse:collapse;height:100%;margin:0;padding:0;background-color:#e9e9e9; width:100%;max-width:600px; margin:0 auto;">
<tbody>
<tr>
<td align="center" valign="top" style="height:100%;margin:0;padding:0;width:100%;border-top:0">

<table border="0" cellpadding="0" cellspacing="0" width="100%" style="border-collapse:collapse;width:100%;max-width:600px;">
<tbody>
<tr>
<td align="left" valign="top">

<table border="0" cellpadding="0" cellspacing="0" width="600" style="border-collapse:collapse; background:#98ba57;width:100%;max-width:600px;">
<tbody>
<tr>
<td valign="top" style="padding:9px;text-align:left">
<img align="left" alt="" src="{image_path}images/logo10.png" style="padding-bottom:0;display:inline!important;vertical-align:bottom;border:0;height:auto;outline:none;text-decoration:none"></td></tr></tbody></table>
                                    
</td>
</tr>

<tr>
<td align="center" valign="top">
                                    
<table border="0" cellpadding="0" cellspacing="0" width="100%" style="border-collapse:collapse;border-top:0;border-bottom:0;width:100%;max-width:600px;">
<tbody><tr><td align="center" valign="top">
                                                
<table border="0" cellpadding="0" cellspacing="0" width="600" style="border-collapse:collapse;width:100%;max-width:600px;">
<tbody><tr><td align="center" valign="top">

<table border="0" cellpadding="0" cellspacing="0" width="100%" style="border-collapse:collapse;background-color:#ffffff;border:0px solid #d5d5d5;width:100%;max-width:600px;">
<tbody><tr><td valign="top">


<table align="left" width="100%" border="0" cellpadding="0" cellspacing="0" style="min-width:100%;border-collapse:collapse;width:100%;max-width:600px;background-color:#ffffff;">
<tbody><tr>

<td valign="top" width="55%;">
<div contenteditable="true">
<div style="display:block; padding:0; font-size:38px; color:#4a4949;margin-left:50px;margin-top:30px;"><b>Free</b></div>
</div>
<div contenteditable="true">
<div style="display:block; padding:0; font-size:33px; color:#98ba57;margin-left:50px;"><b>First Consultant</b></div>
</div>
<div contenteditable="true">
<p style="display:block;padding:0 10%; font-size:10px;color:#b6b6b6;margin-left:25px;margin-top:20px;">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
</div>
</td>

<td valign="top">
<img align="center" alt="" src="{image_path}images/temp10.png" style="max-width:270px;max-height:240px;padding-bottom:0;display:inline!important;vertical-align:bottom;border:0;height:auto;outline:none;text-decoration:none" tabindex="0"></td>
</tr>
</tbody></table>


<table border="0" cellpadding="15" cellspacing="0" style="width:100%;max-width:600px;min-width:100%;border-collapse:collapse; clear:both;text-align:center;" width="100%">
<tbody>

<tr>
<td width="30%;" style="background-color:#f4f4f4;">
<img alt="" src="{image_path}images/icon4.png" tabindex="0" style="border-radius:50%;background:#fff;width:70px;height:70px;margin:5px auto;">
<div contenteditable="true">
<p style="display:block; margin:10px 0; font-size:15px;color:#4a4949;">Painless Treatment</p>
</div>
</td>

<td width="30%;" style="background-color:#ececec;">
<img alt="" src="{image_path}images/icon5.png" tabindex="0" style="border-radius:50%;background:#fff;width:70px;height:70px;margin:5px auto;">
<div contenteditable="true">
<p style="display:block; margin:15px 0 5px 0; font-size:15px;color:#4a4949;">Professional Services</p>
</div>
</td>

<td width="30%;" style="background-color:#f4f4f4;">
<img alt="" src="{image_path}images/icon6.png" tabindex="0" style="border-radius:50%;background:#fff;width:70px;height:70px;margin:5px auto;">
<div contenteditable="true">
<p style="display:block; margin:15px 0 5px 0; font-size:15px;color:#4a4949;">High Quality Drugs</p>
</div>
</td>

</tr>
</tbody>
</table>


<table align="left" border="0" cellpadding="0" cellspacing="0" style="max-width:100%;min-width:100%;border-collapse:collapse; clear:both;width:100%;max-width:600px;" width="100%"><tbody><tr><td valign="top" style="padding-top:0; padding-right:18px; padding-bottom:9px; padding-left:18px;color:#606060; font-size:15px;">


<div contenteditable="true">
<p style="display:block;font-size:13px; text-align:center; color:#bfbfbf!important;line-height:21px;margin-top:5%;">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.</p>
</div>
</td></tr></tbody>
</table>


<table align="left" border="0" cellpadding="0" cellspacing="0" style="min-width:100%;border-collapse:collapse;  clear:both;width:100%;max-width:600px;" width="100%">
<tbody><tr>
<td valign="top" style="padding-top:0; padding-right:18px; padding-bottom:9px; padding-left:18px;">
<div contenteditable="true" style="text-align:center;">
<a href="#" style="border-radius:05px; background-color:#98ba57; padding: 2.2% 5%; text-align:center; text-decoration:none; color:#ffffff; display:inline-block;font-size:21px;font-weight:300;margin:30px 0 30px 0;" class="sm-button">Click Here, To Book Your Appointment Today.</a></div>
</td></tr></tbody>
</table>
				


</td>
</tr>
</tbody></table>
</td>
</tr>
</tbody></table>
</td>
</tr>
</tbody></table>
</td>
</tr>

</tbody>
</table>
			
</td>
</tr>
</tbody>
</table>


</div>',
						  'template_screenshot' => 'template6.jpg',
						  'member_id' => $mem_id,
						  'add_time' => 1470394007,
						  'ip' => '',
						  'status' => 'active'
					   ),
					array(
						  'id' => 11 ,
						  'template_type' => 'default' ,
						  'editor_type' => 'inline',
						  'template_name' => 'Template 11',
						  'template_text' =>'<div style="width:100%; max-width:600px; margin: auto;">


<table align="center" border="0" cellpadding="0" cellspacing="0" height="100%" width="100%" style="border-collapse:collapse;height:100%;margin:0;padding:0;background-color:#fff; width:100%;max-width:600px; margin:0 auto;">
<tbody>
<tr>
<td align="center" valign="top" style="height:100%;margin:0;padding:0;width:100%;border-top:0">

<table border="0" cellpadding="0" cellspacing="0" width="100%" style="border-collapse:collapse;width:100%;max-width:600px;">
<tbody>
<tr>
<td>


<table width="100%" border="0" cellpadding="0" cellspacing="0" style="min-width:100%;border-collapse:collapse;width:100%;max-width:600px;background-color:#fff;">
<tbody><tr>

<td valign="top"> 
<table width="100%" border="0" cellpadding="0" cellspacing="0" style="border-collapse:collapse;width:100%;max-width:600px;background-color:#fff;">
<tbody><tr>
<td valign="top" style="padding:20px 20px;text-align:left">
<img align="left" alt="" src="{image_path}images/logo11.png" style="padding-bottom:0;display:inline!important;vertical-align:bottom;border:0;height:auto;outline:none;text-decoration:none"></td>

</tr>
</tbody>
</table>
</td>
</tr>

</tbody>
</table>

</td>
</tr>

</tbody>
</table>
 
<table width="100%" border="0" cellpadding="0" cellspacing="0" style="border-collapse:collapse;width:97%;max-width:600px;background-color:#00a2b5;">
<tbody>

<tr>

<td valign="top">
<img align="center" alt="" src="{image_path}images/temp11.png" style="max-height:292px;max-width:214px;padding-bottom:0;display:inline!important;vertical-align:bottom;border:0;height:auto;outline:none;text-decoration:none;margin-top:20px;margin-left:10px;" tabindex="0"></td>

<td valign="top">
<div contenteditable="true">
<div style="display:block; padding:0; font-size:65px; color:#fff;margin-left:20px;margin-top:25px;font-weight:100;">Free</div>
</div>
<div contenteditable="true">
<div style="display:block; padding:0; font-size:60px; color:#fff;margin-left:20px;font-weight:100;">Shipping</div>
</div>
<div contenteditable="true">
<a href="#" style="border-radius:25px; background-color:#fff; padding: 1.5% 17%; text-align:center; text-decoration:none; color:#4a4949; display:inline-block;font-size:21px;font-weight:400;margin:15px 0 5px 20px;" class="sm-button">On All Orders</a></div>
<div contenteditable="true" style="display:block;font-size:14px;color:#fff;padding-left: 25px;
 padding-bottom: 5px; padding-top: 5px;">
You will get your delivery by Diwali
</div>
</td>
</tr>

</tbody>
</table>






<table border="0" cellpadding="0" cellspacing="0" width="100%" style="border-collapse:collapse;background-color:#ffffff;border:0px solid #d5d5d5;width:95%;max-width:600px;">
<tbody><tr><td valign="top">
<table border="0" cellpadding="15" cellspacing="0" style="width:100%;max-width:600px;min-width:100%;border-collapse:collapse; clear:both;text-align:center;margin-top:3%;" width="100%">
<tbody>

<tr>
<td>
<img align="center" alt="" src="{image_path}images/icon7.png" style="border:1px solid #e7e7e7;outline:none;max-height:108px;" tabindex="0">
<div contenteditable="true" style="">
<div style="display:block; font-size:16px;color:#4a4949;margin:5px 0 0 0;">Hand Bags</div>
<div style="display:block;font-size:13px;color:#4a4949;margin:5px 0 0 0; ">Under Rs 499</div>
</div>
<div contenteditable="true" style="text-align:center;">
<a href="#" style="border-radius:05px; background-color:#fe9e01;padding: 4% 15%; text-align:center; text-decoration:none; color:#ffffff; display:inline-block;font-size:14px;margin:5px 0 5px 0;" class="sm-button">Buy Now</a></div>
</td>

<td>
<img align="center" alt="" src="{image_path}images/icon8.png"   style="border:1px solid #e7e7e7;outline:none;max-height:108px;" tabindex="0">
<div contenteditable="true" style="">
<div style="display:block; font-size:16px;color:#4a4949;margin:5px 0 0 0;">Sunglasses</div>
<div style="display:block;font-size:13px;color:#4a4949;margin:5px 0 0 0; ">Under Rs 499</div>
</div>
<div contenteditable="true" style="text-align:center;">
<a href="#" style="border-radius:05px; background-color:#fe9e01;padding: 4% 15%; text-align:center; text-decoration:none; color:#ffffff; display:inline-block;font-size:14px;margin:5px 0 5px 0;" class="sm-button">Buy Now</a></div>
</td>

<td>
<img align="center" alt="" src="{image_path}images/icon9.png"  style="border:1px solid #e7e7e7;outline:none;max-height:108px;" tabindex="0">
<div contenteditable="true" style="">
<div style="display:block; font-size:16px;color:#4a4949;margin:5px 0 0 0;">Backpacks</div>
<div style="display:block;font-size:13px;color:#4a4949;margin:5px 0 0 0; ">Under Rs 499</div>
</div>
<div contenteditable="true" style="text-align:center;">
<a href="#" style="border-radius:05px; background-color:#fe9e01;padding: 4% 15%; text-align:center; text-decoration:none; color:#ffffff; display:inline-block;font-size:14px;margin:5px 0 5px 0;" class="sm-button">Buy Now</a></div>
</td>

</tr>

<tr>

<td>
<img align="center" alt="" src="{image_path}images/icon12.png" style="border:1px solid #e7e7e7;outline:none;max-height:108px;" tabindex="0">
<div contenteditable="true" style="">
<div style="display:block; font-size:16px;color:#4a4949;margin:5px 0 0 0;">Watches</div>
<div style="display:block;font-size:13px;color:#4a4949;margin:5px 0 0 0; ">Under Rs 499</div>
</div>
<div contenteditable="true" style="text-align:center;">
<a href="#" style="border-radius:05px; background-color:#fe9e01;padding: 4% 15%; text-align:center; text-decoration:none; color:#ffffff; display:inline-block;font-size:14px;margin:5px 0 5px 0;" class="sm-button">Buy Now</a></div>
</td>

<td>
<img align="center" alt="" src="{image_path}images/icon10.png" style="border:1px solid #e7e7e7;outline:none;max-height:108px;" tabindex="0">
<div contenteditable="true" style="">
<div style="display:block; font-size:16px;color:#4a4949;margin:5px 0 0 0;">Earings</div>
<div style="display:block;font-size:13px;color:#4a4949;margin:5px 0 0 0; ">Under Rs 499</div>
</div>
<div contenteditable="true" style="text-align:center;">
<a href="#" style="border-radius:05px; background-color:#fe9e01;padding: 4% 15%; text-align:center; text-decoration:none; color:#ffffff; display:inline-block;font-size:14px;margin:5px 0 5px 0;" class="sm-button">Buy Now</a></div>
</td>

<td>
<img align="center" alt="" src="{image_path}images/icon11.png" style="border:1px solid #e7e7e7;outline:none;max-height:108px;" tabindex="0">
<div contenteditable="true" style="">
<div style="display:block; font-size:16px;color:#4a4949;margin:5px 0 0 0;">Bangles</div>
<div style="display:block;font-size:13px;color:#4a4949;margin:5px 0 0 0; ">Under Rs 499</div>
</div>
<div contenteditable="true" style="text-align:center;">
<a href="#" style="border-radius:05px; background-color:#fe9e01;padding: 4% 15%; text-align:center; text-decoration:none; color:#ffffff; display:inline-block;font-size:14px;margin:5px 0 5px 0;" class="sm-button">Buy Now</a></div>
</td>
</tr>

</tbody>
</table>


<tr>
<td align="center" valign="top">
<table border="0" cellpadding="0" cellspacing="0" width="600" style="border-collapse:collapse;max-width:600px;width:100%;">
<tbody>
<tr>
<div contenteditable="true">
<div style="display:block; font-size:13px; text-align:right;line-height:100%; color:#4a4949;" align="center;">*T & C Apply</div></div>
</tr>
</tbody>
</table>                                  
</td>
</tr>


<tr>
<td align="center" valign="top">
<table border="0" cellpadding="0" cellspacing="0" width="600" style="border-collapse:collapse;background:#00a2b5;max-width:600px;width:100%;margin:2% 0;">
<tbody>
<tr>
<td valign="top" style="padding:5px;">
</td>
</tr>
</tbody>
</table>                                  
</td>
</tr>


</td>
</tr>
</tbody></table>
			
</td>
</tr>
</tbody>
</table>


</div>',
						  'template_screenshot' => 'template2.jpg',
						  'member_id' => $mem_id,
						  'add_time' => 1470394007,
						  'ip' => '',
						  'status' => 'active'
					   ),
					array(
						  'id' => 12 ,
						  'template_type' => 'default' ,
						  'editor_type' => 'inline',
						  'template_name' => 'Template 12',
						  'template_text' =>'<div style="width:100%; max-width:600px; margin: auto;">


<table align="center" border="0" cellpadding="0" cellspacing="0" height="100%" width="100%" style="border-collapse:collapse;height:100%;margin:0;padding:0;background-color:#e9e9e9; width:100%;max-width:600px; margin:0 auto;">
<tbody>
<tr>
<td align="center" valign="top" style="height:100%;margin:0;padding:0;width:100%;border-top:0">

<table border="0" cellpadding="0" cellspacing="0" width="100%" style="border-collapse:collapse;width:100%;max-width:600px;">
<tbody>
<tr>
<td align="left" valign="top">

<table border="0" cellpadding="0" cellspacing="0" width="600" style="border-collapse:collapse; background:#fff;width:100%;max-width:600px;">
<tbody>
<tr>
<td valign="top" style="padding:17px 30px;text-align:left">
<img align="left" alt="" src="{image_path}images/logo12.png" style="padding-bottom:0;display:inline!important;vertical-align:bottom;border:0;height:auto;outline:none;text-decoration:none;
max-height:39px;"></td></tr></tbody></table>

<tr>
<td align="center" valign="top">
<table border="0" cellpadding="0" cellspacing="0" width="600" style="border-collapse:collapse;background:#eb775e;max-width:600px;width:100%;">
<tbody>
<tr>
<td valign="top" style="padding:2px;">
</td>
</tr>
</tbody>
</table>                                  
</td>
</tr>
                                    
</td>
</tr>

<tr>
<td align="center" valign="top">
                                    
<table border="0" cellpadding="0" cellspacing="0" width="100%" style="border-collapse:collapse;border-top:0;border-bottom:0;width:100%;max-width:600px;">
<tbody><tr><td align="center" valign="top">
                                                
<table border="0" cellpadding="0" cellspacing="0" width="600" style="border-collapse:collapse;width:100%;max-width:600px;">
<tbody><tr><td align="center" valign="top">

<table border="0" cellpadding="0" cellspacing="0" width="100%" style="border-collapse:collapse;background-color:#ffffff;border:0px solid #d5d5d5;width:100%;max-width:600px;">
<tbody><tr><td valign="top">


<table align="left" width="100%" border="0" cellpadding="0" cellspacing="0" style="min-width:100%;border-collapse:collapse;width:100%;max-width:600px;background-color:#fbf7eb;">
<tbody><tr>

<td valign="top">
<div style="background:#f36345;margin-top: 23%;margin-left:14%;padding:7% 0;margin-right:5%;" align="center">
<div contenteditable="true">
<div style="font-size:45px; color:#fff;">Get</div>
<div style="font-size:26px; color:#fff;">20% Discount</div>
<div style="font-size:19px;color:#fff;margin-top:1%;">This Summer</div>
</div>
</div>
</td>

<td valign="top" width="60%">
<img align="center" alt="" src="{image_path}images/temp12.png" style="max-width:360px;max-height:274px;padding-bottom:0;display:inline!important;vertical-align:bottom;border:0;height:auto;outline:none;text-decoration:none" tabindex="0"></td>
</tr>
</tbody></table>


<table border="0" cellpadding="15" cellspacing="0" style="width:100%;max-width:600px;min-width:100%;border-collapse:collapse; clear:both;" width="100%">
<tbody>
<tr>
<td>

<table border="0" cellpadding="15" cellspacing="0" style="width:100%;max-width:600px;min-width:100%;border-collapse:collapse; clear:both;" width="100%">
<tbody>
<tr>
<td>
<img align="center" alt="" src="{image_path}images/icon13.png" tabindex="0" style="max-width:214px;max-height:142px;">
</td>

<td>
<div contenteditable="true" style="padding:2%;">
<div style="font-size:16px;color:#f36345;">Your Beauty Starts Here</div>
<div style="font-size:13px;color:#4a4949;margin-top:2%;">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</div>
</div>
</td>
</tr>

</tbody>
</table>

<table border="0" cellpadding="15" cellspacing="0" style="width:100%;max-width:600px;min-width:100%;border-collapse:collapse; clear:both;" width="100%">
<tbody>
<tr>
<td>
<div contenteditable="true">
<div style="font-size:16px;color:#f36345;">Style Your Hair With Latest Hairstyles</div>
<div style="font-size:13px;color:#4a4949;margin-top:2%;">
<ul style="margin-left: -7%;">
<li>Fish Tail</li>
<li>Water Fall</li>
<li>French Bub</li>
</ul>
</div>
</div>
</td>
<td>
<img alt="" src="{image_path}images/icon14.png" tabindex="0" style="max-width:210px;max-height:140px;">
</td>
</tr>
</tbody>
</table>

<table border="0" cellpadding="15" cellspacing="0" style="width:100%;max-width:600px;min-width:100%;border-collapse:collapse; clear:both;" width="100%">
<tbody>
<tr>
<td>
<img alt="" src="{image_path}images/icon15.png" tabindex="0" style="max-width:214px;max-height:143px;">
</td>
<td>
<div contenteditable="true">
<div style="font-size:16px;color:#f36345;">Special Discount On Bridal Package</div>
<div style="font-size:13px;color:#4a4949;margin-top:2%;">
<ul  style="margin-left: -7%;">
<li>Water Proof Makeup</li>
<li>Every Type Of Hair Styles</li>
<li>Manicure & Pedicure</li>
<li>Body Polishing </li>  
</ul>
</div>
</div>
</td>
</tr>
</tbody>
</table>

</td>
</tr>
</tbody>
</table>


<table align="left" border="0" cellpadding="0" cellspacing="0" style="min-width:100%;border-collapse:collapse;  clear:both;width:100%;max-width:600px;" width="100%">
<tbody><tr>
<td valign="top" style="padding-top:0; padding-right:18px; padding-bottom:9px; padding-left:18px;">
<div contenteditable="true" style="text-align:center;">
<a href="#" style="border-radius:03px; background-color:#f36345; padding: 2.2% 23%; text-align:center; text-decoration:none; color:#ffffff; display:inline-block;font-size:19px;margin:30px 0 30px 0;" class="sm-button">Book Your Appointment Now</a></div>
</td></tr></tbody>
</table>
				
			


</td>
</tr>
</tbody></table>
</td>
</tr>
</tbody></table>
</td>
</tr>
</tbody></table>
</td>
</tr>

</tbody>
</table>
			
</td>
</tr>
</tbody>
</table>


</div>',
						  'template_screenshot' => 'template12.jpg',
						  'member_id' => $mem_id,
						  'add_time' => 1470394007,
						  'ip' => '',
						  'status' => 'active'
					   ),
					array(
						  'id' => 13 ,
						  'template_type' => 'default' ,
						  'editor_type' => 'inline',
						  'template_name' => 'Template 13',
						  'template_text' =>'<div style="width:100%; max-width:600px; margin: auto;">


<table align="center" border="0" cellpadding="0" cellspacing="0" height="100%" width="100%" style="border-collapse:collapse;height:100%;margin:0 auto;padding:0;width:100%;background-color:#e9e9e9;max-width:600px;">
<tbody>
<tr>
<td align="center" valign="top" style="height:100%;margin:0;padding:0;width:100%;border-top:0">

<table border="0" cellpadding="0" cellspacing="0" width="100%" style="border-collapse:collapse;max-width:600px;width:100%;">
<tbody>
<table border="0" cellpadding="0" cellspacing="0" width="600" style="border-collapse:collapse; background:#fff;max-width:600px;width:100%;">
<tbody>
<tr>
<td valign="top" style="padding:18px 0;text-align:center">
<img align="center" alt="" src="{image_path}images/logo13.png" style="padding-bottom:0;display:inline!important;vertical-align:bottom;border:0;width:105px;height:51px;outline:none;text-decoration:none">
</td>
</tr>

<tr>
<td>
<div contenteditable="true">
<div style="display:block; margin:0px 0 15px 0;font-size:15px;color:#050505;" align="center">Womens / Mens / Shoes / Denim</div>
</div>
</td>
</tr>

</tbody>
</table>




<table border="0" cellpadding="0" cellspacing="0" width="100%" style="border-collapse:collapse;background-color:#fff;border:1px solid #fff;max-width:600px;width:100%;">
<tbody>
<tr>
<td valign="top">

<table border="0" cellpadding="0" cellspacing="0" width="92%" style="border-collapse:collapse;margin:0 auto 25px auto;max-width:550px;width:100%;background: #fba5a7;
background: -moz-linear-gradient(left,  #fba5a7 1%, #f5aa98 49%, #ed948d 53%, #9dc3be 100%);
background: -webkit-linear-gradient(left,  #fba5a7 1%,#f5aa98 49%,#ed948d 53%,#9dc3be 100%);
background: linear-gradient(to right,  #fba5a7 1%,#f5aa98 49%,#ed948d 53%,#9dc3be 100%);
filter: progid:DXImageTransform.Microsoft.gradient( startColorstr="#fba5a7", endColorstr="#9dc3be",GradientType=1 );
">
<tbody>
<tr>
<td valign="top">
<table border="0" cellpadding="0" cellspacing="0" style="max-width:470px;width:100%;border-collapse:collapse;clear:both;margin:35px auto;background:rgba(255,255,255, 0.5);" width="100%;"><tbody><tr><td valign="top">


<tbody>
<tr>
<td valign="top">
<table border="0" cellpadding="0" cellspacing="0" style="max-width:440px;width:100%;border-collapse:collapse;clear:both;margin:15px auto;border:1px solid #e6e6e6;" width="100%;"><tbody><tr><td valign="top" style="">

<div contenteditable="true">
<div style="display:block; margin:15px 0 0 0; padding:0; font-size:50px; text-align:center;line-height:100%; color:#050505;font-weight:400;" align="center;">48 HOUR SALE!</div>
</div>

<div contenteditable="true">
<div style="display:block; margin:10px 0 0 0; padding:0; font-size: 23px; color:#050505;font-weight:300;" align="center">EXCLUSIVELY FOR YOU</div>
</div>

<div contenteditable="true"><div style="display:block; margin:10px 0 0px 0;font-size:23px;color:#ee562f;" align="center">ENDS TONIGHT</div></div>

<hr style="background:#e4d8d5; height:1px; border:0; margin:15px 11px 5px 11px;">

<div contenteditable="true"><div style="display:block; margin:20px 0 0px 0;font-size:31px;color:#050505;font-weight:400;" align="center"><b>15% OFF & 50 OR MORE</b></div></div>

<div contenteditable="true">
<div style="display:block; margin:5px 0 0px 0;font-size:17px;color:#9b857f;" align="center">USED CODE: JUST4U</div>
</div>

<div contenteditable="true" style="text-align:center;margin:30px 0 10px 0;">
<a href="#" style="border-radius:0px; background-color:#ee562f; padding:6px 18px;text-align:center;text-decoration:none; color:#ffffff; display:inline-block;font-size:17px;border:2px solid #ce401b;font-weight:300;margin-bottom:2%;" class="sm-button">SHOP WOMENS</a> &nbsp;
<a href="#" style="border-radius:0px; background-color:#ee562f; padding:6px 30px;text-align:center;text-decoration:none; color:#ffffff; display:inline-block;font-size:17px;border:2px solid #ce401b;font-weight:300;margin-bottom:2%;" class="sm-button">SHOP MENS</a>
</div>
</td>
</tr>
</tbody>
</table>
</td>
</tr>
</tbody></td></tr></tbody>
</table>
</td>
</tr>

</tbody>
</table>
</td>
</tr>

</tbody>
</table>


</td>
</tr>
</tbody>
</table>


</td>
</tr>
</tbody>
</table>


</div>',
						  'template_screenshot' => 'template8.jpg',
						  'member_id' => $mem_id,
						  'add_time' => 1470394007,
						  'ip' => '',
						  'status' => 'active'
					   ),
					array(
						  'id' => 14 ,
						  'template_type' => 'default' ,
						  'editor_type' => 'inline',
						  'template_name' => 'Template 14',
						  'template_text' =>'<div style="width:100%; max-width:600px; margin: auto;">


<table align="center" border="0" cellpadding="0" cellspacing="0" height="100%" width="100%" style="border-collapse:collapse;height:100%;margin:0;padding:0;background-color:#e9e9e9; width:100%;max-width:600px; margin:0 auto;">
<tbody>
<tr>
<td align="center" valign="top" style="height:100%;margin:0;padding:0;width:100%;border-top:0">

<table border="0" cellpadding="0" cellspacing="0" width="100%" style="border-collapse:collapse;width:100%;max-width:600px;">
<tbody>
<tr>
<td align="left" valign="top">

<table border="0" cellpadding="0" cellspacing="0" width="600" style="border-collapse:collapse; background:#fff;width:100%;max-width:600px;">
<tbody>
<tr>
<td valign="top" align="center">
<div contenteditable="true" style="text-align:center;">
<div style="display:block; padding:0; font-size:35px; color:#0085b1;margin:15px auto;font-weight:500;">Converting Your Dreams Into<br>Reality Is Our Passion</div>
</div></td></tr></tbody></table>
                                    
</td>
</tr>

<tr>
<td align="center" valign="top">
                                    
<table border="0" cellpadding="0" cellspacing="0" width="100%" style="border-collapse:collapse;border-top:0;border-bottom:0;width:100%;max-width:600px;">
<tbody><tr><td align="center" valign="top">
                                                
<table border="0" cellpadding="0" cellspacing="0" width="600" style="border-collapse:collapse;width:100%;max-width:600px;">
<tbody><tr><td align="center" valign="top">

<table border="0" cellpadding="0" cellspacing="0" width="100%" style="border-collapse:collapse;background-color:#ffffff;border:0px solid #d5d5d5;width:100%;max-width:600px;">
<tbody><tr><td valign="top">


<table align="center" width="100%" border="0" cellpadding="0" cellspacing="0" style="min-width:100%;border-collapse:collapse;width:100%;max-width:600px;background-color:#0085b1;">
<tbody><tr>

<td valign="top" align="center" style="padding-top:0; padding-right:18px; padding-bottom:9px; padding-left:18px; word-break:break-word;">
<div contenteditable="true">
<div style="display:block;font-size:18px; color:#ffffff;margin:25px 21px 25px 21px;font-weight:300;line-height:25px;">We constantly work with the sole objective of seeing you grow in every facet of business. Whether you are a newbie or an experienced business pro, we work to create a distinct entity for you from your competitors. Our dedicated team of experts are standing by your side always.</div>
</div>

<div contenteditable="true">
<div style="display:block; margin:15px 0 0 0; font-size:30px;color:#ffffff;" align="center">Checkout Our Exclusive<br>Business Oriented Services</div>
</div>

</td>
</tr>
</tbody></table>


<table border="0" cellpadding="15" cellspacing="0" style="width:100%;max-width:600px;min-width:100%;border-collapse:collapse; clear:both;text-align:center;background:#0085b1;" width="100%">
<tbody>
<tr>
<td>


<table border="0" cellpadding="15" cellspacing="0" style="width:100%;max-width:600px;min-width:100%;border-collapse:collapse; clear:both;text-align:center;background:#0085b1;" width="100%">
<tbody>

<tr>
<td>
<div style="background:#fff;border-radius:5px;padding:10% 0;">
<img alt="" src="{image_path}images/icon19.png" tabindex="0" style="border:1px solid #dbdbdb;border-radius:50%;width:82px;height:82px;margin:0 auto;max-width:82px;max-height:82px;">
<div contenteditable="true">
<div style="display:block; margin:15px 0 0px 0; font-size:16px;font-weight:400;color:#0085b1;">Email Marketing</div>
<div style="display:block;font-size:13px;color:#818080;font-weight:300;padding:5% 5% 5% 5%;">Lorem ipsum dolor sit amet, no quodsi definitiones vis.</div> 
</div>
</div>
</td>

<td>
<div style="background:#fff;border-radius:5px;padding:10% 0;">
<img alt="" src="{image_path}images/icon20.png" tabindex="0" style="border:1px solid #dbdbdb;border-radius:50%;width:82px;height:82px;margin:0 auto;max-width:82px;max-height:82px;">
<div contenteditable="true">
<div style="display:block; margin:15px 0 0px 0; font-size:16px;font-weight:400;color:#0085b1;">Content Creation</div>
<div style="display:block;font-size:13px;color:#818080;font-weight:300;padding:5% 5% 5% 5%;">Lorem ipsum dolor sit amet, no quodsi definitiones vis.</div>
</div>
</div>
</td>

<td>
<div style="background:#fff;border-radius:5px;padding:10% 0;">
<img alt="" src="{image_path}images/icon21.png" tabindex="0" style="border:1px solid #dbdbdb;border-radius:50%;width:82px;height:82px;margin:0 auto;max-width:82px;max-height:82px;">
<div contenteditable="true">
<div style="display:block; margin:15px 0 0px 0; font-size:16px;font-weight:400;color:#0085b1;">Data Analysis</div>
<div style="display:block;font-size:13px;color:#818080;font-weight:300;padding:5% 5% 5% 5%;">Lorem ipsum dolor sit amet, no quodsi definitiones vis.</div>
</div>
</div>
</td>
</tr>

</tbody></table>
</td>
</tr>
</tbody>
</table>

</td>
</tr>
</tbody>
</table>

<table border="0" cellpadding="15" cellspacing="0" style="width:100%;max-width:600px;min-width:100%;border-collapse:collapse; clear:both;text-align:center;background:#f1f1f1;" width="100%">
<tbody>

<tr><td>
<div contenteditable="true">
<div style="display:block;font-size:25px;color:#4a4949;margin:15px auto;">Our Satisfied Consumers</div>
</div>
</td></tr>
</tbody></table>


<table border="0" cellpadding="15" cellspacing="0" style="width:100%;max-width:600px;min-width:100%;border-collapse:collapse; clear:both;text-align:center;background:#f1f1f1;" width="100%">
<tbody>
<tr>
<td>

<table border="0" cellpadding="15" cellspacing="0" style="width:100%;max-width:600px;min-width:100%;border-collapse:collapse; clear:both;text-align:center;background:#f1f1f1;" width="100%">
<tbody>
<tr>
<td><img alt="" src="{image_path}images/icon16.png" tabindex="0" style="margin:0 auto;border:1px solid #62b3ce;border-radius:50%;background:#fff;width: 115px;height: 115px;"></td>
<td><img alt="" src="{image_path}images/icon17.png" tabindex="0" style="margin:0 auto;border:1px solid #62b3ce;border-radius:50%;background:#fff;width: 115px;height: 115px;"></td>
<td><img alt="" src="{image_path}images/icon18.png" tabindex="0" style="margin:0 auto;border:1px solid #62b3ce;border-radius:50%;background:#fff;width: 115px;height: 115px;"></td>
</tr>
</tbody>
</table>
</td>
</tr>
</tbody>
</table>

<table align="center" border="0" cellpadding="0" cellspacing="0" style="max-width:100%;min-width:100%;border-collapse:collapse; clear:both;width:100%;max-width:600px;background:#fff;" width="100%">
<tbody><tr>
<td valign="top" style="padding-top:0; padding-right:14px; padding-bottom:9px; padding-left:14px;">

<div contenteditable="true">
<div style="display:block;font-size:15px; color:#818080;margin:25px 40px 0 40px;font-weight:300;line-height:25px;text-align:center;">We don’t need to speak much about our work as our huge client base speaks volumes. Stop thinking now and contact us as you could be the next on this list of happy and satisfied businesses.</div>
</div>

<div contenteditable="true" style="text-align:center;">
<a href="#" style="border-radius:05px; background-color:#0085b1; padding: 2.5% 4%; text-align:center; text-decoration:none; color:#ffffff; display:inline-block;font-size:20px;margin:30px 0 30px 0;" class="sm-button">CONTACT OUR BUSINESS EXPERTS NOW</a></div>

</td>
</tr></tbody>
</table>
				


</td>
</tr>
</tbody></table>
</td>
</tr>
</tbody></table>
</td>
</tr>
</tbody></table>
</td>
</tr>

</tbody>
</table>


</div>',
						  'template_screenshot' => 'template14.jpg',
						  'member_id' => $mem_id,
						  'add_time' => 1470394007,
						  'ip' => '',
						  'status' => 'active'
					   )	
						
		);
		$this->db->insert_batch('tbl_template', $values); 
	}
	}
	
	
	function create_tables()
	{
		ini_set('max_execution_time', 60); 
		ini_set('memory_limit','2048M');
		$database=$this->input->post('database'); 
	 	$hostname=$this->input->post('hostname');
 		$username=$this->input->post('username');
	 	$password=$this->input->post('password');
		$mysqli = new mysqli($hostname,$username,$password,$database);
		$version = explode('.', PHP_VERSION);
		 
			if(mysqli_errno()){
			 return mysqli_errno();
			}
		
			
			$query1 = "CREATE TABLE IF NOT EXISTS `ci_sessions` (
					  `session_id` varchar(40) NOT NULL DEFAULT '0',
					  `ip_address` varchar(45) NOT NULL DEFAULT '0',
					  `user_agent` varchar(255) NOT NULL,
					  `last_activity` int(10) unsigned NOT NULL DEFAULT '0',
					  `user_data` longtext CHARACTER SET utf8 COLLATE utf8_bin NOT NULL
					) ENGINE=InnoDB DEFAULT CHARSET=latin1;";
			$mysqli->query($query1); 

			$query2 ="CREATE TABLE IF NOT EXISTS `tbl_autoresponder` (
					  `id` int(11) NOT NULL,
					  `member_id` int(11) NOT NULL,
					  `from_name` varchar(255) NOT NULL,
					  `from_email` varchar(255) NOT NULL,
					  `reply_to` varchar(255) NOT NULL,
					  `auto_name` varchar(255) NOT NULL,
					  `subject` varchar(255) NOT NULL,
					  `template_id` int(11) DEFAULT NULL,
					  `template_text` longtext CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
					  `image` varchar(255) DEFAULT NULL,
					  `in_campaign_id` varchar(255) NOT NULL,
					  `mail_day` int(11) DEFAULT NULL,
					  `enable_on` varchar(255) DEFAULT NULL,
					  `send_time` time DEFAULT NULL,
					  `status` enum('active','inactive') NOT NULL DEFAULT 'active',
					  `add_time` int(11) NOT NULL,
					  `ip` varchar(255) NOT NULL
					) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=latin1;";
			$mysqli->query($query2);
			
			

			$query3 = "CREATE TABLE IF NOT EXISTS `tbl_campaign` (
					  `campaign_id` bigint(20) NOT NULL,
					  `campaign_name` varchar(255) NOT NULL,
					  `campaign_description` text,
					  `member_id` bigint(20) NOT NULL,
					  `add_time` int(11) NOT NULL,
					  `ip` varchar(255) NOT NULL,
					  `status` enum('active','inactive') NOT NULL DEFAULT 'active'
					) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;";
			$mysqli->query($query3);
			
				$query4 ="CREATE TABLE IF NOT EXISTS `tbl_contact` (
						  `id` int(11) NOT NULL,
						  `member_id` int(11) NOT NULL,
						  `name` varchar(255) DEFAULT NULL,
						  `email` varchar(255) NOT NULL,
						  `campaign_id` int(11) NOT NULL,
						  `status` enum('active','inactive') NOT NULL DEFAULT 'active',
						  `ip` varchar(255) NOT NULL,
						  `add_time` int(11) NOT NULL,
						  `address` text,
						  `city` varchar(255) DEFAULT NULL,
						  `state` varchar(255) DEFAULT NULL,
						  `country` varchar(255) DEFAULT NULL,
						  `zip` varchar(255) DEFAULT NULL,
						  `phone` varchar(255) DEFAULT NULL,
						  `custom_1` varchar(255) DEFAULT NULL,
						  `custom_2` varchar(255) DEFAULT NULL,
						  `custom_3` varchar(255) DEFAULT NULL,
						  `add_from` enum('import','add','form') NOT NULL DEFAULT 'import',
						  `varification_code` varchar(255) DEFAULT NULL,
						  `autoresponder_sent_time` int(11) DEFAULT NULL
						) ENGINE=InnoDB AUTO_INCREMENT=5900 DEFAULT CHARSET=latin1;";
				$mysqli->query($query4);
						
				$query5 = "CREATE TABLE IF NOT EXISTS `tbl_cron_urls` (
						  `id` int(11) NOT NULL,
						  `time` datetime NOT NULL,
						  `newsletter_id` int(11) NOT NULL,
						  `url` varchar(255) NOT NULL
						) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;";		
				$mysqli->query($query5);
		
				
				$query6 = "CREATE TABLE IF NOT EXISTS `tbl_email_queue` (
						  `id` int(11) NOT NULL,
						  `newsletter_id` int(11) NOT NULL,
						  `contact_id` int(11) NOT NULL,
						  `cron_id` varchar(255) DEFAULT NULL
						) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=latin1;";		
				$mysqli->query($query6);
		
		
		
				$query7 = "CREATE TABLE IF NOT EXISTS `tbl_file_attatch` (
					  `id` int(11) NOT NULL,
					  `file_name` varchar(255) NOT NULL,
					  `newsletter_id` int(11) NOT NULL,
					  `type` enum('newsletter','autoresponder') NOT NULL DEFAULT 'newsletter'
					) ENGINE=InnoDB DEFAULT CHARSET=latin1;";		
				$mysqli->query($query7);
		
				$query8 = "CREATE TABLE IF NOT EXISTS `tbl_form_template` (
					  `id` int(11) NOT NULL,
					  `template_name` varchar(255) DEFAULT NULL,
					  `template_text` longtext CHARACTER SET utf8 COLLATE utf8_bin,
					  `member_id` int(11) DEFAULT NULL,
					  `add_time` int(11) DEFAULT NULL,
					  `ip` varchar(255) NOT NULL,
					  `status` enum('active','inactive') NOT NULL DEFAULT 'active'
					) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=latin1;";		
				$mysqli->query($query8);
		
		
		
				$query9 = "CREATE TABLE IF NOT EXISTS `tbl_from_email` (
						  `id` int(11) NOT NULL,
						  `member_id` int(11) NOT NULL,
						  `email` varchar(255) NOT NULL,
						  `status` enum('varify','unvarify') NOT NULL DEFAULT 'unvarify',
						  `verify_code` varchar(255) DEFAULT NULL,
						  `add_time` int(11) NOT NULL
						) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;";
				$mysqli->query($query9);
		
		
		
				$query10 = "CREATE TABLE IF NOT EXISTS `tbl_member` (
						  `mem_id` bigint(20) NOT NULL,
						  `email` varchar(255) NOT NULL,
						  `password` varchar(255) NOT NULL,
						  `name` varchar(255) DEFAULT NULL,
						  `city` text,
						  `state` varchar(250) DEFAULT NULL,
						  `country` varchar(250) DEFAULT NULL,
						  `address` text,
						  `pin_code` varchar(250) DEFAULT NULL,
						  `business_name` varchar(255) DEFAULT NULL,
						  `profile_pic` varchar(200) DEFAULT NULL,
						  `forget_pass_code` varchar(255) DEFAULT NULL,
						  `forget_pass_exptime` int(11) DEFAULT NULL,
						  `status` enum('active','inactive') NOT NULL DEFAULT 'active',
						  `add_time` int(11) DEFAULT NULL
						) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;";
				$mysqli->query($query10);
		
		
		
				$query11 = "CREATE TABLE IF NOT EXISTS `tbl_newsletter` (
						  `id` int(11) NOT NULL,
						  `member_id` int(11) NOT NULL,
						  `message_type` enum('schedule','instant') DEFAULT NULL,
						  `newsletter_type` enum('newsletter','autoresponder') NOT NULL DEFAULT 'newsletter',
						  `from_name` varchar(255) DEFAULT NULL,
						  `from_email` varchar(255) DEFAULT NULL,
						  `reply_to` varchar(255) DEFAULT NULL,
						  `message_name` varchar(255) NOT NULL,
						  `subject` varchar(255) NOT NULL,
						  `template_id` int(11) DEFAULT NULL,
						  `template_text` longtext CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
						  `in_campaign_id` text NOT NULL,
						  `ex_campaign_id` text,
						  `sep_list_id` text,
						  `contact_id` longtext,
						  `image` varchar(255) DEFAULT NULL,
						  `schedule_date` datetime DEFAULT NULL,
						  `schedule_timestamp` int(11) DEFAULT NULL,
						  `status` enum('sent','scheduled','process','draft') DEFAULT NULL,
						  `process_stage` enum('complete','inprocess') NOT NULL DEFAULT 'inprocess',
						  `last_procees_id` int(11) DEFAULT NULL,
						  `add_time` int(11) NOT NULL,
						  `sent_time` int(11) DEFAULT NULL,
						  `ip` varchar(255) NOT NULL
						) ENGINE=InnoDB AUTO_INCREMENT=29 DEFAULT CHARSET=latin1;";
				$mysqli->query($query11);
		
		
		
				$query12 = "CREATE TABLE IF NOT EXISTS `tbl_statistics` (
				  `id` int(11) NOT NULL,
				  `type` enum('newsletter','autoresponder') NOT NULL DEFAULT 'newsletter',
				  `newsletter_id` int(11) NOT NULL,
				  `contact_id` int(11) NOT NULL,
				  `email` varchar(255) NOT NULL,
				  `open` enum('yes','no') NOT NULL DEFAULT 'no',
				  `click` enum('yes','no') NOT NULL DEFAULT 'no',
				  `unsubscribed` enum('yes','no') NOT NULL DEFAULT 'no',
				  `bounced` enum('yes','no') NOT NULL DEFAULT 'no',
				  `complaints` enum('yes','no') NOT NULL DEFAULT 'no',
				  `add_time` int(11) NOT NULL,
				  `ip` varchar(255) DEFAULT NULL,
				  `browser` varchar(255) DEFAULT NULL,
				  `device` enum('mobile','desktop') DEFAULT NULL,
				  `country_code` varchar(255) DEFAULT NULL,
				  `open_time` int(11) DEFAULT NULL,
				  `click_time` int(11) DEFAULT NULL
				) ENGINE=InnoDB AUTO_INCREMENT=90 DEFAULT CHARSET=latin1;";
				$mysqli->query($query12);
		
		
		
		
				$query13 = "CREATE TABLE IF NOT EXISTS `tbl_suppression` (
					  `id` int(11) NOT NULL,
					  `member_id` int(11) NOT NULL,
					  `email` varchar(255) NOT NULL,
					  `list_id` int(11) NOT NULL,
					  `status` enum('active','inactive') NOT NULL DEFAULT 'active',
					  `ip` varchar(255) NOT NULL,
					  `add_time` int(11) NOT NULL
					) ENGINE=InnoDB DEFAULT CHARSET=latin1;";			
				$mysqli->query($query13);
		
		
		
		
		
				$query14 = "CREATE TABLE IF NOT EXISTS `tbl_suppression_list` (
						  `id` bigint(20) NOT NULL,
						  `list_name` varchar(255) NOT NULL,
						  `member_id` bigint(20) NOT NULL,
						  `add_time` int(11) NOT NULL,
						  `ip` varchar(255) NOT NULL,
						  `status` enum('active','inactive') NOT NULL DEFAULT 'active'
						) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;";
		
				$mysqli->query($query14);
		
		
		
				$query15 = "CREATE TABLE IF NOT EXISTS `tbl_template` (
				  `id` bigint(20) NOT NULL,
				  `template_type` enum('default','draft') NOT NULL,
				  `editor_type` enum('inline','plain') NOT NULL DEFAULT 'inline',
				  `template_name` varchar(255) NOT NULL,
				  `template_text` longtext CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
				  `template_screenshot` varchar(255) DEFAULT NULL,
				  `member_id` bigint(20) NOT NULL,
				  `add_time` int(11) NOT NULL,
				  `ip` varchar(255) NOT NULL,
				  `status` enum('active','inactive') NOT NULL DEFAULT 'active'
				) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=latin1;";	
				$mysqli->query($query15);
		
		
		
				$query16 = "CREATE TABLE IF NOT EXISTS `tbl_unsubscribe_list` (
						  `id` int(11) NOT NULL,
						  `member_id` int(11) NOT NULL,
						  `contact_id` int(11) NOT NULL,
						  `email` varchar(255) NOT NULL,
						  `ip` varchar(255) NOT NULL,
						  `add_time` int(11) NOT NULL
						) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;";				
				$mysqli->query($query16);
		
		
		
				$query17 = "CREATE TABLE IF NOT EXISTS `tbl_webform` (
				  `id` int(11) NOT NULL,
				  `member_id` int(11) NOT NULL,
				  `form_name` varchar(255) NOT NULL,
				  `campaign_ids` varchar(255) NOT NULL,
				  `template_id` int(11) DEFAULT NULL,
				  `form_html` longtext NOT NULL,
				  `status` enum('active','inactive') NOT NULL DEFAULT 'active',
				  `add_time` int(11) NOT NULL,
				  `thankyou_page` enum('default','custom') NOT NULL DEFAULT 'default',
				  `thankyou_custom` text,
				  `already_subscribe` enum('default','custom') NOT NULL DEFAULT 'default',
				  `alreadysub_custom` text,
				  `double_otp` enum('yes','no') NOT NULL DEFAULT 'no',
				  `image` varchar(255) DEFAULT NULL
				) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;";				
				$mysqli->query($query17);
		
		
		
		
		
		
				$query18 = "CREATE TABLE IF NOT EXISTS `tbl_website_settings` (
				  `id` int(11) NOT NULL,
				  `member_id` int(11) NOT NULL,
				  `footer_text` longtext NOT NULL,
				  `application_URL` text NOT NULL,
				  `database_name` varchar(250) NOT NULL,
				  `license_key` text,
				  `mail_type` varchar(255) NOT NULL DEFAULT 'default',
				  `SMTP_hostname` text,
				  `SMTP_username` text,
				  `SMTP_password` text,
				  `SMTP_port` text,
				  `bounce_type` enum('default','custom') DEFAULT 'default',
				  `bounce_address` text,
				  `bounce_server` text,
				  `bounce_password` text,
				  `bounce_port` int(11) DEFAULT NULL,
				  `account_type` text,
				  `fb_url` varchar(255) DEFAULT NULL,
				  `tw_url` varchar(255) DEFAULT NULL,
				  `insta_url` varchar(255) DEFAULT NULL,
				  `gplus_url` varchar(255) DEFAULT NULL,
				  `last_mail_time` int(11) DEFAULT NULL,
				  `current_hour_mail` int(11) DEFAULT NULL,
				  `mail_hour_limit` int(11) DEFAULT NULL,
				  `mail_minute_limit` int(11) DEFAULT '3'
				) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;";
				$mysqli->query($query18);

				$query19  = "CREATE TABLE IF NOT EXISTS `tbl_external_mail_api` (
					  `id` int(11) NOT NULL,
					  `api_name` varchar(200) NOT NULL,
					  `api_value` text NOT NULL
					) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;";
				$mysqli->query($query19);	
				
		
				//Add Primary key
				$query1 = "ALTER TABLE `ci_sessions` ADD PRIMARY KEY (`session_id`), ADD KEY `last_activity_idx` (`last_activity`);";
				$mysqli->query($query1);
					
				$query2 = "ALTER TABLE `tbl_autoresponder` ADD PRIMARY KEY (`id`);";
				$mysqli->query($query2);

				$query3 = "ALTER TABLE `tbl_campaign` ADD PRIMARY KEY (`campaign_id`);";
				$mysqli->query($query3);

				$query4 = "ALTER TABLE `tbl_contact` ADD PRIMARY KEY (`id`);";
				$mysqli->query($query4);

				$query5 = "ALTER TABLE `tbl_cron_urls` ADD PRIMARY KEY (`id`);";
				$mysqli->query($query5);

				$query6 = "ALTER TABLE `tbl_email_queue` ADD PRIMARY KEY (`id`);";
				$mysqli->query($query6);

				$query7 = "ALTER TABLE `tbl_file_attatch` ADD PRIMARY KEY (`id`);";
				$mysqli->query($query7);

				$query8 = "ALTER TABLE `tbl_form_template` ADD PRIMARY KEY (`id`);";
				$mysqli->query($query8);

				$query9 = "ALTER TABLE `tbl_from_email` ADD PRIMARY KEY (`id`);";
				$mysqli->query($query9);

				$query10 = "ALTER TABLE `tbl_member` ADD PRIMARY KEY (`mem_id`);";
				$mysqli->query($query10);

				$query11 = "ALTER TABLE `tbl_newsletter` ADD PRIMARY KEY (`id`);";
				$mysqli->query($query11);

				$query12 = "ALTER TABLE `tbl_statistics` ADD PRIMARY KEY (`id`);";
				$mysqli->query($query12);

				$query13 = "ALTER TABLE `tbl_suppression` ADD PRIMARY KEY (`id`);";
				$mysqli->query($query13);

				$query14 = "ALTER TABLE `tbl_suppression_list` ADD PRIMARY KEY (`id`);";
				$mysqli->query($query14);
				
				$query15 = "ALTER TABLE `tbl_template` ADD PRIMARY KEY (`id`);";
				$mysqli->query($query15);
				 
				$query16 = "ALTER TABLE `tbl_unsubscribe_list` ADD PRIMARY KEY (`id`);";
				$mysqli->query($query16);

				$query17 = "ALTER TABLE `tbl_webform` ADD PRIMARY KEY (`id`);";
				$mysqli->query($query17);

				$query18 = "ALTER TABLE `tbl_website_settings` ADD PRIMARY KEY (`id`);";
				$mysqli->query($query18);
				
				//modify auto increment
				$query1 = "ALTER TABLE `tbl_autoresponder` MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;";
				$mysqli->query($query1);
					
				$query2 = "ALTER TABLE `tbl_campaign` MODIFY `campaign_id` bigint(20) NOT NULL AUTO_INCREMENT;";
				$mysqli->query($query2);

				$query3 = "ALTER TABLE `tbl_contact` MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;";
				$mysqli->query($query3);

				$query4 = "ALTER TABLE `tbl_cron_urls` MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;";
				$mysqli->query($query4);

				$query5 = "ALTER TABLE `tbl_email_queue` MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;";
				$mysqli->query($query5);

				$query6 = "ALTER TABLE `tbl_file_attatch` MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;";
				$mysqli->query($query6);

				$query7 = "ALTER TABLE `tbl_form_template` MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=11;";
				$mysqli->query($query7);

				$query8 = "ALTER TABLE `tbl_from_email` MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;";
				$mysqli->query($query8);

				$query9 = "ALTER TABLE `tbl_member` MODIFY `mem_id` bigint(20) NOT NULL AUTO_INCREMENT;";
				$mysqli->query($query9);

				$query10 = "ALTER TABLE `tbl_newsletter` MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;";
				$mysqli->query($query10);

				$query11 = "ALTER TABLE `tbl_statistics` MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;";
				$mysqli->query($query11);

				$query12 = "ALTER TABLE `tbl_suppression` MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;";
				$mysqli->query($query12);

				$query13 = "ALTER TABLE `tbl_suppression_list` MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;";
				$mysqli->query($query13);

				$query15 = "ALTER TABLE `tbl_unsubscribe_list` MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;";
				$mysqli->query($query15);

				$query16 = "ALTER TABLE `tbl_webform` MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;";
				$mysqli->query($query16);
				
				$query17 = "ALTER TABLE `tbl_website_settings` MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;";
				$mysqli->query($query17);
				
				
				$query18 = "ALTER TABLE `tbl_external_mail_api` ADD PRIMARY KEY (`id`);";
				$mysqli->query($query18);
				
				$query19 ="ALTER TABLE `tbl_external_mail_api` MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;";
				$mysqli->query($query19);
				
				$query15 = "ALTER TABLE `tbl_template` MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;";
				$mysqli->query($query15);
		 	 	 
	}
	function configSessionUpdate()
	{
		
		$template_path 	= './installation/include/config.php';
		$output_path 	= './application/config/config.php';
		$config_file = file_get_contents($template_path);
		$new  = str_replace("%BASEURL%",site_url(),$config_file);
	
		$handle = fopen($output_path,'w+');

	
		@chmod($output_path,0777);

		
		if(is_writable($output_path)) {

			
			if(fwrite($handle,$new)) {
				return true;
			} else {
				return false;
			}

		} else {
			return false;
		}
	}

	function databaseConnection()
	{
	 
		if($this->db->hostname && $this->db->username && $this->db->database){
			$mysqli = new mysqli($this->db->hostname,$this->db->username,$this->db->password,$this->db->database);
			if(!mysqli_connect_error())
			{
			
				$arr =array();
				$get =$this->db->query('SHOW TABLES');
				$result = $get->result();
				if($result){
					$tab = 'Tables_in_'.$this->db->database;
					foreach($result as $res){
						 
						$arr[] = $res->$tab;
						 
					}
				}
				if(in_array('ci_sessions',$arr) && in_array('tbl_autoresponder',$arr) && in_array('tbl_campaign',$arr)&& in_array('tbl_contact',$arr)&& in_array('tbl_cron_urls',$arr)&& in_array('tbl_email_queue',$arr)&& in_array('tbl_file_attatch',$arr)&& in_array('tbl_form_template',$arr)&& in_array('tbl_from_email',$arr)&& in_array('tbl_member',$arr)&& in_array('tbl_newsletter',$arr)&& in_array('tbl_statistics',$arr)&& in_array('tbl_suppression',$arr)&& in_array('tbl_suppression_list',$arr)&& in_array('tbl_template',$arr)&& in_array('tbl_unsubscribe_list',$arr)&& in_array('tbl_webform',$arr)&& in_array('tbl_website_settings',$arr))
				{
					$this->db->select('mem_id');
					$get =$this->db->get('tbl_member');
					if($get->num_rows()==0)
					{
						$this->session->set_userdata('step1','complete');
						$this->session->set_userdata('step2','complete');
						redirect(site_url().'install/step3');
					}
					else
					{
						redirect(site_url().'install/already');
					}
				}
				else
				{
					$this->session->unset_userdata('step1'); 
				}				
			}
			else
			{
				$this->session->unset_userdata('step2');
			}
		}
		else
		{
			$this->session->unset_userdata('step2');
		}
		
	}	
 }