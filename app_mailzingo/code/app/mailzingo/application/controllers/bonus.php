<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Bonus extends CI_Controller {

	function __construct()
	{ 
	    parent::__construct();				
		$this->checkLogin();
		
	}
	function checkLogin()
	{
	  if($this->session->userdata('mem_id')=='')
	  { redirect('login'); }
	  
	}
	function index()
	{
		 
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $this->config->item('membersarea_url').'bonus/api_index');
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		$output = curl_exec($ch);
		curl_close($ch);
		$output = json_decode($output);
		$this->load->view($this->config->item('templateName').'/header',$output);
		$this->load->view($this->config->item('templateName').'/bonus');
		$this->load->view($this->config->item('templateName').'/footer');
	}
}