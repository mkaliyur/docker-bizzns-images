<?php
defined('BASEPATH') OR exit('No direct script access allowed');
include('app.php');
ini_set('max_execution_time', 1000);
ini_set('memory_limit', '-1');
class Contact extends App {

	function __construct()
	{ 
	    parent::__construct();			
		$this->load->model('contact_model');
		$this->load->model('campaign_model');
		$this->checkLogin();
		$this->member_id = $this->session->userdata('mem_id');
        include 'phpexcel/PHPExcel/IOFactory.php';
		
	}
	function checkLogin()
	{
	  if($this->session->userdata('mem_id')=='')
	  { redirect('login'); }
	}
	public function index()
	{
	 $output['meta_title'] = "Subscriber Management";	
      if($_GET)
		{
		  
		 if($this->input->get('submittype')!='search')
		  {
		   $this->exportAllContact();
		  }
		  
		  if($this->search_subscriber!='yes')
		   {
		     $this->session->set_userdata('error_msg','Please upgrade to use search contact feature. <a target="_blank" href="'.$this->upgrade_link.'" class="upgradelink">Click here to upgrade </a>');
	         redirect('contact-list');
		   }
		  $output['campaign_id'] = $campaign_id = $this->input->get('campaign_id');
		  $output['subscribe_type'] = $subscribe_type = $this->input->get('subscribe_type');
		  $output['message_type'] = $message_type = $this->input->get('message_type');
		  if($message_type=='newsletter')
		  {
		    $output['newsletter_id'] = $newsletter_id = $this->input->get('newsletter_id');
		    $output['stat_type'] = $stat_type = $this->input->get('stat_type');
		  }
		  $output['date_time'] = $date_time = $this->input->get('date_time');
		  if($date_time=='custom')
		  {
		     $output['from_date'] = $from_date = $this->input->get('from_date');
		     $output['to_date'] = $to_date = $this->input->get('to_date');
		  }
		  $output['keyword'] = $keyword = $this->input->get('keyword');
		}
		
		if($message_type=='autoresponder')
		 { $message_type = 'autoresponder'; }
		else 
		 { $message_type = 'newsletter'; }
		$output['total_data']= $this->contact_model->getContactRecord($this->member_id,$campaign_id,$subscribe_type,$message_type,$newsletter_id,$stat_type,$date_time,$from_date,$to_date,$keyword,$start,$content_per_page,1);
		
		$output['campaign_list'] = $this->campaign_model->getCampaignList($this->member_id,null,null,null);
		$output['newsletter_list'] = $this->contact_model->getNewsletterListByType($message_type);
		$this->load->view($this->config->item('templateName').'/header',$output);
		$this->load->view($this->config->item('templateName').'/contact/list');
		$this->load->view($this->config->item('templateName').'/footer');
	}
    public function load_more()
    { 
        $group_no = $this->input->post('group_no');
        $campaign_id = $this->input->post('campaign_id');
        $subscribe_type = $this->input->post('subscribe_type');
        $message_type = $this->input->post('message_type');
        $newsletter_id = $this->input->post('newsletter_id');
        $stat_type = $this->input->post('stat_type');
        $date_time = $this->input->post('date_time');
        $from_date = $this->input->post('from_date');
        $to_date = $this->input->post('to_date');
        $keyword = $this->input->post('keyword');
        $content_per_page = 10;
        $start = ceil($group_no * $content_per_page);
        $output['allRecord'] = $this->contact_model->getContactRecord($this->member_id,$campaign_id,$subscribe_type,$message_type,$newsletter_id,$stat_type,$date_time,$from_date,$to_date,$keyword,$start,$content_per_page,null);
        
        echo $this->load->view($this->config->item('templateName').'/contact/contact-pagination',$output,true);
    }
	public function addContact()
	{
	 $output['meta_title'] = "Add Subscribers";		
	  $output['add_type'] = $add_type = $this->input->get('add_type');
	  if($add_type == 'import')
	  {
		$output['meta_title'] = "Add Subscribers Upload";		  
	  }
	  $output['step'] = $step = $this->input->get('step');
      $this->load->library('csvimport');
	if($add_type=='import' && $this->import_subscriber!='yes')
	 { 
		
	   $this->session->set_userdata('error_msg','Please upgrade to use import contact feature. <a target="_blank" href="'.$this->upgrade_link.'" class="upgradelink">Click here to upgrade </a>');
	   redirect('add-contact');
	 }
	if($add_type=='single' && $this->add_one_at_time_subscriber!='yes')
	 { 
	   $this->session->set_userdata('error_msg','Please upgrade to use Single Contact Add feature. <a target="_blank" href="'.$this->upgrade_link.'" class="upgradelink">Click here to upgrade </a>');
	   redirect('add-contact');
	 }
	if($add_type=='copy' && $this->copy_paste_subscriber!='yes')
	 { 
	   $this->session->set_userdata('error_msg','Please upgrade to use add contact using copy paste feature. <a target="_blank" href="'.$this->upgrade_link.'" class="upgradelink">Click here to upgrade </a>');
	   redirect('add-contact');
	 }
	/*------------ csv upload function step 1 (setting) starts here---------------------------*/
	  if($_POST['submit_1'])
	   { 
		   if($_FILES['csv_file']['name']=='')
		   $this->form_validation->set_rules('csv_file','Choose file','trim|required');
		   $this->form_validation->set_rules('permission','Permission','trim|required');
		   
		   if($this->form_validation->run())
		   {
				unlink('test123.csv');
				$config['upload_path'] = './assets/uploads/csv/';
				$config['allowed_types'] = 'csv|xls';
		
				$this->load->library('upload', $config);
			   // If upload failed, display error
                                
                                
				if (!$this->upload->do_upload('csv_file')) {
					$output['error'] = $this->upload->display_errors(); 
				} else {
					
					$file_data = $this->upload->data();
					$file_path =  './assets/uploads/csv/'.$file_data['file_name'];
					 
					$extension = end(explode('.', $file_data['file_name']));
                        if ($extension == 'xlsx') {
						$excel = PHPExcel_IOFactory::load($file_path);
                        $writer = PHPExcel_IOFactory::createWriter($excel, 'CSV');
                        $writer->save("test123.csv");
                        $file_path = 'test123.csv';
					}



                    if ($extension == 'xls') {
                        $inputFileType = 'Excel5';
                        $inputFileName = $file_path;

                        $objReader = PHPExcel_IOFactory::createReader($inputFileType);
						$objReader->setReadDataOnly(true);
                        $objPHPExcelReader = $objReader->load($inputFileName);

                        $loadedSheetNames = $objPHPExcelReader->getSheetNames();

                        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcelReader, 'CSV');

                        foreach ($loadedSheetNames as $sheetIndex => $loadedSheetName) {
                            $objWriter->setSheetIndex($sheetIndex);
                            $objWriter->save('test123.csv');
                            $file_path = 'test123.csv';
                        }
                    }



                    if ($this->csvimport->get_header($file_path)) {
						$csv_array = $this->csvimport->get_header($file_path);
						$output['importData'][0] = $csv_array;
						$output['file_path'] = $file_path; //add file path in hidden variable
					} else {
						$output['error'] = "File you have uploaded have blank fields.";
					}
				 
				  }
				$output['meta_title'] = "Add Subscribers Mapping"; 
			}
	   }
	 /*------------ csv upload function step 1 (setting) ends here---------------------------*/
	 
	/*------------ csv mapping function step 2 (mapping) starts here---------------------------*/
	 if($_POST['submit_2'])
	   {
			   
		 $this->form_validation->set_rules('field[]','Field','trim|required|callback_validateMapping');
		 if($this->form_validation->run())
		   {
			 $postArray = $_POST;
			 $output['keyArray'] = $postArray['field'];
			 $output['importArray'] = array("ok");
			 $output['campaign_list'] = $this->campaign_model->getCampaignList($this->member_id);
		  } else  {
		     $output['field'] = $field = $this->input->post('field');
		     $output['importData'] = json_decode($this->input->post('importData'));
		  }
		  $output['file_path'] = $_POST['file_path'];  //again filepath in hidden variable
		  
	   }	
	/*------------ csv mapping function step 2 (mapping) ends here---------------------------*/

	/*------------ choose campaign function step 3 (choose campaign) starts here---------------------------*/
	  if($_POST['submit_3'])
	   {
		 $this->load->model("csvupload_model");  
	     $this->form_validation->set_rules('select_checkbox[]','Campaign','trim|required');
	     $this->form_validation->set_rules('file_path','File Path','trim|required|callback_validateContactCount');
		 
		if($this->form_validation->run())
		   {
			 $keyArray = $this->input->post('field');
			 $file_path = $_POST['file_path'];
			 $csv_array = $this->csvimport->get_header($file_path);
			 
			 foreach($csv_array as $ke=>$v)
			 {
				 $new[$v] = $keyArray[$ke];
			 }
			  
			 $campaign_id = $this->input->post('select_checkbox');
			
			 $this->csvupload_model->update_campaign($keyArray,$this->member_id,$file_path,$campaign_id); //add subscriber 
			  
			$this->session->set_userdata('success_msg','Import Done Successfully');
			redirect('add-contact');
		  } 
		  else {
		    $output['importArray'] = "Not";
			$output['file_path'] = $this->input->post('file_path');
			$output['campaign_list'] = $this->campaign_model->getCampaignList($this->member_id);
		  }
		  
	   }
	/*------------ choose campaign function step 3 (choose campaign) ends here---------------------------*/
		
	/*------------ copy paste function step 1 (settings) starts here---------------------------*/
	  if($_POST['submit_4'])
	   {
	     $this->form_validation->set_rules('emails','Email IDs','trim|required|callback_validateEmails');
	     $this->form_validation->set_rules('permission','Permission','trim|required');
          
		 $output['emails'] = $this->input->post('emails');
		  if($this->form_validation->run())
		   {
		     $output['emailList'] = $this->input->post('emails');
			 $output['campaign_list'] = $this->campaign_model->getCampaignList($this->member_id);
		   }
	   }
	/*------------ copy paste function step 1 (settings) ends here---------------------------*/
	
	/*------------ copy paste function step 2 (choose campaign) starts here---------------------------*/
	  if($_POST['submit_5'])
	   {
	     $this->form_validation->set_rules('select_checkbox[]','Choose Campaign','trim|required');
          $campaign_id = $this->input->post('select_checkbox');
		  if($this->form_validation->run())
		   {
		     $importArray = explode(',',$this->input->post('emailList'));
			 foreach($importArray as $key=>$valAdd)
			  {
				if(!empty(trim($valAdd)))
				$this->contact_model->addCopyContact($this->member_id,$valAdd,$campaign_id);
			  }
			  $this->session->set_userdata('success_msg','Contact Added Successfully');
		       redirect('add-contact');
		   } else {
		    $output['emailList'] = $this->input->post('emailList');
			$output['campaign_list'] = $this->campaign_model->getCampaignList($this->member_id);
		   }
	   }
	/*------------ copy paste function step 2 (choose campaign) ends here---------------------------*/
	
	/*------------ single select function step 1 (setting) starts here---------------------------*/
	  if($_POST['submit_6'])
	   {
	     $this->form_validation->set_rules('email','Email','trim|required|valid_email|callback_validateContactCountSingle');
	     $this->form_validation->set_rules('permission','Permission','trim|required');
		 $this->form_validation->set_rules('fieldname[]','Field Name','trim|callback_validateFields');

		 $name = $this->input->post('name');
		 $email = $this->input->post('email');
		 foreach($_POST['fieldname'] as $key=>$val)
			{
			  $$val = $_POST['fieldvalue'][$key];
			}

		  if($this->form_validation->run())
		   {
		     $output['postArray'] = json_encode($_POST);
			 $output['campaign_list'] = $this->campaign_model->getCampaignList($this->member_id);
		   }
		   
		$output['name'] = $name;
		$output['email'] = $email;
		if($address)
		$customFieldArray['address'] = $address;
		if($city)
		$customFieldArray['city'] = $city;
		if($state)
		$customFieldArray['state'] = $state;
		if($country)
		$customFieldArray['country'] = $country;
		if($zip)
		$customFieldArray['zip'] = $zip;
		if($phone)
		$customFieldArray['phone'] = $phone;
		if($custom_1)
		$customFieldArray['custom_1'] = $custom_1;
		if($custom_2)
		$customFieldArray['custom_2'] = $custom_2;
		if($custom_3)
		$customFieldArray['custom_3'] = $custom_3;
		
		$output['customFieldArray'] = $customFieldArray;

	   }
	/*------------ single select function step 1 (setting) ends here---------------------------*/
		
	/*------------ single select function step 2 (choose campaign) starts here---------------------------*/
	  if($_POST['submit_7'])
	   {
	     $this->form_validation->set_rules('select_checkbox[]','Choose Campaign','trim|required');
	     $this->form_validation->set_rules('postArray','email','trim|required');
		 $output['postArray'] = $this->input->post('postArray');
		 $output['campaign_list'] = $this->campaign_model->getCampaignList($this->member_id);

		  if($this->form_validation->run())
		   {
			 $postArray = (array)json_decode($this->input->post('postArray'));
			 $campaign_id = $this->input->post('select_checkbox');
			 $this->contact_model->addSingleContact($this->member_id,$postArray,$campaign_id);
			 $this->session->set_userdata('success_msg','Contact Added Successfully');
		     redirect('add-contact');
		   }
	   }
	   /*------------ single select function step 2 (choose campaign) ends here---------------------------*/

		$this->load->view($this->config->item('templateName').'/header',$output);
		$this->load->view($this->config->item('templateName').'/contact/add_contact');
		$this->load->view($this->config->item('templateName').'/footer');
	}
    function validateMapping($str)
	{
	   if(in_array('email',$this->input->post('field')))
		{  return true; }
	   else
	    {  $this->form_validation->set_message('validateMapping','Please choose at least Email ID in Drop Down List');
		   return FALSE;
		}
	}
	function validateEmails($str)
	{
	   $emailArray = explode(',',$str);

	   foreach($emailArray as $val)
	   {
		  if(!empty(trim($val)))
		  {
		   if (!filter_var(trim($val), FILTER_VALIDATE_EMAIL) === true) {
		    $this->form_validation->set_message('validateEmails','<b>'.substr($val,0,100).'</b> is not a valid email address');
			   return FALSE;
			}
		 }
       }
	   $total_contacts = $this->contact_model->getAllCount($this->member_id);
	   $total_contacts = $total_contacts+sizeof($emailArray);
	   if($total_contacts>$this->subscriber_count && $this->subscriber_count!='unlimited')
	    {
		  $this->form_validation->set_message('validateEmails','Please upgrade to add more than '.$this->subscriber_count.' contacts. <a target="_blank" href="'.$this->upgrade_link.'" class="upgradelink">Click here to upgrade </a>');
		  return FALSE;
		}
	   
	   return true;
	}
	function validateContactCount($str)
	{
		 
		$countthelines = count(file($str, FILE_SKIP_EMPTY_LINES)) - 1;
		//$emailArray = json_decode($str);
		   $total_contacts = $this->contact_model->getAllCount($this->member_id);
		   $total_contacts = $total_contacts+$countthelines;
		   if($total_contacts>$this->subscriber_count && $this->subscriber_count!='unlimited')
			{
			  $this->form_validation->set_message('validateContactCount','Please upgrade to add more than '.$this->subscriber_count.' contacts. <a target="_blank" href="'.$this->upgrade_link.'" class="upgradelink">Click here to upgrade </a>');
			  return FALSE;
			}
		   
		   return true;
	}
	function validateContactCountSingle($str)
	{
	   $total_contacts = $this->contact_model->getAllCount($this->member_id);
	   $total_contacts = $total_contacts+1;
	   if($total_contacts>$this->subscriber_count && $this->subscriber_count!='unlimited')
	    {
		  $this->form_validation->set_message('validateContactCountSingle','Please upgrade to add more than '.$this->subscriber_count.' contacts. <a target="_blank" href="'.$this->upgrade_link.'" class="upgradelink">Click here to upgrade </a>');
		  return FALSE;
		}
	   
	   return true;
	}
    function validateFields($str)
	{
	   $fieldname = array_count_values($_POST['fieldname']);
	   $extracolumns = array();
	   foreach($fieldname as $key=>$value)
	    {
		 if($value>1)
		 $extracolumns[] = ucfirst($key);
		}
	   if(empty($extracolumns))
		{  return true; }
	   else
	    {  
		   //$extracolumns = implode(',',$extracolumns);
		   $this->form_validation->set_message('validateFields', 'Duplicate values are not allowed');
		   return FALSE;
		}
	}
	public function editContact($id)
	{
		$output['meta_title'] = 'Edit Subscriber';
		$output['campaign_list'] = $this->campaign_model->getCampaignList($this->member_id);
		$contact_detail = $this->contact_model->getContactById($id);
		$name = $contact_detail->name;
		$email = $contact_detail->email;
		$address = $contact_detail->address;
		$city = $contact_detail->city;
		$state = $contact_detail->state;
		$country = $contact_detail->country;
		$zip = $contact_detail->zip;
		$phone = $contact_detail->phone;
		$custom_1 = $contact_detail->custom_1;
		$custom_2 = $contact_detail->custom_2;
		$custom_3 = $contact_detail->custom_3;

        if($_POST)
		 {
		   $name = $this->input->post('name');
		   $email = $this->input->post('email');
           $fieldsPost = array();
		   foreach($_POST['fieldname'] as $key=>$val)
			{
			  $$val = $_POST['fieldvalue'][$key];
			  $fieldsPost[] = $val;
			}

		   if(!in_array('address',$fieldsPost))
		   $address = '';
		   if(!in_array('city',$fieldsPost))
		   $city = '';
		   if(!in_array('state',$fieldsPost))
		   $state = '';
		   if(!in_array('country',$fieldsPost))
		   $country = '';
		   if(!in_array('zip',$fieldsPost))
		   $zip = '';
		   if(!in_array('phone',$fieldsPost))
		   $phone = '';
		   if(!in_array('custom_1',$fieldsPost))
		   $custom_1 = '';
		   if(!in_array('custom_2',$fieldsPost))
		   $custom_2 = '';
		   if(!in_array('custom_3',$fieldsPost))
		   $custom_3 = '';
		   
		   //$this->form_validation->set_rules('name','Name','trim|required');
		   $this->form_validation->set_rules('email','Email','trim|required|valid_email');
		   $this->form_validation->set_rules('fieldname[]','Field Name','trim|callback_validateFields');
		   
		   if($this->form_validation->run())
		    {
			  $this->contact_model->updateContact($id,$address,$city,$state,$country,$zip,$phone,$custom_1,$custom_2,$custom_3);
			  $this->session->set_userdata('success_msg','Contact Updated Successfully');
			  redirect('contact-list');
			}
		  
		 }
		 
		$output['name'] = $name;
		$output['email'] = $email;
		
		if($address)
		$customFieldArray['address'] = $address;
		if($city)
		$customFieldArray['city'] = $city;
		if($state)
		$customFieldArray['state'] = $state;
		if($country)
		$customFieldArray['country'] = $country;
		if($zip)
		$customFieldArray['zip'] = $zip;
		if($phone)
		$customFieldArray['phone'] = $phone;
		if($custom_1)
		$customFieldArray['custom_1'] = $custom_1;
		if($custom_2)
		$customFieldArray['custom_2'] = $custom_2;
		if($custom_3)
		$customFieldArray['custom_3'] = $custom_3;
		
		//echo '<pre>'; print_r($customFieldArray); die('gg');
		
		$output['customFieldArray'] = $customFieldArray;
		
		$this->load->view($this->config->item('templateName').'/header',$output);
		$this->load->view($this->config->item('templateName').'/contact/edit');
		$this->load->view($this->config->item('templateName').'/footer');
	}
	public function deleteContact($id)
	{
		 if($this->delete_subscriber!='yes')
		 { 
		   $this->session->set_userdata('error_msg','Please upgrade to use delete contact feature. <a target="_blank" href="'.$this->upgrade_link.'" class="upgradelink">Click here to upgrade </a>');
		   redirect($_SERVER['HTTP_REFERER']);
		 }
	  $this->contact_model->deleteContact($id);
	  $this->session->set_userdata('success_msg','Contact Deleted Successfully');
	  redirect($_SERVER['HTTP_REFERER']);
	}
	public function deleteMultiContact()
	{ 
	  if($_POST)
	   {
		 if($this->delete_subscriber!='yes')
		 { 
		    $data['error_msg'] = 'Please upgrade to use delete contact feature. <a target="_blank" href="'.$this->upgrade_link.'" class="upgradelink">Click here to upgrade </a>';
		    $data['success'] = false;
		 } else {
			 $id = $this->input->post('id');
			 $this->contact_model->removeMultipleContact($id);
			 $data['success'] = true;
		}
		}
	   echo json_encode($data); die;
	 }
	public function copyMoveContact()
	{ 
	  if($_POST)
	   {
	     
		 $actionType = $this->input->post('actionType');
		 if($actionType=='view')
		  {
		    $output['contact_id'] = implode(',',$this->input->post('id'));
		    $output['type'] = $type = $this->input->post('type');
			$output['campaign_list'] = $this->campaign_model->getCampaignListMove($this->member_id);
			$output['page_title'] = $type.' Contact';

			$response['html'] = $this->load->view($this->config->item('templateName').'/contact/move_contact',$output,true);
			$response['success'] = true;
			$this->output
				->set_content_type('application/json')
				->set_output(json_encode($response));
		  } else {
		  
			 $this->form_validation->set_rules('to_campaign[]','Campaign','trim|required');
			 $this->form_validation->set_rules('contact_id','Contact','trim|required');
			 $this->form_validation->set_rules('type','type','trim|required|callback_validateCopyMoveContact');
			 $this->form_validation->set_rules('actionType','Action','trim|required');
			 
			 $contact_id = explode(',',$this->input->post('contact_id'));
		     $to_campaign = $this->input->post('to_campaign');
		     $type = $this->input->post('type');
			 $actionType = $this->input->post('actionType');
			
			 if($this->form_validation->run())
			  {
				  if($type=='move')
				  {
				    foreach($contact_id as $from_val)
					 {
					   foreach($to_campaign as $to_val)
					   $this->contact_model->copyContact($from_val,$to_val);
					 }
					 $this->contact_model->removeMultipleContact($contact_id);
				  }
				  
				 if($type=='copy')
				 {
				    foreach($contact_id as $from_val)
					 {
					   foreach($to_campaign as $to_val)
				       $this->contact_model->copyContact($from_val,$to_val);
					 }
				 }
				 
				 $data['success'] = true;
			  }
			if(validation_errors())
			 {
			   $data['success'] = false;
			   $data['error'] = validation_errors();
			 } else {
			   $data['success'] = true;
			   $data['success_msg'] = 'Contact '.$type.' Successfully';
			   $this->session->set_userdata('success_msg','Contact '.$type.' Successfully');
			   $data['model'] = 'hide';
			 }
			echo json_encode($data); die;
		  }
		 

	   }
	 
	}
	function validateCopyMoveContact($type)
	{
	   if($type=='copy')
	    {  $condition = $this->copy_subscriber; }
	   else { $condition = $this->move_subscriber; }   

	   if($condition=='yes')
		 { return true; }
	   else 
		 { 
		   $this->form_validation->set_message('validateCopyMoveContact','Please upgrade to '.$type.' contact. <a target="_blank" href="'.$this->upgrade_link.'" class="upgradelink">Click here to upgrade </a>');
		   return FALSE;
		 }
	}
	public function exportContact()
	{
	    if($this->export_subscriber!='yes')
		   {
		     $this->session->set_userdata('error_msg','Please upgrade to use export contact feature. <a target="_blank" href="'.$this->upgrade_link.'" class="upgradelink">Click here to upgrade </a>');
	         redirect('contact-list');
		   }
		   
	   $contact_id = explode(',',$this->input->post('id'));
	   $this->load->dbutil();
	   $this->db->select('tbl_contact.name as Name,tbl_contact.email as Email,tbl_campaign.campaign_name as Campaign_Name,tbl_contact.address as Address,tbl_contact.city as City,tbl_contact.state State,tbl_contact.country as Country,tbl_contact.zip as Zip,tbl_contact.phone as Phone,tbl_contact.custom_1 as Custom_1,tbl_contact.custom_2 as Custom_2,tbl_contact.custom_3 as Custom_3');
	   $this->db->where_in('tbl_contact.id',$contact_id);
	   $this->db->join('tbl_campaign','tbl_campaign.campaign_id=tbl_contact.campaign_id','left');
       $query = $this->db->get('tbl_contact');
       $data =  $this->dbutil->csv_from_result($query); 
	   $this->load->helper('download');
	   $filename = 'contact_'.time().'.csv';
       force_download($filename, $data);
	}
	public function getNewsletterList()
	{
        if($_POST)
		 {
		   $type = $this->input->post('type');
		   
		   $output['newsletter_list'] = $this->contact_model->getNewsletterListByType($type);
		   $output['success'] = true;
			$this->output
				->set_content_type('application/json')
				->set_output(json_encode($output));
		 }
	}
	public function exportAllContact()
	{
	    if($this->export_subscriber!='yes')
		   {
		     $this->session->set_userdata('error_msg','Please upgrade to use export contact feature. <a target="_blank" href="'.$this->upgrade_link.'" class="upgradelink">Click here to upgrade </a>');
	         redirect('contact-list');
		   }
	   
	   $output['campaign_id'] = $campaign_id = $this->input->get('campaign_id');
	   $output['subscribe_type'] = $subscribe_type = $this->input->get('subscribe_type');
	   $output['message_type'] = $message_type = $this->input->get('message_type');
	   if($message_type=='newsletter')
	   {
	  	 $output['newsletter_id'] = $newsletter_id = $this->input->get('newsletter_id');
		 $output['stat_type'] = $stat_type = $this->input->get('stat_type');
	   }
	   $output['date_time'] = $date_time = $this->input->get('date_time');
	   if($date_time=='custom')
	   {
		 $output['from_date'] = $from_date = $this->input->get('from_date');
		 $output['to_date'] = $to_date = $this->input->get('to_date');
	   }
	   $output['keyword'] = $keyword = $this->input->get('keyword');
	   
	   
	   $allRecord = $this->contact_model->getContactRecordExport($this->member_id,$campaign_id,$subscribe_type,$message_type,$newsletter_id,$stat_type,$date_time,$from_date,$to_date,$keyword);
	   
	   
	   
	   
	   
	   
	   
	   
	   
	   
	   
	   
	   
	   $contact_id = explode(',',$this->input->post('id'));
	   $this->load->dbutil();
	   $this->db->select('tbl_contact.name as Name,tbl_contact.email as Email,tbl_campaign.campaign_name as Campaign_Name,tbl_contact.address as Address,tbl_contact.city as City,tbl_contact.state State,tbl_contact.country as Country,tbl_contact.zip as Zip,tbl_contact.phone as Phone,tbl_contact.custom_1 as Custom_1,tbl_contact.custom_2 as Custom_2,tbl_contact.custom_3 as Custom_3');
	   $this->db->where_in('tbl_contact.id',$contact_id);
	   $this->db->join('tbl_campaign','tbl_campaign.campaign_id=tbl_contact.campaign_id','left');
       $query = $this->db->get('tbl_contact');
       $data =  $this->dbutil->csv_from_result($allRecord); 
	   $this->load->helper('download');
	   $filename = 'contact_'.time().'.csv';
       force_download($filename, $data);
	}
	
}