<?php
defined('BASEPATH') OR exit('No direct script access allowed');
include('app.php');

require 'spark_post/autoload.php'; 
use SparkPost\SparkPost;
use GuzzleHttp\Client; 
use Http\Adapter\Guzzle6\Client as GuzzleAdapter;

require_once('postmark/autoload.php');
use Postmark\PostmarkClient;

require 'mailgun/autoload.php';
use Mailgun\Mailgun;
require 'elastic_mail/autoload.php';
require 'sendgrid/autoload.php';
class Autoresponder extends App {

	function __construct()
	{ 
	    parent::__construct();				
		$this->load->model('autoresponder_model');
		$this->load->model('newsletter_model');
		$this->load->model('template_model');
		$this->load->model('campaign_model');
		$this->load->model('contact_model');
		$this->load->model('suppression_list_model');
		$this->load->model('suppression_model');
		$this->load->model('unsubscribe_model');
		$this->load->model('mailsending_model');
		$this->checkLogin();
		$this->member_id = $this->session->userdata('mem_id');
	}
	function checkLogin()
	{
	  if($this->session->userdata('mem_id')=='')
	  { redirect('login'); }
	  
	  if($this->autoresponder_management!='yes')
	  {
	    $this->session->set_userdata('error_msg','Please upgrade to use autoresponder feature. <a target="_blank" href="'.$this->upgrade_link.'" class="upgradelink">Click here to upgrade </a>');
	    redirect('newsletter-list/sent');
	  }
	  
	}
	public function index()
	{
		$output['total_data'] = $this->autoresponder_model->getAllCount($this->member_id,null);
        $output['status'] = $status;
		$this->session->unset_userdata('auto_session');
		$this->load->view($this->config->item('templateName').'/header',$output);
		$this->load->view($this->config->item('templateName').'/message/autoresponder/autoresponder_list');
		$this->load->view($this->config->item('templateName').'/footer');
	}
	public function load_more()
	{
		$group_no = $this->input->post('group_no');
		$content_per_page = 8;
		$start = ceil($group_no * $content_per_page);
		$output['allRecord'] = $allRecord = $this->autoresponder_model->getAutoList($this->member_id,'',$start,$content_per_page);
	  //	echo '<pre>'; print_r($allRecord); die;

		echo $this->load->view($this->config->item('templateName').'/message/autoresponder/autoresponder_pagination',$output,true);
	}
	public function addAutoresponder($auto_id)
	{
		if($_GET['add_type']=='new')
		$this->session->unset_userdata('auto_session');
		
		$auto_session = $this->session->userdata('auto_session');

		if($auto_id && $auto_session['auto_id']=='')
		  { 
		    $auto_detail = $this->autoresponder_model->getAutoById($this->member_id,$auto_id);
			$auto_session['in_campaign_id'] = explode(',',$auto_detail->in_campaign_id);
			$auto_session['step5'] = 'yes';
			$auto_session['template_text'] = $auto_detail->template_text;
			$auto_session['image'] = $auto_detail->image;
			$auto_session['step4'] = 'yes';
			$auto_session['template_id'] = $auto_detail->template_id;
			$auto_session['step3'] = 'yes';
			$auto_session['auto_name'] = $auto_detail->auto_name;
		    $auto_session['subject'] = $auto_detail->subject;
			$auto_session['from_name'] = $auto_detail->from_name;
			$auto_session['from_email'] = $auto_detail->from_email;
			$auto_session['reply_to'] = $auto_detail->reply_to;
			$auto_session['mail_day'] = $auto_detail->mail_day;
			$auto_session['enable_on'] = explode(',',$auto_detail->enable_on);
			$auto_session['send_time'] = $auto_detail->send_time;
		    $auto_session['step2'] = 'yes';

			if($auto_session['dirname']=='')
			{ 
				$attatchment = $this->autoresponder_model->getAttatchFile($auto_id);
				$attatchmentArr = array();
				$dirname = time();
				$auto_session['dirname'] = $dirname;
				foreach($attatchment as $valA)
				{
				 $size = filesize('./assets/uploads/email_attatchment/auto/'.$auto_id.'/'.$valA->file_name)/1024;
				 $attatchmentArr[] = array('name'=>$valA->file_name,'size'=>round($size,2));
				 mkdir("./assets/uploads/temp_attatchment/auto/".$dirname, 0777);
				 copy('./assets/uploads/email_attatchment/auto/'.$auto_id.'/'.$valA->file_name,'./assets/uploads/temp_attatchment/auto/'.$dirname.'/'.$valA->file_name);
				}
				$auto_session['attatchment'] = $attatchmentArr;
		    }
			$auto_session['auto_id'] = $auto_id;
			$auto_session['auto_status'] = $auto_detail->status;
			if($auto_session['template_id']=='' || $auto_session['template_id']==0)
		    $auto_session['editor_type'] = 'plain';
			else
		    $auto_session['editor_type'] = 'inline';
			$this->session->set_userdata('auto_session',$auto_session);
	     }
		$output['auto_session'] = $auto_session;
		
		if($_GET['step']) { $step = $_GET['step']; } else { $step = ''; }
		/* for step 1 */
		if($step=='' || $step==1)
		 {
		   if($auto_session['editor_type']=='')
		   redirect('choose-autoresponder-editor');
		   
		   $output['auto_name'] = $auto_session['auto_name'];
		   $output['subject'] = $auto_session['subject'];
		   $output['from_name'] = $auto_session['from_name'];
		   $output['from_email'] = $auto_session['from_email'];
		   $output['reply_to'] = $auto_session['reply_to'];

		   $output['from_emails'] = $from_emails = $this->autoresponder_model->getFromEmailList($this->member_id);
		   $step = 1;
		 }
		if($_POST['step1'])
		 {
		   $output['auto_name'] = $this->input->post('auto_name');
		   $output['subject'] = $this->input->post('subject');
		   $output['from_name'] = $this->input->post('from_name');
		   $output['from_email'] = $this->input->post('from_email');
		   $output['reply_to'] = $this->input->post('reply_to');
		   $mail_type = $this->common_model->getSingleFieldFromAnyTable('mail_type','member_id',$this->session->userdata('mem_id'),'tbl_website_settings');

		   $this->form_validation->set_rules('auto_name','Autoresponder Name','trim|required');
		   $this->form_validation->set_rules('subject','Subject','trim|required');
		   if($mail_type=='smtp' && $mail_type=='default') {
		   $this->form_validation->set_rules('from_email','From Email','trim|required');
		   $this->form_validation->set_rules('reply_to','Reply To','trim|required');
		   }
		  
		   if($this->form_validation->run())
		    {
			   $auto_session['auto_name'] = $this->input->post('auto_name');
			   $auto_session['subject'] = $this->input->post('subject');
			   $auto_session['from_name'] = $this->input->post('from_name');
			   $auto_session['from_email'] = $this->input->post('from_email');
			   $auto_session['reply_to'] = $this->input->post('reply_to');
			   $auto_session['step2'] = 'yes';
			   $this->session->set_userdata('auto_session',$auto_session);
		      redirect('add-autoresponder?step=2');
			}
		}
		/* step 1 ends here */
		
		/* for step 2 */
		if($step==2)
		 {
		   if($auto_session['step2']=='yes')
		    {
			  if($auto_session['editor_type']=='inline')
			  $output['defaultTemplate'] = $this->template_model->getTemplateListByType($this->member_id,'default');
			  $output['draftTemplate'] = $draftTemplate = $this->template_model->getTemplateListByType($this->member_id,'draft',$auto_session['editor_type']);
			  $output['template_id'] = $auto_session['template_id'];
			  if(sizeof($draftTemplate)==0 && $auto_session['editor_type']=='plain')
			   {
			     $auto_session['template_text'] = '';
			     $auto_session['template_id'] = '';
			     $auto_session['step3'] = 'yes';
				 $this->session->set_userdata('auto_session',$auto_session);
				 redirect('add-autoresponder?step=3');
			   }
			   
			}
		   else {
		     redirect('add-autoresponder');
		   }
		 }
		 
		 if($_POST['step2']){
		    
			if($auto_session['step2']=='yes')
		    {
			   $this->form_validation->set_rules('template_id','Choose Template','trim|required');
			   
			   if($this->form_validation->run())
				{
				  $template_detail = $this->template_model->getTemplateById($this->input->post('template_id'));
			      $auto_session['template_text'] = str_replace('{image_path}',$this->config->item('templateAssetsPath').'editor/',$template_detail->template_text);
			      $auto_session['template_id'] = $this->input->post('template_id');
			      $auto_session['step3'] = 'yes';
				  $this->session->set_userdata('auto_session',$auto_session);
				  redirect('add-autoresponder?step=3');
				}
			}
			 else {
		     redirect('add-autoresponder');
		   }
		 }
		/* step 2 ends here */
		
		/* for step 3 */
		if($step==3)
		 {
		   if($auto_session['step3']=='yes')
		    {
			   $output['template_text'] = $auto_session['template_text'];
			}
			else {
		     redirect('add-autoresponder?step=2');
		   }
		 }
		 
		  if($_POST['step3']){
		    
			if($auto_session['step3']=='yes')
		    {
			   $output['template_text'] = $this->input->post('template_text');
			   $this->form_validation->set_rules('template_text','Edit Template','trim|required');
			   
			   if($this->form_validation->run())
				{
			      $auto_session['template_text'] = $this->input->post('template_text');
				  $auto_session['image_text'] = $this->input->post('image_text');
				  $auto_session['step4'] = 'yes';
				  $this->session->set_userdata('auto_session',$auto_session);
				  redirect('add-autoresponder?step=4');
				}
			}
			else {
		     redirect('add-autoresponder?step=2');
		   }
		 }
		/* step 3 ends here */
		
		/* for step 4 */
		if($step==4)
		 { 
		   if($auto_session['step4']=='yes')
		    {
			   $output['campaign_list'] = $this->campaign_model->getCampaignList($this->member_id);
			   $output['in_campaign_id']  = $auto_session['in_campaign_id'];
			}
			else { 
		     redirect('add-autoresponder?step=3');
		   }
		 }
		 
		 if($_POST['step4']){
		    
			if($auto_session['step4']=='yes')
		    {
			  $output['in_campaign_id']  = $this->input->post('in_campaign_id');

			  $this->form_validation->set_rules('in_campaign_id[]','Choose Campaign','trim|required');
			   
			   if($this->form_validation->run())
				{
			        $auto_session['in_campaign_id'] = $this->input->post('in_campaign_id');
					$auto_session['step5'] = 'yes';
					$this->session->set_userdata('auto_session',$auto_session);
					redirect('add-autoresponder?step=5');
			    }
			}
			else {
		     redirect('add-autoresponder?step=3');
		   }
		 }
		/* step 4 ends here */
		
		/* for step 5 */
		if($step==5)
		 { 
		  
		   if($auto_session['step5']=='yes')
		    {
			  $output['auto_name'] = $auto_session['auto_name'];
			  $output['subject'] = $auto_session['subject'];
			  $output['mail_day'] = $auto_session['mail_day'];
			  $output['enable_on'] = $auto_session['enable_on'];
			  $output['send_time'] = $auto_session['send_time'];
			  $output['template_name'] = $this->common_model->getSingleFieldFromAnyTable('template_name','id',$auto_session['template_id'],'tbl_template');
			}
			else { 
		     redirect('add-autoresponder?step=4');
		   }
		 }
		 if($_POST['step5']){
			if($auto_session['step5']=='yes')
		    {
			  $output['mail_day']  = $mail_day  = $this->input->post('mail_day');
			  $output['enable_on'] = $enable_on = $this->input->post('enable_on');
			  $output['send_time'] = $send_time = $this->input->post('send_time');

			  $this->form_validation->set_rules('mail_day','Mail Date','trim|required');
			  $this->form_validation->set_rules('enable_on[]','Active Days','trim|required');
			  if($mail_day!=0)
			  $this->form_validation->set_rules('send_time','Send Time','trim|required');
			  
			  
			   if($this->form_validation->run())
				{
					$subject = $auto_session['subject'];
					$message = $auto_session['template_text'];
					$from_name = $auto_session['from_name'];
					$from_email = $auto_session['from_email'];
					$reply_to = $auto_session['reply_to'];
					if($_POST['step5']=='save')
					$status = 'inactive';
					else
					$status = 'active';
					
					if($auto_session['editor_type']=='inline')
					$template_id = $auto_session['template_id'];
					else
					$template_id = 0;
					
					if($auto_session['image_text'])
					{
						$filteredData=substr($auto_session['image_text'], strpos($auto_session['image_text'], ",")+1);
						$unencodedData=base64_decode($filteredData);
						$filename = $this->member_id.time().'.png';
						file_put_contents('./assets/uploads/autoresponder_images/'.$filename, $unencodedData);
					} else {
						$filename = $this->member_id.time().'.png';
						copy('./assets/uploads/autoresponder_images/'.$auto_session['image'], './assets/uploads/autoresponder_images/'.$filename);
					}

					$insertArray = array(
									"member_id" => $this->member_id,
									"from_name" => $from_name,
									"from_email" => $from_email,
									"reply_to" => $reply_to,
									"auto_name" => $auto_session['auto_name'],
									"subject" => $subject,
									"template_id" => $template_id,
									"template_text" => $message,
									"mail_day" => $mail_day,
									"enable_on" => implode(',',$enable_on),
									"send_time" => $send_time,
									"image" => $filename,
									"in_campaign_id" => implode(",",$auto_session['in_campaign_id']),
									"status" => $status
									);

					$auto_id = $this->autoresponder_model->addAutoresponder($insertArray);

					$attatchArray = $auto_session['attatchment'];
					foreach($attatchArray as $valAttatch)
					 {
					  $this->autoresponder_model->addAttatchFile($valAttatch['name'],$auto_id);
					  $dirname = $auto_session['dirname'];
					  mkdir("./assets/uploads/email_attatchment", 0777);	
					  mkdir("./assets/uploads/email_attatchment/auto", 0777);	
					  mkdir("./assets/uploads/email_attatchment/auto/".$auto_id, 0777);
					  copy('./assets/uploads/temp_attatchment/auto/'.$dirname.'/'.$valAttatch['name'], './assets/uploads/email_attatchment/auto/'.$auto_id.'/'.$valAttatch['name']);
					  unlink('./assets/uploads/temp_attatchment/auto/'.$dirname.'/'.$valAttatch['name']);
					 }
					 rmdir('./assets/uploads/temp_attatchment/auto/'.$dirname);

					 $this->session->unset_userdata('auto_session');
	                 redirect('autoresponder-successfull');
			  }

			}
			else {
		     redirect('add-autoresponder?step=4');
		   }
		 }
		/* step 5 ends here */
		$output['step'] = $step;
		
		if($step==3 && $auto_session['editor_type']=='inline')
		 {
		    $this->load->view($this->config->item('templateName').'/message/autoresponder/inline_editor',$output);
		 } else {
			$this->load->view($this->config->item('templateName').'/header',$output);
			$this->load->view($this->config->item('templateName').'/message/autoresponder/auto_step'.$step);
			$this->load->view($this->config->item('templateName').'/footer');
		}
	}
	public function addTempAttatchment()
	{
     $auto_session = $this->session->userdata('auto_session');
	 if($auto_session['dirname']=='')
     $dirname = time();
	 else
	 $dirname = $auto_session['dirname'];
	 mkdir("./assets/uploads/temp_attatchment", 0777);	
	 mkdir("./assets/uploads/temp_attatchment/auto", 0777);
	 mkdir("./assets/uploads/temp_attatchment/auto/".$dirname, 0777);	
	 $config['upload_path'] = './assets/uploads/temp_attatchment/auto/'.$dirname.'/';
	 $config['allowed_types'] = '*';
	 $this->load->library('upload', $config);				
	  if(!$this->upload->do_upload('attatchment'))
	  {
	     $this->upload->display_errors();	
	  } else {
	     $data1 = array('upload_data' => $this->upload->data());
	  }
	 $attatchment = $auto_session['attatchment'];
	 if($attatchment=='')
	 $attatchment = array();
	 
	 $attatchment[] = array('name' => $data1['upload_data']['file_name'], 'size' => round($_FILES['attatchment']['size']/1024,2));
	 $auto_session['attatchment'] = $attatchment;	
	 $auto_session['dirname'] = $dirname;	
	 $this->session->set_userdata('auto_session',$auto_session);	
	 $data['success'] = true;
	 $data['attatchment'] = $attatchment;
	 $data['attatchCount'] = sizeof($attatchment);
	 echo json_encode($data); die;

	}
	public function deleteAttatchment()
	{
	$auto_session = $this->session->userdata('auto_session');	  
	 $name = $this->input->post('name');
	 $dirname = $auto_session['dirname'];
	 unlink('./assets/uploads/temp_attatchment/auto/'.$dirname.'/'.$name);
	 $attatchment = $auto_session['attatchment'];
	 foreach($attatchment as $key=>$val)
	  {
	   if($val['name']==$name)
	   unset($attatchment[$key]);
	  }
	 $auto_session['attatchment'] = $attatchment;
	 $this->session->set_userdata('auto_session',$auto_session);	
	 
	 $data['success'] = true;
	 $data['attatchCount'] = sizeof($attatchment);
	 echo json_encode($data); die;

	}
	public function sendTestMail()
	{
	  if($_POST)
	   {
		   $this->form_validation->set_rules('template_text','Template','trim|required');
		   $this->form_validation->set_rules('testemail','Test Email','trim|required|valid_email');

		  if($this->form_validation->run())
		  {    
		       $auto_session = $this->session->userdata('auto_session');
			   
			   $mailArray = array();
			   $mailArray['from_name'] = $auto_session['from_name'];
			   $mailArray['from_email'] = $auto_session['from_email'];
			   $mailArray['reply_to'] = $auto_session['reply_to'];
			   $mailArray['message'] = $this->input->post('template_text');
			   $mailArray['contact_email'] = $this->input->post('testemail');
			   $mailArray['attatchmentPath'] = 'temp_attatchment/auto/'.$auto_session['dirname'];
			   $mailArray['member_id'] = $this->member_id;
			   
			   $mail_type = $this->common_model->getSingleFieldFromAnyTable('mail_type','member_id',$this->member_id,'tbl_website_settings');
			  if($mail_type!='default' && $mail_type!='smtp')
				{
				  $api_value = $this->common_model->getSingleFieldFromAnyTable('api_value','api_name',$mail_type,'tbl_external_mail_api');
				  $api_data = unserialize($api_value);
				  $mailArray['from_email'] = $from_email = $api_data['sending_email'];
				}
				 
			   $Mailfooter = '<br/><br/><br/><br/>'.$this->footerText($this->member_id);
			   $Mailfooter = str_replace("{#to_email}",$mailArray['contact_email'],$Mailfooter);
			   $Mailfooter = str_replace("{#from_email}",$mailArray['from_email'],$Mailfooter);
			   $Mailfooter = str_replace("{#unsubscribe_link}",'#',$Mailfooter);

			   $subject=str_replace("{#name}",ucfirst($val->name),$auto_session['subject']);
		       $subject=str_replace("{#email}",$val->email,$subject);
			   $subject=str_replace("{#address}",$val->address,$subject);
			   $subject=str_replace("{#city}",$val->city,$subject);
			   $subject=str_replace("{#state}",$val->state,$subject);
			   $subject=str_replace("{#country}",$val->country,$subject);
			   $subject=str_replace("{#zip}",$val->zip,$subject);
			   $subject=str_replace("{#phone}",$val->phone,$subject);
			   $mailArray['subject'] = $subject;
			   
			   $message=str_replace("{#name}",ucfirst($val->name),$this->input->post('template_text'));
		       $message=str_replace("{#email}",$val->email,$message);
			   $message=str_replace("{#address}",$val->address,$message);
			   $message=str_replace("{#city}",$val->city,$message);
			   $message=str_replace("{#state}",$val->state,$message);
			   $message=str_replace("{#country}",$val->country,$message);
			   $message=str_replace("{#zip}",$val->zip,$message);
			   $message=str_replace("{#phone}",$val->phone,$message);
			   $mailArray['message'] = $message.$Mailfooter;
			   $attatchFiles = array();
			   foreach($auto_session['attatchment'] as $val)
			   $attatchFiles[] = $val['name'];
			   
			   $mailArray['attatchFiles'] = $attatchFiles;
			   $this->sendMail($mailArray);
			   $data['error'] = false;
		   } else { 
			   $data['error'] = validation_errors();
			   
		   }
	   }
	   $data['success'] = true;
	   echo json_encode($data); die;
	}
	public function saveTemplate()
	{
	   if($_POST)
	   {
	     $auto_session = $this->session->userdata('auto_session');
		 $template_name = $auto_session['auto_name'];
		 $template_text = $this->input->post('template_text');
		 
		 $filteredData=substr($_POST['image_text'], strpos($_POST['image_text'], ",")+1);
	     $unencodedData=base64_decode($filteredData);
		 $filename = $this->member_id.time().'.png';
		 file_put_contents('./assets/uploads/template_images/'.$filename, $unencodedData);
		 
		 $this->autoresponder_model->saveTemplate($this->member_id,$template_name,$template_text,$auto_session['editor_type'],$filename);
	   }
	 
	 $data['success'] = true;
	 echo json_encode($data); die;

	}
	public function deleteAutoresponder($id)
	{
	  $this->autoresponder_model->deleteAutoresponder($id);
	  $this->session->set_userdata('success_msg','Autoresponder Deleted Successfully');
	  redirect($_SERVER['HTTP_REFERER']);
	}
	public function deleteMultiAutoresponder()
	{ 
	  if($_POST)
	   {
		 $id = $this->input->post('id');
		 $this->autoresponder_model->removeMultipleAutoresponder($id);
		 $this->session->set_userdata('success_msg','Autoresponder Deleted Successfully');
		 $data['success'] = true;
		}
	   echo json_encode($data); die;
	   }
	public function success()
	{
		$this->load->view($this->config->item('templateName').'/header',$output);
		$this->load->view($this->config->item('templateName').'/message/autoresponder/autoresponder_successfully');
		$this->load->view($this->config->item('templateName').'/footer');
	}
    public function changeStatus()
	{
	  $status = $this->input->post('status');
	  $id = $this->input->post('id');
	  $this->autoresponder_model->statusChange($status,$id);
	  $data['success'] = true;
	  echo json_encode($data); die;
	}
	public function chooseEditor()
	{
		if(!empty($_GET))
		 {
		   $auto_session = $this->session->userdata('auto_session');
		   $auto_session['editor_type'] = $_GET['editor_type'];
		   $this->session->set_userdata('auto_session',$auto_session);
		   redirect('add-autoresponder');
		 }
		$this->load->view($this->config->item('templateName').'/header',$output);
		$this->load->view($this->config->item('templateName').'/message/autoresponder/choose_editor');
		$this->load->view($this->config->item('templateName').'/footer');
	}
	public function sendMail($mailArray)
	{
	  
	  $mailArray['mail_type'] = $mail_type = $this->common_model->getSingleFieldFromAnyTable('mail_type','member_id',$mailArray['member_id'],'tbl_website_settings');

	  if($mailArray['mail_type']!='default' && $mailArray['mail_type']!='smtp')
		 {
		   $mailArray['api_value'] = $this->common_model->getSingleFieldFromAnyTable('api_value','api_name',$mail_type,'tbl_external_mail_api');
		   $mailArray['to'] = $mailArray['contact_email'];
		 }
		 
	  if($mailArray['mail_type']=='default' || $mailArray['mail_type']=='smtp')
	   {
	     $this->mailsending_model->sendTestMail($mailArray);
	   } 
	  else if($mailArray['mail_type']=='amazon_ses')
	   {
	     require_once 'amazon_ses/autoload.php';
	     $this->mailsending_model->sendMailUsingAmazon($mailArray);
	   }
	  else if($mailArray['mail_type']=='sparkpost')
	   {
	     $httpClient = new GuzzleAdapter(new Client());
		 $api_value = unserialize($mailArray['api_value']);
		 $api_key = $api_value['api_key'];
		 $sparky = new SparkPost($httpClient, ['key'=>$api_key]);
		 $response = $this->mailsending_model->sendMailUsingSparkpost($mailArray,$sparky);
	   }
	  else if($mailArray['mail_type']=='postmark')
	   {
		 $httpClient = new GuzzleAdapter(new Client());
		 $api_value = unserialize($mailArray['api_value']);
		 $api_key = $api_value['api_key'];
		 $client = new PostmarkClient($api_key);
		 $response = $this->mailsending_model->sendMailUsingPostmark($mailArray,$client);
	   }
	  else if($mailArray['mail_type']=='mailgun')
	   {
		 $api_value = unserialize($mailArray['api_value']);
		 
		 $api_key = $api_value['api_key'];
		 $sending_domain = $api_value['sending_domain'];
		 $sending_email = $api_value['sending_email'];
		 $mgClient = new Mailgun($api_key);
		 $response = $this->mailsending_model->sendMailUsingMailgun($mailArray,$mgClient,$sending_domain,$sending_email);
	   }
	  else if($mailArray['mail_type']=='elasticmail')
	   {
		 $api_value = unserialize($mailArray['api_value']);
		 $api_key = $api_value['api_key'];
		 try{
		     $elasticEmail = new \ElasticEmail\ElasticEmailV2($api_key);
			 $response = $this->mailsending_model->sendMailUsingElastic($mailArray,$elasticEmail);
		 }
		 catch(Exception $e){
							$response = 'error';	
						}
		 
	   }
	  else if($mailArray['mail_type']=='sendgrid'){
	 	 $api_value = unserialize($mailArray['api_value']);
		 $api_key = $api_value['api_key'];
		 $sending_mail = $api_value['sending_mail'];
	  	 try{
				$response = $this->mailsending_model->sendMailUsingSendgrid($mailArray,$api_key,$sending_mail);
			}
			catch(Exception $e){
				$response = 'error';	
			}
	  
	  } 
	}
	public function footerText($member_id)
	{
		$social_links = $this->newsletter_model->getSocialLinks($member_id);
		$useraddress = $this->newsletter_model->getUserAddress($member_id);
		 
		$fb_url = $social_links->fb_url;
		$tw_url = $social_links->tw_url;
		$insta_url = $social_links->insta_url;
		$gplus_url = $social_links->gplus_url;
		
		if($fb_url)
		 {
		   $social_liks.=  '<a href="'.$fb_url.'"><img src="'.site_url().'assets/default/images/email_footer/images/fb.png" alt="" /></a>';
		 }
		if($tw_url)
		 {
		   $social_liks.=  '<a href="'.$tw_url.'"><img src="'.site_url().'assets/default/images/email_footer/images/tw.png" alt="" /></a>';
		 }
		if($insta_url)
		 {
		   $social_liks.=  '<a href="'.$insta_url.'"><img src="'.site_url().'assets/default/images/email_footer/images/gplus.png" alt=""/></a>';
		 }
		if($gplus_url)
		 {
		   $social_liks.=  '<a href="'.$gplus_url.'"><img src="'.site_url().'assets/default/images/email_footer/images/in.png" alt=""/></a>';
		 }
		 if($social_liks)
		  {
		     $social_box = '<p style="margin:0; padding:0; line-height:20px;"><span style="position: relative;margin-top:-1px; padding-right:10px;">Follow us </span>'.$social_liks.'</p>';
		  }
	 	
		
		$footer_text = '<div style="width:600px; background-color:#f5f5f5;color:#4a4949; padding:15px 15px 10px 15px; margin:0 auto; height:auto; font-family: "Roboto", sans-serif; font-weight:400; font-size:13px;">
			<div style="float:left; width:70%;">
			<p style="margin:0; margin-top:-1px; padding-right:10px; line-height:20px;">This email was sent to {#to_email} by {#from_email}</p>
			'.$social_box.'
			<p style="margin:0; padding-bottom:8px; line-height:20px;">'.$useraddress->address.', '.$useraddress->city.', '.$useraddress->pin_code.', '.$useraddress->state.', '.$useraddress->country.'</p>
			<p style="background:url('.site_url().'assets/default/images/email_footer/images/line.jpg) repeat-x top; width:auto; display:inline-block; font-size:11px; margin:0; padding:5px 0px 0px 0px;"> Getting too many emails from us, click here to <a href="{#unsubscribe_link}" style="color:#1155cc; ">Unsubscribe Now.</a></p>
			</div>

			<div style="float:left; width:30%; text-align:right">
			<p style="margin:0; padding:0px 0px 4px 0px; text-align:center; width:95px; display:inline-block;">Powered by</p>
			<a href="https://www.mailzingo.com/"><img src="'.site_url().'assets/default/images/email_footer/images/email-logo.png" alt=""/></a></div>
			<div style="clear:both"></div>
			</div>';
		return $footer_text; 
	}
	
}