<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
include('app.php');

class Editor extends App {


	public function index($template_id,$type)
	{
		$newsletter_session = $this->session->userdata('newsletter_session');
		$auto_session = $this->session->userdata('auto_session');
		if($newsletter_session['template_text'] && $type=='newsletter')
		{
		 $template_text = $newsletter_session['template_text'];
		} else if($auto_session['template_text'] && $type=='autoresponder')
		{
		 $template_text = $auto_session['template_text'];
		} else {
		 $template_text = str_replace('{image_path}',$this->config->item('templateAssetsPath').'editor/',$this->common_model->getSingleFieldFromAnyTable('template_text','id',$template_id,'tbl_template'));
		}
		
		$output['template_text'] = $template_text;
		
		$this->load->view($this->config->item('templateName').'/inlineeditor',$output);
	}

	public function smarteditor()
	{
		$this->load->view($this->config->item('templateName').'/header');
		$this->load->view($this->config->item('templateName').'/mukeditor');
		$this->load->view($this->config->item('templateName').'/footer');
	}
	
	public function uploadmediaimg()
	{
		$config['upload_path'] = './assets/media_image/';
		$config['allowed_types'] = 'gif|jpg|png|jpeg';
		$config['file_name'] = time();
		$this->load->library('upload', $config);	
		$this->upload->overwrite = true;			
		if(!$this->upload->do_upload('newmediaimg'))
		{ 
			echo $this->upload->display_errors(); die('dd'); } else {
			$data = array('upload_data' => $this->upload->data());						
		}
	}

}
