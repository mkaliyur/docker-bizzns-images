<?php
defined('BASEPATH') OR exit('No direct script access allowed');
include('app.php');

class Campaign extends App {
 
	function __construct()
	{ 
	    parent::__construct();				
		$this->load->model('campaign_model');
		$this->checkLogin();
		$this->member_id = $this->session->userdata('mem_id');

	}
	function checkLogin()
	{
	  if($this->session->userdata('mem_id')=='')
	  { redirect('login'); }
	  
	}
	
	public function index()
	{
		$output['meta_title'] = "Campaign";
		$sort_by = $this->input->get('sort_by');
		$search_by = $this->input->get('search_by');
		$output['allRecord'] = $this->campaign_model->getCampaignList($this->member_id,'',$sort_by,$search_by);
		

		$this->load->view($this->config->item('templateName').'/header',$output);
		$this->load->view($this->config->item('templateName').'/campaign/list');
		$this->load->view($this->config->item('templateName').'/footer');
	}
	public function addCampaign()
	{
		
        if($_POST)
		 {
		   $output['campaign_name'] = $this->input->post('campaign_name');
		   $output['campaign_description'] = $this->input->post('campaign_description');
		   
		   $this->form_validation->set_rules('campaign_name','Campaign Name','trim|required|is_unique[tbl_campaign.campaign_name]|callback_validateAddCampaign');
		   
		   if($this->form_validation->run())
		    {
			  $this->campaign_model->addCampaign($this->member_id);
			}
			
			if(validation_errors())
			 {
			   $data['success'] = false;
			   $data['error'] = validation_errors();
			 } else {
			   $data['success'] = true;
			   $data['success_msg'] = 'Campaign Added Successfully';
			   $this->session->set_userdata('success_msg','Campaign Added Successfully');
			   $data['model'] = 'hide';
			 }
			echo json_encode($data); die;
		  
		 }
	}
	public function editCampaign($id)
	{
		
		$campaign_detail = $this->campaign_model->getCampaignById($id);
		$campaign_name = $campaign_detail->campaign_name;
		$campaign_description = $campaign_detail->campaign_description;
        if($_POST)
		 {
		   $campaign_name = $this->input->post('campaign_name');
		   $campaign_description = $this->input->post('campaign_description');
		   
		   if($campaign_name==$campaign_detail->campaign_name)
		   $this->form_validation->set_rules('campaign_name','Campaign Name','trim|required');
		   else
		   $this->form_validation->set_rules('campaign_name','Campaign Name','trim|required|is_unique[tbl_campaign.campaign_name]');
		   
		   if($this->form_validation->run())
		    {
			  $this->campaign_model->updateCampaign($id);
			}
			
			if(validation_errors())
			 {
			   $data['success'] = false;
			   $data['error'] = validation_errors();
			 } else {
			   $data['success'] = true;
			   $data['success_msg'] = 'Campaign updated Successfully';
			   $this->session->set_userdata('success_msg','Campaign Updated Successfully');
			   $data['model'] = 'hide';
			 }
			echo json_encode($data); die;
		  
		 }
		 
		$output['campaign_name'] = $campaign_name;
		$output['campaign_description'] = $campaign_description;
		$output['id'] = $id;
        $output['page_title'] = 'Edit Campaign';
		
		$response['html'] = $this->load->view($this->config->item('templateName').'/campaign/edit',$output,true);
		$response['success'] = true;
			$this->output
				->set_content_type('application/json')
				->set_output(json_encode($response));
	}
	public function deleteCampaign($id)
	{
	   if($this->delete_campaign!='yes')
		 { 
		   $this->session->set_userdata('error_msg','Please upgrade to use delete campaign feature. <a target="_blank" href="'.$this->upgrade_link.'" class="upgradelink">Click here to upgrade </a>');
		   redirect($_SERVER['HTTP_REFERER']);
		 }
		 
	  $this->campaign_model->deleteCampaign($id);
	  $this->session->set_userdata('success_msg','Campaign Deleted Successfully');
	  redirect('campaign-list');
	}
	public function clearCampaign($id)
	{
	  $this->campaign_model->clearCampaign($id);
	  $this->session->set_userdata('success_msg','Campaign Cleared Successfully');
	  redirect('campaign-list');
	}
	public function copyMoveCampaign()
	{ 
	  if($_POST)
	   {
	     
		 $actionType = $this->input->post('actionType');
		 if($actionType=='view')
		  {
		    $output['from_campaign'] = implode(',',$this->input->post('id'));
		    $output['type'] = $type = $this->input->post('type');
			$output['campaign_list'] = $campaign_list = $this->campaign_model->getCampaignListMove($this->member_id);
			$output['page_title'] = $type.' Campaign';

			$response['html'] = $this->load->view($this->config->item('templateName').'/campaign/move_campaign',$output,true);
			$response['success'] = true;
			$this->output
				->set_content_type('application/json')
				->set_output(json_encode($response));
		  } else {
		  
			 $this->form_validation->set_rules('to_campaign[]','Campaign','trim|required');
			 $this->form_validation->set_rules('from_campaign','From Campaign','trim|required');
			 $this->form_validation->set_rules('type','type','trim|required|callback_validateCopyMoveCampaign');
			 $this->form_validation->set_rules('actionType','Action','trim|required');
			 
			 $from_campaign = explode(',',$this->input->post('from_campaign'));
             $to_campaign = $this->input->post('to_campaign');
             $type = $this->input->post('type');
			 $actionType = $this->input->post('actionType');
			
			 if($this->form_validation->run())
			  {
				 $size = 0;
				 if($type=='move')
				  {
				    foreach($from_campaign as $from_val)
					 {
					   $contact_id = $this->common_model->getSingleFieldFromAnyTable('id','campaign_id',$from_val,'tbl_contact');
					   foreach($to_campaign as $to_val)
					   $this->campaign_model->copyCampaign($from_val,$to_val);
					   if($contact_id)
					   $size = 1;
					 }
					 $this->campaign_model->removeMultipleContact($from_campaign);
				  }
				 
				 if($type=='copy')
				 {
				    foreach($from_campaign as $from_val)
					 {
					   $contact_id = $this->common_model->getSingleFieldFromAnyTable('id','campaign_id',$from_val,'tbl_contact');
					   foreach($to_campaign as $to_val)
				       $this->campaign_model->copyCampaign($from_val,$to_val);		
					   if($contact_id)
					   $size = 1;
					 }
				 }
			  }

			if(validation_errors() || $size==0)
			 {
			   $data['success'] = false;
			   $data['error'] .= validation_errors();
			   if($size==0 && validation_errors()=='')
			   $data['error'] .= 'Nothing to '.$type;
			 } else {
			   $data['success'] = true;
			   $data['success_msg'] = 'Campaign '.$type.' Successfully';
			   $this->session->set_userdata('success_msg','Campaign '.$type.' Successfully');
			   $data['model'] = 'hide';
			 }
			echo json_encode($data); die;
		  }
		 

	   }
	 
	}
	public function deleteMultiCampaign()
	{ 
	  if($_POST)
	   {
		  if($this->delete_campaign!='yes')
		 { 
		    $data['error_msg'] = 'Please upgrade to use delete campaign feature. <a target="_blank" href="'.$this->upgrade_link.'" class="upgradelink">Click here to upgrade </a>';
		    $data['success'] = false;
		 } else {
		  $campaign_id = $this->input->post('id');
		  $this->session->set_userdata('success_msg','Campaign Deleted Successfully');
		  $this->campaign_model->removeMultipleCampaign($campaign_id);
		  $data['success'] = true;
		 }
		}
	   echo json_encode($data); die;
	   }
	function validateAddCampaign($str)
	{
	   if($this->campaign_count=='unlimited')
		{  return true; }
	   else 
	    {  
		   $campaign_count = $this->campaign_model->getAllCount($this->member_id);
		   if($this->campaign_count<=$campaign_count)
		    {
		      $this->form_validation->set_message('validateAddCampaign','Please upgrade to add more campaign. <a target="_blank" href="'.$this->upgrade_link.'" class="upgradelink">Click here to upgrade </a>');
		      return FALSE;
			} else {
			  return true;
			}
		}
	}
	function validateCopyMoveCampaign($type)
	{ 
	   if($type=='copy')
	    {  $condition = trim($this->copy_campaign); }
	   else { $condition = trim($this->move_campaign); }   

	   if($condition=='yes')
		 { return true; }
	   else 
		 { 
		   $this->form_validation->set_message('validateCopyMoveCampaign','Please upgrade to '.$type.' campaign. <a target="_blank" href="'.$this->upgrade_link.'" class="upgradelink">Click here to upgrade </a>');
		   return FALSE;
		 }
	}
}