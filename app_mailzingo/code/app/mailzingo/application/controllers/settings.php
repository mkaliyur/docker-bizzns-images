<?php
defined('BASEPATH') OR exit('No direct script access allowed');
include('app.php');
require 'spark_post/autoload.php';
use SparkPost\SparkPost;
use GuzzleHttp\Client; 
use Http\Adapter\Guzzle6\Client as GuzzleAdapter;

require_once('postmark/autoload.php');
use Postmark\PostmarkClient;

require 'mailgun/autoload.php';
use Mailgun\Mailgun;
require 'elastic_mail/autoload.php';
require 'sendgrid/autoload.php';

class Settings extends App {

    function __construct() {
        parent::__construct();
        $this->load->model('settings_model');
		$this->load->model('newsletter_model');
		$this->load->model('mailsending_model');
        $this->checkLogin();
        $this->load->library('image_lib');
        $this->member_id = $this->session->userdata('mem_id');
	}

    function checkLogin() {
        if ($this->session->userdata('mem_id') == '') {
            redirect('login');
        }
    }

    public function user_setting() {

		$output['meta_title'] = "Settings User";
        $user_details = $this->settings_model->getUser($this->member_id);
        $output['mail_details'] = $this->settings_model->getFrom_email($this->member_id);
        $output['page'] = 'user';
		$output['name'] = $user_details->name;
		$output['country'] = $user_details->country;
		$output['address'] = $user_details->address;
		$output['state'] = $user_details->state;
		$output['pin_code'] = $user_details->pin_code;
		$output['city'] = $user_details->city;
		$output['business_name'] = $user_details->business_name;
		$output['profile_pic'] = $user_details->profile_pic;


        if ($_POST['user_form'] == 'user_form') {

            $output['name']   = $this->input->post('name');
			$output['country']  =  $this->input->post('country');
			$output['address']  = $this->input->post('address');
			$output['state']  =  $this->input->post('state');
			$output['pin_code']  =  $this->input->post('pin_code');
			$output['city'] =  $this->input->post('city');
			$output['business_name']  =  $this->input->post('business_name');

			$this->form_validation->set_rules('name', 'Name', 'trim|regex_match[/^[a-zA-Z ]*$/]|required');
            $this->form_validation->set_rules('country', 'Country', 'trim|regex_match[/^[a-zA-Z ]*$/]|required');
            $this->form_validation->set_rules('address', 'Address', 'trim|required');
            $this->form_validation->set_rules('state', 'State', 'trim|regex_match[/^[a-zA-Z ]*$/]');
            $this->form_validation->set_rules('pin_code', 'Post Code', 'trim|required');
            $this->form_validation->set_rules('city', 'City', 'trim|regex_match[/^[a-zA-Z ]*$/]|required');
            $this->form_validation->set_rules('business_name', 'Business Name', 'trim|required');

            if ($this->form_validation->run()) {

                if ($_FILES && $_FILES['pic']['name']) {
					
					mkdir("./assets/uploads/pics", 0777);
					chmod("./assets/uploads/pics", 0777);
					mkdir("./assets/uploads/pics/profile", 0777);
					chmod("./assets/uploads/pics/profile", 0777);
					
				   $config['upload_path'] = './assets/uploads/pics/profile/';
				   $config['allowed_types'] = 'gif|jpg|png|jpeg';
				   $config['file_name'] = 'profile_pic'.$this->member_id;
				   $this->load->library('upload', $config);	
				   $this->upload->overwrite = true;			
				   if(!$this->upload->do_upload('pic'))
					 {  $output['error'] = $error = $this->upload->display_errors();  } else {
					   $data = array('upload_data' => $this->upload->data());									
					 }
                                        
                }
              if($error=='')
			   {
                $this->settings_model->updateUser($this->member_id, $data['upload_data']['file_name']);
				if($data['upload_data']['file_name'])
				$this->session->set_userdata('profile_pic',$data['upload_data']['file_name']);
				$this->session->set_userdata('business_name',$this->input->post('business_name'));
                $this->session->set_userdata('success_msg', 'Saved Successfully.');
                redirect('settings/user','refresh');
			  }
            }
        }

        if ($_POST['email_submit'] == 'email_submit') {
            $this->form_validation->set_rules('mail', 'Mail Address', 'trim|required|is_unique[tbl_from_email.email]|valid_email');
            if ($this->form_validation->run()) {
                $mail = $this->input->post('mail');
				$varification_code = base64_encode(time().'-'.$this->member_id);
                $this->settings_model->insertMail($mail, $this->member_id,$varification_code);
				
				$subject = 'From Email Confirmation Email';
				$content = 'Hi,<br/> You have added in from email list of '.$user_details->business_name.'. <br/> Please confirm your emailid by clicking on link below <br/> <a href="'.site_url('from-email-verify/'.$varification_code).'">'.site_url('from-email-verify/'.$varification_code)."</a> <br/><br/><br/>
				To Your Success,<br/>
				MailZingo Team ";
				
				$message = file_get_contents('./assets/email_template/index.html');
				$message = str_replace('{image_path}',site_url().'assets/email_template/',$message);
				$message = str_replace('{#Email_Content#}',$content,$message);
				
				$mailArray = array(
			        "subject" => $subject,
				    "message" => $message,
					"to" => $this->input->post('mail'),
					"from_name" => $user_details->business_name,
					"from_email" => $user_details->email,
					"reply_to" => $user_details->email,
					"attatchFiles" => '',
					"unsubscribe_link" => '',
					"newsletter_id" => '',
					"member_id" => $this->member_id
				);
			    $this->sendmail($mailArray);
				
				
                $this->session->set_userdata('success_msg', 'Mail Address saved Successfully.');
                redirect('settings/user');
            }
        }
       
	   $this->load->view($this->config->item('templateName') . '/header', $output);
        $this->load->view($this->config->item('templateName') . '/settings/user_setting');
        $this->load->view($this->config->item('templateName') . '/footer');
    }
    public function delete_mail($id) {
        $this->settings_model->deleteMail($id);
        $this->session->set_userdata('success_msg', 'Mail Address Deleted Successfully');
        redirect('settings/user');
    }
	public function fromEmailVerify($verification_code) {
	
	  $this->settings_model->verifyEmail($verification_code);
	  $this->session->set_userdata('success_msg', 'Email Verified Successfully.');
      redirect('info');

	}
    public function app_setting() {
		$output['meta_title'] = "Settings App";
        $output['app_details'] = $app_details = $this->settings_model->getWeb($this->member_id);
        $output['page'] = 'app';
		$output['license_key'] = $app_details->license_key;
		if($_POST)
		{
		    $output['license_key'] = $license_key = $this->input->post('license_key');
			$this->form_validation->set_rules('app_url', 'App. url', 'trim|required');
			$this->form_validation->set_rules('database', 'Database', 'trim|required');
			$this->form_validation->set_rules('license_key', 'License', 'trim|required');
			if ($this->form_validation->run()) {

					$update_array = array(
						'license_key' => $license_key
					);
					$this->settings_model->updateApp($this->member_id, $update_array);
					$this->session->set_userdata('success_msg', 'Saved Successfully');
	
					redirect('settings/app');
				}
		}
        $this->load->view($this->config->item('templateName') . '/header', $output);
        $this->load->view($this->config->item('templateName') . '/settings/app_setting');
        $this->load->view($this->config->item('templateName') . '/footer');
    }
    public function mail_setting() {
	$output['meta_title'] = "Settings Mail";
	  if($this->smtp_setup!='yes')
	  {
	    $this->session->set_userdata('error_msg','Please upgrade to use SMTP feature. <a target="_blank" href="'.$this->upgrade_link.'" class="upgradelink">Click here to upgrade </a>');
	    redirect('settings/user');
	  }
		
        $output['mail_details'] = $mail_details = $this->settings_model->getWeb($this->member_id);
		
        $output['page'] = 'mail';
        $output['mail_type'] = $mail_details->mail_type;
		$output['SMTP_hostname'] = $mail_details->SMTP_hostname;
		$output['SMTP_username'] = $mail_details->SMTP_username;
		$output['SMTP_password'] = $mail_details->SMTP_password;
		$output['SMTP_port'] = $mail_details->SMTP_port;
		$output['mail_per_hour'] = $mail_details->mail_hour_limit;
		
		if($output['mail_type'] == 'elasticmail')
		{
			 $output['external_active_option'] ='elasticmail'; 
			 $output['mail_type'] = 'external';
			 
		}
		else if($output['mail_type'] == 'amazon_ses')
		{
			 $output['external_active_option'] ='amazon_ses'; 
			 $output['mail_type'] = 'external';
		}
		else if($output['mail_type'] == 'sparkpost')
		{
			$output['external_active_option'] ='sparkpost'; 
			$output['mail_type'] = 'external';
			
		}
		else if($output['mail_type'] == 'postmark')
		{
			$output['external_active_option'] ='postmark'; 	
			 $output['mail_type'] = 'external';
				
			 
		}
		else if($output['mail_type'] == 'mailgun')
		{
			$output['external_active_option'] ='mailgun'; 	
			 $output['mail_type'] = 'external';
		}
		else if($output['mail_type'] == 'sendgrid')
		{
			$output['external_active_option'] ='sendgrid'; 	
			 $output['mail_type'] = 'external';
		}
		
			 $output['elastic_data'] = $this->settings_model->getExternalApi('elasticmail');
			if($output['elastic_data']){	
				 $output['elastic_sending_email'] = $output['elastic_data']['sending_email'];
				 $output['elastic_api_key'] = $output['elastic_data']['api_key']; }
			$output['amazon_data'] = $this->settings_model->getExternalApi('amazon_ses');
			if($output['amazon_data']){
				 $output['amazon_sending_email'] = $output['amazon_data']['sending_mail'];
				 $output['amazon_api_key'] = $output['amazon_data']['secret_key'];
				 $output['amazon_api_secret'] = $output['amazon_data']['secret_password']; 
				 $output['amazon_region'] = $output['amazon_data']['region'];
				 }
			
				$output['sparkpost_data'] = $this->settings_model->getExternalApi('sparkpost');
				if($output['sparkpost_data']){
					$output['sparkpost_sending_email'] = $output['sparkpost_data']['sending_email'];
					$output['sparkpost_api_key'] = $output['sparkpost_data']['api_key'];
					
					}
		  
				$output['postmark_data'] = $this->settings_model->getExternalApi('postmark'); 
				
			if($output['postmark_data']){ 
			
				$output['postmark_sending_email'] = $output['postmark_data']['sending_email'];
				$output['postmark_api_key'] = $output['postmark_data']['api_key'];
				}
			
				$output['mailgun_data'] = $this->settings_model->getExternalApi('mailgun');
				if($output['mailgun_data']){
						$output['mailgun_api_key']= $output['mailgun_data']['api_key'];
						$output['mailgun_sending_domain']= $output['mailgun_data']['sending_domain'];
						$output['mailgun_sending_email']= $output['mailgun_data']['sending_email'];
				}
				$output['sendgrid_data'] = $this->settings_model->getExternalApi('sendgrid');
				if($output['sendgrid_data']){
						$output['sendgrid_api_key']= $output['sendgrid_data']['api_key'];
						$output['sendgrid_sending_email']= $output['sendgrid_data']['sending_mail'];
				}
					
        if ($_POST) {
            $output['mail_type'] = $mail_type = $this->input->post('mail_type');
            $this->form_validation->set_rules('mail_type', 'mail_type', 'trim|required');
            $this->form_validation->set_rules('mail_per_hour', 'Mail Per Hour', 'trim|numeric|callback_numcheck');
			if ($mail_type == 'smtp') {

				$output['SMTP_hostname'] = $this->input->post('SMTP_hostname');
				$output['SMTP_username'] = $this->input->post('SMTP_username');
				$output['SMTP_password'] = $this->input->post('SMTP_password');
				$output['SMTP_port'] = $this->input->post('SMTP_port');

				$this->form_validation->set_rules('SMTP_hostname', 'Hostname', 'trim|required');
                $this->form_validation->set_rules('SMTP_username', 'Username', 'trim|required');
                $this->form_validation->set_rules('SMTP_password', 'Password', 'trim|required');
                $this->form_validation->set_rules('SMTP_port', 'Port', 'trim|required');
            }
			else if($mail_type == 'external')
			{
				
				$this->form_validation->set_rules('option_name', 'External Option', 'trim|required'); 
				
				if($this->input->post('option_name')=='amazon_ses'){
					$option = $this->input->post($this->input->post('option_name'));
					$output['external_active_option'] ='amazon_ses';
					$output['amazon_sending_email'] = $option['sending_mail'];
					$output['amazon_api_key'] = $option['secret_key'];
					$output['amazon_api_secret'] = $option['secret_password'];
					$output['amazon_region'] = $option['region'];
					 
				}
				else if($this->input->post('option_name')=='sparkpost'){
					$option = $this->input->post($this->input->post('option_name'));
					$output['external_active_option'] ='sparkpost';
					$output['sparkpost_sending_email'] = $option['sending_email'];
					$output['sparkpost_api_key'] = $option['api_key'];
					 
				}
				else if($this->input->post('option_name')=='postmark'){
					$option = $this->input->post($this->input->post('option_name'));
					 
					$output['external_active_option'] ='postmark';
					$output['postmark_sending_email'] = $option['sending_email'];
					$output['postmark_api_key'] = $option['api_key'];
				}
				else if($this->input->post('option_name')=='mailgun'){
					$option = $this->input->post($this->input->post('option_name'));
					$output['external_active_option'] ='mailgun';
					$output['mailgun_api_key']= $option['api_key'];
					$output['mailgun_sending_domain']= $option['sending_domain'];
					$output['mailgun_sending_email']= $option['sending_email'];
				}
				else if($this->input->post('option_name')=='elasticmail'){
					$option = $this->input->post($this->input->post('option_name'));
					$output['external_active_option'] ='elasticmail';
					$output['elastic_sending_email'] = $option['sending_email'];
					$output['elastic_api_key'] = $option['api_key'];
					$output['amazon_region'] = $option['region'];
				}
				else if($this->input->post('option_name')=='sendgrid'){
					$option = $this->input->post($this->input->post('option_name'));
					$output['external_active_option'] ='sendgrid';
					$output['sendgrid_sending_email'] = $option['sending_email'];
					$output['sendgrid_api_key'] = $option['api_key'];
				}
			}
            if ($this->form_validation->run()) {
			
			if ($mail_type == 'smtp') { 
			   
			   $user_details = $this->settings_model->getUser($this->member_id);
			   $mailArray['from_name'] = $user_details->business_name;
			   $mailArray['from_email'] = $user_details->email;
			   $mailArray['reply_to'] = $user_details->email;
			   $mailArray['message'] = 'SMTP working fine.';
			   $mailArray['subject'] = 'SMTP Test Mail';
			   $mailArray['contact_email'] = $user_details->email;
			   $mailArray['SMTP_hostname'] = $this->input->post('SMTP_hostname');
			   $mailArray['SMTP_username'] = $this->input->post('SMTP_username');
			   $mailArray['SMTP_password'] = $this->input->post('SMTP_password');
			   $mailArray['SMTP_port'] = $this->input->post('SMTP_port');

			   $response = $this->mailsending_model->sendSMTPTestMail($mailArray);
			   
			   if($response =='error')
			    {
				  $output['error'] = 'Please check SMTP settings you have provided';
				} else {
                $this->settings_model->updateMailSettings($this->member_id);
                $this->session->set_userdata('success_msg', 'Saved Successfully');
                redirect('settings/mail');
				}
			 } 
			else if($mail_type == 'external')
				{
					$option_name = $this->input->post('option_name');

					$user_details = $this->settings_model->getUser($this->member_id);
					$contact_email = $user_details->email;
					if($option_name == 'amazon_ses'){
						require_once 'amazon_ses/autoload.php';
						$output['external_active_option'] = 'amazon_ses';
						$amazon_credentials = $this->input->post($option_name);
						$output['amazon_sending_mail'] = $sending_email = $amazon_credentials['sending_mail'];
						$output['amazon_secret_key'] = $amazon_key = $amazon_credentials['secret_key'];
						$output['amazon_secret_password'] = $amazon_password = $amazon_credentials['secret_password'];
						$output['amazon_region'] = $amazon_region = $region = $amazon_credentials['region'];
						$response = $this->mailsending_model->sendAmazonTestMail($contact_email,$sending_email,$amazon_key,$amazon_password,$amazon_region);
						if($response == 'error')
						{
							$output['error'] = 'Please check Amazon Credentials you have provided';
						} else {
							 $this->settings_model->updateExternalMailSettings($this->member_id,$option_name,$amazon_credentials);
							 $this->session->set_userdata('success_msg', 'Saved Successfully');
							 redirect('settings/mail');
						}
					}
					else if($option_name == 'sparkpost')
					{
						$output['external_active_option'] = 'sparkpost';
						$sparkpost_credentials = $this->input->post($option_name);
						$httpClient = new GuzzleAdapter(new Client());
						$api_key = $sparkpost_credentials['api_key'];
						$sending_email = $sparkpost_credentials['sending_email'];
						$sparky = new SparkPost($httpClient, ['key'=>$api_key]);
						$response = $this->mailsending_model->sendSparkpostTestMail($httpClient,$sparky,$contact_email,$sending_email);
						if($response == 'error')
						{
							$output['error'] = 'Please check Sparkpost Credentials you have provided';
						}
						else {
							 $this->settings_model->updateExternalMailSettings($this->member_id,$option_name,$sparkpost_credentials);
							 $this->session->set_userdata('success_msg', 'Saved Successfully');
							 redirect('settings/mail');
						}
					}
					else if($option_name == 'postmark')
					{
						$output['external_active_option'] = 'postmark';
						$postmark_credentials = $this->input->post($option_name);
						$httpClient = new GuzzleAdapter(new Client());
						$api_key = $postmark_credentials['api_key'];
						$sending_email = $postmark_credentials['sending_email'];
						$client = new PostmarkClient($api_key);
						$response = $this->mailsending_model->sendPostmarkTestMail($client,$sending_email,$contact_email);
						if($response == 'error')
						{
							$output['error'] = 'Please check Postmark Credentials you have provided';
						}
						else {
							 $this->settings_model->updateExternalMailSettings($this->member_id,$option_name,$postmark_credentials);
							 $this->session->set_userdata('success_msg', 'Saved Successfully');
							 redirect('settings/mail');
						}
					}
					else if($option_name == 'mailgun')
					{
					$output['external_active_option'] = 'mailgun';
					$mailgun_credentials = $this->input->post($option_name);
					$api_key = $mailgun_credentials['api_key'];
					$sending_email = $mailgun_credentials['sending_email'];
					$sending_domain = $mailgun_credentials['sending_domain'];
						$mgClient = new Mailgun($api_key);
						$response = $this->mailsending_model->sendMailgunTestMail($mgClient,$sending_domain,$sending_email,$contact_email);
						if($response == 'error')
						{
							$output['error'] = 'Please check Mailgun Credentials you have provided';
						}
						else {
							 $this->settings_model->updateExternalMailSettings($this->member_id,$option_name,$mailgun_credentials);
							 $this->session->set_userdata('success_msg', 'Saved Successfully');
							 redirect('settings/mail');
						} 
					}
					else if($option_name == 'elasticmail')
					{
						$output['external_active_option'] = 'elasticmail';
						$elastic_credentials = $this->input->post($option_name);
						$api_key = $elastic_credentials['api_key'];
						$sending_email = $elastic_credentials['sending_email'];
						try{
							$elasticEmail = new \ElasticEmail\ElasticEmailV2($api_key);
							$response = $this->mailsending_model->sendElasticTestMail($elasticEmail,$contact_email,$sending_email);
						}
						catch(Exception $e){
							$response = 'error';	
						}
						if($response == 'error')
						{
							$output['error'] = 'Please check Elasticmail Credentials you have provided';
						}
						else {
							 $this->settings_model->updateExternalMailSettings($this->member_id,$option_name,$elastic_credentials);
							 $this->session->set_userdata('success_msg', 'Saved Successfully');
							 redirect('settings/mail');
						} 
					}
					else if($option_name == 'sendgrid')
					{
						$output['external_active_option'] = 'sendgrid';
						$sendgrid_credentials = $this->input->post($option_name);
						
						$api_key = $sendgrid_credentials['api_key'];
						$sending_email = $sendgrid_credentials['sending_mail'];
						$response = $this->mailsending_model->sendSendgridTestMail($api_key,$sending_email,$contact_email);	
						if($response == 'error')
						{
							$output['error'] = 'Please check Elasticmail Credentials you have provided';
						}
						else {
							 $this->settings_model->updateExternalMailSettings($this->member_id,$option_name,$sendgrid_credentials);
							 $this->session->set_userdata('success_msg', 'Saved Successfully');
							 redirect('settings/mail');
						} 
					
					}	
				
				}
			 else {
			    
				 $this->settings_model->updateMailSettings($this->member_id);
                $this->session->set_userdata('success_msg', 'Saved Successfully');
                redirect('settings/mail');
			 
			 } 
            
			
			} 
		}

        $this->load->view($this->config->item('templateName') . '/header', $output);
        $this->load->view($this->config->item('templateName') . '/settings/mail_setting');
        $this->load->view($this->config->item('templateName') . '/footer');
    }
	function numcheck($in) {
	 if (intval($in) < 60) {
	   $this->form_validation->set_message('numcheck', 'Minimum Value of mail per hour is 60');
	   return FALSE;
	  } else {
	   return TRUE;
	  }
	}
	public function sendTestMail()	{
	  if($_POST)
	   {
		   $this->form_validation->set_rules('SMTP_hostname','SMTP Hostname','trim|required');
		   $this->form_validation->set_rules('SMTP_username','SMTP Username','trim|required');
		   $this->form_validation->set_rules('SMTP_password','SMTP Password','trim|required');
		   $this->form_validation->set_rules('SMTP_port','SMTP Port','trim|required');
		   $this->form_validation->set_rules('testemail','Test Email','trim|required|valid_email');

		  if($this->form_validation->run())
		  {    
			   $user_details = $this->settings_model->getUser($this->member_id);
			   $mailArray['from_name'] = $user_details->business_name;
			   $mailArray['from_email'] = $user_details->email;
			   $mailArray['reply_to'] = $user_details->email;
			   $mailArray['message'] = 'SMTP working fine.';
			   $mailArray['subject'] = 'SMTP Test Mail';
			   $mailArray['contact_email'] = $this->input->post('testemail');
			   $mailArray['SMTP_hostname'] = $this->input->post('SMTP_hostname');
			   $mailArray['SMTP_username'] = $this->input->post('SMTP_username');
			   $mailArray['SMTP_password'] = $this->input->post('SMTP_password');
			   $mailArray['SMTP_port'] = $this->input->post('SMTP_port');

			   $response = $this->mailsending_model->sendSMTPTestMail($mailArray);
			   if($response=='error')
			   {
			     $data['error'] = 'Unable to send mail using provided settings.';
			   } else {
			     $data['error'] = false;
			   }
		   } else { 
			   $data['error'] = validation_errors();
			   
		   }
	   }
	   $data['success'] = true;
	   echo json_encode($data); die;
	}
    public function bounce_setting() {
	$output['meta_title'] = "Settings Bounce";
	if($this->bounce_setup!='yes')
	  {
	    $this->session->set_userdata('error_msg','Please upgrade to use bounce feature. <a target="_blank" href="'.$this->upgrade_link.'" class="upgradelink">Click here to upgrade </a>');
	    redirect('settings/user');
	  }
	  
        $output['bounce_details'] = $bounce_details = $this->settings_model->getWeb($this->member_id);
        $output['page'] = 'bounce';
        $output['bounce_type'] = $bounce_details->bounce_type;
        $output['bounce_address'] = $bounce_details->bounce_address;
        $output['bounce_server'] = $bounce_details->bounce_server;
       // $output['bounce_username'] = $bounce_details->bounce_username;
        $output['bounce_password'] = $bounce_details->bounce_password;
		$output['bounce_port'] = $bounce_details->bounce_port;
		$output['account_type'] = $bounce_details->account_type;

        if ($_POST) {
            $output['bounce_type'] = $bounce_type = $this->input->post('bounce_type');
            $output['bounce_address'] = $bounce_address = $this->input->post('bounce_address');
            $output['bounce_server'] = $bounce_server = $this->input->post('bounce_server');
           // $output['bounce_username'] = $bounce_username = $this->input->post('bounce_username');
            $output['bounce_password'] = $bounce_password = $this->input->post('bounce_password');
            $output['bounce_port'] = $bounce_port = $this->input->post('bounce_port');
            $output['account_type'] = $account_type = $this->input->post('account_type');
			
            $this->form_validation->set_rules('bounce_type', 'bounce_type', 'trim|required');

            if ($bounce_type == 'custom') {

                $this->form_validation->set_rules('bounce_type', 'Bounce Type', 'trim|required');
                $this->form_validation->set_rules('bounce_address', 'Address', 'trim|required');
                $this->form_validation->set_rules('bounce_server', 'Server', 'trim|required');
             //   $this->form_validation->set_rules('bounce_username', 'User', 'trim|required');
                $this->form_validation->set_rules('bounce_password', 'Password', 'trim|required');
                $this->form_validation->set_rules('account_type', 'Account Type', 'trim|required');
                $this->form_validation->set_rules('bounce_port', 'Bounce Port', 'trim|required');
            }
            if ($this->form_validation->run()) {
			    
			$iserror= false;
			if ($bounce_type == 'custom') {
				$hostname = '{'.$bounce_server.':'.$bounce_port.'/'.$account_type.'/ssl}INBOX';
				//$hostname = '{outlook.office365.com:993/imap/ssl}INBOX';
				$inbox = imap_open($hostname,$bounce_address,$bounce_password) or $error = imap_last_error();
				if($error)
				$iserror= true;
			 }
			  if($iserror==false)
			   {
                 $this->settings_model->updateBounceSettings($this->member_id);
                 $this->session->set_userdata('success_msg', 'Saved Successfully');
                 redirect('settings/bounce');
			   } else {
			     $output['errormsg'] = 'Unable to connet to server using provided settings.';
			   }
            }
        }
        $this->load->view($this->config->item('templateName') . '/header', $output);
        $this->load->view($this->config->item('templateName') . '/settings/bounce_setting');
        $this->load->view($this->config->item('templateName') . '/footer');
    }
    public function label_setting() {

        $output['label_details'] = $label_details = $this->settings_model->getWeb($this->member_id);
        $output['page'] = 'label';
		$output['app_name'] = $label_details->app_name;
		$output['app_footer'] = $label_details->app_footer;
		$output['app_logo'] = $label_details->app_logo;
		$output['favicon'] = $label_details->favicon;
		if($_POST)
		{
			$this->form_validation->set_rules('app_name', 'App Name', 'trim|required');
			$this->form_validation->set_rules('app_footer', 'App Name', 'trim|required');
			
		$output['app_name'] = $app_name = $this->input->post('app_name');
		$output['app_footer'] = $app_footer = $this->input->post('app_footer');

			if ($this->form_validation->run()) {
	
					if ($_FILES && $_FILES['file1']['name']) {
						
						mkdir("./assets/uploads/pics", 0777);
						chmod("./assets/uploads/pics", 0777);
						mkdir("./assets/uploads/pics/logo", 0777);
						chmod("./assets/uploads/pics/logo", 0777);
						
					   $config['upload_path'] = './assets/uploads/pics/logo/';
					   $config['allowed_types'] = 'gif|jpg|png|jpeg';
					   $config['file_name'] = 'logo'.$this->member_id;
					   $this->load->library('upload', $config);	
					   $this->upload->overwrite = true;			
					   if(!$this->upload->do_upload('file1'))
						 {  $this->upload->display_errors();  } else {
						   $data = array('upload_data' => $this->upload->data());									
						 }
					   $logo_image =  $data['upload_data']['file_name'];
					}
					
					if ($_FILES && $_FILES['file2']['name']) {
						
						mkdir("./assets/uploads/pics", 0777);
						chmod("./assets/uploads/pics", 0777);
						mkdir("./assets/uploads/pics/favicon", 0777);
						chmod("./assets/uploads/pics/favicon", 0777);
						
					   $config['upload_path'] = './assets/uploads/pics/favicon/';
					   $config['allowed_types'] = 'gif|jpg|png|jpeg';
					   $config['file_name'] = 'favicon'.$this->member_id;
					   $this->load->library('upload', $config);	
					   $this->upload->overwrite = true;			
					   if(!$this->upload->do_upload('file2'))
						 {  $this->upload->display_errors();  } else {
						   $data1 = array('upload_data' => $this->upload->data());									
						 }
					   $favicon_img =  $data1['upload_data']['file_name'];
					}
	
					$this->settings_model->updateLabelSettings($this->member_id,$logo_image,$favicon_img);
					$this->session->set_userdata('success_msg', 'Saved Successfully');
					redirect('settings/label');
			}
		}

        $this->load->view($this->config->item('templateName') . '/header', $output);
        $this->load->view($this->config->item('templateName') . '/settings/label_setting');
        $this->load->view($this->config->item('templateName') . '/footer');
    }
    public function cron_setting()	{
		$output['meta_title'] = "Settings Cron";
	     $output['page'] = 'cron';
        $this->load->view($this->config->item('templateName') . '/header', $output);
        $this->load->view($this->config->item('templateName') . '/settings/cron_setting');
        $this->load->view($this->config->item('templateName') . '/footer');
	}
	
    public function change_password() {
		$output['meta_title'] = 'Settings Change-password';
        $user_details = $this->settings_model->getUser($this->member_id);
		$output['profile_pic'] = $user_details->profile_pic;

        if ($_POST) {

            $output['name']   = $this->input->post('name');
			$output['country']  =  $this->input->post('country');
			$output['address']  = $this->input->post('address');
			$output['state']  =  $this->input->post('state');
			$output['pin_code']  =  $this->input->post('pin_code');
			$output['city'] =  $this->input->post('city');
			$output['business_name']  =  $this->input->post('business_name');

			$this->form_validation->set_rules('current_password','Current Password','trim|required|callback_CurrPassCheck');
			$this->form_validation->set_rules('new_password','New Password','trim|required|matches[confirm_pass]');
			$this->form_validation->set_rules('confirm_pass','Confirm Password','trim|required');

            if($this->form_validation->run())
			  {
				 $this->settings_model->updatePassword();
				 $this->session->set_userdata('success_msg','Password Changed Successfully.');
				 redirect('settings/change-password');
			  } else {
			   $output['error'] = validation_errors();
			  }
        }
       
	   $output['page'] = 'change_password';
	   $this->load->view($this->config->item('templateName') . '/header', $output);
       $this->load->view($this->config->item('templateName') . '/settings/change_password');
       $this->load->view($this->config->item('templateName') . '/footer');
    }
	public function CurrPassCheck($curr_password) {
        
		$result = $this->common_model->getSingleFieldFromAnyTable('password','mem_id',$this->session->userdata('mem_id'),'tbl_member');
		if($result!=md5($curr_password)) 
		{
			  $this->form_validation->set_message('CurrPassCheck', 'Current Password is wrong');
			   return FALSE;
		} else {
			   return TRUE;
		}
    }
	public function social_settings()
	{
		$output['meta_title'] = "Social Settings";
		$social_details = $this->settings_model->getSocialOptions($this->member_id);
		$output['fb_url'] = $social_details->fb_url;
		$output['tw_url'] = $social_details->tw_url;
		$output['insta_url'] = $social_details->insta_url;
		$output['gplus_url'] = $social_details->gplus_url;
		
		if($_POST)
		{
			$this->form_validation->set_rules('fb_url', 'Facebook Url', 'trim|callback_valid_url_format['.$this->input->post("fb_url").']');
			$this->form_validation->set_rules('tw_url', 'Twitter Url', 'trim|callback_valid_url_format['.$this->input->post("tw_url").']');
			$this->form_validation->set_rules('insta_url', 'Instagram Url', 'trim|callback_valid_url_format['.$this->input->post("insta_url").']');
			$this->form_validation->set_rules('gplus_url', 'Google Url', 'trim|callback_valid_url_format['.$this->input->post("gplus_url").']');
			
		$output['fb_url'] = $this->input->post('fb_url');
		$output['tw_url'] = $this->input->post('tw_url');
		$output['insta_url'] = $this->input->post('insta_url');
		$output['gplus_url'] = $this->input->post('gplus_url');
		 
			if ($this->form_validation->run()) {
	
					 
					$this->settings_model->updateSocialSettings($this->member_id);
					$this->session->set_userdata('success_msg', 'Saved Successfully');
					redirect('settings/social-settings');
			}
			else {
			   $output['error'] = validation_errors();
			  }
		}

		$output['profile_pic'] = $user_details->profile_pic;
		$output['page'] = 'social_settings';
		$this->load->view($this->config->item('templateName') . '/header', $output);
        $this->load->view($this->config->item('templateName') . '/settings/social_settings');
        $this->load->view($this->config->item('templateName') . '/footer');
		
	}
	function valid_url_format($str){
		$pattern = "|^http(s)?://[a-z0-9-]+(.[a-z0-9-]+)*(:[0-9]+)?(/.*)?$|i";
		if($str!=''){
        if (!preg_match($pattern, $str)){
			$this->form_validation->set_message('valid_url_format', 'The %s you entered is not valid.');
           return FALSE;
        }
		}
        return TRUE;
    }
	public function sendMail($mailArray)
	{
	   $smtpDetail = $this->newsletter_model->getSMTPDetail($mailArray['member_id']);
	   $mailArray['mail_type'] = $mail_type = $smtpDetail->mail_type;
	   $mailArray['SMTP_hostname'] = $smtpDetail->SMTP_hostname;
	   $mailArray['SMTP_port'] = $smtpDetail->SMTP_port;
	   $mailArray['SMTP_username'] = $smtpDetail->SMTP_username;
	   $mailArray['SMTP_password'] = $smtpDetail->SMTP_password;
	   $mailArray['bounce_type'] = $smtpDetail->bounce_type;
	   $mailArray['bounce_address'] = $smtpDetail->bounce_address;
		   
	  if($mailArray['mail_type']!='default' && $mailArray['mail_type']!='smtp')
		 {
		   $mailArray['api_value'] = $this->common_model->getSingleFieldFromAnyTable('api_value','api_name',$mail_type,'tbl_external_mail_api');
		 }
		 
	  if($mailArray['mail_type']=='default' || $mailArray['mail_type']=='smtp')
	   {
	     $this->mailsending_model->sendmail($mailArray);
	   } 
	  else if($mailArray['mail_type']=='amazon_ses')
	   {
	     require_once 'amazon_ses/autoload.php';
	     $this->mailsending_model->sendMailUsingAmazon($mailArray);
	   }
	  else if($mailArray['mail_type']=='sparkpost')
	   {
	     $httpClient = new GuzzleAdapter(new Client());
		 $api_value = unserialize($mailArray['api_value']);
		 $api_key = $api_value['api_key'];
		 $sparky = new SparkPost($httpClient, ['key'=>$api_key]);
		 $response = $this->mailsending_model->sendMailUsingSparkpost($mailArray,$sparky);
	   }
	  else if($mailArray['mail_type']=='postmark')
	   {
		 $httpClient = new GuzzleAdapter(new Client());
		 $api_value = unserialize($mailArray['api_value']);
		 $api_key = $api_value['api_key'];
		 $client = new PostmarkClient($api_key);
		 $response = $this->mailsending_model->sendMailUsingPostmark($mailArray,$client);
	   }
	  else if($mailArray['mail_type']=='mailgun')
	   {
		 $api_value = unserialize($mailArray['api_value']);
		 $api_key = $api_value['api_key'];
		 $sending_domain = $api_value['sending_domain'];
		 $sending_email = $api_value['sending_email'];
		 $mgClient = new Mailgun($api_key);
		 $response = $this->mailsending_model->sendMailUsingMailgun($mailArray,$mgClient,$sending_domain,$sending_email);
	   }
	  else if($mailArray['mail_type']=='elasticmail')
	   {
		 $api_value = unserialize($mailArray['api_value']);
		 $api_key = $api_value['api_key'];
		 try{
		     $elasticEmail = new \ElasticEmail\ElasticEmailV2($api_key);
			 $response = $this->mailsending_model->sendMailUsingElastic($mailArray,$elasticEmail);
		 }
		 catch(Exception $e){
							$response = 'error';	
						}
		 
	   }
	   else if($mailArray['mail_type']=='sendgrid'){
	 	 $api_value = unserialize($mailArray['api_value']);
		 $api_key = $api_value['api_key'];
		 $sending_mail = $api_value['sending_mail'];
	  	 try{
				$response = $this->mailsending_model->sendMailUsingSendgrid($mailArray,$api_key,$sending_mail);
			}
			catch(Exception $e){
				$response = 'error';	
			}
	  
	  }
	}
	public function api_settings()
	{
		if($_POST)
		{
			$key = md5(time());
		    $apikey = substr($key, 0, 6).'-'.substr($key, 6, 6).'-'.substr($key, 12, 6).'-'.substr($key, 18, 8);
			$this->settings_model->updateAPIKey($apikey,$this->member_id);
		}
		
		$output['meta_title'] = "API Settings";
		$output['api_url'] = site_url('api');
		$output['api_key'] = $this->common_model->getSingleFieldFromAnyTable('api_key','member_id',$this->member_id,'tbl_website_settings');
		$output['page'] = 'api_settings';
		$this->load->view($this->config->item('templateName') . '/header', $output);
        $this->load->view($this->config->item('templateName') . '/settings/api_settings');
        $this->load->view($this->config->item('templateName') . '/footer');
		
	}
	
}