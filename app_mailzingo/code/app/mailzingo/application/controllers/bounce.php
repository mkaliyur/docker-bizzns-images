<?php
defined('BASEPATH') OR exit('No direct script access allowed');
include('app.php');

class Bounce extends App {

	function __construct()
	{ 
	    parent::__construct();				
	}
	public function index()
	{

	   $this->db->select('bounce_type,bounce_server,bounce_address,bounce_password,bounce_port,account_type');
	   $query = $this->db->get('tbl_website_settings');
	   $data = $query->row();
	   
	   if($data->bounce_type=='default')
	   { return true; }
	   
	    $hostname = '{'.$data->bounce_server.':'.$data->bounce_port.'/'.$data->account_type.'/ssl}INBOX';
		$username = $data->bounce_address;
		$password = $data->bounce_password;
		
		/* try to connect */
		$inbox = imap_open($hostname,$username,$password) or die('Cannot connect to Gmail: ' . imap_last_error());
		$emails = imap_search($inbox,'FROM "Mail Delivery System"');
		//imap_search($conn, 'SUBJECT "HOWTO be Awesome" SINCE "8 August 2008"', SE_UID);
		//echo '<pre>'; print_r($emails); die('gg');
	  
	  if($emails) {
		
		/* begin output var */
		$output = '';
		
		/* put the newest emails on top */
		rsort($emails);
		
		/* for every email... *///8553 
		//$email_number = 8558;
		foreach($emails as $email_number) {
		
			/* get information specific to this email */
			//$overview = imap_fetch_overview($inbox,$email_number,0);
			//$message = imap_fetchbody($inbox,$email_number,2);
			
			//$header = imap_fetchheader($inbox, $email_number);
			$body = imap_body($inbox,$email_number);
			//echo "<pre>";
			//imap_mail_move($inbox, 8587, '[Gmail]/Trash');
			$stats_id = explode('{emailpro}',$body)[1];
			if($stats_id)
			 {
			  $urls = array(site_url('bounce-'.$stats_id.'email'));
	          $this->curl_post_async($urls);
			  imap_delete($inbox, $email_number);
			 }
		
		}
			
		} 
		
		/* close the connection */
		imap_close($inbox);
	}
	public function curl_post_async($urls)
	{
		// initialize the multihandler
			$mh = curl_multi_init();
			
			$channels = array();
			foreach ($urls as $key => $url) {
				// initiate individual channel
				$channels[$key] = curl_init();
				curl_setopt_array($channels[$key], array(
					CURLOPT_URL => $url,
					CURLOPT_RETURNTRANSFER => true,
					CURLOPT_FOLLOWLOCATION => true
				));
			
				// add channel to multihandler
				curl_multi_add_handle($mh, $channels[$key]);
			}
			
			// execute - if there is an active connection then keep looping
			$active = null;
			do {
				$status = curl_multi_exec($mh, $active);
			}
			while ($active && $status == CURLM_OK);
			
			// echo the content, remove the handlers, then close them
			foreach ($channels as $chan) {
				echo curl_multi_getcontent($chan);
				curl_multi_remove_handle($mh, $chan);
				curl_close($chan);
			}
			
			// close the multihandler
			curl_multi_close($mh);
	}
}