<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Error extends CI_Controller {

	
    function __construct() {
        parent::__construct();
    }
	 
	public function _404() {
	
		$this->load->view($this->config->item('templateName').'/header');
		$this->load->view($this->config->item('templateName').'/404_error');
		$this->load->view($this->config->item('templateName').'/footer');
	}
}

