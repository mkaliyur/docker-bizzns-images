<?php
defined('BASEPATH') OR exit('No direct script access allowed');
include('app.php');  
static $_log;
$_log =& load_class('Log');
$_log->write_log("INFO", "test Message", false);

class Login extends App {

    function __construct() {
        parent::__construct();
        $this->load->model('member_model');
		$this->load->model('mailsending_model');
    }

    function checkLogin() {
        if ($this->session->userdata('mem_id') != '') {
            redirect('dashboard');
        }
    }

    public function index() { 

        $output['meta_title'] ="Login Page"; 
		$this->checkLogin();
        if ($_POST) {

            $output['email'] = $this->input->post('email');
            $output['password'] = $this->input->post('password'); 

            $this->form_validation->set_rules('email', 'Email', 'trim|required');
            $this->form_validation->set_rules('password', 'Password', 'trim|required');

            if ($this->form_validation->run()) {
                $response = $this->member_model->checkLogin(); 

                if (!empty($response)) {
                    $this->session->set_userdata('mem_id', $response->mem_id);
                    $this->session->set_userdata('username', $response->name);
                    $this->session->set_userdata('profile_pic', $response->profile_pic);
					$this->session->set_userdata('business_name', $response->business_name);
					$this->session->set_userdata('email', $response->email);
					$this->session->set_userdata('login_update', 'yes');
                    redirect('dashboard');
                } else {
                    $error_msg = 'Invalid login details.';
                    $output['error_msg'] = $error_msg;
                }
            } else {
                $error_msg = validation_errors();
                $output['error_msg'] = $error_msg;
            }
        }
		
        $this->load->view($this->config->item('templateName') . '/login', $output);
    }
    public function forget_password() {
        if ($_POST) {
            $this->form_validation->set_rules('email', 'Mail Address', 'trim|required|valid_email');
            if ($this->form_validation->run()) {
                $user = $this->member_model->checkEmailExist();
                if ($user) {
                    $time = strtotime('+1 day', time());
                    $rand_value = base64_encode($time . '-' . $user->mem_id);
                    $this->member_model->insert_forget_details($rand_value, $user->mem_id);
					$subject = "Forget Password Email";
                    $content = "Hi,<br/> <br/> <br/> 
					            We have received a password reset request for your MailZingo account.<br/><br/> 
								Please click the following link if you forgot your password <a href=".site_url('new-password/'.$rand_value).">".site_url('new-password/'.$rand_value)."</a><br/><br/> 
								If you did not ask to reset your password, then you can ignore this email and your password will not be changed.<br/><br/><br/> 
								To Your Success,<br/><br/> 
								MailZingo Team";
					$message = file_get_contents('./assets/email_template/index.html');
					$message = str_replace('{image_path}',site_url().'assets/email_template/',$message);
					$message = str_replace('{#Email_Content#}',$content,$message);
					
					
					$mailArray = array(
			        "subject" => $subject,
				    "message" => $message,
					"to" => $this->input->post('email'),
					"from_name" => $user->business_name,
					"from_email" => $user->email,
					"reply_to" => $user->email,
					"attatchFiles" => '',
					"unsubscribe_link" => '',
					"newsletter_id" => '',
					"member_id" => $user->mem_id
				   );

			       $this->mailsending_model->sendmail($mailArray);
                    $this->session->set_userdata('success_msg', 'Verification Link Sent Successfully!!');
                    redirect('forget-password');
                } else {
                    $output['error'] = 'Mail Address Not Found';
                }
            }
        }
        $this->load->view('default/forget_password', $output);
    }

    function new_password($var) {
        $randomValue = base64_decode($var);
        $arr = explode('-', $randomValue);


        if ($arr[0] > time()) {
            $row = $this->member_model->getTableRow($arr[1]);
            if ($row->forget_pass_code == $var) {
                $data['is_valid'] = 'yes';

                if ($_POST) {
				    $this->form_validation->set_rules('pass1', 'Password','trim|min_length[6]|required');
                    $this->form_validation->set_rules('pass2', 'Confirm password', 'trim|required|min_length[6]|matches[pass1]');
                    if ($this->form_validation->run()) {
                        $this->member_model->updateForgetPassword($arr[1]);
                        $this->member_model->update_forget_details1($arr[1]);
                        redirect('login');
                    }else{
                        $data['error_msg'] = "Please fill details correctly";
                    }
                }
            }
        } else {
            $data['is_valid'] = 'no';
        }

        $this->load->view('default/create_password', $data);
    }

    function check_equal_less($second_field, $first_field) {
        if ($second_field = $first_field) {
            $this->form_validation->set_message('check_equal_less', 'The First &amp;/or Second fields have errors.');
            return false;
        } else {
            return true;
        }
    }

    public function logout() {
        $this->session->sess_destroy();
        redirect('login');
    }

    function changePassword() {

        if ($this->session->userdata('mem_id') == '') {
            redirect('login');
        }

        if ($_POST) {
            $this->form_validation->set_rules('curr_password', 'Current Password', 'trim|required|callback_CurrPassCheck');
            $this->form_validation->set_rules('new_pass', 'New Password', 'trim|required|matches[confirm_pass]');
            $this->form_validation->set_rules('confirm_pass', 'Confirm Password', 'trim|required');

            if ($this->form_validation->run()) {
                $this->member_model->updatePassword();

                $data['success'] = true;
            }
            if (validation_errors()) {
                $data['success'] = false;
                $data['error'] = validation_errors();
            } else {
                $data['success'] = true;
                $data['success_msg'] = 'Password Updated Successfully';
                $data['model'] = 'hide';
            }
            echo json_encode($data);
            die;
        }
    }

    public function CurrPassCheck($curr_password) {

        $result = $this->common_model->getSingleFieldFromAnyTable('password', 'mem_id', $this->session->userdata('mem_id'), 'tbl_member');
        if ($result != md5($curr_password)) {
            $this->form_validation->set_message('CurrPassCheck', 'Current Password is wrong');
            return FALSE;
        } else {
            return TRUE;
        }
    }
}
