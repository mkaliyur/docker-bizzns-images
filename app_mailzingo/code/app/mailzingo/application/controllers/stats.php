<?php
defined('BASEPATH') OR exit('No direct script access allowed');
include('app.php');

class Stats extends App {

	function __construct()
	{ 
	    parent::__construct();	
		$this->load->model('statics_model');	
		$this->load->model('newsletter_model');	
		$this->checkLogin();
		$this->member_id = $this->session->userdata('mem_id');		
	}
	function checkLogin()
	{
	  if($this->session->userdata('mem_id')=='')
	  { redirect('login'); }
	  
	   if($this->statistics!='yes')
	  {
	    $this->session->set_userdata('error_msg','Please upgrade to use statistics feature. <a target="_blank" href="'.$this->upgrade_link.'" class="upgradelink">Click here to upgrade </a>');
	    redirect('newsletter-list/sent');
	  }
	  
	}
	public function index()
	{
		$output['meta_title'] = 'Newsletter Statistics';
        $output['allRecord'] = $this->newsletter_model->getNewsletterList($this->member_id,'sent',null,null,null);
		
		$this->load->view($this->config->item('templateName').'/header',$output);
		$this->load->view($this->config->item('templateName').'/message/newsletter/stats/list');
		$this->load->view($this->config->item('templateName').'/footer');
	}
        
    public function statsByNewsletter($newsletter_id,$page_type)
	{
		
		$output['newsletter_id'] = $newsletter_id;
		$output['newsletter_detail'] = $this->newsletter_model->getNewsletterById($this->member_id,$newsletter_id);
		if($page_type=='overview')
		 {
			 $output['meta_title'] = 'Newsletter Statistics Overview';
			$output['total_sent'] = $total_sent = $this->statics_model->getNewsletterStatByFieldType($newsletter_id,'','','newsletter');
			$output['total_click'] = $total_click = $this->statics_model->getNewsletterStatByFieldType($newsletter_id,'click','yes','newsletter');
			$output['total_open'] = $total_open = $this->statics_model->getNewsletterStatByFieldType($newsletter_id,'open','yes','newsletter');
			$output['total_unopen'] = $total_sent-$total_open;
			$output['total_unsubscribe'] = $total_unsubscribe = $this->statics_model->getNewsletterStatByFieldType($newsletter_id,'unsubscribed','yes','newsletter');
			$output['total_bounced'] = $total_bounced = $this->statics_model->getNewsletterStatByFieldType($newsletter_id,'bounced','yes','newsletter');
			$output['total_complaint'] = $total_complaint = $this->statics_model->getNewsletterStatByFieldType($newsletter_id,'complaints','yes','newsletter');
			$output['total_deliverd'] = $total_sent-$total_bounced;
			
	    } else if($page_type=='technology'){ 
		 $output['meta_title'] = 'Newsletter Statistics Technology';
			$output['mobile_open'] = $mobile_open = $this->statics_model->getNewsletterStatByDeviceType($newsletter_id,'mobile','newsletter');
			$output['desktop_open'] = $desktop_open = $this->statics_model->getNewsletterStatByDeviceType($newsletter_id,'desktop','newsletter');
			$output['chrome_open'] = $chrome_open = $this->statics_model->getNewsletterStatByBrowserType($newsletter_id,'chrome','newsletter');
			$output['firefox_open'] = $firefox_open = $this->statics_model->getNewsletterStatByBrowserType($newsletter_id,'firefox','newsletter');
			$output['ie_open'] = $ie_open = $this->statics_model->getNewsletterStatByBrowserType($newsletter_id,'IE','newsletters');
			$output['safari_open'] = $safari_open = $this->statics_model->getNewsletterStatByBrowserType($newsletter_id,'safari','newsletter');
			$output['others_open'] = $others_open = $this->statics_model->getNewsletterStatByBrowserType($newsletter_id,'others','newsletter');
			
		} else if($page_type=='location'){
		     $output['meta_title'] = 'Newsletter Statistics Location';
			 $output['location_array']  = $location_array = json_encode($this->statics_model->getCountryArray($newsletter_id,'newsletter'));
		}

		$output['click_per'] = $total_click/$total_sent*100;
		$output['open_per'] = $total_open/$total_sent*100;
		$output['unsubscribe_per'] = $total_unsubscribe/$total_sent*100;
		$output['bounced_per'] = $total_bounced/$total_sent*100;
		$output['complaint_per'] = $total_complaint/$total_sent*100;
		
		$this->load->view($this->config->item('templateName').'/header',$output);
		$this->load->view($this->config->item('templateName').'/message/newsletter/stats/stats_'.$page_type);
		$this->load->view($this->config->item('templateName').'/footer');
	}
	public function getGraph()
	{
	  $output['time_type'] = $time_type = $this->input->post('time_type');
	  $output['newsletter_id'] = $newsletter_id = $this->input->post('newsletter_id');

	  $output['open_data'] = $this->statics_model->getOpenGraph($time_type,$newsletter_id,$this->member_id,'newsletter');
	  $output['click_data'] = $this->statics_model->getClickGraph($time_type,$newsletter_id,$this->member_id,'newsletter');

	  
	  $response['html'] = $this->load->view($this->config->item('templateName').'/message/newsletter/stats/stats_graph',$output,true);
	  $response['success'] = true;
	  $this->output
		   ->set_content_type('application/json')
		   ->set_output(json_encode($response));
	}
	public function statsReportByNewsletter($newsletter_id,$page_type){
	     
		$output['page_type'] = $page_type;
		if($page_type=='sent')
		$page_type = '';
		$output['email_list'] = $this->statics_model->getStatsEmailByNewsletter($newsletter_id,$page_type,'newsletter');
		$output['newsletter_id'] = $newsletter_id;
		
		
		$this->load->view($this->config->item('templateName').'/header',$output);
		$this->load->view($this->config->item('templateName').'/message/newsletter/stats/stats_report');
		$this->load->view($this->config->item('templateName').'/footer');
	}
	public function export($newsletter_id,$page_type)
	{
	   $message_name = $this->common_model->getSingleFieldFromAnyTable('message_name','id',$newsletter_id,'tbl_newsletter');
	   if($page_type=='sent')
	   $field_type = '';
	   else 
	   $field_type = $page_type;
	   
	   $contact_id = $this->input->post('id');
	   $this->load->dbutil();
	   $this->db->select('email as Email');
	   if($newsletter_id)
	   $this->db->where('newsletter_id',$newsletter_id);
	   if($field_type)
	   $this->db->where($field_type,'yes');
	   $this->db->where('type','newsletter');
       $query = $this->db->get('tbl_statistics');
       $data =  $this->dbutil->csv_from_result($query); 
	   $this->load->helper('download');
	   $filename = $message_name.'_'.$page_type.'_'.date('m-d-Y').'.csv';
       force_download($filename, $data);
	}
}
