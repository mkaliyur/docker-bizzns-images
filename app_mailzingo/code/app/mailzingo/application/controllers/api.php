<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
require 'spark_post/autoload.php'; 
use SparkPost\SparkPost;
use GuzzleHttp\Client; 
use Http\Adapter\Guzzle6\Client as GuzzleAdapter;

require_once('postmark/autoload.php');
use Postmark\PostmarkClient;

require 'mailgun/autoload.php';
use Mailgun\Mailgun;
require 'elastic_mail/autoload.php';
require 'sendgrid/autoload.php';

class Api extends CI_Controller {

    function __construct() {
        parent::__construct();
		 $this->load->model('api_model');
		 $this->load->model('campaign_model');
		 $this->load->model('autoresponder_model');	
		 $this->load->model('statics_model');	
		 $this->load->model('newsletter_model');	
		 $this->load->model('mailsending_model');
		
   }
	public function index()
	{
	  $api_key = $_REQUEST['api_key'];
	  $keyexists = $this->api_model->checkForAPI($api_key);
	  if($keyexists)
	  {
		$action = $_REQUEST['action'];
		if($action=='authentication_check')
		 {
		   $data['success'] = true;
		   $data['msg'] = 'API Key Exists';
		 } 
		else if($action=='campaign_list') {
		   $data['campaign_list'] = $this->api_model->getCampaignList();
		   $data['success'] = true;
		 }
		else if($action=='add_subscriber')
		 {
		   $email = $_REQUEST['email'];
		   $campaign_id = $_REQUEST['campaign_id'];
		   if(filter_var(trim($email), FILTER_VALIDATE_EMAIL) && is_numeric($campaign_id))
		    {
			   $member_id = $this->common_model->getSingleFieldFromAnyTable('member_id','api_key',$api_key,'tbl_website_settings');
			   $subsciber_id = $this->api_model->addSubscriber($member_id);
			   $this->api_model->removeFromUnsubscribeList($email);
			   if($subsciber_id=='notdone')
			   { 
				 $data['success'] = false;
				 $data['msg'] = 'Subscriber Already Exists';
			   } else {
				 $this->sendAutoreponder($campaign_id,$subsciber_id);
				 $data['success'] = true;
				 $data['msg'] = 'Subscriber Added Successfully';
			   }
			} else {
				 $data['success'] = false;
				 $data['msg'] = 'Invalid Details';
			}
		 }
	   } else {
		   $data['success'] = false;
		   $data['msg'] = 'API Key does not  Exists';  
	   }
	  echo json_encode($data); die;
	 
	}
	public function sendAutoreponder($campaign_id,$contact_id)
	{ 
	   $auto_list = $this->autoresponder_model->getInstantAutoresponderListToSend($campaign_id);
	   $contactdetail = $this->api_model->getSubscriberDetail($contact_id); 
	   $mailfooter = '<br/><br/><br/><br/>'.$this->footerText($auto_list[0]->member_id);
	   $smtpDetail = $this->newsletter_model->getSMTPDetail($auto_list[0]->member_id);
	   $mail_type = $smtpDetail->mail_type;
	   $SMTP_hostname = $smtpDetail->SMTP_hostname;
	   $SMTP_port = $smtpDetail->SMTP_port;
	   $SMTP_username = $smtpDetail->SMTP_username;
	   $SMTP_password = $smtpDetail->SMTP_password;
	   $bounce_type = $smtpDetail->bounce_type;
	   $bounce_address = $smtpDetail->bounce_address;
	   
	   if($mail_type!='default' && $mail_type!='smtp')
		 {
		   $api_value = $this->common_model->getSingleFieldFromAnyTable('api_value','api_name',$mail_type,'tbl_external_mail_api');
		   $api_data = unserialize($api_value);
		   $from_email = $api_data['sending_email'];
		 }
	  
	   foreach($auto_list as $key=>$val)
		  {
			$checkAlready = $this->autoresponder_model->checkAlreadyMailed($contactdetail->email,$val->id);
			if($checkAlready!=0)
			 { continue; }
			 
			$from_name = $val->from_name;
			if($mail_type=='default' || $mail_type=='smtp')
			$from_email = $val->from_email;
			$reply_to = $val->reply_to;
			$attatchFiles = $this->autoresponder_model->getAttatchFile($val->id);
			$member_id = $val->member_id;
			
			$stats_id = $this->statics_model->addRecord($val->id,'autoresponder',$contactdetail->id,$contactdetail->email);
			$this->autoresponder_model->setAutoSentTime($contactdetail->id);

			$stats_code = base64_encode(base64_encode($stats_id));
			$unsubscribe_link = site_url('unsubscribe-'.$stats_code.'email');
			$openlink = '<img height="0" width="0" src="'.site_url('open-'.$stats_code.'mail').'" />';
			$Mailfooter=str_replace("{#unsubscribe_link}",$unsubscribe_link,$mailfooter);
			$Mailfooter=str_replace("{#open_link}",$openlink,$Mailfooter);
			$Mailfooter = str_replace("{#to_email}",$contactdetail->email,$Mailfooter);
			$Mailfooter = str_replace("{#from_email}",$from_email,$Mailfooter);

			$subject=str_replace("{#name}",ucfirst($contactdetail->name),$val->subject);
			$subject=str_replace("{#email}",$contactdetail->email,$subject);
			$subject=str_replace("{#address}",$contactdetail->address,$subject);
			$subject=str_replace("{#city}",$contactdetail->city,$subject);
			$subject=str_replace("{#state}",$contactdetail->state,$subject);
			$subject=str_replace("{#country}",$contactdetail->country,$subject);
			$subject=str_replace("{#zip}",$contactdetail->zip,$subject);
			$subject=str_replace("{#phone}",$contactdetail->phone,$subject);
			 
			$str=str_replace("{#name}",ucfirst($contactdetail->name),$val->template_text);
			$str=str_replace("{#email}",$contactdetail->email,$str);
			$str=str_replace("{#address}",$contactdetail->address,$str);
			$str=str_replace("{#city}",$contactdetail->city,$str);
			$str=str_replace("{#state}",$contactdetail->state,$str);
			$str=str_replace("{#country}",$contactdetail->country,$str);
			$str=str_replace("{#zip}",$contactdetail->zip,$str);
			$str=str_replace("{#phone}",$contactdetail->phone,$str);
			 
			$messageBody = $this->convertLinks($str,$stats_code).$Mailfooter;
			 
			$mailArray = array(
						 "subject" => $subject,
						 "message" => $messageBody,
						 "to" => $contactdetail->email,
						 "from_name" => $from_name,
						 "from_email" => $from_email,
						 "reply_to" => $reply_to,
						 "attatchFiles" => $attatchFiles,
						 "unsubscribe_link" => $unsubscribe_link,
						 "stats_code" => $stats_code,
						 "newsletter_id" => $val->id,
						 "member_id" => $member_id,
						 "email_type" => 'autoresponder',
						 "mail_type" => $mail_type,
						 "SMTP_hostname" => $SMTP_hostname,
						 "SMTP_port" => $SMTP_port,
						 "SMTP_username" => $SMTP_username,
						 "SMTP_password" => $SMTP_password,
						 "bounce_type" => $bounce_type,
						 "bounce_address" => $bounce_address,
						 "api_value" => $api_value
						);
									
			$this->sendmail($mailArray);
		  } 
	}
	public function footerText($member_id)
	{
		$social_links = $this->newsletter_model->getSocialLinks($member_id);
		$useraddress = $this->newsletter_model->getUserAddress($member_id);
		
		$fb_url = $social_links->fb_url;
		$tw_url = $social_links->tw_url;
		$insta_url = $social_links->insta_url;
		$gplus_url = $social_links->gplus_url;
		
		if($fb_url)
		 {
		   $social_liks.=  '<a href="'.$fb_url.'"><img src="'.site_url().'assets/default/images/email_footer/images/fb.png" alt="" /></a>';
		 }
		if($tw_url)
		 {
		   $social_liks.=  '<a href="'.$tw_url.'"><img src="'.site_url().'assets/default/images/email_footer/images/tw.png" alt=""/></a>';
		 }
		if($insta_url)
		 {
		   $social_liks.=  '<a href="'.$insta_url.'"><img src="'.site_url().'assets/default/images/email_footer/images/gplus.png" alt=""/></a>';
		 }
		if($gplus_url)
		 {
		   $social_liks.=  '<a href="'.$gplus_url.'"><img src="'.site_url().'assets/default/images/email_footer/images/in.png" alt=""/></a>';
		 }
		 if($social_liks)
		  {
		     $social_box = '<p style="margin:0; padding:0; line-height:20px;"><span style="position: relative;margin-top:-1px; padding-right:10px;">Follow us </span>'.$social_liks.'</p>';
		  }
		  
		 
	 	$footer_text = '<div style="width:600px; background-color:#f5f5f5;color:#4a4949; padding:15px 15px 10px 15px; margin:0 auto; height:auto; font-family: "Roboto", sans-serif; font-weight:400; font-size:13px;">
			<div style="float:left; width:70%;">
			<p style="margin:0; margin-top:-1px; padding-right:10px; line-height:20px;">This email was sent to {#to_email} by {#from_email}</p>
			'.$social_box.'
			<p style="margin:0; padding-bottom:8px; line-height:20px;">'.$useraddress->address.', '.$useraddress->city.', '.$useraddress->pin_code.', '.$useraddress->state.', '.$useraddress->country.'</p>
			<p style="background:url('.site_url().'assets/default/images/email_footer/images/line.jpg) repeat-x top; width:auto; display:inline-block; font-size:11px; margin:0; padding:5px 0px 0px 0px;"> Getting too many emails from us, click here to <a href="{#unsubscribe_link}" style="color:#1155cc; ">Unsubscribe Now.</a></p>
			{#open_link}
			</div>

			<div style="float:left; width:30%; text-align:right">
			<p style="margin:0; padding:0px 0px 4px 0px; text-align:center; width:95px; display:inline-block;">Powered by</p>
			<a href="https://www.mailzingo.com/"><img src="'.site_url().'assets/default/images/email_footer/images/email-logo.png" alt=""/></a></div>
			<div style="clear:both"></div>
			</div>';
		return $footer_text; 
	}
	public function convertLinks($message,$stats_code)
	{
	  $click_link = site_url('click-'.$stats_code.'email');
	  $message = str_replace('<a','{smart_href_link_epro} <a',$message);
	  $content = explode("{smart_href_link_epro}", $message);
	  $a_count = count($content);

	  for($i=0;$a_count>$i;$i++)
	   { 
		 if(preg_match('/href=/', $content[$i]))
		  {
			list($Lost,$Keep) = explode("href=\"", trim($content[$i]));
			list($Keep,$Lost) = explode("\"", $Keep);
			if($Keep!='#' && $Keep!='')
			$message= strtr($message, array( "$Keep" => $click_link.base64_encode(base64_encode($Keep)), ));
		  }  
	   } 
	  
	  return str_replace('{smart_href_link_epro}','',$message);
	}
	public function sendMail($mailArray)
	{
	  if($mailArray['mail_type']=='')
	  {
		   $smtpDetail = $this->newsletter_model->getSMTPDetail($mailArray['member_id']);
		   $mailArray['mail_type'] = $mail_type = $smtpDetail->mail_type;
		   $mailArray['SMTP_hostname'] = $smtpDetail->SMTP_hostname;
		   $mailArray['SMTP_port'] = $smtpDetail->SMTP_port;
		   $mailArray['SMTP_username'] = $smtpDetail->SMTP_username;
		   $mailArray['SMTP_password'] = $smtpDetail->SMTP_password;
		   $mailArray['bounce_type'] = $smtpDetail->bounce_type;
		   $mailArray['bounce_address'] = $smtpDetail->bounce_address;
	  }
	  $mail_type = $mailArray['mail_type'];

	  if($mailArray['mail_type']!='default' && $mailArray['mail_type']!='smtp')
		 {
		   $mailArray['api_value'] = $this->common_model->getSingleFieldFromAnyTable('api_value','api_name',$mail_type,'tbl_external_mail_api');
		 }
	
	  if($mailArray['mail_type']=='default' || $mailArray['mail_type']=='smtp')
	   {
	     $this->mailsending_model->sendmail($mailArray);
	   } 
	  else if($mailArray['mail_type']=='amazon_ses')
	   {
	     require_once 'amazon_ses/autoload.php';
	     $this->mailsending_model->sendMailUsingAmazon($mailArray);
	   }
	  else if($mailArray['mail_type']=='sparkpost')
	   {
	     $httpClient = new GuzzleAdapter(new Client());
		 $api_value = unserialize($mailArray['api_value']);
		 $api_key = $api_value['api_key'];
		 $sparky = new SparkPost($httpClient, ['key'=>$api_key]);
		 $response = $this->mailsending_model->sendMailUsingSparkpost($mailArray,$sparky);
	   }
	  else if($mailArray['mail_type']=='postmark')
	   {
		 $httpClient = new GuzzleAdapter(new Client());
		 $api_value = unserialize($mailArray['api_value']);
		 $api_key = $api_value['api_key'];
		 $client = new PostmarkClient($api_key);
		 $response = $this->mailsending_model->sendMailUsingPostmark($mailArray,$client);
	   }
	  else if($mailArray['mail_type']=='mailgun')
	   {
		 $api_value = unserialize($mailArray['api_value']);
		 $api_key = $api_value['api_key'];
		 $sending_domain = $api_value['sending_domain'];
		 $sending_email = $api_value['sending_email'];
		 $mgClient = new Mailgun($api_key);
		 $response = $this->mailsending_model->sendMailUsingMailgun($mailArray,$mgClient,$sending_domain,$sending_email);
	   }
	  else if($mailArray['mail_type']=='elasticmail')
	   {
		 $api_value = unserialize($mailArray['api_value']);
		 $api_key = $api_value['api_key'];
		 try{
		     $elasticEmail = new \ElasticEmail\ElasticEmailV2($api_key);
			 $response = $this->mailsending_model->sendMailUsingElastic($mailArray,$elasticEmail);
		 }
		 catch(Exception $e){
							$response = 'error';	
						}
		 
	   }
	   else if($mailArray['mail_type']=='sendgrid'){
		   
	 	 $api_value = unserialize($mailArray['api_value']);
		 $api_key = $api_value['api_key'];
		 $sending_mail = $api_value['sending_mail'];
	  	 try{
				$response = $this->mailsending_model->sendMailUsingSendgrid($mailArray,$api_key,$sending_mail);
			}
			catch(Exception $e){
				$response = 'error';	
			}
	  }
	}
}