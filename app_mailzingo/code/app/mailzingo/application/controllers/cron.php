<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require 'spark_post/autoload.php'; 
use SparkPost\SparkPost;
use GuzzleHttp\Client; 
use Http\Adapter\Guzzle6\Client as GuzzleAdapter;

require_once('postmark/autoload.php');
use Postmark\PostmarkClient;

require 'mailgun/autoload.php';
use Mailgun\Mailgun;
require 'elastic_mail/autoload.php';
require 'sendgrid/autoload.php';
class Cron extends CI_Controller {

	function __construct()
	{ 
	    parent::__construct();				
		$this->load->model('newsletter_model');
		$this->load->model('autoresponder_model');
		$this->load->model('template_model');
		$this->load->model('campaign_model');
		$this->load->model('contact_model');
		$this->load->model('suppression_list_model');
		$this->load->model('suppression_model');
		$this->load->model('unsubscribe_model');
		$this->load->model('statics_model');
		$this->load->model('mailsending_model');
	}
	public function globalCron()
	{ 
	  $urls = array(site_url('run-cron'),site_url('autoresponder-cron'),site_url('add-in-queue-cron'));
	  $this->multiUrlUsingCurl($urls);
	}
	public function index()
	{
	  $this->db->where('time <=', date('Y-m-d H:i:s'));
	  $query = $this->db->get('tbl_cron_urls');
	  $data = $query->result();
	  $urls = array();
	  foreach($data as $val)
	  $urls[] = $val->url;
	  $this->multiUrlUsingCurl($urls);
	   die('done');
	}
	public function multiUrlUsingCurl($urls)
	{
			$sg = curl_multi_init();
			
			$results = array();
			foreach ($urls as $key => $url) {

				$results[$key] = curl_init();
				curl_setopt_array($results[$key], array(
					CURLOPT_URL => $url,
					CURLOPT_RETURNTRANSFER => true,
					CURLOPT_FOLLOWLOCATION => true
				));
			
				curl_multi_add_handle($sg, $results[$key]);
			}
			
			$flag = null;
			do {
				$status = curl_multi_exec($sg, $flag);
			}
			while ($flag);
			
			foreach ($results as $res) {
				echo curl_multi_getcontent($res);
				curl_multi_remove_handle($sg, $res);
				curl_close($res);
			}
			
			curl_multi_close($sg);
	}
	public function sendNewsletter($cron_id)
	{ 
	    $data = $this->newsletter_model->getQueueNewsletter($cron_id);
        $templateData = $this->newsletter_model->getTemplateDataByNewsletter($data[0]->newsletter_id);
		$from_name = $templateData->from_name;
		$from_email = $templateData->from_email;
		$reply_to = $templateData->reply_to;
		$member_id = $templateData->member_id;
		$attatchFiles = $this->newsletter_model->getAttatchFile($data[0]->newsletter_id);
		$mailfooter = '<br/><br/><br/><br/>'.$this->footerText($member_id);
		$smtpDetail = $this->newsletter_model->getSMTPDetail($member_id);
		$mail_type = $smtpDetail->mail_type;
		$SMTP_hostname = $smtpDetail->SMTP_hostname;
		$SMTP_port = $smtpDetail->SMTP_port;
		$SMTP_username = $smtpDetail->SMTP_username;
		$SMTP_password = $smtpDetail->SMTP_password;
		$bounce_type = $smtpDetail->bounce_type;
		$bounce_address = $smtpDetail->bounce_address;
		
		if($mail_type!='default' && $mail_type!='smtp')
		 {
		   $api_value = $this->common_model->getSingleFieldFromAnyTable('api_value','api_name',$mail_type,'tbl_external_mail_api');
		   $api_data = unserialize($api_value);
		   $from_email = $api_data['sending_email'];
		 }
			
		foreach($data as $key=>$val)
		  { 
		    if($mail_type=='default')
			 {
			   $limit_access = $this->newsletter_model->checkForHourLimit($member_id);
			   if($limit_access=='done')
			    { return; }
			 }
			 $this->db->where('email',$val->email);
			 $this->db->where('type','newsletter');
			 $this->db->where('newsletter_id',$val->newsletter_id);
			 $qt = $this->db->get('tbl_statistics');
			 
			 $this->newsletter_model->removeQueueById($val->queue_id);
			 if($qt->num_rows()!=0)
			 { continue; }
			 $stats_id = $this->statics_model->addRecord($val->newsletter_id,'newsletter',$val->contactid,$val->email);
			 
			 $stats_code = base64_encode(base64_encode($stats_id));
		     $unsubscribe_link = site_url('unsubscribe-'.$stats_code.'email');
			 $openlink = '<img height="0" width="0" src="'.site_url('open-'.$stats_code.'mail').'" />';
			 $Mailfooter=str_replace("{#unsubscribe_link}",$unsubscribe_link,$mailfooter);
			 $Mailfooter=str_replace("{#open_link}",$openlink,$Mailfooter);
			 $Mailfooter = str_replace("{#to_email}",$val->email,$Mailfooter);
			 $Mailfooter = str_replace("{#from_email}",$from_email,$Mailfooter);

			 $subject=str_replace("{#name}",ucfirst($val->name),$templateData->subject);
		     $subject=str_replace("{#email}",$val->email,$subject);
			 $subject=str_replace("{#address}",$val->address,$subject);
			 $subject=str_replace("{#city}",$val->city,$subject);
			 $subject=str_replace("{#state}",$val->state,$subject);
			 $subject=str_replace("{#country}",$val->country,$subject);
			 $subject=str_replace("{#zip}",$val->zip,$subject);
			 $subject=str_replace("{#phone}",$val->phone,$subject);
			 
			 $str=str_replace("{#name}",ucfirst($val->name),$templateData->template_text);
		     $str=str_replace("{#email}",$val->email,$str);
			 $str=str_replace("{#address}",$val->address,$str);
			 $str=str_replace("{#city}",$val->city,$str);
			 $str=str_replace("{#state}",$val->state,$str);
			 $str=str_replace("{#country}",$val->country,$str);
			 $str=str_replace("{#zip}",$val->zip,$str);
			 $str=str_replace("{#phone}",$val->phone,$str);
			 
			 $messageBody = $this->convertLinks($str,$stats_code).$Mailfooter;
			 
			 $mailArray = array(
			              "subject" => $subject,
						  "message" => $messageBody,
						  "to" => $val->email,
						  "from_name" => $from_name,
						  "from_email" => $from_email,
						  "reply_to" => $reply_to,
						  "attatchFiles" => $attatchFiles,
						  "unsubscribe_link" => $unsubscribe_link,
						  "stats_code" => $stats_code,
						  "newsletter_id" => $val->newsletter_id,
						  "member_id" => $member_id,
						  "mail_type" => $mail_type,
						  "SMTP_hostname" => $SMTP_hostname,
						  "SMTP_port" => $SMTP_port,
						  "SMTP_username" => $SMTP_username,
						  "SMTP_password" => $SMTP_password,
						  "bounce_type" => $bounce_type,
						  "bounce_address" => $bounce_address,
						  "api_value" => $api_value
						 );
			 $this->sendMail($mailArray);

			 echo $val->email.'</br>';
		  }
		  $this->newsletter_model->chanegNewsletterStatus($data[0]->newsletter_id);
		  $queueCount = $this->newsletter_model->checkQueueNewsletter($cron_id);
		  if($queueCount==0)
		   {
		     $this->newsletter_model->removeCronURL($cron_id);
		   }
		  die('done');
	}
	public function convertLinks($message,$stats_code)
	{
	  $click_link = site_url('click-'.$stats_code.'email');
	  $message = str_replace('<a','{smart_href_link_epro} <a',$message);
	  $content = explode("{smart_href_link_epro}", $message);
	  $a_count = count($content);

	  for($i=0;$a_count>$i;$i++)
	   { 
		 if(preg_match('/href=/', $content[$i]))
		  {
			list($Lost,$Keep) = explode("href=\"", trim($content[$i]));
			list($Keep,$Lost) = explode("\"", $Keep);
			if($Keep!='#' && $Keep!='')
			$message= strtr($message, array( "$Keep" => $click_link.base64_encode(base64_encode($Keep)), ));
		  }  
	   } 
	  
	  return str_replace('{smart_href_link_epro}','',$message);
	}
	public function addDataInQueue()
	{
	  $this->db->select('message_type,schedule_timestamp,id,contact_id,last_procees_id');
	  $this->db->where('process_stage','inprocess');
	  $this->db->where('status !=','draft');
	  $this->db->order_by('message_type','asc');
	  $this->db->limit(1);
	  $query = $this->db->get('tbl_newsletter');
	  $result = $query->row();
	  if($query->num_rows()==0)
	  return;
	  
	  $message_type = $result->message_type;
	  $schedule_time = $result->schedule_timestamp;
	  $newsletter_id = $result->id;
	  $process_value = 5000;
	  $contact_ids = $result->contact_id;

	  if($result->last_procees_id)
	  $newarray = explode(','.$result->last_procees_id.',',$contact_ids);
	  else 
	  $newarray[1] = $contact_ids;
	  
	  $contact_array = array_filter(explode(',',$newarray[1]));
	  $lastkey = end(array_keys($contact_array));

	  if($lastkey>$process_value)
	   {
	     $newContactarray = array_chunk($contact_array, $process_value)[0];
		 $newlast_procees_key = end(array_keys($newContactarray));
		 $this->setlastProcessId($contact_array[$newlast_procees_key],'inprocess',$newsletter_id);
	   } else {
	     $newContactarray = $contact_array;
	     $newlast_procees_key = $lastkey;
		 $this->setlastProcessId($contact_array[$newlast_procees_key],'complete',$newsletter_id);
	   }
	  
	  
	  $mailPerCron = $this->config->item('mailPerCron');

	   foreach($newContactarray as $key=>$val_contact)
		{
			  if($key%$mailPerCron==0)
			   {
				 $cron_id = time().$key;
				 $this->setCron($cron_id,$message_type,$schedule_time,$newsletter_id);
			   }
			  $this->newsletter_model->addEmailQueue($newsletter_id,$val_contact,$cron_id);
		}
		die('done');
	}
	public function setCron($cron_id,$message_type,$schedule_time,$newsletter_id)
	{
	  if($message_type=='instant')
	  { 
	    $minute = date('i')+1;
		$time = date('Y-m-d H:').$minute.':00'; 
	  } else  {
		$time = date('Y-m-d H:i:s',$schedule_time); 
	  }
	  $this->db->set('time',$time);
	  $this->db->set('newsletter_id',$newsletter_id);
	  $this->db->set('url',site_url('send-newsletter-cron/'.$cron_id));
	  $this->db->insert('tbl_cron_urls');
	}
	public function setlastProcessId($last_procees_id,$process_stage,$newsletter_id) 
	{
	   $this->db->set('last_procees_id',$last_procees_id);
	   $this->db->set('process_stage',$process_stage);
	   $this->db->where('id',$newsletter_id);
	   $this->db->update('tbl_newsletter');
	}
	public function sendAutoresponder()
	{
	  $time = date('H:i').':00';
	  $day = strtolower(date('l'));
	  
	  $auto_list = $this->autoresponder_model->getAutoresponderListToSend($time,$day);
	  $mailfooter = '<br/><br/><br/><br/>'.$this->footerText($auto_list[0]->member_id);
	  $member_id = $auto_list[0]->member_id;
	  $smtpDetail = $this->newsletter_model->getSMTPDetail($member_id);
	  $mail_type = $smtpDetail->mail_type;
	  $SMTP_hostname = $smtpDetail->SMTP_hostname;
	  $SMTP_port = $smtpDetail->SMTP_port;
	  $SMTP_username = $smtpDetail->SMTP_username;
	  $SMTP_password = $smtpDetail->SMTP_password;
	  $bounce_type = $smtpDetail->bounce_type;
	  $bounce_address = $smtpDetail->bounce_address;
	  
	  if($mail_type!='default' && $mail_type!='smtp')
		 {
		   $api_value = $this->common_model->getSingleFieldFromAnyTable('api_value','api_name',$mail_type,'tbl_external_mail_api');
		   $api_data = unserialize($api_value);
		   $from_email = $api_data['sending_email'];
		 }
		
		foreach($auto_list as $key=>$val)
		  {
			$contactlist = $this->autoresponder_model->getAutoContactList($val->mail_day,$val->in_campaign_id); 
			$from_name = $val->from_name;
			if($mail_type=='default' || $mail_type=='smtp')
			$from_email = $val->from_email;
			$reply_to = $val->reply_to;
			$attatchFiles = $this->autoresponder_model->getAttatchFile($val->id);
			$member_id = $val->member_id;
			
			foreach($contactlist as $valContact)
			{
			    $this->db->where('email',$valContact->email);
			    $this->db->where('type','autoresponder');
			    $this->db->where('newsletter_id',$val->id);
			    $qt = $this->db->get('tbl_statistics');
				if($qt->num_rows()!=0)
			    { continue; }
			 
				$stats_id = $this->statics_model->addRecord($val->id,'autoresponder',$valContact->id,$valContact->email);
				$this->autoresponder_model->setAutoSentTime($valContact->id);

				 $stats_code = base64_encode(base64_encode($stats_id));
				 $unsubscribe_link = site_url('unsubscribe-'.$stats_code.'email');
				 $openlink = '<img height="0" width="0" src="'.site_url('open-'.$stats_code.'mail').'" />';
				 $Mailfooter=str_replace("{#unsubscribe_link}",$unsubscribe_link,$mailfooter);
				 $Mailfooter=str_replace("{#open_link}",$openlink,$Mailfooter);
			     $Mailfooter = str_replace("{#to_email}",$valContact->email,$Mailfooter);
			     $Mailfooter = str_replace("{#from_email}",$from_email,$Mailfooter);
				 
				 $subject=str_replace("{#name}",ucfirst($valContact->name),$val->subject);
				 $subject=str_replace("{#email}",$valContact->email,$subject);
				 $subject=str_replace("{#address}",$valContact->address,$subject);
				 $subject=str_replace("{#city}",$valContact->city,$subject);
				 $subject=str_replace("{#state}",$valContact->state,$subject);
				 $subject=str_replace("{#country}",$valContact->country,$subject);
				 $subject=str_replace("{#zip}",$valContact->zip,$subject);
				 $subject=str_replace("{#phone}",$valContact->phone,$subject);
				 
				 $str=str_replace("{#name}",ucfirst($valContact->name),$val->template_text);
				 $str=str_replace("{#email}",$valContact->email,$str);
				 $str=str_replace("{#address}",$valContact->address,$str);
				 $str=str_replace("{#city}",$valContact->city,$str);
				 $str=str_replace("{#state}",$valContact->state,$str);
				 $str=str_replace("{#country}",$valContact->country,$str);
				 $str=str_replace("{#zip}",$valContact->zip,$str);
				 $str=str_replace("{#phone}",$valContact->phone,$str);
				 

				 $messageBody = $this->convertLinks($str,$stats_code).$Mailfooter;
				 
				 $mailArray = array(
							  "subject" => $subject,
							  "message" => $messageBody,
							  "to" => $valContact->email,
							  "from_name" => $from_name,
							  "from_email" => $from_email,
							  "reply_to" => $reply_to,
							  "attatchFiles" => $attatchFiles,
							  "unsubscribe_link" => $unsubscribe_link,
							  "stats_code" => $stats_code,
							  "newsletter_id" => $val->id,
							  "member_id" => $member_id,
							  "email_type" => 'autoresponder',
							  "mail_type" => $mail_type,
							  "SMTP_hostname" => $SMTP_hostname,
							  "SMTP_port" => $SMTP_port,
							  "SMTP_username" => $SMTP_username,
							  "SMTP_password" => $SMTP_password,
							  "bounce_type" => $bounce_type,
							  "bounce_address" => $bounce_address,
							  "api_value" => $api_value
							 );
				  $this->sendmail($mailArray);
				  echo $valContact->email.'</br>';
			 }
	
			
		  }
		  die('done');
	  
	}
	public function footerText($member_id)
	{
		$social_links = $this->newsletter_model->getSocialLinks($member_id);
		$useraddress = $this->newsletter_model->getUserAddress($member_id);
		 
		$fb_url = $social_links->fb_url;
		$tw_url = $social_links->tw_url;
		$insta_url = $social_links->insta_url;
		$gplus_url = $social_links->gplus_url;
		
		if($fb_url)
		 {
		   $social_liks.=  '<a href="'.$fb_url.'"><img src="'.site_url().'assets/default/images/email_footer/images/fb.png" alt=""/></a>';
		 }
		if($tw_url)
		 {
		   $social_liks.=  '<a href="'.$tw_url.'"><img src="'.site_url().'assets/default/images/email_footer/images/tw.png" alt=""/></a>';
		 }
		if($insta_url)
		 {
		   $social_liks.=  '<a href="'.$insta_url.'"><img src="'.site_url().'assets/default/images/email_footer/images/gplus.png" alt=""/></a>';
		 }
		if($gplus_url)
		 {
		   $social_liks.=  '<a href="'.$gplus_url.'"><img src="'.site_url().'assets/default/images/email_footer/images/in.png" alt=""/></a>';
		 }
		 if($social_liks)
		  {
		     $social_box = '<p style="margin:0; padding:0; line-height:20px;"><span style="position: relative;margin-top:-1px; padding-right:10px;">Follow us </span>'.$social_liks.'</p>';
		  }
	 	
		
		$footer_text = '<div style="width:600px; background-color:#f5f5f5;color:#4a4949; padding:15px 15px 10px 15px; margin:0 auto; height:auto; font-family: "Roboto", sans-serif; font-weight:400; font-size:13px;">
			<div style="float:left; width:70%;">
			<p style="margin:0; margin-top:-1px; padding-right:10px; line-height:20px;">This email was sent to {#to_email} by {#from_email}</p>
			'.$social_box.'
			<p style="margin:0; padding-bottom:8px; line-height:20px;">'.$useraddress->address.', '.$useraddress->city.', '.$useraddress->pin_code.', '.$useraddress->state.', '.$useraddress->country.'</p>
			<p style="background:url('.site_url().'assets/default/images/email_footer/images/line.jpg) repeat-x top; width:auto; display:inline-block; font-size:11px; margin:0; padding:5px 0px 0px 0px;"> Getting too many emails from us, click here to <a href="{#unsubscribe_link}" style="color:#1155cc; ">Unsubscribe Now.</a></p>
			{#open_link}
			</div>

			<div style="float:left; width:30%; text-align:right">
			<p style="margin:0; padding:0px 0px 4px 0px; text-align:center; width:95px; display:inline-block;">Powered by</p>
			<a href="https://www.mailzingo.com/"><img src="'.site_url().'assets/default/images/email_footer/images/email-logo.png" alt=""/></a></div>
			<div style="clear:both"></div>
			</div>';
		return $footer_text; 
	}
	public function sendMail($mailArray)
	{
	  if($mailArray['mail_type']=='default' || $mailArray['mail_type']=='smtp')
	   {
	     $this->mailsending_model->sendmail($mailArray);
	   } 
	  else if($mailArray['mail_type']=='amazon_ses')
	   {
	     require_once 'amazon_ses/autoload.php';
	     $this->mailsending_model->sendMailUsingAmazon($mailArray);
	   }
	  else if($mailArray['mail_type']=='sparkpost')
	   {
	     $httpClient = new GuzzleAdapter(new Client());
		 $api_value = unserialize($mailArray['api_value']);
		 $api_key = $api_value['api_key'];
		 $sparky = new SparkPost($httpClient, ['key'=>$api_key]);
		 $response = $this->mailsending_model->sendMailUsingSparkpost($mailArray,$sparky);
	   }
	  else if($mailArray['mail_type']=='postmark')
	   {
		 $httpClient = new GuzzleAdapter(new Client());
		 $api_value = unserialize($mailArray['api_value']);
		 $api_key = $api_value['api_key'];
		 $client = new PostmarkClient($api_key);
		 $response = $this->mailsending_model->sendMailUsingPostmark($mailArray,$client);
	   }
	  else if($mailArray['mail_type']=='mailgun')
	   {
		 $api_value = unserialize($mailArray['api_value']);
		 $api_key = $api_value['api_key'];
		 $sending_domain = $api_value['sending_domain'];
		 $sending_email = $api_value['sending_email'];
		 $mgClient = new Mailgun($api_key);
		 $response = $this->mailsending_model->sendMailUsingMailgun($mailArray,$mgClient,$sending_domain,$sending_email);
	   }
	  else if($mailArray['mail_type']=='elasticmail')
	   {
		 $api_value = unserialize($mailArray['api_value']);
		 $api_key = $api_value['api_key'];
		 try{
		     $elasticEmail = new \ElasticEmail\ElasticEmailV2($api_key);
			 $response = $this->mailsending_model->sendMailUsingElastic($mailArray,$elasticEmail);
		 }
		 catch(Exception $e){
							$response = 'error';	
						}
		 
	   }
		else if($mailArray['mail_type']=='sendgrid')
		{
			$api_value = unserialize($mailArray['api_value']);
			$api_key = $api_value['api_key'];
			$sending_mail = $api_value['sending_mail'];
			try{
				$response = $this->mailsending_model->sendMailUsingSendgrid($mailArray,$api_key,$sending_mail);
			}
			catch(Exception $e){
				$response = 'error';	
			}
				
		}		
	
	}
}