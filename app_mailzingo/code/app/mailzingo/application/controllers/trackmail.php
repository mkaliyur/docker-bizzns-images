<?php
defined('BASEPATH') OR exit('No direct script access allowed');
include('app.php');

class Trackmail extends App {

	function __construct()
	{ 
	    parent::__construct();	
		$this->load->model('statics_model');			
	}
	public function index()
	{
		//$stats_id = base64_decode(base64_decode($un_data));
		//$this->statics_model->setOpen($stats_id);
	}
	public function openEmail($data)
	{
		$this->load->library('Mobile_Detect');
		$detect = new Mobile_Detect;
		if( $detect->isMobile() || $detect->isTablet() ) { 
		$device =  'mobile'; } else { $device = 'desktop'; } 
		
		$id = base64_decode(base64_decode($data));
		$this->statics_model->setOpen($id,$device);
	}
	public function clickEmail($stats_id,$link)
	{   
		$id = base64_decode(base64_decode($stats_id));
		$link = base64_decode(base64_decode($link));
		//echo $stats_id.'  </br> '.$link.'  '; die('ddd');
		if(is_numeric($id))
		{
			$idExists = $this->statics_model->checkStatsIdExists($id);
			//echo $idExists;
			if($idExists==1)
			 { 
				$this->load->library('Mobile_Detect');
		        $detect = new Mobile_Detect;
		        if( $detect->isMobile() || $detect->isTablet() ) { 
		        $device =  'mobile'; } else { $device = 'desktop'; }
		
				$this->statics_model->setClick($id,$device);
			 }
		}
		//redirect($link);
		header('Location: '.$link);
        exit;
	}
	public function bounceEmail($data)
	{
		$id = base64_decode(base64_decode($data));
		$this->statics_model->setBounce($id);
	}
}
