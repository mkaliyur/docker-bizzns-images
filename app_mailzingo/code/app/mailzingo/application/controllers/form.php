<?php
defined('BASEPATH') OR exit('No direct script access allowed');
include('app.php');

class Form extends App {

	function __construct()
	{ 
	    parent::__construct();	
		$this->load->model('form_model');	
		$this->load->model('campaign_model');	
		$this->checkLogin();
		$this->member_id = $this->session->userdata('mem_id');		
	}
	function checkLogin()
	{
	  if($this->session->userdata('mem_id')=='')
	  { redirect('login'); }
	  
	}
	public function index()
	{
		$output['meta_title'] = 'Forms';
        $output['all_record'] = $this->form_model->getFormlist($this->member_id);
		$this->session->unset_userdata('form_session');
		$this->load->view($this->config->item('templateName').'/header',$output);
		$this->load->view($this->config->item('templateName').'/form/list');
		$this->load->view($this->config->item('templateName').'/footer');
	}
	public function formSettings($form_id)
	{
		$output['meta_title'] = 'Form Setting';
		if($_GET['add_type']=='new')
		$this->session->unset_userdata('form_session');
		
		$output['campaign_list'] = $this->campaign_model->getCampaignList($this->member_id);
		if($form_id)
		 {
		   $formdetail = $this->form_model->getFormById($form_id);
		   $form_session['form_name'] = $formdetail->form_name;
		   $form_session['campaign_id'] = explode(',',$formdetail->campaign_ids);
		   $form_session['step2'] = 'yes';
		   $form_session['template_text'] = $formdetail->form_html;
		   $form_session['template_id'] = $formdetail->template_id;
		   $form_session['step3'] = 'yes';
		   $form_session['update'] = 'yes';
		   $form_session['form_id'] = $formdetail->id;
		   $form_session['thankyou_page'] = $formdetail->thankyou_page;
		   $form_session['thankyou_custom'] = $formdetail->thankyou_custom;
		   $form_session['already_subscribe'] = $formdetail->already_subscribe;
		   $form_session['alreadysub_custom'] = $formdetail->alreadysub_custom;
		   $form_session['double_otp'] = $formdetail->double_otp;
		   $this->session->set_userdata('form_session',$form_session);
		 } 
		$output['form_session'] = $form_session = $this->session->userdata('form_session');
		$output['form_name'] = $form_session['form_name'];
		$output['campaign_id'] = $form_session['campaign_id'];
		
		if($_POST) {
		   
		   $output['form_name'] = $this->input->post('form_name');
		   $output['campaign_id'] = $this->input->post('campaign_id');

		   $this->form_validation->set_rules('form_name','Form Name','trim|required');
		   $this->form_validation->set_rules('campaign_id[]','Campaign Id','trim|required');
		   
		   if($this->form_validation->run())
		    {
			   $form_session['form_name'] = $this->input->post('form_name');
			   $form_session['campaign_id'] = $this->input->post('campaign_id');
			   $form_session['step2'] = 'yes';
			   $this->session->set_userdata('form_session',$form_session);
		       redirect('form-template');
			}
		   
		   
		}
		
		$this->load->view($this->config->item('templateName').'/header',$output);
		$this->load->view($this->config->item('templateName').'/form/setting');
		$this->load->view($this->config->item('templateName').'/footer');
	}
	public function formTemplate($form_id)
	{
		$output['meta_title'] = 'Form Template';
		if($form_id)
		 {
		   $formdetail = $this->form_model->getFormById($form_id);
		   $form_session['form_name'] = $formdetail->form_name;
		   $form_session['campaign_id'] = explode(',',$formdetail->campaign_ids);
		   $form_session['step2'] = 'yes';
		   $form_session['template_id'] = $formdetail->template_id;
		   $form_session['template_text'] = $formdetail->form_html;
		   $form_session['step3'] = 'yes';
		   $form_session['update'] = 'yes';
		   $form_session['form_id'] = $formdetail->id;
		   $form_session['thankyou_page'] = $formdetail->thankyou_page;
		   $form_session['thankyou_custom'] = $formdetail->thankyou_custom;
		   $form_session['already_subscribe'] = $formdetail->already_subscribe;
		   $form_session['alreadysub_custom'] = $formdetail->alreadysub_custom;
		   $form_session['double_otp'] = $formdetail->double_otp;
		   $this->session->set_userdata('form_session',$form_session);
		 } 

	    $output['form_session'] = $form_session = $this->session->userdata('form_session');
	    if($form_session['step2']!='yes')
		 {
		   redirect('form-settings');
		 }
		$output['template_id'] = $form_session['template_id'];
		if($_POST)
		 {
		   $this->form_validation->set_rules('template_id','Choose Template','trim|required');
		   
		   if($this->form_validation->run())
			{
			  $template_detail = $this->form_model->getTemplateById($this->input->post('template_id'));
			  $form_session['template_text'] = $template_detail->template_text;
			  $form_session['template_id'] = $this->input->post('template_id');
			  $form_session['step3'] = 'yes';
			  $this->session->set_userdata('form_session',$form_session);
			  redirect('form-editor');
			}
		   
		 }
		$output['template_list'] = $this->form_model->getTemplateList($this->member_id,$this->form_template_count);
		
		$this->load->view($this->config->item('templateName').'/header',$output);
		$this->load->view($this->config->item('templateName').'/form/template');
		$this->load->view($this->config->item('templateName').'/footer');
	}
	public function formEditor($form_id)
	{ 
		$output['meta_title'] = 'Form Editor';
		if($form_id)
		 {
		   $formdetail = $this->form_model->getFormById($form_id);
		   $form_session['form_name'] = $formdetail->form_name;
		   $form_session['campaign_id'] =  explode(',',$formdetail->campaign_ids);
		   $form_session['step2'] = 'yes';
		   $form_session['template_text'] = $formdetail->form_html;
		   $form_session['template_id'] = $formdetail->template_id;
		   $form_session['step3'] = 'yes';
		   $form_session['update'] = 'yes';
		   $form_session['form_id'] = $formdetail->id;
		   $form_session['thankyou_page'] = $formdetail->thankyou_page;
		   $form_session['thankyou_custom'] = $formdetail->thankyou_custom;
		   $form_session['already_subscribe'] = $formdetail->already_subscribe;
		   $form_session['alreadysub_custom'] = $formdetail->alreadysub_custom;
		   $form_session['double_otp'] = $formdetail->double_otp;
		   $this->session->set_userdata('form_session',$form_session);
		 } 

		$output['form_session'] = $form_session = $this->session->userdata('form_session');
		$output['template_text'] = $form_session['template_text'];
		$output['thankyou_page'] = $form_session['thankyou_page'];
		$output['thankyou_custom'] = $form_session['thankyou_custom'];
		$output['already_subscribe'] = $form_session['already_subscribe'];
		$output['alreadysub_custom'] = $form_session['alreadysub_custom'];
		$output['double_otp'] = $form_session['double_otp'];
		$update = $form_session['update'];
		if($form_session['step3']!='yes')
		 {
		   redirect('form-template');
		 }
		 
		 if($_POST){
            
		   $output['template_text'] = $this->input->post('template_text');
		   $output['thankyou_page'] = $this->input->post('thankyou_page');
		   $output['thankyou_custom'] = $this->input->post('thankyou_custom');
		   $output['already_subscribe'] = $this->input->post('already_subscribe');
		   $output['alreadysub_custom'] = $this->input->post('alreadysub_custom');
		   $output['double_otp'] = $this->input->post('double_otp');
		  
		   $this->form_validation->set_rules('template_text','Edit Template','trim|required');
		   $this->form_validation->set_rules('thankyou_page','Thank you Page','trim|required');
		   if($this->input->post('thankyou_page')=='custom')
		   $this->form_validation->set_rules('thankyou_custom','Thanky you page custom','trim|required|callback_valid_url');
		   $this->form_validation->set_rules('already_subscribe','Already Subscribe','trim|required');
		   if($this->input->post('already_subscribe')=='custom')
		   $this->form_validation->set_rules('alreadysub_custom','Already subsribe custom','trim|required|callback_valid_url');

		   if($this->form_validation->run())
			 {
			   if($update=='yes')
			    {
			      $form_id = $form_session['form_id'];
				  $this->form_model->updateForm($this->member_id,$form_session,$form_id);
				} else {
			      $form_id = $this->form_model->addForm($this->member_id,$form_session);
				}
				
				$filteredData=substr($_POST['image_text'], strpos($_POST['image_text'], ",")+1);
	            $unencodedData=base64_decode($filteredData);
				$filename = $form_id.time().'.png';
	            file_put_contents('./assets/uploads/form_images/'.$filename, $unencodedData);
				$this->form_model->updateImage($filename,$form_id);

			   $this->session->unset_userdata('form_session');
			   redirect('form-publish/'.$form_id);
			}
		 }
		
		$this->load->view($this->config->item('templateName').'/header',$output);
		$this->load->view($this->config->item('templateName').'/form/editor');
		$this->load->view($this->config->item('templateName').'/footer');
	}
	public function formPublish($form_id)
	{
		$output['meta_title'] = 'Form Editor';
		$output['form_id'] = $form_id;
		$output['formdetail'] = $this->form_model->getFormById($form_id);
		$this->load->view($this->config->item('templateName').'/header',$output);
		$this->load->view($this->config->item('templateName').'/form/publish');
		$this->load->view($this->config->item('templateName').'/footer');
	}
	public function deleteForm($id)
	{
	  $this->form_model->deleteForm($id);
	  $this->session->set_userdata('success_msg','Form Deleted Successfully');
	  redirect('forms');
	}
	public function deleteMultiForm()
	{ 
	  if($_POST)
	   {
		 $id = $this->input->post('id');
		 $this->form_model->removeMultipleForm($id);
		 $data['success'] = true;
		}
	   echo json_encode($data); die;
	}
	public function viewFormEmbed()
	{
	  if($_POST)
	  {
	      $output['form_id'] = $form_id = $this->input->post('id');
		  $output['formdetail'] = $this->form_model->getFormById($form_id);
		  $response['html'] = $this->load->view($this->config->item('templateName').'/form/view_embed',$output,true);
		  $response['success'] = true;
		  
		  $this->output
			   ->set_content_type('application/json')
			   ->set_output(json_encode($response));
	  
	  }
	}
    public function valid_url($str)
    {
      return (filter_var($str, FILTER_VALIDATE_URL) !== FALSE);
    }
        
}
