<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Faq extends CI_Controller {

	function __construct()
	{ 
	    parent::__construct();				
		$this->checkLogin();
	}
	function checkLogin()
	{
	  if($this->session->userdata('mem_id')=='')
	  { redirect('login'); }
	  
	}
	/**** all faq video function****/
	
	function index($slug = 0)
	{
		$search ='';
		if($_POST['search'])
		{
			$this->form_validation->set_rules('search','Search Text','trim|required');
			if($this->form_validation->run())
			 {
				$search  = $this->input->post('search');
				$url  = $this->config->item('membersarea_url').'faq/userfaq?search='.urlencode($search);
				
			 } 
		}
		else{
		if($slug!='')
		 	$url = $this->config->item('membersarea_url').'faq/userfaq?slug='.urlencode($slug);
		else
			$url = $this->config->item('membersarea_url').'faq/userfaq';
		} 	
		
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		$output = curl_exec($ch);
		curl_close($ch);
		$output = json_decode($output);
		 
		if($slug!='0')
		{
			$output->active_slug = $slug;
		}
		else
		{
			$output->active_slug = $output->allCat[0]->slug;
		}
		$output->search = $search;
		//print_r($output);die;
		$this->load->view($this->config->item('templateName').'/header',$output);
		$this->load->view($this->config->item('templateName').'/faq');
		$this->load->view($this->config->item('templateName').'/footer');
	}
}