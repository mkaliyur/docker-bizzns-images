<?php
defined('BASEPATH') OR exit('No direct script access allowed');
include('app.php');

class Template extends App {

	function __construct()
	{ 
	    parent::__construct();				
		$this->load->model('template_model');
		$this->checkLogin();
		$this->member_id = $this->session->userdata('mem_id');
		
	}
	function checkLogin()
	{
	  if($this->session->userdata('mem_id')=='')
	  { redirect('login'); }
	  
	}
	public function index($page=null)
    {       
	 
	  if(empty($page)){$page='default';
		$output['meta_title'] = 'Template Default';
	  }
	  else{
		  $output['meta_title'] = 'Template Default';
	  }
	  if($page=='default')
	   {
	    $email_template_count = $this->email_template_count;
	   }
	    $output['allRecord'] = $this->template_model->getTemplateList($this->member_id,$page,'',$email_template_count, $output );
		
	   $this->load->view($this->config->item('templateName').'/header' );
       if($page=='draft')
       $this->load->view($this->config->item('templateName').'/template/draft' );
       else
       $this->load->view($this->config->item('templateName').'/template/list' );
	   $this->load->view($this->config->item('templateName').'/footer' );
	}


	public function addTemplate()
	{
	 $output['meta_title'] = 'Template Create';
	   if($this->create_template!='yes')
		 { 
		   $this->session->set_userdata('error_msg','Please upgrade to use add template feature. <a target="_blank" href="'.$this->upgrade_link.'" class="upgradelink">Click here to upgrade </a>');
		   redirect('template-list');
		 }
		 
		if($_POST)
		 {
		   $output['template_name'] = $this->input->post('template_name');
		   $output['template_text'] = $this->input->post('template_text');
		   
		   $this->form_validation->set_rules('template_name','Template Name','trim|required');
		   $this->form_validation->set_rules('template_text','Template Text','trim|required');
		   
		   if($this->form_validation->run())
		    {       
			  $template_id = $this->template_model->addTemplate($this->member_id,'plain');
			  
			    $filteredData=substr($_POST['image_text'], strpos($_POST['image_text'], ",")+1);
	            $unencodedData=base64_decode($filteredData);
				$filename = $template_id.time().'.png';
	            file_put_contents('./assets/uploads/template_images/'.$filename, $unencodedData);
				$this->template_model->updateImage($filename,$template_id);
				
			  $this->session->set_userdata('success_msg','Template Saved To Draft Successfully');
               redirect('template-list/draft');
			}
		 }
		$this->load->view($this->config->item('templateName').'/header',$output);
		$this->load->view($this->config->item('templateName').'/template/edit',null);
		$this->load->view($this->config->item('templateName').'/footer',null);
	}
	public function editTemplate($id,$page)
	{
		 $output['meta_title'] = 'Template Edit';
		$template_detail        = $this->template_model->getTemplateById($id);
		$template_name          = $template_detail->template_name;
		$template_text          = $template_detail->template_text;
		$template_screenshot    = $template_detail->template_screenshot;
		$editor_type = $template_detail->editor_type;
		
        if($_POST)
		 {

		   $template_name = $this->input->post('template_name');
		   $template_text = $this->input->post('template_text'); 
		   
		   $this->form_validation->set_rules('template_name','Template Name','trim|required');
		   $this->form_validation->set_rules('template_text','Template Text','trim|required');
		   
		   if($this->form_validation->run())
		    {     
			  if(empty($page)) {
               $template_id =  $this->template_model->addTemplate($this->member_id,$editor_type); 
			 } else {
                $this->template_model->updateTemplate($id,$editor_type);
				$template_id = $id;
			  }
				
				$filteredData=substr($_POST['image_text'], strpos($_POST['image_text'], ",")+1);
	            $unencodedData=base64_decode($filteredData);
				$filename = $template_id.time().'.png';
	            file_put_contents('./assets/uploads/template_images/'.$filename, $unencodedData);
				$this->template_model->updateImage($filename,$template_id);
				
			  
			  $this->session->set_userdata('success_msg','Template Saved To Draft Successfully');
              redirect('template-list/draft');
            }
		  
		 }
		 
		$output['template_name'] = $template_name;
		$output['template_text'] = $template_text;
        $output['template_id'] = $id; 
		
		if($template_detail->editor_type=='inline')
		$this->load->view($this->config->item('templateName').'/template/inline_edit',$output);
		else
		{
			$this->load->view($this->config->item('templateName').'/header',$output);
			$this->load->view($this->config->item('templateName').'/template/edit');
			$this->load->view($this->config->item('templateName').'/footer');
	     }
	}
	public function deleteTemplate($id)
	{ 
		$template_id=array();
		$template_id[]=$id;
		$arr = $this->template_model->getTemplateScreenshots($template_id);
		foreach ($arr as $screenshotdelete) {
			unlink('./assets/uploads/template_pics/' . $screenshotdelete->template_screenshot);
		}
		$this->template_model->deleteTemplate($id);
		$this->session->set_userdata('success_msg','Template Deleted Successfully');
		redirect('template-list/draft');
     }
    public function deleteMultiTemplate()
	{ 
	  if($_POST)
	   {
		 $template_id = $this->input->post('id');
		 $arr=$this->template_model->getTemplateScreenshots($template_id);
           foreach ($arr as $screenshotdelete){
             unlink('./assets/uploads/template_pics/'.$screenshotdelete->template_screenshot);
            }
		 $this->template_model->removeMultipleTemplate($template_id);
         $this->session->set_userdata('success_msg','Draft Deleted successfully');
		 $data['success'] = true;
		}
	   echo json_encode($data); die;
	   }
}
