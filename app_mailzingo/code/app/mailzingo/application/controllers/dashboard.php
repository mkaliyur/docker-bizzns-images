<?php
defined('BASEPATH') OR exit('No direct script access allowed');
include('app.php');

class Dashboard extends App {

	function __construct()
	{ 
	    parent::__construct();				
		$this->load->model('member_model');
		$this->load->model('dashboard_model');
		$this->load->model('contact_model');
		$this->member_id = $this->session->userdata('mem_id');
		$this->checkLogin();
 
		
	}
	function checkLogin()
	{
	  if($this->session->userdata('mem_id')=='')
	  { redirect('login'); }
	  
	}
	
	public function index()
	{
		// $output['total_contacts'] = $this->dashboard_model->getAllContactCount('',$this->member_id);
		//$output['today_contacts'] = $this->dashboard_model->getAllContactCount('',$this->member_id,'day');
		//$output['contact_month'] = $this->dashboard_model->getAllContactCount('',$this->member_id,'month');
		//$output['today_unsubscribe'] = $this->dashboard_model->getAllUnsubscribeCount('',$this->member_id,'day');
		$output['meta_title'] = "Dashboard";	
	   $total_contacts = $this->contact_model->getAllCount($this->member_id,'active');
	   if($total_contacts>=$this->subscriber_count && $this->subscriber_count!='unlimited')
	    {
		  $output['contact_info_msg'] = 'You have added '.$total_contacts.' subscribers. Please upgrade to add more than '.$this->subscriber_count.' subscribers. <a target="_blank" href="'.$this->upgrade_link.'" class="upgradelink upgrade-btn">Click here to upgrade </a>';
		}
		
		$this->load->view($this->config->item('templateName').'/header',$output);
		$this->load->view($this->config->item('templateName').'/dashboard');
		$this->load->view($this->config->item('templateName').'/footer');
		
	}
	public function getGraph()
	{
	  $output['time_type'] = $time_type = $this->input->post('time_type');
	  $output['graph_type'] = $graph_type = $this->input->post('graph_type');
	  $output['data'] = $data = $this->dashboard_model->getSubscriberGraph($time_type,$graph_type,$this->member_id);
	  
	  $response['html'] = $this->load->view($this->config->item('templateName').'/dashboard_graph',$output,true);
	  $response['success'] = true;
	  $this->output
		   ->set_content_type('application/json')
		   ->set_output(json_encode($response));
	}
	public function getStatsCount()
	{
	  $stats_type = $_POST['stats_type'];
	  
	  if($stats_type=='total_contacts') 
	   $response['count_data'] = $this->dashboard_model->getAllContactCount('',$this->member_id);
	  else if($stats_type=='today_contacts') 
	   $response['count_data'] = $this->dashboard_model->getAllContactCount('',$this->member_id,'day');
	  else if($stats_type=='contact_month') 
	   $response['count_data'] = $this->dashboard_model->getAllContactCount('',$this->member_id,'month');
	  else if($stats_type=='today_unsubscribe') 
	   $response['count_data'] = $this->dashboard_model->getAllUnsubscribeCount('',$this->member_id,'day');
	  
	  $response['success'] = true;
	  $this->output
		   ->set_content_type('application/json')
		   ->set_output(json_encode($response));
	}
}
