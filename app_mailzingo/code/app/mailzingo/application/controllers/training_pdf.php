<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Training_pdf extends CI_Controller {

	function __construct()
	{ 
	    parent::__construct();				
		$this->checkLogin();
	}
	function checkLogin()
	{
	  if($this->session->userdata('mem_id')=='')
	  { redirect('login'); }
	  
	}
	/**** all training video function****/
	public function index()
	{
		$output['meta_title'] = "Training PDF";
		$this->config->item('membersarea_url').'training_pdf/api_index'; 
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $this->config->item('membersarea_url').'training_pdf/api_index');
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		$output = curl_exec($ch);
		curl_close($ch);
		$output = json_decode($output);
		$output->page_name = 'Training Pdf';
		$this->load->view($this->config->item('templateName').'/header',$output);
		$this->load->view($this->config->item('templateName').'/training_pdf');
		$this->load->view($this->config->item('templateName').'/footer');
	}
	
}