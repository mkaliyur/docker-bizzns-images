-- phpMyAdmin SQL Dump
-- version 4.7.7
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Mar 04, 2019 at 12:22 PM
-- Server version: 5.6.41-84.1
-- PHP Version: 5.6.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `mailzingo`
--

-- --------------------------------------------------------

--
-- Table structure for table `ci_sessions`
--

DROP TABLE IF EXISTS `ci_sessions`;
CREATE TABLE `ci_sessions` (
  `session_id` varchar(40) NOT NULL DEFAULT '0',
  `ip_address` varchar(45) NOT NULL DEFAULT '0',
  `user_agent` varchar(255) NOT NULL,
  `last_activity` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `user_data` longtext CHARACTER SET utf8 COLLATE utf8_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ci_sessions`
--

INSERT INTO `ci_sessions` (`session_id`, `ip_address`, `user_agent`, `last_activity`, `user_data`) VALUES
('100272b83bfdc37e39abd45f465fe035', '192.185.225.111', '0', 1551714481, ''),
('16cf1c7d8e597b505cdcbbbd0d4c08a1', '192.185.225.111', '0', 1551721681, ''),
('1db84c54ca41b77b8e8e5efb81983b48', '192.185.225.111', '0', 1551722404, ''),
('20655ab6f6f7931438feb8439e0edccb', '192.185.225.111', '0', 1551711604, ''),
('21e951d565f423c74881475c8d7e84ee', '192.185.225.111', 'curl/7.19.7 (x86_64-redhat-linux-gnu) libcurl/7.19.7 NSS/3.27.1 zlib/1.2.3 libidn/1.18 libssh2/1.4.2', 1551715202, ''),
('28776059cfbdde39f18fa5e7a91cf5e3', '192.185.225.111', '0', 1551716641, ''),
('2883b65100d2ea74c2ed0e81755dc346', '192.185.225.111', 'curl/7.19.7 (x86_64-redhat-linux-gnu) libcurl/7.19.7 NSS/3.27.1 zlib/1.2.3 libidn/1.18 libssh2/1.4.2', 1551713042, ''),
('2b61db866c40fe10839573c3046ab66a', '192.185.225.111', '0', 1551713042, ''),
('2ba3f0f97184a292141e83252250cab7', '192.185.225.111', 'curl/7.19.7 (x86_64-redhat-linux-gnu) libcurl/7.19.7 NSS/3.27.1 zlib/1.2.3 libidn/1.18 libssh2/1.4.2', 1551722403, ''),
('2e53046bdc95a169158adc722c0e9250', '192.185.225.111', '0', 1551722403, ''),
('311093f76a382dec5a205640886caaa6', '192.185.225.111', '0', 1551718081, ''),
('31bb5e1b842c004cfe7949bd89970c35', '192.185.225.111', '0', 1551721681, ''),
('3e0e0ab83af522787baf95a8339220b7', '192.185.225.111', '0', 1551720241, ''),
('50c656f9ae985face7e3ebc46d4c4dfa', '192.185.225.111', '0', 1551716641, ''),
('6dbebe6738af7e08b6a3c8ac9eddab90', '192.185.225.111', '0', 1551720241, ''),
('6eea86f0dbc69d4808770c927e23bde7', '192.185.225.111', '0', 1551718081, ''),
('72280a7d14970b7397b8f6490ae4f9b9', '192.185.225.111', '0', 1551721681, ''),
('8d3916c22b3315d8bf161c504adde8d3', '192.185.225.111', '0', 1551718081, ''),
('8f5f23f60a0513424788c82c62e53fe9', '192.185.225.111', '0', 1551718804, ''),
('920292da1ab2d5e3058f0a3beaecdeac', '192.185.225.111', '0', 1551714481, ''),
('942e6bee32af0624555f3e56b975ff26', '192.185.225.111', 'curl/7.19.7 (x86_64-redhat-linux-gnu) libcurl/7.19.7 NSS/3.27.1 zlib/1.2.3 libidn/1.18 libssh2/1.4.2', 1551716641, ''),
('9bbf18a0ae7b0883e11b71201f593c5a', '192.185.225.111', '0', 1551711604, ''),
('9de1b1bc0e022536823d7fd5347cd75c', '192.185.225.111', '0', 1551711604, ''),
('a448f86dd7ea0e2157a9e71d698e01fb', '192.185.225.111', 'curl/7.19.7 (x86_64-redhat-linux-gnu) libcurl/7.19.7 NSS/3.27.1 zlib/1.2.3 libidn/1.18 libssh2/1.4.2', 1551714481, ''),
('a605d020d59877b783f96503f9515ae5', '192.185.225.111', '0', 1551718804, ''),
('a7b0990248b52473c465d3ed1bc1c015', '192.185.225.111', 'curl/7.19.7 (x86_64-redhat-linux-gnu) libcurl/7.19.7 NSS/3.27.1 zlib/1.2.3 libidn/1.18 libssh2/1.4.2', 1551720241, ''),
('b18ecff8cd91ae3dc60c6a0178933ec4', '192.185.225.111', 'curl/7.19.7 (x86_64-redhat-linux-gnu) libcurl/7.19.7 NSS/3.27.1 zlib/1.2.3 libidn/1.18 libssh2/1.4.2', 1551718803, ''),
('ba66c9cfc8553ba8a2a173790475103d', '192.185.225.111', '0', 1551716641, ''),
('c4edf60e9cc49251b143c6af96ef5f2a', '192.185.225.111', '0', 1551714481, ''),
('c798e245c81c1ce1d93acdf3ea87c6cf', '52.200.151.155', 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/60.0.3112.101 Safari/537.36', 1551715848, ''),
('cccec815879a16031afe241d317e8a24', '192.185.225.111', 'curl/7.19.7 (x86_64-redhat-linux-gnu) libcurl/7.19.7 NSS/3.27.1 zlib/1.2.3 libidn/1.18 libssh2/1.4.2', 1551721681, ''),
('ccea9b708acc5d6f4bee58d9592aa995', '192.185.225.111', '0', 1551715203, ''),
('cf4355439f2913848581d42d6d89e354', '52.200.151.155', 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/60.0.3112.101 Safari/537.36', 1551715839, ''),
('cf92574620cad481c0665a1490a0cb62', '192.185.225.111', '0', 1551722403, ''),
('d141fdcf672becd362959d3830978222', '192.185.225.111', 'curl/7.19.7 (x86_64-redhat-linux-gnu) libcurl/7.19.7 NSS/3.27.1 zlib/1.2.3 libidn/1.18 libssh2/1.4.2', 1551711604, ''),
('d974adf3c01f5d1e65ceda27ce2b9175', '192.185.225.111', '0', 1551718803, ''),
('da8a37ae85a8b8b21793cd89d9345f88', '192.185.225.111', 'curl/7.19.7 (x86_64-redhat-linux-gnu) libcurl/7.19.7 NSS/3.27.1 zlib/1.2.3 libidn/1.18 libssh2/1.4.2', 1551718081, ''),
('de9eb7a48d7aed05afeffe0594b4d8f4', '192.185.225.111', '0', 1551715203, ''),
('e71f9eb2d3245e7e3b2c8a411e36bc90', '192.185.225.111', '0', 1551713042, ''),
('e78836f7dbbdb856ca51a63083ceb2fa', '192.185.225.111', '0', 1551720241, ''),
('f5a396ea02430cb3f0a4740d531db1e5', '192.185.225.111', '0', 1551715203, ''),
('fc3db8f52677c9778af304f1a01c1a07', '192.185.225.111', '0', 1551713042, '');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_autoresponder`
--

DROP TABLE IF EXISTS `tbl_autoresponder`;
CREATE TABLE `tbl_autoresponder` (
  `id` int(11) NOT NULL,
  `member_id` int(11) NOT NULL,
  `from_name` varchar(255) NOT NULL,
  `from_email` varchar(255) NOT NULL,
  `reply_to` varchar(255) NOT NULL,
  `auto_name` varchar(255) NOT NULL,
  `subject` varchar(255) NOT NULL,
  `template_id` int(11) DEFAULT NULL,
  `template_text` longtext CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `image` varchar(255) DEFAULT NULL,
  `in_campaign_id` varchar(255) NOT NULL,
  `mail_day` int(11) DEFAULT NULL,
  `enable_on` varchar(255) DEFAULT NULL,
  `send_time` time DEFAULT NULL,
  `status` enum('active','inactive') NOT NULL DEFAULT 'active',
  `add_time` int(11) NOT NULL,
  `ip` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_campaign`
--

DROP TABLE IF EXISTS `tbl_campaign`;
CREATE TABLE `tbl_campaign` (
  `campaign_id` bigint(20) NOT NULL,
  `campaign_name` varchar(255) NOT NULL,
  `campaign_description` text,
  `member_id` bigint(20) NOT NULL,
  `add_time` int(11) NOT NULL,
  `ip` varchar(255) NOT NULL,
  `status` enum('active','inactive') NOT NULL DEFAULT 'active'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_campaign`
--

INSERT INTO `tbl_campaign` (`campaign_id`, `campaign_name`, `campaign_description`, `member_id`, `add_time`, `ip`, `status`) VALUES
(1, 'mysoretravels', 'Test Campaign for mysore travels', 1, 1495218997, '103.228.222.83', 'active'),
(2, 'Lease Option Sellers', 'Capture details of LeaseOption Sellers', 1, 1512844227, '122.166.152.103', 'active'),
(3, 'Lease Option Buyers - Tallahassee FL', NULL, 1, 1512844266, '122.166.152.103', 'active'),
(4, 'Lease Option Buyers - Bakersfield CA', NULL, 1, 1512844295, '122.166.152.103', 'active'),
(6, 'Jack Allen Homes ContactUs', NULL, 1, 1513166586, '103.228.222.10', 'active');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_contact`
--

DROP TABLE IF EXISTS `tbl_contact`;
CREATE TABLE `tbl_contact` (
  `id` int(11) NOT NULL,
  `member_id` int(11) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `email` varchar(255) NOT NULL,
  `campaign_id` int(11) NOT NULL,
  `status` enum('active','inactive') NOT NULL DEFAULT 'active',
  `ip` varchar(255) NOT NULL,
  `add_time` int(11) NOT NULL,
  `address` text,
  `city` varchar(255) DEFAULT NULL,
  `state` varchar(255) DEFAULT NULL,
  `country` varchar(255) DEFAULT NULL,
  `zip` varchar(255) DEFAULT NULL,
  `phone` varchar(255) DEFAULT NULL,
  `custom_1` varchar(255) DEFAULT NULL,
  `custom_2` varchar(255) DEFAULT NULL,
  `custom_3` varchar(255) DEFAULT NULL,
  `add_from` enum('import','add','form') NOT NULL DEFAULT 'import',
  `varification_code` varchar(255) DEFAULT NULL,
  `autoresponder_sent_time` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_contact`
--

INSERT INTO `tbl_contact` (`id`, `member_id`, `name`, `email`, `campaign_id`, `status`, `ip`, `add_time`, `address`, `city`, `state`, `country`, `zip`, `phone`, `custom_1`, `custom_2`, `custom_3`, `add_from`, `varification_code`, `autoresponder_sent_time`) VALUES
(1, 1, 'Manju', 'mkaliyur@gmail.com', 1, 'active', '103.228.222.83', 1495219194, NULL, NULL, NULL, 'India', NULL, NULL, NULL, NULL, NULL, 'import', NULL, NULL),
(2, 1, 'manju', 'test@test.com', 1, 'active', '103.228.222.1', 1502358493, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'form', NULL, NULL),
(3, 1, 'raymond lane', 'jack.allen0170@gmail.com', 1, 'active', '103.228.222.1', 1502362144, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'form', NULL, NULL),
(4, 1, 'patrick', 'versteegpatrick@gmail.com', 1, 'active', '103.228.222.1', 1502377584, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'form', NULL, NULL),
(5, 1, NULL, 'jack.allen0170@gmail.com', 3, 'active', '103.228.222.10', 1513077215, NULL, NULL, NULL, NULL, '32301', '2222222222', NULL, NULL, NULL, 'form', NULL, NULL),
(6, 1, NULL, 'b@c.com', 3, 'active', '103.228.222.10', 1513077764, NULL, NULL, NULL, NULL, '32301', '2222222222', NULL, NULL, NULL, 'form', NULL, NULL),
(7, 1, NULL, 'm@n.com', 3, 'active', '103.228.222.10', 1513078063, NULL, NULL, NULL, NULL, '93306', '999999', NULL, NULL, NULL, 'form', NULL, NULL),
(8, 1, 'got', 'm@n.com', 2, 'active', '103.228.222.10', 1513168423, '14604 Lakeside Way', NULL, NULL, NULL, '93306', NULL, NULL, NULL, NULL, 'form', NULL, NULL),
(9, 1, NULL, 'a@b.com', 3, 'active', '103.228.222.10', 1513170664, NULL, NULL, NULL, NULL, '75024', '999999999', NULL, NULL, NULL, 'form', NULL, NULL),
(10, 1, 'ddg', 'r@g.com', 6, 'active', '103.228.222.10', 1513239766, NULL, NULL, NULL, NULL, NULL, '(989) 999-9999', NULL, NULL, NULL, 'form', NULL, NULL),
(11, 1, NULL, 'm@n.com', 4, 'active', '103.228.222.10', 1513316823, NULL, NULL, NULL, NULL, '57002', '2222222222', NULL, NULL, NULL, 'form', NULL, NULL),
(12, 1, 'Sherlyn Beteille', 'sherlynbeteille@gmail.com', 6, 'active', '171.50.128.195', 1516336604, NULL, NULL, NULL, NULL, NULL, '(120) 120-1200', NULL, NULL, NULL, 'form', NULL, NULL),
(13, 1, 'Susan Bennett', 'MaryytyFrouninclues@gmail.com', 6, 'active', '166.88.120.17', 1524246526, NULL, NULL, NULL, NULL, NULL, '610-222-6056', NULL, NULL, NULL, 'form', NULL, NULL),
(14, 1, 'Designer Nik', 'nik@creativedesigns.info', 3, 'active', '158.69.17.232', 1527526047, NULL, NULL, NULL, NULL, NULL, '415-231-9876', NULL, NULL, NULL, 'form', NULL, NULL),
(15, 1, NULL, 'Lairenehinds@icloud.com', 3, 'active', '216.212.243.130', 1528647623, NULL, NULL, NULL, NULL, '32317', '8502943999', NULL, NULL, NULL, 'form', NULL, NULL),
(16, 1, NULL, 'Laurenehinds@iclpud.com', 3, 'active', '216.212.243.130', 1528647742, NULL, NULL, NULL, NULL, '32317', '8502943999', NULL, NULL, NULL, 'form', NULL, NULL),
(17, 1, NULL, 'Laurenehinds@icloud.com', 3, 'active', '216.212.243.130', 1528647826, NULL, NULL, NULL, NULL, '31311', '8502943999', NULL, NULL, NULL, 'form', NULL, NULL),
(18, 1, 'Randy', 'Randy@TalkWithLead.com', 6, 'active', '107.173.29.109', 1531016703, NULL, NULL, NULL, NULL, NULL, '416-385-3200', NULL, NULL, NULL, 'form', NULL, NULL),
(19, 1, 'Susan Davis', 'SusanDavis@yourvideodeals.com', 6, 'active', '192.210.227.188', 1531927085, NULL, NULL, NULL, NULL, NULL, '610-222-6056', NULL, NULL, NULL, 'form', NULL, NULL),
(20, 1, 'Sherry Williams', 'contact@dynametrixdigital.org', 6, 'active', '45.61.153.118', 1532669846, NULL, NULL, NULL, NULL, NULL, '(866) 582-0776', NULL, NULL, NULL, 'form', NULL, NULL),
(21, 1, 'Taylor Allen', 'taylort7a8eallen@yahoo.com', 6, 'active', '196.196.86.193', 1534401151, NULL, NULL, NULL, NULL, NULL, '(805) 372-1751', NULL, NULL, NULL, 'form', NULL, NULL),
(22, 1, 'Victoria Clark', 'victoriadw9o7clark@yahoo.com', 6, 'active', '191.96.240.81', 1534429198, NULL, NULL, NULL, NULL, NULL, '619-455-7751', NULL, NULL, NULL, 'form', NULL, NULL),
(23, 1, 'Travis Lathrop', 'produccion@festivalgradual.com', 6, 'active', '199.34.89.2', 1536288264, NULL, NULL, NULL, NULL, NULL, '01.25.18.32.07', NULL, NULL, NULL, 'form', NULL, NULL),
(24, 1, NULL, 'shivanov10@gmail.com', 1, 'active', '103.228.222.83', 1536587444, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'form', NULL, NULL),
(25, 1, 'Winston', '777SMO@TheFirstPagePlan.com', 6, 'active', '173.19.107.175', 1536625731, NULL, NULL, NULL, NULL, NULL, '1-319-423-9473', NULL, NULL, NULL, 'form', NULL, NULL),
(26, 1, 'Madison Williams', 'madisondtn49williams@aol.com', 6, 'active', '107.172.49.92', 1537103436, NULL, NULL, NULL, NULL, NULL, '(805) 372-1751', NULL, NULL, NULL, 'form', NULL, NULL),
(27, 1, 'Susan', 'susanking@videodeal1.com', 6, 'active', '192.227.191.162', 1539021871, NULL, NULL, NULL, NULL, NULL, '323-129-3189', NULL, NULL, NULL, 'form', NULL, NULL),
(28, 1, 'Savannah', 'savannahu3vkdcarter@aol.com', 6, 'active', '172.245.202.147', 1540524779, NULL, NULL, NULL, NULL, NULL, '573-738-8918', NULL, NULL, NULL, 'form', NULL, NULL),
(29, 1, 'William Rivenburg', 'rivenburg.william93@gmail.com', 6, 'active', '23.254.55.110', 1542232293, NULL, NULL, NULL, NULL, NULL, '250-375-5193', NULL, NULL, NULL, 'form', NULL, NULL),
(30, 1, NULL, 'Bbb@gmail.com', 3, 'active', '68.35.241.246', 1542574374, NULL, NULL, NULL, NULL, '32312', '8509262551', NULL, NULL, NULL, 'form', NULL, NULL),
(31, 1, 'Anna', 'annaoqmp2green@aol.com', 6, 'active', '209.99.172.9', 1543779690, NULL, NULL, NULL, NULL, NULL, '573-738-8918', NULL, NULL, NULL, 'form', NULL, NULL),
(32, 1, 'Susan', 'susanmorris@videodeal1.com', 6, 'active', '192.3.246.49', 1544024740, NULL, NULL, NULL, NULL, NULL, '323-129-2854', NULL, NULL, NULL, 'form', NULL, NULL),
(33, 1, 'Brooke', 'brookecvsulsmith@aol.com', 6, 'active', '38.125.234.134', 1545069850, NULL, NULL, NULL, NULL, NULL, '(805) 372-1751', NULL, NULL, NULL, 'form', NULL, NULL),
(34, 1, 'Antwanhow', 'yourmail@gmail.com', 6, 'active', '107.181.78.138', 1545711688, NULL, NULL, NULL, NULL, NULL, '81286748298', NULL, NULL, NULL, 'form', NULL, NULL),
(35, 1, 'Susan', 'susangrey@govideopromo.com', 6, 'active', '158.222.6.159', 1546010485, NULL, NULL, NULL, NULL, NULL, '323-129-2755', NULL, NULL, NULL, 'form', NULL, NULL),
(36, 1, NULL, 'reginabarkley@gmail.com', 3, 'active', '66.87.204.79', 1546381048, NULL, NULL, NULL, NULL, '32309', '8504432007', NULL, NULL, NULL, 'form', NULL, NULL),
(37, 1, NULL, 'quint965@icloud.com', 3, 'active', '68.63.38.152', 1547146624, NULL, NULL, NULL, NULL, '32305', '8505667291', NULL, NULL, NULL, 'form', NULL, NULL),
(38, 1, 'Allison', 'allisonosavrgonzalez@aol.com', 6, 'active', '45.61.153.109', 1549354467, NULL, NULL, NULL, NULL, NULL, '573-738-8918', NULL, NULL, NULL, 'form', NULL, NULL),
(39, 1, 'Mackenzie', 'epopoes@yahoo.com', 6, 'active', '67.213.118.233', 1549553679, NULL, NULL, NULL, NULL, NULL, '805-372-1751', NULL, NULL, NULL, 'form', NULL, NULL),
(40, 1, 'Savannah', 'birzhwort@yahoo.com', 6, 'active', '173.44.155.41', 1549785121, NULL, NULL, NULL, NULL, NULL, '573-738-8918', NULL, NULL, NULL, 'form', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_cron_urls`
--

DROP TABLE IF EXISTS `tbl_cron_urls`;
CREATE TABLE `tbl_cron_urls` (
  `id` int(11) NOT NULL,
  `time` datetime NOT NULL,
  `newsletter_id` int(11) NOT NULL,
  `url` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_email_queue`
--

DROP TABLE IF EXISTS `tbl_email_queue`;
CREATE TABLE `tbl_email_queue` (
  `id` int(11) NOT NULL,
  `newsletter_id` int(11) NOT NULL,
  `contact_id` int(11) NOT NULL,
  `cron_id` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_external_mail_api`
--

DROP TABLE IF EXISTS `tbl_external_mail_api`;
CREATE TABLE `tbl_external_mail_api` (
  `id` int(11) NOT NULL,
  `api_name` varchar(200) NOT NULL,
  `api_value` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_file_attatch`
--

DROP TABLE IF EXISTS `tbl_file_attatch`;
CREATE TABLE `tbl_file_attatch` (
  `id` int(11) NOT NULL,
  `file_name` varchar(255) NOT NULL,
  `newsletter_id` int(11) NOT NULL,
  `type` enum('newsletter','autoresponder') NOT NULL DEFAULT 'newsletter'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_form_template`
--

DROP TABLE IF EXISTS `tbl_form_template`;
CREATE TABLE `tbl_form_template` (
  `id` int(11) NOT NULL,
  `template_name` varchar(255) DEFAULT NULL,
  `template_text` longtext CHARACTER SET utf8 COLLATE utf8_bin,
  `member_id` int(11) DEFAULT NULL,
  `add_time` int(11) DEFAULT NULL,
  `ip` varchar(255) NOT NULL,
  `status` enum('active','inactive') NOT NULL DEFAULT 'active'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_form_template`
--

INSERT INTO `tbl_form_template` (`id`, `template_name`, `template_text`, `member_id`, `add_time`, `ip`, `status`) VALUES
(1, 'Template 1', '<div class=\"form1-bg\" id=\"demo-preview\">\n<div class=\"row\"> \n<div class=\"col-md-12 col-sm-12 col-xs-12\">\n<div class=\"col-md-12 col-sm-12 col-xs-12 form_area1\">\n<div class=\"mdem17 smem15 xsem12 blue text-center\" contenteditable=\"true\">TAKE THE BEST</div>\n<div class=\"mdem17 smem15 xsem12 blue text-center\" contenteditable=\"true\"><b>ONLINE COURSES</b> AVAILABLE</div>\n<div class=\"col-md-10 col-md-offset-1 col-sm-10 col-sm-offset-1 col-xs-10 col-xs-offset-1 mt7 xsmt5\">\n<div class=\"namefield\">\n<div class=\"mdem11 smem11 xsem10 blacktext w500\" contenteditable=\"true\">Name</div>\n<div class=\"mdem11 smem11 xsem10\"><input type=\"text\"  name=\"name\" class=\"fieldinput\" placeholder=\"Name\"/></div>\n</div>\n<div class=\"copydiv\">\n<div class=\"mdem11 smem11 xsem10 blacktext mt2 xsmt4 w500 fieldname\" contenteditable=\"true\">Email Address</div>\n<div class=\"mdem12 smem11 xsem10\"><input type=\"text\" name=\"email\" class=\"fieldinput\" placeholder=\"Email\" /></div>\n</div>\n<div class=\"mdem12 smem12 xsem11 mt15 xsmt10 w400\">\n<a href=\"#\" class=\"form-btn1\" contenteditable=\"true\" id=\"rect\">Sign Up Today</a></div>\n</div>\n</div>\n</div>				\n</div>\n</div>', 1, 1470394007, '', 'active'),
(2, 'Template 2', '<div class=\"form2-bg\" id=\"demo-preview\">\n<div class=\"row\"> \n<div class=\"col-md-12 col-sm-12 col-xs-12\">\n<div class=\"col-md-12 col-sm-12 col-xs-12 form_area2\">\n<div class=\"mdem17 smem15 xsem13 heading w500 text-center robotoslab\" contenteditable=\"true\">EXPERIENCE ADVANTURE <br> OF A LIFE TIME</div>\n<div class=\"mdem13 smem12 xsem12 preheading text-center w300 mt1 xsmt1\" contenteditable=\"true\">For discount, fill in the form</div>\n<div class=\"col-md-10 col-md-offset-1 col-sm-10 col-sm-offset-1 col-xs-10 col-xs-offset-1 mt9 xsmt10\">\n<div class=\"namefield\">\n<div class=\"mdem10 smem10 xsem10 whitetext w300\" contenteditable=\"true\">Full Name</div>\n<div class=\"mdem10 smem10 xsem10\"><input type=\"text\" name=\"name\" class=\"fieldinput\" placeholder=\"Name\"></div>\n</div>\n<div class=\"copydiv\">\n<div class=\"mdem10 smem10 xsem10 whitetext mt2 xsmt4 w300 fieldname\" contenteditable=\"true\">Email Address</div>\n<div class=\"mdem10 smem10 xsem10\"><input type=\"text\"  name=\"email\" class=\"fieldinput\" placeholder=\"Email\"></div>\n</div>\n<div class=\"mdem11 smem11 xsem11 mt10 xsmt10 w300\"><a href=\"#\" class=\"form-btn2\" contenteditable=\"true\"  id=\"rect\">Send My Offer</a></div>\n</div>\n</div>\n</div>				\n</div>\n</div>', 1, 1470394007, '', 'active'),
(3, 'Template 3', '<div class=\"form3-bg\" id=\"demo-preview\">\n<div class=\"row\"> \n<div class=\"form_area3\">\n<div class=\"col-md-12 col-sm-12 col-xs-12 innerform\">\n<div class=\"mdem17 smem15 xsem13 whitetext text-center w600\" contenteditable=\"true\">Want To Get Smart Tips <br> From Our Experts</div>\n<div class=\"mdem11 smem11 xsem11 whitetext text-center w300 mt2 xsmt2\" contenteditable=\"true\">For Details, Fill In The Form</div>\n<div class=\"col-md-10 col-md-offset-1 col-sm-10 col-sm-offset-1 col-xs-10 col-xs-offset-1 mt9 xsmt10\">\n<div class=\"namefield\">\n<div class=\"mdem10 smem10 xsem10 whitetext w300\" contenteditable=\"true\">Full Name</div>\n<div class=\"mdem10 smem10 xsem10\"><input type=\"text\"  name=\"name\" class=\"fieldinput\" placeholder=\"Name\"></div>\n</div>\n<div class=\"copydiv\">\n<div class=\"mdem10 smem10 xsem10 whitetext mt2 xsmt4 w300 fieldname\" contenteditable=\"true\">Email Address</div>\n<div class=\"mdem10 smem10 xsem10\"><input type=\"text\"  name=\"email\" class=\"fieldinput\" placeholder=\"Email\"></div>\n</div>\n<div class=\"mdem11 smem10 xsem10 mt10 xsmt10 w300\"><a href=\"#\" class=\"form-btn3\" contenteditable=\"true\"  id=\"rect\">Send Me Details</a></div>\n</div>\n</div>\n</div>				\n</div>\n</div>', 1, 1470394007, '', 'active'),
(4, 'Template 4', '<div class=\"form4-bg\"  id=\"demo-preview\">\n<div class=\"row\"> \n<div class=\"col-md-12 col-sm-12 col-xs-12\">\n<div class=\"col-md-12 col-sm-12 col-xs-12 form_area4\">\n<div class=\"mdem18 smem15 xsem12 whitetext w700 text-center\" contenteditable=\"true\">CONCERTS</div>\n<div class=\"mdem10 smem12 xsem11 whitetext text-center w500 mt3 xsmt3\" contenteditable=\"true\">DON\'T MISS ANY OF OUR AWESOME CONCERTS</div>\n<div class=\"mdem9 smem12 xsem11 whitetext text-center w300 mt1 xsmt1\" contenteditable=\"true\">Limited amount of passes, reserve your ticket today!</div>\n<div class=\"col-md-8 col-md-offset-2 col-sm-10 col-sm-offset-1 col-xs-10 col-xs-offset-1 mt7 xsmt10\">\n<div class=\"namefield\">\n<div class=\"mdem10 smem10 xsem10 whitetext w300\" contenteditable=\"true\">Full Name</div>\n<div class=\"mdem10 smem10 xsem10\"><input type=\"text\" name=\"name\" class=\"fieldinput\" placeholder=\"Name\"></div>\n</div>\n<div class=\"copydiv\">\n<div class=\"mdem10 smem10 xsem10 whitetext w300 fieldname\" contenteditable=\"true\">Email Address</div>\n<div class=\"mdem10 smem10 xsem10\"><input type=\"text\" name=\"email\" class=\"fieldinput\" placeholder=\"Email\"></div>\n</div>\n<div class=\"mdem11 smem11 xsem11 mt5 xsmt5 w600\"><a href=\"#\" class=\"form-btn4\" contenteditable=\"true\"  id=\"rect\">Reserve Today</a></div>\n</div>\n</div>\n</div>				\n</div>\n</div>', 1, 1470394007, '', 'active'),
(5, 'Template 5', '<div class=\"form5-bg\" id=\"demo-preview\">\n<div class=\"row\"> \n<div class=\"col-md-12 col-sm-12 col-xs-12\">\n<div class=\"col-md-12 col-sm-12 col-xs-12 form_area5\">\n<div class=\"mdem32 smem30 xsem22 greytext text-center breeserif\" contenteditable=\"true\">CAR EXHIBITION</div>\n<div class=\"col-md-10 col-md-offset-1 col-sm-10 col-sm-offset-1 col-xs-10 col-xs-offset-1 mt25 xsmt25\">\n<div class=\"namefield\">\n<div class=\"mdem10 smem9 xsem9\"><input type=\"text\" placeholder=\"Full Name\"  name=\"name\" class=\"fieldinput\"></div>\n</div>\n<div class=\"phonefield\">\n<div class=\"mdem10 smem9 xsem9\"><input type=\"text\"  placeholder=\"Phone Number\"  name=\"phone\" class=\"fieldinput\"></div>\n</div>\n<div class=\"copydiv\">\n<div class=\"mdem10 smem9 xsem9\"><input type=\"text\" placeholder=\"Email Address\"   name=\"email\" class=\"fieldinput\"></div>\n</div>\n<div class=\"mdem15 smem13 xsem13 mt10 xsmt10 w600\"><a href=\"#\" class=\"form-btn5\" contenteditable=\"true\"  id=\"rect\">Join Now</a></div>\n</div>\n</div>	\n</div>			\n</div>\n</div>', 1, 1470394007, '', 'active'),
(6, 'Template 6', '<div class=\"form6-bg\"  id=\"demo-preview\">\n<div class=\"row\"> \n<div class=\"col-md-12 col-sm-12 col-xs-12 form_area6\">\n<div class=\"col-md-8 col-md-offset-2 col-sm-8 col-sm-offset-2 col-xs-10 col-xs-offset-1 arrow_box\">\n<div class=\"mdem17 smem16 xsem15 whitetext w600 text-center letterspace mt13 xsmt5\"  contenteditable=\"true\">FREE COURSE</div>\n<div class=\"mdem10 smem10 xsem9 whitetext text-center w300 mt3 xsmt2\"  contenteditable=\"true\">Inroduction to computer science <br> For join TODAY, Fill in the form</div>\n</div>\n<div class=\"col-md-10 col-md-offset-1 col-sm-10 col-sm-offset-1 col-xs-10 col-xs-offset-1\">\n<br><br>\n<div class=\"namefield\">\n<div class=\"mdem10 smem10 xsem9 whitetext w300\"  contenteditable=\"true\">Full Name</div>\n<div class=\"mdem10 smem10 xsem9\"><input type=\"text\"  name=\"name\" class=\"fieldinput\" placeholder=\"Name\"></div>\n</div>\n<div class=\"copydiv\">\n<div class=\"mdem10 smem10 xsem9 whitetext mt2 xsmt0 w300 fieldname\"  contenteditable=\"true\">Email Address</div>\n<div class=\"mdem10 smem10 xsem9\"><input type=\"text\"  name=\"email\" class=\"fieldinput\" placeholder=\"Email\"></div>\n</div>\n<div class=\"mdem13 smem12 xsem11 mt10 xsmt10 w500\"><a href=\"#\" class=\"form-btn6\" contenteditable=\"true\"  id=\"rect\">Join Now</a></div>\n</div>\n</div>				\n</div>\n</div>', 1, 1470394007, '', 'active'),
(7, 'Template 7', '<div class=\"form7-bg\"  id=\"demo-preview\">\n<div class=\"row\"> \n<div class=\"col-md-12 col-sm-12 col-xs-12\">\n<div class=\"col-md-12 col-sm-12 col-xs-12 form_area7\">\n<div class=\"mdem17 smem15 xsem15 redtext w700 text-center\" contenteditable=\"true\">LETS KEEP IN TOUCH !</div>\n<div class=\"redarrow_box\"><div class=\"mdem12 smem12 xsem11 whitetext text-center w300\" contenteditable=\"true\">Sign up below to get <br> regular updat from our company</div></div>\n<div class=\"col-md-10 col-md-offset-1 col-sm-10 col-sm-offset-1 col-xs-10 col-xs-offset-1 mt5 xsmt5\">\n<div class=\"namefield\">\n<div class=\"mdem10 smem10 xsem10 black w500\" contenteditable=\"true\">Full Name</div>\n<div class=\"mdem10 smem10 xsem10\"><input type=\"text\" name=\"name\" class=\"fieldinput\" placeholder=\"Name\"></div>\n</div>\n<div class=\"copydiv\">\n<div class=\"mdem10 smem10 xsem10 black w500 fieldname\" contenteditable=\"true\">Email Address</div>\n<div class=\"mdem10 smem10 xsem10\"><input type=\"text\" name=\"email\" class=\"fieldinput\" placeholder=\"Email\"></div>\n</div>\n<div class=\"mdem13 smem11 xsem11 mt5 xsmt5 w600\"><a href=\"#\" class=\"form-btn7\" contenteditable=\"true\"  id=\"rect\">Join Now</a></div>\n</div>\n</div>\n</div>				\n</div>\n</div>', 1, 1470394007, '', 'active'),
(8, 'Template 8', '<div class=\"form8-bg\" id=\"demo-preview\">\n<div class=\"row\"> \n<div class=\"col-md-12 col-sm-12 col-xs-12\">\n<div class=\"col-md-12 col-sm-12 col-xs-12 form_area8 whitetext\">\n<div class=\"mdem12 smem11 xsem10 text-center\" contenteditable=\"true\">Want to recieve the best offers of the weak ? </div>\n<div class=\"mdem22 smem15 xsem14 text-center w700 xsmb5\" contenteditable=\"true\">Join Today </div>\n<div class=\"col-md-10 col-md-offset-1 col-sm-10 col-sm-offset-1 col-xs-10 col-xs-offset-1 mt10 xsmt10\">\n<div class=\"namefield\">\n<div class=\"mdem10 smem9 xsem9\"><input type=\"text\" placeholder=\"Full Name\"  name=\"name\" class=\"fieldinput\"></div>\n</div>\n<div class=\"phonefield\">\n<div class=\"mdem10 smem9 xsem9\"><input type=\"text\"  placeholder=\"Phone Number\"  name=\"phone\" class=\"fieldinput\"></div>\n</div>\n<div class=\"copydiv\">\n<div class=\"mdem10 smem9 xsem9\"><input type=\"text\" placeholder=\"Email Address\"  name=\"email\" class=\"fieldinput\"></div>\n</div>\n<div class=\"mdem14 smem13 xsem13 mt10 xsmt10 w500\"><a href=\"#\" class=\"form-btn8\" contenteditable=\"true\"  id=\"rect\">Join Now</a></div>\n</div>\n</div>				\n</div>\n</div>\n</div>', 1, 1470394007, '', 'active'),
(9, 'Template 9', '<div class=\"form9-bg\" id=\"demo-preview\">\n<div class=\"row\"> \n<div class=\"col-md-12 col-sm-12 col-xs-12\">\n<div class=\"col-md-12 col-sm-12 col-xs-12 form_area9\">\n<div class=\"ribbon\"><span  contenteditable=\"true\">&nbsp;Hurry UP!&nbsp;</span></div>\n<div class=\"mdem25 smem15 xsem12 text-center w600 mt16 xsmt15 whitetext\" contenteditable=\"true\">Start Your Fitness</div>\n<div class=\"col-md-10 col-md-offset-1 col-sm-10 col-sm-offset-1 col-xs-10 col-xs-offset-1 mt7 xsmt5\">\n<div class=\"namefield\">\n<div class=\"mdem11 smem11 xsem10 w300 whitetext\" contenteditable=\"true\">Full Name</div>\n<div class=\"mdem11 smem11 xsem10\"><input type=\"text\" name=\"name\" class=\"fieldinput\" placeholder=\"Name\"></div>\n</div>\n<div class=\"copydiv\">\n<div class=\"mdem11 smem11 xsem10 w300 whitetext fieldname\" contenteditable=\"true\">Email Address</div>\n<div class=\"mdem12 smem11 xsem10\"><input type=\"text\" name=\"email\" class=\"fieldinput\" placeholder=\"Email\"></div>\n</div>\n<div class=\"mdem12 smem12 xsem11 mt5 xsmt5 w400\"><a href=\"#\" class=\"form-btn9\" contenteditable=\"true\"  id=\"rect\">Sign Up</a></div>\n</div>\n</div>\n</div>				\n</div>\n</div>', 1, 1470394007, '', 'active'),
(10, 'Template 10', '<div class=\"form10-bg\"  id=\"demo-preview\">\n<div class=\"row\"> \n<div class=\"col-md-12 col-sm-12 col-xs-12 segoe\">\n<div class=\"col-md-12 col-sm-12 col-xs-12 form_area10 whitetext\">\n<div class=\"mdem15 smem13 xsem12 w600 text-center\" contenteditable=\"true\">ONLY TODAY !</div>\n<div class=\"mdem42 smem35 xsem30 yellowtext text-center w700\" contenteditable=\"true\">50% OFF</div>\n<div class=\"mdem11 smem10 xsem9 text-center\" contenteditable=\"true\">Strength is the product of struggle, You <br> must do what others don\'t to acheive <br> what others won\'t</div>\n<div class=\"mdem10 smem9 xsem9 text-center mt5 xsmt8\" contenteditable=\"true\">If so, enter your detail below</div>\n<div class=\"col-md-10 col-md-offset-1 col-sm-10 col-sm-offset-1 col-xs-10 col-xs-offset-1 mt2 xsmt4\">\n<div class=\"namefield\">\n<div class=\"mdem9 smem9 xsem9\"><input type=\"text\" placeholder=\"Name\" name=\"name\" class=\"fieldinput\"></div>\n</div>\n<div class=\"copydiv\">\n<div class=\"mdem9 smem9 xsem9\"><input type=\"text\" placeholder=\"Email\" name=\"email\" class=\"fieldinput\"></div>\n</div>\n<div class=\"mdem11 smem11 xsem11 w500\"><a href=\"#\" class=\"form-btn10\" contenteditable=\"true\"  id=\"rect\">Hurry! Register Today</a></div>\n<div class=\"mdem8 smem8 xsem8 text-center mt4 xsmt5\" contenteditable=\"true\"> <img src=\"./assets/default/images/form-images/icon.png\">&nbsp;&nbsp;Your information will never be shared.</div>\n\n</div>\n</div>\n</div>				\n</div>\n</div>', 1, 1470394007, '', 'active');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_from_email`
--

DROP TABLE IF EXISTS `tbl_from_email`;
CREATE TABLE `tbl_from_email` (
  `id` int(11) NOT NULL,
  `member_id` int(11) NOT NULL,
  `email` varchar(255) NOT NULL,
  `status` enum('varify','unvarify') NOT NULL DEFAULT 'unvarify',
  `verify_code` varchar(255) DEFAULT NULL,
  `add_time` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_from_email`
--

INSERT INTO `tbl_from_email` (`id`, `member_id`, `email`, `status`, `verify_code`, `add_time`) VALUES
(1, 1, 'jack@jackallenhomes.net', 'varify', '', 1512844666),
(2, 1, 'jack.allen0170@gmail.com', 'varify', '', 1512844697);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_member`
--

DROP TABLE IF EXISTS `tbl_member`;
CREATE TABLE `tbl_member` (
  `mem_id` bigint(20) NOT NULL,
  `email` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `city` text,
  `state` varchar(250) DEFAULT NULL,
  `country` varchar(250) DEFAULT NULL,
  `address` text,
  `pin_code` varchar(250) DEFAULT NULL,
  `business_name` varchar(255) DEFAULT NULL,
  `profile_pic` varchar(200) DEFAULT NULL,
  `forget_pass_code` varchar(255) DEFAULT NULL,
  `forget_pass_exptime` int(11) DEFAULT NULL,
  `status` enum('active','inactive') NOT NULL DEFAULT 'active',
  `add_time` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_member`
--

INSERT INTO `tbl_member` (`mem_id`, `email`, `password`, `name`, `city`, `state`, `country`, `address`, `pin_code`, `business_name`, `profile_pic`, `forget_pass_code`, `forget_pass_exptime`, `status`, `add_time`) VALUES
(1, 'mkaliyur@gmail.com', '5cbcf07e36fe37142b407ace0211cbf7', 'manjunathkg', '', '', '', '', '', 'Access3c', NULL, '0', 0, 'active', 1495218268);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_newsletter`
--

DROP TABLE IF EXISTS `tbl_newsletter`;
CREATE TABLE `tbl_newsletter` (
  `id` int(11) NOT NULL,
  `member_id` int(11) NOT NULL,
  `message_type` enum('schedule','instant') DEFAULT NULL,
  `newsletter_type` enum('newsletter','autoresponder') NOT NULL DEFAULT 'newsletter',
  `from_name` varchar(255) DEFAULT NULL,
  `from_email` varchar(255) DEFAULT NULL,
  `reply_to` varchar(255) DEFAULT NULL,
  `message_name` varchar(255) NOT NULL,
  `subject` varchar(255) NOT NULL,
  `template_id` int(11) DEFAULT NULL,
  `template_text` longtext CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `in_campaign_id` text NOT NULL,
  `ex_campaign_id` text,
  `sep_list_id` text,
  `contact_id` longtext,
  `image` varchar(255) DEFAULT NULL,
  `schedule_date` datetime DEFAULT NULL,
  `schedule_timestamp` int(11) DEFAULT NULL,
  `status` enum('sent','scheduled','process','draft') DEFAULT NULL,
  `process_stage` enum('complete','inprocess') NOT NULL DEFAULT 'inprocess',
  `last_procees_id` int(11) DEFAULT NULL,
  `add_time` int(11) NOT NULL,
  `sent_time` int(11) DEFAULT NULL,
  `ip` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_statistics`
--

DROP TABLE IF EXISTS `tbl_statistics`;
CREATE TABLE `tbl_statistics` (
  `id` int(11) NOT NULL,
  `type` enum('newsletter','autoresponder') NOT NULL DEFAULT 'newsletter',
  `newsletter_id` int(11) NOT NULL,
  `contact_id` int(11) NOT NULL,
  `email` varchar(255) NOT NULL,
  `open` enum('yes','no') NOT NULL DEFAULT 'no',
  `click` enum('yes','no') NOT NULL DEFAULT 'no',
  `unsubscribed` enum('yes','no') NOT NULL DEFAULT 'no',
  `bounced` enum('yes','no') NOT NULL DEFAULT 'no',
  `complaints` enum('yes','no') NOT NULL DEFAULT 'no',
  `add_time` int(11) NOT NULL,
  `ip` varchar(255) DEFAULT NULL,
  `browser` varchar(255) DEFAULT NULL,
  `device` enum('mobile','desktop') DEFAULT NULL,
  `country_code` varchar(255) DEFAULT NULL,
  `open_time` int(11) DEFAULT NULL,
  `click_time` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_suppression`
--

DROP TABLE IF EXISTS `tbl_suppression`;
CREATE TABLE `tbl_suppression` (
  `id` int(11) NOT NULL,
  `member_id` int(11) NOT NULL,
  `email` varchar(255) NOT NULL,
  `list_id` int(11) NOT NULL,
  `status` enum('active','inactive') NOT NULL DEFAULT 'active',
  `ip` varchar(255) NOT NULL,
  `add_time` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_suppression_list`
--

DROP TABLE IF EXISTS `tbl_suppression_list`;
CREATE TABLE `tbl_suppression_list` (
  `id` bigint(20) NOT NULL,
  `list_name` varchar(255) NOT NULL,
  `member_id` bigint(20) NOT NULL,
  `add_time` int(11) NOT NULL,
  `ip` varchar(255) NOT NULL,
  `status` enum('active','inactive') NOT NULL DEFAULT 'active'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_template`
--

DROP TABLE IF EXISTS `tbl_template`;
CREATE TABLE `tbl_template` (
  `id` int(11) NOT NULL,
  `template_type` enum('default','draft') NOT NULL,
  `editor_type` enum('inline','plain') NOT NULL DEFAULT 'inline',
  `template_name` varchar(255) NOT NULL,
  `template_text` longtext CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `template_screenshot` varchar(255) DEFAULT NULL,
  `member_id` bigint(20) NOT NULL,
  `add_time` int(11) NOT NULL,
  `ip` varchar(255) NOT NULL,
  `status` enum('active','inactive') NOT NULL DEFAULT 'active'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_template`
--

INSERT INTO `tbl_template` (`id`, `template_type`, `editor_type`, `template_name`, `template_text`, `template_screenshot`, `member_id`, `add_time`, `ip`, `status`) VALUES
(1, 'default', 'inline', 'Template 1', '<div style=\"width:100%; max-width:600px; margin: auto;\"><table style=\"border-collapse:collapse;height:100%;margin:0;padding:0;background-color:#e9e9e9; width:100%;max-width:600px; margin:0 auto;\" width=\"100%\" cellspacing=\"0\" cellpadding=\"0\" border=\"0\" align=\"center\" height=\"100%\">\n<tbody>\n<tr>\n<td style=\"height:100%;margin:0;padding:0;width:100%;border-top:0\" valign=\"top\" align=\"center\">\n\n<table style=\"border-collapse:collapse;width:100%;max-width:600px;\" width=\"100%\" cellspacing=\"0\" cellpadding=\"0\" border=\"0\">\n<tbody>\n<tr><td valign=\"top\" align=\"center\">\n\n<table style=\"border-collapse:collapse; background:#3a617e;width:100%;max-width:600px;\" width=\"600\" cellspacing=\"0\" cellpadding=\"0\" border=\"0\">\n<tbody>\n<tr>\n<td style=\"padding:9px;text-align:center\" valign=\"top\">\n<img alt=\"\" src=\"{image_path}images/logo.png\" style=\"box-sizing: border-box;max-width:163px;padding-bottom:0;display:inline!important;vertical-align:bottom;border:0;height:auto;outline:none;text-decoration:none\" width=\"163\" align=\"middle\"></td></tr></tbody></table>\n                                    \n                                </td>\n                            </tr>\n                            <tr>\n                                <td valign=\"top\" align=\"center\">\n                                    \n<table style=\"border-collapse:collapse;border-top:0;border-bottom:0;width:100%;max-width:600px;\" width=\"100%\" cellspacing=\"0\" cellpadding=\"0\" border=\"0\">\n<tbody><tr><td valign=\"top\" align=\"center\">\n                                                \n<table style=\"border-collapse:collapse;width:100%;max-width:600px;\" width=\"600\" cellspacing=\"0\" cellpadding=\"0\" border=\"0\">\n<tbody><tr><td valign=\"top\" align=\"center\">\n\n<table style=\"border-collapse:collapse;background-color:#ffffff;border:0px solid #d5d5d5;width:100%;max-width:600px;\" width=\"100%\" cellspacing=\"0\" cellpadding=\"0\" border=\"0\">\n<tbody><tr><td valign=\"top\">\n\n\n<table style=\"min-width:100%;border-collapse:collapse;width:100%;max-width:600px;\" width=\"100%\" cellspacing=\"0\" cellpadding=\"0\" border=\"0\" align=\"left\">\n<tbody><tr><td valign=\"top\">\n<img alt=\"\" src=\"{image_path}images/banner.png\" style=\"box-sizing: border-box;max-width:100%;padding-bottom:0;display:inline!important;vertical-align:bottom;border:0;height:auto;outline:none;text-decoration:none\" tabindex=\"0\" width=\"600\" align=\"middle\"></td></tr>\n</tbody></table>\n\n\n<table style=\"max-width:100%;min-width:100%;border-collapse:collapse;  clear:both;width:100%;max-width:600px;\" width=\"100%\" cellspacing=\"0\" cellpadding=\"0\" border=\"0\" align=\"left\"><tbody><tr><td style=\"padding-top:0; padding-right:18px; padding-bottom:9px; padding-left:18px; word-break:break-word; color:#606060; font-size:15px; text-align:left;\" valign=\"top\">\n<div class=\"editblock\" contenteditable=\"true\">\n<div style=\"display:block; margin:15px 0 0 0; padding:0; font-size:40px; font-style:normal; line-height:100%; color:#d95433;\" align=\"center\"><b>Apply Online</b></div>\n</div>\n<div class=\"editblock\" contenteditable=\"true\">\n<p style=\"display:block; margin:15px 0 0 0; padding:0; font-size:20px; font-style:normal; line-height:100%; text-align:center; color:#d95433;\">for your Loan and get faster approval!</p>\n</div>\n<div class=\"editblock\" contenteditable=\"true\">\n<p style=\"display:block; margin:5px 0 0 0; font-size:15px; text-align:center; color:#353535!important;\">Explore Loan offers from leading Banks now. Rates from 11.99% available.</p>\n</div>\n<hr style=\"background:#d6d6d6; height:1px; border:0; margin:15px 0 5px 0;\"></td></tr></tbody>\n</table>\n\n\n\n	<table style=\"width:100%;max-width:600px;min-width:100%;border-collapse:collapse; clear:both;\" width=\"100%\" cellspacing=\"0\" cellpadding=\"15\" border=\"0\" align=\"left\">\n    <tbody>\n	\n	<tr>\n	<td width=\"40%\"><img alt=\"\" src=\"{image_path}images/education-loan.png\" style=\"box-sizing: border-box;max-width:100%;padding-bottom:0;display:inline!important;vertical-align:bottom;border:1px solid #d6d6d6;height:auto;outline:none;text-decoration:none\" tabindex=\"0\" width=\"200\" align=\"middle\"></td>\n    <td style=\"word-break:break-word;color:#4a4949;text-align:left\" valign=\"top\">\n	<div class=\"editblock\" contenteditable=\"true\">\n	<p style=\"display:block; margin:0; padding:0; line-height:100%; font-family:Helvetica; font-size:20px; font-style:normal; line-height:100%;\">\n	<b>Education Loan</b>\n	</p>\n	</div>\n	<div class=\"editblock\" contenteditable=\"true\">\n	<p style=\"display:block; margin-top:15px; margin-right:0px; margin-bottom:0px; margin-left:0px; padding:0; font-family:Helvetica; font-size:14px; font-style:normal; line-height:150%;\">\nLorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.\n	</p>\n	</div>\n	\n	<div class=\"editblock\" contenteditable=\"true\">\n	<a href=\"#\" style=\"font-family:Helvetica; border-radius:03px; background-color:#d95433; padding:9px 18px; text-align:center; text-decoration:none; color:#ffffff; display:inline-block; margin-top:6px;\" class=\"sm-button\">Apply Now</a>\n	</div>\n	\n	<!--<a style=\"font-weight:bold; font-family:Helvetica; letter-spacing:normal; border-radius:3px; background-color:#d95433; padding:12px; line-height:100%; text-align:center; text-decoration:none; color:#ffffff; display:inline-block; margin-top:6px;\"><div contenteditable=\"true\">Apply Now</div></a>-->\n    </td>\n    </tr>\n	\n	\n	<tr>\n	<td><img alt=\"\" src=\"{image_path}images/home-loan.png\" style=\"box-sizing: border-box;max-width:100%;padding-bottom:0;display:inline!important;vertical-align:bottom;border:1px solid #d6d6d6;height:auto;outline:none;text-decoration:none\" tabindex=\"0\" width=\"200\" align=\"middle\"></td>\n    <td style=\"word-break:break-word;color:#4a4949;text-align:left\" valign=\"top\">\n	<div class=\"editblock\" contenteditable=\"true\"><p style=\"display:block; margin:0; padding:0; line-height:100%; font-family:Helvetica; font-size:20px; font-style:normal; line-height:100%;\"><b>Home Loan</b></p></div>\n	<div class=\"editblock\" contenteditable=\"true\">\n	<p style=\"display:block; margin:15px 0 0 0; padding:0; font-family:Helvetica; font-size:14px; font-style:normal; line-height:150%;\">Lorem ipsum <span>dolor</span> sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.\n	</p>\n	\n	</div>\n	\n	<div class=\"editblock\" contenteditable=\"true\">\n	<a href=\"#\" style=\"font-family:Helvetica; border-radius:03px; background-color:#d95433; padding:9px 18px; text-align:center; text-decoration:none; color:#ffffff; display:inline-block; margin-top:6px;\" class=\"sm-button\">Apply Now</a></div>\n	\n    </td>\n    </tr>\n	\n	\n	<tr style=\"border-bottom:15px solid #3a617e;\">\n	<td><img alt=\"\" src=\"{image_path}images/car-loan.png\" style=\"box-sizing: border-box;max-width:100%;padding-bottom:0;display:inline!important;vertical-align:bottom;border:1px solid #d6d6d6;height:auto;outline:none;text-decoration:none\" tabindex=\"0\" width=\"200\" align=\"middle\"></td>\n    <td style=\"word-break:break-word;color:#4a4949;text-align:left\" valign=\"top\">\n	<div class=\"editblock\" contenteditable=\"true\"><p style=\"display:block; margin:0; padding:0; line-height:100%; font-family:Helvetica; font-size:20px; font-style:normal; line-height:100%;\"><b>Loan Loan</b></p></div>\n	<div class=\"editblock\" contenteditable=\"true\"><p style=\"display:block; margin:15px 0 0 0; padding:0; font-family:Helvetica; font-size:14px; font-style:normal; line-height:150%;\">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p></div>\n	\n	<div class=\"editblock\" contenteditable=\"true\">\n	<a href=\"#\" style=\"font-family:Helvetica; border-radius:03px; background-color:#d95433; padding:9px 18px; text-align:center; text-decoration:none; color:#ffffff; display:inline-block; margin-top:6px;\" class=\"sm-button\">Apply Now</a></div>\n    \n    \n    \n    </td>\n    </tr>\n	\n	\n	\n	\n	\n	\n</tbody>\n</table>				\n\n				\n\n\n</td>\n                                                    </tr>\n\n                                                </tbody></table>\n                                            </td>\n                                        </tr>\n                                    </tbody></table>\n                                    \n                                </td>\n                            </tr>\n                           \n                        </tbody></table>\n                        \n                    </td>\n</tr>\n\n</tbody>\n</table>\n			\n	</td>\n    </tr>\n    </tbody>\n    </table>	\n    </div>', 'template1.jpg', 1, 1470394007, '', 'active'),
(2, 'default', 'inline', 'Template 2', '<div style=\"width:100%; max-width:600px; margin: auto;\"><table style=\"border-collapse:collapse;height:100%;margin:0 auto;padding:0;width:100%;background-color:#e9e9e9;max-width:600px;\" width=\"100%\" cellspacing=\"0\" cellpadding=\"0\" border=\"0\" align=\"center\" height=\"100%\">\n<tbody>\n<tr>\n<td style=\"height:100%;margin:0;padding:0;width:100%;border-top:0\" valign=\"top\" align=\"center\">\n\n<table style=\"border-collapse:collapse;max-width:600px;width:100%;\" width=\"100%\" cellspacing=\"0\" cellpadding=\"0\" border=\"0\">\n<tbody>\n<tr><td valign=\"top\" align=\"center\">\n\n<table style=\"border-collapse:collapse; background:#41aa9a;max-width:600px;width:100%;\" width=\"100%\" cellspacing=\"0\" cellpadding=\"0\" border=\"0\">\n<tbody>\n<tr>\n<td style=\"padding:18px;text-align:center\" valign=\"top\">\n<img alt=\"\" src=\"{image_path}images/logo2.png\" style=\"box-sizing: border-box;height:70px;max-height:70px;padding-bottom:0;display:inline!important;vertical-align:bottom;border:0;width:auto;outline:none;text-decoration:none\" align=\"middle\"></td></tr></tbody></table>\n                                    \n                                </td>\n                            </tr>\n                            <tr>\n                                <td valign=\"top\" align=\"center\">\n                                    \n<table style=\"border-collapse:collapse;border-top:0;border-bottom:0;max-width:600px;width:100%;\" width=\"100%\" cellspacing=\"0\" cellpadding=\"0\" border=\"0\">\n<tbody><tr><td valign=\"top\" align=\"center\">\n                                                \n<table style=\"border-collapse:collapse;max-width:600px;width:100%;\" width=\"100%\" cellspacing=\"0\" cellpadding=\"0\" border=\"0\">\n<tbody><tr><td valign=\"top\" align=\"center\">\n\n<table style=\"border-collapse:collapse;background-color:#41aa9a;border:1px solid #41aa9a;max-width:600px;width:100%;\" width=\"100%\" cellspacing=\"0\" cellpadding=\"0\" border=\"0\">\n<tbody><tr><td valign=\"top\">\n<table style=\"border-collapse:collapse;background-color:#fff;border:1px solid #fff;margin:0 auto 25px auto;max-width:550px;width:100%;\" width=\"92%\" cellspacing=\"0\" cellpadding=\"0\" border=\"0\">\n<tbody><tr><td valign=\"top\">\n\n\n<table style=\"max-width:600px;width:100%;min-width:100%;border-collapse:collapse;  clear:both;\" width=\"100%;\" cellspacing=\"0\" cellpadding=\"0\" border=\"0\" align=\"left\"><tbody><tr><td style=\"color:#4a4949; font-size:15px; text-align:left;\" valign=\"top\">\n<div  contenteditable=\"true\">\n<div style=\"display:block; margin:5px 15px 0 0; padding:0; font-size:13px; text-align:right;line-height:100%; color:#dadada;\" align=\"center;\"><b>*T &amp; C Apply</b></div></div>\n<div contenteditable=\"true\">\n<div style=\"display:block; margin:15px 0 0 0; padding:0; font-size:58px; text-align:center;line-height:100%; color:#4a4949;font-weight:100;\" align=\"center;\">Summer</div>\n</div>\n\n<div contenteditable=\"true\">\n<div style=\"display:block; margin:15px 0 0 0; padding:0; font-size:100px; font-style:normal; line-height:100%; color:#4a4949;\" align=\"center\"><b>SALE</b></div>\n</div>\n\n<div contenteditable=\"true\">\n<div style=\"display:block; margin:15px 0 15px 0;font-size:45px; padding: 0% 15%; font-style:normal; line-height:100%; color:#fff;\" align=\"center\"><div style=\"background-color:#41aa9a;padding: 2.5% 0%;\">40% OFF</div></div>\n</div>\n\n<div contenteditable=\"true\">\n<div style=\"display:block; margin:10px 0 10px 0;font-size:23px; padding: 0; font-style:normal; line-height:100%; color:#4a4949;font-weight:400;\" align=\"center\">ALL PURCHASES OVER $ 100</div>\n</div>\n\n<div contenteditable=\"true\">\n<div style=\"display:block; margin:20px 0 20px 0;font-size:50px; padding: 0; font-style:normal; line-height:100%; color:#41aa9a;\" align=\"center\">LAST WEEK!</div>\n</div>\n\n<div contenteditable=\"true\">\n<div style=\"display:block; margin:20px 0 20px 0;font-size:15px; padding: 0; font-style:normal; line-height:150%; color:#4a4949;padding:0 5%;\" align=\"center\">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</div>\n</div>\n\n<div style=\"text-align:center;\" contenteditable=\"true\">\n<a href=\"#\" style=\"border-radius:03px; background-color:#3d455c; padding: 6px 30px; text-align:center; text-decoration:none; color:#ffffff; display:inline-block;font-size:30px;margin:15px 0 15px 0;\" class=\"sm-button\">Shop Now</a></div>\n\n</td>\n</tr>\n</tbody>\n</table>\n</td>\n</tr>\n</tbody></table>\n</td>\n</tr>\n</tbody></table>\n\n\n\n </td>\n</tr>\n</tbody></table>\n</td>\n</tr>\n\n\n</tbody></table>\n</td>\n</tr>\n</tbody>\n</table>\n</td></tr>\n</tbody>\n</table>\n</div>', 'template3.jpg', 1, 1470394007, '', 'active'),
(3, 'default', 'inline', 'Template 3', '<div style=\"width:100%; max-width:600px; margin: auto;\"><table style=\"border-collapse:collapse;height:100%;margin:0;padding:0;width:100%;background-color:#e9e9e9; max-width:600px; margin:0 auto;\" width=\"100%\" cellspacing=\"0\" cellpadding=\"0\" border=\"0\" align=\"center\" height=\"100%\">\n<tbody>\n<tr>\n<td style=\"height:100%;margin:0;padding:0;width:100%;border-top:0\" valign=\"top\" align=\"center\">\n\n<table style=\"border-collapse:collapse;width:100%;max-width:600px;\" width=\"100%\" cellspacing=\"0\" cellpadding=\"0\" border=\"0\">\n<tbody>\n<tr>\n<td valign=\"top\" align=\"center\">\n\n<table style=\"border-collapse:collapse; background:#405292;width:100%;max-width:600px;\" width=\"600\" cellspacing=\"0\" cellpadding=\"0\" border=\"0\">\n<tbody>\n<tr>\n<td style=\"padding:9px;text-align:center;word-break:break-word; color:#606060; font-size:15px;\" valign=\"top\">\n\n<div contenteditable=\"true\">\n<div style=\"display:block; margin:15px 0; padding:0; font-size:28px; font-style:normal; line-height:100%; color:#fff;\" align=\"center\"><b>Providing Total Health Care Solution</b></div>\n</div>\n\n</td>\n</tr>\n</tbody>\n</table>\n                                    \n</td>\n</tr>\n\n<tr>\n<td valign=\"top\" align=\"center\">\n                                    \n<table style=\"border-collapse:collapse;border-top:0;border-bottom:0;background-color:#ffffff;width:100%;max-width:600px;\" width=\"100%\" cellspacing=\"0\" cellpadding=\"0\" border=\"0\">\n<tbody><tr><td valign=\"top\" align=\"center\">\n                                                \n\n\n\n<table style=\"min-width:100%;border-collapse:collapse;width:100%;max-width:600px;\" width=\"100%\" cellspacing=\"0\" cellpadding=\"0\" border=\"0\" align=\"left\">\n<tbody><tr><td valign=\"top\">\n<img alt=\"\" src=\"{image_path}images/temp3.png\" style=\"box-sizing: border-box;max-height:310px;max-width:100%;padding-bottom:0;display:inline!important;vertical-align:bottom;border:0;height:auto;outline:none;text-decoration:none\" tabindex=\"0\" width=\"600\" align=\"middle\"></td></tr>\n</tbody></table>\n\n\n<table style=\"max-width:100%;min-width:100%;border-collapse:collapse;  clear:both;width:100%;max-width:600px;\" width=\"100%;\" cellspacing=\"0\" cellpadding=\"0\" border=\"0\" align=\"left\"><tbody><tr><td style=\"padding-top:0; padding-right:18px; padding-bottom:9px; padding-left:18px; word-break:break-word; color:#606060; font-size:15px; text-align:left;\" valign=\"top\">\n\n<div contenteditable=\"true\">\n<p style=\"display:block; margin:20px 0 10px 0; padding:0; font-size:25px; font-weight:500;  text-align:center; color:#4a4949; line-height:30px;\">Lorem ipsum dolor sit amet, no quodsi definitiones vis. Verterem postulant intellegat </p>\n</div>\n\n<hr style=\"background:#d6d6d6; height:1px; border:0; margin:15px 0 15px 0;\">\n\n\n<div contenteditable=\"true\">\n<p style=\"display:block; margin:15px 0 10px 0; padding:0; font-size:25px; font-weight:400; text-align:center; color:#000000; line-height:30px;\">Description</p>\n</div>\n\n\n<div contenteditable=\"true\">\n<p style=\"display:block; margin:2px 0 10px 0; padding:10px 25px ; font-size:15px; font-weight:400; text-align:center; color:#818080; line-height:25px;\">Lorem ipsum dolor sit amet, no quodsi definitiones vis. Verterem postulant intellegat est et, an partiendo argumentum elaboraret qui. Diam salutatus cu est, choro sententiae nam id. An sint meliore conceptam qui, eum dicit corpora consequuntur no. Aperiam repudiandae pri an. Vidisse torquatos mea ne, debet ludus duo ne, in quodsi fuisset theophrastus qui.\n</p>\n</div>\n\n<div style=\"text-align:center;\" contenteditable=\"true\">\n<a href=\"#\" style=\"font-family:Roboto; border-radius:03px; background-color:#ff4747; padding:2% 20%; text-align:center; text-decoration:none; color:#ffffff; display:inline-block; margin:10px 0 15px 0;font-size:20px;\" class=\"sm-button\">Get Appointment</a>\n</div>\n\n\n</td>\n</tr>\n</tbody>\n</table>\n\n\n\n\n</td>\n</tr>\n</tbody></table>\n</td>\n</tr>\n</tbody>\n</table>\n</td>\n</tr>\n</tbody>\n</table>\n</div>', 'template9.jpg', 1, 1470394007, '', 'active'),
(4, 'default', 'inline', 'Template 4', '<div style=\"width:100%; max-width:600px; margin: auto;\"><table style=\"border-collapse:collapse;height:100%;margin:0;padding:0;width:100%;background-color:#e9e9e9; max-width:600px; margin:0 auto;\" width=\"100%\" cellspacing=\"0\" cellpadding=\"0\" border=\"0\" align=\"center\" height=\"100%\">\n<tbody>\n<tr>\n<td style=\"height:100%;margin:0;padding:0;width:100%;border-top:0\" valign=\"top\" align=\"center\">\n\n<table style=\"border-collapse:collapse;width:100%;max-width:600px;\" width=\"100%\" cellspacing=\"0\" cellpadding=\"0\" border=\"0\">\n<tbody>\n<tr>\n<td valign=\"top\" align=\"center\">\n<table style=\"border-collapse:collapse;background:#fff;max-width:600px;width:100%;\" width=\"600\" cellspacing=\"0\" cellpadding=\"0\" border=\"0\">\n<tbody>\n<tr>\n<td style=\"text-align:center;word-break:break-word;padding:0 9px;\" valign=\"top\">\n\n<div contenteditable=\"true\">\n<div style=\"display:block; margin:30px 0 10px 0; padding:0; font-size:26px; line-height:40px; color:#65a3ca;\" align=\"center\">This Area Is Home Sweet Home For A Sweet,Catchy Headline</div>\n</div>\n\n<div contenteditable=\"true\">\n<div style=\"display:block; margin:8px 0 35px 0; padding:0; font-size:16px; font-style:normal; line-height:40px; color:#4a4949;\" align=\"center\">Maybe A Little Description Section Too, In Case There’s More To Say!</div>\n</div>\n\n</td>\n</tr>\n</tbody>\n</table>                                  \n</td>\n</tr>\n\n<tr>\n<td valign=\"top\" align=\"center\">\n                                    \n<table style=\"border-collapse:collapse;border-top:0;border-bottom:0;background-color:#ffffff;width:100%;max-width:600px;\" width=\"100%\" cellspacing=\"0\" cellpadding=\"0\" border=\"0\">\n<tbody><tr><td valign=\"top\" align=\"center\">\n                                                \n\n\n\n<table style=\"min-width:100%;border-collapse:collapse;max-width:600px;width:100%;\" width=\"100%\" cellspacing=\"0\" cellpadding=\"0\" border=\"0\" align=\"left\">\n<tbody><tr><td valign=\"top\">\n<img alt=\"\" src=\"{image_path}images/temp4.png\" style=\"box-sizing: border-box;max-height:390px;max-width:100%;padding-bottom:0;display:inline!important;vertical-align:bottom;border:0;height:auto;outline:none;text-decoration:none\" tabindex=\"0\" width=\"600\" align=\"middle\"></td></tr>\n</tbody></table>\n\n\n<table style=\"min-width:100%;border-collapse:collapse;  clear:both;max-width:600px;width:100%;\" width=\"100%;\" cellspacing=\"0\" cellpadding=\"0\" border=\"0\" align=\"left\"><tbody><tr><td style=\"padding-top:0; padding-right:18px; padding-bottom:9px; padding-left:18px; word-break:break-word; color:#606060; font-size:15px; text-align:left;\" valign=\"top\">\n\n<div style=\"text-align:center;margin:15px 0 15px 0;padding:2% 0;\" contenteditable=\"true\">\n<a href=\"#\" style=\"font-family:Roboto; border-radius:03px; background-color:#65a3ca; padding:2% 13%; text-align:center; text-decoration:none; color:#ffffff; display:inline-block;font-size:22px;\" class=\"sm-button\">Call To Action</a>\n</div>\n\n<div contenteditable=\"true\">\n<p style=\"display:block; margin:15px 0 10px 0; padding:0; font-size:25px; font-weight:400; text-align:center; color:#606060; line-height:30px;\">Company Name</p>\n</div>\n\n<div contenteditable=\"true\">\n<p style=\"display:block; margin:2px 0 10px 0; padding:10px 30px ; font-size:15px; font-weight:400; text-align:center; color:#818080; line-height:25px;\">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.\n</p>\n</div>\n\n</td>\n</tr>\n</tbody>\n</table>\n\n</td>\n</tr>\n</tbody></table>\n</td>\n</tr>\n</tbody>\n</table>\n</td>\n</tr>\n</tbody>\n</table>\n</div>', 'template7.jpg', 1, 1470394007, '', 'active'),
(5, 'default', 'inline', 'Template 5', '<div style=\"width:100%; max-width:600px; margin: auto;\"><table style=\"border-collapse:collapse;height:100%;margin:0;padding:0;width:100%;background-color:#fff; max-width:600px; margin:0 auto;border-left:20px solid #75caed;\" width=\"100%\" cellspacing=\"0\" cellpadding=\"0\" border=\"0\" align=\"center\" height=\"100%\">\n<tbody>\n<tr>\n<td style=\"height:100%;margin:0;padding:0;width:100%;border-top:0\" valign=\"top\" align=\"center\">\n\n\n                                                \n\n\n\n\n<table style=\"max-width:100%;min-width:100%;border-collapse:collapse;  clear:both;width:100%;max-width:600px;\" width=\"100%;\" cellspacing=\"0\" cellpadding=\"0\" border=\"0\" align=\"left\"><tbody><tr><td style=\"padding:20px 30px; word-break:break-word; color:#606060; font-size:15px; text-align:left;\" valign=\"top\">\n\n\n\n\n<div contenteditable=\"true\">\n<div style=\"display:block; margin:15px 0; padding:0; font-size:28px; font-style:normal; line-height:100%; color:#75caed;\" align=\"left\"><b>Converting Your Dreams Into Reality Is Our Passion</b></div>\n</div>\n\n\n\n<hr style=\"background:#75caed; height:1px; border:0; margin:15px 0 15px 0;\">\n\n\n<div contenteditable=\"true\">\n<p style=\"display:block; margin:10px 0 10px 0; padding:0; font-size:18px; font-weight:500;  text-align:left; color:#4a4949; line-height:30px;\">Dear Customers,</p>\n</div>\n\n\n\n\n<div contenteditable=\"true\">\n<p style=\"display:block; margin:2px 0 5px 0; padding:10px 0px ; font-size:14px; font-weight:400; text-align:left; color:#000; line-height:25px;\">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Verum tamen cum de rebus grandioribus dicas, ipsae res verba rapiunt; Luxuriam non reprehendit, modo sit vacua infinita cupiditate et timore. Apud imperitos tum illa dicta sunt, aliquid etiam coronae datum; Quod non faceret, si in voluptate summum bonum poneret. Quid ei reliquisti, nisi te, quoquo modo loqueretur, intellegere, quid diceret? Ego vero volo in virtute vim esse quam maximam\n</p>\n</div>\n\n<div contenteditable=\"true\">\n<p style=\"display:block; margin:2px 0 5px 0; padding:10px 0px ; font-size:14px; font-weight:400; text-align:left; color:#000; line-height:25px;\">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Verum tamen cum de rebus grandioribus dicas, ipsae res verba rapiunt; Luxuriam non reprehendit, modo sit vacua infinita cupiditate et timore. Apud imperitos tum illa dicta sunt, aliquid etiam coronae datum; Quod non faceret, si in voluptate summum bonum poneret. Quid ei reliquisti, nisi te, quoquo modo loqueretur, intellegere, quid diceret? Ego vero volo in virtute vim esse quam maximam\n</p>\n</div>\n\n<div contenteditable=\"true\">\n<p style=\"display:block; margin:2px 0 5px 0; padding:10px 0px ; font-size:14px; font-weight:400; text-align:left; color:#000; line-height:25px;\">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Verum tamen cum de rebus grandioribus dicas, ipsae res verba rapiunt; Luxuriam non reprehendit, modo sit vacua infinita cupiditate et timore. Apud imperitos tum illa dicta sunt, aliquid etiam coronae datum; Quod non faceret, si in voluptate summum bonum poneret. Quid ei reliquisti, nisi te, quoquo modo loqueretur, intellegere, quid diceret? Ego vero volo in virtute vim esse quam maximam\n</p>\n</div>\n\n\n<div contenteditable=\"true\">\n<p style=\"display:block; margin:2px 0 5px 0; padding:10px 0px ; font-size:14px; font-weight:400; text-align:left; color:#000; line-height:25px;\">Thanks for a great year.\n</p>\n</div>\n\n<div contenteditable=\"true\">\n<p style=\"display:block; margin:2px 0 5px 0; padding:10px 0px ; font-size:14px; font-weight:400; text-align:left; color:#000; line-height:18px;\">\nSincerely<br>\nYour Name\n</p>\n</div>\n\n<div style=\"text-align:center;\" contenteditable=\"true\">\n<a href=\"#\" style=\"font-family:Roboto; border-radius:03px; background-color:#656e72; padding:2% 20%; text-align:center; text-decoration:none; color:#ffffff; display:inline-block; margin:20px 0 15px 0;font-size:20px;\" class=\"sm-button\">Contact Our Business Experts Now</a>\n</div>\n\n\n</td>\n</tr>\n</tbody>\n</table>\n\n\n                                       \n          \n</td>\n</tr>\n\n</tbody>\n</table>\n</div>', 'template15.jpg', 1, 1470394007, '', 'active'),
(6, 'default', 'inline', 'Template 6', '<div style=\"width:100%; max-width:600px; margin: auto;\"><table style=\"border-collapse:collapse;height:100%;margin:0;padding:0;width:100%;background-color:#fff; max-width:600px; margin:0 auto;border:20px solid #d6d6d6;\" width=\"100%\" cellspacing=\"0\" cellpadding=\"0\" border=\"0\" align=\"center\" height=\"100%\">\n<tbody>\n<tr>\n<td style=\"height:100%;margin:0;padding:0;width:100%;border-top:0\" valign=\"top\" align=\"center\">\n\n\n\n<table style=\"max-width:100%;min-width:100%;border-collapse:collapse;  clear:both;width:100%;max-width:600px;\" width=\"100%;\" cellspacing=\"0\" cellpadding=\"0\" border=\"0\"><tbody><tr><td style=\"padding:20px 30px; word-break:break-word; color:#606060; font-size:15px;\" valign=\"top\">\n\n\n<div contenteditable=\"true\">\n<div style=\"display:block; margin:15px 0; padding:0; font-size:40px; font-style:normal; line-height:100%; color:#fb6735;\" align=\"center\"><b>Explore Our Music App!</b></div>\n</div>\n\n<table style=\"min-width:100%;border-collapse:collapse;max-width:600px;width:100%;margin:10% 0; \" width=\"100%\" cellspacing=\"0\" cellpadding=\"0\" border=\"0\" align=\"center\">\n<tbody><tr><td valign=\"top\" align=\"center\">\n<img alt=\"\" src=\"{image_path}images/temp6.png\" style=\"box-sizing: border-box;padding-bottom:0;display:inline!important;vertical-align:bottom;border:0;height:auto;outline:none;text-decoration:none; max-width:500px;\" tabindex=\"0\" align=\"middle\"></td></tr>\n</tbody></table>\n\n\n<div contenteditable=\"true\">\n<div style=\"display:block; margin:9% 0 5% 0; padding:0; font-size:25px; font-style:normal; line-height:100%; color:#fb6735;\" align=\"center\">Our Product Will Amaze You</div>\n</div>\n\n<div contenteditable=\"true\">\n<p style=\"display:block; margin:2px 0 10px 0; padding:10px 30px ; font-size:15px; font-weight:400; text-align:center; color:#818080; line-height:25px;\">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.\n</p>\n</div>\n\n<div style=\"text-align:center;margin:15px 0 15px 0;padding:2% 0;\" contenteditable=\"true\">\n<a href=\"#\" style=\"font-family:Roboto; border-radius:03px; background-color:#232c3b; padding:2% 13%; text-align:center; text-decoration:none; color:#ffffff; display:inline-block;font-size:22px;\" class=\"sm-button\">Explore Now</a>\n</div>\n\n\n</td>\n</tr>\n</tbody>\n</table>\n                        \n          \n</td>\n</tr>\n\n</tbody>\n</table>\n\n</div>', 'template11.jpg', 1, 1470394007, '', 'active'),
(7, 'default', 'inline', 'Template 7', '<div style=\"width:100%; max-width:600px; margin: auto;\">\n\n<table align=\"center\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" height=\"100%\" width=\"100%\" style=\"border-collapse:collapse;height:100%;margin:0;padding:0;width:100%;background-color:#e9e9e9; max-width:600px; margin:0 auto;\">\n<tbody>\n<tr>\n<td align=\"center\" valign=\"top\" style=\"height:100%;margin:0;padding:0;width:100%;border-top:0\">\n\n<table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"100%\" style=\"border-collapse:collapse;width:100%;max-width:600px;\">\n<tbody>\n<tr>\n<td align=\"center\" valign=\"top\">\n<table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"600\" style=\"border-collapse:collapse;background:#ff854f;max-width:600px;width:100%;\">\n<tbody>\n<tr>\n<td valign=\"top\" style=\"text-align:center;word-break:break-word;padding:0 9px;\">\n<div contenteditable=\"true\">\n<div style=\"display:block; margin:15px 0 0 0; padding:0; font-size:125px; font-style:normal; line-height:100%; color:#fff;    letter-spacing: 15px;\" align=\"center\"><b>OUR</b></div></div>\n\n\n<div style=\" margin:5px auto;\"><hr style=\"background:#d8612d; height:2px; border:0; margin:0 auto;width:50%;\"></div>\n\n<div contenteditable=\"true\">\n<div style=\"display:block; margin:15px 0 25px 0; padding:0; font-size:95px; text-align:center;line-height:100%; color:#fff;\" align=\"center;\">EVENT</div>\n</div>\n</td>\n</tr>\n\n</tbody>\n</table>                                  \n</td>\n</tr>\n\n<tr>\n<td align=\"center\" valign=\"top\">\n<table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"600\" style=\"border-collapse:collapse;background:#d8612d;max-width:600px;width:100%;\">\n<tbody>\n<tr>\n<td valign=\"top\" style=\"text-align:center;word-break:break-word;padding:9px;\">\n</td>\n</tr>\n</tbody>\n</table>                                  \n</td>\n</tr>\n\n\n<tr>\n<td align=\"center\" valign=\"top\">\n                                    \n<table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"100%\" style=\"border-collapse:collapse;border-top:0;border-bottom:0;background-color:#ffffff;width:100%;max-width:600px;\">\n<tbody><tr><td align=\"center\" valign=\"top\">\n                                                \n\n<table align=\"left\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" style=\"min-width:100%;border-collapse:collapse;  clear:both;max-width:600px;width:100%;\" width=\"100%;\">\n<tbody>\n<tr>\n<td valign=\"top\" style=\"padding-top:0; padding-right:18px; padding-bottom:9px; padding-left:18px;\">\n\n<div contenteditable=\"true\">\n<p style=\"display:block; margin:18px 0 15px 0; padding:0; font-size:23px; font-weight:400; text-align:center; color:#000; line-height:30px;\">Your Awesome Event Discriotion Right Here</p>\n</div>\n\n<div contenteditable=\"true\">\n<p style=\"display:block; margin:20px 0 18px 0; padding:0; font-size:60px; font-weight:400; text-align:center; color:#4a4949; line-height:30px;\"><b>31.5.2016</b></p>\n</div>\n\n<div style=\" margin:5px auto;\"><hr style=\"background:#d6d6d6; height:2px; border:0; margin:0 auto;width:70%;\"></div>\n\n<div contenteditable=\"true\">\n<p style=\"display:block; margin:17px 0 10px 0; padding:0; font-size:21px; font-weight:400; text-align:center; color:#4a4949; line-height:30px;\">And Where It All Gonna Happen?</p>\n</div>\n\n<div contenteditable=\"true\">\n<div style=\"display:block; margin:15px 0 10px 0; padding:0; font-size:60px; font-style:normal; line-height:100%; color:#ff854f;\" align=\"center\"><b>YOUR CITY</b></div>\n</div>\n\n<div contenteditable=\"true\">\n<div style=\"display:block; margin:15px 0 15px 0; padding:0; font-size:40px; text-align:center;line-height:100%; color:#ff854f;font-weight:100;\" align=\"center;\">AWESOME STREET</div>\n</div>\n\n<div contenteditable=\"true\" style=\"text-align:center;\">\n<a href=\"#\" style=\"border-radius:05px; background-color:#d8612d; padding: 6px 30px; text-align:center; text-decoration:none; color:#ffffff; display:inline-block;font-size:23px;margin:30px 0 30px 0;\" class=\"sm-button\">Join Now</a></div>\n\n</td>\n</tr>\n\n<tr>\n<td align=\"center\" valign=\"top\">\n<table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"600\" style=\"border-collapse:collapse;background:#d8612d;max-width:600px;width:100%;\">\n<tbody>\n<tr>\n<td valign=\"top\" style=\"padding:2px;\">\n</td>\n</tr>\n</tbody>\n</table>                                  \n</td>\n</tr>\n\n\n</tbody>\n</table>\n\n\n</td>\n</tr>\n</tbody></table>\n</td>\n</tr>\n\n</tbody>\n</table>\n\n\n</td>\n</tr>\n</tbody>\n</table>\n\n\n</div>', 'template4.jpg', 1, 1470394007, '', 'active'),
(8, 'default', 'inline', 'Template 8', '<div style=\"width:100%; max-width:600px; margin: auto;\">\n\n<table align=\"center\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" height=\"100%\" width=\"100%\" style=\"border-collapse:collapse;height:100%;margin:0;padding:0;background-color:#e9e9e9; width:100%;max-width:600px; margin:0 auto;\">\n<tbody>\n<tr>\n<td align=\"center\" valign=\"top\" style=\"height:100%;margin:0;padding:0;width:100%;border-top:0\">\n\n<table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"100%\" style=\"border-collapse:collapse;width:100%;max-width:600px;\">\n<tbody>\n<tr>\n<td align=\"left\" valign=\"top\">\n\n<table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"600\" style=\"border-collapse:collapse; background:#fe546b;width:100%;max-width:600px;\">\n<tbody>\n<tr>\n<td valign=\"top\" style=\"padding:9px;text-align:left\">\n<img align=\"left\" alt=\"\" src=\"{image_path}images/logo8.png\" style=\"height:33px;max-height:33px;padding-bottom:0;display:inline!important;vertical-align:bottom;border:0;height:auto;outline:none;text-decoration:none\"></td></tr></tbody></table>\n                                    \n</td>\n</tr>\n\n<tr>\n<td align=\"center\" valign=\"top\">\n                                    \n<table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"100%\" style=\"border-collapse:collapse;border-top:0;border-bottom:0;width:100%;max-width:600px;\">\n<tbody><tr><td align=\"center\" valign=\"top\">\n                                                \n<table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"600\" style=\"border-collapse:collapse;width:100%;max-width:600px;\">\n<tbody><tr><td align=\"center\" valign=\"top\">\n\n<table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"100%\" style=\"border-collapse:collapse;background-color:#ffffff;border:0px solid #d5d5d5;width:100%;max-width:600px;\">\n<tbody><tr><td valign=\"top\">\n\n\n<table align=\"left\" width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" style=\"min-width:100%;border-collapse:collapse;width:100%;max-width:600px;background-color:#eaede2;\">\n<tbody><tr>\n\n<td valign=\"top\" width=\"60%\">\n<div contenteditable=\"true\">\n<div style=\"display:block; padding:0; font-size:40px; color:#fe546b;font-weight:300;margin-left:50px;margin-top:60px;\">Look and feel Your Best.</div>\n</div>\n<div contenteditable=\"true\">\n<p style=\"display:block;padding:0; font-size:16px;color:#4a4949;margin-left:50px;margin-top:20px;\">We have you covered from head to toe Hair, Body & facial Treatments</p>\n</div>\n</td>\n\n<td valign=\"top\">\n<img align=\"center\" alt=\"\" src=\"{image_path}images/temp8.png\" style=\"max-width:240px;max-height:273px;padding-bottom:0;display:inline!important;vertical-align:bottom;border:0;height:auto;outline:none;text-decoration:none\" tabindex=\"0\"></td>\n</tr>\n</tbody></table>\n\n\n<table align=\"left\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" style=\"max-width:100%;min-width:100%;border-collapse:collapse;  clear:both;width:100%;max-width:600px;\" width=\"100%\"><tbody><tr><td valign=\"top\" style=\"padding-top:0; padding-right:18px; padding-bottom:9px; padding-left:18px; word-break:break-word; color:#606060; font-size:15px; text-align:left;\">\n<div contenteditable=\"true\">\n<div style=\"display:block; margin:15px 0 0 0; padding:0; font-size:40px; font-weight:300;color:#fe546b;\" align=\"center\">Beauty Zone</div>\n</div>\n\n<div contenteditable=\"true\">\n<p style=\"display:block; margin:15px 0 5px 0; font-size:15px; text-align:center; color:#353535!important;padding:0 5%;\">Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus.</p>\n</div>\n</td></tr></tbody>\n</table>\n\n\n\n<table border=\"0\" cellpadding=\"15\" cellspacing=\"0\" style=\"width:100%;max-width:600px;min-width:100%;border-collapse:collapse; clear:both;text-align:center;\" width=\"100%\">\n<tbody>\n\n<tr>\n<td>\n<img alt=\"\" src=\"{image_path}images/icon1.png\" tabindex=\"0\" style=\"border:1px solid #dbdbdb;border-radius:50%;width:94px;height:94px;margin:5px auto;\">\n<div contenteditable=\"true\">\n<p style=\"display:block; margin:15px 0 5px 0; font-size:18px;color:#4a4949;font-weight:500;\">Cutting & Styling</p>\n</div>\n</td>\n\n<td>\n<img alt=\"\" src=\"{image_path}images/icon2.png\" tabindex=\"0\" style=\"border:1px solid #dbdbdb;border-radius:50%;background:#fff;width:94px;height:94px;margin:5px auto;\">\n<div contenteditable=\"true\">\n<p style=\"display:block; margin:15px 0 5px 0; font-size:18px;color:#4a4949;font-weight:500;\">Spa & Relaxing</p>\n</div>\n</td>\n\n<td>\n<img alt=\"\" src=\"{image_path}images/icon3.png\" tabindex=\"0\" style=\"border:1px solid #dbdbdb;border-radius:50%;width:94px;height:94px;margin:5px auto;\">\n<div contenteditable=\"true\">\n<p style=\"display:block; margin:15px 0 5px 0; font-size:18px;color:#4a4949;font-weight:500;\">Nail Treatment</p>\n</div>\n</td>\n\n</tr>\n</tbody>\n</table>\n\n<table align=\"left\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" style=\"max-width:100%;min-width:100%;border-collapse:collapse;  clear:both;width:100%;max-width:600px;\" width=\"100%\">\n<tbody><tr>\n<td valign=\"top\" style=\"padding-top:0; padding-right:18px; padding-bottom:9px; padding-left:18px;\">\n<div contenteditable=\"true\" style=\"text-align:center;\">\n<a href=\"#\" style=\"border-radius:05px; background-color:#fe546b; padding: 1.8% 25%; text-align:center; text-decoration:none; color:#ffffff; display:inline-block;font-size:18px;margin:30px 0 30px 0;\" class=\"sm-button\">Get A Appoitment</a></div>\n</td></tr></tbody>\n</table>\n				\n\n</td>\n</tr>\n</tbody></table>\n</td>\n</tr>\n</tbody></table>\n</td>\n</tr>\n</tbody></table>\n</td>\n</tr>\n\n</tbody>\n</table>\n			\n	</td>\n    </tr>\n    </tbody>\n    </table>\n\n\n</div>', 'template13.jpg', 1, 1470394007, '', 'active'),
(9, 'default', 'inline', 'Template 9', '<div style=\"width:100%; max-width:600px; margin: auto;\">\n\n\n<table align=\"center\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" height=\"100%\" width=\"100%\" style=\"border-collapse:collapse;height:100%;margin:0;padding:0;width:100%;background-color:#e9e9e9; max-width:600px; margin:0 auto;\">\n<tbody>\n<tr>\n<td align=\"center\" valign=\"top\" style=\"height:100%;margin:0;padding:0;width:100%;border-top:0\">\n\n<table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"100%\" style=\"border-collapse:collapse;width:100%;max-width:600px;\">\n<tbody>\n<tr>\n<td align=\"left\" valign=\"top\">\n\n<table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"600\" style=\"border-collapse:collapse; background:#fff;width:100%;max-width:600px;\">\n<tbody>\n<tr>\n<td valign=\"top\" style=\"padding:9px;\">\n<img align=\"left\" alt=\"\" src=\"{image_path}images/logo9.png\" width=\"163\" style=\"max-width:163px;padding-bottom:0;display:inline!important;vertical-align:bottom;border:0;height:auto;outline:none;\ntext-decoration:none;margin:10px 0 5px 15px;\"></td></tr></tbody></table>                                 \n</td>\n</tr>\n\n<tr>\n<td align=\"center\" valign=\"top\">\n                                    \n<table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"100%\" style=\"border-collapse:collapse;border-top:0;border-bottom:0;background-color:#ffffff;width:100%;max-width:600px;\">\n<tbody><tr><td align=\"center\" valign=\"top\">\n                                                \n\n<table align=\"left\" width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" style=\"min-width:100%;border-collapse:collapse;width:100%;max-width:600px;\">\n<tbody><tr><td valign=\"top\">\n<img align=\"center\" alt=\"\" src=\"{image_path}images/temp9.png\" width=\"600\" style=\"max-width:100%;padding-bottom:0;display:inline!important;vertical-align:bottom;border:0;height:auto;outline:none;text-decoration:none;max-height:203px;\" tabindex=\"0\"></td></tr>\n</tbody></table>\n\n\n<table align=\"left\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" style=\"max-width:100%;min-width:100%;border-collapse:collapse;  clear:both;width:100%;max-width:600px;\" width=\"100%;\"><tbody><tr><td valign=\"top\" style=\"padding-top:0; padding-right:18px; padding-bottom:9px; padding-left:18px; word-break:break-word; color:#606060; font-size:15px; text-align:left;\">\n\n\n<div contenteditable=\"true\">\n<p style=\"display:block; margin:20px 0 10px 0; padding:0; font-size:25px;text-align:center; color:#4a4949;\">Email Confirmation</p>\n</div>\n\n\n<div contenteditable=\"true\">\n<p style=\"display:block; margin:2px 0 10px 0; padding:10px 25px ; font-size:15px; font-weight:400; text-align:center; color:#818080; line-height:25px;\">Lorem ipsum dolor sit amet, no quodsi definitiones vis. Verterem postulant intellegat est et, an partiendo argumentum elaboraret qui. Diam salutatus cu est, choro sententiae nam id. An sint meliore conceptam qui, eum dicit corpora consequuntur no.\n</p>\n</div>\n\n<div contenteditable=\"true\" style=\"text-align:center;\">\n<a href=\"#\" style=\"font-family:Roboto; border-radius:04px; background-color:#ffb200; padding:1.7% 6%; text-align:center; text-decoration:none; color:#ffffff; display:inline-block; margin:10px 0 20px 0;font-size:23px;\" class=\"sm-button\">Verify Email Address</a>\n</div>\n\n</td>\n</tr>\n</tbody>\n</table>\n                                 \n                                    \n</td>\n</tr>\n</tbody></table>\n</td>\n</tr>\n</tbody>\n</table>\n</td>\n</tr>\n</tbody>\n</table>\n\n\n</div>', 'template10.jpg', 1, 1470394007, '', 'active'),
(10, 'default', 'inline', 'Template 10', '<div style=\"width:100%; max-width:600px; margin: auto;\">\n\n\n<table align=\"center\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" height=\"100%\" width=\"100%\" style=\"border-collapse:collapse;height:100%;margin:0;padding:0;background-color:#e9e9e9; width:100%;max-width:600px; margin:0 auto;\">\n<tbody>\n<tr>\n<td align=\"center\" valign=\"top\" style=\"height:100%;margin:0;padding:0;width:100%;border-top:0\">\n\n<table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"100%\" style=\"border-collapse:collapse;width:100%;max-width:600px;\">\n<tbody>\n<tr>\n<td align=\"left\" valign=\"top\">\n\n<table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"600\" style=\"border-collapse:collapse; background:#98ba57;width:100%;max-width:600px;\">\n<tbody>\n<tr>\n<td valign=\"top\" style=\"padding:9px;text-align:left\">\n<img align=\"left\" alt=\"\" src=\"{image_path}images/logo10.png\" style=\"padding-bottom:0;display:inline!important;vertical-align:bottom;border:0;height:auto;outline:none;text-decoration:none\"></td></tr></tbody></table>\n                                    \n</td>\n</tr>\n\n<tr>\n<td align=\"center\" valign=\"top\">\n                                    \n<table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"100%\" style=\"border-collapse:collapse;border-top:0;border-bottom:0;width:100%;max-width:600px;\">\n<tbody><tr><td align=\"center\" valign=\"top\">\n                                                \n<table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"600\" style=\"border-collapse:collapse;width:100%;max-width:600px;\">\n<tbody><tr><td align=\"center\" valign=\"top\">\n\n<table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"100%\" style=\"border-collapse:collapse;background-color:#ffffff;border:0px solid #d5d5d5;width:100%;max-width:600px;\">\n<tbody><tr><td valign=\"top\">\n\n\n<table align=\"left\" width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" style=\"min-width:100%;border-collapse:collapse;width:100%;max-width:600px;background-color:#ffffff;\">\n<tbody><tr>\n\n<td valign=\"top\" width=\"55%;\">\n<div contenteditable=\"true\">\n<div style=\"display:block; padding:0; font-size:38px; color:#4a4949;margin-left:50px;margin-top:30px;\"><b>Free</b></div>\n</div>\n<div contenteditable=\"true\">\n<div style=\"display:block; padding:0; font-size:33px; color:#98ba57;margin-left:50px;\"><b>First Consultant</b></div>\n</div>\n<div contenteditable=\"true\">\n<p style=\"display:block;padding:0 10%; font-size:10px;color:#b6b6b6;margin-left:25px;margin-top:20px;\">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>\n</div>\n</td>\n\n<td valign=\"top\">\n<img align=\"center\" alt=\"\" src=\"{image_path}images/temp10.png\" style=\"max-width:270px;max-height:240px;padding-bottom:0;display:inline!important;vertical-align:bottom;border:0;height:auto;outline:none;text-decoration:none\" tabindex=\"0\"></td>\n</tr>\n</tbody></table>\n\n\n<table border=\"0\" cellpadding=\"15\" cellspacing=\"0\" style=\"width:100%;max-width:600px;min-width:100%;border-collapse:collapse; clear:both;text-align:center;\" width=\"100%\">\n<tbody>\n\n<tr>\n<td width=\"30%;\" style=\"background-color:#f4f4f4;\">\n<img alt=\"\" src=\"{image_path}images/icon4.png\" tabindex=\"0\" style=\"border-radius:50%;background:#fff;width:70px;height:70px;margin:5px auto;\">\n<div contenteditable=\"true\">\n<p style=\"display:block; margin:10px 0; font-size:15px;color:#4a4949;\">Painless Treatment</p>\n</div>\n</td>\n\n<td width=\"30%;\" style=\"background-color:#ececec;\">\n<img alt=\"\" src=\"{image_path}images/icon5.png\" tabindex=\"0\" style=\"border-radius:50%;background:#fff;width:70px;height:70px;margin:5px auto;\">\n<div contenteditable=\"true\">\n<p style=\"display:block; margin:15px 0 5px 0; font-size:15px;color:#4a4949;\">Professional Services</p>\n</div>\n</td>\n\n<td width=\"30%;\" style=\"background-color:#f4f4f4;\">\n<img alt=\"\" src=\"{image_path}images/icon6.png\" tabindex=\"0\" style=\"border-radius:50%;background:#fff;width:70px;height:70px;margin:5px auto;\">\n<div contenteditable=\"true\">\n<p style=\"display:block; margin:15px 0 5px 0; font-size:15px;color:#4a4949;\">High Quality Drugs</p>\n</div>\n</td>\n\n</tr>\n</tbody>\n</table>\n\n\n<table align=\"left\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" style=\"max-width:100%;min-width:100%;border-collapse:collapse; clear:both;width:100%;max-width:600px;\" width=\"100%\"><tbody><tr><td valign=\"top\" style=\"padding-top:0; padding-right:18px; padding-bottom:9px; padding-left:18px;color:#606060; font-size:15px;\">\n\n\n<div contenteditable=\"true\">\n<p style=\"display:block;font-size:13px; text-align:center; color:#bfbfbf!important;line-height:21px;margin-top:5%;\">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.</p>\n</div>\n</td></tr></tbody>\n</table>\n\n\n<table align=\"left\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" style=\"min-width:100%;border-collapse:collapse;  clear:both;width:100%;max-width:600px;\" width=\"100%\">\n<tbody><tr>\n<td valign=\"top\" style=\"padding-top:0; padding-right:18px; padding-bottom:9px; padding-left:18px;\">\n<div contenteditable=\"true\" style=\"text-align:center;\">\n<a href=\"#\" style=\"border-radius:05px; background-color:#98ba57; padding: 2.2% 5%; text-align:center; text-decoration:none; color:#ffffff; display:inline-block;font-size:21px;font-weight:300;margin:30px 0 30px 0;\" class=\"sm-button\">Click Here, To Book Your Appointment Today.</a></div>\n</td></tr></tbody>\n</table>\n				\n\n\n</td>\n</tr>\n</tbody></table>\n</td>\n</tr>\n</tbody></table>\n</td>\n</tr>\n</tbody></table>\n</td>\n</tr>\n\n</tbody>\n</table>\n			\n</td>\n</tr>\n</tbody>\n</table>\n\n\n</div>', 'template6.jpg', 1, 1470394007, '', 'active');
INSERT INTO `tbl_template` (`id`, `template_type`, `editor_type`, `template_name`, `template_text`, `template_screenshot`, `member_id`, `add_time`, `ip`, `status`) VALUES
(11, 'default', 'inline', 'Template 11', '<div style=\"width:100%; max-width:600px; margin: auto;\">\n\n\n<table align=\"center\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" height=\"100%\" width=\"100%\" style=\"border-collapse:collapse;height:100%;margin:0;padding:0;background-color:#fff; width:100%;max-width:600px; margin:0 auto;\">\n<tbody>\n<tr>\n<td align=\"center\" valign=\"top\" style=\"height:100%;margin:0;padding:0;width:100%;border-top:0\">\n\n<table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"100%\" style=\"border-collapse:collapse;width:100%;max-width:600px;\">\n<tbody>\n<tr>\n<td>\n\n\n<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" style=\"min-width:100%;border-collapse:collapse;width:100%;max-width:600px;background-color:#fff;\">\n<tbody><tr>\n\n<td valign=\"top\"> \n<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" style=\"border-collapse:collapse;width:100%;max-width:600px;background-color:#fff;\">\n<tbody><tr>\n<td valign=\"top\" style=\"padding:20px 20px;text-align:left\">\n<img align=\"left\" alt=\"\" src=\"{image_path}images/logo11.png\" style=\"padding-bottom:0;display:inline!important;vertical-align:bottom;border:0;height:auto;outline:none;text-decoration:none\"></td>\n\n</tr>\n</tbody>\n</table>\n</td>\n</tr>\n\n</tbody>\n</table>\n\n</td>\n</tr>\n\n</tbody>\n</table>\n \n<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" style=\"border-collapse:collapse;width:97%;max-width:600px;background-color:#00a2b5;\">\n<tbody>\n\n<tr>\n\n<td valign=\"top\">\n<img align=\"center\" alt=\"\" src=\"{image_path}images/temp11.png\" style=\"max-height:292px;max-width:214px;padding-bottom:0;display:inline!important;vertical-align:bottom;border:0;height:auto;outline:none;text-decoration:none;margin-top:20px;margin-left:10px;\" tabindex=\"0\"></td>\n\n<td valign=\"top\">\n<div contenteditable=\"true\">\n<div style=\"display:block; padding:0; font-size:65px; color:#fff;margin-left:20px;margin-top:25px;font-weight:100;\">Free</div>\n</div>\n<div contenteditable=\"true\">\n<div style=\"display:block; padding:0; font-size:60px; color:#fff;margin-left:20px;font-weight:100;\">Shipping</div>\n</div>\n<div contenteditable=\"true\">\n<a href=\"#\" style=\"border-radius:25px; background-color:#fff; padding: 1.5% 17%; text-align:center; text-decoration:none; color:#4a4949; display:inline-block;font-size:21px;font-weight:400;margin:15px 0 5px 20px;\" class=\"sm-button\">On All Orders</a></div>\n<div contenteditable=\"true\" style=\"display:block;font-size:14px;color:#fff;padding-left: 25px;\n padding-bottom: 5px; padding-top: 5px;\">\nYou will get your delivery by Diwali\n</div>\n</td>\n</tr>\n\n</tbody>\n</table>\n\n\n\n\n\n\n<table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"100%\" style=\"border-collapse:collapse;background-color:#ffffff;border:0px solid #d5d5d5;width:95%;max-width:600px;\">\n<tbody><tr><td valign=\"top\">\n<table border=\"0\" cellpadding=\"15\" cellspacing=\"0\" style=\"width:100%;max-width:600px;min-width:100%;border-collapse:collapse; clear:both;text-align:center;margin-top:3%;\" width=\"100%\">\n<tbody>\n\n<tr>\n<td>\n<img align=\"center\" alt=\"\" src=\"{image_path}images/icon7.png\" style=\"border:1px solid #e7e7e7;outline:none;max-height:108px;\" tabindex=\"0\">\n<div contenteditable=\"true\" style=\"\">\n<div style=\"display:block; font-size:16px;color:#4a4949;margin:5px 0 0 0;\">Hand Bags</div>\n<div style=\"display:block;font-size:13px;color:#4a4949;margin:5px 0 0 0; \">Under Rs 499</div>\n</div>\n<div contenteditable=\"true\" style=\"text-align:center;\">\n<a href=\"#\" style=\"border-radius:05px; background-color:#fe9e01;padding: 4% 15%; text-align:center; text-decoration:none; color:#ffffff; display:inline-block;font-size:14px;margin:5px 0 5px 0;\" class=\"sm-button\">Buy Now</a></div>\n</td>\n\n<td>\n<img align=\"center\" alt=\"\" src=\"{image_path}images/icon8.png\"   style=\"border:1px solid #e7e7e7;outline:none;max-height:108px;\" tabindex=\"0\">\n<div contenteditable=\"true\" style=\"\">\n<div style=\"display:block; font-size:16px;color:#4a4949;margin:5px 0 0 0;\">Sunglasses</div>\n<div style=\"display:block;font-size:13px;color:#4a4949;margin:5px 0 0 0; \">Under Rs 499</div>\n</div>\n<div contenteditable=\"true\" style=\"text-align:center;\">\n<a href=\"#\" style=\"border-radius:05px; background-color:#fe9e01;padding: 4% 15%; text-align:center; text-decoration:none; color:#ffffff; display:inline-block;font-size:14px;margin:5px 0 5px 0;\" class=\"sm-button\">Buy Now</a></div>\n</td>\n\n<td>\n<img align=\"center\" alt=\"\" src=\"{image_path}images/icon9.png\"  style=\"border:1px solid #e7e7e7;outline:none;max-height:108px;\" tabindex=\"0\">\n<div contenteditable=\"true\" style=\"\">\n<div style=\"display:block; font-size:16px;color:#4a4949;margin:5px 0 0 0;\">Backpacks</div>\n<div style=\"display:block;font-size:13px;color:#4a4949;margin:5px 0 0 0; \">Under Rs 499</div>\n</div>\n<div contenteditable=\"true\" style=\"text-align:center;\">\n<a href=\"#\" style=\"border-radius:05px; background-color:#fe9e01;padding: 4% 15%; text-align:center; text-decoration:none; color:#ffffff; display:inline-block;font-size:14px;margin:5px 0 5px 0;\" class=\"sm-button\">Buy Now</a></div>\n</td>\n\n</tr>\n\n<tr>\n\n<td>\n<img align=\"center\" alt=\"\" src=\"{image_path}images/icon12.png\" style=\"border:1px solid #e7e7e7;outline:none;max-height:108px;\" tabindex=\"0\">\n<div contenteditable=\"true\" style=\"\">\n<div style=\"display:block; font-size:16px;color:#4a4949;margin:5px 0 0 0;\">Watches</div>\n<div style=\"display:block;font-size:13px;color:#4a4949;margin:5px 0 0 0; \">Under Rs 499</div>\n</div>\n<div contenteditable=\"true\" style=\"text-align:center;\">\n<a href=\"#\" style=\"border-radius:05px; background-color:#fe9e01;padding: 4% 15%; text-align:center; text-decoration:none; color:#ffffff; display:inline-block;font-size:14px;margin:5px 0 5px 0;\" class=\"sm-button\">Buy Now</a></div>\n</td>\n\n<td>\n<img align=\"center\" alt=\"\" src=\"{image_path}images/icon10.png\" style=\"border:1px solid #e7e7e7;outline:none;max-height:108px;\" tabindex=\"0\">\n<div contenteditable=\"true\" style=\"\">\n<div style=\"display:block; font-size:16px;color:#4a4949;margin:5px 0 0 0;\">Earings</div>\n<div style=\"display:block;font-size:13px;color:#4a4949;margin:5px 0 0 0; \">Under Rs 499</div>\n</div>\n<div contenteditable=\"true\" style=\"text-align:center;\">\n<a href=\"#\" style=\"border-radius:05px; background-color:#fe9e01;padding: 4% 15%; text-align:center; text-decoration:none; color:#ffffff; display:inline-block;font-size:14px;margin:5px 0 5px 0;\" class=\"sm-button\">Buy Now</a></div>\n</td>\n\n<td>\n<img align=\"center\" alt=\"\" src=\"{image_path}images/icon11.png\" style=\"border:1px solid #e7e7e7;outline:none;max-height:108px;\" tabindex=\"0\">\n<div contenteditable=\"true\" style=\"\">\n<div style=\"display:block; font-size:16px;color:#4a4949;margin:5px 0 0 0;\">Bangles</div>\n<div style=\"display:block;font-size:13px;color:#4a4949;margin:5px 0 0 0; \">Under Rs 499</div>\n</div>\n<div contenteditable=\"true\" style=\"text-align:center;\">\n<a href=\"#\" style=\"border-radius:05px; background-color:#fe9e01;padding: 4% 15%; text-align:center; text-decoration:none; color:#ffffff; display:inline-block;font-size:14px;margin:5px 0 5px 0;\" class=\"sm-button\">Buy Now</a></div>\n</td>\n</tr>\n\n</tbody>\n</table>\n\n\n<tr>\n<td align=\"center\" valign=\"top\">\n<table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"600\" style=\"border-collapse:collapse;max-width:600px;width:100%;\">\n<tbody>\n<tr>\n<div contenteditable=\"true\">\n<div style=\"display:block; font-size:13px; text-align:right;line-height:100%; color:#4a4949;\" align=\"center;\">*T & C Apply</div></div>\n</tr>\n</tbody>\n</table>                                  \n</td>\n</tr>\n\n\n<tr>\n<td align=\"center\" valign=\"top\">\n<table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"600\" style=\"border-collapse:collapse;background:#00a2b5;max-width:600px;width:100%;margin:2% 0;\">\n<tbody>\n<tr>\n<td valign=\"top\" style=\"padding:5px;\">\n</td>\n</tr>\n</tbody>\n</table>                                  \n</td>\n</tr>\n\n\n</td>\n</tr>\n</tbody></table>\n			\n</td>\n</tr>\n</tbody>\n</table>\n\n\n</div>', 'template2.jpg', 1, 1470394007, '', 'active'),
(12, 'default', 'inline', 'Template 12', '<div style=\"width:100%; max-width:600px; margin: auto;\">\n\n\n<table align=\"center\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" height=\"100%\" width=\"100%\" style=\"border-collapse:collapse;height:100%;margin:0;padding:0;background-color:#e9e9e9; width:100%;max-width:600px; margin:0 auto;\">\n<tbody>\n<tr>\n<td align=\"center\" valign=\"top\" style=\"height:100%;margin:0;padding:0;width:100%;border-top:0\">\n\n<table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"100%\" style=\"border-collapse:collapse;width:100%;max-width:600px;\">\n<tbody>\n<tr>\n<td align=\"left\" valign=\"top\">\n\n<table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"600\" style=\"border-collapse:collapse; background:#fff;width:100%;max-width:600px;\">\n<tbody>\n<tr>\n<td valign=\"top\" style=\"padding:17px 30px;text-align:left\">\n<img align=\"left\" alt=\"\" src=\"{image_path}images/logo12.png\" style=\"padding-bottom:0;display:inline!important;vertical-align:bottom;border:0;height:auto;outline:none;text-decoration:none;\nmax-height:39px;\"></td></tr></tbody></table>\n\n<tr>\n<td align=\"center\" valign=\"top\">\n<table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"600\" style=\"border-collapse:collapse;background:#eb775e;max-width:600px;width:100%;\">\n<tbody>\n<tr>\n<td valign=\"top\" style=\"padding:2px;\">\n</td>\n</tr>\n</tbody>\n</table>                                  \n</td>\n</tr>\n                                    \n</td>\n</tr>\n\n<tr>\n<td align=\"center\" valign=\"top\">\n                                    \n<table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"100%\" style=\"border-collapse:collapse;border-top:0;border-bottom:0;width:100%;max-width:600px;\">\n<tbody><tr><td align=\"center\" valign=\"top\">\n                                                \n<table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"600\" style=\"border-collapse:collapse;width:100%;max-width:600px;\">\n<tbody><tr><td align=\"center\" valign=\"top\">\n\n<table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"100%\" style=\"border-collapse:collapse;background-color:#ffffff;border:0px solid #d5d5d5;width:100%;max-width:600px;\">\n<tbody><tr><td valign=\"top\">\n\n\n<table align=\"left\" width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" style=\"min-width:100%;border-collapse:collapse;width:100%;max-width:600px;background-color:#fbf7eb;\">\n<tbody><tr>\n\n<td valign=\"top\">\n<div style=\"background:#f36345;margin-top: 23%;margin-left:14%;padding:7% 0;margin-right:5%;\" align=\"center\">\n<div contenteditable=\"true\">\n<div style=\"font-size:45px; color:#fff;\">Get</div>\n<div style=\"font-size:26px; color:#fff;\">20% Discount</div>\n<div style=\"font-size:19px;color:#fff;margin-top:1%;\">This Summer</div>\n</div>\n</div>\n</td>\n\n<td valign=\"top\" width=\"60%\">\n<img align=\"center\" alt=\"\" src=\"{image_path}images/temp12.png\" style=\"max-width:360px;max-height:274px;padding-bottom:0;display:inline!important;vertical-align:bottom;border:0;height:auto;outline:none;text-decoration:none\" tabindex=\"0\"></td>\n</tr>\n</tbody></table>\n\n\n<table border=\"0\" cellpadding=\"15\" cellspacing=\"0\" style=\"width:100%;max-width:600px;min-width:100%;border-collapse:collapse; clear:both;\" width=\"100%\">\n<tbody>\n<tr>\n<td>\n\n<table border=\"0\" cellpadding=\"15\" cellspacing=\"0\" style=\"width:100%;max-width:600px;min-width:100%;border-collapse:collapse; clear:both;\" width=\"100%\">\n<tbody>\n<tr>\n<td>\n<img align=\"center\" alt=\"\" src=\"{image_path}images/icon13.png\" tabindex=\"0\" style=\"max-width:214px;max-height:142px;\">\n</td>\n\n<td>\n<div contenteditable=\"true\" style=\"padding:2%;\">\n<div style=\"font-size:16px;color:#f36345;\">Your Beauty Starts Here</div>\n<div style=\"font-size:13px;color:#4a4949;margin-top:2%;\">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</div>\n</div>\n</td>\n</tr>\n\n</tbody>\n</table>\n\n<table border=\"0\" cellpadding=\"15\" cellspacing=\"0\" style=\"width:100%;max-width:600px;min-width:100%;border-collapse:collapse; clear:both;\" width=\"100%\">\n<tbody>\n<tr>\n<td>\n<div contenteditable=\"true\">\n<div style=\"font-size:16px;color:#f36345;\">Style Your Hair With Latest Hairstyles</div>\n<div style=\"font-size:13px;color:#4a4949;margin-top:2%;\">\n<ul style=\"margin-left: -7%;\">\n<li>Fish Tail</li>\n<li>Water Fall</li>\n<li>French Bub</li>\n</ul>\n</div>\n</div>\n</td>\n<td>\n<img alt=\"\" src=\"{image_path}images/icon14.png\" tabindex=\"0\" style=\"max-width:210px;max-height:140px;\">\n</td>\n</tr>\n</tbody>\n</table>\n\n<table border=\"0\" cellpadding=\"15\" cellspacing=\"0\" style=\"width:100%;max-width:600px;min-width:100%;border-collapse:collapse; clear:both;\" width=\"100%\">\n<tbody>\n<tr>\n<td>\n<img alt=\"\" src=\"{image_path}images/icon15.png\" tabindex=\"0\" style=\"max-width:214px;max-height:143px;\">\n</td>\n<td>\n<div contenteditable=\"true\">\n<div style=\"font-size:16px;color:#f36345;\">Special Discount On Bridal Package</div>\n<div style=\"font-size:13px;color:#4a4949;margin-top:2%;\">\n<ul  style=\"margin-left: -7%;\">\n<li>Water Proof Makeup</li>\n<li>Every Type Of Hair Styles</li>\n<li>Manicure & Pedicure</li>\n<li>Body Polishing </li>  \n</ul>\n</div>\n</div>\n</td>\n</tr>\n</tbody>\n</table>\n\n</td>\n</tr>\n</tbody>\n</table>\n\n\n<table align=\"left\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" style=\"min-width:100%;border-collapse:collapse;  clear:both;width:100%;max-width:600px;\" width=\"100%\">\n<tbody><tr>\n<td valign=\"top\" style=\"padding-top:0; padding-right:18px; padding-bottom:9px; padding-left:18px;\">\n<div contenteditable=\"true\" style=\"text-align:center;\">\n<a href=\"#\" style=\"border-radius:03px; background-color:#f36345; padding: 2.2% 23%; text-align:center; text-decoration:none; color:#ffffff; display:inline-block;font-size:19px;margin:30px 0 30px 0;\" class=\"sm-button\">Book Your Appointment Now</a></div>\n</td></tr></tbody>\n</table>\n				\n			\n\n\n</td>\n</tr>\n</tbody></table>\n</td>\n</tr>\n</tbody></table>\n</td>\n</tr>\n</tbody></table>\n</td>\n</tr>\n\n</tbody>\n</table>\n			\n</td>\n</tr>\n</tbody>\n</table>\n\n\n</div>', 'template12.jpg', 1, 1470394007, '', 'active'),
(13, 'default', 'inline', 'Template 13', '<div style=\"width:100%; max-width:600px; margin: auto;\">\n\n\n<table align=\"center\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" height=\"100%\" width=\"100%\" style=\"border-collapse:collapse;height:100%;margin:0 auto;padding:0;width:100%;background-color:#e9e9e9;max-width:600px;\">\n<tbody>\n<tr>\n<td align=\"center\" valign=\"top\" style=\"height:100%;margin:0;padding:0;width:100%;border-top:0\">\n\n<table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"100%\" style=\"border-collapse:collapse;max-width:600px;width:100%;\">\n<tbody>\n<table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"600\" style=\"border-collapse:collapse; background:#fff;max-width:600px;width:100%;\">\n<tbody>\n<tr>\n<td valign=\"top\" style=\"padding:18px 0;text-align:center\">\n<img align=\"center\" alt=\"\" src=\"{image_path}images/logo13.png\" style=\"padding-bottom:0;display:inline!important;vertical-align:bottom;border:0;width:105px;height:51px;outline:none;text-decoration:none\">\n</td>\n</tr>\n\n<tr>\n<td>\n<div contenteditable=\"true\">\n<div style=\"display:block; margin:0px 0 15px 0;font-size:15px;color:#050505;\" align=\"center\">Womens / Mens / Shoes / Denim</div>\n</div>\n</td>\n</tr>\n\n</tbody>\n</table>\n\n\n\n\n<table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"100%\" style=\"border-collapse:collapse;background-color:#fff;border:1px solid #fff;max-width:600px;width:100%;\">\n<tbody>\n<tr>\n<td valign=\"top\">\n\n<table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"92%\" style=\"border-collapse:collapse;margin:0 auto 25px auto;max-width:550px;width:100%;background: #fba5a7;\nbackground: -moz-linear-gradient(left,  #fba5a7 1%, #f5aa98 49%, #ed948d 53%, #9dc3be 100%);\nbackground: -webkit-linear-gradient(left,  #fba5a7 1%,#f5aa98 49%,#ed948d 53%,#9dc3be 100%);\nbackground: linear-gradient(to right,  #fba5a7 1%,#f5aa98 49%,#ed948d 53%,#9dc3be 100%);\nfilter: progid:DXImageTransform.Microsoft.gradient( startColorstr=\"#fba5a7\", endColorstr=\"#9dc3be\",GradientType=1 );\n\">\n<tbody>\n<tr>\n<td valign=\"top\">\n<table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" style=\"max-width:470px;width:100%;border-collapse:collapse;clear:both;margin:35px auto;background:rgba(255,255,255, 0.5);\" width=\"100%;\"><tbody><tr><td valign=\"top\">\n\n\n<tbody>\n<tr>\n<td valign=\"top\">\n<table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" style=\"max-width:440px;width:100%;border-collapse:collapse;clear:both;margin:15px auto;border:1px solid #e6e6e6;\" width=\"100%;\"><tbody><tr><td valign=\"top\" style=\"\">\n\n<div contenteditable=\"true\">\n<div style=\"display:block; margin:15px 0 0 0; padding:0; font-size:50px; text-align:center;line-height:100%; color:#050505;font-weight:400;\" align=\"center;\">48 HOUR SALE!</div>\n</div>\n\n<div contenteditable=\"true\">\n<div style=\"display:block; margin:10px 0 0 0; padding:0; font-size: 23px; color:#050505;font-weight:300;\" align=\"center\">EXCLUSIVELY FOR YOU</div>\n</div>\n\n<div contenteditable=\"true\"><div style=\"display:block; margin:10px 0 0px 0;font-size:23px;color:#ee562f;\" align=\"center\">ENDS TONIGHT</div></div>\n\n<hr style=\"background:#e4d8d5; height:1px; border:0; margin:15px 11px 5px 11px;\">\n\n<div contenteditable=\"true\"><div style=\"display:block; margin:20px 0 0px 0;font-size:31px;color:#050505;font-weight:400;\" align=\"center\"><b>15% OFF & 50 OR MORE</b></div></div>\n\n<div contenteditable=\"true\">\n<div style=\"display:block; margin:5px 0 0px 0;font-size:17px;color:#9b857f;\" align=\"center\">USED CODE: JUST4U</div>\n</div>\n\n<div contenteditable=\"true\" style=\"text-align:center;margin:30px 0 10px 0;\">\n<a href=\"#\" style=\"border-radius:0px; background-color:#ee562f; padding:6px 18px;text-align:center;text-decoration:none; color:#ffffff; display:inline-block;font-size:17px;border:2px solid #ce401b;font-weight:300;margin-bottom:2%;\" class=\"sm-button\">SHOP WOMENS</a> &nbsp;\n<a href=\"#\" style=\"border-radius:0px; background-color:#ee562f; padding:6px 30px;text-align:center;text-decoration:none; color:#ffffff; display:inline-block;font-size:17px;border:2px solid #ce401b;font-weight:300;margin-bottom:2%;\" class=\"sm-button\">SHOP MENS</a>\n</div>\n</td>\n</tr>\n</tbody>\n</table>\n</td>\n</tr>\n</tbody></td></tr></tbody>\n</table>\n</td>\n</tr>\n\n</tbody>\n</table>\n</td>\n</tr>\n\n</tbody>\n</table>\n\n\n</td>\n</tr>\n</tbody>\n</table>\n\n\n</td>\n</tr>\n</tbody>\n</table>\n\n\n</div>', 'template8.jpg', 1, 1470394007, '', 'active'),
(14, 'default', 'inline', 'Template 14', '<div style=\"width:100%; max-width:600px; margin: auto;\">\n\n\n<table align=\"center\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" height=\"100%\" width=\"100%\" style=\"border-collapse:collapse;height:100%;margin:0;padding:0;background-color:#e9e9e9; width:100%;max-width:600px; margin:0 auto;\">\n<tbody>\n<tr>\n<td align=\"center\" valign=\"top\" style=\"height:100%;margin:0;padding:0;width:100%;border-top:0\">\n\n<table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"100%\" style=\"border-collapse:collapse;width:100%;max-width:600px;\">\n<tbody>\n<tr>\n<td align=\"left\" valign=\"top\">\n\n<table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"600\" style=\"border-collapse:collapse; background:#fff;width:100%;max-width:600px;\">\n<tbody>\n<tr>\n<td valign=\"top\" align=\"center\">\n<div contenteditable=\"true\" style=\"text-align:center;\">\n<div style=\"display:block; padding:0; font-size:35px; color:#0085b1;margin:15px auto;font-weight:500;\">Converting Your Dreams Into<br>Reality Is Our Passion</div>\n</div></td></tr></tbody></table>\n                                    \n</td>\n</tr>\n\n<tr>\n<td align=\"center\" valign=\"top\">\n                                    \n<table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"100%\" style=\"border-collapse:collapse;border-top:0;border-bottom:0;width:100%;max-width:600px;\">\n<tbody><tr><td align=\"center\" valign=\"top\">\n                                                \n<table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"600\" style=\"border-collapse:collapse;width:100%;max-width:600px;\">\n<tbody><tr><td align=\"center\" valign=\"top\">\n\n<table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"100%\" style=\"border-collapse:collapse;background-color:#ffffff;border:0px solid #d5d5d5;width:100%;max-width:600px;\">\n<tbody><tr><td valign=\"top\">\n\n\n<table align=\"center\" width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" style=\"min-width:100%;border-collapse:collapse;width:100%;max-width:600px;background-color:#0085b1;\">\n<tbody><tr>\n\n<td valign=\"top\" align=\"center\" style=\"padding-top:0; padding-right:18px; padding-bottom:9px; padding-left:18px; word-break:break-word;\">\n<div contenteditable=\"true\">\n<div style=\"display:block;font-size:18px; color:#ffffff;margin:25px 21px 25px 21px;font-weight:300;line-height:25px;\">We constantly work with the sole objective of seeing you grow in every facet of business. Whether you are a newbie or an experienced business pro, we work to create a distinct entity for you from your competitors. Our dedicated team of experts are standing by your side always.</div>\n</div>\n\n<div contenteditable=\"true\">\n<div style=\"display:block; margin:15px 0 0 0; font-size:30px;color:#ffffff;\" align=\"center\">Checkout Our Exclusive<br>Business Oriented Services</div>\n</div>\n\n</td>\n</tr>\n</tbody></table>\n\n\n<table border=\"0\" cellpadding=\"15\" cellspacing=\"0\" style=\"width:100%;max-width:600px;min-width:100%;border-collapse:collapse; clear:both;text-align:center;background:#0085b1;\" width=\"100%\">\n<tbody>\n<tr>\n<td>\n\n\n<table border=\"0\" cellpadding=\"15\" cellspacing=\"0\" style=\"width:100%;max-width:600px;min-width:100%;border-collapse:collapse; clear:both;text-align:center;background:#0085b1;\" width=\"100%\">\n<tbody>\n\n<tr>\n<td>\n<div style=\"background:#fff;border-radius:5px;padding:10% 0;\">\n<img alt=\"\" src=\"{image_path}images/icon19.png\" tabindex=\"0\" style=\"border:1px solid #dbdbdb;border-radius:50%;width:82px;height:82px;margin:0 auto;max-width:82px;max-height:82px;\">\n<div contenteditable=\"true\">\n<div style=\"display:block; margin:15px 0 0px 0; font-size:16px;font-weight:400;color:#0085b1;\">Email Marketing</div>\n<div style=\"display:block;font-size:13px;color:#818080;font-weight:300;padding:5% 5% 5% 5%;\">Lorem ipsum dolor sit amet, no quodsi definitiones vis.</div> \n</div>\n</div>\n</td>\n\n<td>\n<div style=\"background:#fff;border-radius:5px;padding:10% 0;\">\n<img alt=\"\" src=\"{image_path}images/icon20.png\" tabindex=\"0\" style=\"border:1px solid #dbdbdb;border-radius:50%;width:82px;height:82px;margin:0 auto;max-width:82px;max-height:82px;\">\n<div contenteditable=\"true\">\n<div style=\"display:block; margin:15px 0 0px 0; font-size:16px;font-weight:400;color:#0085b1;\">Content Creation</div>\n<div style=\"display:block;font-size:13px;color:#818080;font-weight:300;padding:5% 5% 5% 5%;\">Lorem ipsum dolor sit amet, no quodsi definitiones vis.</div>\n</div>\n</div>\n</td>\n\n<td>\n<div style=\"background:#fff;border-radius:5px;padding:10% 0;\">\n<img alt=\"\" src=\"{image_path}images/icon21.png\" tabindex=\"0\" style=\"border:1px solid #dbdbdb;border-radius:50%;width:82px;height:82px;margin:0 auto;max-width:82px;max-height:82px;\">\n<div contenteditable=\"true\">\n<div style=\"display:block; margin:15px 0 0px 0; font-size:16px;font-weight:400;color:#0085b1;\">Data Analysis</div>\n<div style=\"display:block;font-size:13px;color:#818080;font-weight:300;padding:5% 5% 5% 5%;\">Lorem ipsum dolor sit amet, no quodsi definitiones vis.</div>\n</div>\n</div>\n</td>\n</tr>\n\n</tbody></table>\n</td>\n</tr>\n</tbody>\n</table>\n\n</td>\n</tr>\n</tbody>\n</table>\n\n<table border=\"0\" cellpadding=\"15\" cellspacing=\"0\" style=\"width:100%;max-width:600px;min-width:100%;border-collapse:collapse; clear:both;text-align:center;background:#f1f1f1;\" width=\"100%\">\n<tbody>\n\n<tr><td>\n<div contenteditable=\"true\">\n<div style=\"display:block;font-size:25px;color:#4a4949;margin:15px auto;\">Our Satisfied Consumers</div>\n</div>\n</td></tr>\n</tbody></table>\n\n\n<table border=\"0\" cellpadding=\"15\" cellspacing=\"0\" style=\"width:100%;max-width:600px;min-width:100%;border-collapse:collapse; clear:both;text-align:center;background:#f1f1f1;\" width=\"100%\">\n<tbody>\n<tr>\n<td>\n\n<table border=\"0\" cellpadding=\"15\" cellspacing=\"0\" style=\"width:100%;max-width:600px;min-width:100%;border-collapse:collapse; clear:both;text-align:center;background:#f1f1f1;\" width=\"100%\">\n<tbody>\n<tr>\n<td><img alt=\"\" src=\"{image_path}images/icon16.png\" tabindex=\"0\" style=\"margin:0 auto;border:1px solid #62b3ce;border-radius:50%;background:#fff;width: 115px;height: 115px;\"></td>\n<td><img alt=\"\" src=\"{image_path}images/icon17.png\" tabindex=\"0\" style=\"margin:0 auto;border:1px solid #62b3ce;border-radius:50%;background:#fff;width: 115px;height: 115px;\"></td>\n<td><img alt=\"\" src=\"{image_path}images/icon18.png\" tabindex=\"0\" style=\"margin:0 auto;border:1px solid #62b3ce;border-radius:50%;background:#fff;width: 115px;height: 115px;\"></td>\n</tr>\n</tbody>\n</table>\n</td>\n</tr>\n</tbody>\n</table>\n\n<table align=\"center\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" style=\"max-width:100%;min-width:100%;border-collapse:collapse; clear:both;width:100%;max-width:600px;background:#fff;\" width=\"100%\">\n<tbody><tr>\n<td valign=\"top\" style=\"padding-top:0; padding-right:14px; padding-bottom:9px; padding-left:14px;\">\n\n<div contenteditable=\"true\">\n<div style=\"display:block;font-size:15px; color:#818080;margin:25px 40px 0 40px;font-weight:300;line-height:25px;text-align:center;\">We don’t need to speak much about our work as our huge client base speaks volumes. Stop thinking now and contact us as you could be the next on this list of happy and satisfied businesses.</div>\n</div>\n\n<div contenteditable=\"true\" style=\"text-align:center;\">\n<a href=\"#\" style=\"border-radius:05px; background-color:#0085b1; padding: 2.5% 4%; text-align:center; text-decoration:none; color:#ffffff; display:inline-block;font-size:20px;margin:30px 0 30px 0;\" class=\"sm-button\">CONTACT OUR BUSINESS EXPERTS NOW</a></div>\n\n</td>\n</tr></tbody>\n</table>\n				\n\n\n</td>\n</tr>\n</tbody></table>\n</td>\n</tr>\n</tbody></table>\n</td>\n</tr>\n</tbody></table>\n</td>\n</tr>\n\n</tbody>\n</table>\n\n\n</div>', 'template14.jpg', 1, 1470394007, '', 'active');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_unsubscribe_list`
--

DROP TABLE IF EXISTS `tbl_unsubscribe_list`;
CREATE TABLE `tbl_unsubscribe_list` (
  `id` int(11) NOT NULL,
  `member_id` int(11) NOT NULL,
  `contact_id` int(11) NOT NULL,
  `email` varchar(255) NOT NULL,
  `ip` varchar(255) NOT NULL,
  `add_time` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_webform`
--

DROP TABLE IF EXISTS `tbl_webform`;
CREATE TABLE `tbl_webform` (
  `id` int(11) NOT NULL,
  `member_id` int(11) NOT NULL,
  `form_name` varchar(255) NOT NULL,
  `campaign_ids` varchar(255) NOT NULL,
  `template_id` int(11) DEFAULT NULL,
  `form_html` longtext NOT NULL,
  `status` enum('active','inactive') NOT NULL DEFAULT 'active',
  `add_time` int(11) NOT NULL,
  `thankyou_page` enum('default','custom') NOT NULL DEFAULT 'default',
  `thankyou_custom` text,
  `already_subscribe` enum('default','custom') NOT NULL DEFAULT 'default',
  `alreadysub_custom` text,
  `double_otp` enum('yes','no') NOT NULL DEFAULT 'no',
  `image` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_webform`
--

INSERT INTO `tbl_webform` (`id`, `member_id`, `form_name`, `campaign_ids`, `template_id`, `form_html`, `status`, `add_time`, `thankyou_page`, `thankyou_custom`, `already_subscribe`, `alreadysub_custom`, `double_otp`, `image`) VALUES
(1, 1, 'mysoretravels', '1', 1, '<div class=\"form1-bg\" id=\"demo-preview\">\n<div class=\"row\"> \n<div class=\"col-md-12 col-sm-12 col-xs-12\">\n<div class=\"col-md-12 col-sm-12 col-xs-12 form_area1\">\n<div class=\"mdem17 smem15 xsem12 blue text-center\" contenteditable=\"false\">Join the Best</div>\n<div class=\"mdem17 smem15 xsem12 blue text-center\" contenteditable=\"false\"><b>Mysore Travel Group</b>&nbsp;TODAY!</div>\n<div class=\"col-md-10 col-md-offset-1 col-sm-10 col-sm-offset-1 col-xs-10 col-xs-offset-1 mt7 xsmt5\">\n<div class=\"namefield\">\n<div class=\"mdem11 smem11 xsem10 blacktext w500\" contenteditable=\"false\">Name</div>\n<div class=\"mdem11 smem11 xsem10\"><input type=\"text\" name=\"name\" class=\"fieldinput\" placeholder=\"Name\" data-enpassid=\"__19\"></div>\n</div>\n<div class=\"copydiv\">\n<div class=\"mdem11 smem11 xsem10 blacktext mt2 xsmt4 w500 fieldname\" contenteditable=\"false\">Email Address</div>\n<div class=\"mdem12 smem11 xsem10\"><input type=\"text\" name=\"email\" class=\"fieldinput\" placeholder=\"Email\" data-enpassid=\"__20\"></div>\n</div>\n<div class=\"mdem12 smem12 xsem11 mt15 xsmt10 w400\">\n<a href=\"#\" class=\"form-btn1\" contenteditable=\"false\" id=\"rect\">Sign Up Today</a></div>\n</div>\n</div>\n</div>				\n</div>\n</div>', 'active', 1495219448, 'custom', 'http://mysoretravels.access3c.com/', 'custom', 'http://mysoretravels.access3c.com/', 'no', '11495220116.png'),
(2, 1, 'mysoretravels2', '1', 1, '<div class=\"form1-bg\" id=\"demo-preview\">\n<div class=\"row\"> \n<div class=\"col-md-12 col-sm-12 col-xs-12\">\n<div class=\"col-md-12 col-sm-12 col-xs-12 form_area1\">\n<div class=\"mdem17 smem15 xsem12 blue text-center\" contenteditable=\"false\">Subscribe to</div>\n<div class=\"mdem17 smem15 xsem12 blue text-center\" contenteditable=\"false\"><b>Mysore Travels</b>&nbsp;NewsLetter</div>\n<div class=\"col-md-10 col-md-offset-1 col-sm-10 col-sm-offset-1 col-xs-10 col-xs-offset-1 mt7 xsmt5\">\n<div class=\"namefield\">\n<div class=\"mdem11 smem11 xsem10 blacktext w500\" contenteditable=\"false\">Name</div>\n<div class=\"mdem11 smem11 xsem10\"><input type=\"text\" name=\"name\" class=\"fieldinput\" placeholder=\"Name\" data-enpassid=\"__19\"></div>\n</div>\n<div class=\"copydiv\">\n<div class=\"mdem11 smem11 xsem10 blacktext mt2 xsmt4 w500 fieldname\" contenteditable=\"false\">Email Address</div>\n<div class=\"mdem12 smem11 xsem10\"><input type=\"text\" name=\"email\" class=\"fieldinput\" placeholder=\"Email\" data-enpassid=\"__20\"></div>\n</div>\n<div class=\"mdem12 smem12 xsem11 mt15 xsmt10 w400\">\n<a href=\"#\" class=\"form-btn1\" contenteditable=\"false\" id=\"rect\" style=\"background-color: rgb(78, 141, 187);\">Sign Up Today</a></div>\n</div>\n</div>\n</div>				\n</div>\n</div>', 'active', 1495219865, 'default', NULL, 'default', NULL, 'no', '21502358422.png'),
(3, 1, 'LeaseOption Sellers', '2', 2, '<div class=\"form2-bg\" id=\"demo-preview\">\n<div class=\"row\"> \n<div class=\"col-md-12 col-sm-12 col-xs-12\">\n<div class=\"col-md-12 col-sm-12 col-xs-12 form_area2\">\n<div class=\"mdem17 smem15 xsem13 heading w500 text-center robotoslab\" contenteditable=\"false\">EXPERIENCE ADVANTURE <br> OF A LIFE TIME</div>\n<div class=\"mdem13 smem12 xsem12 preheading text-center w300 mt1 xsmt1\" contenteditable=\"false\">For discount, fill in the form</div>\n<div class=\"col-md-10 col-md-offset-1 col-sm-10 col-sm-offset-1 col-xs-10 col-xs-offset-1 mt9 xsmt10\">\n<div class=\"namefield\">\n<div class=\"mdem10 smem10 xsem10 whitetext w300\" contenteditable=\"false\">Full Name</div>\n<div class=\"mdem10 smem10 xsem10\"><input type=\"text\" name=\"name\" class=\"fieldinput\" placeholder=\"Name\" data-enpassid=\"__19\"></div>\n</div>\n<div class=\"copydiv\">\n<div class=\"mdem10 smem10 xsem10 whitetext mt2 xsmt4 w300 fieldname\" contenteditable=\"false\">Email Address</div>\n<div class=\"mdem10 smem10 xsem10\"><input type=\"text\" name=\"email\" class=\"fieldinput\" placeholder=\"Email\" data-enpassid=\"__20\"></div>\n</div><div class=\"statefield\">\n<div class=\"mdem10 smem10 xsem10 whitetext mt2 xsmt4 w300 fieldname\" contenteditable=\"false\">State</div>\n<div class=\"mdem10 smem10 xsem10\"><input type=\"text\" name=\"state\" class=\"fieldinput\" placeholder=\"State\" data-enpassid=\"__20\"></div>\n</div><div class=\"cityfield\">\n<div class=\"mdem10 smem10 xsem10 whitetext mt2 xsmt4 w300 fieldname\" contenteditable=\"false\">City</div>\n<div class=\"mdem10 smem10 xsem10\"><input type=\"text\" name=\"city\" class=\"fieldinput\" placeholder=\"City\" data-enpassid=\"__20\"></div>\n</div><div class=\"addressfield\">\n<div class=\"mdem10 smem10 xsem10 whitetext mt2 xsmt4 w300 fieldname\" contenteditable=\"false\">Address</div>\n<div class=\"mdem10 smem10 xsem10\"><input type=\"text\" name=\"address\" class=\"fieldinput\" placeholder=\"Address\" data-enpassid=\"__20\"></div>\n</div><div class=\"zipfield\">\n<div class=\"mdem10 smem10 xsem10 whitetext mt2 xsmt4 w300 fieldname\" contenteditable=\"false\">Zip</div>\n<div class=\"mdem10 smem10 xsem10\"><input type=\"text\" name=\"zip\" class=\"fieldinput\" placeholder=\"Zip\" data-enpassid=\"__20\"></div>\n</div><div class=\"phonefield\">\n<div class=\"mdem10 smem10 xsem10 whitetext mt2 xsmt4 w300 fieldname\" contenteditable=\"false\">Phone</div>\n<div class=\"mdem10 smem10 xsem10\"><input type=\"text\" name=\"phone\" class=\"fieldinput\" placeholder=\"Phone\" data-enpassid=\"__20\"></div>\n</div>\n<div class=\"mdem11 smem11 xsem11 mt10 xsmt10 w300\"><a href=\"#\" class=\"form-btn2\" contenteditable=\"false\" id=\"rect\">Send My Offer</a></div>\n</div>\n</div>\n</div>				\n</div>\n</div>', 'active', 1512844874, 'custom', 'http://tallahassee.fl.jackallenhomes.net/seller-details.html', 'custom', 'http://tallahassee.fl.jackallenhomes.net/seller-details.html', 'no', '31513676308.png'),
(4, 1, 'LO Buyers - Tallahassee FL', '3', 2, '<div class=\"form2-bg\" id=\"demo-preview\">\n<div class=\"row\"> \n<div class=\"col-md-12 col-sm-12 col-xs-12\">\n<div class=\"col-md-12 col-sm-12 col-xs-12 form_area2\">\n<div class=\"mdem17 smem15 xsem13 heading w500 text-center robotoslab\" contenteditable=\"false\">EXPERIENCE ADVANTURE <br> OF A LIFE TIME</div>\n<div class=\"mdem13 smem12 xsem12 preheading text-center w300 mt1 xsmt1\" contenteditable=\"false\">For discount, fill in the form</div>\n<div class=\"col-md-10 col-md-offset-1 col-sm-10 col-sm-offset-1 col-xs-10 col-xs-offset-1 mt9 xsmt10\">\n<div class=\"namefield\">\n<div class=\"mdem10 smem10 xsem10 whitetext w300\" contenteditable=\"false\">Full Name</div>\n<div class=\"mdem10 smem10 xsem10\"><input type=\"text\" name=\"name\" class=\"fieldinput\" placeholder=\"Name\"></div>\n</div>\n<div class=\"copydiv\">\n<div class=\"mdem10 smem10 xsem10 whitetext mt2 xsmt4 w300 fieldname\" contenteditable=\"false\">Email Address</div>\n<div class=\"mdem10 smem10 xsem10\"><input type=\"text\" name=\"email\" class=\"fieldinput\" placeholder=\"Email\"></div>\n</div>\n<div class=\"mdem11 smem11 xsem11 mt10 xsmt10 w300\"><a href=\"#\" class=\"form-btn2\" contenteditable=\"false\" id=\"rect\">Send My Offer</a></div>\n</div>\n</div>\n</div>				\n</div>\n</div>', 'active', 1512845264, 'custom', 'http://tallahassee.fl.jackallenhomes.net/thankyou.html', 'custom', 'http://tallahassee.fl.jackallenhomes.net/thankyou.html', 'no', '41513316254.png'),
(5, 1, 'Contact Us-Buyers and Sellers', '6', 2, '<div class=\"form2-bg\" id=\"demo-preview\">\n<div class=\"row\"> \n<div class=\"col-md-12 col-sm-12 col-xs-12\">\n<div class=\"col-md-12 col-sm-12 col-xs-12 form_area2\">\n<div class=\"mdem17 smem15 xsem13 heading w500 text-center robotoslab\" contenteditable=\"false\">EXPERIENCE ADVANTURE <br> OF A LIFE TIME</div>\n<div class=\"mdem13 smem12 xsem12 preheading text-center w300 mt1 xsmt1\" contenteditable=\"false\">For discount, fill in the form</div>\n<div class=\"col-md-10 col-md-offset-1 col-sm-10 col-sm-offset-1 col-xs-10 col-xs-offset-1 mt9 xsmt10\">\n<div class=\"namefield\">\n<div class=\"mdem10 smem10 xsem10 whitetext w300\" contenteditable=\"false\">Full Name</div>\n<div class=\"mdem10 smem10 xsem10\"><input type=\"text\" name=\"name\" class=\"fieldinput\" placeholder=\"Name\" data-enpassid=\"__19\"></div>\n</div>\n<div class=\"copydiv\">\n<div class=\"mdem10 smem10 xsem10 whitetext mt2 xsmt4 w300 fieldname\" contenteditable=\"false\">Email Address</div>\n<div class=\"mdem10 smem10 xsem10\"><input type=\"text\" name=\"email\" class=\"fieldinput\" placeholder=\"Email\" data-enpassid=\"__20\"></div>\n</div><div class=\"phonefield\">\n<div class=\"mdem10 smem10 xsem10 whitetext mt2 xsmt4 w300 fieldname\" contenteditable=\"false\">Phone</div>\n<div class=\"mdem10 smem10 xsem10\"><input type=\"text\" name=\"phone\" class=\"fieldinput\" placeholder=\"Phone\" data-enpassid=\"__20\"></div>\n</div>\n<div class=\"mdem11 smem11 xsem11 mt10 xsmt10 w300\"><a href=\"#\" class=\"form-btn2\" contenteditable=\"false\" id=\"rect\">Send My Offer</a></div>\n</div>\n</div>\n</div>				\n</div>\n</div>', 'active', 1513166699, 'default', NULL, 'default', NULL, 'no', '51513166699.png'),
(6, 1, 'LO Buyers - Bakersfield CA', '4', 2, '<div class=\"form2-bg\" id=\"demo-preview\">\n<div class=\"row\"> \n<div class=\"col-md-12 col-sm-12 col-xs-12\">\n<div class=\"col-md-12 col-sm-12 col-xs-12 form_area2\">\n<div class=\"mdem17 smem15 xsem13 heading w500 text-center robotoslab\" contenteditable=\"false\">EXPERIENCE ADVANTURE <br> OF A LIFE TIME</div>\n<div class=\"mdem13 smem12 xsem12 preheading text-center w300 mt1 xsmt1\" contenteditable=\"false\">For discount, fill in the form</div>\n<div class=\"col-md-10 col-md-offset-1 col-sm-10 col-sm-offset-1 col-xs-10 col-xs-offset-1 mt9 xsmt10\">\n<div class=\"namefield\">\n<div class=\"mdem10 smem10 xsem10 whitetext w300\" contenteditable=\"false\">Full Name</div>\n<div class=\"mdem10 smem10 xsem10\"><input type=\"text\" name=\"name\" class=\"fieldinput\" placeholder=\"Name\" data-enpassid=\"__19\"></div>\n</div>\n<div class=\"copydiv\">\n<div class=\"mdem10 smem10 xsem10 whitetext mt2 xsmt4 w300 fieldname\" contenteditable=\"false\">Email Address</div>\n<div class=\"mdem10 smem10 xsem10\"><input type=\"text\" name=\"email\" class=\"fieldinput\" placeholder=\"Email\" data-enpassid=\"__20\"></div>\n</div><div class=\"cityfield\">\n<div class=\"mdem10 smem10 xsem10 whitetext mt2 xsmt4 w300 fieldname\" contenteditable=\"false\">City</div>\n<div class=\"mdem10 smem10 xsem10\"><input type=\"text\" name=\"city\" class=\"fieldinput\" placeholder=\"City\" data-enpassid=\"__20\"></div>\n</div><div class=\"statefield\">\n<div class=\"mdem10 smem10 xsem10 whitetext mt2 xsmt4 w300 fieldname\" contenteditable=\"false\">State</div>\n<div class=\"mdem10 smem10 xsem10\"><input type=\"text\" name=\"state\" class=\"fieldinput\" placeholder=\"State\" data-enpassid=\"__20\"></div>\n</div><div class=\"addressfield\">\n<div class=\"mdem10 smem10 xsem10 whitetext mt2 xsmt4 w300 fieldname\" contenteditable=\"false\">Address</div>\n<div class=\"mdem10 smem10 xsem10\"><input type=\"text\" name=\"address\" class=\"fieldinput\" placeholder=\"Address\" data-enpassid=\"__20\"></div>\n</div><div class=\"zipfield\">\n<div class=\"mdem10 smem10 xsem10 whitetext mt2 xsmt4 w300 fieldname\" contenteditable=\"false\">Zip</div>\n<div class=\"mdem10 smem10 xsem10\"><input type=\"text\" name=\"zip\" class=\"fieldinput\" placeholder=\"Zip\" data-enpassid=\"__20\"></div>\n</div><div class=\"phonefield\">\n<div class=\"mdem10 smem10 xsem10 whitetext mt2 xsmt4 w300 fieldname\" contenteditable=\"false\">Phone</div>\n<div class=\"mdem10 smem10 xsem10\"><input type=\"text\" name=\"phone\" class=\"fieldinput\" placeholder=\"Phone\" data-enpassid=\"__20\"></div>\n</div>\n<div class=\"mdem11 smem11 xsem11 mt10 xsmt10 w300\"><a href=\"#\" class=\"form-btn2\" contenteditable=\"false\" id=\"rect\">Send My Offer</a></div>\n</div>\n</div>\n</div>				\n</div>\n</div>', 'active', 1513316147, 'custom', 'http://bakersfield.ca.jackallenhomes.net/thankyou.html', 'custom', 'http://bakersfield.ca.jackallenhomes.net/thankyou.html', 'no', '61513316147.png');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_website_settings`
--

DROP TABLE IF EXISTS `tbl_website_settings`;
CREATE TABLE `tbl_website_settings` (
  `id` int(11) NOT NULL,
  `member_id` int(11) NOT NULL,
  `footer_text` longtext NOT NULL,
  `application_URL` text NOT NULL,
  `database_name` varchar(250) NOT NULL,
  `license_key` text,
  `mail_type` varchar(255) NOT NULL DEFAULT 'default',
  `SMTP_hostname` text,
  `SMTP_username` text,
  `SMTP_password` text,
  `SMTP_port` text,
  `bounce_type` enum('default','custom') DEFAULT 'default',
  `bounce_address` text,
  `bounce_server` text,
  `bounce_password` text,
  `bounce_port` int(11) DEFAULT NULL,
  `account_type` text,
  `fb_url` varchar(255) DEFAULT NULL,
  `tw_url` varchar(255) DEFAULT NULL,
  `insta_url` varchar(255) DEFAULT NULL,
  `gplus_url` varchar(255) DEFAULT NULL,
  `last_mail_time` int(11) DEFAULT NULL,
  `current_hour_mail` int(11) DEFAULT NULL,
  `mail_hour_limit` int(11) DEFAULT NULL,
  `mail_minute_limit` int(11) DEFAULT '3'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_website_settings`
--

INSERT INTO `tbl_website_settings` (`id`, `member_id`, `footer_text`, `application_URL`, `database_name`, `license_key`, `mail_type`, `SMTP_hostname`, `SMTP_username`, `SMTP_password`, `SMTP_port`, `bounce_type`, `bounce_address`, `bounce_server`, `bounce_password`, `bounce_port`, `account_type`, `fb_url`, `tw_url`, `insta_url`, `gplus_url`, `last_mail_time`, `current_hour_mail`, `mail_hour_limit`, `mail_minute_limit`) VALUES
(1, 1, '<div style=\"width:600px;color:#4a4949; padding:15px 15px 10px 15px; margin:0 auto; height:auto; font-family: \'Roboto\', sans-serif; font-weight:400; font-size:13px;\">\n                        <div style=\"float:left; width:70%;\">\n                        <p style=\"margin:0; padding:0;\"><span style=\"position: relative;margin-top:-1px; padding-right:10px;float: left;\">Follow us </span>\n                        <a href=\"{#fb_link}\"><img src=\"http://mailzingo.access3c.com/assets/default/images/email_footer/images/fb.png\" /></a>\n                        <a href=\"{#tw_link}\"><img src=\"http://mailzingo.access3c.com/assets/default/images/email_footer/images/tw.png\" /></a>\n                        <a href=\"{#gplus_link}\"><img src=\"http://mailzingo.access3c.com/assets/default/images/email_footer/images/gplus.png\" /></a>\n                        <a href=\"{#in_link}\"><img src=\"http://mailzingo.access3c.com/assets/default/images/email_footer/images/in.png\" /></a></p>\n                        <p style=\"margin:0; padding-bottom:8px; \">{#address}</p>\n                        <p style=\"background:url(images/line.jpg) repeat-x top; width:auto; display:inline-block; font-size:11px; margin:0; padding:5px 0px 0px 0px;\"> Getting too many emails from us, click here to <a href=\"{#unsubscribe_link}\" style=\"color:#1155cc; \">Unsubscribe</a></p>\n                        {#open_link}\n                        </div>\n\n                        <div style=\"float:left; width:30%; text-align:right\">\n                        <p style=\"margin:0; padding:0px 0px 4px 0px; text-align:center; width:95px; display:inline-block;\">Powered by</p>\n                        <img src=\"{#logo_link}\" /></div>\n                        <div style=\"clear:both\"></div>\n                        </div>', 'http://mailzingo.access3c.com/', 'manjunat_mailzingo', '675e42-7496f7-6f9089-d9696e75', 'default', NULL, NULL, NULL, NULL, 'default', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 60, 3);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `ci_sessions`
--
ALTER TABLE `ci_sessions`
  ADD PRIMARY KEY (`session_id`),
  ADD KEY `last_activity_idx` (`last_activity`);

--
-- Indexes for table `tbl_autoresponder`
--
ALTER TABLE `tbl_autoresponder`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_campaign`
--
ALTER TABLE `tbl_campaign`
  ADD PRIMARY KEY (`campaign_id`);

--
-- Indexes for table `tbl_contact`
--
ALTER TABLE `tbl_contact`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_cron_urls`
--
ALTER TABLE `tbl_cron_urls`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_email_queue`
--
ALTER TABLE `tbl_email_queue`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_external_mail_api`
--
ALTER TABLE `tbl_external_mail_api`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_file_attatch`
--
ALTER TABLE `tbl_file_attatch`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_form_template`
--
ALTER TABLE `tbl_form_template`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_from_email`
--
ALTER TABLE `tbl_from_email`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_member`
--
ALTER TABLE `tbl_member`
  ADD PRIMARY KEY (`mem_id`);

--
-- Indexes for table `tbl_newsletter`
--
ALTER TABLE `tbl_newsletter`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_statistics`
--
ALTER TABLE `tbl_statistics`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_suppression`
--
ALTER TABLE `tbl_suppression`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_suppression_list`
--
ALTER TABLE `tbl_suppression_list`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_template`
--
ALTER TABLE `tbl_template`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_unsubscribe_list`
--
ALTER TABLE `tbl_unsubscribe_list`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_webform`
--
ALTER TABLE `tbl_webform`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_website_settings`
--
ALTER TABLE `tbl_website_settings`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tbl_autoresponder`
--
ALTER TABLE `tbl_autoresponder`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbl_campaign`
--
ALTER TABLE `tbl_campaign`
  MODIFY `campaign_id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `tbl_contact`
--
ALTER TABLE `tbl_contact`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=41;

--
-- AUTO_INCREMENT for table `tbl_cron_urls`
--
ALTER TABLE `tbl_cron_urls`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbl_email_queue`
--
ALTER TABLE `tbl_email_queue`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbl_external_mail_api`
--
ALTER TABLE `tbl_external_mail_api`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbl_file_attatch`
--
ALTER TABLE `tbl_file_attatch`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbl_form_template`
--
ALTER TABLE `tbl_form_template`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `tbl_from_email`
--
ALTER TABLE `tbl_from_email`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `tbl_member`
--
ALTER TABLE `tbl_member`
  MODIFY `mem_id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `tbl_newsletter`
--
ALTER TABLE `tbl_newsletter`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbl_statistics`
--
ALTER TABLE `tbl_statistics`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbl_suppression`
--
ALTER TABLE `tbl_suppression`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbl_suppression_list`
--
ALTER TABLE `tbl_suppression_list`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbl_template`
--
ALTER TABLE `tbl_template`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT for table `tbl_unsubscribe_list`
--
ALTER TABLE `tbl_unsubscribe_list`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbl_webform`
--
ALTER TABLE `tbl_webform`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `tbl_website_settings`
--
ALTER TABLE `tbl_website_settings`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
