Base Docker images for the [kickoff-docker-php](https://github.com/thecodingmachine/kickoff-docker-php/) project.

## PHP-FPM

### phpfpm-7.2.1-multi

| Name                       | Version                |
| -------------------------- | ---------------------- |
| Base image                 | `php:7.2.6-fpm-alpine` |
| APCu                       | `5.1.9`                |
| PHP extension for Redis    | `3.1.6`                |
| YAML                       | `2.0.2`                |
| Xdebug                     | `2.6.0`                |
| LibreSSL                   | `2.7.4-r2`             |
| Composer                   | `1.6.2`                |
| prestissimo                | `0.3.7`                |
| PHP Coding Standards Fixer | `2.10.0`               |
| NodeJS                     | `10.14.0-r0`           |
| yarn                       | `1.3.2-r0`             |

## Toolbox

### toolbox-alpine-3.7-multi

| Name       | Version      |
| ---------- | ------------ |
| Base image | `alpine:3.7` |
| LibreSSL   | `2.7.5-r0`   |

### Things to do

1. Add multi arch version of Ngnix and certbot
2. Update platform to use mutli-arch versions
   - https://hub.docker.com/r/arm64v8/docker
   - https://hub.docker.com/r/arm64v8/bash
   - https://hub.docker.com/r/arm64v8/rabbitmq
   -
