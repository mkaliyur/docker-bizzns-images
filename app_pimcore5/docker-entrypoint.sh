#!/bin/bash

echo "!!!!!!!!!!!!!!!!!!!!!!!===>>> inside app_pimcore5 entrypoint"

usermod -u $UID www-data;
chown -R www-data:www-data /tmp; 
chown -R www-data:www-data /var/www/html;


if [ ! /var/www/html/var/tmp/reinstall.flag ]; then
    unzip -oq /tmp/code/pimcore5/htdocs.zip -d /tmp/code/pimcore5
    cd /tmp/code/pimcore5/htdocs
    cp -R . /var/www/html  
    cd /var/www/html
    rm -rf /tmp/code/pimcore5/htdocs
    unzip -oq /tmp/code/pimcore5/vendor.zip -d /var/www/html
    touch /var/www/html/var/tmp/reinstall.flag
fi
echo "===>>> Done Extracting htodcs and vendor to /var/ww/html"

echo "===>>> Copying system.php"
cp /tmp/config/system.php /var/www/html/var/config/system.php 
cp /tmp/config/config.yml /var/www/html/app/config/config.yml
cp /tmp/config/rabbitmq_config.yml /var/www/html/app/config/rabbitmq_config.yml
 
/tmp/utils/wait-for-it.sh mysql:3306 -s -t 30 -- echo "mysql db is up!!!!!"
/tmp/utils/wait-for-it.sh rabbitmq:5672 -s -t 30 -- echo "rabbitmq is up!!!!!"

echo "Resetting admin credentials"
php /var/www/html/bin/console reset-password  -p pass root
# The following packages were automatically installed and are no longer required:
# libaio1 libnuma1 mysql-client-5.7 mysql-client-core-5.7 mysql-common
# apt-get purge mysql-client libaio1 libnuma1 mysql-client-5.7 mysql-client-core-5.7 mysql-common
# apk autoremove
echo "############################################################################"
echo "pimcore database for sample data installed, admin user: root password: pass"
echo "############################################################################"
sleep 10
su www-data





sedi()
{
    sed --version >/dev/null 2>&1 && sed -i -- "$@" || sed -i "" "$@";
}



if [ "$XDEBUG_ENABLED" == "false" ]; then
    sedi "s/\zend_extension/;zend_extension/g" /usr/local/etc/php/conf.d/docker-php-ext-xdebug.ini;
else
    export XDEBUG_CONFIG="remote_host=$XDEBUG_REMOTE_HOST";
fi;

 








exec php-fpm;