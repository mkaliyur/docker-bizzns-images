<?php

namespace OldSound\RabbitMqBundle\RabbitMq;

use PhpAmqpLib\Message\AMQPMessage;
use PhpAmqpLib\Wire\AMQPTable;

/**
 * Producer, that publishes AMQP Messages
 */
class Producer extends BaseAmqp implements ProducerInterface
{
    protected $contentType = 'text/plain';
    protected $deliveryMode = 2;

    public function setContentType($contentType)
    {
        $this->contentType = $contentType;

        return $this;
    }

    public function setDeliveryMode($deliveryMode)
    {
        $this->deliveryMode = $deliveryMode;

        return $this;
    }

    protected function getBasicProperties()
    {
        return array('content_type' => $this->contentType, 'delivery_mode' => $this->deliveryMode);
    }

    /**
     * Publishes the message and merges additional properties with basic properties
     *
     * @param string $msgBody
     * @param string $routingKey
     * @param array $additionalProperties
     * @param array $headers
     */
    public function publish($msgBody, $routingKey = '', $additionalProperties = array(), array $headers = null)
    {
        $results2 = print_r($msgBody, true);
        \Pimcore\Log\Simple::log("event.log", 'Inside RabbitMq Producer msgBody  = '.$results2);

        $results2 = print_r($routingKey, true);
        \Pimcore\Log\Simple::log("event.log", 'Inside RabbitMq Producer Routing Key = '.$results2);

        if ($this->autoSetupFabric) {
            $this->setupFabric();
        }

        $msg = new AMQPMessage((string) $msgBody, array_merge($this->getBasicProperties(), $additionalProperties));

        if (!empty($headers)) {
            $headersTable = new AMQPTable($headers);
            $msg->set('application_headers', $headersTable);
        }

        $this->getChannel()->basic_publish($msg, $this->exchangeOptions['name'], (string)$routingKey);

        $results2 = print_r($this->exchangeOptions, true);
        \Pimcore\Log\Simple::log("event.log", 'Inside RabbitMq Producer :: after basic publish:: exchangeOptions  = '.$results2);

        $array_log = array(
            'body' => $msgBody,
            //$results2 = print_r($msgBody, true);
            //\Pimcore\Log\Simple::log("event.log", 'Inside RabbitMq Producer Inside Array msgBody  = '.$results2);

            'routingkeys' => $routingKey,
            //$results2 = print_r($routingKey, true);
            //\Pimcore\Log\Simple::log("event.log", 'Inside RabbitMq Producer Inside Array routingKey  = '.$results2);

            'properties' => $additionalProperties,
            //$results2 = print_r($additionalProperties, true);
            //\Pimcore\Log\Simple::log("event.log", 'Inside RabbitMq Producer Inside Array additionalProperties  = '.$results2);

            'headers' => $headers
            //$results2 = print_r($headers, true);
            //\Pimcore\Log\Simple::log("event.log", 'Inside RabbitMq Producer Inside Array headers  = '.$results2);

        );


        $results2 = print_r($array_log, true);
        \Pimcore\Log\Simple::log("event.log", 'Inside RabbitMq Producer after publish:: array_log  = '.$results2);

        $this->logger->debug('AMQP message published', array(
            'amqp' => array(
                'body' => $msgBody,
                'routingkeys' => $routingKey,
                'properties' => $additionalProperties,
                'headers' => $headers
            )
        ));
    }
}
