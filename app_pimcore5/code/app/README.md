# Pimcore 5
THE OPEN-SOURCE ENTERPRISE CMS/CMF, PIM, DAM, ECOMMERCE-SUITE  

**Looking for the previous stable (version 4)?  
See https://github.com/pimcore/pimcore/tree/pimcore4**

[![Software License](https://img.shields.io/badge/license-GPLv3-brightgreen.svg?style=flat)](LICENSE.md)
[![Gitter](https://img.shields.io/badge/gitter-join%20chat-brightgreen.svg?style=flat)](https://gitter.im/pimcore/pimcore?utm_source=badge&utm_medium=badge&utm_campaign=pr-badge)

* [Documentation](https://pimcore.com/docs/)
* [API Documentation](https://pimcore.com/docs/api/)
* Homepage: [http://pimcore.com/](https://pimcore.com/) - Learn more about Pimcore
* Like us on [Facebook](https://www.facebook.com/pimcore)
* Twitter: [@pimcore](https://twitter.com/pimcore) - Get the latest news
* Issue Tracker: - [Issues](https://github.com/pimcore/pimcore/issues) - Report bugs here
* Help [translating](https://poeditor.com/join/project/VWmZyvFVMH) Pimcore
* [Forums](https://talk.pimcore.org/) - Discussions


## Contribute

Please have a look at our [contributing guide](CONTRIBUTING.md).

#### Short summary:  
**Bug fixes:** please create a pull request including a step by step description to reproduce the problem  
**Contribute features:** contact the core-team before you start developing (contribute@pimcore.org)  
**Security vulnerabilities:** please contact us (security@pimcore.org)  

## Copyright and license
Copyright: [Pimcore](http://www.pimcore.org) GmbH  
For licensing details please visit [LICENSE.md](LICENSE.md)

## Command to create solution
**Run this Command from C:\Bitnami\pimcore-5.1.3-0\apps\pimcore\htdocs\bin**
php ./console --env=prod  bizzns:create-solution build mys build_mys
php ./console --env=prod  bizzns:copy-master-part1 build mys build_mys
php ./console --env=prod  bizzns:copy-master-part2 build mys build_mys
php ./console --env=prod  bizzns:copy-solution-content-master-part1 build mys build_mys
php ./console --env=prod  bizzns:copy-solution-content-master-part2 build mys build_mys
php ./console --env=prod  bizzns:create-merchant-part1 build mys build_mys srinidhi

**Command to copy Images to Public C:\KPLM\projects\compiled-templates\static-final-builders>linkimages.bat"
**Added a new public function getByNameAndType**
Need Documentation

**Pimcore Upgradation from 5.1 to 5.4**
 php ./console update -u299

**Changed C:\Bitnami\pimcore-5.1.3-0\apps\pimcore\htdocs\vendor\pimcore\pimcore\models\service.php on $this->_user**
