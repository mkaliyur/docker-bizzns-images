<?php
    use Pimcore\Tool;
    use Pimcore\Model\Webservice;
?>




<?php if($this->editmode) { ?>
	<h2>No Admin UI.. will retrieve promoted (on home page? checked)  blog object list</h2>
<?php } ?>




<?php if( !$this->editmode &&  (Tool::isFrontend() || Tool::isFrontentRequestByAdmin() )) {?>
	<?php
        $pagedata = $this->pagedata;
        if (! isset($pagedata)) {
            $pagedata = new \stdClass();
        }
        $pagedata->latestBlogs = [];

        $article = DataObject\BzBlogArticle::getById($blog_id);
        $response = new \stdClass();

        $response->indexing_metadata = new \stdClass();

        $type =  $article->getObject_type();
        $solution_name =  $article->getSolution_name();
        $id = $article->getId();

        $response->indexing_metadata->type = new \stdClass();
        if(!is_null($type)){
        $response->indexing_metadata->type->_index = $solution_name;
        $response->indexing_metadata->type->_type = $type;
        $response->indexing_metadata->type->_id = $id;
        }else{
          \Pimcore\Log\Simple::log("event.log", 'Inside Blog Controller :: Beginning of else =');
          $response->indexing_metadata->type = new \stdClass();
        }


        function getLatestBlogs($locale = 'en'){
         $promotedBlogList = new Object\blogArticle\Listing();
         if($locale === null){
          $promotedBlogList->setLocale($locale);
         }
         $promotedBlogList->setOrderKey( "date" );
         $promotedBlogList->setorder("desc");
         //$promotedBlogList->setCondition("isPromotedToHomePage = ?",true);
         $promotedBlogList->setLimit(10);
         $ret = $promotedBlogList->getObjects();
         return $ret;
        }

        // echo getBySlug('test-news')->getKey(). "\n";




        $promotedBlogs= getLatestBlogs();


         foreach($promotedBlogs as $promotedBlog) {
            $response = new \stdClass();
            $response->title = new \stdClass();
            $response->title->rendered = new \stdClass();
            $response->name=$promotedBlog->o_key;
            $blogItems = $promotedBlog->localizedfields->items;

            foreach($blogItems as $key => $value) {
                $keyvalue = new \stdClass();
                $keyvalue->key = $key;
                $keyvalue->value = $value;
                //array_push($response->keys, $keyvalue);
                if($key == 'en'){
                    //$text = new \stdClass();
                   // $text = $value->text;
                    foreach($value as $key1 => $value1) {
                        $keyvalue1 = new \stdClass();
                        $keyvalue1->key = $key1;
                        $keyvalue1->value = $value1;
                        //array_push($response->keys1, $keyvalue1);

                        if($key1 == 'title'){
                            $response->title->rendered=$value1;
                        }

                        if($key1 == 'briefSummary'){
                            $response->briefSummary = $value1;
                        }

                        if($key1 == 'headline'){
                            $response->headline = $value1;
                        }

                        if($key1 == 'slug'){
                            $response->slug = $value1;
                        }

                    }

                }
            }

            $response->date = $promotedBlog->date;
            $response->author = $promotedBlog->author;
            array_push($pagedata->latestBlogs, $response);
        }



     ?>

<?php } ?>
