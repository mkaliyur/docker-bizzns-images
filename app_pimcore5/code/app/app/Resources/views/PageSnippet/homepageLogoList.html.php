<?php
    use Pimcore\Tool;
    use Pimcore\Model\Webservice;
    use Pimcore\Model\DataObject\BzListingBuilders\Listing;
?>




<?php if($this->editmode) { ?>
  <section class="layout">
    <?=
      $this->href("objectPath", [
      "types" => ["object"],
        "subtypes" => [
            "object" => ["object"]
         ],
         "classes" => ["BzLayout"]
    ]);
    ?>
  </section>
<?php } ?>




<?php if( !$this->editmode &&  (Tool::isFrontend() || Tool::isFrontentRequestByAdmin() )) {?>
	<?php
        $pagedata = $this->getParam('page_data');
        if (! isset($pagedata)) {
            $pagedata = new \stdClass();
        }
        $pagedata->homepageLogoList = [];
        $container = $pagedata;


        function getListingsByDisplayOnHomePageFlag($locale = 'en'){
         $promotedListingList = new Listing();
         $promotedListingList->setCondition("displayOnHomePage = ?",true);
         //$promotedListingList->setLimit(9);
         $ret = $promotedListingList->getObjects();
         return $ret;
        }



        if (!function_exists('createSlug')) {
            function createSlug($str, $delimiter = '-'){
                $slug = strtolower(trim(preg_replace('/[\s-]+/', $delimiter, preg_replace('/[^A-Za-z0-9-]+/', $delimiter, preg_replace('/[&]/', 'and', preg_replace('/[\']/', '', iconv('UTF-8', 'ASCII//TRANSLIT', $str))))), $delimiter));
                return $slug;

            }
        }

        // echo getBySlug('test-news')->getKey(). "\n";
        if (!function_exists('getImageDetails')) {
            function getImageDetails($server_base,$image){
                    $ret = new \stdClass();
                    $ret->type = $image->type;
                    $ret->id = $image->id;
                    $ret->parentId = $image->parentId;
                    $ret->parent= $image->parent;
                    $ret->filename = $image->filename;
                    $ret->path = $image->path;
                    $ret->mimetype = $image->mimetype;
                    $serverPath = $server_base;
                    $ret->guid = $serverPath.$image->path.$image->filename;
                    return $ret;
            }
        }





        $promotedListings= getListingsByDisplayOnHomePageFlag();

        foreach($promotedListings as $listing) {
            $response = new \stdClass();
            if (!$listing->name){
                $response->name = $listing->headline;
            }else{
                $response->name=$listing->name;
            }

            $response->title=new \stdClass();
            $response->title->rendered=$response->name;
            $response->slug = createSlug($response->name);
            $response->domain = $listing->domain->Name;
            $response->datatype = $listing->datatype->name;
            $response->headline = $listing->headline;



            $response->logo_image = new \stdClass();
            if(!empty($listing->logo_image)){
                $image=$listing->logo_image;
                $server_base = $this->document->getProperty('server_base');
                $response->logo_image = getImageDetails($server_base,$image);
            }
            $response->logo_tagline = $listing->logo_tagline;


            array_push($pagedata->homepageLogoList, $response);
        }


     ?>

     <?php
     $objectPath_isempty = $this->href("objectPath")->isEmpty();
     if(!$objectPath_isempty){
       $object_path = $this->href("objectPath")->getFullPath();
       $this->layoutHelper($container,$object_path);
     }else{
       $container->Layout = new \stdClass();
     }
    ?>

<?php } ?>
