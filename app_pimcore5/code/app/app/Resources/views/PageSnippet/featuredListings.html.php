<?php
    use Pimcore\Tool;
    use Pimcore\Model\Webservice;
?>

<?php if($this->editmode) { ?>
	<h2>No Admin UI.. will retrieve featured Listings object list</h2>
<?php } ?>

<?php if( !$this->editmode &&  (Tool::isFrontend() || Tool::isFrontentRequestByAdmin() )) {?>
	<?php
        $pagedata = $this->getParam('page_data');
        if (! isset($pagedata)) {
            $pagedata = new \stdClass();
        }
        $pagedata->featutedListings = [];

     ?>
     <?php
        $this->encoder = new Webservice\JsonEncoder();
        $encodedresults = $this->encoder->encode($pagedata);
     ?>

<?php } ?>
