<?php
    use Pimcore\Tool;
    use Pimcore\Model\Webservice;
    use Pimcore\Model\DataObject;
?>

<?php if($this->editmode) { ?>
	<h2>This component will retrieve menu item object tree given a path</h2>

    <style type="text/css">
        .area-contentblock {
            margin-top: 60px;
        }
    </style>

    <section class="area-contentblock" style="margin-top:30px;">
        <h4>Enter menu object path to be pulled in</h4>
        <div>
            <label>Enter menu object Name here</label>
            <div><?= $this->input("menu_name"); ?></div>
        </div>
        <div class="alert alert-info">
          <h2>Drag and drop Menu Object Path here</h2>
            <?=
              $this->href("menuobjectPath", [
              "types" => ["object"],
                "subtypes" => [
                    "object" => ["object"]
                 ],
                 "classes" => ["BzMenuItem"]
            ]);
            ?>
        </div>
    </section>


<?php } ?>

<?php if( !$this->editmode &&  (Tool::isFrontend() || Tool::isFrontentRequestByAdmin() )) {?>
	<?php
        $pagedata = $this->getParam('page_data');

        if (! isset($pagedata)) {
            $pagedata = new \stdClass();
        }


        $containerName = $this->getParam("container");

        if( isset($containerName)){
            if (!isset($pagedata->$containerName))
            {
                $container = $pagedata->$containerName = new \stdClass();
            }else{
                $container = $pagedata->$containerName;
            }
        }else{
            $container = $pagedata;
        }




        $menuname = $this->input("menu_name");
        $menuname =preg_replace('/\s+/', '', $menuname);
        $container->$menuname = [];

        $menupath = $this->href("menuobjectPath");
        //$menupath = $this->input("menupath");

        //$categories = Object\bzCategories::getList();
        $menuitems = DataObject::getByPath($menupath)->getChilds();
        //$menuitems = DataObject::getByPath($menupath);

        // $ret = $menuitems ;
        // array_push($container->$menuname,$ret);


        if (!function_exists('createSlug')) {
            function createSlug($str, $delimiter = '-'){
                $slug = strtolower(trim(preg_replace('/[\s-]+/', $delimiter, preg_replace('/[^A-Za-z0-9-]+/', $delimiter, preg_replace('/[&]/', 'and', preg_replace('/[\']/', '', iconv('UTF-8', 'ASCII//TRANSLIT', $str))))), $delimiter));
                return $slug;
            }
        }


        if (!function_exists('getMenuChildElements')){
          //\Pimcore\Log\Simple::log("event.log", "page-snippet::menu-object:: Trying to create function getMenuChildElements" );
            function getMenuChildElements($element){
            	$ret = new \stdClass();
            	$ret->name = $element->getName();
                //$ret->element = $element;
                $ret->slug = createSlug($ret->name);
            	$ret->type = $element->o_type;
            	if($ret->type=='folder'){
            		$ret->name=$element->o_key;
            	}


            	$ret->o_id = $element->o_id;
            	$ret->o_parentId = $element->o_parentId;
            	$ret->o_path = $element->o_path;
                $ret->targetUrl = $element->getTargetURL();
                $ret->hasChilds = $element->o_hasChilds;
                $ret->element = $element;

                // if (isset($element->o_hasChilds)){
                    $children = $element->getChilds();
                    if(sizeof($children) > 0){
                        $ret->children = [];
                        foreach( (array) $children as $child_element ){
                            $retchild = getMenuChildElements($child_element);
                            array_push($ret->children,$retchild);
                            $ret->count = 0;
                            foreach ((array)$ret->children as $c ){
                                $ret->count = $ret->count + $c->count;
                            }
                        }
                    }
                // }
                //\Pimcore\Log\Simple::log("event.log", "page-snippet::menu-object:: Returning from func getMenuChildElements " );
            	return $ret;
            }
        }


        foreach( (array) $menuitems as $menuitem ){
        	$ret = getMenuChildElements($menuitem);
        	array_push($container->$menuname,$ret);
        }

     ?>

<?php } ?>
