<?php
    use Pimcore\Tool;
    use Pimcore\Model\Webservice;
?>

<?php if($this->editmode) { ?>
  <style type="text/css">
        .area-extvideo {
            margin-top: 60px;
        }
    </style>


  <section class="area-extvideo">
    <h4>Enter Youtube Video URL</h4>
    <div><?= $this->input("videourl"); ?></div>
  </section>
<?php } ?>


<?php if( !$this->editmode &&   (Tool::isFrontend()  || Tool::isFrontentRequestByAdmin() )) { ?>
<?php
  $pagedata = $this->getParam('page_data');
  if (!isset($pagedata)) {
            $pagedata = new \stdClass();
  }
  $pagedata->extvideo = new \stdClass();
  $youtubeUrl =  $this->input("videourl")->getData();
  $pagedata->extvideo->url = $youtubeUrl;
?>

<?php } ?>
