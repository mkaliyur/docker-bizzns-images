<?php
    use Pimcore\Tool;
    use Pimcore\Model\Webservice;
    use Pimcore\Model\DataObject;
?>

<?php if($this->editmode) { ?>
	<h2>This component will retrieve menu item object tree given a path</h2>

  <div class="alert alert-info">
    <h2>Drag and drop bzLayout Objectpath here</h2>
      <?=
        $this->href("objectPath", [
        "types" => ["object"],
          "subtypes" => [
              "object" => ["object"]
           ],
           "classes" => ["BzLayout"]
      ]);
  ?>
  </div>

<?php } ?>

<?php if( !$this->editmode &&  (Tool::isFrontend() || Tool::isFrontentRequestByAdmin() )) {?>
	<?php
        $pagedata = $this->getParam('page_data');
        if (! isset($pagedata)) {
            $pagedata = new \stdClass();
        }

        $containerName = null;
        if( isset($containerName)){
            if (!isset($pagedata->$containerName)){
                $container = $pagedata->$containerName = new \stdClass();
            }else{
                $container = $pagedata->$containerName;
            }
        }else{
            $container = $pagedata;
        }

        $container->Layout = new \stdClass();

         $objectPath_isempty = $this->href("objectPath")->isEmpty();
         if(!$objectPath_isempty){
           $object_path = $this->href("objectPath")->getFullPath();
         }
         $layout = DataObject::getByPath($object_path);

         $container->Layout->name = $layout->name;
         $container->Layout->selector = $layout->selector;

         $container->Layout->settings = [];
         if( !empty($layout->getSettings()) &&   !empty($layout->getSettings()->getItems() )){
           foreach( (array) $layout->getSettings()->items as $settings_item ){
             $ret = new \stdClass();
             $ret->name = $settings_item->setting_name;
             $ret->value = $settings_item->setting_value;
             array_push($container->Layout->settings,$ret);
           }
        }


  ?>

  <?php } ?>
