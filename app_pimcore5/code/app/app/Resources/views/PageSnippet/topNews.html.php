<?php
    use Pimcore\Tool;
    use Pimcore\Model\Webservice;
    use Pimcore\Model\DataObject\BzNews\Listing;

?>

<?php if($this->editmode) { ?>
	<h2>No Admin UI.. will retrieve top News object list</h2>
<?php } ?>


<?php if( !$this->editmode &&  (Tool::isFrontend() || Tool::isFrontentRequestByAdmin() )) {?>
	<?php
        $pagedata = $this->getParam('page_data');
        if (! isset($pagedata)) {
            $pagedata = new \stdClass();
        }
        $pagedata->topNews = [];

        function getTopNews($locale = 'en'){
         $topNewsList = new Listing();
         $topNewsList->setCondition("topnews = ?",true);
         $topNewsList->setLimit(9);
         $ret = $topNewsList->getObjects();
         return $ret;
        }

        if (!function_exists('createSlug')) {
            function createSlug($str, $delimiter = '-'){
                $slug = strtolower(trim(preg_replace('/[\s-]+/', $delimiter, preg_replace('/[^A-Za-z0-9-]+/', $delimiter, preg_replace('/[&]/', 'and', preg_replace('/[\']/', '', iconv('UTF-8', 'ASCII//TRANSLIT', $str))))), $delimiter));
                return $slug;

            }
        }

        if (!function_exists('getImageDetails')){
            function getImageDetails($server_base,$image){
                    $ret = new \stdClass();
                    $ret->type = $image->type;
                    $ret->id = $image->id;
                    $ret->parentId = $image->parentId;
                    $ret->parent= $image->parent;
                    $ret->filename = $image->filename;
                    $ret->path = $image->path;
                    $ret->mime_type = $image->mimetype;
                    $ret->post_mime_type = $image->mimetype;
                    $serverPath = $server_base;
                    $ret->guid = $serverPath.$image->path.$image->filename;
                    return $ret;
            }
        }

        $topNewsItems= getTopNews();

        foreach($topNewsItems as $newsItem) {

            $response = new \stdClass();
            $response->name=$newsItem->o_key;
            $response->title=new \stdClass();
            $response->title->rendered = new \stdClass();
            //$response->slug = $response->name;
            //$response->newsItem = $newsItem;
            $response->date = $newsItem->date;
            if( !isset($response->slug)){
                $response->slug = createSlug($newsItem->o_key);
            }

            $newsItems = $newsItem->localizedfields->items;

            foreach($newsItems as $key => $value) {
                $keyvalue = new \stdClass();
                $keyvalue->key = $key;
                $keyvalue->value = $value;
                if($key == 'en'){
                    //$text = new \stdClass();
                   // $text = $value->text;
                    foreach($value as $key1 => $value1) {
                        $keyvalue1 = new \stdClass();
                        $keyvalue1->key = $key1;
                        $keyvalue1->value = $value1;
                        //array_push($response->keys1, $keyvalue1);

                        if($key1 == 'headline'){
                            $response->headline = $value1;
                        }

                        if($key1 == 'title'){
                            $response->title->rendered=$value1;
                            $response->slug = createSlug($response->title->rendered);
                        }

                        if($key1 == 'text'){
                            $response->content = $value1;
                        }

                        if($key1 == 'tags'){
                            array_push($response->tagnames,$value1);
                        }

                        if($key1 == 'topnews'){
                            $response->isPromotedToHomePage- $value1;
                        }

                        if($key1 == 'shortText'){
                            $response->shortText=$value1;
                        }
                    }

                }
            }
            array_push($pagedata->topNews, $response);
        }

        $response->image1 = new \stdClass();
        if(!empty ($newsItem->image_1) ){
            $image1 = $newsItem->image_1;
            $server_base = $this->document->getProperty('server_base');
            $ret = getImageDetails($server_base,$image1);
            $response->image1 = $ret;
        }


     ?>
     <?php
      $this->encoder = new Webservice\JsonEncoder();
      $encodedresults = $this->encoder->encode($pagedata);
    ?>


<?php } ?>
