<?php
    use Pimcore\Tool;
    use Pimcore\Model\Webservice;
    use Pimcore\Model\DataObject;
?>


<?php if($this->editmode) { ?>
    <div class="alert alert-info">
      <h2>Drag and drop BzTags path here</h2>
        <?=
          $this->href("blogtagsPath", [
          "types" => ["object"],
            "subtypes" => [
                "object" => ["object"]
             ],
             "classes" => ["BzTags"]
        ]);
    ?>
    </div>
    <section>
      <h2>Drag and drop BzLayout path here</h2>
        <?=
          $this->href("bzlayoutPath", [
          "types" => ["object"],
            "subtypes" => [
                "object" => ["object"]
             ],
             "classes" => ["BzLayout"]
        ]);
    ?>
   </section>


<?php } ?>




<?php if( !$this->editmode &&  (Tool::isFrontend() || Tool::isFrontentRequestByAdmin() )) {?>
    <?php
        $pagedata = $this->getParam('page_data');

        if (! isset($pagedata)) {
            $pagedata = new \stdClass();
        }


        //$containerName = $this->getParam("container");
        $containerName = null;
        if( isset($containerName)){
            if (!isset($pagedata->$containerName)){
                $container = $pagedata->$containerName = new \stdClass();
            }else{
                $container = $pagedata->$containerName;
            }
        }else{
            $container = $pagedata;
        }



        $container->blogTags= new \stdClass();
        $container->blogTags->data = [];
        $container->blogTags->Layout= new \stdClass();

        $blogtagsPath_isempty = $this->href("blogtagsPath")->isEmpty();
        if(!$blogtagsPath_isempty){
        $blogtags_path = $this->href("blogtagsPath")->getFullPath();

       //$categories = DataObject\bzCategories::getList();
       $tags = DataObject::getByPath($blogtags_path)->getChilds();

        if (!function_exists('createSlug')) {
                function createSlug($str, $delimiter = '-'){
                    $slug = strtolower(trim(preg_replace('/[\s-]+/', $delimiter, preg_replace('/[^A-Za-z0-9-]+/', $delimiter, preg_replace('/[&]/', 'and', preg_replace('/[\']/', '', iconv('UTF-8', 'ASCII//TRANSLIT', $str))))), $delimiter));
                    return $slug;
                }
        }


        if (!function_exists("getBlogTagReferenceCounts")){
            function getBlogTagReferenceCounts($id){
                $ret_count = 0;
                $blogsList = new DataObject\BlogArticle\Listing();
                $conditions = [];
                $conditions[] = "tags LIKE '%|".(int)$id. ",%'";
                //$conditions[] = "headline LIKE '%"."H"."%'";
                //\Pimcore\Log\Simple::log("event.log", 'Inside view of blogTags::Inside getReferenceCounts int id= '.$id );


                if (!empty($conditions)) {
                     $blogsList->setCondition(implode(" AND ", $conditions));
                }

                //$objects = $listingsList->load();

                //echo $listingsList;

                $ret_count = $blogsList->count();

                return $ret_count;
            }
        }

        if (!function_exists("getBlogTagsChildElements")){
            function getBlogTagsChildElements($element){
                $ret = new \stdClass();
                $ret->name = $element->name;
                $ret->slug = createSlug($ret->name);
                $ret->type = $element->o_type;
                if($ret->type=='folder'){
                    $ret->name=$element->o_key;
                }


                $ret->o_id = $element->o_id;
                $ret->o_parentId = $element->o_parentId;
                $ret->o_path = $element->o_path;

                $ret->count = getBlogTagReferenceCounts($element->o_id);
                //$ret->cat_orig = $element;

                $children = $element->getChilds();

                if(sizeof($children) > 0){
                    $ret->children = [];
                    foreach( (array) $children as $child_element ){
                        $retchild = getBlogTagsChildElements($child_element);
                        array_push($ret->children,$retchild);
                        $ret->count = 0;
                        foreach ((array)$ret->children as $c ){
                            $ret->count = $ret->count + $c->count;
                        }
                    }
                }
                return $ret;
            }
        }


        foreach( (array) $tags as $tag ){
            $ret = getBlogTagsChildElements($tag);
            array_push($container->blogTags->data,$ret);
        }
      }

     ?>

     <?php
     $bzlayoutPath_isempty = $this->href("bzlayoutPath")->isEmpty();
     if(!$bzlayoutPath_isempty){
     $bzlayout_path = $this->href("bzlayoutPath")->getFullPath();
     $this->layoutHelper($container->blogTags,$bzlayout_path);
     }
     ?>

<?php } ?>
