<?php
	use Pimcore\Tool;
	use Pimcore\Model\Webservice;
?>


<?php if($this->editmode) { ?>

	<style type="text/css">
		    body {
		        padding:0;
		        margin: 0;
		        font-family: "Lucida Sans Unicode", Arial;
		        font-size: 14px;
		    }

		    #site {
		        margin: 0 auto;
		        width: 600px;
		        padding: 30px 0 0 0;
		        color:#65615E;
		    }

		    h1, h2, h3 {
		        font-size: 18px;
		        padding: 0 0 5px 0;
		        border-bottom: 1px solid #001428;
		        margin-bottom: 5px;
		    }

		    h3 {
		        font-size: 14px;
		        padding: 15px 0 5px 0;
		        margin-bottom: 5px;
		        border-color: #cccccc;
		    }

		    img {
		        border: 0;
		    }

		    p {
		        padding: 0 0 5px 0;
		    }

		    a {
		        color: #000;
		    }

		    #logo {
		        text-align: center;
		        padding: 50px 0;
		    }

		    #logo hr {
		        display: block;
		        height: 1px;
		        overflow: hidden;
		        background: #BBB;
		        border: 0;
		        padding:0;
		        margin:30px 0 20px 0;
		    }

		    .claim {
		        text-transform: uppercase;
		        color:#BBB;
		    }

		    #site ul {
		        padding: 10px 0 10px 20px;
		        list-style: circle;
		    }

		    .alert {
		    	margin:30px 0 20px 0;
		    }

	</style>


    <div class="alert alert-info">
    	<h2>Drag and drop component snippets here</h2>
        <?=
        	$this->multihref("componentPaths", [
    			"types" => ["document"],
		        "subtypes" => [
		            "document" => ["snippet"]
		         ]
				]);
		?>
    </div>

<?php } ?>



<?php if( !$this->editmode ) { ?>

	<?php
	  $pagedata = $this->getParam('page_data');
		if (! isset($pagedata)) {
			$pagedata = new \stdClass();
		}


		$pagedata->components = new \stdClass();


        //$pagedata->test = new \stdClass();
        $count=0;


        //$logger->error($mhref);

        $pagesecComponents = array();
        $pagesecComponent_names = array();

        foreach($this->multihref("componentPaths") as $element ):
            //$propertyName = 'component'.$count;
            //$pagedata->test->$propertyName = $element->getId() ;

            $pagesecComponents[$count] = $element->getId();
            $element_key = $element->getKey();
            $pagesecComponent_names[$count] = $element_key;

            $count++;
        endforeach;


        //$this->view->page_data = $pagedata;

        $arrLength = count($pagesecComponents);
        for($x = 0; $x < $arrLength; $x++) {
            $id = $pagesecComponents[$x];
            $key = $pagesecComponent_names[$x];
            $pagedata->components->$key = new \stdClass();
            $container = $pagedata->components->$key;

            try{
              $this->inc($id, ["page_data" => $container],false  );
            }
            catch (Exception $e) {
            }

        }



    ?>

    <?php
       //$this->encoder = new Webservice\JsonEncoder();
       //$encodedresults = $this->encoder->encode($pagedata);
    ?>




<?php } ?>
