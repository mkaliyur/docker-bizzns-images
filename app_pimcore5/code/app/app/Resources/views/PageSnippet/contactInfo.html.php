<!--Edit mode code -->
<?php
	use Pimcore\Tool;
    use Pimcore\Model\Webservice;
?>


<?php if($this->editmode) { ?>
	<style type="text/css">
        .area-contentblock {
            margin-top: 60px;
        }
    </style>


	<section class="area-contentblock" style="margin-top:30px;">
		<h4>Enter Your Info Here</h4>
    <div><label>Enter Your Contact Number Here</label></div>
		<div><?= $this->input("contact"); ?></div>
	</section>

  <section class="area-contentblock" style="margin-top:30px;">
    <div><label>Enter Your Address Here</label></div>
    <div><?= $this->input("address"); ?></div>
  </section>

  <section class="area-contentblock" style="margin-top:30px;">
    <div><label>Enter Your Mail Id Here</label></div>
		<div><?= $this->input("mailid"); ?></div>
	</section>

<?php } ?>



<!--Frond end code -->
<?php if( $this->editmode == false &&  (Tool::isFrontend() || Tool::isFrontentRequestByAdmin() )) { ?>
<?php
    if (!isset($pagedata)) {
        $pagedata = new \stdClass();
    }

    $pagedata = $this->input('contact')->getData();
    $results2 = print_r($pagedata, true);
    \Pimcore\Log\Simple::log("event.log", 'Inside view of PageSnippetcontactinfo pagedata = '. $results2);

    array_push($pagedata, );

?>
<?php
  //$this->encoder = new Webservice\JsonEncoder();
  //$this->encoder->encode($pagedata);
 ?>



<?php } ?>
