<?php
    use Pimcore\Tool;
    use Pimcore\Model\Webservice;
    use Pimcore\Model\DataObject\BzListingBuilders\Listing;
?>


<?php if($this->editmode) { ?>
  <section class="layout">
    <?=
      $this->href("objectPath", [
      "types" => ["object"],
        "subtypes" => [
            "object" => ["object"]
         ],
         "classes" => ["BzLayout"]
    ]);
    ?>
  </section>
<?php } ?>


<?php if( !$this->editmode &&  (Tool::isFrontend() || Tool::isFrontentRequestByAdmin() )) {?>
	<?php
        $domainname = $this->getParam('domainname');
        $marketname = $this->getParam('marketname');


        $pagedata = $this->getParam('page_data');
        if (! isset($pagedata)) {
            $pagedata = new \stdClass();
        }

        $pagedata->promotedListings = new \stdClass();
        $pagedata->promotedListings->data = [];
        $pagedata->promotedListings->Layout = new \stdClass();
        $container = $pagedata;


        function getListingsByIsPromotedFlag($locale = 'en'){
         $promotedListingList = new Listing();
         $promotedListingList->setCondition("isPromoted = ?" ,[true]);
         //$promotedListingList->setCondition("isPromoted = ? AND domain_name = ? " ,[true, $domainname]);
         $promotedListingList->setLimit(6);
         $ret = $promotedListingList->getObjects();
         return $ret;
        }


        if (!function_exists('createSlug')) {
            function createSlug($str, $delimiter = '-'){
                $slug = strtolower(trim(preg_replace('/[\s-]+/', $delimiter, preg_replace('/[^A-Za-z0-9-]+/', $delimiter, preg_replace('/[&]/', 'and', preg_replace('/[\']/', '', iconv('UTF-8', 'ASCII//TRANSLIT', $str))))), $delimiter));
                return $slug;

            }
        }

        // echo getBySlug('test-news')->getKey(). "\n";
        if (!function_exists('getImageDetails')){
            function getImageDetails($server_base,$image){
                    $ret = new \stdClass();
                    $ret->type = $image->getType();
                    $ret->id = $image->getId();
                    $ret->parentId = $image->getParentId();
                    $ret->parent= $image->getParent();
                    $ret->filename = $image->getFilename();
                    $ret->path = $image->getPath();
                    $ret->mimetype = $image->getMimetype();
                    $serverPath = $server_base;
                    $ret->guid = $serverPath.$image->getPath().$image->getFilename();
                    return $ret;
            }

        }


        $promotedListings= getListingsByIsPromotedFlag();


         foreach($promotedListings as $listing) {
            $response = new \stdClass();
            if (!$listing->getName()){
                $response->name = $listing->getHeadline();
            }else{
                $response->name=$listing->getName();
            }

            $response->title=new \stdClass();
            $response->title->rendered=$response->name;
            $response->slug = createSlug($response->name);
            $response->domain = $listing->domain->Name;
            $response->datatype = $listing->datatype->name;
            $response->headline = $listing->getHeadline();
            $response->shortDescription = $listing->getShortDescription();
            $response->rating = $listing->getRating();
            $response->isFeatured = $listing->getIsFeatured();
            $response->isPromoted = $listing->getIsPromoted();
            $response->isPremium = $listing->getIsPremium();
            $response->isClaimed = $listing->getIsClaimed();
            $response->listOrder = $listing->getListorder();
            $response->mainContactNumber = $listing->getMainContactNumber();
            $response->websiteUrl = $listing->getWebsiteUrl();
            $response->email=$listing->getEmail();
            $response->domainname = $domainname;
            $response->marketname = $marketname;

            $response->bannerImages = [];
            if(!empty($listing->getBannerImages())){
                foreach((array) $listing->getBannerImages() as $image ){
                    $server_base = $this->document->getProperty('server_base');
                    $ret = getImageDetails($server_base,$image);

                    array_push($response->bannerImages,$ret);
                }
            }


            array_push($pagedata->promotedListings->data, $response);
        }
     ?>

     <?php
     $objectPath_isempty = $this->href("objectPath")->isEmpty();
     if(!$objectPath_isempty){
       $object_path = $this->href("objectPath")->getFullPath();
       $this->layoutHelper($container->promotedListings,$object_path);
     }else{
       $container->Layout = new \stdClass();
     }
    ?>

<?php } ?>
