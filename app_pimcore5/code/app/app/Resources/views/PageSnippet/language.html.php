<!--Edit mode code -->
<?php
	use Pimcore\Tool;
    use Pimcore\Model\Webservice;
?>
<?php if($this->editmode) { ?>
	<style type="text/css">
        .area-language {
            margin-top: 60px;
        }

    </style>


	<section class="area-language" style="margin-top:30px;">
		<h4>Define Languages Used on the site Here</h4>
		<div><?= $this->input("language1"); ?></div>
		<div><?= $this->input("language2"); ?></div>
		<div><?= $this->input("language3"); ?></div>
		<div><?= $this->input("language4"); ?></div>
		<div><?= $this->input("language5"); ?></div>
	</section>
<?php } ?>



<!--Frond end code -->
<?php if(!$this->editmode) { ?>
<?php

	$pagedata = $this->getParam('page_data');
	if (!isset($pagedata)) {
            $pagedata = new \stdClass();
    }



    $pagedata->languages = [];
    $pagedata->languages[0] = $this->input("language1")->getData();
    $pagedata->languages[1] = $this->input("language2")->getData();
    $pagedata->languages[2] = $this->input("language3")->getData();
    $pagedata->languages[3] = $this->input("language4")->getData();
    $pagedata->languages[4] = $this->input("language5")->getData();




?>



<?php } ?>
