<!--Edit mode code -->
<?php
	use Pimcore\Tool;
    use Pimcore\Model\Webservice;
?>


<?php if($this->editmode) { ?>
	<style type="text/css">
        .area-contentblock {
            margin-top: 60px;
        }
    </style>


	<section class="area-contentblock" style="margin-top:30px;">
		<h4>Enter content block heading</h4>
    <div><label>Enter content block data here</label>
		<div><?= $this->input("content"); ?></div>
	</section>
<?php } ?>



<!--Frond end code -->
<?php if(!$this->editmode) { ?>
<?php

	$pagedata = $request->get('page_data');
	if (!isset($pagedata)) {
      $pagedata = new \stdClass();
  }

    $pagedata->topmenu = new \stdClass();

     $pagedata->topmenu->name = $this->input("content");


?>



<?php } ?>
