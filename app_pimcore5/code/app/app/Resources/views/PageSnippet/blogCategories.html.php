<?php
    use Pimcore\Tool;
    use Pimcore\Model\Webservice;
    use Pimcore\Model\DataObject;
    use Pimcore\Model\DataObject\BlogCategory\Listing;
?>


<?php if($this->editmode){ ?>
  <section>
    <h2>Drag and drop BzLayout path here</h2>
      <?=
        $this->href("bzlayoutPath", [
        "types" => ["object"],
          "subtypes" => [
              "object" => ["object"]
           ],
           "classes" => ["BzLayout"]
      ]);
  ?>
 </section>


  <style type="text/css">
        body {
            padding:0;
            margin: 0;
            font-family: "Lucida Sans Unicode", Arial;
            font-size: 14px;
        }

        #site {
            margin: 0 auto;
            width: 600px;
            padding: 30px 0 0 0;
            color:#65615E;
        }

        h1, h2, h3 {
            font-size: 18px;
            padding: 0 0 5px 0;
            border-bottom: 1px solid #001428;
            margin-bottom: 5px;
        }

        h3 {
            font-size: 14px;
            padding: 15px 0 5px 0;
            margin-bottom: 5px;
            border-color: #cccccc;
        }

        img {
            border: 0;
        }

        p {
            padding: 0 0 5px 0;
        }

        a {
            color: #000;
        }

        #logo {
            text-align: center;
            padding: 50px 0;
        }

        #logo hr {
            display: block;
            height: 1px;
            overflow: hidden;
            background: #BBB;
            border: 0;
            padding:0;
            margin:30px 0 20px 0;
        }

        .claim {
            text-transform: uppercase;
            color:#BBB;
        }

        #site ul {
            padding: 10px 0 10px 20px;
            list-style: circle;
        }

        .alert {
          margin:30px 0 20px 0;
        }

  </style>

    <div class="alert alert-info">
      <h2>Drag and drop bzblogcategory path here</h2>
        <?=
          $this->href("blogcategoryPath", [
          "types" => ["object"],
            "subtypes" => [
                "object" => ["object"]
             ],
             "classes" => ["BzBlogCategory"]
        ]);
        ?>
    </div>

<?php } ?>


<?php if( !$this->editmode &&  (Tool::isFrontend() || Tool::isFrontentRequestByAdmin() )) {?>
	<?php
        $pagedata = $this->getParam('page_data');
        //\Pimcore\Log\Simple::log("event.log",">>>>> KGM:: INSIDE blogCategoriesps second action CONTROLLER ");
        if (! isset($pagedata)) {
            $pagedata = new \stdClass();
        }


        //$containerName = $request->get("container");
        $containerName = null;
        if( isset($containerName)){
            if (!isset($pagedata->$containerName)){
                $container = $pagedata->$containerName = new \stdClass();
            }else{
                $container = $pagedata->$containerName;
            }
        }else{
            $container = $pagedata;
        }

        $container->blogCategories = new \stdClass();
        $container->blogCategories->data = [];
        $container->blogCategories->Layout = new \stdClass();

         $blogcategoryPath_isempty = $this->href("blogcategoryPath")->isEmpty();
         if(!$blogcategoryPath_isempty){
         $blogcategory_path = $this->href("blogcategoryPath")->getFullPath();

        //$categories = DataObject\bzCategories::getList();
        $categories = DataObject::getByPath($blogcategory_path)->getChilds();

        if (!function_exists('createSlug')) {
            function createSlug($str, $delimiter = '-'){
                $slug = strtolower(trim(preg_replace('/[\s-]+/', $delimiter, preg_replace('/[^A-Za-z0-9-]+/', $delimiter, preg_replace('/[&]/', 'and', preg_replace('/[\']/', '', iconv('UTF-8', 'ASCII//TRANSLIT', $str))))), $delimiter));
                return $slug;
            }
        }

        if (!function_exists("getReferenceCounts")){
            function getReferenceCounts($id){
                $ret_count = 0;
                $blogList = new DataObject\BlogArticle\Listing();

                $conditions = [];
                $conditions[] = "categories LIKE '%|".(int)$id. ",%'";
                //\Pimcore\Log\Simple::log("event.log", 'Inside view of blogCategories::Inside getReferenceCounts int id= '.$id);

                //$conditions[] = "headline LIKE '%"."H"."%'";


                if (!empty($conditions)) {
                     $blogList->setCondition(implode(" AND ", $conditions));
                }

                //$objects = $listingsList->load();

                //echo $listingsList;

                $ret_count = $blogList->count();

                return $ret_count;
            }

        }

        if (!function_exists("getChildElements")){
            function getChildElements($element){
                $ret = new \stdClass();
                $ret->name = $element->getName();
                $ret->slug = createSlug($ret->name);
                $ret->type = $element->o_type;
                if($ret->type=='folder'){
                    $ret->name=$element->o_key;
                }


                $ret->o_id = $element->o_id;
                $ret->o_parentId = $element->o_parentId;
                $ret->o_path = $element->o_path;

                $ret->count = getReferenceCounts($element->o_id);
                //$ret->cat_orig = $element;

                $children = $element->getChilds();

                if(sizeof($children) > 0){
                    $ret->children = [];
                    foreach( (array) $children as $child_element ){
                        $retchild = getChildElements($child_element);
                        array_push($ret->children,$retchild);
                        $ret->count = 0;
                        foreach ((array)$ret->children as $c ){
                            $ret->count = $ret->count + $c->count;
                        }
                    }
                }
                return $ret;
            }

        }



        foreach( (array) $categories as $cat ){
        	$ret = getChildElements($cat);
        	array_push($container->blogCategories->data,$ret);
        }
      }
     ?>

     <?php
       $bzlayoutPath_isempty = $this->href("bzlayoutPath")->isEmpty();
       if(!$bzlayoutPath_isempty){
       $bzlayout_path = $this->href("bzlayoutPath")->getFullPath();
       $this->layoutHelper($container->blogCategories,$bzlayout_path);
      }
     ?>


<?php } ?>
