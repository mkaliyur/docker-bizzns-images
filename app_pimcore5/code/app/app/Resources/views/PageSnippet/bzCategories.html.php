<?php
  use Pimcore\Tool;
  use Pimcore\Model\Webservice;
  use \Pimcore\Model\DataObject;
  use Pimcore\Model\DataObject\BzCategories\Listing;
?>


<?php if($this->editmode) { ?>
  <div class="alert alert-info">
    <h2>Drag and drop bzcategory path here</h2>
      <?=
        $this->href("bzcategoryPath", [
        "types" => ["object"],
          "subtypes" => [
              "object" => ["object"]
           ],
           "classes" => ["BzCategories"]
      ]);
  ?>
  </div>
  <section>
    <h2>Drag and drop BzLayout path here</h2>
      <?=
        $this->href("bzlayoutPath", [
        "types" => ["object"],
          "subtypes" => [
              "object" => ["object"]
           ],
           "classes" => ["BzLayout"]
      ]);
  ?>
</section>


<?php } ?>


<?php if( !$this->editmode &&  (Tool::isFrontend() || Tool::isFrontentRequestByAdmin() )) {?>
	<?php
        $pagedata = $this->getParam('page_data');
        if (! isset($pagedata)) {
            $pagedata = new \stdClass();
        }
        //$containerName = $this->getParam("container");
        $containerName = null;
        if( isset($containerName)){
            if (!isset($pagedata->$containerName)){
                $container = $pagedata->$containerName = new \stdClass();
            }else{
                $container = $pagedata->$containerName;
            }
        }else{
            $container = $pagedata;
        }


        $container->bzCategories = new \stdClass();
        $container->bzCategories->data = [];
        $container->bzCategories->Layout = new \stdClass();

        $bzcategoryPath_isempty = $this->href("bzcategoryPath")->isEmpty();
        if(!$bzcategoryPath_isempty){
        $bzcategory_path = $this->href("bzcategoryPath")->getFullPath();

       //$categories = DataObject\bzCategories::getList();
       $categories = DataObject::getByPath($bzcategory_path)->getChilds();
       //$layout = DataObject::getByPath($bzlayout_path)->getChilds();


        if (!function_exists('createSlug')) {
            function createSlug($str, $delimiter = '-'){
                $slug = strtolower(trim(preg_replace('/[\s-]+/', $delimiter, preg_replace('/[^A-Za-z0-9-]+/', $delimiter, preg_replace('/[&]/', 'and', preg_replace('/[\']/', '', iconv('UTF-8', 'ASCII//TRANSLIT', $str))))), $delimiter));
                return $slug;
            }
        }

        function getListingsReferenceCounts($id){
            $ret_count = 0;
            $listingsList = new DataObject\BzListingBuilders\Listing();
            $conditions = [];
            $conditions[] = "category LIKE '%|".(int)$id. ",%'";
            //$conditions[] = "headline LIKE '%"."H"."%'";


            if (!empty($conditions)) {
                 $listingsList->setCondition(implode(" AND ", $conditions));
            }

            //$objects = $listingsList->load();

            //echo $listingsList;

            $ret_count = $listingsList->count();
            $results2 = print_r($ret_count, true);
            \Pimcore\Log\Simple::log("event.log", 'Inside bzcategories :: getListingsReferenceCounts:: ret_count ='.$results2);

            return $ret_count;
        }

        function getListingsChildElements($element){
        	$ret = new \stdClass();
        	$ret->name = $element->getName();
            $ret->slug = createSlug($ret->name);
        	$ret->type = $element->o_type;
        	if($ret->type=='folder'){
        		$ret->name=$element->o_key;
        	}

        	$ret->o_id = $element->o_id;
        	$ret->o_parentId = $element->o_parentId;
        	$ret->o_path = $element->o_path;

            $ret->count = getListingsReferenceCounts($element->o_id);
         	//$ret->cat_orig = $element;

        	$children = $element->getChilds();

        	if(sizeof($children) > 0){
        		$ret->children = [];
        		foreach( (array) $children as $child_element ){
        			$retchild = getListingsChildElements($child_element);
        			array_push($ret->children,$retchild);
                    $ret->count = 0;
                    foreach ((array)$ret->children as $c ){
                        $ret->count = $ret->count + $c->count;
                    }
        		}
        	}
        	return $ret;
        }

        foreach( (array) $categories as $cat ){
        	$ret = getListingsChildElements($cat);
        	array_push($container->bzCategories->data,$ret);
        }
      }

     ?>
     <?php
     $bzlayoutPath_isempty = $this->href("bzlayoutPath")->isEmpty();
     if(!$bzlayoutPath_isempty){
     $bzlayout_path = $this->href("bzlayoutPath")->getFullPath();
     $this->layoutHelper($container->bzCategories,$bzlayout_path);
    }
     ?>
<?php } ?>
