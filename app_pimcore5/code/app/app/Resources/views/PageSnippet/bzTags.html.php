<?php
    use Pimcore\Tool;
    use Pimcore\Model\Webservice;
    use Pimcore\Model\DataObject;
?>


<?php if($this->editmode) { ?>
  <div class="alert alert-info">
    <h2>Drag and drop BzTags path here</h2>
      <?=
        $this->href("bztagsPath", [
        "types" => ["object"],
          "subtypes" => [
              "object" => ["object"]
           ],
           "classes" => ["BzTags"]
      ]);
  ?>
  </div>
  <section>
    <h2>Drag and drop BzLayout path here</h2>
      <?=
        $this->href("bzlayoutPath", [
        "types" => ["object"],
          "subtypes" => [
              "object" => ["object"]
           ],
           "classes" => ["BzLayout"]
      ]);
  ?>
</section>
<?php } ?>



<?php if( !$this->editmode &&  (Tool::isFrontend() || Tool::isFrontentRequestByAdmin() )) {?>
	<?php
        $pagedata = $this->getParam('page_data');
        if (! isset($pagedata)) {
            $pagedata = new \stdClass();
        }



        //$containerName = $this->getParam("container");
        $containerName = null;
        if( isset($containerName)){
            if (!isset($pagedata->$containerName)){
                $container = $pagedata->$containerName = new \stdClass();
            }else{
                $container = $pagedata->$containerName;
            }
        }else{
            $container = $pagedata;
        }




        $container->bzTags = new \stdClass();
        $container->bzTags->data = [];
        $container->bzTags->Layout = new \stdClass();




        $bztagsPath_isempty = $this->href("bztagsPath")->isEmpty();
        if(!$bztagsPath_isempty){
        $bztags_path = $this->href("bztagsPath")->getFullPath();

       //$categories = DataObject\bzCategories::getList();
       $tags = DataObject::getByPath($bztags_path)->getChilds();

        if (!function_exists('createSlug')) {
            function createSlug($str, $delimiter = '-'){
                $slug = strtolower(trim(preg_replace('/[\s-]+/', $delimiter, preg_replace('/[^A-Za-z0-9-]+/', $delimiter, preg_replace('/[&]/', 'and', preg_replace('/[\']/', '', iconv('UTF-8', 'ASCII//TRANSLIT', $str))))), $delimiter));
                return $slug;
            }
        }


        function getBzTagsReferenceCounts($id){
            $ret_count = 0;
            $listingsList = new DataObject\BzListingBuilders\Listing();
            $conditions = [];
            $conditions[] = "tags LIKE '%|".(int)$id. ",%'";
            //$conditions[] = "headline LIKE '%"."H"."%'";


            if (!empty($conditions)) {
                 $listingsList->setCondition(implode(" AND ", $conditions));
            }

            //$objects = $listingsList->load();

            //echo $listingsList;

            $ret_count = $listingsList->count();

            return $ret_count;
        }

        function getBzTagsChildElements($element){
        	$ret = new \stdClass();
        	$ret->name = $element->getName();
            $ret->slug = createSlug($ret->name);
        	$ret->type = $element->o_type;
        	if($ret->type=='folder'){
        		$ret->name=$element->o_key;
        	}


        	$ret->o_id = $element->o_id;
        	$ret->o_parentId = $element->o_parentId;
        	$ret->o_path = $element->o_path;

            $ret->count = getBzTagsReferenceCounts($element->o_id);
         	//$ret->cat_orig = $element;

        	$children = $element->getChilds();

        	if(sizeof($children) > 0){
        		$ret->children = [];
        		foreach( (array) $children as $child_element ){
        			$retchild = getBzTagsChildElements($child_element);
        			array_push($ret->children,$retchild);
                    $ret->count = 0;
                    foreach ((array)$ret->children as $c ){
                        $ret->count = $ret->count + $c->count;
                    }
        		}
        	}
        	return $ret;
        }



        foreach( (array) $tags as $tag ){
        	$ret = getBzTagsChildElements($tag);
        	array_push($container->bzTags->data,$ret);
        }
      }

     ?>

     <?php
     $bzlayoutPath_isempty = $this->href("bzlayoutPath")->isEmpty();
     if(!$bzlayoutPath_isempty){
     $bzlayout_path = $this->href("bzlayoutPath")->getFullPath();
     $this->layoutHelper($container->bzTags,$bzlayout_path);
    }
     ?>


<?php } ?>
