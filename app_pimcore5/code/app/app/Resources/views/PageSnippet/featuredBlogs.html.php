<?php
    use Pimcore\Tool;
    use Pimcore\Model\Webservice;
?>


<?php if($this->editmode) { ?>
	<h2>No Admin UI.. will retrieve top 3 blog object list</h2>
<?php } ?>


<?php if( !$this->editmode &&  (Tool::isFrontend() || Tool::isFrontentRequestByAdmin() )) {?>
	<?php
        $pagedata = $this->pagedata;
        if (! isset($pagedata)) {
            $pagedata = new \stdClass();
        }
        $pagedata->featuredBlogs = [];



     ?>

<?php } ?>
