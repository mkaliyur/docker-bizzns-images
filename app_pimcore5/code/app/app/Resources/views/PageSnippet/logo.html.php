<?php
    use Pimcore\Tool;
    use Pimcore\Model\Webservice;
?>

<?php
   //  function getSnippetLayout(){
   //    $pagedata = $this->getParam('page_data');
   //    if (! isset($pagedata)) {
   //        $pagedata = new \stdClass();
   //    }
   //
   //    $containerName = null;
   //    if( isset($containerName)){
   //        if (!isset($pagedata->$containerName)){
   //            $container = $pagedata->$containerName = new \stdClass();
   //        }else{
   //            $container = $pagedata->$containerName;
   //        }
   //    }else{
   //        $container = $pagedata;
   //    }
   //    $container->Layout = new \stdClass();
   //
   //     $objectPath_isempty = $this->href("objectPath")->isEmpty();
   //     if(!$objectPath_isempty){
   //       $object_path = $this->href("objectPath")->getFullPath();
   //     }
   //     $layout = DataObject::getByPath($object_path);
   //
   //     $container->Layout->name = $layout->name;
   //     $container->Layout->selector = $layout->selector;
   //
   //     $container->Layout->settings = [];
   //     if( !empty($layout->getSettings()) &&   !empty($layout->getSettings()->getItems() )){
   //       foreach( (array) $layout->getSettings()->items as $settings_item ){
   //         $ret = new \stdClass();
   //         $ret->name = $settings_item->setting_name;
   //         $ret->value = $settings_item->setting_value;
   //         array_push($container->Layout->settings,$ret);
   //       }
   //    }
   //
   // }//End of Function getSnippetLayout
 ?>

  <?php
      //$pagedata = $this->page_data;
      //$pagedata = $this->getParam('page_data');
      $pagedata = $this->getParam('page_data');
  ?>

<!-- Excute only in Admin mode -->
  <?php if($this->editmode) {?>


    <style type="text/css">

    <style type="text/css">
        .alert {
            margin-top: 60px;
        }

    </style>

  <section>
     <h2>Define Logo </h2>

    <label class="alert"> Enter Logo Text </label>
    <?= $this->input("LogoText"); ?>

    <hr/>

    <label class="alert"> Enter Tag Line </label>
    <?= $this->input("TagLine"); ?>

    <p class="alert">Logo Small Image</p>
    <?= $this->image("logo_small_img"); ?>
  </section>
  <section>
    <div class="alert alert-info">
      <h2>Drag and drop bzLayout Objectpath here</h2>
        <?=
          $this->href("objectPath", [
          "types" => ["object"],
            "subtypes" => [
                "object" => ["object"]
             ],
             "classes" => ["BzLayout"]
        ]);
    ?>
    </div>
  </section>

<?php }?>



  <?php if( $this->editmode == false &&  (Tool::isFrontend() || Tool::isFrontentRequestByAdmin() )) {?>
    <!-- execute only on front end -->


  <?php
          if (!isset($pagedata)) {
              $pagedata = new \stdClass();
          }
          //get Image data
          $img_logo_small_path = $this->image("logo_small_img")->getSrc();

          $asset_img_logo_small = \Pimcore\Model\Asset::getByPath($img_logo_small_path);

          $mimetype_logo_small = $asset_img_logo_small->getMimetype();

          $is_empty = $this->image("logo_small")->isEmpty() ;

          $serverPath = $this->document->getProperty('server_base');

          //load pagedata object
          $pagedata->logo = new \stdClass();
          $pagedata->logo->logoText = $this->input("LogoText");
          $pagedata->logo->tagline = $this->input('TagLine');
          $pagedata->logo->img_small = new \stdClass();
          $pagedata->logo->img_small->logo_small_src = $img_logo_small_path;
          $pagedata->logo->img_small->guid=$serverPath.$img_logo_small_path;
          $pagedata->logo->img_small->post_mime_type=$mimetype_logo_small;

          //$logger->info("$pagedata after logo view is done == ");
          //$logger->info($pagedata);
          //$results = print_r($pagedata, true);

  ?>

  <?php
    // $this->encoder = new Webservice\JsonEncoder();
    // $encodedresults = $this->encoder->encode($pagedata);
    // return $encodedresults;
  ?>


  <?php } ?>
