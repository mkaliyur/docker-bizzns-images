<?php
    use Pimcore\Tool;
    use Pimcore\Model\Webservice;
    use Pimcore\Model\DataObject\BzNews\Listing;
?>


<?php if($this->editmode) { ?>
	<h2>No Admin UI.. will retrieve top News object list</h2>
<?php } ?>


<?php if( !$this->editmode &&  (Tool::isFrontend() || Tool::isFrontentRequestByAdmin() )) {?>
	<?php

        $pagedata = $this->getParam('page_data');
        if (! isset($pagedata)) {
            $pagedata = new \stdClass();
        }
        $pagedata->latestNews = [];



        function getLatestNews($locale = 'en'){
         $topNewsList = new Listing();
         if($locale === null){
          $topNewsList->setLocale($locale);
         }

        $topNewsList->setCondition("topnews = ?",true);
          //$topNewsList->setLocale($locale);
         $topNewsList->setOrderKey( "date" );
         $topNewsList->setorder("desc");
         $topNewsList->setLimit(9);
         $ret = $topNewsList->getObjects();
         return $ret;
        }

        if (!function_exists('createSlug')) {
            function createSlug($str, $delimiter = '-'){
                $slug = strtolower(trim(preg_replace('/[\s-]+/', $delimiter, preg_replace('/[^A-Za-z0-9-]+/', $delimiter, preg_replace('/[&]/', 'and', preg_replace('/[\']/', '', iconv('UTF-8', 'ASCII//TRANSLIT', $str))))), $delimiter));
                return $slug;
            }
        }



        if (!function_exists('getImageDetails')){
            function getImageDetails($server_base,$image){
                    $ret = new \stdClass();
                    $ret->type = $image->getType();
                    $ret->id = $image->getId();
                    $ret->parentId = $image->getParentId();
                    $ret->parent= $image->getParent();
                    $ret->filename = $image->getFilename();
                    $ret->path = $image->getPath();
                    $ret->mime_type = $image->getMimetype();
                    $ret->post_mime_type = $image->getMimetype();
                    $serverPath = $server_base;
                    $ret->guid = $serverPath.$image->getPath().$image->getFilename();
                    return $ret;
            }
        }

        $topNewsItems= getLatestNews();

        foreach($topNewsItems as $newsItem) {

            $response = new \stdClass();
            $response->id = $newsItem->o_id;
            //$response->name=$newsItem->o_key;
            $response->title=new \stdClass();
            $response->title->rendered = new \stdClass();
            //$response->newsItem = $newsItem;
            $response->name = $newsItem->getName();
            $response->date = $newsItem->getDate();
            $response->headline = $newsItem->getHeadline();
            $response->title = $newsItem->getTitle();
            $response->slug = $newsItem->getSlug();
            if( !isset($response->slug) ){
                $response->slug = createSlug($newsItem->getName());
            }

            //$response->tags = $newsItem->getTags();
            $response->topnews = $newsItem->getTopnews();
            $response->shortText = $newsItem->getShortText();

            $response->image1 = new \stdClass();
            if(!empty ($newsItem->getImage_1()) ){
                $image1 = $newsItem->getImage_1();
                $server_base = $this->document->getProperty('server_base');
                $ret = getImageDetails($server_base,$image1);
                $response->image1 = $ret;
            }


            array_push($pagedata->latestNews, $response);

        }

     ?>
     <?php
        $this->encoder = new Webservice\JsonEncoder();
        $encodedresults = $this->encoder->encode($pagedata);
     ?>


<?php } ?>
