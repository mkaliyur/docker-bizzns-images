<?php
    use Pimcore\Tool;
    use Pimcore\Model\Webservice;
    use Pimcore\Model\DataObject\BzBlogArticle\Listing;
?>

<?php if($this->editmode) { ?>
  <section class="layout">
    <?=
      $this->href("objectPath", [
      "types" => ["object"],
        "subtypes" => [
            "object" => ["object"]
         ],
         "classes" => ["BzLayout"]
    ]);
    ?>
  </section>
<?php } ?>


<?php if( !$this->editmode &&  (Tool::isFrontend() || Tool::isFrontentRequestByAdmin() )){?>
  <?php
        $pagedata = $this->getParam('page_data');
        if (! isset($pagedata)) {
            $pagedata = new \stdClass();
        }
        $pagedata->promotedBlogs = new \stdClass();
        $pagedata->promotedBlogs->data = [];
        $pagedata->promotedBlogs->Layout = new \stdClass();
        $container = $pagedata;

        function getByIsPromotedToHomePage($locale = 'en'){
         $promotedBlogList = new Listing();
         if($locale === null){
          $promotedBlogList->setLocale($locale);
         }
         $promotedBlogList->setCondition("isPromotedToHomePage = ?",true);
         $promotedBlogList->setLimit(6);
         $ret = $promotedBlogList->getObjects();

         // $promo_objects = print_r($ret, true);
         // \Pimcore\Log\Simple::log("event.log", 'Inside view of promotedblogs objects== '. $promo_objects);

         return $ret;
        }

        // echo getBySlug('test-news')->getKey(). "\n";


        $promotedBlogs= getByIsPromotedToHomePage();

         foreach($promotedBlogs as $promotedBlog) {
            $response = new \stdClass();
            $response->title = new \stdClass();
            $response->title->rendered = new \stdClass();
            $response->name=$promotedBlog->o_key;
            // $blogItems = $promotedBlog;
            //
            // foreach($blogItems as $key => $value) {
            //     $keyvalue = new \stdClass();
            //     $keyvalue->key = $key;
            //     $keyvalue->value = $value;
            //     //array_push($response->keys, $keyvalue);
            //     if($key == 'en'){
            //         //$text = new \stdClass();
            //        // $text = $value->text;
            //         foreach($value as $key1 => $value1) {
            //             $keyvalue1 = new \stdClass();
            //             $keyvalue1->key = $key1;
            //             $keyvalue1->value = $value1;
            //             //array_push($response->keys1, $keyvalue1);
            //
            //             if($key1 == 'title'){
            //                 $response->title->rendered=$value1;
            //             }
            //
            //             if($key1 == 'briefSummary'){
            //                 $response->briefSummary = $value1;
            //             }
            //
            //             if($key1 == 'headline'){
            //                 $response->headline = $value1;
            //             }
            //
            //             if($key1 == 'slug'){
            //                 $response->slug = $value1;
            //             }
            //
            //         }
            //
            //     }
            // }

            $response->date = $promotedBlog->getDate();
            $response->author = $promotedBlog->getAuthor();
            $response->title = $promotedBlog->getTitle();
            $response->briefSummary = $promotedBlog->getBriefSummary();
            $response->headline = $promotedBlog->getHeadline();
            $response->slug = $promotedBlog->getSlug();

            array_push($pagedata->promotedBlogs->data, $response);
        }
     ?>

     <?php
     $objectPath_isempty = $this->href("objectPath")->isEmpty();
     if(!$objectPath_isempty){
       $object_path = $this->href("objectPath")->getFullPath();
       $this->layoutHelper($container->promotedBlogs,$object_path);
     }else{
       $container->Layout = new \stdClass();
     }

      // $this->encoder = new Webservice\JsonEncoder();
      // $encodedresults = $this->encoder->encode($pagedata);
    ?>
<?php } ?>
