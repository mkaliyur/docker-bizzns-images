<?php
/**
 * @var \Pimcore\Templating\PhpEngine $this
 * @var \Pimcore\Templating\PhpEngine $view
 * @var \Pimcore\Templating\GlobalVariables $app
 */
	use Pimcore\Tool;
	use Pimcore\Model\Webservice;
	use Pimcore\Model\DataObject;
	use Pimcore\Model\DataObject\BzPublishQueue\Listing;
?>
<?php
//\Pimcore\Log\Simple::log("event.log", 'Inside view of queue::queueAction:: beginning of view ');
?>
<?php if($this->editmode) { ?>

	<style type="text/css">
		    body {
		        padding:0;
		        margin: 0;
		        font-family: "Lucida Sans Unicode", Arial;
		        font-size: 14px;
		    }

		    #site {
		        margin: 0 auto;
		        width: 600px;
		        padding: 30px 0 0 0;
		        color:#65615E;
		    }

		    h1, h2, h3 {
		        font-size: 18px;
		        padding: 0 0 5px 0;
		        border-bottom: 1px solid #001428;
		        margin-bottom: 5px;
		    }

		    h3 {
		        font-size: 14px;
		        padding: 15px 0 5px 0;
		        margin-bottom: 5px;
		        border-color: #cccccc;
		    }

		    img {
		        border: 0;
		    }

		    p {
		        padding: 0 0 5px 0;
		    }

		    a {
		        color: #000;
		    }

		    #logo {
		        text-align: center;
		        padding: 50px 0;
		    }

		    #logo hr {
		        display: block;
		        height: 1px;
		        overflow: hidden;
		        background: #BBB;
		        border: 0;
		        padding:0;
		        margin:30px 0 20px 0;
		    }

		    .claim {
		        text-transform: uppercase;
		        color:#BBB;
		    }

		    #site ul {
		        padding: 10px 0 10px 20px;
		        list-style: circle;
		    }

		    .alert {
		    	margin:30px 0 20px 0;
		    }

	</style>

	<div>Note: This page name is used in the scripts! If you change the name, you will need to change the template scripts </div>
	<div class="alert" >
	<div><label>Enter Page Name Here</label></div>
	<?=$this->input('pagename'); ?>
  </div>
	<div class="alert" >
	<div><label>Enter Page Headline Here</label></div>
	<?=$this->input('headline'); ?>
	</div>

	<div>
	 <hr/>
	</div>

<?php } ?>


<?php if( !$this->editmode &&   (Tool::isFrontend()  || Tool::isFrontentRequestByAdmin() )) { ?>
	<?php
	//\Pimcore\Log\Simple::log("event.log", 'Inside view of queue::queueAction:: Not in Edit mode::Beginning ');

		$pagedata = $this->page_data;

		if (! isset($pagedata)) {
			$pagedata = new \stdClass();
		}

		//Set Page title
		$pagedata->title = new \stdClass();
        $pagedata->title->rendered=$this->input('pagename')->getData();
        $str = $pagedata->title->rendered;
        $delimiter = '-';
        $slug = strtolower(trim(preg_replace('/[\s-]+/', $delimiter, preg_replace('/[^A-Za-z0-9-]+/', $delimiter, preg_replace('/[&]/', 'and', preg_replace('/[\']/', '', iconv('UTF-8', 'ASCII//TRANSLIT', $str))))), $delimiter));
        $pagedata->slug = $slug;
				$pagedata->headline=$this->input('headline')->getData();


				$queue_solution_id = $pagedata->solutionid;


				$pagedata->documents = [];
				$pagedata->assets = [];
				$pagedata->objects = [];
				//$target_array = $pagedata->objects;
				$isObject = false;
				$isDocument = false;
				$isAsset = false;


				$entries = new DataObject\BzPublishQueue\Listing();
				$entries->setUnpublished(true);
				$entries->setCondition("NOT is_processed =  ? AND solution_id = ? ", [true, $queue_solution_id]);
				$bzQueueItems = $entries->load();


				foreach($bzQueueItems as $queueItem) {
					$response = new \stdClass();
					$response->o_id = $queueItem->o_id;
					$results2 = print_r($response, true);
					//\Pimcore\Log\Simple::log("event.log", 'Inside view of queue::queueAction:: response= '. $results2);

					$response->event_type = $queueItem->getEvent_type();

					$event_type_string = $queueItem->getEvent_type();

					if (strpos($event_type_string, 'DOCUMENT::') === 0) {
					   $isDocument = true;
						 $isAsset = false;
						 $isObject = false;
					}
					if (strpos($event_type_string, 'ASSET::') === 0) {
						 $isAsset = true;
						 $isObject = false;
						 $isDocument = false;
					}
					if (strpos($event_type_string, 'OBJECT::') === 0) {
						 $isObject = true;
						 $isDocument = false;
						 $isAsset = false;
					}

					$response->url = $queueItem->getUrl();
					$response->is_processed = $queueItem->getIs_processed();
					if ($isObject){
						array_push($pagedata->objects, $response);
					}
					if ($isDocument){
						array_push($pagedata->documents, $response);
					}
					if ($isAsset){
						array_push($pagedata->assets, $response);
					}
				}

    ?>

	<?php
		 $this->encoder = new Webservice\JsonEncoder();
		 $this->encoder->encode($pagedata);
	?>

<?php }?>
