<?php
/**
 * @var \Pimcore\Templating\PhpEngine $this
 * @var \Pimcore\Templating\PhpEngine $view
 * @var \Pimcore\Templating\GlobalVariables $app
 */
 use Pimcore\Model\Webservice;
 use Pimcore\Tool;


?>

<?php if($this->editmode) { ?>

<?php } ?>

<?php if( !$this->editmode &&   (Tool::isFrontend()  || Tool::isFrontentRequestByAdmin() )) { ?>

<?php
	$res = $this->response;
  $this->encoder = new Webservice\JsonEncoder();
  $this->encoder->encode($res);
?>
<?php } ?>
