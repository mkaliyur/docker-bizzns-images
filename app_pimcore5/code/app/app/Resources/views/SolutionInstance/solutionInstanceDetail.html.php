<?php
/**
 * @var \Pimcore\Templating\PhpEngine $this
 * @var \Pimcore\Templating\PhpEngine $view
 * @var \Pimcore\Templating\GlobalVariables $app
 */
	use Pimcore\Tool;
	use Pimcore\Model\Webservice;
	use Pimcore\Model\DataObject;
	use Pimcore\Model\DataObject\BzPublishQueue\Listing;
?>

<?php if($this->editmode) { ?>

	<style type="text/css">
		    body {
		        padding:0;
		        margin: 0;
		        font-family: "Lucida Sans Unicode", Arial;
		        font-size: 14px;
		    }

		    #site {
		        margin: 0 auto;
		        width: 600px;
		        padding: 30px 0 0 0;
		        color:#65615E;
		    }

		    h1, h2, h3 {
		        font-size: 18px;
		        padding: 0 0 5px 0;
		        border-bottom: 1px solid #001428;
		        margin-bottom: 5px;
		    }

		    h3 {
		        font-size: 14px;
		        padding: 15px 0 5px 0;
		        margin-bottom: 5px;
		        border-color: #cccccc;
		    }

		    img {
		        border: 0;
		    }

		    p {
		        padding: 0 0 5px 0;
		    }

		    a {
		        color: #000;
		    }

		    #logo {
		        text-align: center;
		        padding: 50px 0;
		    }

		    #logo hr {
		        display: block;
		        height: 1px;
		        overflow: hidden;
		        background: #BBB;
		        border: 0;
		        padding:0;
		        margin:30px 0 20px 0;
		    }

		    .claim {
		        text-transform: uppercase;
		        color:#BBB;
		    }

		    #site ul {
		        padding: 10px 0 10px 20px;
		        list-style: circle;
		    }

		    .alert {
		    	margin:30px 0 20px 0;
		    }

	</style>

	<div>Note: This page name is used in the scripts! If you change the name, you will need to change the template scripts </div>
	<div class="alert" >
	<div><label>Enter Page Name Here</label></div>
	<?=$this->input('pagename'); ?>
  </div>
	<div class="alert" >
	<div><label>Enter Page Headline Here</label></div>
	<?=$this->input('headline'); ?>
	</div>

	<div>
	 <hr/>
	</div>

<?php } ?>


<?php if( !$this->editmode &&   (Tool::isFrontend()  || Tool::isFrontentRequestByAdmin() )) { ?>
	<?php
		$pagedata = $this->page_data;
		if (! isset($pagedata)) {
			$pagedata = new \stdClass();
		}

    $sInstance = $pagedata->instanceObject;
    $response = new \stdClass();
    $response->o_id = $sInstance->o_id;
		$response->name = $sInstance->name;
		$response->folder = $sInstance->folder;
		$response->environment = $sInstance->environment;
		$response->initial_date_of_deployment = $sInstance->initial_date_of_deployment;
		$response->solution_href = $sInstance->solution_href;
		$response->ip_address = $sInstance->ip_address;
		$response->dns_name_of_the_box = $sInstance->dns_name_of_the_box;
		$response->url_of_the_solution = $sInstance->url_of_the_solution;
		$response->admin_url = $sInstance->admin_url;
		$response->ssh_user_login = $sInstance->ssh_user_login;
		$response->ssh_password = $sInstance->ssh_password;
		$response->box_provider = $sInstance->box_provider;
		$response->box_provider_url = $sInstance->box_provider_url;
		$response->box_provider_login = $sInstance->box_provider_login;
		$response->instance_git_repository_url = $sInstance->instance_git_repository_url;

    ?>

	<?php
		 $this->encoder = new Webservice\JsonEncoder();
		 $this->encoder->encode($response);
	?>

<?php }?>
