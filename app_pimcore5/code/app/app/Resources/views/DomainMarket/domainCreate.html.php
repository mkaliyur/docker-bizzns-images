<?php
/**
 * @var \Pimcore\Templating\PhpEngine $this
 * @var \Pimcore\Templating\PhpEngine $view
 * @var \Pimcore\Templating\GlobalVariables $app
 */
 use Pimcore\Model\Webservice;
 use Pimcore\Tool;
 use Website\Controller\Action;
 use Pimcore\Controller\FrontendController;
 use Pimcore\Model\Asset;
 use Pimcore\Model\Document;
 use Pimcore\Model\DataObject;
 use Pimcore\Model\Element;
 use Symfony\Component\HttpFoundation\Request;
 use Symfony\Component\HttpFoundation\Response;
 use Pimcore\Model\DataObject\BzPublishQueue\Listing;
 use Pimcore\Model\DataObject\BzDomain;
 use Pimcore\Model\DataObject\BzSolutions;
 use Pimcore\Model\Object\Folder;

?>

<?php if($this->editmode) { ?>
  <style type="text/css">
        body {
            padding:0;
            margin: 0;
            font-family: "Lucida Sans Unicode", Arial;
            font-size: 14px;
        }

        #site {
            margin: 0 auto;
            width: 600px;
            padding: 30px 0 0 0;
            color:#65615E;
        }

        h1, h2, h3 {
            font-size: 18px;
            padding: 0 0 5px 0;
            border-bottom: 1px solid #001428;
            margin-bottom: 5px;
        }

        h3 {
            font-size: 14px;
            padding: 15px 0 5px 0;
            margin-bottom: 5px;
            border-color: #cccccc;
        }

        img {
            border: 0;
        }

        p {
            padding: 0 0 5px 0;
        }

        a {
            color: #000;
        }

        #logo {
            text-align: center;
            padding: 50px 0;
        }

        #logo hr {
            display: block;
            height: 1px;
            overflow: hidden;
            background: #BBB;
            border: 0;
            padding:0;
            margin:30px 0 20px 0;
        }

        .claim {
            text-transform: uppercase;
            color:#BBB;
        }

        #site ul {
            padding: 10px 0 10px 20px;
            list-style: circle;
        }

        .alert {
          margin:30px 0 20px 0;
        }

  </style>

  <div><h2>Enter the Domain Name Below</h2></div>
  <div class="alert" >
  <div ><label><h2>Enter Page Name Here</h2></label></div>
  <?=$this->input('pagename'); ?>
  </div>
  <div>
  <hr/>
  </div>

  <div class="alert" >
  <div><label><h2>Enter Domain Name Here</h2></label></div>
  <?=$this->input('domain_name'); ?>
  </div>

  <div>
  <hr/>
  </div>

<?php } ?>

<?php if( !$this->editmode &&  (Tool::isFrontend() || Tool::isFrontentRequestByAdmin() )) {?>
 <?php
    //\Pimcore\Log\Simple::log("event.log", 'Inside View of Domain create ::Beginning');

     $domain_name_frontend = $this->input("domain_name")->getData();
     //$results2 = print_r($domain_name_frontend, true);
     //\Pimcore\Log\Simple::log("event.log", 'Inside View of Domain create ::domain_name_frontend= '.$results2);

     $domain_name_frontend_exists = DataObject::getByPath("/organization/Domains//".$domain_name_frontend);
     $master_domain_name_exists = DataObject::getByPath("/library/master//".$domain_name_frontend);

     // if(!is_null($domain_name_frontend_exists)){
     //   \Pimcore\Log\Simple::log("event.log", 'Inside View of Domain create ::Inside First if');
     //
     //   $response = new \stdClass();
     //   $response->status = "Error";
     //   $response->message = "This Domain Name ".$domain_name_frontend ." already exists first if.";
     //   $response_encoder = new Webservice\JsonEncoder();
     //   return $response_encoder->encode($response);;
     // }else{
     //   \Pimcore\Log\Simple::log("event.log", 'Inside View of Domain create ::Inside First else domain_name_frontend_exists is null');
     // }
     //
     // if(!is_null($master_domain_name_exists)){
     //   \Pimcore\Log\Simple::log("event.log", 'Inside View of Domain create ::Inside Second if');
     //   $response = new \stdClass();
     //   $response->status = "Error";
     //   $response->message = "This Master Domain Name ".$domain_name_frontend ." already exists second if.";
     //   $response_encoder = new Webservice\JsonEncoder();
     //   return $response_encoder->encode($response);
     // }else{
     //   \Pimcore\Log\Simple::log("event.log", 'Inside View of Domain create ::Inside Second else master_domain_name_exists is null');
     //
     // }


      $bz_domain_name = new DataObject\BzDomain();
      $bz_domain_name->setKey($domain_name_frontend);
      $bz_domain_name->setName($domain_name_frontend.'_domain');
      $parent_path = "/organization/Domains";
      $parent_folder_path = DataObject::getByPath($parent_path);
      $parent_folder_id = $parent_folder_path->o_id;
      $bz_domain_name->setParentId($parent_folder_id);
      $domain_name_created_exists = DataObject::getByPath("/organization/Domains//".$domain_name_frontend);
      if(is_null($domain_name_created_exists) || empty($domain_name_created_exists)){
        //\Pimcore\Log\Simple::log("event.log", 'Inside View of Domain create ::Inside if of domain_name_created_exists');
        $bz_domain_name->save();
      }
      //\Pimcore\Log\Simple::log("event.log", 'Inside View of Domain create ::After saving bz_domain_name= '.$bz_domain_name);

      //$response = new \stdClass();
      //$response->status = "Success";
      //$response->message = "This Domain Name ".$domain_name_frontend ." created successfully.";

      $bz_library_master = new Folder();
      $bz_library_master->setKey($domain_name_frontend);
      $parent_path = "/library/master";
      $parent_folder_path = DataObject::getByPath($parent_path);
      $parent_folder_id = $parent_folder_path->o_id;
      $bz_library_master->setParentId($parent_folder_id);
      //\Pimcore\Log\Simple::log("event.log", 'Inside View of Domain create ::About to save bz_library_master= '.$bz_library_master);
      $master_name_created_exists = DataObject::getByPath("/library/master//".$domain_name_frontend);
      if(is_null($master_name_created_exists) || empty($master_name_created_exists)){
        //\Pimcore\Log\Simple::log("event.log", 'Inside View of Domain create ::Inside if of master_name_created_exists');
        $bz_library_master->save();
      }
      // $response = new \stdClass();
      // $response->status = "Success";
      // $response->message = "This Master Name ".$bz_library_master ." created successfully.";
      // return $response;


      $copyObjectSource = DataObject::getByPath("/library/master/Global");
      $parent_path = "/library/master/".$domain_name_frontend;
      $copyObjectTarget = DataObject::getByPath($parent_path);
      //\Pimcore\Log\Simple::log("event.log", 'Inside View of Domain create ::Before copyRecursive ');
      $user = \Pimcore\Tool\Admin::getCurrentUser();
      //$results2 = print_r($user, true);
      //\Pimcore\Log\Simple::log("event.log", 'Inside View of Domain create ::user= '.$results2);
      $service = new DataObject\Service($user);
      $copyObject = $service->copyRecursive($copyObjectTarget, $copyObjectSource);
      $parent_folder_id = $copyObjectTarget->o_id;
      $copyObject->setParentId($parent_folder_id);
      //\Pimcore\Log\Simple::log("event.log", 'Inside View of Domain create ::About to save copyObject= '.$copyObject);
      $copyObject->save();
      //\Pimcore\Log\Simple::log("event.log", 'Inside View of Domain create ::After saving copyObject= '.$copyObject);
      $response = new \stdClass();
      $response->status = "Success";
      $response->message = "This Child Object ".$copyObject ." created successfully.";
      //return $response;


      $document_domain_folder = new Document\Folder();
      $document_domain_folder->setKey($domain_name_frontend);
      $parent_path = "/domains";
      $parent_path_folder = Document::getByPath($parent_path);
      $parent_path_folder_id = $parent_path_folder->id;
      //$results2 = print_r($parent_path_folder_id, true);
      //\Pimcore\Log\Simple::log("event.log", 'Inside View of Domain create ::parent_path_folder_id= '.$results2);

      $document_domain_folder->setParentId($parent_path_folder_id);
      $master_doc_domain_name_created_exists = Document::getByPath("/domains//".$domain_name_frontend);
      if(is_null($master_doc_domain_name_created_exists) || empty($master_doc_domain_name_created_exists)){
      $document_domain_folder->save();
      }
      //\Pimcore\Log\Simple::log("event.log", 'Inside View of Domain create ::After saving document document_domain_folder= '.$document_domain_folder);

      $copyDocumentSource = Document::getByPath("/library/Global");
      $document_parent_path = "/domains//".$domain_name_frontend;
      $copyDocumentTarget = Document::getByPath($document_parent_path);
      //\Pimcore\Log\Simple::log("event.log", 'Inside View of Domain create ::copyDocument1 ');

      $user1 = \Pimcore\Tool\Admin::getCurrentUser();
      //$results2 = print_r($user1, true);
      //\Pimcore\Log\Simple::log("event.log", 'Inside View of Domain create ::user1 '.$results2);

      $service1 = new Document\Service($user1);
      //\Pimcore\Log\Simple::log("event.log", 'Inside View of Domain create ::copyDocument3 ');
      try{
        $copyDocument = $service1->copyRecursive($copyDocumentTarget, $copyDocumentSource);
      }catch(Exception $e){
        //\Pimcore\Log\Simple::log("event.log", 'Inside View of Domain create  inside CAtch::copyDocument4 ');
      }

      $parent_folder_id = $copyDocumentTarget->id;
      $copyDocument->setParentId($parent_folder_id);
      //\Pimcore\Log\Simple::log("event.log", 'Inside View of Domain create ::About to save copyDocument ');
      $copyDocument->save();

      //\Pimcore\Log\Simple::log("event.log", 'Inside View of Domain create ::After saving document copyDocument= ');
      $response = new \stdClass();
      $response->status = "Success";
      $response->message = "This Document Folder ".$domain_name_frontend ." created successfully.";

 ?>

 <?php
    //\Pimcore\Log\Simple::log("event.log", 'Inside View of Domain create ::Returning encoder response');
    $this->encoder = new Webservice\JsonEncoder();
    $this->encoder->encode($response);
 ?>

<?php } ?>
