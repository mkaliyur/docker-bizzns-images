<?php
/**
 * @var \Pimcore\Templating\PhpEngine $this
 * @var \Pimcore\Templating\PhpEngine $view
 * @var \Pimcore\Templating\GlobalVariables $app
 */
 use Pimcore\Model\Webservice;
 use Pimcore\Tool;
 use Website\Controller\Action;
 use Pimcore\Controller\FrontendController;
 use Pimcore\Model\Asset;
 use Pimcore\Model\Document;
 use Pimcore\Model\DataObject;
 use Pimcore\Model;
 use Symfony\Component\HttpFoundation\Request;
 use Symfony\Component\HttpFoundation\Response;
 use Pimcore\Model\DataObject\BzPublishQueue\Listing;
 use Pimcore\Model\DataObject\BzDomain;
 use Pimcore\Model\DataObject\BzSolutions;
 use Pimcore\Model\Object\Folder;

?>

<?php if($this->editmode) { ?>
  <style type="text/css">
        body {
            padding:0;
            margin: 0;
            font-family: "Lucida Sans Unicode", Arial;
            font-size: 14px;
        }

        #site {
            margin: 0 auto;
            width: 600px;
            padding: 30px 0 0 0;
            color:#65615E;
        }

        h1, h2, h3 {
            font-size: 18px;
            padding: 0 0 5px 0;
            border-bottom: 1px solid #001428;
            margin-bottom: 5px;
        }

        h3 {
            font-size: 14px;
            padding: 15px 0 5px 0;
            margin-bottom: 5px;
            border-color: #cccccc;
        }

        img {
            border: 0;
        }

        p {
            padding: 0 0 5px 0;
        }

        a {
            color: #000;
        }

        #logo {
            text-align: center;
            padding: 50px 0;
        }

        #logo hr {
            display: block;
            height: 1px;
            overflow: hidden;
            background: #BBB;
            border: 0;
            padding:0;
            margin:30px 0 20px 0;
        }

        .claim {
            text-transform: uppercase;
            color:#BBB;
        }

        #site ul {
            padding: 10px 0 10px 20px;
            list-style: circle;
        }

        .alert {
          margin:30px 0 20px 0;
        }

  </style>

  <div><h2>Enter the Domain Name Below</h2></div>
  <div class="alert" >
  <div ><label><h2>Enter Page Name Here</h2></label></div>
  <?=$this->input('pagename'); ?>
  </div>
  <div>
  <hr/>
  </div>

  <div class="alert" >
  <div><label><h2>Enter Domain Name Here</h2></label></div>
  <?=$this->input('domain_name'); ?>
  </div>

  <div>
  <hr/>
  </div>

<?php } ?>

<?php if( !$this->editmode &&  (Tool::isFrontend() || Tool::isFrontentRequestByAdmin() )) {?>
 <?php


 ?>
 <?php
    $this->encoder = new Webservice\JsonEncoder();
    $this->encoder->encode($response);
 ?>

<?php } ?>
