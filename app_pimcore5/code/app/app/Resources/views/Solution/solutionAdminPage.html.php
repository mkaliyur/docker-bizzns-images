<?php
/**
 * @var \Pimcore\Templating\PhpEngine $this
 * @var \Pimcore\Templating\PhpEngine $view
 * @var \Pimcore\Templating\GlobalVariables $app
 */
 use Pimcore\Model\Webservice;
 use Pimcore\Tool;
 use Website\Controller\Action;
 use Pimcore\Controller\FrontendController;
 use Pimcore\Model\Asset;
 use Pimcore\Model\Document;
 use Pimcore\Model\DataObject;
 use Pimcore\Model;
 use Symfony\Component\HttpFoundation\Request;
 use Symfony\Component\HttpFoundation\Response;
 use Pimcore\Model\DataObject\BzPublishQueue\Listing;
 use Pimcore\Model\DataObject\BzDomain;
 use Pimcore\Model\DataObject\BzSolutions;
 use Pimcore\Model\Object\Folder;

?>

<?php if($this->editmode) { ?>
  <style type="text/css">
        body {
            padding:0;
            margin: 0;
            font-family: "Lucida Sans Unicode", Arial;
            font-size: 14px;
        }

        #site {
            margin: 0 auto;
            width: 600px;
            padding: 30px 0 0 0;
            color:#65615E;
        }

        h1, h2, h3 {
            font-size: 18px;
            padding: 0 0 5px 0;
            border-bottom: 1px solid #001428;
            margin-bottom: 5px;
        }

        h3 {
            font-size: 14px;
            padding: 15px 0 5px 0;
            margin-bottom: 5px;
            border-color: #cccccc;
        }

        img {
            border: 0;
        }

        p {
            padding: 0 0 5px 0;
        }

        a {
            color: #000;
        }

        #logo {
            text-align: center;
            padding: 50px 0;
        }

        #logo hr {
            display: block;
            height: 1px;
            overflow: hidden;
            background: #BBB;
            border: 0;
            padding:0;
            margin:30px 0 20px 0;
        }

        .claim {
            text-transform: uppercase;
            color:#BBB;
        }

        #site ul {
            padding: 10px 0 10px 20px;
            list-style: circle;
        }

        .alert {
          margin:30px 0 20px 0;
        }

  </style>

  <div><h2>Select the Domain Name and Market Name Below.</h2></div>
  <div class="alert" >
  <div><label><h2>Enter Page Name Here</h2></label></div>
  <?=$this->input('pagename'); ?>
  </div>
  <div>
  <hr/>
  </div>


  <?php
    $domainList = new DataObject\BzDomain\Listing();
    $domainList->setUnpublished(true);
    $domainListPath = "/organization/Domains/";
    $domainList->setCondition("o_path =  ? ", [$domainListPath]);
    $domainListObject = $domainList->load();

    $storeArray = [];
     foreach($domainListObject as $domainListName) {
       $domainListName1 = $domainListName->getName();
       $domainListName2 = $domainListName->getStorageValue();

       $storeArrayOptions = [$domainListName1, $domainListName2];
       array_push($storeArray, $storeArrayOptions);
     }
  ?>
  <section>
  <h2>Select Domain Name Here</h2>
    <?= $this->select("domain_name", [
        "store" => $storeArray
    ]);
    if($this->select("domain_name")->isEmpty()):
     $this->select("domain_name")->setDataFromResource("Select");
    endif;
    ?>
  </section>
  <div>
  <hr/>
  </div>

  <?php
    $marketList = new DataObject\BzMarketArea\Listing();
    $marketList->setUnpublished(true);
    $marketListPath = "/organization/MarketArea/";
    $marketList->setCondition("o_path =  ? ", [$marketListPath]);
    $marketListObject = $marketList->load();

    $storeArray = [];
     foreach($marketListObject as $marketListName) {
       $marketListName1 = $marketListName->getName();
       $marketListName2 = $marketListName->getTitle();

       $storeArrayOptions = [$marketListName1, $marketListName2];
       array_push($storeArray, $storeArrayOptions);
     }
  ?>

  <section>
  <h2>Select Market Name Here</h2>
    <?= $this->select("market_name", [
        "store" => $storeArray
    ]);
      if($this->select("market_name")->isEmpty()):
       $this->select("market_name")->setDataFromResource("Select");
      endif;
   ?>
  </section>

  <div>
  <hr/>
  </div>

  <div class="alert" >
  <div><label><h2>Enter the Solution Name Here.</h2></label></div>
  <?=$this->input('solutionname'); ?>
  </div>
  <div>
  <hr/>
  </div>


<?php } ?>

<?php if( !$this->editmode &&  (Tool::isFrontend() || Tool::isFrontentRequestByAdmin() )) {?>
 <?php

     $domain_name_frontend = $this->select("domain_name")->getData();
     $market_name_frontend = $this->select("market_name")->getData();
     $solution_name_frontend = $this->input("solutionname")->getData();

     $solution_name_frontend_path = "/solution2//".$solution_name_frontend;
     $solution_name_frontend_exists = Folder::getBypath($solution_name_frontend_path);

     if(!is_null($solution_name_frontend_exists)){
       $response = new \stdClass();
       $response->status = "Error";
       $response->message = "This Solution Name ".$solution_name_frontend ." already exists.";
       $response_encoder = new Webservice\JsonEncoder();
       return $response_encoder->encode($response);;
     }else{
       try{
         $this->action("solutionCreate", "Solution", null, ["domainname" => $domain_name_frontend,"marketname"=>$market_name_frontend]);
       }catch(Exception $e){

       }
       $response = new \stdClass();
       $response->status = "Success";
       $response->message = "This Solution Name ".$solution_name_frontend ." created successfully.";

     }



    // $results2 = print_r($ret2, true);
    // \Pimcore\Log\Simple::log("event.log", 'Inside soln page second ret2='.$results2);


    // $results2 = print_r($domain_name, true);
    // \Pimcore\Log\Simple::log("event.log", 'Inside soln page domain_name1='.$results2);
    //
    // if(is_null($domain_name)){
    //   $domain_name = $this->select("domain_name")->getData();
    //   $results2 = print_r($domain_name, true);
    //   \Pimcore\Log\Simple::log("event.log", 'Inside soln page domain_name2='.$results2);
    // }


	// $res = $this->response;
  // $this->encoder = new Webservice\JsonEncoder();
  // $this->encoder->encode($res);
 ?>
 <?php
    $this->encoder = new Webservice\JsonEncoder();
    $this->encoder->encode($response);
 ?>

<?php } ?>
