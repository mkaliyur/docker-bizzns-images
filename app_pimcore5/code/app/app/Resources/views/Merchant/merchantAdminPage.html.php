<?php
/**
 * @var \Pimcore\Templating\PhpEngine $this
 * @var \Pimcore\Templating\PhpEngine $view
 * @var \Pimcore\Templating\GlobalVariables $app
 */
 use Pimcore\Model\Webservice;
 use Pimcore\Tool;
 use Website\Controller\Action;
 use Pimcore\Controller\FrontendController;
 use Pimcore\Model\Asset;
 use Pimcore\Model\Document;
 use Pimcore\Model\DataObject;
 use Pimcore\Model;
 use Symfony\Component\HttpFoundation\Request;
 use Symfony\Component\HttpFoundation\Response;
 use Pimcore\Model\DataObject\BzPublishQueue\Listing;
 use Pimcore\Model\DataObject\BzDomain;
 use Pimcore\Model\DataObject\BzSolutions;
 use Pimcore\Model\Object\Folder;

?>

<?php if($this->editmode) { ?>
  <style type="text/css">
        body {
            padding:0;
            margin: 0;
            font-family: "Lucida Sans Unicode", Arial;
            font-size: 14px;
        }

        #site {
            margin: 0 auto;
            width: 600px;
            padding: 30px 0 0 0;
            color:#65615E;
        }

        h1, h2, h3 {
            font-size: 18px;
            padding: 0 0 5px 0;
            border-bottom: 1px solid #001428;
            margin-bottom: 5px;
        }

        h3 {
            font-size: 14px;
            padding: 15px 0 5px 0;
            margin-bottom: 5px;
            border-color: #cccccc;
        }

        img {
            border: 0;
        }

        p {
            padding: 0 0 5px 0;
        }

        a {
            color: #000;
        }

        #logo {
            text-align: center;
            padding: 50px 0;
        }

        #logo hr {
            display: block;
            height: 1px;
            overflow: hidden;
            background: #BBB;
            border: 0;
            padding:0;
            margin:30px 0 20px 0;
        }

        .claim {
            text-transform: uppercase;
            color:#BBB;
        }

        #site ul {
            padding: 10px 0 10px 20px;
            list-style: circle;
        }

        .alert {
          margin:30px 0 20px 0;
        }

  </style>

  <div><h2>Select the Solution Name and Merchant Name Below.</h2></div>
  <div class="alert" >
  <div ><label><h2>Enter Page Name Here</h2></label></div>
  <?=$this->input('pagename'); ?>
  </div>
  <div>
  <hr/>
  </div>

 <?php
   $solutionList = new DataObject\BzSolutions\Listing();
   $solutionList->setUnpublished(true);
   $solutionListPath = "/organization/Solutions/";
   $solutionList->setCondition("o_path =  ? ", [$solutionListPath]);
   $solutionListObject = $solutionList->load();

   $storeArray = [];
    foreach($solutionListObject as $solutionListName) {
      $solutionListName1 = $solutionListName->getName();
      $solutionListName2 = $solutionListName->getDisplay_name();

      $storeArrayOptions = [$solutionListName1, $solutionListName2];
      array_push($storeArray, $storeArrayOptions);
    }
 ?>
  <section>
    <h2>Select Solution Name Here</h2>
      <?= $this->select("solution_name", [
          "store" => $storeArray
      ]);
   ?>
  </section>
  <div>
  <hr/>
  </div>


  <div class="alert" >
  <div ><label><h2>Enter Merchant Name Here</h2></label></div>
  <?=$this->input('merchant_name'); ?>
  </div>

  <div>
  <hr/>
  </div>


<?php } ?>

<?php if( !$this->editmode &&  (Tool::isFrontend() || Tool::isFrontentRequestByAdmin() )) {?>
 <?php
    //$results2 = print_r($availableUserPermissions, true);
    //\Pimcore\Log\Simple::log("event.log", 'Inside solution page Editmode');

     $solution_name_frontend = $this->select("solution_name")->getData();
     $merchant_name_frontend = $this->input("merchant_name")->getData();

     $merchant_name_frontend_path = "/solution2//".$solution_name_frontend."/parent_".$solution_name_frontend."/Merchants//".$merchant_name_frontend;
     $merchant_name_frontend_exists = Dataobject::getBypath($merchant_name_frontend_path);

     if(!is_null($merchant_name_frontend_exists)){
       $response = new \stdClass();
       $response->status = "Error";
       $response->message = "This Merchant Name ".$merchant_name_frontend ." already exists.";
       $response_encoder = new Webservice\JsonEncoder();
       return $response_encoder->encode($response);;
     }else{
     try{
       $this->action("merchantCreate", "Merchant", null, ["solution_name" => $solution_name_frontend,"merchant_name"=>$merchant_name_frontend]);
     }
     catch(Exception $e){
       //$results2 = print_r($availableUserPermissions, true);
       //\Pimcore\Log\Simple::log("event.log", 'Inside solution page catch'.$e->getMessage());
     }
     $response = new \stdClass();
     $response->status = "Success";
     $response->message = "This Merchant Name ".$merchant_name_frontend ." created successfully.";

   }

 ?>
<?php } ?>
