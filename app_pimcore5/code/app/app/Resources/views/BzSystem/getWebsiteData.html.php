<?php
/**
 * @var \Pimcore\Templating\PhpEngine $this
 * @var \Pimcore\Templating\PhpEngine $view
 * @var \Pimcore\Templating\GlobalVariables $app
 */
	use Pimcore\Tool;
	use Pimcore\Model\Webservice;
?>



<?php if($this->editmode) { ?>

<?php } ?>


<?php if( !$this->editmode) { ?>

	<?php  
		$pagedata = $this->pagedata;   

		if (! isset($pagedata)) {
			$pagedata = new \stdClass(); 
		} 
    ?>
	<?php 
		$this->encoder = new Webservice\JsonEncoder();
		$this->encoder->encode($pagedata);
	?>

<?php } ?>
