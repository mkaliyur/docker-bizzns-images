<?php
	use Pimcore\Tool;
	use Pimcore\Model\Webservice;
?>



<?php if($this->editmode) { ?>
    <div class="alert alert-info"> 
         <h2>This controller cannot be used in EDIT mode! Used with static route for listing data</h2>
    </div>
<?php } ?>

 

<?php if( !$this->editmode &&   (Tool::isFrontend()  || Tool::isFrontentRequestByAdmin() )) { ?>
	<?php  

		$pagedata = $this->pagedata;   

		if (! isset($pagedata)) {
			$pagedata = new \stdClass(); 
		} 


    ?>



	<?php 
		$this->encoder = new Webservice\JsonEncoder();
		$this->encoder->encode($pagedata);
	?>
      
<?php } ?>

 