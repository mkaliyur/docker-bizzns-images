<?php
/**
 * @var \Pimcore\Templating\PhpEngine $this
 * @var \Pimcore\Templating\PhpEngine $view
 * @var \Pimcore\Templating\GlobalVariables $app
 */
	use Pimcore\Tool;
	use Pimcore\Model\Webservice;
	use Symfony\Component\HttpFoundation\Response;
	use Symfony\Component\HttpFoundation\JsonResponse;

?>



<?php if($this->editmode) { ?>

	<style type="text/css">
		    body {
		        padding:0;
		        margin: 0;
		        font-family: "Lucida Sans Unicode", Arial;
		        font-size: 14px;
		    }

		    #site {
		        margin: 0 auto;
		        width: 600px;
		        padding: 30px 0 0 0;
		        color:#65615E;
		    }

		    h1, h2, h3 {
		        font-size: 18px;
		        padding: 0 0 5px 0;
		        border-bottom: 1px solid #001428;
		        margin-bottom: 5px;
		    }

		    h3 {
		        font-size: 14px;
		        padding: 15px 0 5px 0;
		        margin-bottom: 5px;
		        border-color: #cccccc;
		    }

		    img {
		        border: 0;
		    }

		    p {
		        padding: 0 0 5px 0;
		    }

		    a {
		        color: #000;
		    }

		    #logo {
		        text-align: center;
		        padding: 50px 0;
		    }

		    #logo hr {
		        display: block;
		        height: 1px;
		        overflow: hidden;
		        background: #BBB;
		        border: 0;
		        padding:0;
		        margin:30px 0 20px 0;
		    }

		    .claim {
		        text-transform: uppercase;
		        color:#BBB;
		    }

		    #site ul {
		        padding: 10px 0 10px 20px;
		        list-style: circle;
		    }

		    .alert {
		    	margin:30px 0 20px 0;
		    }

	</style>

	<div>Note: This page name is used in the scripts! If you change the name, you will need to change the template scripts </div>
	<div class="alert" >
	<div ><label>Enter Page Name Here</label></div>
	<?=$this->input('pagename'); ?>
  </div>
	<div class="alert" >
	<div ><label>Enter Page Headline Here</label></div>
	<?=$this->input('headline'); ?>
	</div>

	<div class="alert" >
	<div ><label>Enter Domain Name Here</label></div>
	<?=$this->input('domainname'); ?>
	</div>

	<div class="alert" >
	<div ><label>Enter Market Name Here</label></div>
	<?=$this->input('marketname'); ?>
	</div>


	<div>
	<hr/>
	</div>

	<div> Drag and drop page sections in the box below! </div>
    <div class="alert alert-info">
    <?=
    	$this->multihref("componentPaths", [
			"types" => ["document"],
        "subtypes" => [
            "document" => ["snippet"]
         ]
		]);
		?>
  </div>
<?php } ?>


<?php if( !$this->editmode &&   (Tool::isFrontend()  || Tool::isFrontentRequestByAdmin() )) { ?>
	<?php
	  $domainname = $this->input('domainname')->getData();
		$marketname = $this->input('marketname')->getData();


		//$pagedata = $this->getParam('page_data');
		$pagedata = $this->page_data;
		$results2 = print_r($pagedata, true);
		\Pimcore\Log\Simple::log("event.log", 'Inside view of BZPAGE View Receiving pagedata results2 pagedata = '.$results2);

		if (! isset($pagedata)) {
			$pagedata = new \stdClass();
		}
		$pagedata->indexing_metadata = new \stdClass();
		$index = $this->document->getProperty('solution_name');

		$pagedata->indexing_metadata->_index = $index;
		$pagedata->indexing_metadata->_type = "pages";
		$pagedata->indexing_metadata->_id = "home-page";



		//Set Page title
		$pagedata->title=new \stdClass();
        $pagedata->title->rendered=$this->input('pagename')->getData();

        $str = $pagedata->title->rendered;
        $delimiter = '-';
        $slug = strtolower(trim(preg_replace('/[\s-]+/', $delimiter, preg_replace('/[^A-Za-z0-9-]+/', $delimiter, preg_replace('/[&]/', 'and', preg_replace('/[\']/', '', iconv('UTF-8', 'ASCII//TRANSLIT', $str))))), $delimiter));

        $pagedata->slug = $slug;


				$pagedata->headline=$this->input('headline')->getData();

        //$pagedata->test = new \stdClass();
        $count=0;

        //$logger->error($mhref);

        $pageComponents = array();

        foreach($this->multihref("componentPaths") as $element ):
            //$propertyName = 'component'.$count;
            //$pagedata->test->$propertyName = $element->getId() ;
            $pageComponents[$count] = $element->getId();
            $count++;
        endforeach;

        //$this->view->page_data = $pagedata;

        $arrLength = count($pageComponents);
         $message =" IN page - arrLength being processed is " . $arrLength;
         //\Pimcore\Log\Simple::log("event.log", $message);
        for($x = 0; $x < $arrLength; $x++) {
            $id = $pageComponents[$x];
            //\Pimcore\Log\Simple::log("event.log", "before about to include id == ".$id);
            $this->inc($id, ["page_data" => $pagedata,"domainname" => $domainname, "marketname" => $marketname],false  );
						//\Pimcore\Log\Simple::log("event.log", "after call to include id == ".$id);
        }

    ?>

	<?php
	  $this->encoder = new Webservice\JsonEncoder();
		$this->encoder->encode($pagedata);
	 ?>

<?php } ?>
