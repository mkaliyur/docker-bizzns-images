<?php

namespace localMigration;

include_once __DIR__ . "/vendor/autoload.php";

\Pimcore\Bootstrap::startupCli();

use Pimcore\Bundle\EcommerceFrameworkBundle\PimcoreEcommerceFrameworkBundle;
use Pimcore\Db;
use Pimcore\Migrations\InstallVersion;
use Pimcore\Model\Redirect;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\Yaml\Yaml;

/**
 * This script contains all update scripts which were introduced during Pimcore 5 development and are needed when migrating
 * from Pimcore 4 to 5. After upgrading to Pimcore 5, run this scripts with.
 */
$migrations = [];

// queries as documented in migration guide
$migrations['pre update scripts'] = function () {
    $db     = Db::get();
    $schema = $db->getSchemaManager()->createSchema();

    if (!$schema->getTable('documents_page')->hasColumn('legacy')) {
        $db->query('ALTER TABLE `documents_page` ADD COLUMN `legacy` TINYINT(1) NULL AFTER `personas`');
    }

    if (!$schema->getTable('documents_snippet')->hasColumn('legacy')) {
        $db->query('ALTER TABLE `documents_snippet` ADD COLUMN `legacy` TINYINT(1) NULL AFTER `contentMasterDocumentId`');
    }

    if (!$schema->getTable('documents_newsletter')->hasColumn('legacy')) {
        $db->query('ALTER TABLE `documents_newsletter` ADD COLUMN `legacy` TINYINT(1) NULL');
    }

    if (!$schema->getTable('documents_printpage')->hasColumn('legacy')) {
        $db->query('ALTER TABLE `documents_printpage` ADD COLUMN `legacy` TINYINT(1) NULL');
    }

    if (!$schema->getTable('documents_email')->hasColumn('legacy')) {
        $db->query('ALTER TABLE `documents_email` ADD COLUMN `legacy` TINYINT(1) NULL');
    }

    $db->query("ALTER TABLE `translations_website` CHANGE COLUMN `key` `key` VARCHAR(190) NOT NULL DEFAULT '' COLLATE 'utf8mb4_bin'");
    $db->query("ALTER TABLE `translations_admin` CHANGE COLUMN `key` `key` VARCHAR(190) NOT NULL DEFAULT '' COLLATE 'utf8mb4_bin'");
};

// build 48
$migrations[48] = function () {
    $db     = Db::get();
    $schema = $db->getSchemaManager()->createSchema();

    if (!$schema->getTable('schedule_tasks')->hasColumn('version')) {
        $db->query('ALTER TABLE `schedule_tasks` ADD INDEX `version` (`version`);');
    }
};

// build 54
$migrations[54] = function () {
    $fs = new Filesystem();

    $configFile = implode(DIRECTORY_SEPARATOR, [
        PIMCORE_APP_ROOT,
        'config',
        'local',
        'update_54_legacy_naming.yml'
    ]);

    $configData = [
        'pimcore' => [
            'documents' => [
                'editables' => [
                    'naming_strategy' => 'legacy'
                ]
            ]
        ]
    ];

    try {
        $yaml = Yaml::dump($configData, 100);
        $yaml = '# created by build 54 - see https://github.com/pimcore/pimcore/issues/1467' . "\n" . $yaml;

        $fs->dumpFile($configFile, $yaml);
    } catch (\Exception $e) {
        echo sprintf(PHP_EOL . 'ERROR: Failed to write YML config to %s: %s' . PHP_EOL, $configFile, $e->getMessage()) . PHP_EOL;
        echo <<<EOF
Please add the following configuration manually:
-----------------------------------------
pimcore:
    documents:
        editables:
            naming_strategy: legacy
-----------------------------------------
EOF;
    }
};

// build 66
$migrations[66] = function () {
    $list = new \Pimcore\Model\DataObject\Objectbrick\Definition\Listing();
    $list = $list->load();

    if (is_array($list)) {
        foreach ($list as $brickDefinition) {
            if ($brickDefinition instanceof \Pimcore\Model\DataObject\Objectbrick\Definition) {
                $classDefinitions = $brickDefinition->getClassDefinitions();

                if (is_array($classDefinitions)) {
                    foreach ($classDefinitions as &$classDefinition) {
                        $definition = \Pimcore\Model\DataObject\ClassDefinition::getById($classDefinition['classname']);

                        $classDefinition['classname'] = $definition->getName();
                    }
                }

                $brickDefinition->setClassDefinitions($classDefinitions);
                $brickDefinition->save();
            }
        }
    }
};

// build 74
$migrations[74] = function () {
    $db = Db::get();
    $db->query("ALTER TABLE `documents_link` CHANGE COLUMN `internalType` `internalType` ENUM('document','asset','object') NULL DEFAULT NULL AFTER `id`;");
};

// build 90
$migrations[90] = function () {
    $db = Db::get();
    $db->query('ALTER TABLE `tags` CHANGE COLUMN `idPath` `idPath` VARCHAR(190) NULL DEFAULT NULL;');
    $db->query('ALTER TABLE `tracking_events` CHANGE COLUMN `category` `category` VARCHAR(190) NULL DEFAULT NULL;');
    $db->query('ALTER TABLE `tracking_events` CHANGE COLUMN `action` `action` VARCHAR(190) NULL DEFAULT NULL;');
    $db->query('ALTER TABLE `tracking_events` CHANGE COLUMN `label` `label` VARCHAR(190) NULL DEFAULT NULL;');
    $db->query('ALTER TABLE `tracking_events` CHANGE COLUMN `data` `data` VARCHAR(190) NULL DEFAULT NULL;');
    $db->query('ALTER TABLE `users` CHANGE COLUMN `password` `password` VARCHAR(190) NULL DEFAULT NULL;');
    $db->query("ALTER TABLE `website_settings` CHANGE COLUMN `name` `name` VARCHAR(190) NULL DEFAULT '';");
    $db->query('ALTER TABLE `classificationstore_stores` CHANGE COLUMN `name` `name` VARCHAR(190) NULL DEFAULT NULL;');
    $db->query("ALTER TABLE `classificationstore_groups` CHANGE COLUMN `name` `name` VARCHAR(190) NULL DEFAULT '';");
    $db->query("ALTER TABLE `classificationstore_keys` CHANGE COLUMN `name` `name` VARCHAR(190) NULL DEFAULT '';");
    $db->query('ALTER TABLE `classificationstore_keys` CHANGE COLUMN `type` `type` VARCHAR(190) NULL DEFAULT NULL;');
};

// build 97
function build97Check($fieldDefinitions, $needsSave)
{
    /** @var $fieldDefinition */
    foreach ($fieldDefinitions as $fieldDefinition) {
        if ($fieldDefinition instanceof  \Pimcore\Model\DataObject\ClassDefinition\Data\Localizedfields) {
            $needsSave = build97Check($fieldDefinition->getFieldDefinitions(), $needsSave);
        } elseif ($fieldDefinition instanceof \Pimcore\Model\DataObject\ClassDefinition\Data\Relations\AbstractRelations) {
            if (method_exists($fieldDefinition, 'getLazyLoading') && $fieldDefinition->getLazyLoading()) {
                $needsSave |= true;
                $fieldDefinition->setLazyLoading(false);
            }
        }
    }

    return $needsSave;
}

$migrations[97] = function () {
    $list = new \Pimcore\Model\DataObject\Fieldcollection\Definition\Listing();
    $list = $list->load();

    /** @var $collectionDef \Pimcore\Model\DataObject\Fieldcollection\Definition */
    foreach ($list as $collectionDef) {
        $needsSave = false;

        $fieldDefinitions = $collectionDef->getFieldDefinitions();
        $needsSave |= build97Check($fieldDefinitions, $needsSave);

        if ($needsSave) {
            $collectionDef->save();
        }
    }
};

// build 100
$migrations[100] = function () {
    $filesystem = new Filesystem();

    if ($filesystem->exists(PIMCORE_CLASS_DIRECTORY . '/Object')) {
        $filesystem->rename(PIMCORE_CLASS_DIRECTORY . '/Object', PIMCORE_CLASS_DIRECTORY . '/__please_delete_Object');

        $classList = new \Pimcore\Model\DataObject\ClassDefinition\Listing();
        $classes = $classList->load();
        foreach ($classes as $class) {
            $class->save();
        }

        $brickList = new \Pimcore\Model\DataObject\Objectbrick\Definition\Listing();
        $brickList = $brickList->load();
        foreach ($brickList as $brickDef) {
            $brickDef->save();
        }

        $fcList = new \Pimcore\Model\DataObject\Fieldcollection\Definition\Listing();
        $fcList = $fcList->load();
        foreach ($fcList as $collectionDef) {
            $collectionDef->save();
        }
    }
};

$migrations[134] = function () {

    // only add mapping if the ecommerce framework bundle is enabled
    if (!PimcoreEcommerceFrameworkBundle::isEnabled()) {
        return;
    }

    $configFile = PIMCORE_APP_ROOT . '/config/local/update_134_ecommerce_legacy_mapping.yml';

    $configData = [
        'pimcore_ecommerce_framework' => [
            'use_legacy_class_mapping' => true
        ]
    ];

    $fs = new Filesystem();
    $relativeConfigFile = $fs->makePathRelative($configFile, PIMCORE_PROJECT_ROOT);

    try {
        $yaml = Yaml::dump($configData, 100);
        $yaml = '# created by build 134 - you can safely remove this if you don\'t need any legacy class mapping' . "\n" . $yaml;

        $fs->dumpFile($configFile, $yaml);
    } catch (\Exception $e) {
        echo sprintf(PHP_EOL . '<p><strong style="color: red">ERROR:</strong> Failed to write YML config to <code>%s</code>: %s</p>' . PHP_EOL, $configFile, $e->getMessage());
        echo <<<EOF
<p>Please add the following configuration manually:<br>
<pre>
pimcore_ecommerce_framework:
    use_legacy_class_mapping: true
</pre>
</p>
EOF;
    }

};


$migrations[141] = function () {
    $db = Db::get();
    $db->insert('users_permission_definitions', ['key' => 'piwik_settings']);
    $db->insert('users_permission_definitions', ['key' => 'piwik_reports']);
};

$migrations[144] = function () {

    $db = \Pimcore\Db::get();

    $db->query('
        CREATE TABLE `gridconfigs` (
            `id` INT(11) NOT NULL AUTO_INCREMENT,
            `ownerId` INT(11) NULL,
            `classId` INT(11) NULL,
            `name` VARCHAR(50) NULL,
            `searchType` VARCHAR(50) NULL,
            `config` LONGTEXT NULL,
            `description` LONGTEXT NULL,
            `creationDate` INT(11) NULL,
            `modificationDate` INT(11) NULL,
            PRIMARY KEY (`id`),
            INDEX `ownerId` (`ownerId`),
            INDEX `classId` (`classId`),
            INDEX `searchType` (`searchType`)
        )
        DEFAULT CHARSET=utf8mb4;
        ;
    ');

    $db->query('
        CREATE TABLE `gridconfig_favourites` (
            `ownerId` INT(11) NOT NULL,
            `classId` INT(11) NOT NULL,
            `objectId` INT(11) NOT NULL DEFAULT \'0\',
            `gridConfigId` INT(11) NULL,
            `searchType` VARCHAR(50) NOT NULL DEFAULT \'\',
          PRIMARY KEY (`ownerId`, `classId`, `searchType`, `objectId`),
            INDEX `ownerId` (`ownerId`),
            INDEX `classId` (`classId`),
            INDEX `searchType` (`searchType`)
        )
        DEFAULT CHARSET=utf8mb4;
        ;
    ');

    $db->query('
        CREATE TABLE `gridconfig_shares` (
            `gridConfigId` INT(11) NOT NULL,
            `sharedWithUserId` INT(11) NOT NULL,
            PRIMARY KEY (`gridConfigId`, `sharedWithUserId`),
            INDEX `gridConfigId` (`gridConfigId`),
            INDEX `sharedWithUserId` (`sharedWithUserId`)
        )
        DEFAULT CHARSET=utf8mb4;
        ;
    ');

};

$migrations[145] = function () {
    $classList = new \Pimcore\Model\DataObject\ClassDefinition\Listing();
    $classes = $classList->load();
    if (is_array($classes)) {
        foreach ($classes as $class) {
            $doSave = false;
            foreach ($class->getFieldDefinitions() as $fieldDef) {
                if ($fieldDef instanceof \Pimcore\Model\DataObject\ClassDefinition\Data\Select) {
                    $fieldDef->setQueryColumnType('varchar');
                    $fieldDef->setColumnType('varchar');
                    $doSave = true;
                }
            }
            if ($doSave) {
                $class->save();
            }
        }
    }
};

$migrations[151] = function () {
    $db = \Pimcore\Db::get();

    $db->query("UPDATE users_permission_definitions SET `key`='tags_configuration' WHERE `key`=\"tags_config\"");

};

$migrations[152] = function () {

    $db = \Pimcore\Db::get();

    $db->query('
        CREATE TABLE `importconfigs` (
            `id` INT(11) NOT NULL AUTO_INCREMENT,
            `ownerId` INT(11) NULL,
            `classId` INT(11) NULL,
            `name` VARCHAR(50) NULL,
            `config` LONGTEXT NULL,
            `description` LONGTEXT NULL,
            `creationDate` INT(11) NULL,
            `modificationDate` INT(11) NULL,
            `shareGlobally` TINYINT(1) NULL,
            PRIMARY KEY (`id`),
            INDEX `ownerId` (`ownerId`),
            INDEX `classId` (`classId`),
            INDEX `shareGlobally` (`shareGlobally`)
        )
        DEFAULT CHARSET=utf8mb4;
        ;
    ');

    $db->query('
        CREATE TABLE `importconfig_shares` (
            `importConfigId` INT(11) NOT NULL,
            `sharedWithUserId` INT(11) NOT NULL,
            PRIMARY KEY (`importConfigId`, `sharedWithUserId`),
            INDEX `importConfigId` (`importConfigId`),
            INDEX `sharedWithUserId` (`sharedWithUserId`)
        )
        DEFAULT CHARSET=utf8mb4;
        ;
    ');

    $db->query('
        ALTER TABLE `gridconfigs`
            ADD COLUMN `shareGlobally` TINYINT(1) NULL DEFAULT NULL AFTER `creationDate`,
            ADD INDEX `shareGlobally` (`shareGlobally`);
        ;
    ');

};

$migrations[153] = function () {
    $db = \Pimcore\Db::get();
    $db->insert('users_permission_definitions', ['key' => 'share_configurations']);
    $db->insert('users_permission_definitions', ['key' => 'gdpr_data_extractor']);
};

$migrations[159] = function () {
    $db = \Pimcore\Db::get();

    $db->query('
        ALTER TABLE `redirects`
            ADD COLUMN `type` VARCHAR(100) DEFAULT NULL AFTER `id`,
            ADD COLUMN `regex` TINYINT(1) DEFAULT NULL AFTER `priority`
        ;
    ');

    $db->beginTransaction();

    try {
        // existing behaviour was always a regex-match, so update existing rows to do so
        $db->query('UPDATE redirects SET regex = 1');

        // update type depending on sourceEntireUrl setting
        $db->executeQuery('UPDATE redirects SET type = ? WHERE sourceEntireUrl = 1', [
            Redirect::TYPE_ENTIRE_URI
        ]);

        $db->executeQuery('UPDATE redirects SET type = ? WHERE sourceEntireUrl <> 1 OR sourceEntireUrl IS NULL', [
            Redirect::TYPE_PATH_QUERY
        ]);

        $db->commit();
    } catch (\Throwable $e) {
        $db->rollBack();
        throw $e;
    }

    // drop column as it isn't needed anymore
    // unfortunately MySQL does not support transactional DDL
    // so we need to do this after the transaction succeeded
    $db->query('ALTER TABLE redirects DROP COLUMN sourceEntireUrl');

};

$migrations[161] = function () {

    $db = \Pimcore\Db::get();
    $db->query('ALTER TABLE `quantityvalue_units` MODIFY `abbreviation` varchar(20)');

    // targeting
    $db->query('RENAME TABLE targeting_personas TO targeting_target_groups');

    $db->query('ALTER TABLE `targeting_target_groups` DROP `conditions`');
    $db->query('ALTER TABLE `documents_page` CHANGE `personas` `targetGroupIds` VARCHAR(255)');

    $db->query('RENAME TABLE targeting_rules TO PLEASE_DELETE__targeting_rules');
    $db->query(<<<'EOF'
        CREATE TABLE `targeting_rules` (
          `id` int(11) NOT NULL AUTO_INCREMENT,
          `name` varchar(255) NOT NULL DEFAULT '',
          `description` text,
          `scope` varchar(50) DEFAULT NULL,
          `active` tinyint(1) DEFAULT NULL,
          `prio` smallint(5) unsigned NOT NULL DEFAULT '0',
          `conditions` longtext,
          `actions` longtext,
          PRIMARY KEY (`id`)
        ) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
EOF
    );

    $db->query(<<<'EOF'
        CREATE TABLE `targeting_storage` (
          `visitorId` varchar(100) NOT NULL,
          `scope` varchar(50) NOT NULL,
          `name` varchar(100) NOT NULL,
          `value` text,
          `creationDate` datetime DEFAULT NULL,
          `modificationDate` datetime DEFAULT NULL,
          PRIMARY KEY (`visitorId`,`scope`,`name`),
          KEY `targeting_storage_scope_index` (`scope`),
          KEY `targeting_storage_name_index` (`name`),
          KEY `targeting_storage_visitorId_index` (`visitorId`)
        ) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
EOF
    );

};

$migrations[168] = function () {
    $db = \Pimcore\Db::get();
    $db->query('ALTER TABLE email_log ADD COLUMN `replyTo` VARCHAR(255) DEFAULT NULL AFTER `from`;');
    $db->query('ALTER TABLE documents_email ADD COLUMN `replyTo` VARCHAR(255) DEFAULT NULL AFTER `from`;');

};

$migrations[181] = function () {
    $db = \Pimcore\Db::get();
    $db->query('ALTER TABLE `email_log` ADD INDEX `sentDate` (`sentDate`, `id`);');
};

$migrations[182] = function () {
    $oldPath = PIMCORE_LOG_DIRECTORY . '/fileobjects';
    $newPath = PIMCORE_PRIVATE_VAR . '/application-logger';

    if (is_dir($oldPath) && !is_dir($newPath)) {
        rename($oldPath, $newPath);
    }
};

$migrations[183] = function () {
    $db = \Pimcore\Db::get();
    $db->query('ALTER TABLE `versions` ADD COLUMN `binaryDataHash` VARCHAR(40) NULL DEFAULT NULL;');
    $db->query('ALTER TABLE `versions` ADD INDEX `binaryDataHash` (`binaryDataHash`);');
};

$migrations[184] = function () {
    $db = \Pimcore\Db::get();
    $db->query('ALTER TABLE `versions` DROP COLUMN `binaryDataHash`;');
};

$migrations[186] = function () {
    $db = \Pimcore\Db::get();
    $websiteSettings = $db->fetchAll('SELECT * FROM website_settings');

    if (!$websiteSettings) {
        $websiteSettings = [];
    }

    $file = \Pimcore\Config::locateConfigFile('website-settings.php');
    $table = \Pimcore\Db\PhpArrayFileTable::get($file);
    $table->truncate();

    foreach ($websiteSettings as $websiteSetting) {
        unset($websiteSetting['id']);
        $table->insertOrUpdate($websiteSetting, $websiteSetting['id']);
    }

    $db->query('RENAME TABLE `website_settings` TO `PLEASE_DELETE__website_settings`;');

};

$migrations[191] = function () {
    $db = \Pimcore\Db::get();
    $db->insert('users_permission_definitions', ['key' => 'asset_metadata']);
};

$migrations[195] = function () {
    $db = \Pimcore\Db::get();
    $db->query('ALTER TABLE assets ROW_FORMAT=DYNAMIC;');
    $db->query('ALTER TABLE documents ROW_FORMAT=DYNAMIC;');
    $db->query('ALTER TABLE objects ROW_FORMAT=DYNAMIC;');
    $db->query('ALTER TABLE properties ROW_FORMAT=DYNAMIC;');
    $db->query('ALTER TABLE search_backend_data ROW_FORMAT=DYNAMIC;');
    $db->query('ALTER TABLE users_workspaces_asset ROW_FORMAT=DYNAMIC;');
    $db->query('ALTER TABLE users_workspaces_document ROW_FORMAT=DYNAMIC;');
    $db->query('ALTER TABLE users_workspaces_object ROW_FORMAT=DYNAMIC;');

    $db->query("ALTER TABLE `assets`
        CHANGE COLUMN `filename` `filename` VARCHAR(255) CHARACTER SET 'utf8' COLLATE 'utf8_general_ci'  NULL DEFAULT ''AFTER `type`,
        CHANGE COLUMN `path` `path` VARCHAR(765) CHARACTER SET 'utf8' COLLATE 'utf8_general_ci' NULL DEFAULT NULL AFTER `filename`;
    ");

    $db->query("ALTER TABLE `documents`
        CHANGE COLUMN `key` `key` VARCHAR(255) CHARACTER SET 'utf8' COLLATE 'utf8_general_ci'  NULL DEFAULT ''AFTER `type`,
        CHANGE COLUMN `path` `path` VARCHAR(765) CHARACTER SET 'utf8' COLLATE 'utf8_general_ci' NULL DEFAULT NULL AFTER `key`;
    ");

    $db->query("ALTER TABLE `objects`
        CHANGE COLUMN `o_key` `o_key` VARCHAR(255) CHARACTER SET 'utf8' COLLATE 'utf8_general_ci'  NULL DEFAULT ''AFTER `o_type`,
        CHANGE COLUMN `o_path` `o_path` VARCHAR(765) CHARACTER SET 'utf8' COLLATE 'utf8_general_ci' NULL DEFAULT NULL AFTER `o_key`;
    ");

    $db->query("ALTER TABLE `properties`
        CHANGE COLUMN `cpath` `cpath` VARCHAR(765) CHARACTER SET 'utf8' COLLATE 'utf8_general_ci'  NULL DEFAULT ''AFTER `ctype`;
    ");

    $db->query("ALTER TABLE `search_backend_data`
        CHANGE COLUMN `fullpath` `fullpath` VARCHAR(765) CHARACTER SET 'utf8' COLLATE 'utf8_general_ci'  NULL DEFAULT ''AFTER `id`;
    ");

    $db->query("ALTER TABLE `users_workspaces_asset`
        CHANGE COLUMN `cpath` `cpath` VARCHAR(765) CHARACTER SET 'utf8' COLLATE 'utf8_general_ci'  NULL DEFAULT ''AFTER `cid`;
    ");

    $db->query("ALTER TABLE `users_workspaces_document`
        CHANGE COLUMN `cpath` `cpath` VARCHAR(765) CHARACTER SET 'utf8' COLLATE 'utf8_general_ci'  NULL DEFAULT ''AFTER `cid`;
    ");

    $db->query("ALTER TABLE `users_workspaces_object`
        CHANGE COLUMN `cpath` `cpath` VARCHAR(765) CHARACTER SET 'utf8' COLLATE 'utf8_general_ci'  NULL DEFAULT ''AFTER `cid`;
    ");
};

$migrations[202] = function () {
    $db = \Pimcore\Db::get();

    // mark ecommerce install migration as migrated if framework is currently installed
    if (PimcoreEcommerceFrameworkBundle::isInstalled()) {

        // create migration table if not exists
        $factory = \Pimcore::getContainer()->get('Pimcore\Migrations\Configuration\ConfigurationFactory');
        $bundle = \Pimcore::getKernel()->getBundle('PimcoreEcommerceFrameworkBundle');
        $config = $factory->getForBundle($bundle, $db);
        $config->createMigrationTable();

        $sql = <<<'SQL'
            INSERT IGNORE INTO
                pimcore_migrations (migration_set, version, migrated_at)
            VALUES
                (:migration_set, :version, NOW())
SQL;

        $db->executeQuery($sql, [
            'migration_set' => 'PimcoreEcommerceFrameworkBundle',
            'version'       => InstallVersion::INSTALL_VERSION,
        ]);
    }

    $db->insert('users_permission_definitions', ['key' => 'admin_translations']);
};

$migrations[219] = function () {
    $db = \Pimcore\Db::get();

    $db->query("ALTER TABLE `objects` ADD `o_childrenSortBy` ENUM('key','index') NULL DEFAULT NULL AFTER `o_className`;");
    $db->query('ALTER TABLE `objects` ADD INDEX `index` (`o_index`);');
};

$migrations[225] = function () {
    $db = \Pimcore\Db::get();

    $db->query('ALTER TABLE `documents_page` CHANGE COLUMN `description` `description` VARCHAR(383) NULL DEFAULT NULL;');
    $db->query('ALTER table users ADD COLUMN `lastLogin` int(11) unsigned NULL;');
    $db->query('ALTER table email_log ADD FULLTEXT INDEX `fulltext` (`from`, `to`, `cc`, `bcc`, `subject`, `params`);');
};

$migrations[227] = function () {
    $classList = new \Pimcore\Model\DataObject\ClassDefinition\Listing();
    $classes = $classList->load();
    foreach ($classes as $class) {
        $class->save();
    }

    $brickList = new \Pimcore\Model\DataObject\Objectbrick\Definition\Listing();
    $brickList = $brickList->load();
    foreach ($brickList as $brickDef) {
        $brickDef->save();
    }

};

$migrations[235] = function () {
    $db = \Pimcore\Db::get();
    $db->query("REPLACE INTO `users_permission_definitions` (`key`) VALUES ('clear_fullpage_cache');");
};

$migrations[253] = function () {
    $db = \Pimcore\Db::get();

    $db->query('ALTER TABLE `users`
	ADD COLUMN `keyBindings` TEXT NULL AFTER `lastLogin`;
');

};

$migrations[256] = function () {
    $db = \Pimcore\Db::get();

    $db->query('ALTER TABLE `users` ADD COLUMN `twoFactorAuthentication` VARCHAR(255) AFTER `apiKey`');

};

$migrations[270] = function () {
    $db = \Pimcore\Db::get();
    $db->query('ALTER TABLE `classes`
        ALTER `id` DROP DEFAULT;
    ');

    $db->query('ALTER TABLE `classes`
	    CHANGE COLUMN `id` `id` VARCHAR(50) NOT NULL FIRST;');

    $db->query('ALTER TABLE `objects`
	    CHANGE COLUMN `o_classId` `o_classId` VARCHAR(50) NULL DEFAULT NULL AFTER `o_userModification`;');

    $db->query('ALTER TABLE `gridconfigs`
        CHANGE COLUMN `classId` `classId` VARCHAR(50) NULL DEFAULT NULL AFTER `ownerId`;
    ');

    $db->query('ALTER TABLE `gridconfig_favourites`
	    ALTER `classId` DROP DEFAULT;');

    $db->query('ALTER TABLE `gridconfig_favourites`
	    CHANGE COLUMN `classId` `classId` VARCHAR(50) NOT NULL AFTER `ownerId`;');

    $db->query('ALTER TABLE `importconfigs`
        CHANGE COLUMN `classId` `classId` VARCHAR(50) NULL DEFAULT NULL AFTER `ownerId`;
    ');

    $db->query('ALTER TABLE `custom_layouts`
        ALTER `classId` DROP DEFAULT;
    ');

    $db->query('ALTER TABLE `custom_layouts`
        CHANGE COLUMN `classId` `classId` VARCHAR(50) NOT NULL AFTER `id`;');

};

$migrations[277] = function () {
    $classList = new \Pimcore\Model\DataObject\ClassDefinition\Listing();
    $classes = $classList->load();
    foreach ($classes as $ckey => $class) {
        foreach ($class->getFieldDefinitions() as $fkey => $field) {
            if ($field->getUnique()) {
                $class->save(); //save class to remove unique constraint form query table
                break;
            }
        }
    }
};

$migrations[280] = function () {
    //setting replyTo = null if default empty value exists
    $emailLogs = new \Pimcore\Model\Tool\Email\Log\Listing();
    $emailLogs->setCondition("replyTo = ''");
    foreach ($emailLogs->load() as $ekey => $emailLog) {
        $emailLog->setReplyTo(null);
        $emailLog->save();
    }

};

$migrations[283] = function () {

    $classList = new \Pimcore\Model\DataObject\ClassDefinition\Listing();
    $classes = $classList->load();
    foreach ($classes as $ckey => $class) {
        $found = false;
        foreach ($class->getFieldDefinitions() as $fkey => $field) {
            if ($field instanceof \Pimcore\Model\DataObject\ClassDefinition\Data\ObjectsMetadata) {
                $allowedClassId = $field->getAllowedClassId();
                if (is_numeric($allowedClassId)) {
                    $class = \Pimcore\Model\DataObject\ClassDefinition::getById($allowedClassId);
                    $allowedClassId  = $class ? $class->getName() : null;
                }

		if($allowedClassId){
			$field->setAllowedClassId($allowedClassId);
			$found = true;
		}

            }
        }
        if ($found) {
            try {
                $class->save();
            } catch (\Exception $e) {
                echo "Error: " . $e->getMessage() . "\n";
            }
        }
    }

    $brickList = new \Pimcore\Model\DataObject\Objectbrick\Definition\Listing();
    $brickList = $brickList->load();
    foreach ($brickList as $brickDef) {
        $found = false;
        foreach ($brickDef->getFieldDefinitions() as $fkey => $field) {
            if ($field instanceof \Pimcore\Model\DataObject\ClassDefinition\Data\ObjectsMetadata) {
                $allowedClassId = $field->getAllowedClassId();
                if (is_numeric($allowedClassId)) {
                    $class = \Pimcore\Model\DataObject\ClassDefinition::getById($allowedClassId);
                    $allowedClassId  = $class ? $class->getName() : null;
                }

		if($allowedClassId){
			$field->setAllowedClassId($allowedClassId);
			$found = true;
		}

            }
        }
        if ($found) {
            try {
                $brickDef->save();
            } catch (\Exception $e) {
                echo "Error: " . $e->getMessage() . "\n";
            }
        }
    }

    $fcList = new \Pimcore\Model\DataObject\Fieldcollection\Definition\Listing();
    $fcList = $fcList->load();
    foreach ($fcList as $collectionDef) {
        $found = false;
        foreach ($collectionDef->getFieldDefinitions() as $fkey => $field) {
            if ($field instanceof \Pimcore\Model\DataObject\ClassDefinition\Data\ObjectsMetadata) {
                $allowedClassId = $field->getAllowedClassId();
                if (is_numeric($allowedClassId)) {
                    $class = \Pimcore\Model\DataObject\ClassDefinition::getById($allowedClassId);
                    $allowedClassId  = $class ? $class->getName() : null;
                }

		if($allowedClassId){
			$field->setAllowedClassId($allowedClassId);
			$found = true;
		}

            }
        }
        if ($found) {
            try {
                $collectionDef->save();
            } catch (\Exception $e) {
                echo "Error: " . $e->getMessage() . "\n";
            }
        }
    }

};

// Do some compatibility checks first!

$db = \Pimcore\Db::get();
// $results2 = print_r($db, true);
// \Pimcore\Log\Simple::log("event.log", 'Manual Migration = '.$results2);

$incompatible = false;

$largePrefix = $db->fetchRow("SHOW GLOBAL VARIABLES LIKE 'innodb\_large\_prefix';");
if ($largePrefix && $largePrefix['Value'] != 'ON') {
    $incompatible = true;
}
$fileFormat = $db->fetchRow("SHOW GLOBAL VARIABLES LIKE 'innodb\_file\_format';");
if ($fileFormat && $fileFormat['Value'] != 'Barracuda') {
    $incompatible = true;
}
$fileFilePerTable = $db->fetchRow("SHOW GLOBAL VARIABLES LIKE 'innodb\_file\_per\_table';");
if ($fileFilePerTable && $fileFilePerTable['Value'] != 'ON') {
    $incompatible = true;
}

if ($incompatible) {
    echo "Your MySQL/MariaDB Server is incompatible!\n\n";
    echo "Please ensure the following MySQL/MariaDB system variables are set accordingly:\n";
    echo "innodb_file_format = Barracuda\ninnodb_large_prefix = 1\ninnodb_file_per_table = 1\n\n";
    exit;
}


if (version_compare(PHP_VERSION, '7.1', '<')) {
    echo "Pimcore requires PHP >= 7.1, your current version is: " . PHP_VERSION . "\n\n";
    exit;
}

$offset = 0;
if(isset($_SERVER['argv'][1])) {
    $offset = $_SERVER['argv'][1];
    if(!is_numeric($offset)) {
        echo "Build offset must be a numeric value\n"; exit;
    }
    $offset = intval($offset);
}


foreach ($migrations as $type => $migration) {

    if(is_numeric($type) && $type <= $offset) continue;

    try {
        echo sprintf(
            'Executing migration for %s...',
            is_numeric($type) ? 'build ' . $type : $type
        );

        $migration();

        echo "Done\n";
    } catch (\Throwable $e) {
        echo '---> Migration step ' . $type . " failed! Please check manually! \n\n\n";
    }
}
