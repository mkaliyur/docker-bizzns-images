<?php
namespace AppBundle\Templating\Helper;

use Pimcore\Model\Document;
use Pimcore\Model\Document\Service;
use Pimcore\Tool;
use Symfony\Component\Templating\Helper\Helper;
use Pimcore\Model\DataObject;

class LayoutHelper extends Helper {

  private $documentService;

  public function __construct(Service $documentService)
  {
      $this->documentService = $documentService;
  }

  /**
   * @inheritDoc
   */
  public function getName()
  {
      return 'LayoutHelper';
  }

  public function __invoke ($container, $layoutPath){
    //\Pimcore\Log\Simple::log("event.log",">>>>> KGM:: INSIDE LayoutHelper invoke method");


    $container->Layout = new \stdClass();

     $layout = DataObject::getByPath($layoutPath);

     $container->Layout->name = $layout->getName();
     $container->Layout->selector = $layout->getSelector();

     $container->Layout->settings = [];
     if( !empty($layout->getSettings()) &&   !empty($layout->getSettings()->getItems() )){
       foreach( (array) $layout->getSettings()->getItems() as $settings_item ){
         $ret = new \stdClass();
         $ret->name = $settings_item->getSetting_name();
         $ret->value = $settings_item->getSetting_value();
         array_push($container->Layout->settings,$ret);
       }
    }

    return $container;

  }

}

?>
