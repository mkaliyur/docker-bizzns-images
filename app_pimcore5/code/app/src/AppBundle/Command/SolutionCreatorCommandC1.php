<?php

namespace AppBundle\Command;

use Pimcore\Console\AbstractCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Input\InputArgument;
use Pimcore\Model\Element\Service as elementalias;
use Pimcore\Model\Webservice;
use Pimcore\Model\DataObject;
use Pimcore\Model\DataObject\Service as Dataservice;
use Pimcore\Model\DataObject\BzPublishQueue\Listing;
use Pimcore\Model\DataObject\BzDomain;
use Pimcore\Model\DataObject\BzSolutions;
use Pimcore\Model\Object\Folder;
use Pimcore\Model\Asset;
use Pimcore\Model\Asset\Folder as AssetFolder;
use Pimcore\Model\Document;
use Pimcore\Db;
use AppBundle\Command\SolutionCommandHelper;




class SolutionCreatorCommandC1 extends AbstractCommand
{
    private $DataobjectService;

    public function __construct(Dataservice $dataobjectService)
    {
      $this->DataobjectService = $dataobjectService;

      parent::__construct();
    }

    protected function configure()
    {
        $this
            ->setName('bizzns:create-solution')
            ->setDescription('Command to create an instance of bizzns solution')
            ->addArgument('domainname', InputArgument::REQUIRED, 'Bizzns Domainname to be created.')
            ->addArgument('marketname', InputArgument::REQUIRED, 'Bizzns Marketname to be created.')
            ->addArgument('solutionname', InputArgument::REQUIRED, 'Bizzns Solutionname to be created. AlphaNumeric with - or _ :: No spaces');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        //$this->getContainer()->get('profiler')->disable();
        Db::getConnection()->getConfiguration()->setSQLLogger(null);
        //      Version::disable(); //Second run
        //      Cache::disable(); //Third run
        //\Pimcore\Cache::disable();
        ini_set('memory_limit','2048M');

        $domain_name = $input->getArgument('domainname');
        $domain_name_exists = DataObject::getByPath("/organization/Domains/".$domain_name);
        if(!is_null($domain_name_exists)){
          $this->output->writeln('********* This Domain Name Already Exists *************');
          \Pimcore\Log\Simple::log("event.log", 'Inside Solution create inside if');
        }else{
          $bz_domain_name = new DataObject\BzDomain();
          $bz_domain_name->setKey($domain_name);
          $bz_domain_name->setName($domain_name.'_domain');

          $master_listing = DataObject::getByPath("/library/Global/listings/global-default-listing");
          $bz_domain_name->setMaster_listing($master_listing);

          $master_deal = DataObject::getByPath("/library/Global/deals/global-default-deal");
          $bz_domain_name->setMaster_deal($master_deal);

          $master_event = DataObject::getByPath("/library/Global/events/global-default-event");
          $bz_domain_name->setMaster_event($master_event);

          $master_audio = DataObject::getByPath("/library/Global/audio/global-default-audio");
          $bz_domain_name->setMaster_audio($master_audio);

          $master_video = DataObject::getByPath("/library/Global/video/global-default-video");
          $bz_domain_name->setMaster_video($master_video);

          $master_article = DataObject::getByPath("/library/Global/articles/global-default-article");
          $bz_domain_name->setMaster_article($master_article);

          $master_news = DataObject::getByPath("/library/Global/news/global-default-news");
          $bz_domain_name->setMaster_news($master_news);


          $parent_path = "/organization/Domains";
          $parent_folder_path = DataObject::getByPath($parent_path);
          $parent_folder_id = $parent_folder_path->o_id;
          $bz_domain_name->setParentId($parent_folder_id);
          $domain_name_created_exists = DataObject::getByPath("/organization/Domains//".$domain_name);
          print "Domain ". $domain_name." Created!";
        //  \Pimcore\Log\Simple::log("event.log", 'Inside Solution create saving domain_name= '.$domain_name);
          $bz_domain_name->save();

          //Master Folder for the new domain_name
          $libraryMasterFolder = new Folder();
          $libraryMasterFolder->setKey($domain_name);
          $parent_folder = DataObject::getByPath("/library/master");
          $parent_folder_id = $parent_folder->o_id;
          $libraryMasterFolder->setParentId($parent_folder_id);
          $libraryMasterFolder->save();

        }

        //Process Market
        $market_name = $input->getArgument('marketname');
        $market_name_exists = DataObject::getByPath("/organization/MarketArea/".$market_name);
        if(!is_null($market_name_exists)){
          $this->output->writeln('********* This Market Name Already Exists *************');
        }else{
          //$this->output->writeln('********* In else This Market Name doesnt Exist *************');
          $bz_market_name = new DataObject\BzMarketArea();
          $bz_market_name->setKey($market_name);
          $bz_market_name->setName($market_name.'_market');
          $market_parent_path = "/organization/MarketArea";
          $market_parent_folder_path = DataObject::getByPath($market_parent_path);
          $market_parent_folder_id = $market_parent_folder_path->o_id;
          $bz_market_name->setParentId($market_parent_folder_id);
          \Pimcore\Log\Simple::log("event.log", 'Inside Solution create saving market_name= '.$market_name);
          $bz_market_name->save();

        }


        $solution_key = $input->getArgument('solutionname');
        if(is_null($solution_key)){
          $solution_key = \Pimcore\File::getValidFilename($market_name.'_'.$domain_name);
        }

        if(is_null($solution_key)){
         return;
        }


        $solution_name_exists = DataObject::getByPath("/organization/Solutions/".$solution_key);
        if(!is_null($solution_name_exists)){
          $this->output->writeln('********* This Solution Name Already Exists *************');
          //return;
        }else{
          $bz_solutions = new DataObject\BzSolutions();
          $bz_solutions->setKey($solution_key);
          $parent_folder = DataObject::getByPath("/organization/Solutions");
          $parent_folder_id = $parent_folder->o_id;
          $bz_solutions->setParentId($parent_folder_id);
          $bz_solutions->setName($solution_key.'_solution');
          $domainHref_path = DataObject::getByPath("/organization/Domains/".$domain_name);
          $bz_solutions->setDomain_href($domainHref_path);
          $marketHref_path = DataObject::getByPath("/organization/MarketArea/".$market_name);
          $bz_solutions->setMarket_href($marketHref_path);
          $bz_solutions->save();
        }



        $solutionFolder = new Folder();
        $solutionFolder->setKey($solution_key);
        $parent_folder = DataObject::getByPath("/solutions");
        $parent_folder_id = $parent_folder->o_id;
        $solutionFolder->setParentId($parent_folder_id);
        $solutionFolder->save();
        $this->output->writeln('********* Just created Folder under solutions *************'.$solution_key);

        $solutionParent = new DataObject\BzParent();
        $solutionParent->setKey("parent_".$solution_key);

        $solutionParent->setDomain_name($domain_name);
        $solutionParent->setMarket_name($market_name);
        $solutionParent->setSolution_name($solution_key);

        $marketHref_Object = DataObject::getByPath("/organization/MarketArea/".$market_name);
        $solutionParent->setMarket_area_href($marketHref_Object);

        $domainHref_Object = DataObject::getByPath("/organization/Domains/".$domain_name);
        $solutionParent->setDomain_href($domainHref_Object);

        //$masterListing_object = $domainHref_Object->getMaster_listing();
        //$solutionParent->setListing_inherits_from($masterListing_object);

        $masterDeal_object = $domainHref_Object->getMaster_deal();
        $solutionParent->setDeal_inherits_from($masterDeal_object);

        $masterEvent_object = $domainHref_Object->getMaster_event();
        $solutionParent->setEvent_inherits_from($masterEvent_object);

        $masterVideo_object = $domainHref_Object->getMaster_audio();
        $solutionParent->setAudio_inherits_from($masterVideo_object);

        $masterVideo_object = $domainHref_Object->getMaster_video();
        $solutionParent->setVideo_inherits_from($masterVideo_object);

        $masterArticle_object = $domainHref_Object->getMaster_article();
        $solutionParent->setArticle_inherits_from($masterArticle_object);

        $masterNews_object = $domainHref_Object->getMaster_news();
        $solutionParent->setNews_inherits_from($masterNews_object);

        $parent_folder_id2 = $solutionFolder->o_id;
        $solutionParent->setParentId($parent_folder_id2);
        $solutionParent->save();

        $contentFolder = new Folder();
        $contentFolder->setKey("Content");
        $contentParent = DataObject::getByPath("/solutions//".$solution_key."//parent_".$solution_key);
        $parent_folder_content = $contentParent->o_id;
        $contentFolder->setParentId($parent_folder_content);
        $contentFolder->save();

        $merchantFolder = new Folder();
        $merchantFolder->setKey("Merchants");
        $merchantParent = DataObject::getByPath("/solutions//".$solution_key."/parent_".$solution_key);
        $parent_folder_merchant = $merchantParent->o_id;
        $merchantFolder->setParentId($parent_folder_merchant);
        $merchantFolder->save();

        $assetSolutionFolder = new AssetFolder();
        $assetSolutionFolder->setType('folder');
        $assetSolutionFolder->setFilename($solution_key);
        $parent_folder = Asset::getByPath("/solutions");
        $parent_folder_id = $parent_folder->getId();
        $asset_solution_id = $assetSolutionFolder->setParentId($parent_folder_id);
        $assetSolutionFolder->save();

        $assetMerchantFolder = new AssetFolder();
        $assetMerchantFolder->setType('folder');
        $assetMerchantFolder->setFilename("Merchants");
        $parent_folder = Asset::getByPath("//solutions//".$solution_key);
        $parent_folder_id = $parent_folder->getId();
        $assetMerchantFolder->setParentId($parent_folder_id);
        $assetMerchantFolder->save();


        $this->output->writeln('********* Successfully Created Asset Folders*************');

        $this->output->writeln('********* Just created solutionParent under *************'.$solution_key);
        return;
        $copyContentObjectPath = DataObject::getByPath("/library/master/".$domain_name);
        //$copyObject = elementalias::cloneMe($copyObjectPath);
        $parent_path = "/solutions//".$solution_key."//parent_/".$solution_key. "/Content";
        $parent_folder_path = DataObject::getByPath($parent_path);
        try{
          $copyContentObject = $this->DataobjectService->copyRecursive($parent_folder_path, $copyContentObjectPath);
        }
        catch(Exception $e){
          \Pimcore\Log\Simple::log("event.log", 'Inside Solution create Content Copy Inside catch'.$e->getMessage());
        }
        $parent_folder_id = $parent_folder_path->o_id;
        $copyContentObject->setParentId($parent_folder_id);
        $copyContentObject->save();

        // $copyContentObjectPath = DataObject::getByPath("/library/master/".$domain_name);
        // //$copyObject = elementalias::cloneMe($copyObjectPath);
        // //$parent_path_content = "/solutions/build52_hyd52/parent_build52_hyd52/Content";
        // $parent_path_content = "/solutions//".$solution_key."//parent_/".$solution_key. "/Content";
        // $results2 = print_r($parent_path_content, true);
        // \Pimcore\Log\Simple::log("event.log", 'Inside SolutionCreatorCommand parent_path_content='.$results2);
        //

        // $parent_folder_path_content = DataObject::getByPath($parent_path_content);
        // try{
        //   $copyObject = $this->DataobjectService->copyRecursive($parent_folder_path_content, $copyContentObjectPath);
        // }
        // catch(Exception $e){
        //   \Pimcore\Log\Simple::log("event.log", 'Inside Solution create Content Copy Inside catch'.$e->getMessage());
        // }
        // $parent_folder_id = $parent_folder_path_content->o_id;
        // $copyContentObject->setParentId($parent_folder_id);
        // $copyContentObject->save();


        $this->output->writeln('********* Creating Domain Market and Solution and Solution Parent*************');
        //$this->output->writeln('******Bizzns Solutionname has been Created*******.');


    }
}
