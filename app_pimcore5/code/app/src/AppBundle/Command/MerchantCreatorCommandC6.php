<?php

namespace AppBundle\Command;

use Pimcore\Console\AbstractCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Input\InputArgument;
use Pimcore\Model\Element\Service as elementalias;
use Pimcore\Model\Webservice;
use Pimcore\Model\DataObject;
use Pimcore\Model\DataObject\BzParent;
use Pimcore\Model\DataObject\BzBlogArticle;
use Pimcore\Model\DataObject\BzAudio;
use Pimcore\Model\DataObject\BzVideo;
use Pimcore\Model\DataObject\BzDeals;
use Pimcore\Model\DataObject\BzEvents;
use Pimcore\Model\DataObject\BzListing2;
use Pimcore\Model\DataObject\BzListingBuilders;
use Pimcore\Model\DataObject\BzMerchant;
use Pimcore\Model\DataObject\Service as Dataservice;
use Pimcore\Model\DataObject\BzPublishQueue\Listing;
use Pimcore\Model\DataObject\BzDomain;
use Pimcore\Model\DataObject\BzSolutions;
use Pimcore\Model\Object\Folder;
use Pimcore\Model\Asset;
use Pimcore\Model\Document;
use Pimcore\Db;
use AppBundle\Command\SolutionCommandHelper;
use Pimcore\Model\Object\Folder as DataObjectFolder;
use Pimcore\Model\Asset\Folder as AssetFolder;
use Pimcore\Model\User;
use Pimcore\Tool\Authentication;
use Pimcore\Tool;
use Pimcore\Model\User\UserRole\Folder as UserFolder;
use Pimcore\Model\Element;
use Pimcore\Model\User\Workspace\DataObject as WorkspaceDataobject;
use Pimcore\Model\User\Workspace\Asset as WorkspaceAsset;


class MerchantCreatorCommandC6 extends AbstractCommand
{
    private $DataobjectService;

    public function __construct(Dataservice $dataobjectService)
    {
      $this->DataobjectService = $dataobjectService;

      parent::__construct();
    }

    protected function configure()
    {
        $this
            ->setName('bizzns:create-merchant-part1')
            ->setDescription('Command to create an instance of bizzns solution')
            ->addArgument('domainname', InputArgument::REQUIRED, 'Bizzns Domainname to be created.')
            ->addArgument('marketname', InputArgument::REQUIRED, 'Bizzns Marketname to be created.')
            ->addArgument('solutionname', InputArgument::REQUIRED, 'Bizzns Solutionname to be created. AlphaNumeric with - or _ :: No spaces')
            ->addArgument('merchantname', InputArgument::REQUIRED, 'Bizzns Merchantname to be created.');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
      Db::getConnection()->getConfiguration()->setSQLLogger(null);

      $domain_name = $input->getArgument('domainname');
      $domain_name_exists = DataObject::getByPath("/organization/Domains/".$domain_name);
      if(is_null($domain_name_exists)){
        $this->output->writeln('********* Please create Domain Name by executing Command1 *************');
      }

      $market_name = $input->getArgument('marketname');
      $market_name_exists = DataObject::getByPath("/organization/MarketArea/".$market_name);
      if(is_null($market_name_exists)){
        $this->output->writeln('********* Please create Market Name by executing Command1 *************');
      }

      $solution_key = $input->getArgument('solutionname');
      $solution_name_exists = DataObject::getByPath("/solutions//".$solution_key);
      if(is_null($solution_name_exists)){
        $this->output->writeln('********* Please create Solution Name by executing Command1 *************');
      }

      $merchant_name1 = $input->getArgument('merchantname');
      $merchant_name = str_replace(' ', '', $merchant_name1);
      $merchant_name_exists = DataObject::getByPath("/solutions//".$solution_key."/parent_".$solution_key."/Merchants//".$merchant_name);
      if(!is_null($merchant_name_exists)){
        $this->output->writeln('********* This Merchant Name Already Exists *************');
      }else{
        $create_bz_merchant = new BzMerchant();
        $create_bz_merchant->setKey($merchant_name);
        $parent_folder = DataObject::getByPath("/organization/Merchants1");
        $parent_folder_id = $parent_folder->o_id;
        $create_bz_merchant->setParentId($parent_folder_id);
        $create_bz_merchant->save();

        $merchantParent = new BzParent();
        $merchantParent->setKey($merchant_name);
        $parent_folder = DataObject::getByPath("/solutions//".$solution_key."/parent_".$solution_key."/Merchants//");
        $parent_folder_id = $parent_folder->o_id;
        $merchantParent->setParentId($parent_folder_id);
        $merchantParent->save();

        $merchantFolder = new DataObjectFolder();
        $merchantFolder->setKey($merchant_name);
        $parent_folder = DataObject::getByPath("/solutions//".$solution_key."/parent_".$solution_key."/Merchants//".$merchant_name);
        $parent_folder_id = $parent_folder->o_id;
        $merchantFolder->setParentId($parent_folder_id);
        $merchantFolder->save();

        $source_array = array('Articles', 'Audios', 'Deals', 'Events', 'Listings', 'News', 'Videos');
        foreach ($source_array as $src) {
        $merchantContentFolder = new DataObjectFolder();
        $merchantContentFolder->setKey($src);
        $parent_folder = DataObject::getByPath("/solutions//".$solution_key."/parent_".$solution_key."/Merchants//".$merchant_name."/".$merchant_name);
        $parent_folder_id = $parent_folder->o_id;
        $merchantContentFolder->setParentId($parent_folder_id);
        $merchantContentFolder->save();
       }

       $merchantContentArticleObject = new BzBlogArticle();
       $merchantContentArticleObject->setKey('SampleArticle');
       $merchantContentArticleObject->setObject_type('Article');
       $parent_folder = DataObject::getByPath("/solutions//".$solution_key."/parent_".$solution_key."/Merchants//".$merchant_name."/".$merchant_name."/Articles");
       $parent_folder_id = $parent_folder->o_id;
       $merchantContentArticleObject->setParentId($parent_folder_id);
       $merchantContentArticleObject->save();

       $merchantContentAudioObject = new BzAudio();
       $merchantContentAudioObject->setKey('SampleAudio');
       $parent_folder = DataObject::getByPath("/solutions//".$solution_key."/parent_".$solution_key."/Merchants//".$merchant_name."/".$merchant_name."/Audios");
       $parent_folder_id = $parent_folder->o_id;
       $merchantContentAudioObject->setParentId($parent_folder_id);
       $merchantContentAudioObject->save();

       $merchantContentDealObject = new BzDeals();
       $merchantContentDealObject->setKey('SampleDeal');
       $parent_folder = DataObject::getByPath("/solutions//".$solution_key."/parent_".$solution_key."/Merchants//".$merchant_name."/".$merchant_name."/Deals");
       $parent_folder_id = $parent_folder->o_id;
       $merchantContentDealObject->setParentId($parent_folder_id);
       $merchantContentDealObject->save();

       $merchantContentEventObject = new BzEvents();
       $merchantContentEventObject->setKey('SampleEvent');
       $parent_folder = DataObject::getByPath("/solutions//".$solution_key."/parent_".$solution_key."/Merchants//".$merchant_name."/".$merchant_name."/Events");
       $parent_folder_id = $parent_folder->o_id;
       $merchantContentEventObject->setParentId($parent_folder_id);
       $merchantContentEventObject->save();

       $merchantContentListingObject = new BzListingBuilders();
       $merchantContentListingObject->setKey('SampleListing');
       $parent_folder = DataObject::getByPath("/solutions//".$solution_key."/parent_".$solution_key."/Merchants//".$merchant_name."/".$merchant_name."/Listings");
       $parent_folder_id = $parent_folder->o_id;
       $merchantContentListingObject->setParentId($parent_folder_id);
       $merchantContentListingObject->save();

       $merchantContentVideoObject = new BzVideo();
       $merchantContentVideoObject->setKey('SampleVideo');
       $parent_folder = DataObject::getByPath("/solutions//".$solution_key."/parent_".$solution_key."/Merchants//".$merchant_name."/".$merchant_name."/Videos");
       $parent_folder_id = $parent_folder->o_id;
       $merchantContentVideoObject->setParentId($parent_folder_id);
       $merchantContentVideoObject->save();


       $assetMerchantNameFolder = new AssetFolder();
       $assetMerchantNameFolder->setType('folder');
       $assetMerchantNameFolder->setFilename($merchant_name);
       $parent_folder = Asset::getByPath("/images/solutions//".$solution_key."/Merchants//");
       $parent_folder_id = $parent_folder->getId();
       $assetMerchantNameFolder->setParentId($parent_folder_id);
       $assetMerchantNameFolder->save();

       $source_array = array('Images', 'Other Docs', 'PDF', 'Videos');
       foreach ($source_array as $src) {
       $assetMerchantContentFolder = new AssetFolder();
       $assetMerchantContentFolder->setType('folder');
       $assetMerchantContentFolder->setFilename($src);
       $parent_folder = Asset::getByPath("/images/solutions//".$solution_key."/Merchants//".$merchant_name);
       $parent_folder_id = $parent_folder->getId();
       $assetMerchantContentFolder->setParentId($parent_folder_id);
       $assetMerchantContentFolder->save();
      }


       $this->output->writeln('********* Merchant Folder and MerchantContentObjects Created Successfully *************');



              $type_user = 'user';
              $className = User\Service::getClassNameForType($type_user);
              $solution_parentid = 0;

              $userSolutionFolder = new UserFolder();
              try{
                $userSolutionFolder->setName('solutions');
                $userSolutionFolder->setType('userfolder');
                $userSolutionFolder->setParentId($solution_parentid);
                $userSolutionFolder = $userSolutionFolder->save();
                $this->output->writeln('********* Solutions User Folder solutions created New *************');
              }catch(\Exception $e){
                if($e instanceof \Doctrine\DBAL\Exception\UniqueConstraintViolationException){
                  $this->output->writeln('********* Solutions User Folder solutions Exists::Not creating New *************');
                  $userSolutionFolder->getByNameAndType('solutions', 'userfolder');
                }else{
                  $this->output->writeln('********* Received Exception while creating userSolutionFolder :: Cannot Continue:: Returning *************');
                  return;
                }
              }
              $this->output->writeln('********* Created userSolutionFolder Successfully *************');

              $userSolutionnameFolder = new UserFolder();
              try{
                $userSolutionnameFolder->setName($solution_key);
                $userSolutionnameFolder->setType('userfolder');
                $parent_folder_id = $userSolutionFolder->id;
                $userSolutionnameFolder->setParentId($parent_folder_id);
                $userSolutionnameFolder = $userSolutionnameFolder->save();

                $this->output->writeln('********* solution_key User Folder created New *************= ' .$solution_key);
              }catch(\Exception $e){
                if($e instanceof \Doctrine\DBAL\Exception\UniqueConstraintViolationException){
                  $this->output->writeln('*********solution_key Folder Exists::Not creating New *************'.$solution_key);
                  try{
                    //$userSolutionnameFolder = $userSolutionnameFolder->getByNameAndType($solution_key, 'userfolder');
                    $userSolutionnameFolder->getByNameAndType($solution_key, 'userfolder');
                  }catch(\Exception $e){
                    $this->output->writeln('********* Received Exception while calling getByNameAndType :: solution_key ::  *************'.$solution_key. '::::Messgae = '.$e->getMessage());
                  }
                }else{
                  $this->output->writeln('********* Received Exception while creating solution_key Folder :: Cannot Continue:: Returning *************');
                  //return;
                }
              }
              $this->output->writeln('********* Created solution_key Successfully *************');


              $userMerchantFolder = new UserFolder();
              try{
                $userMerchantFolder->setName('Merchants');
                $userMerchantFolder->setType('userfolder');
                $parent_folder_id = $userSolutionnameFolder->id;
                $userMerchantFolder = $userMerchantFolder->setParentId($parent_folder_id);
                $userMerchantFolder = $userMerchantFolder->save();

                $this->output->writeln('********* Merchants User Folder created New = ');
              }catch(\Exception $e){
                if($e instanceof \Doctrine\DBAL\Exception\UniqueConstraintViolationException){
                  $this->output->writeln('********* Solutions User userMerchantFolder Folder Exists::Not creating New userMerchantFolder =');
                  try{
                    $userMerchantFolder->getByNameAndType('Merchants', 'userfolder');
                  }catch(\Exception $e){
                    $this->output->writeln('********* Received Exception while calling getByNameAndType :: Merchants ::  *************Message = '.$e->getMessage(). '******Returning******');
                    return;
                  }
                }else{
                  $this->output->writeln('********* Received Exception while creating userMerchantFolder :: Cannot Continue:: Returning *************');
                  return;
                }
              }
              $this->output->writeln('********* Created Solutions userMerchantFolder Successfully*************');

              $userMerchantNameFolder = new UserFolder();
              try{
                $userMerchantNameFolder->setName($merchant_name);
                $userMerchantNameFolder->setType('userfolder');
                $parent_folder_id = $userMerchantFolder->id;
                $userMerchantNameFolder = $userMerchantNameFolder->setParentId($parent_folder_id);
                $userMerchantNameFolder = $userMerchantNameFolder->save();
                $this->output->writeln('********* userMerchantNameFolder created New For=*************'.$merchant_name);
              }catch(\Exception $e){
                if($e instanceof \Doctrine\DBAL\Exception\UniqueConstraintViolationException){
                  $this->output->writeln('********* Solutions User Merchant 2 Folder Exists::Not creating New *************');
                  try{
                    //$userMerchantNameFolder = $userMerchantNameFolder->getByNameAndType($merchant_name, 'userfolder');
                    $userMerchantNameFolder->getByNameAndType($merchant_name, 'userfolder');
                  }catch(\Exception $e){
                    $this->output->writeln('********* Received Exception while calling getByNameAndType :: Merchants ::  *************Message = '.$e->getMessage());
                  }
                }else{
                  $this->output->writeln('******** Received Exception while creating userMerchantFolder :: Cannot Continue:: Returning *************');
                  return;
                }
              }

              $this->output->writeln('********* Created Solutions userMerchantFolder Successfully*************');

              $userMerchantAdminFolder = new UserFolder();
              try{
                $admin_group_name = 'Admin_'.$merchant_name.'_id_'.$userMerchantNameFolder->id;
                $userMerchantAdminFolder->setName($admin_group_name);
                $userMerchantAdminFolder->setType('userfolder');
                $parent_folder_id = $userMerchantNameFolder->id;
                $userMerchantAdminFolder = $userMerchantAdminFolder->setParentId($parent_folder_id);
                $userMerchantAdminFolder = $userMerchantAdminFolder->save();
                $this->output->writeln('********* Merchants Admin Folder created New =*************'.$admin_group_name);
              }catch(\Exception $e){
                if($e instanceof \Doctrine\DBAL\Exception\UniqueConstraintViolationException){
                  $this->output->writeln('********* Merchant Admin Folder Exists::Not creating New *************');
                  try{
                    //$userMerchantAdminFolder = $userMerchantAdminFolder->getByNameAndType($admin_group_name, 'userfolder');
                    $userMerchantAdminFolder->getByNameAndType($admin_group_name, 'userfolder');
                  }catch(\Exception $e){
                    $this->output->writeln('********* Received Exception while calling getByNameAndType :: Merchant Admin ::  *************Message = '.$e->getMessage());
                  }
                }else{
                  $this->output->writeln('******** Received Exception while creating userMerchantAdminFolder :: Cannot Continue:: Returning *************');
                  return;
                }
              }

              $this->output->writeln('********* Created Solutions UserMerchantAdminFolder Successfully*************');

              $type_user = 'user';
              $className = User\Service::getClassNameForType($type_user);
              $userMerchantAdminFolder = $userMerchantAdminFolder->id;

              $parentId = $userMerchantAdminFolder;
              $name = $merchant_name.'_admin';
              $active = 1;

              $password = Tool\Authentication::getPasswordHash($name, 'Admin1234$');

                $user = $className::create([
                  'parentId' => $parentId,
                  'name' => $name,
                  'password' => $password,
                  'active' => $active
                ]);


           //$userFolder = UserFolder::getByName("All Users/Merchants");
           $list = new User\Role\Listing();
           $list->setCondition('name = ?', 'merchant_admin');
           $list->load();

           $roleList = $list->getRoles();

           /** @var $role User\Role */
           $roles = [];
           foreach ($roleList as $role) {
               $roles[] = $role->getId();
           }
           $user->setRoles($roles);
           $user->setPermission('assets', true);
           $user->setPermission('objects', true);
           $user->setPermission('documents', false);

           $user = $user->save();

           $merchant_asset_path = "/images/solutions//".$solution_key."/Merchants//".$merchant_name;

           $workspaceAssetArray = array();
           $asset_array_object = new WorkspaceAsset();
           $element = Element\Service::getElementByPath('asset', $merchant_asset_path);
           if($element){
             $assetpath_id = $element->getId();
             $assetpath_fullpath = $element->getRealFullPath();
           }
           $asset_array_object->cpath = $assetpath_fullpath;
           $asset_array_object->cid = $assetpath_id;
           $asset_array_object->userId = $user->getId();
           $asset_array_object->list = true;
           $asset_array_object->view = true;
           $asset_array_object->save = true;
           $asset_array_object->publish = true;
           $asset_array_object->delete = true;
           $asset_array_object->rename = true;
           $asset_array_object->create = true;
           $asset_array_object->settings = false;
           $asset_array_object->versions = false;
           $asset_array_object->properties = false;
           $asset_array_object->lEdit = false;
           $asset_array_object->lView = false;
           $asset_array_object->layouts = false;
           //$asset_array_object->id =

           // assign it to the array here; you don't need the [0] index then
           $workspaceAssetArray[] = $asset_array_object;

           $user = $user->setWorkspacesAsset($workspaceAssetArray);

           // $objcopy = unserialize(serialize($asset_array_object));
           // $array_spaces = $asset_array[0];
           // array_push($array_spaces,$objcopy);
           // $asset_array[0] = $array_spaces;
           // $workspaces_new->asset = $asset_array;
           //
           // $workspaces_new->object = [];
           // $object_array = $workspaces_new->object = array();
           // //$asset_array_spaces = $asset_array->spaces = [];
           // array_push($object_array, []);


           $workspaceObjectArray = array();
           $object_array_obj = new WorkspaceDataobject();
           $merchant_object_path = "/solutions//".$solution_key."/parent_".$solution_key."/Merchants//".$merchant_name."/".$merchant_name;
           $element = Element\Service::getElementByPath('object', $merchant_object_path);
           if($element){
             $objectpath_id = $element->getId();
             $objectpath_fullpath = $element->getRealFullPath();
           }

           $object_array_obj->path = $merchant_name_path;
           //$object_array_obj = WorkspaceDataobject::getId();
           //$results2 = print_r($user, true);
           //\Pimcore\Log\Simple::log("event.log", 'Inside merchant Creator Command before user save:: object_array_obj = '.$results2);

           $object_array_obj->cpath = $objectpath_fullpath;
           $object_array_obj->cid = $objectpath_id;
           $object_array_obj->userId = $user->getId();
           $object_array_obj->list = true;
           $object_array_obj->view = true;
           $object_array_obj->save = true;
           $object_array_obj->unpublish = true;
           $object_array_obj->publish = true;
           $object_array_obj->delete = true;
           $object_array_obj->rename = true;
           $object_array_obj->create = true;
           $object_array_obj->settings = false;
           $object_array_obj->versions = false;
           $object_array_obj->properties = false;
           $object_array_obj->lEdit = false;
           $object_array_obj->lView = false;
           $object_array_obj->layouts = false;

           $workspaceObjectArray[] = $object_array_obj;

           $user = $user->setWorkspacesObject($workspaceObjectArray);
           //$results2 = print_r($user, true);
           //\Pimcore\Log\Simple::log("event.log", 'Inside merchant Creator Command before user save:: user2 = '.$results2);


           // $objcopy = unserialize(serialize($object_array_obj));
           // $array_spaces = $object_array[0];
           // array_push($array_spaces,$objcopy);
           // $object_array[0] = $array_spaces;
           // $workspaces_new->object = $object_array;

           //$workspaces_new->document = [];

           //$results2 = print_r($object_array_obj, true);
           //\Pimcore\Log\Simple::log("event.log", 'Inside merchant controller object_array_obj = '.$results2);

           //$results2 = print_r($workspaces_new, true);
           //\Pimcore\Log\Simple::log("event.log", 'Inside merchant controller workspaces_new = '.$results2);


           // $workspaces = $workspaces_new;
           // foreach ($workspaces as $type => $spaces) {
           //   $newWorkspaces = [];
           //   //$results2 = print_r($type, true);
           //   //\Pimcore\Log\Simple::log("event.log", 'Inside merchant controller type = '.$results2);
           //
           //     foreach ($spaces as $space) {
           //         $element = Element\Service::getElementByPath($type, $space['path']);
           //         //$results2 = print_r($element, true);
           //         //\Pimcore\Log\Simple::log("event.log", 'Inside merchant controller element = '.$results2);
           //
           //         if ($element) {
           //             $className = '\\Pimcore\\Model\\User\\Workspace\\' . Element\Service::getBaseClassNameForElement($type);
           //             //$results2 = print_r($className, true);
           //             //\Pimcore\Log\Simple::log("event.log", 'Inside merchant controller Inside If of the element className= '.$results2);
           //
           //             $workspace = new $className();
           //             $workspace->setValues($space);
           //             $workspace->setCid($element->getId());
           //             $workspace->setCpath($element->getRealFullPath());
           //             $workspace->setUserId($user->getId());
           //
           //             $newWorkspaces[] = $workspace;
           //         }
           //     }
           //
           //     $user->{'setWorkspaces' . ucfirst($type)}($newWorkspaces);
           // }


           $user->save();
           $this->output->writeln('********* Successfully Created User*************');

      }
    }
}
