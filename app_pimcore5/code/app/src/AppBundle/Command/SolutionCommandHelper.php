<?php

namespace AppBundle\Command;


use Pimcore\Model\DataObject\Service as Dataservice;
use Pimcore\Model\DataObject;



class SolutionCommandHelper
{

  private $DataobjectService;

  public function __construct(Dataservice $dataobjectService)
  {
    $this->DataobjectService = $dataobjectService;
  }

  public function copyFolder(string $fromPath,string $toPath){
      //$results2 = print_r($fromPath, true);
      //\Pimcore\Log\Simple::log("event.log", 'Inside SolutionCommandHelper fromPath= '.$results2);
      //\Pimcore\Log\Simple::log("event.log", 'Inside SolutionCommandHelper toPath= '.$toPath);

     $sourceFolder = DataObject::getByPath($fromPath);//$fromPath
     //$copyObject = elementalias::cloneMe($copyObjectPath);
     //$parent_path = "/library/master/".$domain_name;//$toPath
     $destination_parent_folder = DataObject::getByPath($toPath);
     //$results2 = print_r($destination_parent_folder, true);
     //\Pimcore\Log\Simple::log("event.log", 'Inside SolutionCommandHelperTo Folder = '.$results2);
    try{
      $copyObject = $this->DataobjectService->copyRecursive($destination_parent_folder, $sourceFolder);
    }
    catch(Exception $e){
      \Pimcore\Log\Simple::log("event.log", 'Caught Exception during copyRecursive Inside SolutionCommandHelper::copyGlobal'.$e->getMessage());
    }
    $parent_folder_id = $destination_parent_folder->o_id;
    $copyObject->setParentId($parent_folder_id);
    $copyObject->save();
  }

}
