<?php

namespace AppBundle\Command;

use Pimcore\Console\AbstractCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Input\InputArgument;
use Pimcore\Model\Element\Service as elementalias;
use Pimcore\Model\Webservice;
use Pimcore\Model\DataObject;
use Pimcore\Model\DataObject\Service as Dataservice;
use Pimcore\Model\DataObject\BzPublishQueue\Listing;
use Pimcore\Model\DataObject\BzDomain;
use Pimcore\Model\DataObject\BzSolutions;
use Pimcore\Model\Object\Folder;
use Pimcore\Model\Asset;
use Pimcore\Model\Document;
use Pimcore\Db;
use AppBundle\Command\SolutionCommandHelper;


class CopySolutionContentCommandC5 extends AbstractCommand
{
    private $DataobjectService;

    public function __construct(Dataservice $dataobjectService)
    {
      $this->DataobjectService = $dataobjectService;

      parent::__construct();
    }

    protected function configure()
    {
        $this
            ->setName('bizzns:copy-solution-content-master-part2')
            ->setDescription('Command to create an instance of bizzns solution')
            ->addArgument('domainname', InputArgument::REQUIRED, 'Bizzns Domainname to be created.')
            ->addArgument('marketname', InputArgument::REQUIRED, 'Bizzns Marketname to be created.')
            ->addArgument('solutionname', InputArgument::REQUIRED, 'Bizzns Solutionname to be created. AlphaNumeric with - or _ :: No spaces');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        Db::getConnection()->getConfiguration()->setSQLLogger(null);

        $domain_name = $input->getArgument('domainname');
        $domain_name_exists = DataObject::getByPath("/organization/Domains/".$domain_name);
        if(is_null($domain_name_exists)){
          $this->output->writeln('********* Please create Domain Name by executing Command1 *************');
        }

        $market_name = $input->getArgument('marketname');
        $market_name_exists = DataObject::getByPath("/organization/MarketArea/".$market_name);
        if(is_null($market_name_exists)){
          $this->output->writeln('********* Please create Market Name by executing Command1 *************');
        }

        $solution_key = $input->getArgument('solutionname');
        $solution_name_exists = DataObject::getByPath("/solutions//".$solution_key);
        if(is_null($solution_name_exists)){
          $this->output->writeln('********* Please create Solution Name by executing Command1 *************');
        }else{
          $source_array = array('layouts', 'listings', 'menu', 'news', 'video');
          foreach ($source_array as $src) {
            $fromPath = '/library/master/'.$domain_name.'/'.$src;
            //$results2 = print_r($fromPath, true);
            //\Pimcore\Log\Simple::log("event.log", 'Inside CopySolutionContentCommand fromPath= '.$results2);

            $toPath = "/solutions//".$solution_key."/parent_".$solution_key."/Content";
            //$results2 = print_r($toPath, true);
            //\Pimcore\Log\Simple::log("event.log", 'Inside CopySolutionContentCommand toPath= '.$results2);

            $helper = new SolutionCommandHelper($this->DataobjectService);
            $copy_global = $helper->copyFolder($fromPath, $toPath);
            $this->dump('Copied src ='.$src);
          }
        }

        return;

    }
}
