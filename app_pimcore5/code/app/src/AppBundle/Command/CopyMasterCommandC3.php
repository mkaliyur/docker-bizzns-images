<?php

namespace AppBundle\Command;

use Pimcore\Console\AbstractCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Input\InputArgument;
use Pimcore\Model\Element\Service as elementalias;
use Pimcore\Model\Webservice;
use Pimcore\Model\DataObject;
use Pimcore\Model\DataObject\Service as Dataservice;
use Pimcore\Model\DataObject\BzPublishQueue\Listing;
use Pimcore\Model\DataObject\BzDomain;
use Pimcore\Model\DataObject\BzSolutions;
use Pimcore\Model\Object\Folder;
use Pimcore\Model\Asset;
use Pimcore\Model\Document;
use Pimcore\Db;
use AppBundle\Command\SolutionCommandHelper;




class CopyMasterCommandC3 extends AbstractCommand
{
    private $DataobjectService;

    public function __construct(Dataservice $dataobjectService)
    {
      $this->DataobjectService = $dataobjectService;

      parent::__construct();
    }

    protected function configure()
    {
        $this
            ->setName('bizzns:copy-master-part2')
            ->setDescription('Command to create an instance of bizzns solution')
            ->addArgument('domainname', InputArgument::REQUIRED, 'Bizzns Domainname to be created.')
            ->addArgument('marketname', InputArgument::REQUIRED, 'Bizzns Marketname to be created.')
            ->addArgument('solutionname', InputArgument::REQUIRED, 'Bizzns Solutionname to be created. AlphaNumeric with - or _ :: No spaces');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        Db::getConnection()->getConfiguration()->setSQLLogger(null);

        $domain_name = $input->getArgument('domainname');
        $domain_name_exists = DataObject::getByPath("/organization/Domains/".$domain_name);
        if(is_null($domain_name_exists)){
          $this->output->writeln('********* This Domain Name and Library Master Folder Name Already Exists:: Copying Master from Global*************');
          \Pimcore\Log\Simple::log("event.log", 'Inside Solution create inside if');


        }
        else{
          //Copy Master files from Global to Domain Level Master
          $source_array = array('layouts', 'listings', 'menu', 'news', 'video');
          foreach ($source_array as $src) {
            $fromPath = '/library/Global/'.$src;
            $toPath = '/library/master/'.$domain_name;
            $helper = new SolutionCommandHelper($this->DataobjectService);
            $copy_global = $helper->copyFolder($fromPath, $toPath);
            $this->dump('Copied src ='.$src);
          }

        }


        return;


















        //Process Market
        $market_name = $input->getArgument('marketname');
        $market_name_exists = DataObject::getByPath("/organization/MarketArea/".$market_name);
        if(!is_null($market_name_exists)){
          $this->output->writeln('********* This Market Name Already Exists *************');
        }else{
          //$this->output->writeln('********* In else This Market Name doesnt Exist *************');
          $bz_market_name = new DataObject\BzMarketArea();
          $bz_market_name->setKey($market_name);
          $bz_market_name->setName($market_name.'_market');
          $market_parent_path = "/organization/MarketArea";
          $market_parent_folder_path = DataObject::getByPath($market_parent_path);
          $market_parent_folder_id = $market_parent_folder_path->o_id;
          $bz_market_name->setParentId($market_parent_folder_id);
          \Pimcore\Log\Simple::log("event.log", 'Inside Solution create saving market_name= '.$market_name);
          $bz_market_name->save();


          return;


          $fromPath = '/library/Global/audio';
          $toPath = '/library/master/'.$domain_name;
          $helper = new SolutionCommandHelper($this->DataobjectService);
          $copy_global = $helper->copyFolder($fromPath, $toPath);

          $fromPath = '/library/Global/news';
          $toPath = '/library/master/'.$domain_name;
          $helper = new SolutionCommandHelper($this->DataobjectService);
          $copy_global = $helper->copyFolder($fromPath, $toPath);




          $copyObjectPath = DataObject::getByPath("/library/Global");
          //$copyObject = elementalias::cloneMe($copyObjectPath);
          $parent_path = "/library/master/".$domain_name;
          $parent_folder_path = DataObject::getByPath($parent_path);
          try{
            $copyObject = $this->DataobjectService->copyRecursive($parent_folder_path, $copyObjectPath);
          }
          catch(Exception $e){
            \Pimcore\Log\Simple::log("event.log", 'Caught Exception saving library Master Inside Solution create Inside catch'.$e->getMessage());
          }
          $parent_folder_id = $parent_folder_path->o_id;
          $copyObject->setParentId($parent_folder_id);
          $copyObject->save();
          \Pimcore\Cache::enable();


        }


        $solution_key = $input->getArgument('solutionname');
        if(is_null($solution_key)){
          $solution_key = \Pimcore\File::getValidFilename($market_name.'_'.$domain_name);
        }

        if(is_null($solution_key)){
         return;
        }


        $solution_name_exists = DataObject::getByPath("/organization/Solutions/".$solution_key);
        if(!is_null($solution_name_exists)){
          $this->output->writeln('********* This Solution Name Already Exists *************');
          //return;
        }else{
          $bz_solutions = new DataObject\BzSolutions();
          $bz_solutions->setKey($solution_key);
          $parent_folder = DataObject::getByPath("/organization/Solutions");
          $parent_folder_id = $parent_folder->o_id;
          $bz_solutions->setParentId($parent_folder_id);
          $bz_solutions->setName($solution_key.'_solution');
          $domainHref_path = DataObject::getByPath("/organization/Domains/".$domain_name);
          $bz_solutions->setDomain_href($domainHref_path);
          $marketHref_path = DataObject::getByPath("/organization/MarketArea/".$market_name);
          $bz_solutions->setMarket_href($marketHref_path);
          $bz_solutions->save();
        }



        $solutionFolder = new Folder();
        $solutionFolder->setKey($solution_key);
        $parent_folder = DataObject::getByPath("/solutions");
        $parent_folder_id = $parent_folder->o_id;
        $solutionFolder->setParentId($parent_folder_id);
        $solutionFolder->save();
        $this->output->writeln('********* Just created Folder under solutions *************'.$solution_key);

        $solutionParent = new DataObject\BzParent();
        $solutionParent->setKey("parent_".$solution_key);

        $solutionParent->setDomain_name($domain_name);
        $solutionParent->setMarket_name($market_name);

        $marketHref_Object = DataObject::getByPath("/organization/MarketArea/".$market_name);
        $solutionParent->setMarket_area_href($marketHref_Object);

        $domainHref_Object = DataObject::getByPath("/organization/Domains/".$domain_name);
        $solutionParent->setDomain_href($domainHref_Object);

        $masterListing_object = $domainHref_Object->getMaster_listing();
        $solutionParent->setListing_inherits_from($masterListing_object);

        $masterDeal_object = $domainHref_Object->getMaster_deal();
        $solutionParent->setDeal_inherits_from($masterDeal_object);

        $masterEvent_object = $domainHref_Object->getMaster_event();
        $solutionParent->setEvent_inherits_from($masterEvent_object);

        $masterVideo_object = $domainHref_Object->getMaster_audio();
        $solutionParent->setAudio_inherits_from($masterVideo_object);

        $masterVideo_object = $domainHref_Object->getMaster_video();
        $solutionParent->setVideo_inherits_from($masterVideo_object);

        $masterArticle_object = $domainHref_Object->getMaster_article();
        $solutionParent->setArticle_inherits_from($masterArticle_object);

        $masterNews_object = $domainHref_Object->getMaster_news();
        $solutionParent->setNews_inherits_from($masterNews_object);

        $parent_folder_id2 = $solutionFolder->o_id;
        $solutionParent->setParentId($parent_folder_id2);
        $solutionParent->save();
        $this->output->writeln('********* Just created solutionParent under *************'.$solution_key);

        $contentFolder = new Folder();
        $contentFolder->setKey("Content");
        $parent_folder_content = $solutionParent->o_id;
        $contentFolder->setParentId($parent_folder_content);
        $contentFolder->save();

        $copyContentObjectPath = DataObject::getByPath("/library/master/".$domain_name);
        //$copyObject = elementalias::cloneMe($copyObjectPath);
        $parent_path = "/solutions//".$solution_key."//parent_/".$solution_key. "/Content";
        $parent_folder_path = DataObject::getByPath($parent_path);
        try{
          $copyContentObject = $this->DataobjectService->copyRecursive($parent_folder_path, $copyContentObjectPath);
        }
        catch(Exception $e){
          \Pimcore\Log\Simple::log("event.log", 'Inside Solution create Content Copy Inside catch'.$e->getMessage());
        }
        $parent_folder_id = $parent_folder_path->o_id;
        $copyContentObject->setParentId($parent_folder_id);
        $copyContentObject->save();

        // $copyContentObjectPath = DataObject::getByPath("/library/master/".$domain_name);
        // //$copyObject = elementalias::cloneMe($copyObjectPath);
        // //$parent_path_content = "/solutions/build52_hyd52/parent_build52_hyd52/Content";
        // $parent_path_content = "/solutions//".$solution_key."//parent_/".$solution_key. "/Content";
        // $results2 = print_r($parent_path_content, true);
        // \Pimcore\Log\Simple::log("event.log", 'Inside SolutionCreatorCommand parent_path_content='.$results2);
        //

        // $parent_folder_path_content = DataObject::getByPath($parent_path_content);
        // try{
        //   $copyObject = $this->DataobjectService->copyRecursive($parent_folder_path_content, $copyContentObjectPath);
        // }
        // catch(Exception $e){
        //   \Pimcore\Log\Simple::log("event.log", 'Inside Solution create Content Copy Inside catch'.$e->getMessage());
        // }
        // $parent_folder_id = $parent_folder_path_content->o_id;
        // $copyContentObject->setParentId($parent_folder_id);
        // $copyContentObject->save();

        $merchantFolder = new Folder();
        $merchantFolder->setKey("Merchants");
        $parent_folder_merchant = $solutionParent->o_id;
        $merchantFolder->setParentId($parent_folder_merchant);
        $merchantFolder->save();

        $this->output->writeln('********* Creating Domain Market and Solution and Solution Parent*************');
        //$this->output->writeln('******Bizzns Solutionname has been Created*******.');

    }
}
