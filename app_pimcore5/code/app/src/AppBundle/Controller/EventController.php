<?php
namespace AppBundle\Controller;

use Website\Controller\Action;
use Pimcore\Controller\FrontendController;
use Pimcore\Model\DataObject;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Pimcore\Model\DataObject\BzEvents;
use Pimcore\Model\DataObject\BzReservables;
use Pimcore\Http\Exception\ResponseException;
use Utils\Ds\Map;


class EventController extends FrontendController
{


    private  function createSlug($str, $delimiter = '-'){
        $slug = strtolower(trim(preg_replace('/[\s-]+/', $delimiter, preg_replace('/[^A-Za-z0-9-]+/', $delimiter, preg_replace('/[&]/', 'and', preg_replace('/[\']/', '', iconv('UTF-8', 'ASCII//TRANSLIT', $str))))), $delimiter));
        return $slug;
    }

    private function getserverbase(){
        $server_base = $this->document->getProperty('server_base');
        return $server_base;
    }


    public function getstorageMap(){
      $storage = new Map();
      return $storage;
    }

    private function getreservableMap($storageMap, $key, $value){
      \Pimcore\Log\Simple::log("event.log", 'Beginning key=='.$key);
      \Pimcore\Log\Simple::log("event.log", 'Beginning value=='.$value->name);


      if ($storageMap->isEmpty()){
        \Pimcore\Log\Simple::log("event.log", 'a Inside Event Controller :: Inside if::getreservableMap Func after isEmpty ');

        $storageMap->put($key, []);
        \Pimcore\Log\Simple::log("event.log", 'b Inside Event Controller :: Inside if::getreservableMap Func after storagemap ');

        $value_array = $storageMap->get($key);
        \Pimcore\Log\Simple::log("event.log", 'c Inside Event Controller :: Inside if::getreservableMap Func after valuearray =='.$value_array);

        array_push($value_array, $value);
        $results2 = print_r($value_array, true);
        \Pimcore\Log\Simple::log("event.log", 'c after array push valuearray =='.$results2);

        $storageMap->put($key, $value_array);
        \Pimcore\Log\Simple::log("event.log", 'd Inside Event Controller :: Inside if::getreservableMap Func before return ');

        return $storageMap;
      }else{
        $keyExists = $storageMap->hasKey($key);
        \Pimcore\Log\Simple::log("event.log", 'key='.$key .'for value='.$value->name);
        \Pimcore\Log\Simple::log("event.log", 'keyexists='.$keyExists);


        if($keyExists){
          \Pimcore\Log\Simple::log("event.log", 'f  keyexists is true');

          $value_array = $storageMap->get($key);

          if($value_array !== null){ //Key Exists. Add to existing value.
            array_push($value_array, $value);
            $storageMap->put($key, $value_array);
          }else{  //Key Exists, but it is not current key
            $storageMap->put($key, []);
            $value_array = $storageMap->get($key);
            array_push($value_array, $value);
            $storageMap->put($key, $value_array);
          }
           
        }else{ //Key doesnot exists
          \Pimcore\Log\Simple::log("event.log", 'i keyexists is false');

          $storageMap->put($key, []);
          $value_array = $storageMap->get($key);
          array_push($value_array, $value);
          $storageMap->put($key, $value_array);
        }
        \Pimcore\Log\Simple::log("event.log", 'j Inside Event Controller :: Inside else::getreservableMap Func before return ');

        return $storageMap;
      }
    }
    // private function getreservableMap($storageMap, $key, $value){
    //   if($storageMap->count() == 0){
    //     $keyClass = new \stdClass();
    //     $keyClass->key = $key;
    //     $storageMap[$keyClass] = [];
    //     $value_array = $storageMap[$keyClass];
    //     array_push($value_array, $value);
    //     $storageMap[$keyClass] = $value_array;
    //     return $storageMap;
    //   }else{
    //     foreach($storageMap as $lkey){
    //     $keyClass = new \stdClass();
    //     $keyClass->key = $key;

    //     if($lkey == $keyClass){
    //       $value_array = $storageMap[$lkey];
    //       array_push($value_array, $value);
    //       $storageMap[$lkey] = $value_array;
    //       return $storageMap;      
    //     }else{
    //       $storageMap[$keyClass] = [];
    //       $value_array = $storageMap[$keyClass];
    //       array_push($value_array, $value);
    //       $storageMap[$keyClass] = $value_array;
    //       return $storageMap;
    //     }
    //    }
    //     //$results2 = print_r($storageMap[$keyClass], true);
    //     //\Pimcore\Log\Simple::log("event.log", 'Inside Event Controller :: Inside getreservableMap storageMapkeyClass=:: '.$results2);
    //     return $storageMap;      
    //   }
    // }

    public function getImageDetails($server_base,$image){
                $ret = new \stdClass();
                $ret->type = $image->getType();
                $ret->id = $image->getId();
                $ret->parentId = $image->getParentId();
                $ret->parent= $image->getParent();
                $ret->filename = $image->getFilename();
                $ret->path = $image->getPath();
                $ret->mime_type = $image->getMimetype();
                $ret->post_mime_type = $image->getMimetype();
                $serverPath = $server_base;
                $ret->guid = $serverPath.$image->getPath().$image->getFilename();
                return $ret;
    }


    public function eventDetailAction(Request $request)
    {

        $pagedata = new \stdClass();
        $domainid = $this->document->getProperty('domainid');

        //Get id param of listing to be $retrieved.
        $event_id = $request->get('eventid');


        $eventItem = BzEvents::getById($event_id);

        if (!$eventItem instanceof BzEvents || !$eventItem->isPublished()) {
            $response = new \stdClass();
            $response->status_code = "404";
            $response->message = sprintf('EventId %s not Found', $event_id);
            $pagedata->response = $response;
            $this->view->pagedata = $pagedata->response;
            return;
        }
        //$domain = $listing->getDomain();
        //$images = $listing->getImages();

        $response = new \stdClass();

        $response->indexing_metadata = new \stdClass();

        $type =  $eventItem->getObject_type();
        $solution_name =  $eventItem->getSolution_name();
        $id = $eventItem->getId();

        $response->indexing_metadata->_index = $solution_name;
        $response->indexing_metadata->_type = $type;
        $response->indexing_metadata->_id = $id;

        $response->meta = new \stdClass();
        $response->meta->lookup = new \stdClass();
        $response->meta->lookup->P = "Parking";
        $response->meta->lookup->S = "Stall";
        $response->meta->lookup->H = "On Hold";
        $response->meta->lookup->A = "Available";
        $response->meta->lookup->B = "Booked";
        $response->meta->lookup->R = "Reserved";
        $response->meta->lookup->NA = "Not Available";

        $response->context = new \stdClass();

        $domain_href = $eventItem->getDomain_href();
        $response->context->domain_href = new \stdClass();
        if(!is_null($domain_href)){
          $response->context->domain_href->name = $domain_href->getName();
          $response->context->domain_href_id = $domain_href->getId();
          $response->context->domain_href_path = $domain_href->getPath();
          $response->context->domain_href_key = $domain_href->getKey();
        //$response->context->domain_href_classname = $domain_href->o_className;
        }else{
          $response->context->domain_href = new \stdClass();
        }

        $market_area_href = $eventItem->getMarket_area_href();
        $response->context->market_area_href = new \stdClass();
        if(!is_null($market_area_href)){
          $response->context->market_area_href->name = $market_area_href->getName();
          $response->context->market_area_href_id = $market_area_href->getId();
          $response->context->market_area_href_path = $market_area_href->getPath();
          $response->context->market_area_href_key = $market_area_href->getKey();
          $response->context->market_area_href_classname = $market_area_href->getClassName();
        //$response->market_area_hreforig = $market_area_href;
        }else{
          $response->context->market_area_href = new \stdClass();
        }

        $merchant_href = $eventItem->getMerchant_href();
        $response->context->merchant_href = new \stdClass();
        if(!is_null($merchant_href)){
          $response->context->merchant_href->name = $merchant_href->getName();
          $response->context->merchant_href_id = $merchant_href->getName();
          $response->context->merchant_href_path = $merchant_href->getPath();
          $response->context->merchant_href_key = $merchant_href->getKey();
          $response->context->merchant_href_classname = $merchant_href->getClassName();
        }else{
          $response->context->merchant_href = new \stdClass();
        }

        //$response->event = $eventItem;
        $response->domain = new \stdClass();
        $domain = $this->document->getProperty('domain');
        //$response->event = $eventItem;
        //$response->doc = $this;
        $response->domain = $domain;
        $response->name = $eventItem->getName();
        $response->object_type = $eventItem->getObject_type();
        $response->title = $eventItem->getTitle() ;
        $response->listOrder = $eventItem->getListorder();
        $response->organisers = $eventItem->getOrganisers();
        $response->event_start = $eventItem->getEvent_start();
        $response->event_end = $eventItem->getEvent_end();
        $response->duration = $eventItem->getDuration();
        $response->duration_units = $eventItem->getDuration_units();
        $response->event_location = $eventItem->getEvent_location();
        $response->eventDescription = $eventItem->getEvent_description();
        $response->event_calendar_url = $eventItem->getEvent_calendar_url();
        $response->event_webpage_url = $eventItem->getEvent_webpage_url();
        $response->slug = $eventItem->getSlug();
        if( !isset($response->slug)){
            $response->slug = $this->createSlug($eventItem->getName() );
        }

        $response->reservableList = new \stdClass();
        $reservableMap =  new Map(); 
        //var_dump($reservableMap);

        if ( !is_null ($eventItem->getReservable() ) && !empty($eventItem->getReservable()->getItems() ) ){
          foreach((array) $eventItem->getReservable()->getItems() as $item ){
              $results2 = print_r($item, true);
              \Pimcore\Log\Simple::log("event.log", 'Inside Event Controller :: Inside if of BzEventStall:: item$$$$$$='.$results2);

            $item_type = $item->getType();
            if($item_type == 'BzEventStall'  ){
              $item_copy = clone $item;
              $ret = new \stdClass();
              $ret->name = $item_copy->getName();
              $ret->reservable_id = $item_copy->getReservable_id();
              $ret->category = $item_copy->getCategory();
              $ret->price = $item_copy->getPrice();
              $ret->status = $item_copy->getStatus();
              $ret->action = $item_copy->getAction();
              $ret->typedisplayname = $item_copy->getTypedisplayname();
              $ret->type = 'Stall';
              $ret->inventory = $item_copy->getInventory();
              $reservableMap = $this->getreservableMap($reservableMap, 'EventStall', $ret); 
              //var_dump($reservableItem);
              // $results2 = print_r($item->getType(), true);
              // \Pimcore\Log\Simple::log("event.log", 'Inside Event Controller :: Not BzEventStall:: Inside else='.$results2);
              //$reservableMap = $reservableMap.union($reservableItem) ; 
              // array_push($response->reservableList,$ret);
            }

            if($item_type == 'BzEventParking'  ){
              \Pimcore\Log\Simple::log("event.log", 'Inside Event Controller :: Not BzEventStall:: Inside else='.$results2);
              //$reservableMap = $this->getstorageMap();
              $itemcopy = clone $item;
              $ret = new \stdClass();
              $ret->name = $itemcopy->getName();
              $ret->reservable_id = $itemcopy->getReservable_id();
              $ret->category = $itemcopy->getCategory();
              $ret->price = $itemcopy->getPrice();
              $ret->status = $itemcopy->getStatus();
              $ret->action = $itemcopy->getAction();
              $ret->typedisplayname = $itemcopy->getTypedisplayname();
              $ret->type = 'Parking';
              $ret->inventory = $itemcopy->getInventory();
              $reservableMap = $this->getreservableMap($reservableMap, 'EventParking', $ret);
            }
          }
          
          $response->reservableList = $reservableMap;
        }

        // $response->reservableList = [];
        // if ( !is_null($eventItem->getReservable() ) && !empty($eventItem->getReservable()->getItems() )   ){
        //   foreach((array) $eventItem->getReservable()->getItems() as $item ){
        //     if ( method_exists($item, 'getBzEventStall' ) &&   !empty($item->getBzEventStall())){
        //         if($item->getType() == 'BzEventStall'  ){
        //             $reservable_item = new \stdClass();
        //             $reservable_item->type = "Shop";
        //             $reservable_item->reservables = [];
        //             foreach($item->getBzEventStall()->getData() as $key=>$value) {
        //               $reservable = new \stdClass();
        //               $reservable->key = $key;
        //               foreach($value as $key1=>$value1) {
        //                 $reservable->value = $value1;
        //               }
        //               array_push($reservable_item->reservables,$reservable);
        //             }
        //             array_push($response->reservableList,$reservable_item);
        //         }
        //     }
        //     if ( method_exists($item, 'getBzEventParking' ) && !empty($item->getBzEventParking() ) ){
        //         if($item->getType() == 'BzEventParking'  ){
        //             $reservable_item = new \stdClass();
        //             $reservable_item->type = "Parking";
        //             $reservable_item->reservables = [];
        //             foreach($item->getBzEventParking()->getData() as $key=>$value) {
        //               $reservable = new \stdClass();
        //               $reservable->key = $key;
        //               foreach($value as $key1=>$value1) {
        //                 $reservable->value = $value1;
        //               }
        //               array_push($reservable_item->reservables,$reservable);
        //             }
        //             array_push($response->reservableList,$reservable_item);
        //         }
        //     }

        //   }
        // }

        $server_base = $this->document->getProperty('server_base');
        $eventImage = $eventItem->getEventImage();
        if ( null!== $eventImage && null!== $eventImage && null !== $eventImage->getFilename() ){
          $response->eventImage = $this->getImageDetails($server_base,$eventImage );
        }else{
          $response->eventImage = new \stdClass();
        }

        $response->reservablelist = [];
        if(!empty($eventItem->getReservablehref())){
            foreach( (array) $eventItem->getReservablehref() as $reservable ){
                //$reservablecopy = $eventItem->getReservablehref();
                $ret = new \stdClass();
                $ret->name = $reservable->getName();
                $ret->reservable_id = $reservable->getReservable_id();
                $ret->category = $reservable->getCategory();
                $ret->price = $reservable->getPrice();
                $ret->status = $reservable->getStatus();
                $ret->action = $reservable->getAction();
                $ret->displayname = $reservable->getTypedisplayname();
                $ret->inventory = $reservable->getInventory();
                $ret->objecttype = $reservable->getObject_type();
                $ret->solutionname = $reservable->getSolution_name();
                $ret->type = $reservable->getRtype();
                array_push($response->reservablelist, $ret);
            }
             //$response->reservablelist = array_values(array_unique($response->reservablelist));
        }


        $pagedata->response = new \stdClass();
        $pagedata->response = $response;
        $this->view->pagedata = $pagedata->response;
    }

    public function eventReservableAction(Request $request)
    {
      $pagedata = new \stdClass();
        $domainid = $this->document->getProperty('domainid');

        //Get id param of listing to be $retrieved.
        $reservable_id = $request->get('reservableid');


        $reservableItem = BzReservables::getById($reservable_id);

        if (!$reservableItem instanceof BzReservables || !$reservableItem->isPublished()) {
            $response = new \stdClass();
            $response->status_code = "404";
            $response->message = sprintf('ReservableId %s not Found', $reservable_id);
            $pagedata->response = $response;
            $this->view->pagedata = $pagedata->response;
            return;
        }

        $response = new \stdClass();

        $response->indexing_metadata = new \stdClass();

        $type =  $reservableItem->getObject_type();
        $solution_name =  $reservableItem->getSolution_name();
        $id = $reservableItem->getId();

        $response->indexing_metadata->_index = $solution_name;
        $response->indexing_metadata->_type = $type;
        $response->indexing_metadata->_id = $id;
        
        $response->name = $reservableItem->getName();
        $response->reservable_id = $reservableItem->getReservable_id();
        $response->category = $reservableItem->getCategory();
        $response->price = $reservableItem->getPrice();
        $response->status = $reservableItem->getStatus();
        $response->action = $reservableItem->getAction();
        $response->typedisplayname = $reservableItem->getTypedisplayname();
        $response->type = 'Parking';
        $response->inventory = $reservableItem->getInventory();

        $pagedata->response = new \stdClass();
        $pagedata->response = $response;
        $this->view->pagedata = $pagedata->response;
    }




}


?>
