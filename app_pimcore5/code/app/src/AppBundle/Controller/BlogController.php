<?php

namespace AppBundle\Controller;

use Pimcore\Controller\FrontendController;
use Pimcore\Model\DataObject;
use Symfony\Component\HttpFoundation\Request;
use Zend\Paginator\Paginator;

class BlogController extends FrontendController
{

    private  function createSlug($str, $delimiter = '-'){
        $slug = strtolower(trim(preg_replace('/[\s-]+/', $delimiter, preg_replace('/[^A-Za-z0-9-]+/', $delimiter, preg_replace('/[&]/', 'and', preg_replace('/[\']/', '', iconv('UTF-8', 'ASCII//TRANSLIT', $str))))), $delimiter));
        return $slug;
    }


    private function getserverbase(){
        $server_base = $this->document->getProperty('server_base');
        return $server_base;
    }

    public function getImageDetails($server_base,$image){
                $ret = new \stdClass();
                //$results2 = print_r($ret, true);
                //\Pimcore\Log\Simple::log("event.log", 'Inside Blog Controller poster image ret='.$results2);

                $ret->type = $image->getType();
                $ret->id = $image->getId();
                $ret->parentId = $image->getParentId();
                $ret->parent= $image->getParent();
                $ret->filename = $image->getFilename();
                $ret->path = $image->getPath();
                $ret->mime_type = $image->getMimetype();
                $ret->post_mime_type = $image->getMimetype();
                $serverPath = $server_base;
                $ret->guid = $serverPath.$image->getPath().$image->getFilename();
                return $ret;
              }



    public function indexAction(Request $request)
    {
        //$this->enableLayout();

        // get a list of news objects and order them by date
        $blogList = new DataObject\BzBlogArticle\Listing();
        $blogList->setOrderKey("date");
        $blogList->setOrder("DESC");

        $conditions = [];

        if ($request->get("category")) {
            $conditions[] = "categories LIKE " . $blogList->quote("%," . (int) $request->get("category") . ",%");
        }

        if ($request->get("archive")) {
            $conditions[] = "DATE_FORMAT(FROM_UNIXTIME(date), '%Y-%c') = " . $blogList->quote($request->get("archive"));
        }

        if (!empty($conditions)) {
            $blogList->setCondition(implode(" AND ", $conditions));
        }

        //$paginator = \Zend_Paginator::factory($blogList);
        $paginator = new \Zend\Paginator\Paginator($blogList);
        $paginator->setCurrentPageNumber($request->get('page'));
        $paginator->setItemCountPerPage(5);

        $this->view->articles = $paginator;

        // get all categories
        $categories = DataObject\BlogCategory::getList(); // this is an alternative way to get an object list
        $this->view->categories = $categories;

        // archive information, we have to do this in pure SQL
        $db = \Pimcore\Db::get();
        $ranges = $db->fetchCol("SELECT DATE_FORMAT(FROM_UNIXTIME(date), '%Y-%c') as ranges FROM object_5 GROUP BY DATE_FORMAT(FROM_UNIXTIME(date), '%b-%Y') ORDER BY ranges ASC");
        $this->view->archiveRanges = $ranges;
    }

    public function detailAction(Request $request)
    {
        //$this->enableLayout();

        // "id" is the named parameters in "Static Routes"
        $article = DataObject\BzBlogArticle::getById($request->get("id"));

        if (!$article instanceof DataObject\BzBlogArticle || !$article->isPublished()) {
            $response = new \stdClass();
            $response->status_code = "404";
            $response->message = sprintf('Blog ID %s not Found', $blog_id);
            $pagedata->response = $response;
            $this->view->pagedata = $pagedata->response;
            return;
        }

        $this->view->article = $article;
    }


    public function detailJsonAction(Request $request)
    {


        $pagedata = new \stdClass();
        // $pagedata->name="Listing detail Page";
        //$pagedata->title=new \stdClass();
        //$pagedata->title->rendered="Listing Detail Page";
        //$pagedata->slug = createSlug ()
        //get domainid property
        $domainid = $this->document->getProperty('domainid');

        //Get id param of listing to be $retrieved.
        $blog_id = $request->get('blogid');



        // "id" is the named parameters in "Static Routes"
        $article = DataObject\BzBlogArticle::getById($blog_id);
        $results2 = print_r($article, true);
        \Pimcore\Log\Simple::log("event.log", 'Inside Blog Controller article='.$results2);

        //$response = new \stdClass();


        if (!$article instanceof DataObject\BzBlogArticle || !$article->isPublished()) {
          $response = new \stdClass();
          $response->status_code = "404";
          $response->message = sprintf('Blog ID %s not Found', $blog_id);
          $pagedata->response = $response;
          $this->view->pagedata = $pagedata->response;
          return;
        }

        $response = new \stdClass();

        $response->indexing_metadata = new \stdClass();

        $type =  $article->getObject_type();
        $solution_name =  $article->getSolution_name();
        $id = $article->getId();

        $response->indexing_metadata->_index = $solution_name;
        $response->indexing_metadata->_type = $type;
        $response->indexing_metadata->_id = $id;


        $response->context = new \stdClass();

        $domain_href = $article->getDomain_href();
        $response->context->domain_href = new \stdClass();
        if(!is_null($domain_href)){
        $response->context->domain_href->name = $domain_href->getName();
        $response->context->domain_href_id = $domain_href->getId();
        $response->context->domain_href_path = $domain_href->getPath();
        $response->context->domain_href_key = $domain_href->getKey();
        //$response->context->domain_href_classname = $domain_href->o_className;
      }else{
        $response->context->domain_href = new \stdClass();
      }

        $market_area_href = $article->getMarket_area_href();
        $response->context->market_area_href = new \stdClass();
        if(!is_null($market_area_href)){
        $response->context->market_area_href->name = $market_area_href->getName();
        $response->context->market_area_href_id = $market_area_href->getId();
        $response->context->market_area_href_path = $market_area_href->getPath();
        $response->context->market_area_href_key = $market_area_href->getKey();
        $response->context->market_area_href_classname = $market_area_href->getClassName();
        //$response->market_area_hreforig = $market_area_href;
      }else{
        $response->context->market_area_href = new \stdClass();
      }

        $merchant_href = $article->getMerchant_href();
        $response->context->merchant_href = new \stdClass();
        if(!is_null($merchant_href)){
        $response->context->merchant_href->name = $merchant_href->getName();
        $response->context->merchant_href_id = $merchant_href->getId();
        $response->context->merchant_href_path = $merchant_href->getPath();
        $response->context->merchant_href_key = $merchant_href->getKey();
        $response->context->merchant_href_classname = $merchant_href->getClassName();
      }else{
        $response->context->merchant_href = new \stdClass();
      }

        $response->name=$article->getO_key();
        $response->title = $article->getTitle();
        //$response->headline->$article->headline;
        //$response->slug = $article->slug||$this->createSlug($article->o_key);
        //$blogItems = $article->getLocalizedfields()->getItems();
        $response->content = new \stdClass();
        $response->tagnames = [];
        //$response->keys =[];
        //$response->keys1 =[];

        $response->title = $article->getTitle();
        $response->object_type = $article->getObject_type();
        $response->text = $article->getText();
        $response->headline = $article->getHeadline();

        $response->slug = $article->getSlug();
        if( !isset($response->slug)){
            $response->slug = $this->createSlug($article->o_key);
        }

        $response->tags = $article->getTags();

        $response->isPromotedToHomePage = $article->getIsPromotedToHomePage();
        $response->briefSummary = $article->getBriefSummary();


        // foreach($blogItems as $key => $value) {
        //     $keyvalue = new \stdClass();
        //     $keyvalue->key = $key;
        //     $keyvalue->value = $value;
        //     //array_push($response->keys, $keyvalue);
        //     if($key == 'en'){
        //         //$text = new \stdClass();
        //        // $text = $value->text;
        //         foreach($value as $key1 => $value1) {
        //             $keyvalue1 = new \stdClass();
        //             $keyvalue1->key = $key1;
        //             $keyvalue1->value = $value1;
        //             //array_push($response->keys1, $keyvalue1);
        //
        //             if($key1 == 'title'){
        //                 $response->title->rendered=$value1;
        //             }
        //
        //             if($key1 == 'object_type'){
        //               $response->object_type = $value1;
        //             }
        //
        //             if($key1 == 'text'){
        //                 $response->content = $value1;
        //             }
        //
        //             if($key1 == 'headline'){
        //                 $response->headline = $value1;
        //             }
        //
        //             if($key1 == 'slug'){
        //                 $response->slug = $value1;
        //
        //                 if( !isset($response->slug)){
        //                     $response->slug = $this->createSlug($article->o_key);
        //                 }
        //
        //             }
        //
        //             if($key1 == 'tags'){
        //                 array_push($response->tagnames,$value1);
        //             }
        //
        //             if($key1 == 'isPromotedToHomePage'){
        //                 $response->isPromotedToHomePage- $value1;
        //             }
        //
        //             if($key1 == 'briefSummary'){
        //                 $response->briefSummary= $value1;
        //             }
        //         }
        //     }
        // }
        $response->date = $article->getDate();
        $response->author = $article->getAuthor();
        $server_base = $this->document->getProperty('server_base');

        if(!is_null($article->getPoster_Image())){
          $posterImage = $article->getPoster_Image()->getImage();
          $response->poster_Image = $this->getImageDetails($server_base,$posterImage );
        }else{
          $response->poster_Image = null;
        }

        $response->images = [];
        if(!empty($article->getImages() )){
            foreach((array) $article->getImages() as $image ){
                $server_base = $this->document->getProperty('server_base');
                $ret = $this->getImageDetails($server_base,$image);

                array_push($response->images,$ret);
            }
        }


        $response->categorynames = [];
        if(!empty($article->getCategories() )){
            foreach( (array) $article->getCategories() as $category ){
                $ret = new \stdClass();
                $ret->name = $category->getName();
                $name = $category->getName();
                $response->categorynames[] = $name;
                $ret->o_path = $category->getO_path();
                $doneParentProcessing = false;
                $pcat = $category->getParentCategory();
                $ret_back = $ret;
                while (!$doneParentProcessing){
                    if(!is_null($pcat) ){
                        $ret->parentCategory = new \stdClass();
                        $ret->parentCategory->name = $pcat->getName();
                        $name = $pcat->getName();
                        $ret->parentCategory->o_path = $pcat->getO_path();
                        //array_push($response->categorynames, $name);
                        $response->categorynames[] = $name;

                        if(is_null($pcat->getParentCategory() )){
                            $doneParentProcessing = true;
                        }else{
                            $pcat = $pcat->parentCategory;
                            $ret = $ret->parentCategory;
                        }

                    }else{
                        $doneParentProcessing = true;
                    }
                }

            }

            $response->categorynames = array_values (array_unique ($response->categorynames));
        }

        //$response->article = $article;

        $pagedata->response = new \stdClass();
        $pagedata->response = $response;
        $this->view->pagedata = $pagedata->response;
    }

    private function getYTVideoId(string $youtubeUrl){
      if(is_null($youtubeUrl)){
        return '';
      }
      $link = $youtubeUrl;
      $ytvideo_id = explode("?v=", $link); // For videos like http://www.youtube.com/watch?v=...
      if (empty($ytvideo_id[1]))
          $ytvideo_id = explode("/v/", $link); // For videos like http://www.youtube.com/watch/v/..

      $ytvideo_id = explode("&", $ytvideo_id[1]); // Deleting any other params
      $ytvideo_id = $ytvideo_id[0];
      return $ytvideo_id;
    }
    public function detailVideoJsonAction(Request $request){

        $pagedata = new \stdClass();
        $domainid = $this->document->getProperty('domainid');

        //Get id param of listing to be $retrieved.
        $video_id = $request->get('videoid');


        $bzvideo = DataObject\BzVideo::getById($video_id);

        if (!$bzvideo instanceof DataObject\BzVideo || !$bzvideo->isPublished()) {
            // this will trigger a 404 error response
            $response = new \stdClass();
            $response->status_code = "404";
            $response->message = sprintf('VideoId %s not Found', $video_id);
            $pagedata->response = $response;
            $this->view->pagedata = $pagedata->response;
            return;
        }

        $response = new \stdClass();

        $response->indexing_metadata = new \stdClass();

        $type =  $bzvideo->getObject_type();
        $solution_name =  $bzvideo->getSolution_name();
        $id = $bzvideo->getId();

        $response->indexing_metadata->_index = $solution_name;
        $response->indexing_metadata->_type = $type;
        $response->indexing_metadata->_id = $id;


        $response->context = new \stdClass();


        $domain_href = $bzvideo->getDomain_href();
        $response->context->domain_href = new \stdClass();
        if(!is_null($domain_href)){
        $response->context->domain_href->name = $domain_href->getName();
        $response->context->domain_href_id = $domain_href->o_id;
        $response->context->domain_href_path = $domain_href->o_path;
        $response->context->domain_href_key = $domain_href->o_key;
        //$response->context->domain_href_classname = $domain_href->o_className;
       }else{
        $response->context->domain_href = new \stdClass();
       }

        $market_area_href = $bzvideo->getMarket_area_href();
        $response->context->market_area_href = new \stdClass();
        if(!is_null($market_area_href)){
        $response->context->market_area_href->name = $market_area_href->getName();
        $response->context->market_area_href_id = $market_area_href->o_id;
        $response->context->market_area_href_path = $market_area_href->o_path;
        $response->context->market_area_href_key = $market_area_href->o_key;
        $response->context->market_area_href_classname = $market_area_href->getClassName();
        //$response->market_area_hreforig = $market_area_href;
       }else{
        $response->context->market_area_href = new \stdClass();
       }

        $merchant_href = $bzvideo->getMerchant_href();
        $response->context->merchant_href = new \stdClass();
        if(!is_null($merchant_href)){
        $response->context->merchant_href->name = $merchant_href->getName();
        $response->context->merchant_href_id = $merchant_href->o_id;
        $response->context->merchant_href_path = $merchant_href->o_path;
        $response->context->merchant_href_key = $merchant_href->o_key;
        $response->context->merchant_href_classname = $merchant_href->getClassName();
        }else{
         $response->context->merchant_href = new \stdClass();
        }

        //$response->videoitem = $bzvideo;
        $response->videoName = $bzvideo->getName();

        $response->slug = $bzvideo->getSlug();
        if( !isset( $response->slug ) ){
            $response->slug = $this->createSlug($bzvideo->getVideoName() );
        }
        $response->object_type = $bzvideo->getObject_type();
        if(is_null($bzvideo->getTitle())){
            $response->title = "default";
        }else{
            $response->title = $bzvideo->getTitle();
        }

        $response->videoType = $bzvideo->getVideoType() ;
        $response->videoURL = $bzvideo->getVideoURL();
        $response->ytVideoId = $this->getYTVideoID($bzvideo->getVideoURL());
        $response->videoID = $bzvideo->getVideoID() ;
        $response->localVideoPath = $bzvideo->getLocalVideoPath();
        $response->video_blog = $bzvideo->getVideo_blog();
        $response->video_transcript = $bzvideo->getVideo_transcript();




        $pagedata->response = new \stdClass();
        $pagedata->response = $response;
        $this->view->pagedata = $pagedata->response;

    }



    public function detailAudioJsonAction(Request $request){

        $pagedata = new \stdClass();
        $domainid = $this->document->getProperty('domainid');

        //Get id param of listing to be $retrieved.
        $audio_id = $request->get('audioid');


        $bzaudio = DataObject\BzAudio::getById($audio_id);

        if (!$bzaudio instanceof DataObject\BzAudio || !$bzaudio->isPublished()) {
            // this will trigger a 404 error response
            $response = new \stdClass();
            $response->status_code = "404";
            $response->message = sprintf('AudioID %s not Found', $audio_id);
            $pagedata->response = $response;
            $this->view->pagedata = $pagedata->response;
            return;
        }

        $response = new \stdClass();

        $response->indexing_metadata = new \stdClass();

        $type =  $bzaudio->getObject_type();
        $solution_name =  $bzaudio->getSolution_name();
        $id = $bzaudio->getId();

        $response->indexing_metadata->_index = $solution_name;
        $response->indexing_metadata->_type = $type;
        $response->indexing_metadata->_id = $id;


        $response->context = new \stdClass();

        $domain_href = $bzaudio->getDomain_href();
        $response->context->domain_href = new \stdClass();
        if(!is_null($domain_href)){
        $response->context->domain_href->name = $domain_href->getName();
        $response->context->domain_href_id = $domain_href->o_id;
        $response->context->domain_href_path = $domain_href->o_path;
        $response->context->domain_href_key = $domain_href->o_key;
       }else{
        $response->context->domain_href = new \stdClass();
       }
        //$response->context->domain_href_classname = $domain_href->o_className;

        $market_area_href = $bzaudio->getMarket_area_href();
        $response->context->market_area_href = new \stdClass();
        if(!is_null($market_area_href)){
        $response->context->market_area_href->name = $market_area_href->getName();
        $response->context->market_area_href_id = $market_area_href->o_id;
        $response->context->market_area_href_path = $market_area_href->o_path;
        $response->context->market_area_href_key = $market_area_href->o_key;
        $response->context->market_area_href_classname = $market_area_href->getClassName();
        //$response->market_area_hreforig = $market_area_href;
       }else{
        $response->context->market_area_href = new \stdClass();
       }

        $merchant_href = $bzaudio->getMerchant_href();
        $response->context->merchant_href = new \stdClass();
        if(!is_null($merchant_href)){
        $response->context->merchant_href->name = $merchant_href->getName();
        $response->context->merchant_href_id = $merchant_href->o_id;
        $response->context->merchant_href_path = $merchant_href->o_path;
        $response->context->merchant_href_key = $merchant_href->o_key;
        $response->context->merchant_href_classname = $merchant_href->getClassName();
        }else{
         $response->context->merchant_href = new \stdClass();
        }

        $audioFile = $bzaudio->getAudioFile();
        $response->audioFile = new \stdClass();
        if(!is_null($audioFile)){
        $response->audioFile->name = $audioFile->getFilename();
        $response->audioFile->id = $audioFile->getId();
        $response->audioFile->path = $audioFile->getPath();
        $response->audioFile->key = $audioFile->getKey();
        //$response->audioFile_classname = $audioFile->getClassName();
        }else{
         $response->audioFile = new \stdClass();
        }


        //$response->audioitem = $bzaudio;


        $response->name = $bzaudio->getName();
        $response->slug = $bzaudio->getSlug();
        if( !isset( $response->slug ) ) {
            $response->slug = $this->createSlug($bzaudio->getName() );
        }
        $response->object_type = $bzaudio->getObject_type();
        $response->title = $bzaudio->getTitle() || "";

        $response->audioType = $bzaudio->getAudioType();
        $response->url = $bzaudio->getUrl();
        $response->audio_blog = $bzaudio->getAudio_blog();
        $response->audio_transcript = $bzaudio->getAudio_transcript();




        $pagedata->response = new \stdClass();
        $pagedata->response = $response;
        $this->view->pagedata = $pagedata->response;

    }



    public function sidebarBoxAction(Request $request)
    {
        $items = (int) $request->get("items");
        if (!$items) {
            $items = 3;
        }

        // this is the alternative way of getting a list of objects
        $blogList = DataObject\BzBlogArticle::getList([
            "limit" => $items,
            "order" => "DESC",
            "orderKey" => "date"
        ]);

        $this->view->articles = $blogList;
    }
}


?>
