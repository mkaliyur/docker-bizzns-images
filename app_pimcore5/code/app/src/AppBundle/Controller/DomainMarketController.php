<?php
namespace AppBundle\Controller;

use Website\Controller\Action;
use Pimcore\Controller\FrontendController;
use Pimcore\Model\Asset;
use Pimcore\Model\Document;
use Pimcore\Model\DataObject;
use Pimcore\Model;
use Pimcore\Tool;
use Pimcore\Model\Webservice;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Pimcore\Model\DataObject\BzPublishQueue\Listing;
use Pimcore\Model\DataObject\BzDomain;
use Pimcore\Model\DataObject\BzSolutions;
use Pimcore\Model\Object\Folder;


class DomainMarketController extends FrontendController
    {

      //private function libraryMasterCreate($domain_object) {
      //\Pimcore\Log\Simple::log("event.log", 'Inside Domain market Controller ::libraryMasterCreate action');
      // $bz_library_master = new Folder();
      // $bz_library_master->setKey($domain_object->name);
      // $bz_library_master->setName($domain_object->name.' Global Master');
      // $parent_path = "/library/master";
      // $parent_folder_path = DataObject::getByPath($parent_path);
      // $parent_folder_id = $parent_folder_path->o_id;
      // $bz_library_master->setParentId($parent_folder_id);
      // $bz_library_master->save();
    //}

    private function domainCreate($domain_object) {
      $this->view->response = $response;
      // $bz_domain_name = new DataObject\BzDomain();
      // $bz_domain_name->setKey($domain_object->name);
      // $bz_domain_name->setName($domain_object->name.'_domain');
      // $parent_path = "/organization/Domains";
      // $parent_folder_path = DataObject::getByPath($parent_path);
      // $parent_folder_id = $parent_folder_path->o_id;
      // $bz_domain_name->setParentId($parent_folder_id);
      // $bz_domain_name->save();
      // $response = new \stdClass();
      // $response->status = "Success";
      // $response->message = "This Domain Name ".$domain_name ." created successfully.";
      // return $response;

      // $copyObjectPath = DataObject::getByPath("/library/master/Global");
      // $copyObject = Pimcore\Model\Element\Service::cloneMe($copyObjectPath);
      // $parent_path = "/library/master/".$bz_domain_name;
      // $parent_folder_path = DataObject::getByPath($parent_path);
      // $parent_folder_id = $parent_folder_path->o_id;
      // $copyObject->setParentId($parent_folder_id);
      // $copyObject->save();
      // $response = new \stdClass();
      // $response->status = "Success";
      // $response->message = "This Child Object ".$copyObject ." created successfully.";
      // return $response;
    }

    private function marketCreate($market_object) {
      $bz_market_name = new DataObject\BzMarketArea();
      $bz_market_name->setKey($market_object->name);
      $bz_market_name->setName($market_object->name.'_market');
      $market_parent_path = "/organization/MarketArea";
      $market_parent_folder_path = DataObject::getByPath($market_parent_path);
      $market_parent_folder_id = $market_parent_folder_path->o_id;
      $bz_market_name->setParentId($market_parent_folder_id);
      $bz_market_name->save();
      $response = new \stdClass();
      $response->status = "Success";
      $response->message = "This market Name ".$market_name ." created successfully.";
    }

    public function domainCreateAction(Request $request)
    {
      $this->view->response = $response;
    }

    public function marketCreateAction(Request $request)
    {
      $this->view->response = $response;
    }
    public function newDomainCreateAction(Request $request){
      $is_secure = SecurityController::isSecure($request);
      if(!$is_secure){
        $response = new \stdClass();
        $response->message = "This request cannot be processed. Security test failed";
        return new response(json_encode($response));
      }

      $is_valid = SecurityController::isValid($request, 'POST');
      if(!($is_valid)){
        $response = new \stdClass();
        $response->message = "This request cannot be processed. Method should be POST.";
        return new response (json_encode($response));
      }

      $domain_name_array = $request->request->get("domain-name");
      $domain_name = $domain_name_array['name'];

      $results2 = print_r($domain_name_array, true);
    //  \Pimcore\Log\Simple::log("event.log", 'Inside solu instance domain_name_array= '. $results2);

      $domain_obj = new DataObject\BzDomain\Listing();
      $domain_obj->setCondition(" name =?", [$domain_name]);
      $domain_obj_map = $domain_obj->load();
      $results2 = print_r($domain_obj_map, true);
    //  \Pimcore\Log\Simple::log("event.log", 'Inside solu instance domain_obj_map= '. $results2);

      $domain_name_exists = DataObject::getByPath("/organization/Domains//".$domain_name);

      if(!is_null($domain_name_exists)){
        $response = new \stdClass();
        $response->status = "Error";
        $response->message = "This Domain Name ".$domain_name ." already exists.";
        $response_encoder = new Webservice\JsonEncoder();
        return $response_encoder->encode($response);
      }else{
        $domain_create_object = new \stdClass();
        $domain_create_object->name = $domain_name;
        $this->disableViewAutoRender();
        $createStatus = $this->domainCreate($domain_create_object);
        $this->libraryMasterCreate($domain_create_object);
        $response_encoder = new Webservice\JsonEncoder();
        return $response_encoder->encode($createStatus);
      }
    }

    public function newMarketCreateAction(Request $request){
      $is_secure = SecurityController::isSecure($request);
      if(!$is_secure){
        $response = new \stdClass();
        $response->message = "This request cannot be processed. Security test failed";
        return new response(json_encode($response));
      }

      $is_valid = SecurityController::isValid($request, 'POST');
      if(!($is_valid)){
        $response = new \stdClass();
        $response->message = "This request cannot be processed. Method should be POST.";
        return new response (json_encode($response));
      }

      $market_name_array = $request->request->get("market-name");
      $market_name = $market_name_array['name'];
      //$market_desc = $market_name_array['desc'];

      $market_name_exists = DataObject::getByPath("/organization/MarketArea//".$market_name);

      if(!is_null($market_name_exists)){
        $response = new \stdClass();
        $response->status = "Error";
        $response->message = "This market Name ".$market_name ." already exists.";
        $response_encoder = new Webservice\JsonEncoder();
        return $response_encoder->encode($response);;
      }else{
        $market_create_object = new \stdClass();
        $market_create_object->name = $market_name;

        $this->disableViewAutoRender();
        $createStatus = $this->marketCreate($market_create_object);
        $response_encoder = new Webservice\JsonEncoder();
        return $response_encoder->encode($createStatus);
     }
    }
}
?>
