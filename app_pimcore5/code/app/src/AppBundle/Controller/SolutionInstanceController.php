<?php
namespace AppBundle\Controller;

use Website\Controller\Action;
use Pimcore\Controller\FrontendController;
use AppBundle\Controller\SecurityController;
use Pimcore\Model\Asset;
use Pimcore\Model\Document;
use Pimcore\Model\DataObject;
use Pimcore\Model;
use Pimcore\Tool;
use Pimcore\Model\DataObject\BzSolutionInstance\Listing;
use Pimcore\Model\Webservice;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;


class SolutionInstanceController extends FrontendController
    {
    public function indexAction() {
        parent::indexAction();
    }

    private  function createSlug($str, $delimiter = '-'){
        $slug = strtolower(trim(preg_replace('/[\s-]+/', $delimiter, preg_replace('/[^A-Za-z0-9-]+/', $delimiter, preg_replace('/[&]/', 'and', preg_replace('/[\']/', '', iconv('UTF-8', 'ASCII//TRANSLIT', $str))))), $delimiter));
        return $slug;
    }

    public function solutionInstanceListAction(Request $request)
    {
      $pagedata = new \stdClass();
      $this->view->page_data = $pagedata;
    }

    public function solutionInstanceDetailAction(Request $request)
    {
      $pagedata = new \stdClass();

      $instance_id = $request->get('instanceid');
      //$results2 = print_r($instance_id, true);
      //\Pimcore\Log\Simple::log("event.log", 'Inside view of solu inst instance_id= '. $results2);

      $instanceObject = DataObject\BzSolutionInstance::getById($instance_id);
      //$results2 = print_r($instanceObject, true);
      //\Pimcore\Log\Simple::log("event.log", 'Inside view of solu inst instanceObject= '. $results2);

      $pagedata->instanceObject = $instanceObject;

      $this->view->page_data = $pagedata;
    }


    public function registerNewSolutionInstanceAction(Request $request){
      $is_secure = SecurityController::isSecure($request);
      if(!$is_secure){
        $response = new \stdClass();
        $response->message = "This request cannot be processed";
        return new response(json_encode($response));
      }

      $registering_entity = $request->request->get("solution-instance");
      $response = new \stdClass();
      $response->name = $registering_entity['name'];
      $response->description = $registering_entity['description'];

      $solutionInstanceListing = new DataObject\BzSolutionInstance\Listing();
      $solutionInstanceListing->setCondition(" name = ? ",[$response->name]);
      //$solutionInstanceListing->setFolder("test folder");
      $solutionInstanceListing->load();

      $results2 = print_r($solutionInstanceListing, true);
      // \Pimcore\Log\Simple::log("event.log", 'Inside solu instance solutionInstanceListing= '. $results2);

      return new response(json_encode($response));

       $this->disableViewAutoRender();
       //return $response;
    }
}
?>
