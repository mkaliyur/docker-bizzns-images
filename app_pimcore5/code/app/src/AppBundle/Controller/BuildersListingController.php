<?php
namespace AppBundle\Controller;

use Website\Controller\Action;
use Pimcore\Controller\FrontendController;
use Pimcore\Model\Asset;
use Pimcore\Model\Document;
use Pimcore\Model\DataObject;
use Pimcore\Model;
use Pimcore\Tool;
use Pimcore\Model\Webservice;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Pimcore\Model\DataObject\BzListing2;
use Pimcore\Model\DataObject\BzListingBuilders;


class BuildersListingController extends FrontendController
    {


    public function indexAction() {
        //$logger->error(">>>>>$$$$$  KGM:: INSIDE ListingController CONTROLLER ");

        parent::indexAction();
    }

    private  function createSlug($str, $delimiter = '-'){
        $slug = strtolower(trim(preg_replace('/[\s-]+/', $delimiter, preg_replace('/[^A-Za-z0-9-]+/', $delimiter, preg_replace('/[&]/', 'and', preg_replace('/[\']/', '', iconv('UTF-8', 'ASCII//TRANSLIT', $str))))), $delimiter));
        return $slug;
    }

    private function getImageDetails($server_base,$image){
              $ret = new \stdClass();
              $ret->type = $image->getType();
              $ret->id = $image->getId();
              $ret->parentId = $image->getParentId();
              $ret->parent= $image->getParent();
              $ret->filename = $image->getFilename();
              $ret->path = $image->getPath();
              $ret->mime_type = $image->getMimetype();
              $ret->post_mime_type = $image->getMimetype();
              $serverPath = $server_base;
              $ret->guid = $serverPath.$image->getPath().$image->getFilename();
              return $ret;
    }


    public function detailAction(Request $request)
    {

        $pagedata = new \stdClass();
        $domainid = $this->document->getProperty('domainid');

        //Get id param of listing to be $retrieved.
        $listing_id = $request->get('listingid');

        $listing = BzListingBuilders::getById($listing_id);

        if (!$listing instanceof BzListingBuilders || !$listing->isPublished()) {
            // this will trigger a 404 error response
            $response = new \stdClass();
            $response->status_code = "404";
            $response->message = sprintf('ListingId %s not Found', $listing_id);
            $pagedata->response = $response;
            $this->view->pagedata = $pagedata->response;
            return;
        }


        $response = new \stdClass();
        $response->indexing_metadata = new \stdClass();

        $type =  $listing->getObject_type();
        $solution_name =  $listing->getSolution_name();
        $id = $listing->getId();

        $response->indexing_metadata = new \stdClass();
        $response->indexing_metadata->_index = $solution_name;
        $response->indexing_metadata->_type = $type;
        $response->indexing_metadata->_id = $id;



        $response->context = new \stdClass();

        $domain_href = $listing->getDomain_href();
        $response->context->domain_href = new \stdClass();
        if(!is_null($domain_href)){
        $response->context->domain_href->name = $domain_href->getName();
        $response->context->domain_href_id = $domain_href->o_id;
        $response->context->domain_href_path = $domain_href->o_path;
        $response->context->domain_href_key = $domain_href->o_key;
      }else{
        $response->context->domain_href = new \stdClass();
      }
        //$response->context->domain_href_classname = $domain_href->o_className;

        $market_area_href = $listing->getMarket_area_href();
        $response->context->market_area_href = new \stdClass();
        if(!is_null($market_area_href)){
          $response->context->market_area_href->name = $market_area_href->getName();
          $response->context->market_area_href_id = $market_area_href->o_id;
          $response->context->market_area_href_path = $market_area_href->o_path;
          $response->context->market_area_href_key = $market_area_href->o_key;
          $response->context->market_area_href_classname = $market_area_href->getClassName();
        }else{
          $response->context->market_area_href = new \stdClass();
        }
        //$response->market_area_hreforig = $market_area_href;

        $merchant_href = $listing->getMerchant_href();
        $response->context->merchant_href = new \stdClass();
        if(!is_null($merchant_href)){
        $response->context->merchant_href->name = $merchant_href->getName();
        $response->context->merchant_href_id = $merchant_href->o_id;
        $response->context->merchant_href_path = $merchant_href->o_path;
        $response->context->merchant_href_key = $merchant_href->o_key;
        $response->context->merchant_href_classname = $merchant_href->getClassName();
       }else{
        $response->context->merchant_href = new \stdClass();
       }
        $response->__content = $listing->__content;
        if (!$listing->getName()){
            $response->name = $listing->getHeadline();
        }else{
            $response->name=$listing->getName();
        }

        $response->title=new \stdClass();
        $response->title = $listing->getTitle();
        $response->slug = $listing->getSlug();
        if(empty($listing->getSlug())){
            $response->slug = $this->createSlug($listing->getName());
        }else{
            $response->slug = "default";
        }
        $response->object_type = $listing->getObject_type();
        $response->headline = $listing->getHeadline();
        if ($listing->getLogo_image()){
            $server_base = $this->document->getProperty('server_base');
            $response->logo_image = $this->getImageDetails($server_base,$listing->getLogo_image() );
        }
        $response->shortDescription = $listing->getShortDescription();
        $response->rating = $listing->getRating();
        $response->isFeatured = $listing->getIsFeatured();
        $response->isPromoted = $listing->getIsPromoted();
        $response->isPremium = $listing->getIsPremium();
        $response->isClaimed = $listing->getIsClaimed();
        $response->listOrder = $listing->getListorder();
        $response->objectClassName = $listing->getObjectClassName();
        $response->tag_cloud = $listing->getTag_cloud();
        $response->address= new \stdClass();
        $response->address->address_line_1 = $listing->getAddress_line_1();
        $response->address->address_line_2 = $listing->getAddress_line_2();
        $response->address->city = $listing->getCity();
        $response->address->country = $listing->getCountry();
        $response->address->zip = $listing->getZip();
        $response->address->addressGeoLocation = $listing->getAddressGeoLocation();
        $response->mainContactNumber = $listing->getMainContactNumber();
        $response->websiteUrl = $listing->getWebsiteUrl();
        $response->hoursOfOperation = $listing->getHoursOfOperation();
        $response->email=$listing->getEmail();
        $response->additional_phones=$listing->getAdditional_phones();
        $response->services = $listing->getServices();
        $response->longDescription = $listing->getLongDescription();



        $response->popularlistinghref = [];
        if(!empty($listing->getPopularlistinghref())){
            foreach( (array) $listing->getPopularlistinghref() as $popularlisting ){
                $ret = new \stdClass();
                $ret->name = $popularlisting->getName();
                $ret->address = $popularlisting->getAddress_line_1().','.$popularlisting->getAddress_line_2().','.$popularlisting->getAddress_line_3();
                $ret->websiteUrl = $popularlisting->getWebsiteUrl();
                $ret->slug = $popularlisting->getSlug();
                if(empty($popularlisting->getSlug())){
                    $ret->slug = $this->createSlug($popularlisting->getName());
                }
                $bannerimages = $popularlisting->getBannerImages();
                if(!empty($bannerimages[0])){
                    $ret->popularImagePath = $bannerimages[0]->getPath().$bannerimages[0]->getFilename();
                }
                array_push($response->popularlistinghref, $ret);
            }
        }





        $response->images = [];
        if(!empty($listing->getImages() )){
            foreach((array) $listing->getImages() as $image ){
                $server_base = $this->document->getProperty('server_base');
                $ret = $this->getImageDetails($server_base,$image);

                array_push($response->images,$ret);
            }
        }

        $response->videos_local = [];
        if(!empty($listing->getVideos_local())){
            foreach( (array) $listing->getVideos_local() as $video ){
                $server_base = $this->document->getProperty('server_base');
                $ret = $this->getImageDetails($server_base,$video);
                array_push($response->videos_local,$ret);
            }
        }


        $response->bannerImages = [];
        if(!empty($listing->getBannerImages())){
            foreach((array) $listing->getBannerImages() as $image ){
                $server_base = $this->document->getProperty('server_base');
                $ret = $this->getImageDetails($server_base,$image);

                array_push($response->bannerImages,$ret);
            }
        }


        $response->tagnames = [];
        if(!empty($listing->getTags())){
            foreach( (array) $listing->getTags() as $tag ){
                $ret = new \stdClass();
                $ret-> name = $tag->o_key;
                $ret-> path = $tag->o_path;
                array_push($response->tagnames, $ret->name);
                //array_push($response->tag_objects,$ret);

            }
            $response->tagnames = array_values(array_unique($response->tagnames));
        }



        $response->categorynames = [];
        if(!empty($listing->getCategory())){
            foreach( (array) $listing->getCategory() as $category ){
                $ret = new \stdClass();
                $ret->name = $category->getName();
                $name = $category->getName();
                $response->categorynames[] = $name;
                $ret->o_path = $category->o_path;
                $doneParentProcessing = false;
                $pcat = $category->getParentCategory();
                $ret_back = $ret;
                while (!$doneParentProcessing){
                    if(!is_null($pcat) ){
                        $ret->parentCategory = new \stdClass();
                        $ret->parentCategory->name = $pcat->getName();
                        $name = $pcat->getName();
                        $ret->parentCategory->o_path = $pcat->getPath();
                        //array_push($response->categorynames, $name);
                        $response->categorynames[] = $name;

                        if(is_null($pcat->getParentCategory())){
                            $doneParentProcessing = true;
                        }else{
                            $pcat = $pcat->getParentCategory();
                            $ret = $ret->parentCategory;
                        }

                    }else{
                        $doneParentProcessing = true;
                    }
                }

            }
            $response->categorynames = array_values(array_unique($response->categorynames));
        }



        $response->external_video = [];
        if( !empty($listing->getExternal_media()) &&   !empty($listing->getExternal_media()->getItems() )){
            foreach( (array) $listing->getExternal_media()->getItems as $external_media_item ){
                $ret = new \stdClass();
                $ret->video_source = $external_media_item->video_source;
                $ret->video_url = $external_media_item->video_url;
                $ret->index = $external_media_item->index;
                array_push($response->external_video,$ret);
            }
        }

        $response->social_media = [];
        if( ( !empty($listing->getSocial_media()) &&  $listing->getSocial_media() !== null )  &&  !empty($listing->getSocial_media()->getItems() )){
            foreach( (array) $listing->getSocial_media->items as $social_media ){
                $ret = new \stdClass();
                $ret->social_media = $social_media->social_media;
                $ret->url = $social_media->url;
                $ret->index = $social_media->index;
                array_push($response->social_media,$ret);
            }
        }



        $response->benefits = [];
        if ( !is_null ($listing->getBenefits() ) && !empty($listing->getBenefits()->getItems() ) ){
            foreach((array) $listing->getBenefits()->getItems() as $item ){
                $ret = new \stdClass();
                $ret->benefitTitle = $item->getBenefitTitle();
                $ret->benefitDescription = $item->getBenefitDescription();
                $ret->benefitImage = $item->getBenefitImage();
                $ret->index = $item->getIndex();
                array_push($response->benefits,$ret);
            }
        }



        $response->amenitiesList = [];
        if ( !is_null($listing->getAmenities() ) && !empty($listing->getAmenities()->getItems() )   ){
          foreach((array) $listing->getAmenities()->getItems() as $item ){

            if ( method_exists($item, 'getAmenities' ) &&   !empty($item->getAmenities())){
                if($item->getType() == 'Amenities'  ){
                    $feature_item = new \stdClass();
                    $feature_item->type = "Amenities";
                    $feature_item->amenities = [];
                    foreach($item->getAmenities()->getData() as $key=>$value) {
                      $feature = new \stdClass();
                      $feature->key = $key;
                      foreach($value as $key1=>$value1) {
                        $feature->value = $value1;
                      }
                      array_push($feature_item->amenities,$feature);
                    }
                    array_push($response->amenitiesList,$feature_item);
                }
            }


          }
        }


        $response->testimonials = [];
        if ( !is_null( $listing->getTestimonials() ) && !empty($listing->getTestimonials()->getItems() ) ){
            foreach((array) $listing->getTestimonials()->getItems() as $item ){
                $ret = new \stdClass();
                $ret->author = $item->getAuthor();
                $ret->testimonial_text = $item->getTestimonial_text();
                $server_base = $this->document->getProperty('server_base');
                $image = $item->getAuthor_image();
                if( !is_null($image ) && !empty($image) ){
                  $authorImage = $this->getImageDetails($server_base,$image);
                  $ret->author_image = $authorImage;
                }else{
                  $authorImage = new \stdClass();
                }
                $ret->author_website = $item->getAuthor_website();
                $ret->index = $item->getIndex();
                array_push($response->testimonials,$ret);
            }
        }

        $response->carousel = [];
        if ( !is_null($listing->getSlide() ) && !empty($listing->getSlide()->getItems() ) ){
            foreach((array) $listing->getSlide()->getItems() as $item ){
                $ret = new \stdClass();
                $server_base = $this->document->getProperty('server_base');
                $image = $item->getImage();
                if (!is_null($image) && !empty($image)){
                  $slideImage = $this->getImageDetails($server_base,$image);
                }else{
                  $slideImage = new \stdClass();
                }

                $ret->type = $item->getType();
                $ret->image = $slideImage;
                $ret->caption = $item->getCaption();
                $ret->subCaption = $item->getSubCaption();
                array_push($response->carousel,$ret);
            }
        }


        $pagedata->response = new \stdClass();
        $pagedata->response = $response;
        //$pagedata->id=$listing_id;
        //$pagedata->images=$images;

        $this->view->pagedata = $pagedata->response;


    }


    public function categoryAction()
    {

        $pagedata = new \stdClass();
        $pagedata->name="Listing category Page";
        $this->view->pagedata = $pagedata;
    }






}


?>
