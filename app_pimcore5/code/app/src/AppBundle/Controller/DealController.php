<?php
namespace AppBundle\Controller;

use Website\Controller\Action;
use Pimcore\Controller\FrontendController;
use Pimcore\Model\DataObject;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Pimcore\Model\DataObject\BzDeals;

class DealController extends FrontendController
{


    private  function createSlug($str, $delimiter = '-'){
        $slug = strtolower(trim(preg_replace('/[\s-]+/', $delimiter, preg_replace('/[^A-Za-z0-9-]+/', $delimiter, preg_replace('/[&]/', 'and', preg_replace('/[\']/', '', iconv('UTF-8', 'ASCII//TRANSLIT', $str))))), $delimiter));
        return $slug;

    }




    private function getserverbase(){
        $server_base = $this->document->getProperty('server_base');
        return $server_base;
    }

    public function getImageDetails($server_base,$image){
                $ret = new \stdClass();
                if(!is_null($image)){
                  $ret->type = $image->getType();
                  $ret->id = $image->getId();
                  $ret->parentId = $image->getParentId();
                  $ret->parent= $image->getParent();
                  $ret->filename = $image->getFilename();
                  $ret->path = $image->getPath();
                  $ret->mime_type = $image->getMimetype();
                  $ret->post_mime_type = $image->getMimetype();
                  $serverPath = $server_base;
                  $ret->guid = $serverPath.$image->getPath().$image->getFilename();
                }else{
                  $ret->type = null;
                  $ret->id = null;
                  $ret->parentId = null;
                  $ret->parent= null;
                  $ret->filename = null;
                  $ret->path = null;
                  $ret->mime_type = null;
                  $ret->post_mime_type = null;
                  $serverPath = $server_base;
                  $ret->guid = null;
                }
                return $ret;
    }

    public function dealDetailAction(Request $request)
    {

        $pagedata = new \stdClass();
        $domainid = $this->document->getProperty('domainid');

        //Get id param of listing to be $retrieved.
        $deal_id = $request->get('dealid');


        $dealItem = BzDeals::getById($deal_id);

        if (!$dealItem instanceof BzDeals || !$dealItem->isPublished()) {
            // this will trigger a 404 error response
            $response = new \stdClass();
            $response->status_code = "404";
            $response->message = sprintf('DealId %s not Found', $deal_id);
            $pagedata->response = $response;
            $this->view->pagedata = $pagedata->response;
            return;
        }

        //$domain = $listing->getDomain();

        //$images = $listing->getImages();

        $response = new \stdClass();
        $response->indexing_metadata = new \stdClass();

        $type =  $dealItem->getObject_type();
        $solution_name =  $dealItem->getSolution_name();
        $id = $dealItem->getId();

        $response->indexing_metadata = new \stdClass();
        $response->indexing_metadata->_index = $solution_name;
        $response->indexing_metadata->_type = $type;
        $response->indexing_metadata->_id = $id;


        $response->context = new \stdClass();

        $domain_href = $dealItem->getDomain_href();
        $response->context->domain_href = new \stdClass();
        if(!is_null($domain_href)){
        $response->context->domain_href->name = $domain_href->getName();
        $response->context->domain_href_id = $domain_href->o_id;
        $response->context->domain_href_path = $domain_href->o_path;
        $response->context->domain_href_key = $domain_href->o_key;
        //$response->context->domain_href_classname = $domain_href->o_className;
       }else{
        $response->context->domain_href = new \stdClass();
       }

        $market_area_href = $dealItem->getMarket_area_href();
        $response->context->market_area_href = new \stdClass();
        if(!is_null($market_area_href)){
        $response->context->market_area_href->name = $market_area_href->getName();
        $response->context->market_area_href_id = $market_area_href->o_id;
        $response->context->market_area_href_path = $market_area_href->o_path;
        $response->context->market_area_href_key = $market_area_href->o_key;
        $response->context->market_area_href_classname = $market_area_href->getClassName();
        //$response->market_area_hreforig = $market_area_href;
      }
       else{
         $response->context->market_area_href = new \stdClass();
       }
        $merchant_href = $dealItem->getMerchant_href();
        $response->context->merchant_href = new \stdClass();
        if(!is_null($merchant_href)){
        $response->context->merchant_href->name = $merchant_href->getName();
        $response->context->merchant_href_id = $merchant_href->o_id;
        $response->context->merchant_href_path = $merchant_href->o_path;
        $response->context->merchant_href_key = $merchant_href->o_key;
        $response->context->merchant_href_classname = $merchant_href->getClassName();
      }else{
        $response->context->merchant_href = new \stdClass();
      }


        //$response->deal = $dealItem;
        $response->domain = new \stdClass();
        $domain = $this->document->getProperty('domain');
        //$response->deal = $dealItem;
        //$response->doc = $this;
        $response->domain = $domain;
        $response->name = $dealItem->getName();
        $response->object_type = $dealItem->getObject_type();
        $response->title = $dealItem->getTitle() || "";
        $response->breifDescription = $dealItem->getBreifDescription();
        $response->isActive = $dealItem->getIsActive() ;
        $response->dealType = $dealItem->getDealType() ;
        $response->dealUrl = $dealItem->getDealUrl();
        $response->startDate = $dealItem->getStartDate();
        $response->expirationDate = $dealItem->getExpirationDate();
        $response->dealOriginalPrice = $dealItem->getDealOriginalPrice();
        $response->dealFinalPrice = $dealItem->getDealFinalPrice();
        $response->dealTerms = $dealItem->getDealTerms();
        $response->slug = $dealItem->getSlug();
        if( !isset($response->slug)){
            $response->slug = $this->createSlug($dealItem->getName() );
        }

        $server_base = $this->document->getProperty('server_base');
        $dealImage = $dealItem->getDealImage();
        if ( null !== $dealImage && null !== $dealImage->getFilename() ){
        	$response->dealImage = $this->getImageDetails($server_base,$dealImage );
        }else{
        	$response->dealImage = new \stdClass();
        }




        $pagedata->response = new \stdClass();
        $pagedata->response = $response;


        $this->view->pagedata = $pagedata->response;
    }










}


?>
