<?php
namespace AppBundle\Controller;

use Pimcore\Model\DataObject;
use Pimcore\Model\Webservice;
use Pimcore\Controller\FrontendController;
//use Pimcore\Model\DataObject\News\Listing;
use Pimcore\Model\DataObject\BzNews\Listing;
use Zend\Paginator\Paginator;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;


class NewsController extends FrontendController
    {


    public function init() {
        parent::init();
    }


    private  function createSlug($str, $delimiter = '-'){
        $slug = strtolower(trim(preg_replace('/[\s-]+/', $delimiter, preg_replace('/[^A-Za-z0-9-]+/', $delimiter, preg_replace('/[&]/', 'and', preg_replace('/[\']/', '', iconv('UTF-8', 'ASCII//TRANSLIT', $str))))), $delimiter));
        return $slug;

    }

    private function getImageDetails($server_base,$image){
                $ret = new \stdClass();
                $ret->type = $image->getType();
                $ret->id = $image->getId();
                $ret->parentId = $image->getParentId();
                $ret->parent= $image->getParent();
                $ret->filename = $image->getFilename();
                $ret->path = $image->getPath();
                $ret->mime_type = $image->getMimetype();
                $ret->post_mime_type = $image->getMimetype();
                $serverPath = $server_base;
                $ret->guid = $serverPath.$image->getPath().$image->getFilename();
                return $ret;
    }



    public function detailAction(Request $request)
    {
        $pagedata = new \stdClass();
        $domainid = $this->document->getProperty('domainid');

        //Get id param of listing to be $retrieved.
        $news_id = $request->get('newsid');


        $newsItem = DataObject\BzNews::getById($news_id);

        if (!$newsItem instanceof DataObject\BzNews || !$newsItem->isPublished()) {
            // this will trigger a 404 error response
            $response = new \stdClass();
            $response->status_code = "404";
            $response->message = sprintf('NewsId %s not Found', $news_id);
            $pagedata->response = $response;
            $this->view->pagedata = $pagedata->response;
            return;
        }


        $response = new \stdClass();

        $response->indexing_metadata = new \stdClass();

        $type =  $newsItem->getObject_type();
        $solution_name =  $newsItem->getSolution_name();
        $id = $newsItem->getId();

        $response->indexing_metadata = new \stdClass();
        $response->indexing_metadata->_index = $solution_name;
        $response->indexing_metadata->_type = $type;
        $response->indexing_metadata->_id = $id;

        $response->context = new \stdClass();

        $domain_href = $newsItem->getDomain_href();
        $response->context->domain_href = new \stdClass();
        if(!is_null($domain_href)){
        $response->context->domain_href->name = $domain_href->getName();
        $response->context->domain_href_id = $domain_href->o_id;
        $response->context->domain_href_path = $domain_href->o_path;
        $response->context->domain_href_key = $domain_href->o_key;
        //$response->context->domain_href_classname = $domain_href->o_className;
        }else{
            $response->context->domain_href = new \stdClass();
          }

        $market_area_href = $newsItem->getMarket_area_href();
        $response->context->market_area_href = new \stdClass();
        if(!is_null($market_area_href)){
        $response->context->market_area_href->name = $market_area_href->getName();
        $response->context->market_area_href_id = $market_area_href->o_id;
        $response->context->market_area_href_path = $market_area_href->o_path;
        $response->context->market_area_href_key = $market_area_href->o_key;
        $response->context->market_area_href_classname = $market_area_href->getO_className();
        }else{
            $response->context->market_area_href = new \stdClass();
        }

        //$response->market_area_hreforig = $market_area_href;

        $merchant_href = $newsItem->getMerchant_href();
        $response->context->merchant_href = new \stdClass();
        if(!is_null($merchant_href)){
        $response->context->merchant_href->name = $merchant_href->getName();
        $response->context->merchant_href_id = $merchant_href->o_id;
        $response->context->merchant_href_path = $merchant_href->o_path;
        $response->context->merchant_href_key = $merchant_href->o_key;
        $response->context->merchant_href_classname = $merchant_href->o_className;
        }else{
            $response->context->merchant_href = new \stdClass();
        }


        $response->name=$newsItem->getO_key() ;
        $response->title=new \stdClass();
        $response->title->rendered = new \stdClass();
        //$response->newsItem = $newsItem;
        $response->date = $newsItem->getDate() ;
        $response->slug = $newsItem->getSlug() ;
        $response->solution_name = $newsItem->getSolution_name() ;

        if( !isset($response->slug) ){
            $response->slug = $this->createSlug($newsItem->getO_key() );
        }
        $response->headline = $newsItem->getHeadline();
        $response->title = $newsItem->getTitle();
        $response->shortText = $newsItem->getShortText();
        $response->text = $newsItem->getText();
        $response->tag_cloud = $newsItem->getTag_cloud();
        $response->topnews = $newsItem->getTopnews();
        //$response->briefSummary = $newsItem->getBriefSummary();

        $response->server_base = $this->document->getProperty('server_base');

        //$newsItems = $newsItem->getLocalizedfields()->getItems();


        // foreach($newsItems as $key => $value) {
        //     $keyvalue = new \stdClass();
        //     $keyvalue->key = $key;
        //     $keyvalue->value = $value;
        //     if($key == 'en'){
        //         //$text = new \stdClass();
        //        // $text = $value->text;
        //         foreach($value as $key1 => $value1) {
        //             $keyvalue1 = new \stdClass();
        //             $keyvalue1->key = $key1;
        //             $keyvalue1->value = $value1;
        //             //array_push($response->keys1, $keyvalue1);
        //
        //             if($key1 == 'headline'){
        //                 $response->headline = $value1;
        //             }
        //
        //             if($key1 == 'title'){
        //                 $response->title->rendered=$value1;
        //                 $response->slug = $this->createSlug($response->title->rendered);
        //             }
        //
        //             if($key1 == 'shortText'){
        //                 $response->shortText=$value1;
        //             }
        //
        //             if($key1 == 'text'){
        //                 $response->content = $value1;
        //             }
        //
        //             if($key1 == 'tags'){
        //                 array_push($response->tagnames,$value1);
        //             }
        //
        //             if($key1 == 'topnews'){
        //                 $response->isPromotedToHomePage- $value1;
        //             }
        //
        //             if($key1 == 'briefSummary'){
        //                 $response->briefSummary= $value1;
        //             }
        //         }
        //
        //     }
        // }

        $response->image1 = new \stdClass();
        if(!empty ($newsItem->getImage_1() ) ){
            $image1 = $newsItem->getImage_1()  ;
            $server_base = $this->document->getProperty('server_base');

            $ret = $this->getImageDetails($server_base,$image1);
            $response->image1 = $ret;
        }


        $response->image2 = new \stdClass();
        if(!empty ($newsItem->getImage_2()  ) ){
            $image2 = $newsItem->getImage_2()   ;
            $server_base = $this->document->getProperty('server_base');
            $ret = $this->getImageDetails($server_base,$image2 );
            $response->image2 =$ret;
        }


        $response->image3 = new \stdClass();
        if(!empty ($newsItem->getImage_3() ) ){
            $image3 = $newsItem->getImage_3() ;
            $server_base = $this->document->getProperty('server_base');
            $ret = $this->getImageDetails($server_base,$image3 );
            $response->image3 = $ret;
        }


        // $response->images = [];
        // if(  !is_null($newsItem->getImages()) &&    !empty($newsItem->getImages() ) ){
        //     foreach((array) $newsItem->getImages() as $image ){
        //         $server_base = $this->document->getProperty('server_base');
        //         $ret = $this->getImageDetails($server_base,$image);

        //         array_push($response->images,$ret);
        //     }
        // }



        $response->categorynames = [];
        if(  !is_null($newsItem->getCategories()) &&    !empty($newsItem->getCategories())){
            foreach( (array) $newsItem->getCategories() as $category ){
                $ret = new \stdClass();
                $ret->name = $category->getName();
                $name = $category->getName();
                $response->categorynames[] = $name;
                $ret->o_path = $category->getO_path() ;
                $doneParentProcessing = false;
                $pcat = $category->getParentNewsCategory() ;
                $ret_back = $ret;
                while (!$doneParentProcessing){
                    if(!is_null($pcat) ){
                        $ret->parentCategory = new \stdClass();
                        $ret->parentCategory->name = $pcat->getName() ;
                        $name = $pcat->name;
                        $ret->parentCategory->o_path = $pcat->getO_path() ;
                        //array_push($response->categorynames, $name);
                        $response->categorynames[] = $name;

                        if(is_null($pcat->getParentNewsCategory() )){
                            $doneParentProcessing = true;
                        }else{
                            $pcat = $pcat->getParentNewsCategory()  ;
                            $ret = $ret->parentCategory;
                        }

                    }else{
                        $doneParentProcessing = true;
                    }
                }

            }
            $response->categorynames = array_values(array_unique($response->categorynames));
        }



        $response->external_video = [];
        if( !is_null($newsItem->getExternal_media())  &&  !empty($newsItem->getExternal_media()->getItems() )){
            foreach( (array) $newsItem->getExternal_media()->getItems() as $external_media_item ){
                $ret = new \stdClass();
                $ret->video_source = $external_media_item->getVideo_source();
                $ret->video_url = $external_media_item->getVideo_url();
                $ret->index = $external_media_item->getIndex();
                array_push($response->external_video,$ret);
            }
        }

        $pagedata->response = new \stdClass();
        $pagedata->response = $response;


        $this->view->pagedata = $pagedata->response;
    }


    public function categoryAction(Request $request)
    {

        $pagedata = new \stdClass();
        $pagedata->name="Listing category Page";
        $this->view->pagedata = $pagedata;
    }



}


?>
