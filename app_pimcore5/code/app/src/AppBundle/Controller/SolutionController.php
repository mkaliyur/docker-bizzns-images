<?php
namespace AppBundle\Controller;

use Website\Controller\Action;
use Pimcore\Controller\FrontendController;
use Pimcore\Model\Asset;
use Pimcore\Model\Document;
use Pimcore\Model\DataObject;
use Pimcore\Model;
use Pimcore\Tool;
use Pimcore\Model\Webservice;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Pimcore\Model\DataObject\BzPublishQueue\Listing;
use Pimcore\Model\DataObject\BzDomain;
use Pimcore\Model\DataObject\BzSolutions;
use Pimcore\Model\Object\Folder;


class SolutionController extends FrontendController
    {

    public function indexAction() {

        parent::indexAction();
    }

    private  function createSlug($str, $delimiter = '-'){
        $slug = strtolower(trim(preg_replace('/[\s-]+/', $delimiter, preg_replace('/[^A-Za-z0-9-]+/', $delimiter, preg_replace('/[&]/', 'and', preg_replace('/[\']/', '', iconv('UTF-8', 'ASCII//TRANSLIT', $str))))), $delimiter));
        return $slug;
    }

    public function solutionCreateAction(Request $request)
    {
      $domain_name = $request->get('domainname');
      $domain_name_exists = DataObject::getByPath("/organization/Domains/".$domain_name);
      if(is_null($domain_name_exists)){
        $response = new \stdClass();
        $response->status = "Error";
        $response->message = "This Domain ".$domain_name ." doesn't exist.";
        return new response(json_encode($response));
      }

      $market_name = $request->get('marketname');
      $market_name_exists = DataObject::getByPath("/organization/MarketArea/".$market_name);
      if(is_null($market_name_exists)){
        $response = new \stdClass();
        $response->status = "Error";
        $response->message = "This MarketArea ".$market_name ." doesn't exist.";
        return new response(json_encode($response));
      }

      $solution_key = \Pimcore\File::getValidFilename($market_name.'_'.$domain_name);
      $solution_key_exists = Folder::getByPath("/solutions/folder_".$solution_key);
      if(!is_null($solution_key_exists)){
        $response = new \stdClass();
        $response->status = "Error";
        $response->message = "This solution ".$solution_key ." already exists";
        return new response(json_encode($response));
      }

      $bz_solutions = new DataObject\BzSolutions();
      $bz_solutions->setKey($solution_key);
      $parent_folder = DataObject::getByPath("/organization/Solutions");
      $parent_folder_id = $parent_folder->o_id;
      $bz_solutions->setParentId($parent_folder_id);
      $bz_solutions->setName($solution_key.'_solution');
      $domainHref_path = DataObject::getByPath("/organization/Domains/".$domain_name);
      $bz_solutions->setDomain_href($domainHref_path);
      $marketHref_path = DataObject::getByPath("/organization/MarketArea/".$market_name);
      $bz_solutions->setMarket_href($marketHref_path);
      $bz_solutions->save();

      $solutionList = new DataObject\BzSolutions\Listing();
      //$solutionList->setCondition('name = ?', 'merchant_admin');
      $solutionListLoad = $solutionList->load();

      $solutionFolder = new Folder();
      $solutionFolder->setKey($solution_key);
      $parent_folder = DataObject::getByPath("/solution2");
      $parent_folder_id = $parent_folder->o_id;
      $solutionFolder->setParentId($parent_folder_id);
      $solutionFolder->save();

      $solutionParent = new DataObject\BzParent();
      $solutionParent->setKey("parent_".$solution_key);

      $marketHref_Object = DataObject::getByPath("/organization/MarketArea/".$market_name);
      $solutionParent->setMarket_area_href($marketHref_Object);

      $domainHref_Object = DataObject::getByPath("/organization/Domains/".$domain_name);
      $solutionParent->setDomain_href($domainHref_Object);

      $masterListing_object = $domainHref_Object->getMaster_listing();
      $solutionParent->setListing_inherits_from($masterListing_object);

      $masterDeal_object = $domainHref_Object->getMaster_deal();
      $solutionParent->setDeal_inherits_from($masterDeal_object);

      $masterEvent_object = $domainHref_Object->getMaster_event();
      $solutionParent->setEvent_inherits_from($masterEvent_object);

      $masterVideo_object = $domainHref_Object->getMaster_audio();
      $solutionParent->setAudio_inherits_from($masterVideo_object);

      $masterVideo_object = $domainHref_Object->getMaster_video();
      $solutionParent->setVideo_inherits_from($masterVideo_object);

      $masterArticle_object = $domainHref_Object->getMaster_article();
      $solutionParent->setArticle_inherits_from($masterArticle_object);

      $masterNews_object = $domainHref_Object->getMaster_news();
      $solutionParent->setNews_inherits_from($masterNews_object);

      $parent_folder_id2 = $solutionFolder->o_id;
      $solutionParent->setParentId($parent_folder_id2);
      $solutionParent->save();

      $merchantFolder = new Folder();
      $merchantFolder->setKey("Merchants");
      $parent_folder_merchant = $solutionParent->o_id;
      $merchantFolder->setParentId($parent_folder_merchant);
      $merchantFolder->save();

      $response = new \stdClass();
      $response->status = "Success";
      $response->message = "Solution ".$solution_key. " got created.";
      return new response(json_encode($response));

    }

    public function solutionAdminPageAction(Request $request){
    }

    public function newSolutionCreateAction(Request $request){
      $is_secure = SecurityController::isSecure($request);
      if(!$is_secure){
        $response = new \stdClass();
        $response->message = "This request cannot be processed. Security test failed";
        return new response(json_encode($response));
      }

      $is_valid = SecurityController::isValid($request, 'POST');
      if(!($is_valid)){
        $response = new \stdClass();
        $response->message = "This request cannot be processed. Method should be POST.";
        return new response (json_encode($response));
      }

      $solution_name_array = $request->request->get("solution-name");
    //  $results2 = print_r($solution_name_array, true);
    //  \Pimcore\Log\Simple::log("event.log", 'Inside solu cont solution_name_array= '. $results2);

      $solution_name_domain = $solution_name_array['domain'];
      $domain_name_exists = DataObject::getByPath("/organization/Domains/".$solution_name_domain);
      if(is_null($domain_name_exists)){
        $response = new \stdClass();
        $response->status = "Error";
        $response->message = "This Domain ".$solution_name_domain ." doesn't exist.";
        return new response(json_encode($response));
      }

      $solution_name_market = $solution_name_array['market'];
      $market_name_exists = DataObject::getByPath("/organization/MarketArea/".$solution_name_market);
      if(is_null($market_name_exists)){
        $response = new \stdClass();
        $response->status = "Error";
        $response->message = "This MarketArea ".$solution_name_market ." doesn't exist.";
        return new response(json_encode($response));
      }

      $solution_key = \Pimcore\File::getValidFilename($solution_name_market.'_'.$solution_name_domain);
      $solution_key_exists = Folder::getByPath("//solution2//".$solution_key);
      if(!is_null($solution_key_exists)){
        $response = new \stdClass();
        $response->status = "Error";
        $response->message = "This solution ".$solution_key ." already exists";
        return new response(json_encode($response));
      }

      $bz_solutions = new DataObject\BzSolutions();
      $bz_solutions->setKey($solution_key);
      $parent_folder = DataObject::getByPath("/organization/Solutions");
      $parent_folder_id = $parent_folder->o_id;
      $bz_solutions->setParentId($parent_folder_id);
      $bz_solutions->setName($solution_key.'_solution');
      $domainHref_path = DataObject::getByPath("/organization/Domains/".$solution_name_domain);
      $bz_solutions->setDomain_href($domainHref_path);
      $marketHref_path = DataObject::getByPath("/organization/MarketArea/".$solution_name_market);
      $bz_solutions->setMarket_href($marketHref_path);
      $bz_solutions->save();

      $solutionList = new DataObject\BzSolutions\Listing();
      //$solutionList->setCondition('name = ?', 'merchant_admin');
      $solutionListLoad = $solutionList->load();

      $solutionFolder = new Folder();
      $solutionFolder->setKey($solution_key);
      $parent_folder = DataObject::getByPath("/solution2");
      $parent_folder_id = $parent_folder->o_id;
      $solutionFolder->setParentId($parent_folder_id);
      $solutionFolder->save();

      $solutionParent = new DataObject\BzParent();
      $solutionParent->setKey("parent_".$solution_key);

      $marketHref_Object = DataObject::getByPath("/organization/MarketArea/".$solution_name_market);
      $solutionParent->setMarket_area_href($marketHref_Object);

      $domainHref_Object = DataObject::getByPath("/organization/Domains/".$solution_name_domain);
      $solutionParent->setDomain_href($domainHref_Object);

      $masterListing_object = $domainHref_Object->getMaster_listing();
      $solutionParent->setListing_inherits_from($masterListing_object);

      $masterDeal_object = $domainHref_Object->getMaster_deal();
      $solutionParent->setDeal_inherits_from($masterDeal_object);

      $masterEvent_object = $domainHref_Object->getMaster_event();
      $solutionParent->setEvent_inherits_from($masterEvent_object);

      $masterVideo_object = $domainHref_Object->getMaster_audio();
      $solutionParent->setAudio_inherits_from($masterVideo_object);

      $masterVideo_object = $domainHref_Object->getMaster_video();
      $solutionParent->setVideo_inherits_from($masterVideo_object);

      $masterArticle_object = $domainHref_Object->getMaster_article();
      $solutionParent->setArticle_inherits_from($masterArticle_object);

      $masterNews_object = $domainHref_Object->getMaster_news();
      $solutionParent->setNews_inherits_from($masterNews_object);

      $parent_folder_id2 = $solutionFolder->o_id;
      $solutionParent->setParentId($parent_folder_id2);
      $solutionParent->save();

      $merchantFolder = new Folder();
      $merchantFolder->setKey("Merchants");
      $parent_folder_merchant = $solutionParent->o_id;
      $merchantFolder->setParentId($parent_folder_merchant);
      $merchantFolder->save();

      $this->disableViewAutoRender();

      $response = new \stdClass();
      $response->status = "Success";
      $response->message = "Solution ".$solution_key. " got created.";
      return new response(json_encode($response));
    }

}
?>
