<?php
namespace AppBundle\Controller;

use Website\Controller\Action;
use Pimcore\Controller\FrontendController;
use Pimcore\Model\Asset;
use Pimcore\Model\Document;
use Pimcore\Model;
use Pimcore\Tool;
use Pimcore\Model\Webservice;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Pimcore\Model\DataObject\BzPublishQueue\Listing;
use Pimcore\Model\DataObject\BzDomain;
use Pimcore\Model\Object\Folder as DataObjectFolder;
use Pimcore\Model\DataObject\BzBlogArticle;
use Pimcore\Model\User\UserRole\Folder as UserFolder;
use Pimcore\Model\Asset\Folder as AssetFolder;

use Pimcore\Logger;
use Pimcore\Model\DataObject;
use Pimcore\Model\Element;
use Pimcore\Model\User;
use Symfony\Component\HttpFoundation\BinaryFileResponse;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Session\Attribute\AttributeBagInterface;
use Symfony\Component\HttpKernel\Event\FilterControllerEvent;
use Symfony\Component\HttpKernel\Event\FilterResponseEvent;
use Symfony\Component\Routing\Annotation\Route;

class MerchantController extends FrontendController
    {

    public function indexAction() {
        parent::indexAction();
    }

    private function createAssetFolderByPath(string $folderName,string $parentFolderPath){
      $assetFolder = new AssetFolder();
      $assetFolder->setType('folder');
      $assetFolder->setFilename($folderName);
      $asset_parent_folder_path = $parentFolderPath;
      $asset_parent_folder = Asset::getByPath($asset_parent_folder_path);
      $asset_parent_folder_id = $asset_parent_folder->id;
      $assetFolder->setParentId($asset_parent_folder_id);
      return $ret = $assetFolder->save();
    }

    private function createAssetFolderByParentId(string $folderName,int $parentFolderid){
      $assetFolder = new AssetFolder();
      $assetFolder->setType('folder');
      $assetFolder->setFilename($folderName);
      $asset_parent_folder_id = $parentFolderid;
      $assetFolder->setParentId($asset_parent_folder_id);
      return $ret = $assetFolder->save();
    }

    private function createDataObjectFolder (string $folderName, int $parentId){
      $newFolder = new DataObjectFolder();
      $newFolder->setKey($folderName);
      $newFolder_id = $parentId;
      $newFolder->setParentId($newFolder_id);
      return $newFolder->save();
    }

    private function createObjectInstance (string $className,string $name, int $parentId){
      $ret = 'default';
      $klass = DataObject\ClassDefinition::getByName($className);
      $className = 'Pimcore\\Model\\DataObject\\' . ucfirst($className);
      $parent = DataObject::getById($parentId);

           $intendedPath = $parent->getRealFullPath() . '/' .$name;

           if (!DataObject\Service::pathExists($intendedPath)) {
               $object = $this->get('pimcore.model.factory')->build($className);
               if ($object instanceof DataObject\Concrete) {
                   $object->setOmitMandatoryCheck(true); // allow to save the object although there are mandatory fields
               }
               $object->setClassId($klass->id);
               $object->setParentId($parentId);
               $object->setKey($name);
               $object->setPublished(true);
               $object->setClassName($klass->name);
               $object->setCreationDate(time());
               //$object->setUserOwner($this->getAdminUser()->getId());
               //$object->setUserModification($this->getAdminUser()->getId());

               try {
                   $ret = $object->save();
               } catch (\Exception $e) {
                    //\Pimcore\Log\Simple::log("event.log", 'Inside createObjectInstance else exception= '.$e->getMessage());
               }
           } else {
               $message = 'prevented creating object because object with same path+key already exists';
               //\Pimcore\Log\Simple::log("event.log", 'Inside createObjectInstance else1 message= '.$message);
           }

       return $ret;
    }


    public function merchantCreateAction(Request $request)
    {
      $solution_name = $request->get('solution_name');
      $solution_name_exists = DataObject::getByPath("/solution2//".$solution_name);
      if(is_null($solution_name_exists)){
        $response = new \stdClass();
        $response->status = "Error";
        $response->message = "This Solution ".$solution_name ." doesn't exist.";
        return new Response(json_encode($response));
      }

      $merchant_name = $request->get('merchant_name');
      $merchant_name_path = "/solution2//".$solution_name."/parent_".$solution_name."/Merchants//".$merchant_name;
      $merchant_name_exists = DataObject::getByPath($merchant_name_path);
      if(!is_null($merchant_name_exists)){
        $response = new \stdClass();
        $response->status = "Error";
        $response->message = "This Merchant ".$merchant_name ." already exists.";
        return new Response(json_encode($response));
      }

      $asset_parent_folder_path = "//solutions//".$solution_name."//Merchants//";
      $ret1 = $this->createAssetFolderByPath($merchant_name,$asset_parent_folder_path);
      $ret = $this->createAssetFolderByParentId('Videos',$ret1->id);
      $ret = $this->createAssetFolderByParentId('Images',$ret1->id);
      $ret = $this->createAssetFolderByParentId('PDF',$ret1->id);
      $ret = $this->createAssetFolderByParentId('Other Docs',$ret1->id);

      $merchantParent = new DataObject\BzParent();
      $merchantParent->setKey($merchant_name);
      $merchant_parent_folder_path = "/solution2//".$solution_name."/parent_".$solution_name."/Merchants//";
      $merchant_parent_folder = DataObject::getByPath($merchant_parent_folder_path);
      $results2 = print_r($merchant_parent_folder, true);
      \Pimcore\Log\Simple::log("event.log", 'Inside merchant controller merchant_parent_folder= '.$results2);

      $merchant_parent_folder_id = $merchant_parent_folder->o_id;
      $merchantParent->setParentId($merchant_parent_folder_id);
      $savedMerchantParent = $merchantParent->save();

      $savedMerchantFolder = $this->createDataObjectFolder($merchant_name,$savedMerchantParent->o_id);
      $savedArticleFolder = $this->createDataObjectFolder('Articles',$savedMerchantFolder->o_id);
      $this->createObjectInstance('BzBlogArticle','Sample Article',$savedArticleFolder->o_id);

      $savedDealsFolder = $this->createDataObjectFolder('Deals',$savedMerchantFolder->o_id);
      $this->createObjectInstance('BzDeals','Sample Deals',$savedDealsFolder->o_id);

      $savedEventsFolder = $this->createDataObjectFolder('Events',$savedMerchantFolder->o_id);
      $this->createObjectInstance('BzEvents','Sample Events',$savedEventsFolder->o_id);

      $savedVideosFolder = $this->createDataObjectFolder('Videos',$savedMerchantFolder->o_id);
      $this->createObjectInstance('BzVideo','Sample Video',$savedVideosFolder->o_id);

      $savedListingsFolder = $this->createDataObjectFolder('Listings',$savedMerchantFolder->o_id);
      $this->createObjectInstance('BzListing2','Sample Listing',$savedListingsFolder->o_id);

      $savedAudioFolder = $this->createDataObjectFolder('Audios',$savedMerchantFolder->o_id);
      $this->createObjectInstance('BzAudio','Sample Audio',$savedAudioFolder->o_id);

      $type_user = 'user';
      $className = User\Service::getClassNameForType($type_user);
      //$results2 = print_r($className, true);
      //\Pimcore\Log\Simple::log("event.log", 'Inside merchant controller className = '.$results2);

      $parentId = 0;
      $name = $merchant_name.'_admin';
      $active = 1;

        $password = Tool\Authentication::getPasswordHash($name, 'Admin1234$');

          $user = $className::create([
            'parentId' => $parentId,
            'name' => $name,
            'password' => $password,
            'active' => $active
          ]);
          //$results2 = print_r($user, true);
          //\Pimcore\Log\Simple::log("event.log", 'Inside merchant cont user = '.$results2);

          // $user = new User();
          // $user->setFirstname($merchant_name);
          // $user->setLastname("admin");
          // $user->setUsername("testuser");
          // $user->setRoles(['merchant_admin']);
          // $user->setPassword('password123');

          //$userFolder = Folder1::getById(9);
          $userFolder = UserFolder::getByName("All Users/Merchants");
          //$results2 = print_r($userFolder, true);
          //\Pimcore\Log\Simple::log("event.log", 'Inside merchant controller userFolder = '.$results2);

          $list = new User\Role\Listing();
          $list->setCondition('name = ?', 'merchant_admin');
          $list->load();

          $roleList = $list->getRoles();
          //$results2 = print_r($roleList, true);
          //\Pimcore\Log\Simple::log("event.log", 'Inside merchant controller roleList = '.$results2);

          /** @var $role User\Role */
          $roles = [];
          foreach ($roleList as $role) {
              $roles[] = $role->getId();
          }
          $user->setRoles($roles);
          $user->setPermission('assets', true);
          $user->setPermission('objects', true);
          $user->setPermission('documents', false);

          $merchant_asset_path = "/solutions//".$solution_name."/Merchants//".$merchant_name;

          $workspaces_new = new \stdClass();
          $asset_array = $workspaces_new->asset = array();
          //$asset_array_spaces = $asset_array->spaces = [];
          array_push($asset_array, []);
          $asset_array_object = new \stdClass();
          $asset_array_object->path = $merchant_asset_path;
          $asset_array_object->list = false;
          $asset_array_object->view = true;
          $asset_array_object->save = true;
          $asset_array_object->publish = true;
          $asset_array_object->delete = true;
          $asset_array_object->rename = true;
          $asset_array_object->create = true;
          $asset_array_object->settings = false;
          $asset_array_object->versions = false;
          $asset_array_object->properties = false;
          $asset_array_object->lEdit = false;
          $asset_array_object->lView = false;
          $asset_array_object->layouts = false;
          //$asset_array_object->id =

          $objcopy = unserialize(serialize($asset_array_object));
          $array_spaces = $asset_array[0];
          array_push($array_spaces,$objcopy);
          $asset_array[0] = $array_spaces;
          $workspaces_new->asset = $asset_array;

          $workspaces_new->object = [];
          $object_array = $workspaces_new->object = array();
          //$asset_array_spaces = $asset_array->spaces = [];
          array_push($object_array, []);
          $object_array_obj = new \stdClass();
          $object_array_obj->path = $merchant_name_path;
          $object_array_obj->list = false;
          $object_array_obj->view = true;
          $object_array_obj->save = true;
          $object_array_obj->unpublish = true;
          $object_array_obj->publish = true;
          $object_array_obj->delete = true;
          $object_array_obj->rename = true;
          $object_array_obj->create = true;
          $object_array_obj->settings = false;
          $object_array_obj->versions = false;
          $object_array_obj->properties = false;
          $object_array_obj->lEdit = false;
          $object_array_obj->lView = false;
          $object_array_obj->layouts = false;

          $objcopy = unserialize(serialize($object_array_obj));
          $array_spaces = $object_array[0];
          array_push($array_spaces,$objcopy);
          $object_array[0] = $array_spaces;
          $workspaces_new->object = $object_array;

          $workspaces_new->document = [];

          //$results2 = print_r($object_array_obj, true);
          //\Pimcore\Log\Simple::log("event.log", 'Inside merchant controller object_array_obj = '.$results2);

          //$results2 = print_r($workspaces_new, true);
          //\Pimcore\Log\Simple::log("event.log", 'Inside merchant controller workspaces_new = '.$results2);


          $workspaces = $workspaces_new;
          foreach ($workspaces as $type => $spaces) {
            $newWorkspaces = [];
            //$results2 = print_r($type, true);
            //\Pimcore\Log\Simple::log("event.log", 'Inside merchant controller type = '.$results2);

              foreach ($spaces as $space) {
                  $element = Element\Service::getElementByPath($type, $space['path']);
                  //$results2 = print_r($element, true);
                  //\Pimcore\Log\Simple::log("event.log", 'Inside merchant controller element = '.$results2);

                  if ($element) {
                      $className = '\\Pimcore\\Model\\User\\Workspace\\' . Element\Service::getBaseClassNameForElement($type);
                      //$results2 = print_r($className, true);
                      //\Pimcore\Log\Simple::log("event.log", 'Inside merchant controller Inside If of the element className= '.$results2);

                      $workspace = new $className();
                      $workspace->setValues($space);
                      $workspace->setCid($element->getId());
                      $workspace->setCpath($element->getRealFullPath());
                      $workspace->setUserId($user->getId());

                      $newWorkspaces[] = $workspace;
                  }
              }

              $user->{'setWorkspaces' . ucfirst($type)}($newWorkspaces);
          }

          $user->save();

          $response = new \stdClass();
          $response->status = "Success";
          $response->message = "Solution Parent".$solution_name. " got created.";
          return new Response(json_encode($response));
    }

    public function merchantAdminPageAction (Request $request){

    }

    public function newMerchantCreateAction(Request $request)
    {
      $this->disableViewAutoRender();
      $is_secure = SecurityController::isSecure($request);
      if(!$is_secure){
        $response = new \stdClass();
        $response->message = "This request cannot be processed. Security test failed";
        return new Response(json_encode($response));
      }

      $is_valid = SecurityController::isValid($request, 'POST');
      if(!($is_valid)){
        $response = new \stdClass();
        $response->message = "This request cannot be processed. Method should be POST.";
        return new response (json_encode($response));
      }

      $merchant_array = $request->request->get("merchant");
      //$results2 = print_r($merchant_array, true);
      //\Pimcore\Log\Simple::log("event.log", 'Inside merchant controller merchant_array= '. $results2);

      if(is_null($merchant_array)){
        $response = new \stdClass();
        $response->status = "Error";
        $response->message = "This Merchant Cannot be created. No parameter received.";
        return new Response(json_encode($response));
      }

      $solution_name = $merchant_array['solution-name'];
      //$results2 = print_r($solution_name, true);
      //\Pimcore\Log\Simple::log("event.log", 'Inside merchant controller solution_name= '. $results2);

      if(is_null($solution_name)){
        $response = new \stdClass();
        $response->status = "Error";
        $response->message = "This Merchant Cannot be created. Solution name is required.";
        return new Response(json_encode($response));
      }
      // $is_empty = SecurityController::isEmpty($request, $solution_name);
      // if(!($is_empty)){
      //   $response = new \stdClass();
      //   $response->message = "This Merchant Cannot be created. Solution name length is zero.";
      //   return new response (json_encode($response));
      // }


      $merchant_name = $merchant_array['name'];
      //$results2 = print_r($merchant_name, true);
      //\Pimcore\Log\Simple::log("event.log", 'Inside merchant controller merchant_name= '. $results2);

      if(is_null($merchant_name)){
        $response = new \stdClass();
        $response->status = "Error";
        $response->message = "This Merchant Cannot be created. Merchant name is required.";
        return new Response(json_encode($response));
      }
      // $is_empty = SecurityController::isEmpty($request, $merchant_name);
      // if(!($is_empty)){
      //   $response = new \stdClass();
      //   $response->message = "This Merchant Cannot be created.hfhfg Merchant name length is zero.";
      //   return new response (json_encode($response));
      // }

      $solution_name_exists = DataObject::getByPath("/solution2//".$solution_name);
      //$results2 = print_r($solution_name_exists, true);
      //\Pimcore\Log\Simple::log("event.log", 'Inside merchant controller solution_name_exists= '. $results2);

      if(is_null($solution_name_exists)){
        $response = new \stdClass();
        $response->status = "Error";
        $response->message = "This Solution ".$solution_name ." doesn't exist.";
        return new Response(json_encode($response));
      }

      $merchant_name_path = "/solution2//".$solution_name."/parent_".$solution_name."/Merchants//".$merchant_name;
      //$results2 = print_r($merchant_name_path, true);
      //\Pimcore\Log\Simple::log("event.log", 'Inside merchant controller merchant_name_path= '. $results2);

      $merchant_name_exists = DataObject::getByPath($merchant_name_path);
      //$results2 = print_r($merchant_name_path, true);
      //\Pimcore\Log\Simple::log("event.log", 'Inside merchant controller merchant_name_path= '. $results2);

      if(!is_null($merchant_name_exists)){
        $response = new \stdClass();
        $response->status = "Error";
        $response->message = "This Merchant ".$merchant_name ." already exists.";
        return new Response(json_encode($response));
      }

      set_time_limit (90);
      $asset_parent_folder_path = "//solutions//".$solution_name."//Merchants//";
      $ret1 = $this->createAssetFolderByPath($merchant_name,$asset_parent_folder_path);
      $ret = $this->createAssetFolderByParentId('Videos',$ret1->id);
      $ret = $this->createAssetFolderByParentId('Images',$ret1->id);
      $ret = $this->createAssetFolderByParentId('PDF',$ret1->id);
      $ret = $this->createAssetFolderByParentId('Other Docs',$ret1->id);

      $merchantParent = new DataObject\BzParent();

      $merchantParent->setKey($merchant_name);
      $merchant_parent_folder_path = "//solution2//".$solution_name."/parent_".$solution_name."//Merchants//";
      \Pimcore\Log\Simple::log("event.log", 'Inside merchant controller after merchant_parent_folder_path= '. $results2);

      $merchant_parent_folder = DataObject::getByPath($merchant_parent_folder_path);
      //$results2 = print_r($merchant_parent_folder, true);
      //\Pimcore\Log\Simple::log("event.log", 'Inside merchant controller merchant_parent_folder= '. $results2);

      $merchant_parent_folder_id = $merchant_parent_folder->o_id;
      $merchantParent->setParentId($merchant_parent_folder_id);
      $savedMerchantParent = $merchantParent->save();
      //\Pimcore\Log\Simple::log("event.log", 'Inside merchant controller savedMerchantParent= ');

      $savedMerchantFolder = $this->createDataObjectFolder($merchant_name,$savedMerchantParent->o_id);
      $savedArticleFolder = $this->createDataObjectFolder('Articles',$savedMerchantFolder->o_id);
      $this->createObjectInstance('BzBlogArticle','Sample Article',$savedArticleFolder->o_id);
      //\Pimcore\Log\Simple::log("event.log", 'Inside merchant controller after BzBlogArticle ');

      $savedDealsFolder = $this->createDataObjectFolder('Deals',$savedMerchantFolder->o_id);
      $this->createObjectInstance('BzDeals','Sample Deals',$savedDealsFolder->o_id);

      //\Pimcore\Log\Simple::log("event.log", 'Inside merchant controller after BzDeals ');

      $savedEventsFolder = $this->createDataObjectFolder('Events',$savedMerchantFolder->o_id);
      $this->createObjectInstance('BzEvents','Sample Events',$savedEventsFolder->o_id);
      //\Pimcore\Log\Simple::log("event.log", 'Inside merchant controller after BzEvents ');

      $savedVideosFolder = $this->createDataObjectFolder('Videos',$savedMerchantFolder->o_id);
      $this->createObjectInstance('BzVideo','Sample Video',$savedVideosFolder->o_id);
      //\Pimcore\Log\Simple::log("event.log", 'Inside merchant controller after bzvideo ');

      $savedListingsFolder = $this->createDataObjectFolder('Listings',$savedMerchantFolder->o_id);
      $this->createObjectInstance('BzListing2','Sample Listing',$savedListingsFolder->o_id);
      //\Pimcore\Log\Simple::log("event.log", 'Inside merchant controller after BzListing2 ');

      $savedAudioFolder = $this->createDataObjectFolder('Audios',$savedMerchantFolder->o_id);
      $this->createObjectInstance('BzAudio','Sample Audio',$savedAudioFolder->o_id);

      //\Pimcore\Log\Simple::log("event.log", 'Inside merchant controller before typeuser= ');

      $type_user = 'user';
      $className = User\Service::getClassNameForType($type_user);
      //$results2 = print_r($className, true);
      //\Pimcore\Log\Simple::log("event.log", 'Inside merchant controller className = '.$results2);

      $parentId = 0;
      $name = $merchant_name.'_admin';
      $active = 1;

        $password = Tool\Authentication::getPasswordHash($name, 'Admin1234$');

          $user = $className::create([
            'parentId' => $parentId,
            'name' => $name,
            'password' => $password,
            'active' => $active
          ]);
          //$results2 = print_r($user, true);
          //\Pimcore\Log\Simple::log("event.log", 'Inside merchant cont user = '.$results2);

          // $user = new User();
          // $user->setFirstname($merchant_name);
          // $user->setLastname("admin");
          // $user->setUsername("testuser");
          // $user->setRoles(['merchant_admin']);
          // $user->setPassword('password123');

          //$userFolder = Folder1::getById(9);
          $userFolder = UserFolder::getByName("All Users/Merchants");
          //$results2 = print_r($userFolder, true);
          //\Pimcore\Log\Simple::log("event.log", 'Inside merchant controller userFolder = '.$results2);

          $list = new User\Role\Listing();
          $list->setCondition('name = ?', 'merchant_admin');
          $list->load();

          $roleList = $list->getRoles();
          //$results2 = print_r($roleList, true);
          //\Pimcore\Log\Simple::log("event.log", 'Inside merchant controller roleList = '.$results2);

          /** @var $role User\Role */
          $roles = [];
          foreach ($roleList as $role) {
              $roles[] = $role->getId();
          }
          $user->setRoles($roles);
          $user->setPermission('assets', true);
          $user->setPermission('objects', true);
          $user->setPermission('documents', false);

          $merchant_asset_path = "/solutions//".$solution_name."/Merchants//".$merchant_name;

          $workspaces_new = new \stdClass();
          $asset_array = $workspaces_new->asset = array();
          //$asset_array_spaces = $asset_array->spaces = [];
          array_push($asset_array, []);
          $asset_array_object = new \stdClass();
          $asset_array_object->path = $merchant_asset_path;
          $asset_array_object->list = false;
          $asset_array_object->view = true;
          $asset_array_object->save = true;
          $asset_array_object->publish = true;
          $asset_array_object->delete = true;
          $asset_array_object->rename = true;
          $asset_array_object->create = true;
          $asset_array_object->settings = false;
          $asset_array_object->versions = false;
          $asset_array_object->properties = false;
          $asset_array_object->lEdit = false;
          $asset_array_object->lView = false;
          $asset_array_object->layouts = false;
          //$asset_array_object->id =

          $objcopy = unserialize(serialize($asset_array_object));
          $array_spaces = $asset_array[0];
          array_push($array_spaces,$objcopy);
          $asset_array[0] = $array_spaces;
          $workspaces_new->asset = $asset_array;

          $workspaces_new->object = [];
          $object_array = $workspaces_new->object = array();
          //$asset_array_spaces = $asset_array->spaces = [];
          array_push($object_array, []);
          $object_array_obj = new \stdClass();
          $object_array_obj->path = $merchant_name_path;
          $object_array_obj->list = false;
          $object_array_obj->view = true;
          $object_array_obj->save = true;
          $object_array_obj->unpublish = true;
          $object_array_obj->publish = true;
          $object_array_obj->delete = true;
          $object_array_obj->rename = true;
          $object_array_obj->create = true;
          $object_array_obj->settings = false;
          $object_array_obj->versions = false;
          $object_array_obj->properties = false;
          $object_array_obj->lEdit = false;
          $object_array_obj->lView = false;
          $object_array_obj->layouts = false;

          $objcopy = unserialize(serialize($object_array_obj));
          $array_spaces = $object_array[0];
          array_push($array_spaces,$objcopy);
          $object_array[0] = $array_spaces;
          $workspaces_new->object = $object_array;

          $workspaces_new->document = [];

          //$results2 = print_r($object_array_obj, true);
          //\Pimcore\Log\Simple::log("event.log", 'Inside merchant controller object_array_obj = '.$results2);

          //$results2 = print_r($workspaces_new, true);
          //\Pimcore\Log\Simple::log("event.log", 'Inside merchant controller workspaces_new = '.$results2);


          $workspaces = $workspaces_new;
          foreach ($workspaces as $type => $spaces) {
            $newWorkspaces = [];
            //$results2 = print_r($type, true);
            //\Pimcore\Log\Simple::log("event.log", 'Inside merchant controller type = '.$results2);

              foreach ($spaces as $space) {
                  $element = Element\Service::getElementByPath($type, $space['path']);
                  //$results2 = print_r($element, true);
                  //\Pimcore\Log\Simple::log("event.log", 'Inside merchant controller element = '.$results2);

                  if ($element) {
                      $className = '\\Pimcore\\Model\\User\\Workspace\\' . Element\Service::getBaseClassNameForElement($type);
                      //$results2 = print_r($className, true);
                      //\Pimcore\Log\Simple::log("event.log", 'Inside merchant controller Inside If of the element className= '.$results2);

                      $workspace = new $className();
                      $workspace->setValues($space);
                      $workspace->setCid($element->getId());
                      $workspace->setCpath($element->getRealFullPath());
                      $workspace->setUserId($user->getId());

                      $newWorkspaces[] = $workspace;
                  }
              }

              $user->{'setWorkspaces' . ucfirst($type)}($newWorkspaces);
          }
          //\Pimcore\Log\Simple::log("event.log", 'Inside Merchant controller before user save');

          $user->save();
          //\Pimcore\Log\Simple::log("event.log", 'Inside Merchant controller after user save');


          $response = new \stdClass();
          $response->status = "Success";
          $response->message = "Merchant got created";
          //\Pimcore\Log\Simple::log("event.log", 'Inside Merchant controller about to send reponse'.$response);

          return new Response(json_encode($response));
    }

  }
?>
