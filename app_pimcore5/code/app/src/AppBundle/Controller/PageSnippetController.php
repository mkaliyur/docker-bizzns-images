<?php
namespace AppBundle\Controller;

use Website\Controller\Action;
use Pimcore\Controller\FrontendController;
use Pimcore\Model\Asset;
use Pimcore\Model\Document;
use Pimcore\Model\DataObject;
use Pimcore\Model;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Pimcore\Tool;
use Pimcore\Model\Webservice;


class PageSnippetController extends FrontendController
{



    public function snippetAction(Request $request)
    {
      $this->view->temp_data = "temp";
    }


    public function logoAction(Request $request)
    {
      $paramarray = array();
      if (!isset($pagedata)) {
        $pagedata = new \stdClass();
      }
      $paramarray['page_data'] = $pagedata;
      //return $this->render(":PageSnippet:logo.html.php", ["page_data" => $pagedata]);
      $this->view->page_data=$pagedata;
    }


    public function menuObjectAction(Request $request){
      $this->view->temp_data = "temp";
    }


    public function topmenuAction(Request $request)
    {
      $this->view->temp_data = "temp";
    }


    public function languageAction(Request $request)
    {
      $this->view->temp_data = "temp";
    }
    public function language1Action(Request $request)
    {
      $this->view->temp_data = "temp";
    }

    public function carouselAction(Request $request){
        $this->view->temp_data = "temp";
    }

    public function headerAction(Request $request){
        // $this->extend();
      //  $pagedata = $request->get('page_data');
        $this->view->temp_data = "temp";
    }
    public function header1Action(Request $request){
        // $this->extend();
      //  $pagedata = $request->get('page_data');
        $this->view->temp_data = "temp";

    }

    public function footerAction(Request $request){
        $this->view->temp_data = "temp";
    }

    public function copyrightAction(Request $request){
      $this->view->temp_data = "temp";
    }

    public function leftnavAction(Request $request){
      $this->view->temp_data = "temp";
    }

    public function rightnavAction(Request $request){
      $this->view->temp_data = "temp";
    }

    public function contentBlockAction(Request $request){
      $this->view->temp_data = "temp";
    }

    public function externalVideoAction(Request $request){
      $this->view->temp_data = "temp";
    }
    public function bzCategoriesAction(Request $request){
      $this->view->temp_data = "temp";
    }

    public function bzTagsAction(Request $request){
      $this->view->temp_data = "temp";
    }

    public function blogCategoriesAction(Request $request){
      $this->view->temp_data = "temp";
    }

    public function blogTagsAction(Request $request){
      $this->view->temp_data = "temp";
    }

    public function featuredBlogsAction(Request $request){
      $this->view->temp_data = "temp";
    }

    public function promotedBlogsAction(Request $request){
      $this->view->temp_data = "temp";
    }

    public function latestBlogsAction(Request $request){
      $this->view->temp_data = "temp";
    }

    public function topNewsAction(Request $request){
      $this->view->temp_data = "temp";
    }

    public function latestNewsAction(Request $request){
      $this->view->temp_data = "temp";
    }

    public function featuredListingsAction(Request $request){
      $this->view->temp_data = "temp";
    }

    public function promotedListingsAction(Request $request){
        $this->view->temp_data = "temp";
    }

    public function homepageLogoListAction(Request $request){
      $this->view->temp_data = "temp";
    }

    public function masterComponentAction(Request $request){
      $this->view->temp_data = "temp";
    }

    public function frontendListAction(Request $request){
      $this->view->temp_data = "temp";
    }

    public function frontendSingleAction(Request $request){
      $this->view->temp_data = "temp";
    }

    public function pageareaAction(Request $request){
      $this->view->temp_data = "temp";
    }

    public function contactInfoAction(Request $request){
      $this->view->temp_data = "temp";
    }


}

?>
