<?php
namespace AppBundle\Controller;

use Website\Controller\Action;
use Pimcore\Controller\FrontendController;
use Pimcore\Model\Asset;
use Pimcore\Model\Document;
use Pimcore\Model\DataObject;
use Pimcore\Model;
use Pimcore\Tool;
use Pimcore\Model\Webservice;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Pimcore\Model\DataObject\BzPublishQueue\Listing;


class SecurityController extends FrontendController
    {

      public static function isSecure(Request $request){
        return true;
      }

      public static function isValid(Request $request, string $requestType){
        $methodCheck = $request->getMethod();
        if($methodCheck == $requestType){
          return true;
        }else{
          return false;
        }
      }

      public static function isEmpty(Request $request, string $lengthCheck){
        if( (!is_string($lengthCheck) || intval(strlen($lengthCheck))==0)){
          return true;
        }else{
          return false;
        }
      }
    }
?>
