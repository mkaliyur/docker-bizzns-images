<?php
namespace AppBundle\Controller;

use Website\Controller\Action;
use Pimcore\Controller\FrontendController;
use Pimcore\Model\Asset;
use Pimcore\Model\Document;
use Pimcore\Model\DataObject;
use Pimcore\Model;
use Pimcore\Tool;
use Pimcore\Model\Webservice;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Pimcore\Model\DataObject\BzPublishQueue\Listing;


class QueueController extends FrontendController
    {

    public function indexAction() {
        parent::indexAction();
    }

    private  function createSlug($str, $delimiter = '-'){
        $slug = strtolower(trim(preg_replace('/[\s-]+/', $delimiter, preg_replace('/[^A-Za-z0-9-]+/', $delimiter, preg_replace('/[&]/', 'and', preg_replace('/[\']/', '', iconv('UTF-8', 'ASCII//TRANSLIT', $str))))), $delimiter));
        return $slug;
    }

    public function updateAction(Request $request)
    {
      $qentry_id = $request->query->get('id');
      if(is_null($qentry_id)){
        $response = new \stdClass();
        $response->status = "error";
        $response->message = "Update requires id of qentry as query parameter. Ex: ?id=123";
      }else{
        $qobject = DataObject\BzPublishQueue::getById($qentry_id);
        //$results2 = print_r($qobject, true);
        //\Pimcore\Log\Simple::log("event.log", 'Inside view of queue qobject= '. $results2);

        if(is_null($qobject)){
          $response = new \stdClass();
          $response->status = "error";
          $response->message = "BzPublishQueue entry with id= ".$qentry_id .' was not found' ;

        }else{
          $qobject->setIs_processed(true);
          $qobject->save();
          $response = new \stdClass();
          $response->status = "success";
          $response->message = "BzPublishQueue entry with id= ".$qentry_id .' was updated to is_processed = true' ;
        }
      }
      $this->view->response = $response;
    }

    public function queueAction(Request $request)
    {
      \Pimcore\Log\Simple::log("event.log", 'Inside view of queue::queueAction:: Beginning of queueAction ');

      $key = $this->document->getKey();
      //$results2 = print_r($key, true);
      //\Pimcore\Log\Simple::log("event.log", 'Inside view of queue::queueAction:: key= '. $results2);


      //$request_path_info = $request->getPathInfo();
      //$o_path_array = explode("/publish_queue/", $request_path_info);
      //$o_path_array_size = sizeof($o_path_array);
      $solutionid = $key;
      $pagedata = $request->get("page_data");
      if (! isset($pagedata)) {
          $pagedata = new \stdClass();
      }
      $pagedata->solutionid = $solutionid;
      $pagedata->event_type = $event_type;

      //$results2 = print_r($pagedata, true);
      //\Pimcore\Log\Simple::log("event.log", 'Inside view of queue::queueAction:: Sending pagedata to queue:: pagedata= '. $results2);

      $this->view->page_data = $pagedata;
    }

    public function deleteQueueAction(Request $request){
      //$qdelete_id = $request->query->get('id');
        $dobject_id = $request->query->get('id');
        if(is_null($dobject_id)){
          $response = new \stdClass();
          $response->status = "error";
          $response->message = "Invalid delete id. ID is required. Got null " ;
          return new response(json_encode($response));
        }

        $dobject = DataObject\BzPublishQueue::getById($dobject_id);
        if (is_null($dobject)){
          $response = new \stdClass();
          $response->status = "error";
          $response->message = "No object found for id= ".$dobject_id  ;
          return new response(json_encode($response));
        }

        $publish_status = $dobject->getIs_processed();
        if ($publish_status == 1){
          $dobject->delete();
          $response = new \stdClass();
          $response->status = "Success";
          $response->message = "Object for classname BzPublishQueue with id= ".$dobject_id." deleted successfully"  ;
          return new response(json_encode($response));
        }
      $this->view->response = $response;
    }


}
?>
