<?php
namespace AppBundle\Controller;

use Website\Controller\Action;
use Pimcore\Controller\FrontendController;
use Pimcore\Model\Asset;
use Pimcore\Model\Document;
use Pimcore\Model\DataObject;
use Pimcore\Model;
use Pimcore\Tool;
use Pimcore\Model\Webservice;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;


class Bzpage1Controller extends FrontendController {

    public function indexAction() {
        parent::indexAction();
    }

    private function createSlug($str, $delimiter = '-'){
        $slug = strtolower(trim(preg_replace('/[\s-]+/', $delimiter, preg_replace('/[^A-Za-z0-9-]+/', $delimiter, preg_replace('/[&]/', 'and', preg_replace('/[\']/', '', iconv('UTF-8', 'ASCII//TRANSLIT', $str))))), $delimiter));
        return $slug;
    }

    public function pageAction(Request $request)
    {
        $pagedata = $request->get("page_data");
        if (! isset($pagedata)) {
            $pagedata =   new \stdClass();
        }
        $this->view->page_data = $pagedata;
    }

    public function blogSingleAction(Request $request)
    {
        $pagedata = $request->get("page_data");
        if (! isset($pagedata)) {
            $pagedata =  new \stdClass();
        }
        $this->view->page_data = $pagedata;
    }

    public function blogListingAction(Request $request)
    {
        $pagedata = $request->get("page_data");
        if (! isset($pagedata)) {
            $pagedata =   new \stdClass();
        }
        $this->view->page_data = $pagedata;
    }

}
?>
