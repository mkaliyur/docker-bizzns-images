<?php
namespace AppBundle\Controller;

use Website\Controller\Action;
use Pimcore\Controller\FrontendController;
use Pimcore\Model\Asset;
use Pimcore\Model\Document;
use Pimcore\Model\DataObject;
use Pimcore\Model;
use Pimcore\Tool;
use Pimcore\Model\Webservice;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Pimcore\Model\DataObject\BzPublishQueue\Listing;
use Pimcore\Model\DataObject\BzSolutions;
use Pimcore\Model\DataObject\BzSystem;


class BzSystemController extends FrontendController
{
    public function indexAction() {
        parent::indexAction();
    }

    private  function createSlug($str, $delimiter = '-'){
        $slug = strtolower(trim(preg_replace('/[\s-]+/', $delimiter, preg_replace('/[^A-Za-z0-9-]+/', $delimiter, preg_replace('/[&]/', 'and', preg_replace('/[\']/', '', iconv('UTF-8', 'ASCII//TRANSLIT', $str))))), $delimiter));
        return $slug;
    }

    public function solutionCreateAction(Request $request)
    {

    }

    public function getSolutionAction(Request $request)
    {
        $bzsolutions = $request->get('solutionid');
        $solutionList = BzSolutions::getById($system_id);
    }

    public function getSolutionListAction(Request $request)
    {        
        $solutionList = new DataObject\BzSystem\Listing();
        $solutionList->setCondition("name = ?", "BzSystem");

        foreach ($solutionList as $solution) {
            $pagedata = new \stdClass();
            $pagedata->Websites = [];
    
            if(!empty($solution->getBz_solutions())){
            foreach( (array) $solution->getBz_solutions() as $solutionlist1 ){
                $ret1 = new \stdClass();
                $ret1->site = new \stdClass();    
                $ret1->site->name = $solutionlist1->getName();
                $ret1->site->name = str_replace(" ", "", $ret1->site->name);
                $ret1->site->owner = $solutionlist1->getOwner();
                $ret1->site->giturl = $solutionlist1->getGiturl();
                $ret1->site->folder = $solutionlist1->getFolder();
                $ret1->site->asset_folder = $solutionlist1->getAsset_folder();
                $ret1->site->gitlab_access_token = $solutionlist1->getGitlab_access_token();
                $ret1->site->bind_port = $solutionlist1->getBind_port();
                $ret1->site->qout_object_update = $solutionlist1->getQout_object_update();
                $ret1->site->qout_document_update = $solutionlist1->getQout_document_update();
                $ret1->site->website_config_file_location = $solutionlist1->getWebsite_config_file_location();
                $ret1->site->website_template_db_giturl = $solutionlist1->getWebsite_template_db_giturl();
                $ret1->site->website_template_name = $solutionlist1->getWebsite_template_name();
                $ret1->site->website_source_url = $solutionlist1->getWebsite_source_url();
                // $ret1->site->website = $solutionlist1->getWebsite();


                $ret1->site->volumes=[];
                foreach((array) $solutionlist1->getVolumes() as $volume){
                    $volumename1 = $volume->getVolume_input1();
                    $volumename2 = $volume->getVolume_input2();
                    $volumename3 = $volume->getVolume_input3();
                    $volumename4 = $volume->getVolume_input4();
                    $volumename5 = $volume->getVolume_input5();
                    array_push($ret1->site->volumes, $volumename1, $volumename2, $volumename3, $volumename4, $volumename5);
                }
                $ret1->site->shared_volumes=[];
                foreach((array) $solutionlist1->getShared_volumes() as $sharedvolume){
                    $sharedvolumename1 = $sharedvolume->getSharedvolume_input1();
                    array_push($ret1->site->shared_volumes, $sharedvolumename1);
                }
                
                $ret1->site->cron_jobs=[];
                foreach((array) $solutionlist1->getCron_jobs() as $cronjob){
                    $ret2 = new \stdClass();
                    $ret2->job_name = $cronjob->getJob_name();
                    $ret2->executable = $cronjob->getExecutable();
                    $ret2->min = $cronjob->getMin();
                    $ret2->hours = $cronjob->getHours();
                    $ret2->day_of_month = $cronjob->getDay_of_month();
                    $ret2->month_of_year = $cronjob->getMonth_of_year();
                    $ret2->day_of_the_week = $cronjob->getDay_of_the_week();
                    $ret2->job_user = $cronjob->getJob_user();
                    array_push($ret1->site->cron_jobs, $ret2);
                }
                array_push($pagedata->Websites, $ret1);
            }
        }
        }
        $this->view->pagedata = $pagedata;
    }

    public function getWebsiteDataAction(Request $request)
    {
        $domain_name = strtolower($request->get('domainname'));
        $domain_name_upper = ucfirst($domain_name);
        $market_name = $request->get('marketname');
        $request_solution_name = strtolower($market_name)."_".strtolower($domain_name);
        
        
        $className = PIMCORE_PRIVATE_VAR."/classes/DataObject/BzListing".$domain_name_upper."/Listing.php";
        require_once($className);
        
        // require_once $className . ".php";
        $nsClass = "Pimcore\\Model\\DataObject\\BzListing".$domain_name_upper."\\Listing";
        $listingInstance = new $nsClass;


        $domain_path =  "/organization/Domains/".$domain_name;
        $market_path =  "/organization/MarketArea/".$market_name;

        $pagedata = new \stdClass();


        $pagedata->pages = [];
        $websitePagesList = new DataObject\BzSolutions\Listing();
        $websitePagesList->setCondition("name = ?", $request_solution_name);
        foreach ($websitePagesList as $pageItem) {
            $ret = $pageItem->getWebsite();
            if(!empty($ret)){
                $pages_href = $ret->getPages_href();
                foreach($pages_href as $page){
                    $page_document_ref = $page->getPage_document();
                    $page_document_path = $page_document_ref->getPath();
                    $page_document_key = $page_document_ref->getkey();
                    
                    $server_name = $page->getProperty('server_name');
                    $results2 = print_r($server_name, true);
                    \Pimcore\Log\Simple::log("event.log", 'KGM:: Inside BzSystem Controller page_document_ref_final= server_name= '.$results2);

                    $endswith = substr_compare($server_name, "/", -strlen("/")) === 0;
                    \Pimcore\Log\Simple::log("event.log", "KGM:::  endswith == ".$endswith );
                    if($endswith){
                        $pos = strpos($page_document_path, "/");
                        if ($pos !== false) {
                            $page_document_path = substr_replace($page_document_path,"", $pos, strlen("/"));
                        } 
                        $page_document_ref_final = $server_name.$page_document_path.$page_document_key;
                        $results2 = print_r($page_document_ref_final, true);
                        \Pimcore\Log\Simple::log("event.log", 'Inside BzSystem Controller page_document_ref_final= Inside if= '.$results2);
                    }else{
                        $page_document_ref_final = $server_name.$page_document_path.$page_document_key;
                        $results2 = print_r($page_document_ref_final, true);
                        \Pimcore\Log\Simple::log("event.log", 'Inside BzSystem Controller page_document_ref_final= Inside else= '.$results2);

                    }
                    // $page_document_ref_final = $page_document_path.$page_document_key;
                    array_push($pagedata->pages, $page_document_ref_final);
                }
            }
            // array_push($pagedata->pages, $ret);
        }



        $pagedata->audio = [];
        $websiteAudioList = new DataObject\BzAudio\Listing();
        $websiteAudioList->setCondition("solution_name = ?", $request_solution_name);
        foreach ($websiteAudioList as $audioItem) {
            $o_id = $audioItem->getO_id();
            $solution_name = $audioItem->getSolution_name();
            if(empty($solution_name)){
                continue;
            }
            $obj_type = strtolower($audioItem->getO_className());
            
            $server_name = $audioItem->getProperty('server_name');
            $endswith = substr_compare($server_name, "/", -strlen("/")) === 0;
            if($endswith){
                $ret = $server_name.$solution_name."/".$obj_type."/".$o_id;
            }else{
                $ret = $server_name."/".$solution_name."/".$obj_type."/".$o_id;
            }
            array_push($pagedata->audio, $ret);
        }

        $pagedata->blog = [];
        $websiteBlogList = new DataObject\BzBlogArticle\Listing();
        $websiteBlogList->setCondition("solution_name = ?", $request_solution_name);
        foreach ($websiteBlogList as $blogItem) {
            $o_id = $blogItem->getO_id();
            $solution_name = $blogItem->getSolution_name();
            if(empty($solution_name)){
                continue;
            }
            $obj_type = strtolower($blogItem->getO_className());
            
            $server_name = $blogItem->getProperty('server_name');
            $endswith = substr_compare($server_name, "/", -strlen("/")) === 0;
            if($endswith){
                $ret = $server_name.$solution_name."/".$obj_type."/".$o_id;
            }else{
                $ret = $server_name."/".$solution_name."/".$obj_type."/".$o_id;
            }
            array_push($pagedata->blog, $ret);
        }

        $pagedata->deals = [];
        $websiteDealList = new DataObject\BzDeals\Listing();
        $websiteDealList->setCondition("solution_name = ?", $request_solution_name);
        foreach ($websiteDealList as $dealItem) {
            $o_id = $dealItem->getO_id();
            $solution_name = $dealItem->getSolution_name();
            if(empty($solution_name)){
                continue;
            }
            $obj_type = strtolower($dealItem->getO_className());
            
            $server_name = $dealItem->getProperty('server_name');
            $endswith = substr_compare($server_name, "/", -strlen("/")) === 0;
            if($endswith){
                $ret = $server_name.$solution_name."/".$obj_type."/".$o_id;
            }else{
                $ret = $server_name."/".$solution_name."/".$obj_type."/".$o_id;
            }
            array_push($pagedata->deals, $ret);
        }

        $pagedata->events = [];
        $websiteEventList = new DataObject\BzEvents\Listing();
        $websiteEventList->setCondition("solution_name = ?", $request_solution_name);
        foreach ($websiteEventList as $eventItem) {
            $o_id = $eventItem->getO_id();
            $solution_name = $eventItem->getSolution_name();
            if(empty($solution_name)){
                continue;
            }
            $obj_type = strtolower($eventItem->getO_className());
            
            $server_name = $eventItem->getProperty('server_name');
            $endswith = substr_compare($server_name, "/", -strlen("/")) === 0;
            if($endswith){
                $ret = $server_name.$solution_name."/".$obj_type."/".$o_id;
            }else{
                $ret = $server_name."/".$solution_name."/".$obj_type."/".$o_id;
            }
            array_push($pagedata->events, $ret);
        }


        $pagedata->listings = [];
        $websiteListingList = $listingInstance;
        $websiteListingList->setCondition("solution_name = ?", $request_solution_name);
        foreach ($websiteListingList as $listingItem) {
            $o_id = $listingItem->getO_id();
            $solution_name = $listingItem->getSolution_name();
            if(empty($solution_name)){
                continue;
            }
            $obj_type = strtolower($listingItem->getO_className());
            
            $server_name = $listingItem->getProperty('server_name');
            $endswith = substr_compare($server_name, "/", -strlen("/")) === 0;
            if($endswith){
                $ret = $server_name.$solution_name."/".$obj_type."/".$o_id;
            }else{
                $ret = $server_name."/".$solution_name."/".$obj_type."/".$o_id;
            }
            array_push($pagedata->listings, $ret);
        }

        $pagedata->news = [];
        $websiteNewsList = new DataObject\BzNews\Listing();
        $websiteNewsList->setCondition("solution_name = ?", $request_solution_name);
        foreach ($websiteNewsList as $newsItem) {
            $o_id = $newsItem->getO_id();
            $solution_name = $newsItem->getSolution_name();
            if(empty($solution_name)){
                continue;
            }
            $obj_type = strtolower($newsItem->getO_className());
            
            $server_name = $newsItem->getProperty('server_name');
            $endswith = substr_compare($server_name, "/", -strlen("/")) === 0;
            if($endswith){
                $ret = $server_name.$solution_name."/".$obj_type."/".$o_id;
            }else{
                $ret = $server_name."/".$solution_name."/".$obj_type."/".$o_id;
            }
            array_push($pagedata->news, $ret);
        }
        
        $pagedata->videos = [];
        $websiteVideoList = new DataObject\BzVideo\Listing();
        $websiteVideoList->setCondition("solution_name = ?", $request_solution_name);
        foreach ($websiteVideoList as $videoItem) {
            $o_id = $videoItem->getO_id();
            $solution_name = $videoItem->getSolution_name();
            if(empty($solution_name)){
                continue;
            }
            $obj_type = strtolower($videoItem->getO_className());
            
            $server_name = $videoItem->getProperty('server_name');
            $endswith = substr_compare($server_name, "/", -strlen("/")) === 0;
            if($endswith){
                $ret = $server_name.$solution_name."/".$obj_type."/".$o_id;
            }else{
                $ret = $server_name."/".$solution_name."/".$obj_type."/".$o_id;
            }
            array_push($pagedata->videos, $ret);
        }

        $this->view->pagedata = $pagedata;
    }
}
?>
