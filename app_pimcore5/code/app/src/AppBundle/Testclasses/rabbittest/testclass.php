<?php
namespace AppBundle\Testclasses\rabbittest;

use OldSound\RabbitMqBundle\RabbitMq\ConsumerInterface;
use PhpAmqpLib\Message\AMQPMessage;

class testclass implements ConsumerInterface
{
   private $logger; // Monolog-logger.

   // Init:
   public function __construct( $logger )
   {
      $this->logger = $logger;
      echo "testclass is listening...";
   }

   public function execute(AMQPMessage $msg)
   {
      $message = unserialize($msg->body);
      echo "Got a message here in testclass::execute ";
      // Do something with the data. Save to db, write a log, whatever.
   }
}
