<?php

namespace Website\Model\BzUser;

class AbstractUser extends \Pimcore\Model\User\AbstractUser {

    // start overriding stuff
    public static function getByNameAndType($name, $type)
      {
        //$results2 = print_r($userFolder, true);
        //\Pimcore\Log\Simple::log("event.log", 'Inside Abstract User::getByName trying to get  = '.$name);

          try {
              $userOrFolder = new static();

              $userOrFolder->getDao()->getByNameAndType($name, $type);
              return $userOrFolder;
          } catch (\Exception $e) {
              return false;
          }
      }
}
