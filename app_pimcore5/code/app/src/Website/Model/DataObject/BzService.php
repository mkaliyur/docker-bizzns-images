<?php

namespace Website\Model\DataObject;

use Pimcore\Model\DataObject\Service;

class BzService extends \Pimcore\Model\DataObject\Service {

  public function copyRecursive($target, $source)
  {
    \Pimcore\Log\Simple::log("event.log", 'Inside BzService ::  copyRecursive');

      // avoid recursion
      if (!$this->_copyRecursiveIds) {
          $this->_copyRecursiveIds = [];
      }
      if (in_array($source->getId(), $this->_copyRecursiveIds)) {
          return;
      }

      $source->getProperties();
      //load all in case of lazy loading fields
      self::loadAllObjectFields($source);

      $new = Element\Service::cloneMe($source);
      $new->setId(null);
      $new->setChilds(null);
      $new->setKey(Element\Service::getSaveCopyName('object', $new->getKey(), $target));
      $new->setParentId($target->getId());
      if($this->_user){
        \Pimcore\Log\Simple::log("event.log", 'Inside Service.php new6 Inside if ');
        $new->setUserOwner($this->_user->getId());
        $new->setUserModification($this->_user->getId());
      }
      $new->setDao(null);
      $new->setLocked(false);
      $new->setCreationDate(time());
      $new->save();

      // add to store
      $this->_copyRecursiveIds[] = $new->getId();

      foreach ($source->getChilds() as $child) {
          $this->copyRecursive($new, $child);
      }

      $this->updateChilds($target, $new);

      // triggers actions after the complete document cloning
      \Pimcore::getEventDispatcher()->dispatch(DataObjectEvents::POST_COPY, new DataObjectEvent($new, [
          'base_element' => $source // the element used to make a copy
      ]));

      return $new;
  }
}
