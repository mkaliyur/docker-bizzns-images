<?php

namespace Website\Model\DataObject;

use Pimcore\Model\DataObject;
use Pimcore\Model\DataObject\AbstractObject;

class BzConcreteAudio extends \Website\Model\DataObject\BzConcrete
{

    /**
     * @var
     */
    private $adminEventHook;

    protected function getInhertied_from(){
        //$inherits_from = $this->inherited_from;
        $inherits_from = $this->getInherited_from ();
        \Pimcore\Log\Simple::log("kgm", "\n".">>>00000<<<< getInhertied_from :: inherits_from = ".$inherits_from );
        if (is_null($inherits_from) || empty($inherits_from)) {
          $inherits_from = $this-> getContextInheritedValue ( "audio_inherits_from" );
        }
        return $inherits_from;
    }
}
