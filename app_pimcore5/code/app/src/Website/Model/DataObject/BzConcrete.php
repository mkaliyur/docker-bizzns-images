<?php

namespace Website\Model\DataObject;

use Pimcore\Model\DataObject;
use Pimcore\Model\Document;
use Pimcore\Model\DataObject\AbstractObject;
use Pimcore\Event\AdminEvents;
use Symfony\Component\EventDispatcher\GenericEvent;
use Pimcore\Event\DataObjectEvents;
use Pimcore\Event\Model\DataObjectEvent;
use Pimcore\Event\DocumentEvents;
use Pimcore\Event\Model\DocumentEvent;
use Pimcore\Event\AssetEvents;
use Pimcore\Event\Model\AssetEvent;

use Carbon\Carbon;


class BzConcrete extends Concrete
{

    /**
     * @var
     */
    private $adminEventHook; //Legacy from Pimcore 4 - not used anymore

    protected static  $inheritableClasses = ['BzParent'];
    protected static $inheritableClassesIDs = [];

    protected $inheritableContextFields  = ['domain_name','market_name','market_area_href','domain_href','merchant_href','deal_inherits_from','event_inherits_from','audio_inherits_from',
        'video_inherits_from','article_inherits_from','solution_name'
    ];

    /**
     * An array of fields that were inherited from a parent
     * Customise this per extending class
     * @var array
     */
    protected $inheritableFields = [];

    public static function getSubscribedEvents()
     {
         return [
            AdminEvents::OBJECT_GET_PRE_SEND_DATA  => 'onPreSendData',
            DataObjectEvents::PRE_UPDATE => 'onPreUpdateDataObject',
            DataObjectEvents::PRE_ADD => 'onPreAddDataObject'
            //DataObjectEvents::POST_ADD  => 'onPostAddDataObject',
            //DataObjectEvents::POST_UPDATE => 'onPostUpdateDataObject',
            //DataObjectEvents::POST_DELETE => 'onPostDeleteDataObject',
            //DocumentEvents::POST_ADD => 'onPostAddDocument',
            //DocumentEvents::POST_UPDATE => 'onPostUpdateDocument'
            //DocumentEvents::POST_DELETE => 'onPostDeleteDocument',
            //AssetEvents::POST_ADD => 'onPostAddAsset',
            //AssetEvents::POST_UPDATE => 'onPostUpdateAsset',
            //AssetEvents::POST_DELETE => 'onPostDeleteAsset'
         ];
     }

     public function onPreUpdateDataObject(DataObjectEvent $e){
      $this->handleAddUpdatePublishEvent($e, 'OBJECT::PreUPDATE');
      }

    public function onPreAddDataObject(DataObjectEvent $e){
      $this->handleAddUpdatePublishEvent($e, 'OBJECT::PreADD');
    }

    public function handleAddUpdatePublishEvent (DataObjectEvent $e){
      $data = $e->getArguments()['data'];
      $object = $e->getObject();
      // $results2 = print_r($object, true);
      // \Pimcore\Log\Simple::log("event.log", 'Inside BzConcrete data object handleAddUpdatePublishEvent object = '.$results2);

      $o_type = $object->getO_type();
      if ($o_type == 'folder'){
        return;
      }else{
        \Pimcore\Log\Simple::log("event.log", 'Inside BzConcrete data object handleAddUpdatePublishEvent o_type is Folder  '.$o_type);
      }

      $o_className = strtolower($object->getO_className());
      if ($o_className == 'BzPublishQueue'){
        return;
      }

      $o_path = $object->o_path;

      $o_key = $object->o_key;
      $o_path_array = explode("/", $o_path);
      $solution_name_property = $object->getProperty('solution_name');
      $solution_id = $solution_name_property;

      foreach($object->getO_class()->getFieldDefinitions() as $fieldDefinition) {        
        if($fieldDefinition->getName() == 'type_display_name'){
          $type_display_name = $fieldDefinition->getTitle();  
        } 
      }

      $object->setObject_type($type_display_name);

      if($o_path_array[1] == 'solutions'){    
        $object->setSolution_name($solution_name_property);
      }else{
        \Pimcore\Log\Simple::log("event.log", 'Inside BzConcrete data object handleAddUpdatePublishEvent solution_id Inside else= ');
      }
    }
    public function handleObjectPublishEvent (DataObjectEvent $e, string $eventType ){
      $data = $e->getArguments()['data'];
      $object = $e->getObject();

      $o_id = $object->o_id;
      $results2 = print_r($o_id, true);

      $o_className = $object->getO_className();

      if ($o_className == 'BzPublishQueue' ){
        return;
      }

      $o_path = $object->o_path;
      $o_key = $object->o_key;
      $o_path_array = explode("/", $o_path);
      $solution_name_property = $object->getProperty('solution_name');
      $solution_id = $solution_name_property;

      $event_type = $eventType;

      $event_file_prefix = strtolower($eventType);

      $server_name = $object->getProperty('server_name');
      $url = $server_name.strtolower( $solution_id. '/' .$o_className. '/' .$o_id);
      //$url = strtolower($url);
      $pobject = new DataObject\BzPublishQueue();

      $dt = Carbon::now();
      $dtstr = $dt->toDateTimeString();
      $pobject->setPublishDateTime($dt);
      $pobject->setKey(\Pimcore\File::getValidFilename($dtstr.'_'.$event_file_prefix.'_'.$o_id));
      $parent_folder_path = "/system/publish_queue/";

      $parent_folder = DataObject::getByPath($parent_folder_path);
      $parent_folder_id = $parent_folder->o_id;

      $pobject->setParentId($parent_folder_id);
      $pobject->setClass_name($o_className);
      $pobject->setObject_id($o_id);
      $pobject->setObject_path($o_path);
      $pobject->setObject_key($o_key);
      $pobject->setSolution_id($solution_id);
      $pobject->setEvent_type($event_type);
      $pobject->setUrl($url);
      $pobject->setIs_processed(false);
      try{
      $pobject->save(["versionNote" => "my new version"]);
      //$results2 = print_r($pobject, true);
      //// \Pimcore\Log\Simple::log("event.log", 'Inside BzConcrete pobject '.$results2);

      }catch(Exception $e){
        $results2 = print_r($e, true);
        // \Pimcore\Log\Simple::log("event.log", 'Inside BzConcrete. Catch exception during save '.$results2);
      }

    }

    public function onPostAddDataObject (DataObjectEvent $e) {
      $this->handleObjectPublishEvent($e, 'OBJECT::CREATE');
    }

    public function onPostUpdateDataObject (DataObjectEvent $e) {
      $this->handleObjectPublishEvent($e, 'OBJECT::UPDATE');
     }

    public function onPostDeleteDataObject (DataObjectEvent $e) {
      $this->handleObjectPublishEvent($e, 'OBJECT::DELETE');
    }

    public function handleDocumentPublishEvent (DocumentEvent $e, string $eventType){
      $data = $e->getArguments();
      $document = $e->getDocument();

      $id = $document->id;
      $type = $document->type;
      $path = $document->path;
      $key = $document->key;
      $o_path = ($path .$key);
      //$o_className = $object->o_className;
      //if ($o_className == 'BzPublishQueue' ){
        //return;
      //}
      $o_path_array = explode("/", $o_path);
      $solution_name_property = $object->getProperty('solution_name');
      $solution_id = $solution_name_property;

      // $solution_id = $o_path_array[2];

      $event_type = $eventType;
      $event_file_prefix = strtolower($eventType);
      $server_name = $document->getProperty('server_base');
      $url = $server_name.strtolower($o_path);
      //$url = strtolower($url);
      $pdocument = new DataObject\BzPublishQueue();
      $dt = Carbon::now();
      $dtstr = $dt->toDateTimeString();
      $pdocument->setPublishDateTime($dt);
      $pdocument->setKey(\Pimcore\File::getValidFilename($dtstr.'_'.$event_file_prefix.'_'.$id));

      $parent_folder_path = "/system/publish_queue/";
      $parent_folder = DataObject::getByPath($parent_folder_path);
      $parent_folder_id = $parent_folder->o_id;
      $pdocument->setParentId($parent_folder_id);

      $pdocument->setClass_name($type);
      //$pdocument->setClass_name($o_className);
      $pdocument->setObject_id($id);
      $pdocument->setObject_path($o_path);
      $pdocument->setObject_key($key);
      $pdocument->setSolution_id($solution_id);
      $pdocument->setEvent_type($event_type);
      $pdocument->setUrl($url);
      $pdocument->setIs_processed(false);
      $pdocument->save(["versionNote" => "my new version"]);
    }

    public function onPostAddDocument (DocumentEvent $e){
      $this->handleDocumentPublishEvent ($e, 'DOCUMENT::CREATE');
    }

    public function onPostUpdateDocument (DocumentEvent $e){
      $this->handleDocumentPublishEvent ($e, 'DOCUMENT::UPDATE');
    }

    public function onPostDeleteDocument (DocumentEvent $e){
      $this->handleDocumentPublishEvent ($e, 'DOCUMENT::DELETE');
    }

    public function handleAssetPublishEvent (AssetEvent $e, string $eventType){
      $data = $e->getArguments();
      $asset = $e->getAsset();

      $id = $asset->id;
      $type = $asset->type;
      $path = $asset->path;
      $filename = $asset->filename;
      $o_path = ($path .$filename);
      //$o_className = $object->o_className;
      //if ($o_className == 'BzPublishQueue' ){
        //return;
      //}
      $o_path_array = explode("/", $o_path);
      $solution_name_property = $object->getProperty('solution_name');
      $solution_id = $solution_name_property;

      // $solution_id = $o_path_array[2];
      $event_type = $eventType;
      $event_file_prefix = strtolower($eventType);
      $server_name = $asset->getProperty('server_base');
      $url = $server_name.strtolower($o_path);
      //$url = strtolower($url);
      $passet = new DataObject\BzPublishQueue();
      $dt = Carbon::now();
      $dtstr = $dt->toDateTimeString();
      $passet->setPublishDateTime($dt);
      $passet->setKey(\Pimcore\File::getValidFilename($dtstr.'_'.$event_file_prefix.'_'.$id));
      $parent_folder_path = "/system/publish_queue/";
      $parent_folder = DataObject::getByPath($parent_folder_path);
      $parent_folder_id = $parent_folder->o_id;
      $passet->setParentId($parent_folder_id);
      $passet->setClass_name($type);
      //$passet->setClass_name($o_className);
      $passet->setObject_id($id);
      $passet->setObject_path($o_path);
      $passet->setObject_key($filename);
      $passet->setSolution_id($solution_id);
      $passet->setEvent_type($event_type);
      $passet->setUrl($url);
      $passet->setIs_processed(false);
      $passet->save(["versionNote" => "my new version"]);

    }

    public function onPostAddAsset (AssetEvent $e, string $eventType){
      $this->handleAssetPublishEvent ($e, 'ASSET::CREATE');
    }

    public function onPostUpdateAsset (AssetEvent $e, string $eventType){
      $this->handleAssetPublishEvent ($e, 'ASSET::UPDATE');
    }

    public function onPostDeleteAsset (AssetEvent $e, string $eventType){
      $this->handleAssetPublishEvent ($e, 'ASSET::DELETE');
    }


    public function onPreSendData (GenericEvent $e) {
      //$results2 = print_r($merchant_parent_folder, true);
      // \Pimcore\Log\Simple::log("event.log", 'Inside BzConcrete onPreSendData');

      $data = $e->getArguments()['data'];
      $results2 = print_r($data, true);
      // \Pimcore\Log\Simple::log("event.log", 'Inside BzConcrete beginning data='.$results2);

      $object = $e->getArguments()['object'];
      if ($object instanceof BzConcrete) {
          $inheritedFields = $object->getInheritedFields();
          $inherited_json = json_encode($inheritedFields);
          //$results2 = print_r($inherited_json, true);
          // \Pimcore\Log\Simple::log("event.log", 'Inside BzConcrete inherited_json='.$results2);

          //$results2 = print_r($inheritedFields, true);
          // \Pimcore\Log\Simple::log("event.log", 'Inside BzConcrete inheritedFields='.$results2);

         $inherited_domain_name = $inheritedFields['domain_name']['data'];
         //$results2 = print_r($inherited_domain_name, true);
         // \Pimcore\Log\Simple::log("event.log", 'Inside BzConcrete inherited_domain_name='.$results2);

         $data['data']['domain_name'] = $inherited_domain_name;
         //$results2 = print_r($data['data']['domain_name'], true);
         // \Pimcore\Log\Simple::log("event.log", 'Inside BzConcrete after setting data_domain_name='.$results2);

          $inherited_market_name = $inheritedFields->market_name->data;
          //$results2 = print_r($inherited_market_name, true);
          // \Pimcore\Log\Simple::log("event.log", 'Inside BzConcrete inherited_market_name='.$results2);

          foreach($inheritedFields as $fieldName=>$editmodeData) {
            //$results2 = print_r($fieldName, true);
            // \Pimcore\Log\Simple::log("event.log", 'Inside BzConcrete fieldName='.$results2);

            //$results2 = print_r($editmodeData, true);
            // \Pimcore\Log\Simple::log("event.log", 'Inside BzConcrete editmodeData='.$results2);

              if (isset($data['metaData'][$fieldName])) {
                // \Pimcore\Log\Simple::log("event.log", 'Inside BzConcrete inside if=');
                  $data['metaData'][$fieldName] = $editmodeData['metaData'];
                  //$results2 = print_r($data, true);
                  // \Pimcore\Log\Simple::log("event.log", 'Inside BzConcrete data='.$results2);
              }

              //add the value
              $data['data'][$fieldName] = $editmodeData['data'];
          }
      }

      $e->setArgument('data', $data);

    }

}
