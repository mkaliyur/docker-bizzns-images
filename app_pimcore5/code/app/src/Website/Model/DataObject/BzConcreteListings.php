<?php

namespace Website\Model\DataObject;

use Pimcore\Model\DataObject;
use Pimcore\Model\DataObject\AbstractObject;

class BzConcreteListings extends \Website\Model\DataObject\BzConcrete
{

    /**
     * @var
     */
    private $adminEventHook;

    protected function getInhertied_from(){
        // \Pimcore\Log\Simple::log("kgm", "\n\n\n\n\n>>>>>>>> Inside getInhertied_from method::BzConcrete_Listings ".$this->inherited_from);
        $inherits_from = $this->inherited_from;
        if (is_null($inherits_from) || empty($inherits_from)) {
          $inherits_from = $this-> getContextInheritedValue ( "listing_inherits_from" );
          // \Pimcore\Log\Simple::log("kgm", "\n\n\n\n\n>>>>>>>> BzConcreteListings:: Inherited from was null ::  Inside If Condition  :: after getContextInheritedValue ".$inherits_from);
        }
        return $inherits_from;
    }


}
