<?php

namespace Website\Model\DataObject;

use Pimcore\Model\DataObject\Folder;

class BzFolder extends \Pimcore\Model\DataObject\Folder {

protected $name;

public function getName() {
	$data = $this->name;
	return $data;
}

/**
* Set name - Name
* @param string $name
* @return \Pimcore\Model\DataObject\BzListing2
*/
public function setName($name) {
	$this->name = $name;
	return $this;
}

public function setObject_type($object_type) {
	return $this;
}

public function setSolution_name($solution_name) {
	return $this;
}

}
