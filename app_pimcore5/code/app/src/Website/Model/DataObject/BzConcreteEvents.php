<?php

namespace Website\Model\DataObject;

use Pimcore\Model\DataObject;
use Pimcore\Model\DataObject\AbstractObject;

class BzConcreteEvents extends \Website\Model\DataObject\BzConcrete
{
    /**
     * @var
     */
    private $adminEventHook;

    protected function getInhertied_from(){
        // \Pimcore\Log\Simple::log("kgm", "\n\n\n\n\n>>>>>>>> Inside getInhertied_from method::BzConcrete_Events ".$this->inherited_from);
        $inherits_from = $this->inherited_from;
        if (is_null($inherits_from) || empty($inherits_from)) {
          $inherits_from = $this-> getContextInheritedValue ( "event_inherits_from" );
          // \Pimcore\Log\Simple::log("kgm", "\n\n\n\n\n>>>>>>>> BzConcreteEvents:: Inherited from was null ::  Inside If Condition  :: after getContextInheritedValue ".$inherits_from);
        }
        return $inherits_from;
    }
}
