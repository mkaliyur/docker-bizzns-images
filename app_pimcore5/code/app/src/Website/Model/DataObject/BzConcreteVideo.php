<?php

namespace Website\Model\DataObject;

use Pimcore\Model\DataObject;
use Pimcore\Model\DataObject\AbstractObject;

class BzConcreteVideo extends \Website\Model\DataObject\BzConcrete
{

    /**
     * @var
     */
    private $adminEventHook;

    protected function getInhertied_from(){
        $inherits_from = $this->inherited_from;
        if (is_null($inherits_from) || empty($inherits_from)) {
          $inherits_from = $this-> getContextInheritedValue ( "video_inherits_from" );
        }
        return $inherits_from;
    }
}
