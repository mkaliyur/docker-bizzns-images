<?php

namespace Website\Model\DataObject;

use Pimcore\Model\DataObject;
use Pimcore\Model\DataObject\AbstractObject;
use Pimcore\Model\DataObject\ClassDefinition;
use Symfony\Component\EventDispatcher\GenericEvent;
use Pimcore\Event\AdminEvents;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Website\Model\DataObject\BzConcrete;
use Pimcore\Event\DataObjectEvents;
use Pimcore\Event\Model\DataObjectEvent;

class Concrete extends \Pimcore\Model\DataObject\Concrete implements EventSubscriberInterface
{

    /**
     * @var
     */
    private $adminEventHook;


    protected static $inheritableClasses = ['BzParent'];

    protected static $inheritableClassesIDs = [];

    /**
     * An array of fields that were inherited from a parent
     * Customise this per extending class
     * @var array
     */
    protected $inheritableFields = [];

    public function setInheritableFieldsValue($key, $value) {
        $this->inheritableFields[$key] = $value;
    }

    public function getInheritableFieldsValue($key) {
        return $this->inheritableFields[$key];
    }



    protected $inheritableContextFields = [];

    public function setInheritableContextFieldsValue($key, $value) {
        $this->inheritableContextFields[$key] = $value;
    }

    public function getInheritableContextFieldsValue($key) {
        return $this->inheritableContextFields[$key];
    }


    /**
     * Fields that have been inherited from parents
     * @var array
     */
    private $inheritedFields = [];



    public static function add_value($value) {
        array_push(self::$inheritableClassesIDs, $value );
    }


    public static function loadInheritableClassIDs(){
        $class_id_array = [];
        $array = self::$inheritableClasses;
        foreach ($array as $value) {
            $class =  \Pimcore\Model\DataObject\ClassDefinition::getByName($value);

            if ($class) {
               $class_id =  $class->getId();
                // \Pimcore\Log\Simple::log("kgm", "\n\n\n\n\n>>>>>>>>>>>>>>>>>"."class was true! id = ".$class_id);
               self::add_value($class_id);
            }else{
                self::add_value(34);
                // \Pimcore\Log\Simple::log("kgm", "\n\n\n\n\n>>>>>>>>>>>>>>>>>"."class was FALSE!");
            }
        }
    }



    protected function getInhertied_from(){
        // \Pimcore\Log\Simple::log("kgm", "\n\n\n\n\n>>>>>>>> Inside getInhertied_from method::Concrete ");
    }



    public  function isAllowedInheritanceClass($parentClassId)
    {

        if( empty(self::$inheritableClassesIDs) ){
            self::loadInheritableClassIDs();
        }

        return $parentClassId == $this->getClassId() ||
                                    in_array($parentClassId, self::$inheritableClassesIDs );
    }


    public function preGetValue($key)
    {

        if( $key != 'inherited_from'){
            $this->setInheritableFieldsValue($key,$key);
        }

        if (in_array($key, $this->getInheritableFields() )) {
            //\Pimcore\Log\Simple::log("kgm", "\n"."in_array getInheritableFields true:: Inside getInheritableFields for key = ".$key );
            $ret = $this->getObjectValueWithInheritanceCheck($key);
            return $ret;
        }else{
             \Pimcore\Log\Simple::log("kgm","Concrete!! preGetValue :: data_key was NOT in array getInheritableFields ! for key == ".$key);

        }
    }


    public function getContextInheritedValue($key){

       $parenttest = $this->getParent();

        //$results2 = print_r($parenttest, true);
        //\Pimcore\Log\Simple::log("event.log", 'Inside Concrete php getContextInheritedValue before if parenttest='.$results2);

        if ($parenttest instanceof AbstractObject) {
             //\Pimcore\Log\Simple::log("event.log", "1  abs object true for key= ".$key );

            $parent = $parenttest;
            while ($parent && $parent->getType() == "folder") {
              //$results2 = print_r($parent, true);
              //\Pimcore\Log\Simple::log("event.log", "<<<<<<=====>>>>>> parent of Folder Beginning of while = ".$results2 );

                $parent = $parent->getParent();

                //$results2 = print_r($parent, true);
                //\Pimcore\Log\Simple::log("event.log", "<<<<<<=====>>>>>> parent of Folder Ending of while= ".$results2 );

            }
            while ($parent && ($parent->getType() == "object" || $parent->getType() == "variant")) {
                 //\Pimcore\Log\Simple::log("event.log", "2  Object true id --".$parent->getId() );
                if ($this->isAllowedInheritanceClass($parent->getClassId())) {
                     //\Pimcore\Log\Simple::log("event.log", "3  isAllowedInheritanceClass true " );
                    $getter = 'get' . ucfirst($key);
                     //\Pimcore\Log\Simple::log("event.log", "4  getter is ".$getter );
                    if (method_exists($parent, $getter)) {
                      //$results2 = print_r($getter, true);
                      //\Pimcore\Log\Simple::log("event.log", 'Inside Concrete php getter='.$results2);

                        //set inherited fields to override in the admin panel
                        //as the preget value is overwritten in the concrete class for each
                        //object value
                         //\Pimcore\Log\Simple::log("event.log", "5  getter exists ".$getter );
                        if (\Pimcore::inAdmin()) {
                            //$this->initAdminEventHook(); //Legacy from Pimcore 4 - not used anymore
                            $fieldDefinition = $this->getClass()->getFieldDefinition($key);
                            $this->inheritedFields[$key] = $this->getEditmodeDataForField($parent, $key, $fieldDefinition);
                        }

                        $ret_data = $parent->$getter();
                         //\Pimcore\Log\Simple::log("event.log", "6a context returning ".$ret_data );

                        if (is_null($ret_data) || empty($ret_data)) {
                            $parent = $parent->getParent();

                            while ($parent && $parent->getType() == "folder") {
                                $parent = $parent->getParent();
                            }
                        }
                        else{
                             //\Pimcore\Log\Simple::log("event.log", "6b context returning ret_data ".$ret_data );
                            return $ret_data;
                        }

                    }
                }

            }
        }else{
        }

        return null;

    }




    /**
     * Based on getNextParentforInheritance but tweaked for preGetters
     * @return null|static
     */
    public function getInheritedValue($that, $key)
    {

    //    \Pimcore\Log\Simple::log("kgm","000 Inside getInheritedValue starting processing for key  = ".$key);

        if(in_array($key, $this->getInheritableContextFields())){
            //\Pimcore\Log\Simple::log("kgm","in_array getInheritableContextFields is true for " . $key );
            return $this->getContextInheritedValue($key);
        }else{
          //\Pimcore\Log\Simple::log("kgm","001 Key not in getInheritableContextFields for key  = ".$key);
        }

        // \Pimcore\Log\Simple::log("kgm","in_array getInheritableContextFields is FALSE for " . $key );


        $that = $this;
        $need_inheritance = true;
        $ret_data = null;

        while ($need_inheritance){
            //step 1 if not check if inherited_from is set
                //$data_if = $this->inherited_from;
                $data_if = $that->getInhertied_from();
                //\Pimcore\Log\Simple::log("kgm", $this->getInhertied_from() );
                 //\Pimcore\Log\Simple::log("kgm", "\n\n\n\n while 00 dataif ".$data_if." for key = . ".$key );

                if (is_null($data_if) || empty($data_if)) {
                     //\Pimcore\Log\Simple::log("kgm","while 00_2 :: data_if was null! in getInheritedValue ".$key  );
                    //$need_inheritace = false;
                    $parent = $that->getParent();
                     //\Pimcore\Log\Simple::log("kgm","while 00_3 :: data_if was null! inside getParent".$key  );
                }else{
                    //step 2 if inherit_from is set, get parent object and traverse the tree
                    $object = DataObject::getByPath($data_if);
                    $parent = $object;
                     //\Pimcore\Log\Simple::log("kgm","*** getInheritedValue:: data_if not null!! in else:: after getting parent:: for key  ".$key     );
                     //\Pimcore\Log\Simple::log("kgm", "--> 0 parent type was ".$parent->getType()." for ".$key );

                }

                while ($parent && $parent->getType() == "folder") {
                   //\Pimcore\Log\Simple::log("kgm","while 00_2 :: data_if was null! inside while folder ".$key  );
                        $parent = $parent->getParent();
                }

                if ($parent  instanceof AbstractObject  &&
                    ($parent->getType() == "object" || $parent->getType() == "variant")   ) {
                     //\Pimcore\Log\Simple::log("kgm", "1  abs object true " );

                   // while ($parent && ($parent->getType() == "object" || $parent->getType() == "variant")) {      \Pimcore\Log\Simple::log("kgm", "2  Object true:: Inside while parent id -- ".$parent->getId() );

                        if ($this->isAllowedInheritanceClass($parent->getClassId())) {
                              //\Pimcore\Log\Simple::log("kgm", "\n\n\n 3  isAllowedInheritanceClass true for ".$key  );
                            $getter = 'get' . ucfirst($key);
                             //\Pimcore\Log\Simple::log("kgm", "4  getter is ".$getter );
                            if (method_exists($parent, $getter)) {
                                //set inherited fields to override in the admin panel
                                //as the preget value is overwritten in the concrete class for each
                                //object value
                                  //\Pimcore\Log\Simple::log("kgm", "5  getter exists ".$getter );
                                if (\Pimcore::inAdmin()) {
                                     //\Pimcore\Log\Simple::log("kgm", "5a  isadmin  true  ".$getter );
                                    //$this->initAdminEventHook(); //Legacy from Pimcore 4 - not used anymore
                                     //\Pimcore\Log\Simple::log("kgm", "5a  isadmin  true ::after initAdminEventHook ".$key );
                                    $fieldDefinition = $this->getClass()->getFieldDefinition($key);
                                     //\Pimcore\Log\Simple::log("kgm", "5a  isadmin  true ::after fieldDefinition:: calling getEditmodeDataForField  ".$key );
                                    $this->inheritedFields[$key] = $this->getEditmodeDataForField($parent, $key, $fieldDefinition);
                                     //\Pimcore\Log\Simple::log("kgm", "5a  isadmin  true ::after calling: getEditmodeDataForField ".$key );
                                }

                                //\Pimcore\Log\Simple::log("kgm", "6  returning ".$parent->$getter() );
                                $ret_data_value = $parent->$getter();
                                  //\Pimcore\Log\Simple::log("kgm", "6a  returning ret_data ". " for key ".$key."  " );
                                if (is_null($ret_data_value) || empty($ret_data_value)) {
                                    $need_inheritance = true;
                                    $that=$parent;
                                }
                                else{
                                    $ret_data = $ret_data_value;
                                    $need_inheritance = false;
                                }


                            }else{
                               //\Pimcore\Log\Simple::log("kgm", "5b method does NOT exists ".$getter);
                              $need_inheritance = false;
                            }
                        }else{
                             //\Pimcore\Log\Simple::log("kgm", "6a this->isAllowedInheritanceClass($parent->getClassId())  was FALSE!! ");
                        }
                    //}
                }else{
                     //\Pimcore\Log\Simple::log("kgm", "in else! parent was not AbstractObject!! for key =".$key );
                    //step 3 if not.. field value is null.. return it
                    $need_inheritance = false;
                     //\Pimcore\Log\Simple::log("kgm","7 null - key == ".$key);
                }
        }

        //\Pimcore\Log\Simple::log("kgm","8 returning - ret_data  for key  = ".$key);
        return $ret_data;

    }

    /**
     * Returns the list of fields for this object that can be inherited
     * @return array
     */
    public function &getInheritableFields()
    {
        return $this->inheritableFields;
    }





    public function &getInheritableContextFields()
    {
        return $this->inheritableContextFields;
    }



    /**
     * Returns the list of fields for this object that were inherited
     * @return array
     */
    public function getInheritedFields()
    {
        return $this->inheritedFields;
    }

    public function getObjectValueWithInheritanceCheck($key)
    {

        $that = $this;
         // \Pimcore\Log\Simple::log("kgm","getObjectValueWithInheritanceCheck:=: processing key ==  ".$key  );

        $data_if = $this->inherited_from;


        if( $key == 'inherited_from'){
            return $data_if;
        }else{

            if (is_null($that->lazyLoadedFields)) {
                // \Pimcore\Log\Simple::log("kgm","inside is_null($that->lazyLoadedFields)");
                $data = $that->$key;
            }elseif (in_array($key, $that->lazyLoadedFields)) {
                 // \Pimcore\Log\Simple::log("kgm","inside  in_array($key, $that lazyLoadedFields called");
                $data = $that->getClass()->getFieldDefinition($key)->preGetData($that);
            } else {
                // \Pimcore\Log\Simple::log("kgm","inside else");
                $data = $that->$key;
            }

            // \Pimcore\Log\Simple::log("kgm", "!!!! inside getObjectValueWithInheritanceCheck  data = ".$data );
            if (is_null($data) || empty($data)) {
                 // \Pimcore\Log\Simple::log("kgm", "getObjectValueWithInheritanceCheck is_null data true!!abt to call getInheritedValue!!!!!!!" );
                //return $that->getInheritedValue($that,$key);
                $data = $that->getInheritedValue($that,$key);
            } else{

                if ($data instanceof \Pimcore\Model\DataObject\Fieldcollection ) {
                   // \Pimcore\Log\Simple::log("kgm", "YES!! is instanceof pf Fieldcollection");
                   //print_r($data->items );
                   //return $that->getInheritedValue($that,$key);
                   $data = $that->getInheritedValue($that,$key);

                }

            }

            return $data;

        }

    }

     public static function getSubscribedEvents()
     {
         return [
          //AdminEvents::OBJECT_GET_PRE_SEND_DATA  => 'onPreSendData'
         ];
     }




    /**
     * Provides an admin panel hook that is automatically applied if inheritance is deteected
     */
    //Legacy from Pimcore 4 - not used anymore - BzConcrete Handles on presend data event.
    // private function initAdminEventHook()
    // {
    //     if (!$this->adminEventHook) {
    //         $this->adminEventHook = \Pimcore::getEventManager()
    //             ->attach("admin.object.get.preSendData", function (\Zend_EventManager_Event $e) {
    //                 $object = $e->getParam('object');
    //
    //                 $returnValueContainer = $e->getParam('returnValueContainer');
    //                 $data = $returnValueContainer->getData();
    //
    //                 if ($object instanceof Concrete) {
    //
    //                     $inheritedFields = $object->getInheritedFields();
    //
    //                     foreach($inheritedFields as $fieldName=>$editmodeData) {
    //                         if (isset($data['metaData'][$fieldName])) {
    //                             $data['metaData'][$fieldName] = $editmodeData['metaData'];
    //                         }
    //
    //                         //add the value
    //                         $data['data'][$fieldName] = $editmodeData['data'];
    //                     }
    //
    //                 }
    //
    //                 $returnValueContainer->setData($data);
    //             });
    //     }
    // }

    protected function getEditmodeDataForField($object, $key, $fieldDefinition)
    {

        // \Pimcore\Log\Simple::log("kgm", " Inside getEditmodeDataForField :: for key  " .$key  );
        $getter = "get" . ucfirst($key);

        // \Pimcore\Log\Simple::log("kgm", " Inside getEditmodeDataForField :: gtter now is ".$getter  ."  for key  " .$key  );

        $editmodeData = [
            'data' => null,
            'metaData' => [
                'objectid' => $object->getId()
            ]
        ];

        if ((
            $fieldDefinition instanceof Object\ClassDefinition\Data\Relations\AbstractRelations
            && $fieldDefinition->getLazyLoading()
            && !$fieldDefinition instanceof Object\ClassDefinition\Data\DataObjectsMetadata
            && !$fieldDefinition instanceof Object\ClassDefinition\Data\MultihrefMetadata
        )
        || $fieldDefinition instanceof Object\ClassDefinition\Data\Nonownerobjects
        ) {


            // \Pimcore\Log\Simple::log("kgm", " Inside getEditmodeDataForField :: INSIDE IF for key  " .$key  );

            $refId = null;

            if ($fieldDefinition->isRemoteOwner()) {
                $refKey = $fieldDefinition->getOwnerFieldName();
                $refClass = Object\ClassDefinition::getByName($fieldDefinition->getOwnerClassName());
                if ($refClass) {
                    $refId = $refClass->getId();
                }
            } else {
                $refKey = $key;
            }

            $relations = $object->getRelationData($refKey, !$fieldDefinition->isRemoteOwner(), $refId);

            if (!empty($relations)) {
                $data = [];
                if ($fieldDefinition instanceof Object\ClassDefinition\Data\Href) {
                    $data = $relations[0];
                } else {
                    foreach ($relations as $rel) {
                        if ($fieldDefinition instanceof Object\ClassDefinition\Data\Objects) {
                            $data[] = [$rel["id"], $rel["path"], $rel["subtype"]];
                        } else {
                            $data[] = [$rel["id"], $rel["path"], $rel["type"], $rel["subtype"]];
                        }
                    }
                }
                $editmodeData['data'] = $data;
                $editmodeData['metaData']['objectid'] = $object->getId();
                $editmodeData['metaData']['inherited'] = true;
            }

        } else {

            // \Pimcore\Log\Simple::log("kgm", " Inside getEditmodeDataForField :: INSIDE ELSE for key  " .$key." getter == ".$getter  );

            // \Pimcore\Log\Simple::log("kgm", " Inside getEditmodeDataForField :: INSIDE ELSE :: just before calling getter $getter :: for key  " .$key  );

            $fieldData = $object->$getter();

            // \Pimcore\Log\Simple::log("kgm", " *******>> SHOULD COME HERE !! Inside getEditmodeDataForField :: INSIDE ELSE :: just after getting fielddata :: for key  " .$key  );

            if (empty($fieldData)){
                // \Pimcore\Log\Simple::log("kgm", " Inside getEditmodeDataForField :: INSIDE ELSE :: fieldData was empty!   for key  " .$key  );
                //return $editmodeData;
            }


            if ($fieldDefinition instanceof Object\ClassDefinition\Data\CalculatedValue) {


                // \Pimcore\Log\Simple::log("kgm", " Inside getEditmodeDataForField :: INSIDE ELSE::  fieldDefinition is CalculatedValue  " .$key  );

                $fieldData = new Object\Data\CalculatedValue($fieldDefinition->getName());
                $fieldData->setContextualData("object", null, null, null);
                $value = $fieldDefinition->getDataForEditmode($fieldData, $object, true);
            } else {


                // \Pimcore\Log\Simple::log("kgm", " Inside getEditmodeDataForField :: INSIDE ELSE::  fieldDefinition is NOT a CalculatedValue . getting value " .$key  );

                $value = $fieldDefinition->getDataForEditmode($fieldData, $object, true);

                // \Pimcore\Log\Simple::log("kgm", " Inside getEditmodeDataForField :: INSIDE ELSE::  fieldDefinition is NOT a CalculatedValue . AFTER getting value " .$key  );
            }

            if (!$fieldDefinition->isEmpty($fieldData)) {
                // \Pimcore\Log\Simple::log("kgm", " Inside getEditmodeDataForField :: INSIDE ELSE:: isEmpty true for key  " .$key  );
                $editmodeData['metaData']['objectid'] = $object->getId();
                $editmodeData['metaData']['inherited'] = true;
                $editmodeData['data'] = $value;
            }else{
                // \Pimcore\Log\Simple::log("kgm", " Inside getEditmodeDataForField :: INSIDE ELSE:: isEmpty FALSE for key  " .$key." setting data to ".$value  );
                $editmodeData['metaData']['objectid'] = $object->getId();
                $editmodeData['metaData']['inherited'] = true;
                $editmodeData['data'] = $value;
            }



        }

        // \Pimcore\Log\Simple::log("kgm", " returning editmodeData ::  "   );
        return $editmodeData;
    }

}
