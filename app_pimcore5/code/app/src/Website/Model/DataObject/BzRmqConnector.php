<?php

namespace Website\Model\DataObject;

use Pimcore\Model\DataObject;
use Pimcore\Model\Document;
use Pimcore\Model\DataObject\AbstractObject;
use Pimcore\Event\AdminEvents;
use Symfony\Component\EventDispatcher\GenericEvent;
use Pimcore\Event\DataObjectEvents;
use Pimcore\Event\Model\DataObjectEvent;
use Pimcore\Event\DocumentEvents;
use Pimcore\Event\Model\DocumentEvent;
use Pimcore\Event\AssetEvents;
use Pimcore\Event\Model\AssetEvent;
use Pimcore\Model\DataObject\Folder as DataObjectFolder;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpKernel\KernelEvents;
use Symfony\Component\HttpKernel\Event\GetResponseEvent;
use Symfony\Component\HttpKernel\Event\FilterControllerEvent;
use Symfony\Component\HttpKernel\Event\GetResponseForControllerResultEvent ;
use Symfony\Component\HttpKernel\Event\FilterResponseEvent;
use Symfony\Component\HttpKernel\Event\FinishRequestEvent;
use Symfony\Component\HttpKernel\Event\PostResponseEvent;
use Symfony\Component\HttpKernel\Event\GetResponseForExceptionEvent;
use OldSound\RabbitMqBundle\RabbitMq\ProducerInterface;

use Carbon\Carbon;


class BzRmqConnector implements EventSubscriberInterface
{

    /**
     * @var
     */
    private $adminEventHook;
    private $producer;



    protected static  $inheritableClasses = ['BzParent'];
    protected static $inheritableClassesIDs = [];

    protected $inheritableContextFields  = ['domain_name','market_name','market_area_href','domain_href','merchant_href','listing_inherits_from','deal_inherits_from','event_inherits_from','audio_inherits_from',
        'video_inherits_from','article_inherits_from','news_inherits_from'
    ];

    /**
     * An array of fields that were inherited from a parent
     * Customise this per extending class
     * @var array
     */
    protected $inheritableFields = [];

    private $mqservice;

    public function __construct($content_update_service)
     {
         $this->mqservice = $content_update_service ;
     }

    public static function getSubscribedEvents()
     {
         return [
              // KernelEvents::REQUEST => 'onKernelRequest',
              // KernelEvents::CONTROLLER => 'onController',
              // KernelEvents::VIEW => 'onViewEvent',
              // KernelEvents::RESPONSE => 'onResponse',
              // KernelEvents::FINISH_REQUEST => 'onFinishRequest',
              // KernelEvents::TERMINATE => 'onTerminate',
              // KernelEvents::EXCEPTION => 'onException',
              //KernelEvents::TERMINATE => 'onSendingKernalTerminate',
              //KernelEvents::EXCEPTIONKernelEvents::EX => 'onSendingKernalException'

            DataObjectEvents::POST_ADD  => 'onPostAddDataObject',
            DataObjectEvents::POST_UPDATE => 'onPostUpdateDataObject',
            DataObjectEvents::POST_DELETE => 'onPostDeleteDataObject',
            DocumentEvents::POST_ADD => 'onPostAddDocument',
            DocumentEvents::POST_UPDATE => 'onPostUpdateDocument',
            DocumentEvents::POST_DELETE => 'onPostDeleteDocument',
            AssetEvents::POST_ADD => 'onPostAddAsset',
            AssetEvents::POST_UPDATE => 'onPostUpdateAsset',
            AssetEvents::POST_DELETE => 'onPostDeleteAsset'
         ];
     }

     public function onKernelRequest(GetResponseEvent $e){
       $data = $e->getRequest()->getRequestUri();
       //$results2 = print_r($data, true);
       // \Pimcore\Log\Simple::log("event.log", 'Inside BzEventListener onKernelRequest data='.$results2);
       return;
     }

     public function onController(FilterControllerEvent $e){
       $data = $e->getRequestType();
       //$results2 = print_r($data, true);
       // \Pimcore\Log\Simple::log("event.log", 'Inside BzEventListener onController data='.$results2);
       return;
     }

     public function onViewEvent(GetResponseForControllerResultEvent $e){
       $data = $e->hasResponse();
       //$results2 = print_r($data, true);
       // \Pimcore\Log\Simple::log("event.log", 'Inside BzEventListener onViewEvent data='.$results2);
       return;
     }

     public function onResponse(FilterResponseEvent $e){
       $data = $e->getResponse();
       //$results2 = print_r($data, true);
       //// \Pimcore\Log\Simple::log("event.log", 'Inside BzEventListener onResponse data='.$results2);
       return;
     }

     public function onFinishRequest(FinishRequestEvent $e){
       $data = $e->getRequestType();
       //$results2 = print_r($data, true);
       // \Pimcore\Log\Simple::log("event.log", 'Inside BzEventListener onFinishRequest data='.$results2);
       return;
     }

     public function onTerminate(PostResponseEvent $e){
       $data = $e->getRequestType();
       //$results2 = print_r($data, true);
       // \Pimcore\Log\Simple::log("event.log", 'Inside BzEventListener onTerminate data='.$results2);
       return;
     }

     public function onException(GetResponseForExceptionEvent $e){
       $data = $e->getException();
       //$results2 = print_r($data, true);
       // \Pimcore\Log\Simple::log("event.log", 'Inside BzEventListener onException data='.$results2);
       return;
     }

    public function handleObjectPublishEvent (DataObjectEvent $e, string $eventType ){


       //\Pimcore\Log\Simple::log("event.log", 'BzRmqConnector::handleObjectPublishEvent:: Start ' );
       //If it is folder event ignore it
       $data = $e->getArguments()['data'];
       $object = $e->getObject();

       //$results2 = print_r($object, true);
       //\Pimcore\Log\Simple::log("event.log", 'Inside BzRmqConnector data object ='.$results2);

       $instanceof_object = get_class ($object);
       //$results2 = print_r($instanceof_object, true);
       //\Pimcore\Log\Simple::log("event.log", 'Inside BzRmqConnector instanceof_object='.$results2);

       if ($object instanceof DataObjectFolder) {

         return;
       }else{
         \Pimcore\Log\Simple::log("event.log", 'Inside BzRmqConnector:: Object was not an instance of a data object');

       }


       $o_id = $object->getId();
       //$results2 = print_r($o_id, true);
       //\Pimcore\Log\Simple::log("event.log", 'Inside BzRmqConnector ::handleObjectPublishEvent:: o_id ='.$results2);

       //$name = $object->name;
       $name = $object->getName();
       $slug_lowercase = strtolower($name);
       //$slug = str_replace(" ", "-", $slug_lowercase);
       $slug = str_replace(str_split(' '), '-', $slug_lowercase).'-'.$o_id;
       $object_type = $object->getObject_type();
       //$results2 = print_r($object_type, true);
       //\Pimcore\Log\Simple::log("event.log", 'Inside BzRmqConnector object_type= '.$results2);

       $o_path = $object->getPath();
       $o_key = $object->getKey();
       $o_className = $object->getClassName();

       $o_path_array = explode("/", $o_path);
       $solution_id = $o_path_array[2];
       $object_type = $object->getObject_type();
       $server_name = $object->getProperty('server_name');
       $url1 = $server_name.strtolower("/".$solution_id. '/' .$o_className. '/' .$o_id);
       $url = strtolower($url1);


       try{
         $msg = array('user_id' => 1235, 'image_path' => '/path/to/new/pic.png');
         $message = [
           "id" => $o_id,
           "slug" => $slug,
           "url" => $url,
           "solutionid" => $solution_id,
           "object_type" => $object_type,
           "event" => $eventType
         ];
        //$this->producer->publish(serialize($message), 'mysorebuildersweb');
         $this->mqservice->publish(json_encode($message), $solution_id);
       }
       catch(Exception $e){
         //$error = $e->getException();
         \Pimcore\Log\Simple::log("event.log", 'BzRmqConnector:: Inside handleObjectPublishEvent:: catch ::Exception'.$e);
       }

      // $data = $e->getArguments()['data'];
      // $object = $e->getObject();
      // //$results2 = print_r($object, true);
      // //\Pimcore\Log\Simple::log("event.log", 'Inside BzConcrete data object '.$results2);
      //
      // // if (!$object instanceof BzConcrete){
          return;
      // // }
      //
      // $o_id = $object->o_id;
      // //$results2 = print_r($o_id, true);
      // // \Pimcore\Log\Simple::log("event.log", 'Inside BzEventListener o_id '.$results2);
      //
      // $o_className = $object->o_className;
      // $results2 = print_r($o_className, true);
      // \Pimcore\Log\Simple::log("event.log", 'Inside BzEventListener o_className '.$results2);
      //
      // if ($o_className == 'BzPublishQueue' || $o_className == NULL ){
      //   \Pimcore\Log\Simple::log("event.log", 'Inside BzEventListener o_className Returning o_className= '.$o_className);
      //   return;
      // }
      //
      // $o_path = $object->o_path;
      // $o_key = $object->o_key;
      // $o_path_array = explode("/", $o_path);
      // //$results2 = print_r($o_path_array, true);
      // // \Pimcore\Log\Simple::log("event.log", 'Inside BzEventListener o_path_array '.$results2);
      //
      // $solution_id = $o_path_array[2];
      //
      // $event_type = $eventType;
      // //$results2 = print_r($event_type, true);
      // // \Pimcore\Log\Simple::log("event.log", 'Inside BzEventListener event_type '.$results2);
      //
      // $event_file_prefix = strtolower($eventType);
      // //$results2 = print_r($event_file_prefix, true);
      // // \Pimcore\Log\Simple::log("event.log", 'Inside BzEventListener event_file_prefix '.$results2);
      //
      // $server_name = $object->getProperty('server_name');
      // $url = $server_name.strtolower( $solution_id. '/' .$o_className. '/' .$o_id);
      // //$url = strtolower($url);
      // $pobject = new DataObject\BzPublishQueue();
      // //$results2 = print_r($pobject, true);
      // //// \Pimcore\Log\Simple::log("event.log", 'Inside BzEventListener. pobject '.$results2);
      //
      // $dt = Carbon::now();
      // $dtstr = $dt->toDateTimeString();
      // $pobject->setPublishDateTime($dt);
      // $pobject->setKey(\Pimcore\File::getValidFilename($dtstr.'_'.$event_file_prefix.'_'.$o_id));
      // $parent_folder_path = "/system/publish_queue/";
      //
      // $parent_folder = DataObject::getByPath($parent_folder_path);
      // $parent_folder_id = $parent_folder->o_id;
      //
      // $pobject->setParentId($parent_folder_id);
      // $pobject->setClass_name($o_className);
      // $pobject->setObject_id($o_id);
      // $pobject->setObject_path($o_path);
      // $pobject->setObject_key($o_key);
      // $pobject->setSolution_id($solution_id);
      // $pobject->setEvent_type($event_type);
      // $pobject->setUrl($url);
      // $pobject->setIs_processed(false);
      // try{
      // $pobject->save_child_object(["versionNote" => "my new version"]);
      // ///$results2 = print_r($pobject, true);
      // // \Pimcore\Log\Simple::log("event.log", 'Inside BzEventListener pobject try');
      // }
      // catch(Exception $ex){
      //   \Pimcore\Log\Simple::log("event.log", 'Inside BzEventListener. Catch exception during save ex= ');
      //   $results2 = print_r($ex, true);
      // }
      // //return $e->response();
    }

    public function onPostAddDataObject (DataObjectEvent $e) {
      return $this->handleObjectPublishEvent($e, 'OBJECT::CREATE');
    }

    public function onPostUpdateDataObject (DataObjectEvent $e) {
      $is_event_propagation_stopped == $e->isPropagationStopped();
      return $this->handleObjectPublishEvent($e, 'OBJECT::UPDATE');
     }

    public function onPostDeleteDataObject (DataObjectEvent $e) {
        return $this->handleObjectPublishEvent($e, 'OBJECT::DELETE');
    }

    public function handleDocumentPublishEvent (DocumentEvent $e, string $eventType){
      \Pimcore\Log\Simple::log("event.log", 'BzRmqConnector::handleDocumentPublishEvent:: Start ' );

       $data = $e->getArguments()['data'];
       $document = $e->getDocument();
       //$results2 = print_r($document, true);
       //\Pimcore\Log\Simple::log("event.log", 'Inside BzRmqConnector doc '.$results2);

       $id = $document->getId();
       $results2 = print_r($id, true);
       \Pimcore\Log\Simple::log("event.log", 'Inside BzRmqConnector ::handleDocumentPublishEvent:: $id ='.$results2);

       $type = $document->getType();
       $path = $document->getPath();
       $key = $document->getKey();
       $o_path = ($path .$key);

       // $o_path_array = explode("/", $o_path);
       // $solution_id = $o_path_array[2];
       //
       // $event_type = $eventType;
       // $event_file_prefix = strtolower($eventType);
       $server_name = $document->getProperty('server_base');
       $url = $server_name.strtolower($o_path);
       $url = strtolower($url);

       try{
         $message = [
           "id" => $id,
           "url" => $url,
           "object_type" => 'pages'
         ];

         \Pimcore\Log\Simple::log("event.log", 'BzRmqConnector:: Inside handleDocumentPublishEvent:: Just before calling publish');

        try{
          $this->mqservice->publish(json_encode($message), 'builders_document_updates');
        }catch(Exception $e){
         \Pimcore\Log\Simple::log("event.log", 'BzRmqConnector:: Inside handleDocumentPublishEvent::mqservice::publish:: catch ::Exception'.$e);
       }

       }
       catch(Exception $e){
         //$error = $e->getException();
         \Pimcore\Log\Simple::log("event.log", 'BzRmqConnector:: Inside handleDocumentPublishEvent:: catch ::Exception'.$e);
       }


      // $id = $document->id;
      // $type = $document->type;
      // $path = $document->path;
      // $key = $document->key;
      // $o_path = ($path .$key);
      // //$o_className = $object->o_className;
      // //if ($o_className == 'BzPublishQueue' ){
      \Pimcore\Log\Simple::log("event.log", 'BzRmqConnector:: Inside handleDocumentPublishEvent:: Just before return');

         return;
      // //}
      // $o_path_array = explode("/", $o_path);
      // $solution_id = $o_path_array[2];
      //
      // $event_type = $eventType;
      // $event_file_prefix = strtolower($eventType);
      // $server_name = $document->getProperty('server_base');
      // $url = $server_name.strtolower($o_path);
      // //$url = strtolower($url);
      // $pdocument = new DataObject\BzPublishQueue();
      // $dt = Carbon::now();
      // $dtstr = $dt->toDateTimeString();
      // $pdocument->setPublishDateTime($dt);
      // $pdocument->setKey(\Pimcore\File::getValidFilename($dtstr.'_'.$event_file_prefix.'_'.$id));
      //
      // $parent_folder_path = "/system/publish_queue/";
      // $parent_folder = DataObject::getByPath($parent_folder_path);
      // $parent_folder_id = $parent_folder->o_id;
      // $pdocument->setParentId($parent_folder_id);
      //
      // $pdocument->setClass_name($type);
      // //$pdocument->setClass_name($o_className);
      // $pdocument->setObject_id($id);
      // $pdocument->setObject_path($o_path);
      // $pdocument->setObject_key($key);
      // $pdocument->setSolution_id($solution_id);
      // $pdocument->setEvent_type($event_type);
      // $pdocument->setUrl($url);
      // $pdocument->setIs_processed(false);
      // $pdocument->save_child_object(["versionNote" => "my new version"]);
    }

    public function onPostAddDocument (DocumentEvent $e){
      $this->handleDocumentPublishEvent ($e, 'DOCUMENT::CREATE');
    }

    public function onPostUpdateDocument (DocumentEvent $e){
      $this->handleDocumentPublishEvent ($e, 'DOCUMENT::UPDATE');
    }

    public function onPostDeleteDocument (DocumentEvent $e){
      $this->handleDocumentPublishEvent ($e, 'DOCUMENT::DELETE');
    }

    public function handleAssetPublishEvent (AssetEvent $e, string $eventType){
      \Pimcore\Log\Simple::log("event.log", 'BzRmqConnector::handleAssetPublishEvent:: Start ' );

      // $data = $e->getArguments();
      // $asset = $e->getAsset();
      //
      // $id = $asset->id;
      // $type = $asset->type;
      // $path = $asset->path;
      // $filename = $asset->filename;
      // $o_path = ($path .$filename);
      // //$o_className = $object->o_className;
      // //if ($o_className == 'BzPublishQueue' ){
         return;
      // //}
      // $o_path_array = explode("/", $o_path);
      // $solution_id = $o_path_array[2];
      // $event_type = $eventType;
      // $event_file_prefix = strtolower($eventType);
      // $server_name = $asset->getProperty('server_base');
      // $url = $server_name.strtolower($o_path);
      // //$url = strtolower($url);
      // $passet = new DataObject\BzPublishQueue();
      // $dt = Carbon::now();
      // $dtstr = $dt->toDateTimeString();
      // $passet->setPublishDateTime($dt);
      // $passet->setKey(\Pimcore\File::getValidFilename($dtstr.'_'.$event_file_prefix.'_'.$id));
      // $parent_folder_path = "/system/publish_queue/";
      // $parent_folder = DataObject::getByPath($parent_folder_path);
      // $parent_folder_id = $parent_folder->o_id;
      // $passet->setParentId($parent_folder_id);
      // $passet->setClass_name($type);
      // //$passet->setClass_name($o_className);
      // $passet->setObject_id($id);
      // $passet->setObject_path($o_path);
      // $passet->setObject_key($filename);
      // $passet->setSolution_id($solution_id);
      // $passet->setEvent_type($event_type);
      // $passet->setUrl($url);
      // $passet->setIs_processed(false);
      // $passet->save(["versionNote" => "my new version"]);
      //
    }

    public function onPostAddAsset (AssetEvent $e, string $eventType){
      $this->handleAssetPublishEvent ($e, 'ASSET::CREATE');
    }

    public function onPostUpdateAsset (AssetEvent $e, string $eventType){
      $this->handleAssetPublishEvent ($e, 'ASSET::UPDATE');
    }

    public function onPostDeleteAsset (AssetEvent $e, string $eventType){
      $this->handleAssetPublishEvent ($e, 'ASSET::DELETE');
    }


    public function onPreUpdateDataObject(DataObjectEvent $e){
      $data = $e;

      //$results2 = print_r($data, true);
      // \Pimcore\Log\Simple::log("event.log", 'Inside BzEventListener onPreUpdateDataObject data='.$results2);
      return;
    }

    public function onPreSendData (GenericEvent $e) {
      //$results2 = print_r($merchant_parent_folder, true);
      // \Pimcore\Log\Simple::log("event.log", 'Inside BzEventListener onPreSendData');

      $data = $e->getArguments()['data'];
      //$results2 = print_r($data, true);
      // \Pimcore\Log\Simple::log("event.log", 'Inside BzEventListener beginning data='.$results2);

      $object = $e->getArguments()['object'];
      //$results2 = print_r($object, true);
      //// \Pimcore\Log\Simple::log("event.log", 'Inside BzEventListener object '.$results2);

      if ($object instanceof BzConcrete) {
         // \Pimcore\Log\Simple::log("event.log", 'Inside BzEventListener ::Inside if Instance of BzConcrete');

          $inheritedFields = $object->getInheritedFields();
          $inherited_json = json_encode($inheritedFields);
          $results2 = print_r($inherited_json, true);
          // \Pimcore\Log\Simple::log("event.log", 'Inside BzEventListener inherited_json='.$results2);

          $results2 = print_r($inheritedFields, true);
          // \Pimcore\Log\Simple::log("event.log", 'Inside BzEventListener inheritedFields='.$results2);

         $inherited_domain_name = $inheritedFields['domain_name']['data'];
         $results2 = print_r($inherited_domain_name, true);
         // \Pimcore\Log\Simple::log("event.log", 'Inside BzEventListener inherited_domain_name='.$results2);

         $data['data']['domain_name'] = $inherited_domain_name;
         $results2 = print_r($data['data']['domain_name'], true);
         // \Pimcore\Log\Simple::log("event.log", 'Inside BzEventListener after setting data_domain_name='.$results2);

          $inherited_market_name = $inheritedFields->market_name->data;
          $results2 = print_r($inherited_market_name, true);
          // \Pimcore\Log\Simple::log("event.log", 'Inside BzEventListener inherited_market_name='.$results2);

          foreach($inheritedFields as $fieldName=>$editmodeData) {
            $results2 = print_r($fieldName, true);
            // \Pimcore\Log\Simple::log("event.log", 'Inside BzEventListener fieldName='.$results2);

            $results2 = print_r($editmodeData, true);
            // \Pimcore\Log\Simple::log("event.log", 'Inside BzEventListener editmodeData='.$results2);

              if (isset($data['metaData'][$fieldName])) {
                // \Pimcore\Log\Simple::log("event.log", 'Inside BzEventListener inside if=');
                  $data['metaData'][$fieldName] = $editmodeData['metaData'];
                  $results2 = print_r($data, true);
                  // \Pimcore\Log\Simple::log("event.log", 'Inside BzEventListener data='.$results2);
              }

              //add the value
              $data['data'][$fieldName] = $editmodeData['data'];
          }
      }else{
        // // \Pimcore\Log\Simple::log("event.log", 'Inside BzEventListener ::Inside else Instance of BzConcrete');

      }

      $e->setArgument('data', $data);

    }

}
