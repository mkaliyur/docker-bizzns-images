<?php

include(__DIR__ . "/../../pimcore/config/startup_cli.php");

use Pimcore\Model\DataObject;

 //\Pimcore\Log\Simple::log("event.log", 'Inside delete queue = ');

 $entries = new DataObject\BzPublishQueue\Listing();
 $entries->setUnpublished(true);
 $loadedQueueItems = $entries->load();

 foreach ($loadedQueueItems as $deleteAllQueueItem){
   $deleteAllQueueItem->delete();
 }

?>
