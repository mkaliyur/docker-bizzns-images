<?php

use Symfony\Component\DependencyInjection\Argument\RewindableGenerator;

// This file has been auto-generated by the Symfony Dependency Injection Component for internal use.
// Returns the public 'Pimcore\Workflow\Dumper\StateMachineGraphvizDumper' shared autowired service.

return $this->services['Pimcore\Workflow\Dumper\StateMachineGraphvizDumper'] = new \Pimcore\Workflow\Dumper\StateMachineGraphvizDumper(${($_ = isset($this->services['Pimcore\Workflow\Manager']) ? $this->services['Pimcore\Workflow\Manager'] : $this->load('getManager2Service.php')) && false ?: '_'});
