<?php

use Symfony\Component\DependencyInjection\Argument\RewindableGenerator;

// This file has been auto-generated by the Symfony Dependency Injection Component for internal use.
// Returns the public 'pimcore_ecommerce.payment_manager' shared autowired service.

return $this->services['pimcore_ecommerce.payment_manager'] = new \Pimcore\Bundle\EcommerceFrameworkBundle\PaymentManager\PaymentManager(new \Symfony\Component\DependencyInjection\ServiceLocator([]));
