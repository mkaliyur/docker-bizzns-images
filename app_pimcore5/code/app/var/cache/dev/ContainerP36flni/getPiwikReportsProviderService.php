<?php

use Symfony\Component\DependencyInjection\Argument\RewindableGenerator;

// This file has been auto-generated by the Symfony Dependency Injection Component for internal use.
// Returns the public 'Pimcore\Bundle\EcommerceFrameworkBundle\Reports\Piwik\PiwikReportsProvider' shared autowired service.

return $this->services['Pimcore\Bundle\EcommerceFrameworkBundle\Reports\Piwik\PiwikReportsProvider'] = new \Pimcore\Bundle\EcommerceFrameworkBundle\Reports\Piwik\PiwikReportsProvider(${($_ = isset($this->services['Pimcore\Analytics\SiteId\SiteIdProvider']) ? $this->services['Pimcore\Analytics\SiteId\SiteIdProvider'] : $this->getSiteIdProviderService()) && false ?: '_'}, ${($_ = isset($this->services['Pimcore\Analytics\Piwik\Config\Config']) ? $this->services['Pimcore\Analytics\Piwik\Config\Config'] : $this->load('getConfigService.php')) && false ?: '_'}, ${($_ = isset($this->services['Pimcore\Analytics\Piwik\WidgetBroker']) ? $this->services['Pimcore\Analytics\Piwik\WidgetBroker'] : $this->load('getWidgetBrokerService.php')) && false ?: '_'}, ${($_ = isset($this->services['Pimcore\Translation\Translator']) ? $this->services['Pimcore\Translation\Translator'] : $this->getTranslatorService()) && false ?: '_'});
