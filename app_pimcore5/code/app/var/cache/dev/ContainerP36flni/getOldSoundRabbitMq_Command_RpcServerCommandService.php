<?php

use Symfony\Component\DependencyInjection\Argument\RewindableGenerator;

// This file has been auto-generated by the Symfony Dependency Injection Component for internal use.
// Returns the private 'old_sound_rabbit_mq.command.rpc_server_command' shared service.

$this->services['old_sound_rabbit_mq.command.rpc_server_command'] = $instance = new \OldSound\RabbitMqBundle\Command\RpcServerCommand();

$instance->setName('rabbitmq:rpc-server');

return $instance;
