<?php

use Symfony\Component\DependencyInjection\Argument\RewindableGenerator;

// This file has been auto-generated by the Symfony Dependency Injection Component for internal use.
// Returns the public 'console.command.appbundle_command_merchantcreatorcommandc6' shared autowired service.

return $this->services['console.command.appbundle_command_merchantcreatorcommandc6'] = new \AppBundle\Command\MerchantCreatorCommandC6(${($_ = isset($this->services['pimcore.model.dataobject.service']) ? $this->services['pimcore.model.dataobject.service'] : ($this->services['pimcore.model.dataobject.service'] = new \Pimcore\Model\DataObject\Service())) && false ?: '_'});
