<?php

use Symfony\Component\DependencyInjection\Argument\RewindableGenerator;

// This file has been auto-generated by the Symfony Dependency Injection Component for internal use.
// Returns the public 'old_sound_rabbit_mq.mysore_builders_consumer' shared service.

$this->services['old_sound_rabbit_mq.mysore_builders_consumer'] = $instance = new \OldSound\RabbitMqBundle\RabbitMq\Consumer(${($_ = isset($this->services['old_sound_rabbit_mq.connection.default']) ? $this->services['old_sound_rabbit_mq.connection.default'] : $this->load('getOldSoundRabbitMq_Connection_DefaultService.php')) && false ?: '_'});

$instance->setExchangeOptions(['name' => 'exg_content_updates', 'type' => 'topic', 'passive' => false, 'durable' => true, 'auto_delete' => false, 'internal' => false, 'nowait' => false, 'declare' => true, 'arguments' => NULL, 'ticket' => NULL]);
$instance->setQueueOptions(['name' => 'qout_mysore_builders', 'routing_keys' => [0 => 'mysore_builders'], 'passive' => false, 'durable' => true, 'exclusive' => false, 'auto_delete' => false, 'nowait' => false, 'declare' => true, 'arguments' => NULL, 'ticket' => NULL]);
$instance->setCallback([0 => ${($_ = isset($this->services['test_class']) ? $this->services['test_class'] : $this->load('getTestClassService.php')) && false ?: '_'}, 1 => 'execute']);
$instance->setEventDispatcher(${($_ = isset($this->services['debug.event_dispatcher']) ? $this->services['debug.event_dispatcher'] : $this->getDebug_EventDispatcherService()) && false ?: '_'});

return $instance;
