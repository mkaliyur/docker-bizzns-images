<?php

use Symfony\Component\DependencyInjection\Argument\RewindableGenerator;

// This file has been auto-generated by the Symfony Dependency Injection Component for internal use.
// Returns the private 'service_locator.b5zram2' shared service.

return $this->services['service_locator.b5zram2'] = new \Symfony\Component\DependencyInjection\ServiceLocator(['actionsButtonService' => function () {
    $f = function (\Pimcore\Workflow\ActionsButtonService $v = null) { return $v; }; return $f(${($_ = isset($this->services['Pimcore\Workflow\ActionsButtonService']) ? $this->services['Pimcore\Workflow\ActionsButtonService'] : $this->load('getActionsButtonServiceService.php')) && false ?: '_'});
}, 'placeStatusInfo' => function () {
    $f = function (\Pimcore\Workflow\Place\StatusInfo $v = null) { return $v; }; return $f(${($_ = isset($this->services['Pimcore\Workflow\Place\StatusInfo']) ? $this->services['Pimcore\Workflow\Place\StatusInfo'] : $this->load('getStatusInfoService.php')) && false ?: '_'});
}, 'router' => function () {
    return ${($_ = isset($this->services['cmf_routing.router']) ? $this->services['cmf_routing.router'] : $this->getCmfRouting_RouterService()) && false ?: '_'};
}, 'workflowManager' => function () {
    $f = function (\Pimcore\Workflow\Manager $v = null) { return $v; }; return $f(${($_ = isset($this->services['Pimcore\Workflow\Manager']) ? $this->services['Pimcore\Workflow\Manager'] : $this->load('getManager2Service.php')) && false ?: '_'});
}]);
