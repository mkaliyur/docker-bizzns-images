<?php

use Symfony\Component\DependencyInjection\Argument\RewindableGenerator;

// This file has been auto-generated by the Symfony Dependency Injection Component for internal use.
// Returns the private 'Pimcore\Image\Optimizer\ZopflipngOptimizer' shared service.

return $this->services['Pimcore\Image\Optimizer\ZopflipngOptimizer'] = new \Pimcore\Image\Optimizer\ZopflipngOptimizer();
