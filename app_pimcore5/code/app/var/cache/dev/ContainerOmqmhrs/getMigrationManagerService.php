<?php

use Symfony\Component\DependencyInjection\Argument\RewindableGenerator;

// This file has been auto-generated by the Symfony Dependency Injection Component for internal use.
// Returns the public 'Pimcore\Migrations\MigrationManager' shared autowired service.

if ($lazyLoad) {
    return $this->services['Pimcore\Migrations\MigrationManager'] = $this->createProxy('MigrationManager_012d3a8', function () {
        return \MigrationManager_012d3a8::staticProxyConstructor(function (&$wrappedInstance, \ProxyManager\Proxy\LazyLoadingInterface $proxy) {
            $wrappedInstance = $this->load('getMigrationManagerService.php', false);

            $proxy->setProxyInitializer(null);

            return true;
        });
    });
}

return new \Pimcore\Migrations\MigrationManager(${($_ = isset($this->services['doctrine.dbal.default_connection']) ? $this->services['doctrine.dbal.default_connection'] : $this->getDoctrine_Dbal_DefaultConnectionService()) && false ?: '_'}, ${($_ = isset($this->services['Pimcore\Migrations\Configuration\ConfigurationFactory']) ? $this->services['Pimcore\Migrations\Configuration\ConfigurationFactory'] : $this->load('getConfigurationFactoryService.php')) && false ?: '_'});
