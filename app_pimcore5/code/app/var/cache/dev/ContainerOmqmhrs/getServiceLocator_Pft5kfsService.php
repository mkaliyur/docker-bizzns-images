<?php

use Symfony\Component\DependencyInjection\Argument\RewindableGenerator;

// This file has been auto-generated by the Symfony Dependency Injection Component for internal use.
// Returns the private 'service_locator.pft5kfs' shared service.

return $this->services['service_locator.pft5kfs'] = new \Symfony\Component\DependencyInjection\ServiceLocator(['documentRouteHandler' => function () {
    $f = function (\Pimcore\Routing\Dynamic\DocumentRouteHandler $v = null) { return $v; }; return $f(${($_ = isset($this->services['Pimcore\Routing\Dynamic\DocumentRouteHandler']) ? $this->services['Pimcore\Routing\Dynamic\DocumentRouteHandler'] : $this->getDocumentRouteHandlerService()) && false ?: '_'});
}]);
