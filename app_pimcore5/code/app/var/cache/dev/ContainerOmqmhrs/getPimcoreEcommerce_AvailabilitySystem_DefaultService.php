<?php

use Symfony\Component\DependencyInjection\Argument\RewindableGenerator;

// This file has been auto-generated by the Symfony Dependency Injection Component for internal use.
// Returns the private 'pimcore_ecommerce.availability_system.default' shared autowired service.

return $this->services['pimcore_ecommerce.availability_system.default'] = new \Pimcore\Bundle\EcommerceFrameworkBundle\AvailabilitySystem\AvailabilitySystem();
