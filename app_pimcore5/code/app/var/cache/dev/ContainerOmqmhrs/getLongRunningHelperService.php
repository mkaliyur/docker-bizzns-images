<?php

use Symfony\Component\DependencyInjection\Argument\RewindableGenerator;

// This file has been auto-generated by the Symfony Dependency Injection Component for internal use.
// Returns the public 'Pimcore\Helper\LongRunningHelper' shared autowired service.

$this->services['Pimcore\Helper\LongRunningHelper'] = $instance = new \Pimcore\Helper\LongRunningHelper(${($_ = isset($this->services['doctrine']) ? $this->services['doctrine'] : $this->getDoctrineService()) && false ?: '_'});

$instance->setLogger(${($_ = isset($this->services['monolog.logger.pimcore']) ? $this->services['monolog.logger.pimcore'] : $this->load('getMonolog_Logger_PimcoreService.php')) && false ?: '_'});

return $instance;
