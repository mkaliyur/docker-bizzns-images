<?php

use Symfony\Component\DependencyInjection\Argument\RewindableGenerator;

// This file has been auto-generated by the Symfony Dependency Injection Component for internal use.
// Returns the private 'old_sound_rabbit_mq.delete_command' shared service.

$this->services['old_sound_rabbit_mq.delete_command'] = $instance = new \OldSound\RabbitMqBundle\Command\DeleteCommand();

$instance->setName('rabbitmq:delete');

return $instance;
