<?php 

/** 
* Generated at: 2019-09-03T12:03:07+02:00
* Inheritance: no
* Variants: no
* Changed by: root (2)
* IP: ::1


Fields Summary: 
- object_type [input]
- solution_name [input]
*/ 


return Pimcore\Model\DataObject\ClassDefinition::__set_state(array(
   'id' => '7',
   'name' => 'BzPartnerListings',
   'description' => '',
   'creationDate' => 0,
   'modificationDate' => 1567504985,
   'userOwner' => 2,
   'userModification' => 2,
   'parentClass' => '',
   'listingParentClass' => '',
   'useTraits' => '',
   'listingUseTraits' => '',
   'encryption' => false,
   'encryptedTables' => 
  array (
  ),
   'allowInherit' => false,
   'allowVariants' => NULL,
   'showVariants' => false,
   'layoutDefinitions' => 
  Pimcore\Model\DataObject\ClassDefinition\Layout\Panel::__set_state(array(
     'fieldtype' => 'panel',
     'labelWidth' => 100,
     'layout' => NULL,
     'name' => 'pimcore_root',
     'type' => NULL,
     'region' => NULL,
     'title' => NULL,
     'width' => NULL,
     'height' => NULL,
     'collapsible' => false,
     'collapsed' => false,
     'bodyStyle' => NULL,
     'datatype' => 'layout',
     'permissions' => NULL,
     'childs' => 
    array (
      0 => 
      Pimcore\Model\DataObject\ClassDefinition\Data\Input::__set_state(array(
         'fieldtype' => 'input',
         'width' => NULL,
         'queryColumnType' => 'varchar',
         'columnType' => 'varchar',
         'columnLength' => 190,
         'phpdocType' => 'string',
         'regex' => '',
         'unique' => false,
         'showCharCount' => NULL,
         'name' => 'object_type',
         'title' => 'Object Type',
         'tooltip' => 'Object Type is used by front-end logic to create data files.
Data files will be created under folders with "Object Type" as folder  names.',
         'mandatory' => false,
         'noteditable' => false,
         'index' => false,
         'locked' => false,
         'style' => '',
         'permissions' => NULL,
         'datatype' => 'data',
         'relationType' => false,
         'invisible' => false,
         'visibleGridView' => false,
         'visibleSearch' => false,
      )),
      1 => 
      Pimcore\Model\DataObject\ClassDefinition\Data\Input::__set_state(array(
         'fieldtype' => 'input',
         'width' => NULL,
         'queryColumnType' => 'varchar',
         'columnType' => 'varchar',
         'columnLength' => 190,
         'phpdocType' => 'string',
         'regex' => '',
         'unique' => false,
         'showCharCount' => false,
         'name' => 'solution_name',
         'title' => 'Solution Name',
         'tooltip' => 'Solution Name field is mandatory.  Solution  Name should be equal to "Solution Key". And this is the _index in the json',
         'mandatory' => false,
         'noteditable' => false,
         'index' => false,
         'locked' => false,
         'style' => '',
         'permissions' => NULL,
         'datatype' => 'data',
         'relationType' => false,
         'invisible' => false,
         'visibleGridView' => false,
         'visibleSearch' => false,
      )),
    ),
     'locked' => false,
  )),
   'icon' => '',
   'previewUrl' => '',
   'group' => '',
   'showAppLoggerTab' => false,
   'linkGeneratorReference' => '',
   'propertyVisibility' => 
  array (
    'grid' => 
    array (
      'id' => true,
      'key' => false,
      'path' => true,
      'published' => true,
      'modificationDate' => true,
      'creationDate' => true,
    ),
    'search' => 
    array (
      'id' => true,
      'key' => false,
      'path' => true,
      'published' => true,
      'modificationDate' => true,
      'creationDate' => true,
    ),
  ),
   'dao' => NULL,
));
