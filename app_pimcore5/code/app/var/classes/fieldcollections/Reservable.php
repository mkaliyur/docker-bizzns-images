<?php 

/** 
* Generated at: 2019-06-04T11:50:25+02:00
* IP: ::1


Fields Summary: 
 - Reservable [structuredTable]
*/ 


return Pimcore\Model\DataObject\Fieldcollection\Definition::__set_state(array(
   'key' => 'Reservable',
   'parentClass' => '',
   'title' => '',
   'group' => '',
   'layoutDefinitions' => 
  Pimcore\Model\DataObject\ClassDefinition\Layout\Panel::__set_state(array(
     'fieldtype' => 'panel',
     'labelWidth' => 100,
     'layout' => NULL,
     'name' => NULL,
     'type' => NULL,
     'region' => NULL,
     'title' => NULL,
     'width' => NULL,
     'height' => NULL,
     'collapsible' => false,
     'collapsed' => false,
     'bodyStyle' => NULL,
     'datatype' => 'layout',
     'permissions' => NULL,
     'childs' => 
    array (
      0 => 
      Pimcore\Model\DataObject\ClassDefinition\Layout\Panel::__set_state(array(
         'fieldtype' => 'panel',
         'labelWidth' => 100,
         'layout' => NULL,
         'name' => 'Layout',
         'type' => NULL,
         'region' => NULL,
         'title' => '',
         'width' => NULL,
         'height' => NULL,
         'collapsible' => false,
         'collapsed' => false,
         'bodyStyle' => '',
         'datatype' => 'layout',
         'permissions' => NULL,
         'childs' => 
        array (
          0 => 
          Pimcore\Model\DataObject\ClassDefinition\Data\StructuredTable::__set_state(array(
             'fieldtype' => 'structuredTable',
             'width' => '',
             'height' => '',
             'labelWidth' => NULL,
             'labelFirstCell' => '',
             'cols' => 
            array (
              0 => 
              array (
                'position' => 1,
                'key' => 'available',
                'label' => 'Is Available?',
                'type' => 'bool',
                'id' => 'extModel4259-1',
                'length' => NULL,
              ),
              1 => 
              array (
                'type' => 'bool',
                'position' => 2,
                'key' => 'notavailable',
                'id' => 'extModel4259-2',
                'label' => 'Not Available',
              ),
              2 => 
              array (
                'type' => 'bool',
                'position' => 3,
                'key' => 'onhold',
                'id' => 'extModel4259-3',
                'length' => NULL,
                'label' => 'On Hold',
              ),
              3 => 
              array (
                'type' => 'bool',
                'position' => 4,
                'key' => 'booked',
                'id' => 'extModel4259-4',
                'label' => 'Booked',
              ),
              4 => 
              array (
                'type' => 'bool',
                'position' => 5,
                'key' => 'reserved',
                'id' => 'extModel4259-5',
                'label' => 'Reserved',
              ),
            ),
             'rows' => 
            array (
              0 => 
              array (
                'position' => 1,
                'key' => 'bzshop',
                'label' => 'Bz Shop',
                'id' => 'extModel4235-1',
              ),
              1 => 
              array (
                'position' => 2,
                'key' => 'bzslot',
                'id' => 'extModel4235-2',
                'label' => 'Bz Slot',
              ),
              2 => 
              array (
                'position' => 3,
                'key' => 'bzseat',
                'id' => 'extModel4235-3',
                'label' => 'Bz Seat',
              ),
              3 => 
              array (
                'position' => 4,
                'key' => 'bztable',
                'id' => 'extModel4235-4',
                'label' => 'Bz Table',
              ),
              4 => 
              array (
                'position' => 5,
                'key' => 'bzparking',
                'id' => 'extModel4235-9',
                'label' => 'Bz Parking',
              ),
            ),
             'phpdocType' => '\\Pimcore\\Model\\DataObject\\Data\\StructuredTable',
             'name' => 'Reservable',
             'title' => 'Reservable',
             'tooltip' => '',
             'mandatory' => false,
             'noteditable' => false,
             'index' => false,
             'locked' => false,
             'style' => '',
             'permissions' => NULL,
             'datatype' => 'data',
             'relationType' => false,
             'invisible' => false,
             'visibleGridView' => false,
             'visibleSearch' => false,
          )),
        ),
         'locked' => false,
      )),
    ),
     'locked' => false,
  )),
   'dao' => NULL,
));
