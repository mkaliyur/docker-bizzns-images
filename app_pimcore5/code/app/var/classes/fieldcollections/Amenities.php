<?php 

/** 
* Generated at: 2019-04-30T14:43:01+02:00
* IP: ::1


Fields Summary: 
 - amenities [structuredTable]
*/ 


return Pimcore\Model\DataObject\Fieldcollection\Definition::__set_state(array(
   'key' => 'Amenities',
   'parentClass' => '',
   'title' => '',
   'group' => '',
   'layoutDefinitions' => 
  Pimcore\Model\DataObject\ClassDefinition\Layout\Panel::__set_state(array(
     'fieldtype' => 'panel',
     'labelWidth' => 100,
     'layout' => NULL,
     'name' => NULL,
     'type' => NULL,
     'region' => NULL,
     'title' => NULL,
     'width' => NULL,
     'height' => NULL,
     'collapsible' => false,
     'collapsed' => false,
     'bodyStyle' => NULL,
     'datatype' => 'layout',
     'permissions' => NULL,
     'childs' => 
    array (
      0 => 
      Pimcore\Model\DataObject\ClassDefinition\Layout\Panel::__set_state(array(
         'fieldtype' => 'panel',
         'labelWidth' => 100,
         'layout' => NULL,
         'name' => 'Layout',
         'type' => NULL,
         'region' => NULL,
         'title' => '',
         'width' => NULL,
         'height' => NULL,
         'collapsible' => false,
         'collapsed' => false,
         'bodyStyle' => '',
         'datatype' => 'layout',
         'permissions' => NULL,
         'childs' => 
        array (
          0 => 
          Pimcore\Model\DataObject\ClassDefinition\Data\StructuredTable::__set_state(array(
             'fieldtype' => 'structuredTable',
             'width' => '',
             'height' => '',
             'labelWidth' => NULL,
             'labelFirstCell' => '',
             'cols' => 
            array (
              0 => 
              array (
                'position' => 1,
                'key' => 'available',
                'label' => 'Is Available?',
                'type' => 'bool',
                'id' => 'extModel3687-1',
                'length' => NULL,
              ),
            ),
             'rows' => 
            array (
              0 => 
              array (
                'position' => 1,
                'key' => 'swimmingpool',
                'label' => 'Swimming Pool',
                'id' => 'extModel3663-1',
              ),
              1 => 
              array (
                'position' => 2,
                'key' => 'garden',
                'id' => 'extModel3881-1',
                'label' => 'Garden',
              ),
              2 => 
              array (
                'position' => 3,
                'key' => 'indoorgames',
                'id' => 'extModel3881-2',
                'label' => 'Indoor Games',
              ),
              3 => 
              array (
                'position' => 4,
                'key' => 'amphitheatre',
                'id' => 'extModel3881-3',
                'label' => 'Amphi Theatre',
              ),
              4 => 
              array (
                'position' => 5,
                'key' => 'clubhouse',
                'id' => 'extModel3881-4',
                'label' => 'Club House',
              ),
              5 => 
              array (
                'position' => 6,
                'key' => 'parking',
                'id' => 'extModel3881-5',
                'label' => 'Parking',
              ),
            ),
             'phpdocType' => '\\Pimcore\\Model\\DataObject\\Data\\StructuredTable',
             'name' => 'amenities',
             'title' => 'amenities',
             'tooltip' => '',
             'mandatory' => false,
             'noteditable' => false,
             'index' => false,
             'locked' => false,
             'style' => '',
             'permissions' => NULL,
             'datatype' => 'data',
             'relationType' => false,
             'invisible' => false,
             'visibleGridView' => false,
             'visibleSearch' => false,
          )),
        ),
         'locked' => false,
      )),
    ),
     'locked' => false,
  )),
   'dao' => NULL,
));
