<?php 

/** 
* Generated at: 2019-03-06T12:59:11+01:00


Fields Summary: 
 - RestaurantFeatureList [structuredTable]
*/ 


return Pimcore\Model\DataObject\Fieldcollection\Definition::__set_state(array(
   'key' => 'RestaurantFeatureList',
   'parentClass' => '',
   'title' => NULL,
   'group' => NULL,
   'layoutDefinitions' => 
  Pimcore\Model\DataObject\ClassDefinition\Layout\Panel::__set_state(array(
     'fieldtype' => 'panel',
     'labelWidth' => 100,
     'layout' => NULL,
     'name' => NULL,
     'type' => NULL,
     'region' => NULL,
     'title' => NULL,
     'width' => NULL,
     'height' => NULL,
     'collapsible' => false,
     'collapsed' => false,
     'bodyStyle' => NULL,
     'datatype' => 'layout',
     'permissions' => NULL,
     'childs' => 
    array (
      0 => 
      Pimcore\Model\DataObject\ClassDefinition\Layout\Panel::__set_state(array(
         'fieldtype' => 'panel',
         'labelWidth' => 100,
         'layout' => NULL,
         'name' => 'RestaurantFeatureList',
         'type' => NULL,
         'region' => NULL,
         'title' => 'Restaurant  FeatureList',
         'width' => NULL,
         'height' => NULL,
         'collapsible' => false,
         'collapsed' => false,
         'bodyStyle' => '',
         'datatype' => 'layout',
         'permissions' => NULL,
         'childs' => 
        array (
          0 => 
          Pimcore\Model\DataObject\ClassDefinition\Data\StructuredTable::__set_state(array(
             'fieldtype' => 'structuredTable',
             'width' => '',
             'height' => '',
             'labelWidth' => NULL,
             'labelFirstCell' => '',
             'cols' => 
            array (
              0 => 
              array (
                'position' => 1,
                'key' => 'available',
                'label' => 'Available?',
                'type' => 'bool',
                'id' => 'extModel9463-1',
              ),
            ),
             'rows' => 
            array (
              0 => 
              array (
                'position' => 1,
                'key' => 'ac_dining_hall',
                'label' => 'AC Dining Hall',
                'id' => 'extModel9439-1',
              ),
              1 => 
              array (
                'position' => 2,
                'key' => 'free_parking',
                'id' => 'extModel9439-2',
                'label' => 'Free Parking',
              ),
            ),
             'phpdocType' => '\\Pimcore\\Model\\DataObject\\Data\\StructuredTable',
             'name' => 'RestaurantFeatureList',
             'title' => 'Restaurant FeatureList',
             'tooltip' => '',
             'mandatory' => false,
             'noteditable' => false,
             'index' => false,
             'locked' => false,
             'style' => '',
             'permissions' => NULL,
             'datatype' => 'data',
             'relationType' => false,
             'invisible' => false,
             'visibleGridView' => false,
             'visibleSearch' => false,
          )),
        ),
         'locked' => false,
      )),
    ),
     'locked' => false,
  )),
   'dao' => NULL,
));
