<?php 

/** 
* Generated at: 2019-03-06T12:59:11+01:00


Fields Summary: 
 - hotel_featurelist [structuredTable]
*/ 


return Pimcore\Model\DataObject\Fieldcollection\Definition::__set_state(array(
   'key' => 'HotelFeatureList',
   'parentClass' => '',
   'title' => NULL,
   'group' => NULL,
   'layoutDefinitions' => 
  Pimcore\Model\DataObject\ClassDefinition\Layout\Panel::__set_state(array(
     'fieldtype' => 'panel',
     'labelWidth' => 100,
     'layout' => NULL,
     'name' => NULL,
     'type' => NULL,
     'region' => NULL,
     'title' => NULL,
     'width' => NULL,
     'height' => NULL,
     'collapsible' => false,
     'collapsed' => false,
     'bodyStyle' => NULL,
     'datatype' => 'layout',
     'permissions' => NULL,
     'childs' => 
    array (
      0 => 
      Pimcore\Model\DataObject\ClassDefinition\Layout\Panel::__set_state(array(
         'fieldtype' => 'panel',
         'labelWidth' => 100,
         'layout' => NULL,
         'name' => 'Layout',
         'type' => NULL,
         'region' => NULL,
         'title' => NULL,
         'width' => NULL,
         'height' => NULL,
         'collapsible' => false,
         'collapsed' => false,
         'bodyStyle' => NULL,
         'datatype' => 'layout',
         'permissions' => NULL,
         'childs' => 
        array (
          0 => 
          Pimcore\Model\DataObject\ClassDefinition\Data\StructuredTable::__set_state(array(
             'fieldtype' => 'structuredTable',
             'width' => '',
             'height' => '',
             'labelWidth' => NULL,
             'labelFirstCell' => '',
             'cols' => 
            array (
              0 => 
              array (
                'position' => 1,
                'key' => 'available',
                'label' => 'Is Available?',
                'type' => 'bool',
                'id' => 'extModel476-1',
              ),
            ),
             'rows' => 
            array (
              0 => 
              array (
                'position' => 1,
                'key' => 'airconditioning',
                'label' => 'Air Conditioning',
                'id' => 'extModel452-1',
              ),
              1 => 
              array (
                'position' => 2,
                'key' => 'balcony',
                'id' => 'extModel452-2',
                'label' => 'Balcony',
              ),
              2 => 
              array (
                'position' => 3,
                'key' => 'bedding',
                'id' => 'extModel452-3',
                'label' => 'Bedding',
              ),
              3 => 
              array (
                'position' => 4,
                'key' => 'cabletv',
                'id' => 'extModel452-4',
                'label' => 'Cable TV',
              ),
              4 => 
              array (
                'position' => 5,
                'key' => 'cleaningafterexit',
                'id' => 'extModel452-5',
                'label' => 'Cleaning After Exit',
              ),
              5 => 
              array (
                'position' => 6,
                'key' => 'coffeepot',
                'id' => 'extModel452-6',
                'label' => 'Coffee Pot',
              ),
              6 => 
              array (
                'position' => 7,
                'key' => 'computer',
                'id' => 'extModel452-7',
                'label' => 'Computer',
              ),
              7 => 
              array (
                'position' => 8,
                'key' => 'cot',
                'id' => 'extModel452-8',
                'label' => 'Cot',
              ),
              8 => 
              array (
                'position' => 9,
                'key' => 'dishwasher',
                'id' => 'extModel452-9',
                'label' => 'Dishwasher',
              ),
            ),
             'phpdocType' => '\\Pimcore\\Model\\DataObject\\Data\\StructuredTable',
             'name' => 'hotel_featurelist',
             'title' => 'Hotel Featurelist',
             'tooltip' => '',
             'mandatory' => false,
             'noteditable' => false,
             'index' => false,
             'locked' => false,
             'style' => '',
             'permissions' => NULL,
             'datatype' => 'data',
             'relationType' => false,
             'invisible' => false,
             'visibleGridView' => false,
             'visibleSearch' => false,
          )),
        ),
         'locked' => false,
      )),
    ),
     'locked' => false,
  )),
   'dao' => NULL,
));
