<?php 

namespace Pimcore\Model\DataObject\BzSystemComponentCronJobs;

use Pimcore\Model\DataObject;

/**
 * @method DataObject\BzSystemComponentCronJobs current()
 * @method DataObject\BzSystemComponentCronJobs[] load()
 */

class Listing extends DataObject\Listing\Concrete {

protected $classId = "46";
protected $className = "BzSystemComponentCronJobs";


}
