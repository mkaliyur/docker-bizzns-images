<?php 

namespace Pimcore\Model\DataObject\BzListingTravels;

use Pimcore\Model\DataObject;

/**
 * @method DataObject\BzListingTravels current()
 * @method DataObject\BzListingTravels[] load()
 */

class Listing extends DataObject\Listing\Concrete {

protected $classId = "49";
protected $className = "BzListingTravels";


}
