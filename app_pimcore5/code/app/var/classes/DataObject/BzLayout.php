<?php 

/** 
* Generated at: 2019-09-03T10:26:34+02:00
* Inheritance: yes
* Variants: no
* Changed by: root (2)
* IP: ::1


Fields Summary: 
- name [input]
- selector [input]
- settings [fieldcollections]
- object_type [input]
- solution_name [input]
*/ 

namespace Pimcore\Model\DataObject;



/**
* @method static \Pimcore\Model\DataObject\BzLayout\Listing getByName ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\BzLayout\Listing getBySelector ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\BzLayout\Listing getBySettings ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\BzLayout\Listing getByObject_type ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\BzLayout\Listing getBySolution_name ($value, $limit = 0) 
*/

class BzLayout extends Concrete implements \Pimcore\Model\DataObject\DirtyIndicatorInterface {



use \Pimcore\Model\DataObject\Traits\DirtyIndicatorTrait;

protected $o_classId = "32";
protected $o_className = "BzLayout";
protected $name;
protected $selector;
protected $settings;
protected $object_type;
protected $solution_name;


/**
* @param array $values
* @return \Pimcore\Model\DataObject\BzLayout
*/
public static function create($values = array()) {
	$object = new static();
	$object->setValues($values);
	return $object;
}

/**
* Get name - Name
* @return string
*/
public function getName () {
	$preValue = $this->preGetValue("name"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->name;
	if(\Pimcore\Model\DataObject::doGetInheritedValues() && $this->getClass()->getFieldDefinition("name")->isEmpty($data)) {
		return $this->getValueFromParent("name");
	}
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set name - Name
* @param string $name
* @return \Pimcore\Model\DataObject\BzLayout
*/
public function setName ($name) {
	$fd = $this->getClass()->getFieldDefinition("name");
	$this->name = $name;
	return $this;
}

/**
* Get selector - selector
* @return string
*/
public function getSelector () {
	$preValue = $this->preGetValue("selector"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->selector;
	if(\Pimcore\Model\DataObject::doGetInheritedValues() && $this->getClass()->getFieldDefinition("selector")->isEmpty($data)) {
		return $this->getValueFromParent("selector");
	}
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set selector - selector
* @param string $selector
* @return \Pimcore\Model\DataObject\BzLayout
*/
public function setSelector ($selector) {
	$fd = $this->getClass()->getFieldDefinition("selector");
	$this->selector = $selector;
	return $this;
}

/**
* @return \Pimcore\Model\DataObject\Fieldcollection
*/
public function getSettings () {
	$preValue = $this->preGetValue("settings"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { return $preValue;}
	$data = $this->getClass()->getFieldDefinition("settings")->preGetData($this);
	 return $data;
}

/**
* Set settings - settings
* @param \Pimcore\Model\DataObject\Fieldcollection $settings
* @return \Pimcore\Model\DataObject\BzLayout
*/
public function setSettings ($settings) {
	$fd = $this->getClass()->getFieldDefinition("settings");
	$this->settings = $fd->preSetData($this, $settings);
	return $this;
}

/**
* Get object_type - Object Type
* @return string
*/
public function getObject_type () {
	$preValue = $this->preGetValue("object_type"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->object_type;
	if(\Pimcore\Model\DataObject::doGetInheritedValues() && $this->getClass()->getFieldDefinition("object_type")->isEmpty($data)) {
		return $this->getValueFromParent("object_type");
	}
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set object_type - Object Type
* @param string $object_type
* @return \Pimcore\Model\DataObject\BzLayout
*/
public function setObject_type ($object_type) {
	$fd = $this->getClass()->getFieldDefinition("object_type");
	$this->object_type = $object_type;
	return $this;
}

/**
* Get solution_name - Solution Name
* @return string
*/
public function getSolution_name () {
	$preValue = $this->preGetValue("solution_name"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->solution_name;
	if(\Pimcore\Model\DataObject::doGetInheritedValues() && $this->getClass()->getFieldDefinition("solution_name")->isEmpty($data)) {
		return $this->getValueFromParent("solution_name");
	}
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set solution_name - Solution Name
* @param string $solution_name
* @return \Pimcore\Model\DataObject\BzLayout
*/
public function setSolution_name ($solution_name) {
	$fd = $this->getClass()->getFieldDefinition("solution_name");
	$this->solution_name = $solution_name;
	return $this;
}

protected static $_relationFields = array (
);

protected $lazyLoadedFields = array (
);

}

