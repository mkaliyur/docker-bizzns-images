<?php 

/** 
* Generated at: 2019-09-03T12:02:42+02:00
* Inheritance: no
* Variants: no
* Changed by: root (2)
* IP: ::1


Fields Summary: 
- name [input]
- parentNewsCategory [manyToOneRelation]
- object_type [input]
- solution_name [input]
*/ 

namespace Pimcore\Model\DataObject;



/**
* @method static \Pimcore\Model\DataObject\BzNewsCategory\Listing getByName ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\BzNewsCategory\Listing getByParentNewsCategory ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\BzNewsCategory\Listing getByObject_type ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\BzNewsCategory\Listing getBySolution_name ($value, $limit = 0) 
*/

class BzNewsCategory extends Concrete implements \Pimcore\Model\DataObject\DirtyIndicatorInterface {



use \Pimcore\Model\DataObject\Traits\DirtyIndicatorTrait;

protected $o_classId = "40";
protected $o_className = "BzNewsCategory";
protected $name;
protected $parentNewsCategory;
protected $object_type;
protected $solution_name;


/**
* @param array $values
* @return \Pimcore\Model\DataObject\BzNewsCategory
*/
public static function create($values = array()) {
	$object = new static();
	$object->setValues($values);
	return $object;
}

/**
* Get name - Name
* @return string
*/
public function getName () {
	$preValue = $this->preGetValue("name"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->name;
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set name - Name
* @param string $name
* @return \Pimcore\Model\DataObject\BzNewsCategory
*/
public function setName ($name) {
	$fd = $this->getClass()->getFieldDefinition("name");
	$this->name = $name;
	return $this;
}

/**
* Get parentNewsCategory - Parent News Category
* @return \Pimcore\Model\DataObject\BzNewsCategory
*/
public function getParentNewsCategory () {
	$preValue = $this->preGetValue("parentNewsCategory"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->getClass()->getFieldDefinition("parentNewsCategory")->preGetData($this);
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set parentNewsCategory - Parent News Category
* @param \Pimcore\Model\DataObject\BzNewsCategory $parentNewsCategory
* @return \Pimcore\Model\DataObject\BzNewsCategory
*/
public function setParentNewsCategory ($parentNewsCategory) {
	$fd = $this->getClass()->getFieldDefinition("parentNewsCategory");
	$currentData = $this->getParentNewsCategory();
	$isEqual = $fd->isEqual($currentData, $parentNewsCategory);
	if (!$isEqual) {
		$this->markFieldDirty("parentNewsCategory", true);
	}
	$this->parentNewsCategory = $fd->preSetData($this, $parentNewsCategory);
	return $this;
}

/**
* Get object_type - Object Type
* @return string
*/
public function getObject_type () {
	$preValue = $this->preGetValue("object_type"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->object_type;
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set object_type - Object Type
* @param string $object_type
* @return \Pimcore\Model\DataObject\BzNewsCategory
*/
public function setObject_type ($object_type) {
	$fd = $this->getClass()->getFieldDefinition("object_type");
	$this->object_type = $object_type;
	return $this;
}

/**
* Get solution_name - Solution Name
* @return string
*/
public function getSolution_name () {
	$preValue = $this->preGetValue("solution_name"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->solution_name;
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set solution_name - Solution Name
* @param string $solution_name
* @return \Pimcore\Model\DataObject\BzNewsCategory
*/
public function setSolution_name ($solution_name) {
	$fd = $this->getClass()->getFieldDefinition("solution_name");
	$this->solution_name = $solution_name;
	return $this;
}

protected static $_relationFields = array (
  'parentNewsCategory' => 
  array (
    'type' => 'manyToOneRelation',
  ),
);

protected $lazyLoadedFields = array (
  0 => 'parentNewsCategory',
);

}

