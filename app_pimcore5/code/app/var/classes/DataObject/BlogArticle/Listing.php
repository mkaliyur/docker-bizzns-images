<?php 

namespace Pimcore\Model\DataObject\BlogArticle;

use Pimcore\Model\DataObject;

/**
 * @method DataObject\BlogArticle current()
 * @method DataObject\BlogArticle[] load()
 */

class Listing extends DataObject\Listing\Concrete {

protected $classId = "5";
protected $className = "blogArticle";


}
