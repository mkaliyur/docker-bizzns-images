<?php 

namespace Pimcore\Model\DataObject\BzBlogTags;

use Pimcore\Model\DataObject;

/**
 * @method DataObject\BzBlogTags current()
 * @method DataObject\BzBlogTags[] load()
 */

class Listing extends DataObject\Listing\Concrete {

protected $classId = "39";
protected $className = "BzBlogTags";


}
