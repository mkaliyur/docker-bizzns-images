<?php 

namespace Pimcore\Model\DataObject\BzPublishQueue;

use Pimcore\Model\DataObject;

/**
 * @method DataObject\BzPublishQueue current()
 * @method DataObject\BzPublishQueue[] load()
 */

class Listing extends DataObject\Listing\Concrete {

protected $classId = "33";
protected $className = "BzPublishQueue";


}
