<?php 

/** 
* Generated at: 2019-08-12T12:14:28+02:00
* Inheritance: no
* Variants: no
* Changed by: root (2)
* IP: ::1


Fields Summary: 
- name [input]
- briefDesc [textarea]
- datatype_href [manyToManyRelation]
- marketarea_href [manyToManyRelation]
- object_type [input]
- bz_solutions [manyToManyRelation]
*/ 

namespace Pimcore\Model\DataObject;



/**
* @method static \Pimcore\Model\DataObject\BzSystem\Listing getByName ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\BzSystem\Listing getByBriefDesc ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\BzSystem\Listing getByDatatype_href ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\BzSystem\Listing getByMarketarea_href ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\BzSystem\Listing getByObject_type ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\BzSystem\Listing getByBz_solutions ($value, $limit = 0) 
*/

class BzSystem extends Concrete implements \Pimcore\Model\DataObject\DirtyIndicatorInterface {



use \Pimcore\Model\DataObject\Traits\DirtyIndicatorTrait;

protected $o_classId = "27";
protected $o_className = "BzSystem";
protected $name;
protected $briefDesc;
protected $datatype_href;
protected $marketarea_href;
protected $object_type;
protected $bz_solutions;


/**
* @param array $values
* @return \Pimcore\Model\DataObject\BzSystem
*/
public static function create($values = array()) {
	$object = new static();
	$object->setValues($values);
	return $object;
}

/**
* Get name - name
* @return string
*/
public function getName () {
	$preValue = $this->preGetValue("name"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->name;
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set name - name
* @param string $name
* @return \Pimcore\Model\DataObject\BzSystem
*/
public function setName ($name) {
	$fd = $this->getClass()->getFieldDefinition("name");
	$this->name = $name;
	return $this;
}

/**
* Get briefDesc - Brief Description
* @return string
*/
public function getBriefDesc () {
	$preValue = $this->preGetValue("briefDesc"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->briefDesc;
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set briefDesc - Brief Description
* @param string $briefDesc
* @return \Pimcore\Model\DataObject\BzSystem
*/
public function setBriefDesc ($briefDesc) {
	$fd = $this->getClass()->getFieldDefinition("briefDesc");
	$this->briefDesc = $briefDesc;
	return $this;
}

/**
* Get datatype_href - Datatype Href
* @return \Pimcore\Model\DataObject\BzDatatypes[]
*/
public function getDatatype_href () {
	$preValue = $this->preGetValue("datatype_href"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->getClass()->getFieldDefinition("datatype_href")->preGetData($this);
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set datatype_href - Datatype Href
* @param \Pimcore\Model\DataObject\BzDatatypes[] $datatype_href
* @return \Pimcore\Model\DataObject\BzSystem
*/
public function setDatatype_href ($datatype_href) {
	$fd = $this->getClass()->getFieldDefinition("datatype_href");
	$currentData = $this->getDatatype_href();
	$isEqual = $fd->isEqual($currentData, $datatype_href);
	if (!$isEqual) {
		$this->markFieldDirty("datatype_href", true);
	}
	$this->datatype_href = $fd->preSetData($this, $datatype_href);
	return $this;
}

/**
* Get marketarea_href - Marketarea Href
* @return \Pimcore\Model\DataObject\BzMarketArea[]
*/
public function getMarketarea_href () {
	$preValue = $this->preGetValue("marketarea_href"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->getClass()->getFieldDefinition("marketarea_href")->preGetData($this);
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set marketarea_href - Marketarea Href
* @param \Pimcore\Model\DataObject\BzMarketArea[] $marketarea_href
* @return \Pimcore\Model\DataObject\BzSystem
*/
public function setMarketarea_href ($marketarea_href) {
	$fd = $this->getClass()->getFieldDefinition("marketarea_href");
	$currentData = $this->getMarketarea_href();
	$isEqual = $fd->isEqual($currentData, $marketarea_href);
	if (!$isEqual) {
		$this->markFieldDirty("marketarea_href", true);
	}
	$this->marketarea_href = $fd->preSetData($this, $marketarea_href);
	return $this;
}

/**
* Get object_type - Object Type
* @return string
*/
public function getObject_type () {
	$preValue = $this->preGetValue("object_type"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->object_type;
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set object_type - Object Type
* @param string $object_type
* @return \Pimcore\Model\DataObject\BzSystem
*/
public function setObject_type ($object_type) {
	$fd = $this->getClass()->getFieldDefinition("object_type");
	$this->object_type = $object_type;
	return $this;
}

/**
* Get bz_solutions - bz_solutions
* @return \Pimcore\Model\DataObject\BzSolutions[]
*/
public function getBz_solutions () {
	$preValue = $this->preGetValue("bz_solutions"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->getClass()->getFieldDefinition("bz_solutions")->preGetData($this);
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set bz_solutions - bz_solutions
* @param \Pimcore\Model\DataObject\BzSolutions[] $bz_solutions
* @return \Pimcore\Model\DataObject\BzSystem
*/
public function setBz_solutions ($bz_solutions) {
	$fd = $this->getClass()->getFieldDefinition("bz_solutions");
	$currentData = $this->getBz_solutions();
	$isEqual = $fd->isEqual($currentData, $bz_solutions);
	if (!$isEqual) {
		$this->markFieldDirty("bz_solutions", true);
	}
	$this->bz_solutions = $fd->preSetData($this, $bz_solutions);
	return $this;
}

protected static $_relationFields = array (
  'datatype_href' => 
  array (
    'type' => 'manyToManyRelation',
  ),
  'marketarea_href' => 
  array (
    'type' => 'manyToManyRelation',
  ),
  'bz_solutions' => 
  array (
    'type' => 'manyToManyRelation',
  ),
);

protected $lazyLoadedFields = array (
  0 => 'bz_solutions',
);

}

