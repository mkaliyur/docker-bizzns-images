<?php 

namespace Pimcore\Model\DataObject\BzListing2;

use Pimcore\Model\DataObject;

/**
 * @method DataObject\BzListing2 current()
 * @method DataObject\BzListing2[] load()
 */

class Listing extends DataObject\Listing\Concrete {

protected $classId = "18";
protected $className = "BzListing2";


}
