<?php 

namespace Pimcore\Model\DataObject\BzDeals;

use Pimcore\Model\DataObject;

/**
 * @method DataObject\BzDeals current()
 * @method DataObject\BzDeals[] load()
 */

class Listing extends DataObject\Listing\Concrete {

protected $classId = "22";
protected $className = "BzDeals";


}
