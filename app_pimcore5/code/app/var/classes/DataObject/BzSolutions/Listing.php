<?php 

namespace Pimcore\Model\DataObject\BzSolutions;

use Pimcore\Model\DataObject;

/**
 * @method DataObject\BzSolutions current()
 * @method DataObject\BzSolutions[] load()
 */

class Listing extends DataObject\Listing\Concrete {

protected $classId = "34";
protected $className = "BzSolutions";


}
