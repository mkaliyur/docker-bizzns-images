<?php 

namespace Pimcore\Model\DataObject\BzDatatypes;

use Pimcore\Model\DataObject;

/**
 * @method DataObject\BzDatatypes current()
 * @method DataObject\BzDatatypes[] load()
 */

class Listing extends DataObject\Listing\Concrete {

protected $classId = "12";
protected $className = "BzDatatypes";


}
