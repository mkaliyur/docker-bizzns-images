<?php 

/** 
* Generated at: 2019-08-26T09:29:35+02:00
* Inheritance: no
* Variants: no
* Changed by: root (2)
* IP: ::1


Fields Summary: 
- name [input]
- object_type [input]
- solution_name [input]
- home_index_page [input]
- listing_list [input]
- listing_single [input]
- blog_list [input]
- blog_single [input]
- deal_list [input]
- deal_single [input]
- news_list [input]
- news_single [input]
- iwant [input]
- website_page [input]
- page_document [manyToOneRelation]
*/ 

namespace Pimcore\Model\DataObject;



/**
* @method static \Pimcore\Model\DataObject\BzPage\Listing getByName ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\BzPage\Listing getByObject_type ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\BzPage\Listing getBySolution_name ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\BzPage\Listing getByHome_index_page ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\BzPage\Listing getByListing_list ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\BzPage\Listing getByListing_single ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\BzPage\Listing getByBlog_list ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\BzPage\Listing getByBlog_single ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\BzPage\Listing getByDeal_list ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\BzPage\Listing getByDeal_single ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\BzPage\Listing getByNews_list ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\BzPage\Listing getByNews_single ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\BzPage\Listing getByIwant ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\BzPage\Listing getByWebsite_page ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\BzPage\Listing getByPage_document ($value, $limit = 0) 
*/

class BzPage extends Concrete implements \Pimcore\Model\DataObject\DirtyIndicatorInterface {



use \Pimcore\Model\DataObject\Traits\DirtyIndicatorTrait;

protected $o_classId = "50";
protected $o_className = "BzPage";
protected $name;
protected $object_type;
protected $solution_name;
protected $home_index_page;
protected $listing_list;
protected $listing_single;
protected $blog_list;
protected $blog_single;
protected $deal_list;
protected $deal_single;
protected $news_list;
protected $news_single;
protected $iwant;
protected $website_page;
protected $page_document;


/**
* @param array $values
* @return \Pimcore\Model\DataObject\BzPage
*/
public static function create($values = array()) {
	$object = new static();
	$object->setValues($values);
	return $object;
}

/**
* Get name - name
* @return string
*/
public function getName () {
	$preValue = $this->preGetValue("name"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->name;
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set name - name
* @param string $name
* @return \Pimcore\Model\DataObject\BzPage
*/
public function setName ($name) {
	$fd = $this->getClass()->getFieldDefinition("name");
	$this->name = $name;
	return $this;
}

/**
* Get object_type - object_type
* @return string
*/
public function getObject_type () {
	$preValue = $this->preGetValue("object_type"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->object_type;
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set object_type - object_type
* @param string $object_type
* @return \Pimcore\Model\DataObject\BzPage
*/
public function setObject_type ($object_type) {
	$fd = $this->getClass()->getFieldDefinition("object_type");
	$this->object_type = $object_type;
	return $this;
}

/**
* Get solution_name - solution_name
* @return string
*/
public function getSolution_name () {
	$preValue = $this->preGetValue("solution_name"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->solution_name;
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set solution_name - solution_name
* @param string $solution_name
* @return \Pimcore\Model\DataObject\BzPage
*/
public function setSolution_name ($solution_name) {
	$fd = $this->getClass()->getFieldDefinition("solution_name");
	$this->solution_name = $solution_name;
	return $this;
}

/**
* Get home_index_page - home_index_page
* @return string
*/
public function getHome_index_page () {
	$preValue = $this->preGetValue("home_index_page"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->home_index_page;
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set home_index_page - home_index_page
* @param string $home_index_page
* @return \Pimcore\Model\DataObject\BzPage
*/
public function setHome_index_page ($home_index_page) {
	$fd = $this->getClass()->getFieldDefinition("home_index_page");
	$this->home_index_page = $home_index_page;
	return $this;
}

/**
* Get listing_list - listing_list
* @return string
*/
public function getListing_list () {
	$preValue = $this->preGetValue("listing_list"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->listing_list;
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set listing_list - listing_list
* @param string $listing_list
* @return \Pimcore\Model\DataObject\BzPage
*/
public function setListing_list ($listing_list) {
	$fd = $this->getClass()->getFieldDefinition("listing_list");
	$this->listing_list = $listing_list;
	return $this;
}

/**
* Get listing_single - listing_single
* @return string
*/
public function getListing_single () {
	$preValue = $this->preGetValue("listing_single"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->listing_single;
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set listing_single - listing_single
* @param string $listing_single
* @return \Pimcore\Model\DataObject\BzPage
*/
public function setListing_single ($listing_single) {
	$fd = $this->getClass()->getFieldDefinition("listing_single");
	$this->listing_single = $listing_single;
	return $this;
}

/**
* Get blog_list - blog_list
* @return string
*/
public function getBlog_list () {
	$preValue = $this->preGetValue("blog_list"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->blog_list;
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set blog_list - blog_list
* @param string $blog_list
* @return \Pimcore\Model\DataObject\BzPage
*/
public function setBlog_list ($blog_list) {
	$fd = $this->getClass()->getFieldDefinition("blog_list");
	$this->blog_list = $blog_list;
	return $this;
}

/**
* Get blog_single - blog_single
* @return string
*/
public function getBlog_single () {
	$preValue = $this->preGetValue("blog_single"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->blog_single;
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set blog_single - blog_single
* @param string $blog_single
* @return \Pimcore\Model\DataObject\BzPage
*/
public function setBlog_single ($blog_single) {
	$fd = $this->getClass()->getFieldDefinition("blog_single");
	$this->blog_single = $blog_single;
	return $this;
}

/**
* Get deal_list - deal_list
* @return string
*/
public function getDeal_list () {
	$preValue = $this->preGetValue("deal_list"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->deal_list;
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set deal_list - deal_list
* @param string $deal_list
* @return \Pimcore\Model\DataObject\BzPage
*/
public function setDeal_list ($deal_list) {
	$fd = $this->getClass()->getFieldDefinition("deal_list");
	$this->deal_list = $deal_list;
	return $this;
}

/**
* Get deal_single - deal_single
* @return string
*/
public function getDeal_single () {
	$preValue = $this->preGetValue("deal_single"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->deal_single;
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set deal_single - deal_single
* @param string $deal_single
* @return \Pimcore\Model\DataObject\BzPage
*/
public function setDeal_single ($deal_single) {
	$fd = $this->getClass()->getFieldDefinition("deal_single");
	$this->deal_single = $deal_single;
	return $this;
}

/**
* Get news_list - news_list
* @return string
*/
public function getNews_list () {
	$preValue = $this->preGetValue("news_list"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->news_list;
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set news_list - news_list
* @param string $news_list
* @return \Pimcore\Model\DataObject\BzPage
*/
public function setNews_list ($news_list) {
	$fd = $this->getClass()->getFieldDefinition("news_list");
	$this->news_list = $news_list;
	return $this;
}

/**
* Get news_single - news_single
* @return string
*/
public function getNews_single () {
	$preValue = $this->preGetValue("news_single"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->news_single;
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set news_single - news_single
* @param string $news_single
* @return \Pimcore\Model\DataObject\BzPage
*/
public function setNews_single ($news_single) {
	$fd = $this->getClass()->getFieldDefinition("news_single");
	$this->news_single = $news_single;
	return $this;
}

/**
* Get iwant - iwant
* @return string
*/
public function getIwant () {
	$preValue = $this->preGetValue("iwant"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->iwant;
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set iwant - iwant
* @param string $iwant
* @return \Pimcore\Model\DataObject\BzPage
*/
public function setIwant ($iwant) {
	$fd = $this->getClass()->getFieldDefinition("iwant");
	$this->iwant = $iwant;
	return $this;
}

/**
* Get website_page - website_page
* @return string
*/
public function getWebsite_page () {
	$preValue = $this->preGetValue("website_page"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->website_page;
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set website_page - website_page
* @param string $website_page
* @return \Pimcore\Model\DataObject\BzPage
*/
public function setWebsite_page ($website_page) {
	$fd = $this->getClass()->getFieldDefinition("website_page");
	$this->website_page = $website_page;
	return $this;
}

/**
* Get page_document - page_document
* @return \Pimcore\Model\Document\Page
*/
public function getPage_document () {
	$preValue = $this->preGetValue("page_document"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->getClass()->getFieldDefinition("page_document")->preGetData($this);
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set page_document - page_document
* @param \Pimcore\Model\Document\Page $page_document
* @return \Pimcore\Model\DataObject\BzPage
*/
public function setPage_document ($page_document) {
	$fd = $this->getClass()->getFieldDefinition("page_document");
	$currentData = $this->getPage_document();
	$isEqual = $fd->isEqual($currentData, $page_document);
	if (!$isEqual) {
		$this->markFieldDirty("page_document", true);
	}
	$this->page_document = $fd->preSetData($this, $page_document);
	return $this;
}

protected static $_relationFields = array (
  'page_document' => 
  array (
    'type' => 'manyToOneRelation',
  ),
);

protected $lazyLoadedFields = array (
  0 => 'page_document',
);

}

