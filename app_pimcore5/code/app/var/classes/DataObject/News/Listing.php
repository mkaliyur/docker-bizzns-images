<?php 

namespace Pimcore\Model\DataObject\News;

use Pimcore\Model\DataObject;

/**
 * @method DataObject\News current()
 * @method DataObject\News[] load()
 */

class Listing extends DataObject\Listing\Concrete {

protected $classId = "2";
protected $className = "news";


}
