<?php 

/** 
* Generated at: 2019-08-14T14:56:36+02:00
* Inheritance: no
* Variants: no
* Changed by: root (2)
* IP: ::1


Fields Summary: 
- name [input]
- object_type [input]
- sharedvolume_input1 [input]
*/ 

namespace Pimcore\Model\DataObject;



/**
* @method static \Pimcore\Model\DataObject\BzSystemComponentSharedVolume\Listing getByName ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\BzSystemComponentSharedVolume\Listing getByObject_type ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\BzSystemComponentSharedVolume\Listing getBySharedvolume_input1 ($value, $limit = 0) 
*/

class BzSystemComponentSharedVolume extends Concrete implements \Pimcore\Model\DataObject\DirtyIndicatorInterface {



use \Pimcore\Model\DataObject\Traits\DirtyIndicatorTrait;

protected $o_classId = "45";
protected $o_className = "BzSystemComponentSharedVolume";
protected $name;
protected $object_type;
protected $sharedvolume_input1;


/**
* @param array $values
* @return \Pimcore\Model\DataObject\BzSystemComponentSharedVolume
*/
public static function create($values = array()) {
	$object = new static();
	$object->setValues($values);
	return $object;
}

/**
* Get name - name
* @return string
*/
public function getName () {
	$preValue = $this->preGetValue("name"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->name;
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set name - name
* @param string $name
* @return \Pimcore\Model\DataObject\BzSystemComponentSharedVolume
*/
public function setName ($name) {
	$fd = $this->getClass()->getFieldDefinition("name");
	$this->name = $name;
	return $this;
}

/**
* Get object_type - object_type
* @return string
*/
public function getObject_type () {
	$preValue = $this->preGetValue("object_type"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->object_type;
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set object_type - object_type
* @param string $object_type
* @return \Pimcore\Model\DataObject\BzSystemComponentSharedVolume
*/
public function setObject_type ($object_type) {
	$fd = $this->getClass()->getFieldDefinition("object_type");
	$this->object_type = $object_type;
	return $this;
}

/**
* Get sharedvolume_input1 - sharedvolume_input1
* @return string
*/
public function getSharedvolume_input1 () {
	$preValue = $this->preGetValue("sharedvolume_input1"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->sharedvolume_input1;
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set sharedvolume_input1 - sharedvolume_input1
* @param string $sharedvolume_input1
* @return \Pimcore\Model\DataObject\BzSystemComponentSharedVolume
*/
public function setSharedvolume_input1 ($sharedvolume_input1) {
	$fd = $this->getClass()->getFieldDefinition("sharedvolume_input1");
	$this->sharedvolume_input1 = $sharedvolume_input1;
	return $this;
}

protected static $_relationFields = array (
);

protected $lazyLoadedFields = array (
);

}

