<?php 

namespace Pimcore\Model\DataObject\BzBlogCategory;

use Pimcore\Model\DataObject;

/**
 * @method DataObject\BzBlogCategory current()
 * @method DataObject\BzBlogCategory[] load()
 */

class Listing extends DataObject\Listing\Concrete {

protected $classId = "38";
protected $className = "BzBlogCategory";


}
