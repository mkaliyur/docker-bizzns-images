<?php 

/** 
* Generated at: 2019-08-14T15:10:31+02:00
* Inheritance: no
* Variants: no
* Changed by: root (2)
* IP: ::1


Fields Summary: 
- job_name [input]
- executable [input]
- min [input]
- hours [input]
- day_of_month [input]
- month_of_year [input]
- day_of_the_week [input]
- job_user [input]
- name [input]
- object_type [input]
*/ 

namespace Pimcore\Model\DataObject;



/**
* @method static \Pimcore\Model\DataObject\BzSystemComponentCronJobs\Listing getByJob_name ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\BzSystemComponentCronJobs\Listing getByExecutable ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\BzSystemComponentCronJobs\Listing getByMin ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\BzSystemComponentCronJobs\Listing getByHours ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\BzSystemComponentCronJobs\Listing getByDay_of_month ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\BzSystemComponentCronJobs\Listing getByMonth_of_year ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\BzSystemComponentCronJobs\Listing getByDay_of_the_week ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\BzSystemComponentCronJobs\Listing getByJob_user ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\BzSystemComponentCronJobs\Listing getByName ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\BzSystemComponentCronJobs\Listing getByObject_type ($value, $limit = 0) 
*/

class BzSystemComponentCronJobs extends Concrete implements \Pimcore\Model\DataObject\DirtyIndicatorInterface {



use \Pimcore\Model\DataObject\Traits\DirtyIndicatorTrait;

protected $o_classId = "46";
protected $o_className = "BzSystemComponentCronJobs";
protected $job_name;
protected $executable;
protected $min;
protected $hours;
protected $day_of_month;
protected $month_of_year;
protected $day_of_the_week;
protected $job_user;
protected $name;
protected $object_type;


/**
* @param array $values
* @return \Pimcore\Model\DataObject\BzSystemComponentCronJobs
*/
public static function create($values = array()) {
	$object = new static();
	$object->setValues($values);
	return $object;
}

/**
* Get job_name - job_name
* @return string
*/
public function getJob_name () {
	$preValue = $this->preGetValue("job_name"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->job_name;
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set job_name - job_name
* @param string $job_name
* @return \Pimcore\Model\DataObject\BzSystemComponentCronJobs
*/
public function setJob_name ($job_name) {
	$fd = $this->getClass()->getFieldDefinition("job_name");
	$this->job_name = $job_name;
	return $this;
}

/**
* Get executable - executable
* @return string
*/
public function getExecutable () {
	$preValue = $this->preGetValue("executable"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->executable;
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set executable - executable
* @param string $executable
* @return \Pimcore\Model\DataObject\BzSystemComponentCronJobs
*/
public function setExecutable ($executable) {
	$fd = $this->getClass()->getFieldDefinition("executable");
	$this->executable = $executable;
	return $this;
}

/**
* Get min - min
* @return string
*/
public function getMin () {
	$preValue = $this->preGetValue("min"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->min;
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set min - min
* @param string $min
* @return \Pimcore\Model\DataObject\BzSystemComponentCronJobs
*/
public function setMin ($min) {
	$fd = $this->getClass()->getFieldDefinition("min");
	$this->min = $min;
	return $this;
}

/**
* Get hours - hours
* @return string
*/
public function getHours () {
	$preValue = $this->preGetValue("hours"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->hours;
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set hours - hours
* @param string $hours
* @return \Pimcore\Model\DataObject\BzSystemComponentCronJobs
*/
public function setHours ($hours) {
	$fd = $this->getClass()->getFieldDefinition("hours");
	$this->hours = $hours;
	return $this;
}

/**
* Get day_of_month - day_of_month
* @return string
*/
public function getDay_of_month () {
	$preValue = $this->preGetValue("day_of_month"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->day_of_month;
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set day_of_month - day_of_month
* @param string $day_of_month
* @return \Pimcore\Model\DataObject\BzSystemComponentCronJobs
*/
public function setDay_of_month ($day_of_month) {
	$fd = $this->getClass()->getFieldDefinition("day_of_month");
	$this->day_of_month = $day_of_month;
	return $this;
}

/**
* Get month_of_year - month_of_year
* @return string
*/
public function getMonth_of_year () {
	$preValue = $this->preGetValue("month_of_year"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->month_of_year;
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set month_of_year - month_of_year
* @param string $month_of_year
* @return \Pimcore\Model\DataObject\BzSystemComponentCronJobs
*/
public function setMonth_of_year ($month_of_year) {
	$fd = $this->getClass()->getFieldDefinition("month_of_year");
	$this->month_of_year = $month_of_year;
	return $this;
}

/**
* Get day_of_the_week - day_of_the_week
* @return string
*/
public function getDay_of_the_week () {
	$preValue = $this->preGetValue("day_of_the_week"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->day_of_the_week;
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set day_of_the_week - day_of_the_week
* @param string $day_of_the_week
* @return \Pimcore\Model\DataObject\BzSystemComponentCronJobs
*/
public function setDay_of_the_week ($day_of_the_week) {
	$fd = $this->getClass()->getFieldDefinition("day_of_the_week");
	$this->day_of_the_week = $day_of_the_week;
	return $this;
}

/**
* Get job_user - job_user
* @return string
*/
public function getJob_user () {
	$preValue = $this->preGetValue("job_user"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->job_user;
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set job_user - job_user
* @param string $job_user
* @return \Pimcore\Model\DataObject\BzSystemComponentCronJobs
*/
public function setJob_user ($job_user) {
	$fd = $this->getClass()->getFieldDefinition("job_user");
	$this->job_user = $job_user;
	return $this;
}

/**
* Get name - name
* @return string
*/
public function getName () {
	$preValue = $this->preGetValue("name"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->name;
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set name - name
* @param string $name
* @return \Pimcore\Model\DataObject\BzSystemComponentCronJobs
*/
public function setName ($name) {
	$fd = $this->getClass()->getFieldDefinition("name");
	$this->name = $name;
	return $this;
}

/**
* Get object_type - object_type
* @return string
*/
public function getObject_type () {
	$preValue = $this->preGetValue("object_type"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->object_type;
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set object_type - object_type
* @param string $object_type
* @return \Pimcore\Model\DataObject\BzSystemComponentCronJobs
*/
public function setObject_type ($object_type) {
	$fd = $this->getClass()->getFieldDefinition("object_type");
	$this->object_type = $object_type;
	return $this;
}

protected static $_relationFields = array (
);

protected $lazyLoadedFields = array (
);

}

