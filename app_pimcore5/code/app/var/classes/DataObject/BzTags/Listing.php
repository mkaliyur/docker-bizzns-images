<?php 

namespace Pimcore\Model\DataObject\BzTags;

use Pimcore\Model\DataObject;

/**
 * @method DataObject\BzTags current()
 * @method DataObject\BzTags[] load()
 */

class Listing extends DataObject\Listing\Concrete {

protected $classId = "17";
protected $className = "BzTags";


}
