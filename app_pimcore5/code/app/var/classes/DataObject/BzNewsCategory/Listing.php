<?php 

namespace Pimcore\Model\DataObject\BzNewsCategory;

use Pimcore\Model\DataObject;

/**
 * @method DataObject\BzNewsCategory current()
 * @method DataObject\BzNewsCategory[] load()
 */

class Listing extends DataObject\Listing\Concrete {

protected $classId = "40";
protected $className = "BzNewsCategory";


}
