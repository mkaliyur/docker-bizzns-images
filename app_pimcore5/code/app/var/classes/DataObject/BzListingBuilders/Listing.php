<?php 

namespace Pimcore\Model\DataObject\BzListingBuilders;

use Pimcore\Model\DataObject;

/**
 * @method DataObject\BzListingBuilders current()
 * @method DataObject\BzListingBuilders[] load()
 */

class Listing extends DataObject\Listing\Concrete {

protected $classId = "42";
protected $className = "BzListingBuilders";


}
