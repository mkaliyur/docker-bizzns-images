<?php 

namespace Pimcore\Model\DataObject\BzMarketArea;

use Pimcore\Model\DataObject;

/**
 * @method DataObject\BzMarketArea current()
 * @method DataObject\BzMarketArea[] load()
 */

class Listing extends DataObject\Listing\Concrete {

protected $classId = "25";
protected $className = "BzMarketArea";


}
