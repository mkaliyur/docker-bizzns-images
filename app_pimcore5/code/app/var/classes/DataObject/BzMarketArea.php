<?php 

/** 
* Generated at: 2019-09-03T12:01:25+02:00
* Inheritance: no
* Variants: no
* Changed by: root (2)
* IP: ::1


Fields Summary: 
- market_id [input]
- name [input]
- title [input]
- slug [input]
- country [country]
- region [select]
- state [select]
- city [input]
- zipCodes [input]
- domains [reverseManyToManyObjectRelation]
- solution_name [input]
- object_type [input]
*/ 

namespace Pimcore\Model\DataObject;



/**
* @method static \Pimcore\Model\DataObject\BzMarketArea\Listing getByMarket_id ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\BzMarketArea\Listing getByName ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\BzMarketArea\Listing getByTitle ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\BzMarketArea\Listing getBySlug ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\BzMarketArea\Listing getByCountry ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\BzMarketArea\Listing getByRegion ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\BzMarketArea\Listing getByState ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\BzMarketArea\Listing getByCity ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\BzMarketArea\Listing getByZipCodes ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\BzMarketArea\Listing getBySolution_name ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\BzMarketArea\Listing getByObject_type ($value, $limit = 0) 
*/

class BzMarketArea extends Concrete implements \Pimcore\Model\DataObject\DirtyIndicatorInterface {



use \Pimcore\Model\DataObject\Traits\DirtyIndicatorTrait;

protected $o_classId = "25";
protected $o_className = "BzMarketArea";
protected $market_id;
protected $name;
protected $title;
protected $slug;
protected $country;
protected $region;
protected $state;
protected $city;
protected $zipCodes;
protected $solution_name;
protected $object_type;


/**
* @param array $values
* @return \Pimcore\Model\DataObject\BzMarketArea
*/
public static function create($values = array()) {
	$object = new static();
	$object->setValues($values);
	return $object;
}

/**
* Get market_id - market_id
* @return string
*/
public function getMarket_id () {
	$preValue = $this->preGetValue("market_id"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->market_id;
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set market_id - market_id
* @param string $market_id
* @return \Pimcore\Model\DataObject\BzMarketArea
*/
public function setMarket_id ($market_id) {
	$fd = $this->getClass()->getFieldDefinition("market_id");
	$this->market_id = $market_id;
	return $this;
}

/**
* Get name - Name
* @return string
*/
public function getName () {
	$preValue = $this->preGetValue("name"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->name;
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set name - Name
* @param string $name
* @return \Pimcore\Model\DataObject\BzMarketArea
*/
public function setName ($name) {
	$fd = $this->getClass()->getFieldDefinition("name");
	$this->name = $name;
	return $this;
}

/**
* Get title - Title
* @return string
*/
public function getTitle () {
	$preValue = $this->preGetValue("title"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->title;
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set title - Title
* @param string $title
* @return \Pimcore\Model\DataObject\BzMarketArea
*/
public function setTitle ($title) {
	$fd = $this->getClass()->getFieldDefinition("title");
	$this->title = $title;
	return $this;
}

/**
* Get slug - Slug
* @return string
*/
public function getSlug () {
	$preValue = $this->preGetValue("slug"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->slug;
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set slug - Slug
* @param string $slug
* @return \Pimcore\Model\DataObject\BzMarketArea
*/
public function setSlug ($slug) {
	$fd = $this->getClass()->getFieldDefinition("slug");
	$this->slug = $slug;
	return $this;
}

/**
* Get country - country
* @return string
*/
public function getCountry () {
	$preValue = $this->preGetValue("country"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->country;
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set country - country
* @param string $country
* @return \Pimcore\Model\DataObject\BzMarketArea
*/
public function setCountry ($country) {
	$fd = $this->getClass()->getFieldDefinition("country");
	$this->country = $country;
	return $this;
}

/**
* Get region - region
* @return string
*/
public function getRegion () {
	$preValue = $this->preGetValue("region"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->region;
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set region - region
* @param string $region
* @return \Pimcore\Model\DataObject\BzMarketArea
*/
public function setRegion ($region) {
	$fd = $this->getClass()->getFieldDefinition("region");
	$this->region = $region;
	return $this;
}

/**
* Get state - state
* @return string
*/
public function getState () {
	$preValue = $this->preGetValue("state"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->state;
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set state - state
* @param string $state
* @return \Pimcore\Model\DataObject\BzMarketArea
*/
public function setState ($state) {
	$fd = $this->getClass()->getFieldDefinition("state");
	$this->state = $state;
	return $this;
}

/**
* Get city - city
* @return string
*/
public function getCity () {
	$preValue = $this->preGetValue("city"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->city;
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set city - city
* @param string $city
* @return \Pimcore\Model\DataObject\BzMarketArea
*/
public function setCity ($city) {
	$fd = $this->getClass()->getFieldDefinition("city");
	$this->city = $city;
	return $this;
}

/**
* Get zipCodes - zip codes
* @return string
*/
public function getZipCodes () {
	$preValue = $this->preGetValue("zipCodes"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->zipCodes;
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set zipCodes - zip codes
* @param string $zipCodes
* @return \Pimcore\Model\DataObject\BzMarketArea
*/
public function setZipCodes ($zipCodes) {
	$fd = $this->getClass()->getFieldDefinition("zipCodes");
	$this->zipCodes = $zipCodes;
	return $this;
}

/**
* Get solution_name - Solution Name
* @return string
*/
public function getSolution_name () {
	$preValue = $this->preGetValue("solution_name"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->solution_name;
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set solution_name - Solution Name
* @param string $solution_name
* @return \Pimcore\Model\DataObject\BzMarketArea
*/
public function setSolution_name ($solution_name) {
	$fd = $this->getClass()->getFieldDefinition("solution_name");
	$this->solution_name = $solution_name;
	return $this;
}

/**
* Get object_type - Object Type
* @return string
*/
public function getObject_type () {
	$preValue = $this->preGetValue("object_type"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->object_type;
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set object_type - Object Type
* @param string $object_type
* @return \Pimcore\Model\DataObject\BzMarketArea
*/
public function setObject_type ($object_type) {
	$fd = $this->getClass()->getFieldDefinition("object_type");
	$this->object_type = $object_type;
	return $this;
}

protected static $_relationFields = array (
);

protected $lazyLoadedFields = array (
);

}

