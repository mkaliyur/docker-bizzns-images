<?php 

/** 
* Generated at: 2019-09-03T12:01:59+02:00
* Inheritance: no
* Variants: no
* Changed by: root (2)
* IP: ::1


Fields Summary: 
- parentMenuItem [manyToOneRelation]
- name [input]
- displayText [input]
- targetURL [input]
- object_type [input]
- solution_name [input]
*/ 

namespace Pimcore\Model\DataObject;



/**
* @method static \Pimcore\Model\DataObject\BzMenuItem\Listing getByParentMenuItem ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\BzMenuItem\Listing getByName ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\BzMenuItem\Listing getByDisplayText ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\BzMenuItem\Listing getByTargetURL ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\BzMenuItem\Listing getByObject_type ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\BzMenuItem\Listing getBySolution_name ($value, $limit = 0) 
*/

class BzMenuItem extends Concrete implements \Pimcore\Model\DataObject\DirtyIndicatorInterface {



use \Pimcore\Model\DataObject\Traits\DirtyIndicatorTrait;

protected $o_classId = "20";
protected $o_className = "BzMenuItem";
protected $parentMenuItem;
protected $name;
protected $displayText;
protected $targetURL;
protected $object_type;
protected $solution_name;


/**
* @param array $values
* @return \Pimcore\Model\DataObject\BzMenuItem
*/
public static function create($values = array()) {
	$object = new static();
	$object->setValues($values);
	return $object;
}

/**
* Get parentMenuItem - Parent Menu Item
* @return \Pimcore\Model\DataObject\BzMenuItem
*/
public function getParentMenuItem () {
	$preValue = $this->preGetValue("parentMenuItem"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->getClass()->getFieldDefinition("parentMenuItem")->preGetData($this);
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set parentMenuItem - Parent Menu Item
* @param \Pimcore\Model\DataObject\BzMenuItem $parentMenuItem
* @return \Pimcore\Model\DataObject\BzMenuItem
*/
public function setParentMenuItem ($parentMenuItem) {
	$fd = $this->getClass()->getFieldDefinition("parentMenuItem");
	$currentData = $this->getParentMenuItem();
	$isEqual = $fd->isEqual($currentData, $parentMenuItem);
	if (!$isEqual) {
		$this->markFieldDirty("parentMenuItem", true);
	}
	$this->parentMenuItem = $fd->preSetData($this, $parentMenuItem);
	return $this;
}

/**
* Get name - Name
* @return string
*/
public function getName () {
	$preValue = $this->preGetValue("name"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->name;
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set name - Name
* @param string $name
* @return \Pimcore\Model\DataObject\BzMenuItem
*/
public function setName ($name) {
	$fd = $this->getClass()->getFieldDefinition("name");
	$this->name = $name;
	return $this;
}

/**
* Get displayText - Display Text
* @return string
*/
public function getDisplayText () {
	$preValue = $this->preGetValue("displayText"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->displayText;
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set displayText - Display Text
* @param string $displayText
* @return \Pimcore\Model\DataObject\BzMenuItem
*/
public function setDisplayText ($displayText) {
	$fd = $this->getClass()->getFieldDefinition("displayText");
	$this->displayText = $displayText;
	return $this;
}

/**
* Get targetURL - Target URL
* @return string
*/
public function getTargetURL () {
	$preValue = $this->preGetValue("targetURL"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->targetURL;
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set targetURL - Target URL
* @param string $targetURL
* @return \Pimcore\Model\DataObject\BzMenuItem
*/
public function setTargetURL ($targetURL) {
	$fd = $this->getClass()->getFieldDefinition("targetURL");
	$this->targetURL = $targetURL;
	return $this;
}

/**
* Get object_type - Object Type
* @return string
*/
public function getObject_type () {
	$preValue = $this->preGetValue("object_type"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->object_type;
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set object_type - Object Type
* @param string $object_type
* @return \Pimcore\Model\DataObject\BzMenuItem
*/
public function setObject_type ($object_type) {
	$fd = $this->getClass()->getFieldDefinition("object_type");
	$this->object_type = $object_type;
	return $this;
}

/**
* Get solution_name - Solution Name
* @return string
*/
public function getSolution_name () {
	$preValue = $this->preGetValue("solution_name"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->solution_name;
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set solution_name - Solution Name
* @param string $solution_name
* @return \Pimcore\Model\DataObject\BzMenuItem
*/
public function setSolution_name ($solution_name) {
	$fd = $this->getClass()->getFieldDefinition("solution_name");
	$this->solution_name = $solution_name;
	return $this;
}

protected static $_relationFields = array (
  'parentMenuItem' => 
  array (
    'type' => 'manyToOneRelation',
  ),
);

protected $lazyLoadedFields = array (
);

}

