<?php 

namespace Pimcore\Model\DataObject\BzWebsite;

use Pimcore\Model\DataObject;

/**
 * @method DataObject\BzWebsite current()
 * @method DataObject\BzWebsite[] load()
 */

class Listing extends DataObject\Listing\Concrete {

protected $classId = "51";
protected $className = "BzWebsite";


}
