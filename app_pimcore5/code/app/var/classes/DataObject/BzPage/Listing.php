<?php 

namespace Pimcore\Model\DataObject\BzPage;

use Pimcore\Model\DataObject;

/**
 * @method DataObject\BzPage current()
 * @method DataObject\BzPage[] load()
 */

class Listing extends DataObject\Listing\Concrete {

protected $classId = "50";
protected $className = "BzPage";


}
