<?php 

namespace Pimcore\Model\DataObject\BzLayout;

use Pimcore\Model\DataObject;

/**
 * @method DataObject\BzLayout current()
 * @method DataObject\BzLayout[] load()
 */

class Listing extends DataObject\Listing\Concrete {

protected $classId = "32";
protected $className = "BzLayout";


}
