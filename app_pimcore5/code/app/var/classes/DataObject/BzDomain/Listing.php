<?php 

namespace Pimcore\Model\DataObject\BzDomain;

use Pimcore\Model\DataObject;

/**
 * @method DataObject\BzDomain current()
 * @method DataObject\BzDomain[] load()
 */

class Listing extends DataObject\Listing\Concrete {

protected $classId = "9";
protected $className = "BzDomain";


}
