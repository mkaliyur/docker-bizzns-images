<?php 

/** 
* Generated at: 2019-03-29T14:08:05+01:00
* Inheritance: no
* Variants: no
* Changed by: root (2)
* IP: ::1


Fields Summary: 
- name [input]
- folder [input]
- environment [input]
- initial_date_of_deployment [datetime]
- solution_href [manyToOneRelation]
- ip_address [input]
- dns_name_of_the_box [input]
- url_of_the_solution [input]
- admin_url [input]
- ssh_user_login [input]
- ssh_password [input]
- box_provider [input]
- box_provider_url [input]
- box_provider_login [input]
- instance_git_repository_url [input]
- object_type [input]
*/ 

namespace Pimcore\Model\DataObject;



/**
* @method static \Pimcore\Model\DataObject\BzSolutionInstance\Listing getByName ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\BzSolutionInstance\Listing getByFolder ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\BzSolutionInstance\Listing getByEnvironment ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\BzSolutionInstance\Listing getByInitial_date_of_deployment ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\BzSolutionInstance\Listing getBySolution_href ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\BzSolutionInstance\Listing getByIp_address ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\BzSolutionInstance\Listing getByDns_name_of_the_box ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\BzSolutionInstance\Listing getByUrl_of_the_solution ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\BzSolutionInstance\Listing getByAdmin_url ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\BzSolutionInstance\Listing getBySsh_user_login ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\BzSolutionInstance\Listing getBySsh_password ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\BzSolutionInstance\Listing getByBox_provider ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\BzSolutionInstance\Listing getByBox_provider_url ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\BzSolutionInstance\Listing getByBox_provider_login ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\BzSolutionInstance\Listing getByInstance_git_repository_url ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\BzSolutionInstance\Listing getByObject_type ($value, $limit = 0) 
*/

class BzSolutionInstance extends Concrete implements \Pimcore\Model\DataObject\DirtyIndicatorInterface {



use \Pimcore\Model\DataObject\Traits\DirtyIndicatorTrait;

protected $o_classId = "36";
protected $o_className = "BzSolutionInstance";
protected $name;
protected $folder;
protected $environment;
protected $initial_date_of_deployment;
protected $solution_href;
protected $ip_address;
protected $dns_name_of_the_box;
protected $url_of_the_solution;
protected $admin_url;
protected $ssh_user_login;
protected $ssh_password;
protected $box_provider;
protected $box_provider_url;
protected $box_provider_login;
protected $instance_git_repository_url;
protected $object_type;


/**
* @param array $values
* @return \Pimcore\Model\DataObject\BzSolutionInstance
*/
public static function create($values = array()) {
	$object = new static();
	$object->setValues($values);
	return $object;
}

/**
* Get name - name
* @return string
*/
public function getName () {
	$preValue = $this->preGetValue("name"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->name;
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set name - name
* @param string $name
* @return \Pimcore\Model\DataObject\BzSolutionInstance
*/
public function setName ($name) {
	$fd = $this->getClass()->getFieldDefinition("name");
	$this->name = $name;
	return $this;
}

/**
* Get folder - folder
* @return string
*/
public function getFolder () {
	$preValue = $this->preGetValue("folder"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->folder;
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set folder - folder
* @param string $folder
* @return \Pimcore\Model\DataObject\BzSolutionInstance
*/
public function setFolder ($folder) {
	$fd = $this->getClass()->getFieldDefinition("folder");
	$this->folder = $folder;
	return $this;
}

/**
* Get environment - environment
* @return string
*/
public function getEnvironment () {
	$preValue = $this->preGetValue("environment"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->environment;
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set environment - environment
* @param string $environment
* @return \Pimcore\Model\DataObject\BzSolutionInstance
*/
public function setEnvironment ($environment) {
	$fd = $this->getClass()->getFieldDefinition("environment");
	$this->environment = $environment;
	return $this;
}

/**
* Get initial_date_of_deployment - initial_date_of_deployment
* @return \Carbon\Carbon
*/
public function getInitial_date_of_deployment () {
	$preValue = $this->preGetValue("initial_date_of_deployment"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->initial_date_of_deployment;
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set initial_date_of_deployment - initial_date_of_deployment
* @param \Carbon\Carbon $initial_date_of_deployment
* @return \Pimcore\Model\DataObject\BzSolutionInstance
*/
public function setInitial_date_of_deployment ($initial_date_of_deployment) {
	$fd = $this->getClass()->getFieldDefinition("initial_date_of_deployment");
	$this->initial_date_of_deployment = $initial_date_of_deployment;
	return $this;
}

/**
* Get solution_href - solution_href
* @return 
*/
public function getSolution_href () {
	$preValue = $this->preGetValue("solution_href"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->getClass()->getFieldDefinition("solution_href")->preGetData($this);
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set solution_href - solution_href
* @param  $solution_href
* @return \Pimcore\Model\DataObject\BzSolutionInstance
*/
public function setSolution_href ($solution_href) {
	$fd = $this->getClass()->getFieldDefinition("solution_href");
	$currentData = $this->getSolution_href();
	$isEqual = $fd->isEqual($currentData, $solution_href);
	if (!$isEqual) {
		$this->markFieldDirty("solution_href", true);
	}
	$this->solution_href = $fd->preSetData($this, $solution_href);
	return $this;
}

/**
* Get ip_address - ip_address
* @return string
*/
public function getIp_address () {
	$preValue = $this->preGetValue("ip_address"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->ip_address;
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set ip_address - ip_address
* @param string $ip_address
* @return \Pimcore\Model\DataObject\BzSolutionInstance
*/
public function setIp_address ($ip_address) {
	$fd = $this->getClass()->getFieldDefinition("ip_address");
	$this->ip_address = $ip_address;
	return $this;
}

/**
* Get dns_name_of_the_box - dns_name_of_the_box
* @return string
*/
public function getDns_name_of_the_box () {
	$preValue = $this->preGetValue("dns_name_of_the_box"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->dns_name_of_the_box;
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set dns_name_of_the_box - dns_name_of_the_box
* @param string $dns_name_of_the_box
* @return \Pimcore\Model\DataObject\BzSolutionInstance
*/
public function setDns_name_of_the_box ($dns_name_of_the_box) {
	$fd = $this->getClass()->getFieldDefinition("dns_name_of_the_box");
	$this->dns_name_of_the_box = $dns_name_of_the_box;
	return $this;
}

/**
* Get url_of_the_solution - url_of_the_solution
* @return string
*/
public function getUrl_of_the_solution () {
	$preValue = $this->preGetValue("url_of_the_solution"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->url_of_the_solution;
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set url_of_the_solution - url_of_the_solution
* @param string $url_of_the_solution
* @return \Pimcore\Model\DataObject\BzSolutionInstance
*/
public function setUrl_of_the_solution ($url_of_the_solution) {
	$fd = $this->getClass()->getFieldDefinition("url_of_the_solution");
	$this->url_of_the_solution = $url_of_the_solution;
	return $this;
}

/**
* Get admin_url - admin_url
* @return string
*/
public function getAdmin_url () {
	$preValue = $this->preGetValue("admin_url"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->admin_url;
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set admin_url - admin_url
* @param string $admin_url
* @return \Pimcore\Model\DataObject\BzSolutionInstance
*/
public function setAdmin_url ($admin_url) {
	$fd = $this->getClass()->getFieldDefinition("admin_url");
	$this->admin_url = $admin_url;
	return $this;
}

/**
* Get ssh_user_login - ssh_user_login
* @return string
*/
public function getSsh_user_login () {
	$preValue = $this->preGetValue("ssh_user_login"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->ssh_user_login;
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set ssh_user_login - ssh_user_login
* @param string $ssh_user_login
* @return \Pimcore\Model\DataObject\BzSolutionInstance
*/
public function setSsh_user_login ($ssh_user_login) {
	$fd = $this->getClass()->getFieldDefinition("ssh_user_login");
	$this->ssh_user_login = $ssh_user_login;
	return $this;
}

/**
* Get ssh_password - ssh_password
* @return string
*/
public function getSsh_password () {
	$preValue = $this->preGetValue("ssh_password"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->ssh_password;
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set ssh_password - ssh_password
* @param string $ssh_password
* @return \Pimcore\Model\DataObject\BzSolutionInstance
*/
public function setSsh_password ($ssh_password) {
	$fd = $this->getClass()->getFieldDefinition("ssh_password");
	$this->ssh_password = $ssh_password;
	return $this;
}

/**
* Get box_provider - box_provider
* @return string
*/
public function getBox_provider () {
	$preValue = $this->preGetValue("box_provider"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->box_provider;
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set box_provider - box_provider
* @param string $box_provider
* @return \Pimcore\Model\DataObject\BzSolutionInstance
*/
public function setBox_provider ($box_provider) {
	$fd = $this->getClass()->getFieldDefinition("box_provider");
	$this->box_provider = $box_provider;
	return $this;
}

/**
* Get box_provider_url - box_provider_url
* @return string
*/
public function getBox_provider_url () {
	$preValue = $this->preGetValue("box_provider_url"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->box_provider_url;
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set box_provider_url - box_provider_url
* @param string $box_provider_url
* @return \Pimcore\Model\DataObject\BzSolutionInstance
*/
public function setBox_provider_url ($box_provider_url) {
	$fd = $this->getClass()->getFieldDefinition("box_provider_url");
	$this->box_provider_url = $box_provider_url;
	return $this;
}

/**
* Get box_provider_login - box_provider_login
* @return string
*/
public function getBox_provider_login () {
	$preValue = $this->preGetValue("box_provider_login"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->box_provider_login;
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set box_provider_login - box_provider_login
* @param string $box_provider_login
* @return \Pimcore\Model\DataObject\BzSolutionInstance
*/
public function setBox_provider_login ($box_provider_login) {
	$fd = $this->getClass()->getFieldDefinition("box_provider_login");
	$this->box_provider_login = $box_provider_login;
	return $this;
}

/**
* Get instance_git_repository_url - instance_git_repository_url
* @return string
*/
public function getInstance_git_repository_url () {
	$preValue = $this->preGetValue("instance_git_repository_url"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->instance_git_repository_url;
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set instance_git_repository_url - instance_git_repository_url
* @param string $instance_git_repository_url
* @return \Pimcore\Model\DataObject\BzSolutionInstance
*/
public function setInstance_git_repository_url ($instance_git_repository_url) {
	$fd = $this->getClass()->getFieldDefinition("instance_git_repository_url");
	$this->instance_git_repository_url = $instance_git_repository_url;
	return $this;
}

/**
* Get object_type - Object Type
* @return string
*/
public function getObject_type () {
	$preValue = $this->preGetValue("object_type"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->object_type;
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set object_type - Object Type
* @param string $object_type
* @return \Pimcore\Model\DataObject\BzSolutionInstance
*/
public function setObject_type ($object_type) {
	$fd = $this->getClass()->getFieldDefinition("object_type");
	$this->object_type = $object_type;
	return $this;
}

protected static $_relationFields = array (
  'solution_href' => 
  array (
    'type' => 'manyToOneRelation',
  ),
);

protected $lazyLoadedFields = array (
);

}

