<?php 

/** 
* Generated at: 2019-08-30T11:29:09+02:00
* Inheritance: no
* Variants: no
* Changed by: root (2)
* IP: ::1


Fields Summary: 
- domain_name [input]
- market_name [input]
- inherited_from [manyToOneRelation]
- market_area_href [manyToOneRelation]
- domain_href [manyToOneRelation]
- merchant_href [manyToOneRelation]
- object_type [input]
- name [input]
- title [input]
- solution_name [input]
- headline [input]
- shortDescription [input]
- isPromoted [checkbox]
- isPremium [checkbox]
- isFeatured [checkbox]
- isClaimed [checkbox]
- ObjectClassName [select]
- tag_cloud [input]
- type_display_name [input]
- address_line_1 [input]
- address_line_2 [input]
- address_line_3 [input]
- city [input]
- state [input]
- zip [input]
- country [input]
- addressGeoLocation [geopoint]
- mainContactNumber [input]
- websiteUrl [input]
- hoursOfOperation [wysiwyg]
- email [input]
- additional_phones [wysiwyg]
- services [wysiwyg]
- longDescription [wysiwyg]
- rating [numeric]
- images [manyToManyRelation]
- videos_local [manyToManyRelation]
- bannerImages [manyToManyRelation]
- tags [manyToManyRelation]
- category [manyToManyRelation]
- external_media [fieldcollections]
- social_media [fieldcollections]
- benefits [fieldcollections]
- features [fieldcollections]
- testimonials [fieldcollections]
- pageBackGroundImage [image]
- pageBackgroundColour [input]
- slide [fieldcollections]
- logo_image [image]
- logo_tagline [input]
- displayOnHomePage [checkbox]
- listing_inherits_from [manyToOneRelation]
- deal_inherits_from [manyToOneRelation]
- event_inherits_from [manyToOneRelation]
- audio_inherits_from [manyToOneRelation]
- video_inherits_from [manyToOneRelation]
- article_inherits_from [manyToOneRelation]
- news_inherits_from [manyToOneRelation]
*/ 

namespace Pimcore\Model\DataObject;



/**
* @method static \Pimcore\Model\DataObject\BzListingTravels\Listing getByDomain_name ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\BzListingTravels\Listing getByMarket_name ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\BzListingTravels\Listing getByInherited_from ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\BzListingTravels\Listing getByMarket_area_href ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\BzListingTravels\Listing getByDomain_href ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\BzListingTravels\Listing getByMerchant_href ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\BzListingTravels\Listing getByObject_type ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\BzListingTravels\Listing getByName ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\BzListingTravels\Listing getByTitle ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\BzListingTravels\Listing getBySolution_name ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\BzListingTravels\Listing getByHeadline ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\BzListingTravels\Listing getByShortDescription ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\BzListingTravels\Listing getByIsPromoted ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\BzListingTravels\Listing getByIsPremium ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\BzListingTravels\Listing getByIsFeatured ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\BzListingTravels\Listing getByIsClaimed ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\BzListingTravels\Listing getByObjectClassName ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\BzListingTravels\Listing getByTag_cloud ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\BzListingTravels\Listing getByType_display_name ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\BzListingTravels\Listing getByAddress_line_1 ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\BzListingTravels\Listing getByAddress_line_2 ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\BzListingTravels\Listing getByAddress_line_3 ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\BzListingTravels\Listing getByCity ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\BzListingTravels\Listing getByState ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\BzListingTravels\Listing getByZip ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\BzListingTravels\Listing getByCountry ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\BzListingTravels\Listing getByAddressGeoLocation ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\BzListingTravels\Listing getByMainContactNumber ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\BzListingTravels\Listing getByWebsiteUrl ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\BzListingTravels\Listing getByHoursOfOperation ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\BzListingTravels\Listing getByEmail ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\BzListingTravels\Listing getByAdditional_phones ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\BzListingTravels\Listing getByServices ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\BzListingTravels\Listing getByLongDescription ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\BzListingTravels\Listing getByRating ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\BzListingTravels\Listing getByImages ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\BzListingTravels\Listing getByVideos_local ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\BzListingTravels\Listing getByBannerImages ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\BzListingTravels\Listing getByTags ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\BzListingTravels\Listing getByCategory ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\BzListingTravels\Listing getByExternal_media ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\BzListingTravels\Listing getBySocial_media ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\BzListingTravels\Listing getByBenefits ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\BzListingTravels\Listing getByFeatures ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\BzListingTravels\Listing getByTestimonials ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\BzListingTravels\Listing getByPageBackGroundImage ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\BzListingTravels\Listing getByPageBackgroundColour ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\BzListingTravels\Listing getBySlide ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\BzListingTravels\Listing getByLogo_image ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\BzListingTravels\Listing getByLogo_tagline ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\BzListingTravels\Listing getByDisplayOnHomePage ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\BzListingTravels\Listing getByListing_inherits_from ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\BzListingTravels\Listing getByDeal_inherits_from ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\BzListingTravels\Listing getByEvent_inherits_from ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\BzListingTravels\Listing getByAudio_inherits_from ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\BzListingTravels\Listing getByVideo_inherits_from ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\BzListingTravels\Listing getByArticle_inherits_from ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\BzListingTravels\Listing getByNews_inherits_from ($value, $limit = 0) 
*/

class BzListingTravels extends \Website\Model\DataObject\BzConcreteListings implements \Pimcore\Model\DataObject\DirtyIndicatorInterface {



use \Pimcore\Model\DataObject\Traits\DirtyIndicatorTrait;

protected $o_classId = "49";
protected $o_className = "BzListingTravels";
protected $domain_name;
protected $market_name;
protected $inherited_from;
protected $market_area_href;
protected $domain_href;
protected $merchant_href;
protected $object_type;
protected $name;
protected $title;
protected $solution_name;
protected $headline;
protected $shortDescription;
protected $isPromoted;
protected $isPremium;
protected $isFeatured;
protected $isClaimed;
protected $ObjectClassName;
protected $tag_cloud;
protected $type_display_name;
protected $address_line_1;
protected $address_line_2;
protected $address_line_3;
protected $city;
protected $state;
protected $zip;
protected $country;
protected $addressGeoLocation;
protected $mainContactNumber;
protected $websiteUrl;
protected $hoursOfOperation;
protected $email;
protected $additional_phones;
protected $services;
protected $longDescription;
protected $rating;
protected $images;
protected $videos_local;
protected $bannerImages;
protected $tags;
protected $category;
protected $external_media;
protected $social_media;
protected $benefits;
protected $features;
protected $testimonials;
protected $pageBackGroundImage;
protected $pageBackgroundColour;
protected $slide;
protected $logo_image;
protected $logo_tagline;
protected $displayOnHomePage;
protected $listing_inherits_from;
protected $deal_inherits_from;
protected $event_inherits_from;
protected $audio_inherits_from;
protected $video_inherits_from;
protected $article_inherits_from;
protected $news_inherits_from;


/**
* @param array $values
* @return \Pimcore\Model\DataObject\BzListingTravels
*/
public static function create($values = array()) {
	$object = new static();
	$object->setValues($values);
	return $object;
}

/**
* Get domain_name - domain_name
* @return string
*/
public function getDomain_name () {
	$preValue = $this->preGetValue("domain_name"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->domain_name;
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set domain_name - domain_name
* @param string $domain_name
* @return \Pimcore\Model\DataObject\BzListingTravels
*/
public function setDomain_name ($domain_name) {
	$fd = $this->getClass()->getFieldDefinition("domain_name");
	$this->domain_name = $domain_name;
	return $this;
}

/**
* Get market_name - market_name
* @return string
*/
public function getMarket_name () {
	$preValue = $this->preGetValue("market_name"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->market_name;
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set market_name - market_name
* @param string $market_name
* @return \Pimcore\Model\DataObject\BzListingTravels
*/
public function setMarket_name ($market_name) {
	$fd = $this->getClass()->getFieldDefinition("market_name");
	$this->market_name = $market_name;
	return $this;
}

/**
* Get inherited_from - Inherited From Listing
* @return \Pimcore\Model\DataObject\BzListing2
*/
public function getInherited_from () {
	$preValue = $this->preGetValue("inherited_from"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->getClass()->getFieldDefinition("inherited_from")->preGetData($this);
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set inherited_from - Inherited From Listing
* @param \Pimcore\Model\DataObject\BzListing2 $inherited_from
* @return \Pimcore\Model\DataObject\BzListingTravels
*/
public function setInherited_from ($inherited_from) {
	$fd = $this->getClass()->getFieldDefinition("inherited_from");
	$currentData = $this->getInherited_from();
	$isEqual = $fd->isEqual($currentData, $inherited_from);
	if (!$isEqual) {
		$this->markFieldDirty("inherited_from", true);
	}
	$this->inherited_from = $fd->preSetData($this, $inherited_from);
	return $this;
}

/**
* Get market_area_href - market_area_href
* @return \Pimcore\Model\DataObject\BzMarketArea
*/
public function getMarket_area_href () {
	$preValue = $this->preGetValue("market_area_href"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->getClass()->getFieldDefinition("market_area_href")->preGetData($this);
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set market_area_href - market_area_href
* @param \Pimcore\Model\DataObject\BzMarketArea $market_area_href
* @return \Pimcore\Model\DataObject\BzListingTravels
*/
public function setMarket_area_href ($market_area_href) {
	$fd = $this->getClass()->getFieldDefinition("market_area_href");
	$currentData = $this->getMarket_area_href();
	$isEqual = $fd->isEqual($currentData, $market_area_href);
	if (!$isEqual) {
		$this->markFieldDirty("market_area_href", true);
	}
	$this->market_area_href = $fd->preSetData($this, $market_area_href);
	return $this;
}

/**
* Get domain_href - Domain Href
* @return \Pimcore\Model\DataObject\BzDomain
*/
public function getDomain_href () {
	$preValue = $this->preGetValue("domain_href"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->getClass()->getFieldDefinition("domain_href")->preGetData($this);
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set domain_href - Domain Href
* @param \Pimcore\Model\DataObject\BzDomain $domain_href
* @return \Pimcore\Model\DataObject\BzListingTravels
*/
public function setDomain_href ($domain_href) {
	$fd = $this->getClass()->getFieldDefinition("domain_href");
	$currentData = $this->getDomain_href();
	$isEqual = $fd->isEqual($currentData, $domain_href);
	if (!$isEqual) {
		$this->markFieldDirty("domain_href", true);
	}
	$this->domain_href = $fd->preSetData($this, $domain_href);
	return $this;
}

/**
* Get merchant_href - Merchant Href
* @return \Pimcore\Model\DataObject\BzMerchant
*/
public function getMerchant_href () {
	$preValue = $this->preGetValue("merchant_href"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->getClass()->getFieldDefinition("merchant_href")->preGetData($this);
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set merchant_href - Merchant Href
* @param \Pimcore\Model\DataObject\BzMerchant $merchant_href
* @return \Pimcore\Model\DataObject\BzListingTravels
*/
public function setMerchant_href ($merchant_href) {
	$fd = $this->getClass()->getFieldDefinition("merchant_href");
	$currentData = $this->getMerchant_href();
	$isEqual = $fd->isEqual($currentData, $merchant_href);
	if (!$isEqual) {
		$this->markFieldDirty("merchant_href", true);
	}
	$this->merchant_href = $fd->preSetData($this, $merchant_href);
	return $this;
}

/**
* Get object_type - Object Type
* @return string
*/
public function getObject_type () {
	$preValue = $this->preGetValue("object_type"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->object_type;
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set object_type - Object Type
* @param string $object_type
* @return \Pimcore\Model\DataObject\BzListingTravels
*/
public function setObject_type ($object_type) {
	$fd = $this->getClass()->getFieldDefinition("object_type");
	$this->object_type = $object_type;
	return $this;
}

/**
* Get name - Name
* @return string
*/
public function getName () {
	$preValue = $this->preGetValue("name"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->name;
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set name - Name
* @param string $name
* @return \Pimcore\Model\DataObject\BzListingTravels
*/
public function setName ($name) {
	$fd = $this->getClass()->getFieldDefinition("name");
	$this->name = $name;
	return $this;
}

/**
* Get title - Title
* @return string
*/
public function getTitle () {
	$preValue = $this->preGetValue("title"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->title;
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set title - Title
* @param string $title
* @return \Pimcore\Model\DataObject\BzListingTravels
*/
public function setTitle ($title) {
	$fd = $this->getClass()->getFieldDefinition("title");
	$this->title = $title;
	return $this;
}

/**
* Get solution_name - Solution Name
* @return string
*/
public function getSolution_name () {
	$preValue = $this->preGetValue("solution_name"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->solution_name;
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set solution_name - Solution Name
* @param string $solution_name
* @return \Pimcore\Model\DataObject\BzListingTravels
*/
public function setSolution_name ($solution_name) {
	$fd = $this->getClass()->getFieldDefinition("solution_name");
	$this->solution_name = $solution_name;
	return $this;
}

/**
* Get headline - headline
* @return string
*/
public function getHeadline () {
	$preValue = $this->preGetValue("headline"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->headline;
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set headline - headline
* @param string $headline
* @return \Pimcore\Model\DataObject\BzListingTravels
*/
public function setHeadline ($headline) {
	$fd = $this->getClass()->getFieldDefinition("headline");
	$this->headline = $headline;
	return $this;
}

/**
* Get shortDescription - shortDescription
* @return string
*/
public function getShortDescription () {
	$preValue = $this->preGetValue("shortDescription"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->shortDescription;
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set shortDescription - shortDescription
* @param string $shortDescription
* @return \Pimcore\Model\DataObject\BzListingTravels
*/
public function setShortDescription ($shortDescription) {
	$fd = $this->getClass()->getFieldDefinition("shortDescription");
	$this->shortDescription = $shortDescription;
	return $this;
}

/**
* Get isPromoted - isPromoted
* @return boolean
*/
public function getIsPromoted () {
	$preValue = $this->preGetValue("isPromoted"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->isPromoted;
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set isPromoted - isPromoted
* @param boolean $isPromoted
* @return \Pimcore\Model\DataObject\BzListingTravels
*/
public function setIsPromoted ($isPromoted) {
	$fd = $this->getClass()->getFieldDefinition("isPromoted");
	$this->isPromoted = $isPromoted;
	return $this;
}

/**
* Get isPremium - isPremium
* @return boolean
*/
public function getIsPremium () {
	$preValue = $this->preGetValue("isPremium"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->isPremium;
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set isPremium - isPremium
* @param boolean $isPremium
* @return \Pimcore\Model\DataObject\BzListingTravels
*/
public function setIsPremium ($isPremium) {
	$fd = $this->getClass()->getFieldDefinition("isPremium");
	$this->isPremium = $isPremium;
	return $this;
}

/**
* Get isFeatured - isFeatured
* @return boolean
*/
public function getIsFeatured () {
	$preValue = $this->preGetValue("isFeatured"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->isFeatured;
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set isFeatured - isFeatured
* @param boolean $isFeatured
* @return \Pimcore\Model\DataObject\BzListingTravels
*/
public function setIsFeatured ($isFeatured) {
	$fd = $this->getClass()->getFieldDefinition("isFeatured");
	$this->isFeatured = $isFeatured;
	return $this;
}

/**
* Get isClaimed - isClaimed
* @return boolean
*/
public function getIsClaimed () {
	$preValue = $this->preGetValue("isClaimed"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->isClaimed;
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set isClaimed - isClaimed
* @param boolean $isClaimed
* @return \Pimcore\Model\DataObject\BzListingTravels
*/
public function setIsClaimed ($isClaimed) {
	$fd = $this->getClass()->getFieldDefinition("isClaimed");
	$this->isClaimed = $isClaimed;
	return $this;
}

/**
* Get ObjectClassName - Object Class Name
* @return string
*/
public function getObjectClassName () {
	$preValue = $this->preGetValue("ObjectClassName"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->ObjectClassName;
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set ObjectClassName - Object Class Name
* @param string $ObjectClassName
* @return \Pimcore\Model\DataObject\BzListingTravels
*/
public function setObjectClassName ($ObjectClassName) {
	$fd = $this->getClass()->getFieldDefinition("ObjectClassName");
	$this->ObjectClassName = $ObjectClassName;
	return $this;
}

/**
* Get tag_cloud - tag_cloud
* @return string
*/
public function getTag_cloud () {
	$preValue = $this->preGetValue("tag_cloud"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->tag_cloud;
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set tag_cloud - tag_cloud
* @param string $tag_cloud
* @return \Pimcore\Model\DataObject\BzListingTravels
*/
public function setTag_cloud ($tag_cloud) {
	$fd = $this->getClass()->getFieldDefinition("tag_cloud");
	$this->tag_cloud = $tag_cloud;
	return $this;
}

/**
* Get type_display_name - listings
* @return string
*/
public function getType_display_name () {
	$preValue = $this->preGetValue("type_display_name"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->type_display_name;
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set type_display_name - listings
* @param string $type_display_name
* @return \Pimcore\Model\DataObject\BzListingTravels
*/
public function setType_display_name ($type_display_name) {
	$fd = $this->getClass()->getFieldDefinition("type_display_name");
	$this->type_display_name = $type_display_name;
	return $this;
}

/**
* Get address_line_1 - address_line_1
* @return string
*/
public function getAddress_line_1 () {
	$preValue = $this->preGetValue("address_line_1"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->address_line_1;
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set address_line_1 - address_line_1
* @param string $address_line_1
* @return \Pimcore\Model\DataObject\BzListingTravels
*/
public function setAddress_line_1 ($address_line_1) {
	$fd = $this->getClass()->getFieldDefinition("address_line_1");
	$this->address_line_1 = $address_line_1;
	return $this;
}

/**
* Get address_line_2 - address_line_2
* @return string
*/
public function getAddress_line_2 () {
	$preValue = $this->preGetValue("address_line_2"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->address_line_2;
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set address_line_2 - address_line_2
* @param string $address_line_2
* @return \Pimcore\Model\DataObject\BzListingTravels
*/
public function setAddress_line_2 ($address_line_2) {
	$fd = $this->getClass()->getFieldDefinition("address_line_2");
	$this->address_line_2 = $address_line_2;
	return $this;
}

/**
* Get address_line_3 - address_line_3
* @return string
*/
public function getAddress_line_3 () {
	$preValue = $this->preGetValue("address_line_3"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->address_line_3;
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set address_line_3 - address_line_3
* @param string $address_line_3
* @return \Pimcore\Model\DataObject\BzListingTravels
*/
public function setAddress_line_3 ($address_line_3) {
	$fd = $this->getClass()->getFieldDefinition("address_line_3");
	$this->address_line_3 = $address_line_3;
	return $this;
}

/**
* Get city - city
* @return string
*/
public function getCity () {
	$preValue = $this->preGetValue("city"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->city;
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set city - city
* @param string $city
* @return \Pimcore\Model\DataObject\BzListingTravels
*/
public function setCity ($city) {
	$fd = $this->getClass()->getFieldDefinition("city");
	$this->city = $city;
	return $this;
}

/**
* Get state - State
* @return string
*/
public function getState () {
	$preValue = $this->preGetValue("state"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->state;
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set state - State
* @param string $state
* @return \Pimcore\Model\DataObject\BzListingTravels
*/
public function setState ($state) {
	$fd = $this->getClass()->getFieldDefinition("state");
	$this->state = $state;
	return $this;
}

/**
* Get zip - zip
* @return string
*/
public function getZip () {
	$preValue = $this->preGetValue("zip"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->zip;
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set zip - zip
* @param string $zip
* @return \Pimcore\Model\DataObject\BzListingTravels
*/
public function setZip ($zip) {
	$fd = $this->getClass()->getFieldDefinition("zip");
	$this->zip = $zip;
	return $this;
}

/**
* Get country - country
* @return string
*/
public function getCountry () {
	$preValue = $this->preGetValue("country"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->country;
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set country - country
* @param string $country
* @return \Pimcore\Model\DataObject\BzListingTravels
*/
public function setCountry ($country) {
	$fd = $this->getClass()->getFieldDefinition("country");
	$this->country = $country;
	return $this;
}

/**
* Get addressGeoLocation - addressGeoLocation
* @return \Pimcore\Model\DataObject\Data\Geopoint
*/
public function getAddressGeoLocation () {
	$preValue = $this->preGetValue("addressGeoLocation"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->addressGeoLocation;
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set addressGeoLocation - addressGeoLocation
* @param \Pimcore\Model\DataObject\Data\Geopoint $addressGeoLocation
* @return \Pimcore\Model\DataObject\BzListingTravels
*/
public function setAddressGeoLocation ($addressGeoLocation) {
	$fd = $this->getClass()->getFieldDefinition("addressGeoLocation");
	$this->addressGeoLocation = $addressGeoLocation;
	return $this;
}

/**
* Get mainContactNumber - mainContactNumber
* @return string
*/
public function getMainContactNumber () {
	$preValue = $this->preGetValue("mainContactNumber"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->mainContactNumber;
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set mainContactNumber - mainContactNumber
* @param string $mainContactNumber
* @return \Pimcore\Model\DataObject\BzListingTravels
*/
public function setMainContactNumber ($mainContactNumber) {
	$fd = $this->getClass()->getFieldDefinition("mainContactNumber");
	$this->mainContactNumber = $mainContactNumber;
	return $this;
}

/**
* Get websiteUrl - websiteUrl
* @return string
*/
public function getWebsiteUrl () {
	$preValue = $this->preGetValue("websiteUrl"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->websiteUrl;
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set websiteUrl - websiteUrl
* @param string $websiteUrl
* @return \Pimcore\Model\DataObject\BzListingTravels
*/
public function setWebsiteUrl ($websiteUrl) {
	$fd = $this->getClass()->getFieldDefinition("websiteUrl");
	$this->websiteUrl = $websiteUrl;
	return $this;
}

/**
* Get hoursOfOperation - hoursOfOperation
* @return string
*/
public function getHoursOfOperation () {
	$preValue = $this->preGetValue("hoursOfOperation"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->getClass()->getFieldDefinition("hoursOfOperation")->preGetData($this);
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set hoursOfOperation - hoursOfOperation
* @param string $hoursOfOperation
* @return \Pimcore\Model\DataObject\BzListingTravels
*/
public function setHoursOfOperation ($hoursOfOperation) {
	$fd = $this->getClass()->getFieldDefinition("hoursOfOperation");
	$this->hoursOfOperation = $hoursOfOperation;
	return $this;
}

/**
* Get email - email
* @return string
*/
public function getEmail () {
	$preValue = $this->preGetValue("email"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->email;
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set email - email
* @param string $email
* @return \Pimcore\Model\DataObject\BzListingTravels
*/
public function setEmail ($email) {
	$fd = $this->getClass()->getFieldDefinition("email");
	$this->email = $email;
	return $this;
}

/**
* Get additional_phones - additional_phones
* @return string
*/
public function getAdditional_phones () {
	$preValue = $this->preGetValue("additional_phones"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->getClass()->getFieldDefinition("additional_phones")->preGetData($this);
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set additional_phones - additional_phones
* @param string $additional_phones
* @return \Pimcore\Model\DataObject\BzListingTravels
*/
public function setAdditional_phones ($additional_phones) {
	$fd = $this->getClass()->getFieldDefinition("additional_phones");
	$this->additional_phones = $additional_phones;
	return $this;
}

/**
* Get services - services
* @return string
*/
public function getServices () {
	$preValue = $this->preGetValue("services"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->getClass()->getFieldDefinition("services")->preGetData($this);
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set services - services
* @param string $services
* @return \Pimcore\Model\DataObject\BzListingTravels
*/
public function setServices ($services) {
	$fd = $this->getClass()->getFieldDefinition("services");
	$this->services = $services;
	return $this;
}

/**
* Get longDescription - longDescription
* @return string
*/
public function getLongDescription () {
	$preValue = $this->preGetValue("longDescription"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->getClass()->getFieldDefinition("longDescription")->preGetData($this);
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set longDescription - longDescription
* @param string $longDescription
* @return \Pimcore\Model\DataObject\BzListingTravels
*/
public function setLongDescription ($longDescription) {
	$fd = $this->getClass()->getFieldDefinition("longDescription");
	$this->longDescription = $longDescription;
	return $this;
}

/**
* Get rating - rating
* @return float
*/
public function getRating () {
	$preValue = $this->preGetValue("rating"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->rating;
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set rating - rating
* @param float $rating
* @return \Pimcore\Model\DataObject\BzListingTravels
*/
public function setRating ($rating) {
	$fd = $this->getClass()->getFieldDefinition("rating");
	$this->rating = $rating;
	return $this;
}

/**
* Get images - images
* @return \Pimcore\Model\Asset\Image[]
*/
public function getImages () {
	$preValue = $this->preGetValue("images"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->getClass()->getFieldDefinition("images")->preGetData($this);
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set images - images
* @param \Pimcore\Model\Asset\Image[] $images
* @return \Pimcore\Model\DataObject\BzListingTravels
*/
public function setImages ($images) {
	$fd = $this->getClass()->getFieldDefinition("images");
	$currentData = $this->getImages();
	$isEqual = $fd->isEqual($currentData, $images);
	if (!$isEqual) {
		$this->markFieldDirty("images", true);
	}
	$this->images = $fd->preSetData($this, $images);
	return $this;
}

/**
* Get videos_local - videos
* @return \Pimcore\Model\Asset\Video[]
*/
public function getVideos_local () {
	$preValue = $this->preGetValue("videos_local"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->getClass()->getFieldDefinition("videos_local")->preGetData($this);
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set videos_local - videos
* @param \Pimcore\Model\Asset\Video[] $videos_local
* @return \Pimcore\Model\DataObject\BzListingTravels
*/
public function setVideos_local ($videos_local) {
	$fd = $this->getClass()->getFieldDefinition("videos_local");
	$currentData = $this->getVideos_local();
	$isEqual = $fd->isEqual($currentData, $videos_local);
	if (!$isEqual) {
		$this->markFieldDirty("videos_local", true);
	}
	$this->videos_local = $fd->preSetData($this, $videos_local);
	return $this;
}

/**
* Get bannerImages - Banner Images
* @return \Pimcore\Model\Asset\Image[]
*/
public function getBannerImages () {
	$preValue = $this->preGetValue("bannerImages"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->getClass()->getFieldDefinition("bannerImages")->preGetData($this);
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set bannerImages - Banner Images
* @param \Pimcore\Model\Asset\Image[] $bannerImages
* @return \Pimcore\Model\DataObject\BzListingTravels
*/
public function setBannerImages ($bannerImages) {
	$fd = $this->getClass()->getFieldDefinition("bannerImages");
	$currentData = $this->getBannerImages();
	$isEqual = $fd->isEqual($currentData, $bannerImages);
	if (!$isEqual) {
		$this->markFieldDirty("bannerImages", true);
	}
	$this->bannerImages = $fd->preSetData($this, $bannerImages);
	return $this;
}

/**
* Get tags - tags
* @return \Pimcore\Model\DataObject\BzTags[]
*/
public function getTags () {
	$preValue = $this->preGetValue("tags"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->getClass()->getFieldDefinition("tags")->preGetData($this);
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set tags - tags
* @param \Pimcore\Model\DataObject\BzTags[] $tags
* @return \Pimcore\Model\DataObject\BzListingTravels
*/
public function setTags ($tags) {
	$fd = $this->getClass()->getFieldDefinition("tags");
	$currentData = $this->getTags();
	$isEqual = $fd->isEqual($currentData, $tags);
	if (!$isEqual) {
		$this->markFieldDirty("tags", true);
	}
	$this->tags = $fd->preSetData($this, $tags);
	return $this;
}

/**
* Get category - category
* @return \Pimcore\Model\DataObject\BzCategories[]
*/
public function getCategory () {
	$preValue = $this->preGetValue("category"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->getClass()->getFieldDefinition("category")->preGetData($this);
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set category - category
* @param \Pimcore\Model\DataObject\BzCategories[] $category
* @return \Pimcore\Model\DataObject\BzListingTravels
*/
public function setCategory ($category) {
	$fd = $this->getClass()->getFieldDefinition("category");
	$currentData = $this->getCategory();
	$isEqual = $fd->isEqual($currentData, $category);
	if (!$isEqual) {
		$this->markFieldDirty("category", true);
	}
	$this->category = $fd->preSetData($this, $category);
	return $this;
}

/**
* @return \Pimcore\Model\DataObject\Fieldcollection
*/
public function getExternal_media () {
	$preValue = $this->preGetValue("external_media"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { return $preValue;}
	$data = $this->getClass()->getFieldDefinition("external_media")->preGetData($this);
	 return $data;
}

/**
* Set external_media - External Media
* @param \Pimcore\Model\DataObject\Fieldcollection $external_media
* @return \Pimcore\Model\DataObject\BzListingTravels
*/
public function setExternal_media ($external_media) {
	$fd = $this->getClass()->getFieldDefinition("external_media");
	$this->external_media = $fd->preSetData($this, $external_media);
	return $this;
}

/**
* @return \Pimcore\Model\DataObject\Fieldcollection
*/
public function getSocial_media () {
	$preValue = $this->preGetValue("social_media"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { return $preValue;}
	$data = $this->getClass()->getFieldDefinition("social_media")->preGetData($this);
	 return $data;
}

/**
* Set social_media - Social Media
* @param \Pimcore\Model\DataObject\Fieldcollection $social_media
* @return \Pimcore\Model\DataObject\BzListingTravels
*/
public function setSocial_media ($social_media) {
	$fd = $this->getClass()->getFieldDefinition("social_media");
	$this->social_media = $fd->preSetData($this, $social_media);
	return $this;
}

/**
* @return \Pimcore\Model\DataObject\Fieldcollection
*/
public function getBenefits () {
	$preValue = $this->preGetValue("benefits"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { return $preValue;}
	$data = $this->getClass()->getFieldDefinition("benefits")->preGetData($this);
	 return $data;
}

/**
* Set benefits - Benefits
* @param \Pimcore\Model\DataObject\Fieldcollection $benefits
* @return \Pimcore\Model\DataObject\BzListingTravels
*/
public function setBenefits ($benefits) {
	$fd = $this->getClass()->getFieldDefinition("benefits");
	$this->benefits = $fd->preSetData($this, $benefits);
	return $this;
}

/**
* @return \Pimcore\Model\DataObject\Fieldcollection
*/
public function getFeatures () {
	$preValue = $this->preGetValue("features"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { return $preValue;}
	$data = $this->getClass()->getFieldDefinition("features")->preGetData($this);
	 return $data;
}

/**
* Set features - Features
* @param \Pimcore\Model\DataObject\Fieldcollection $features
* @return \Pimcore\Model\DataObject\BzListingTravels
*/
public function setFeatures ($features) {
	$fd = $this->getClass()->getFieldDefinition("features");
	$this->features = $fd->preSetData($this, $features);
	return $this;
}

/**
* @return \Pimcore\Model\DataObject\Fieldcollection
*/
public function getTestimonials () {
	$preValue = $this->preGetValue("testimonials"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { return $preValue;}
	$data = $this->getClass()->getFieldDefinition("testimonials")->preGetData($this);
	 return $data;
}

/**
* Set testimonials - Testimonials
* @param \Pimcore\Model\DataObject\Fieldcollection $testimonials
* @return \Pimcore\Model\DataObject\BzListingTravels
*/
public function setTestimonials ($testimonials) {
	$fd = $this->getClass()->getFieldDefinition("testimonials");
	$this->testimonials = $fd->preSetData($this, $testimonials);
	return $this;
}

/**
* Get pageBackGroundImage - Page BackGround Image
* @return \Pimcore\Model\Asset\Image
*/
public function getPageBackGroundImage () {
	$preValue = $this->preGetValue("pageBackGroundImage"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->pageBackGroundImage;
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set pageBackGroundImage - Page BackGround Image
* @param \Pimcore\Model\Asset\Image $pageBackGroundImage
* @return \Pimcore\Model\DataObject\BzListingTravels
*/
public function setPageBackGroundImage ($pageBackGroundImage) {
	$fd = $this->getClass()->getFieldDefinition("pageBackGroundImage");
	$this->pageBackGroundImage = $pageBackGroundImage;
	return $this;
}

/**
* Get pageBackgroundColour - Page Background Colour
* @return string
*/
public function getPageBackgroundColour () {
	$preValue = $this->preGetValue("pageBackgroundColour"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->pageBackgroundColour;
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set pageBackgroundColour - Page Background Colour
* @param string $pageBackgroundColour
* @return \Pimcore\Model\DataObject\BzListingTravels
*/
public function setPageBackgroundColour ($pageBackgroundColour) {
	$fd = $this->getClass()->getFieldDefinition("pageBackgroundColour");
	$this->pageBackgroundColour = $pageBackgroundColour;
	return $this;
}

/**
* @return \Pimcore\Model\DataObject\Fieldcollection
*/
public function getSlide () {
	$preValue = $this->preGetValue("slide"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { return $preValue;}
	$data = $this->getClass()->getFieldDefinition("slide")->preGetData($this);
	 return $data;
}

/**
* Set slide - Slide
* @param \Pimcore\Model\DataObject\Fieldcollection $slide
* @return \Pimcore\Model\DataObject\BzListingTravels
*/
public function setSlide ($slide) {
	$fd = $this->getClass()->getFieldDefinition("slide");
	$this->slide = $fd->preSetData($this, $slide);
	return $this;
}

/**
* Get logo_image - logo_image
* @return \Pimcore\Model\Asset\Image
*/
public function getLogo_image () {
	$preValue = $this->preGetValue("logo_image"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->logo_image;
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set logo_image - logo_image
* @param \Pimcore\Model\Asset\Image $logo_image
* @return \Pimcore\Model\DataObject\BzListingTravels
*/
public function setLogo_image ($logo_image) {
	$fd = $this->getClass()->getFieldDefinition("logo_image");
	$this->logo_image = $logo_image;
	return $this;
}

/**
* Get logo_tagline - logo_tagline
* @return string
*/
public function getLogo_tagline () {
	$preValue = $this->preGetValue("logo_tagline"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->logo_tagline;
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set logo_tagline - logo_tagline
* @param string $logo_tagline
* @return \Pimcore\Model\DataObject\BzListingTravels
*/
public function setLogo_tagline ($logo_tagline) {
	$fd = $this->getClass()->getFieldDefinition("logo_tagline");
	$this->logo_tagline = $logo_tagline;
	return $this;
}

/**
* Get displayOnHomePage - displayOnHomePage
* @return boolean
*/
public function getDisplayOnHomePage () {
	$preValue = $this->preGetValue("displayOnHomePage"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->displayOnHomePage;
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set displayOnHomePage - displayOnHomePage
* @param boolean $displayOnHomePage
* @return \Pimcore\Model\DataObject\BzListingTravels
*/
public function setDisplayOnHomePage ($displayOnHomePage) {
	$fd = $this->getClass()->getFieldDefinition("displayOnHomePage");
	$this->displayOnHomePage = $displayOnHomePage;
	return $this;
}

/**
* Get listing_inherits_from - Listing Inherits From
* @return \Pimcore\Model\DataObject\BzListing2
*/
public function getListing_inherits_from () {
	$preValue = $this->preGetValue("listing_inherits_from"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->getClass()->getFieldDefinition("listing_inherits_from")->preGetData($this);
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set listing_inherits_from - Listing Inherits From
* @param \Pimcore\Model\DataObject\BzListing2 $listing_inherits_from
* @return \Pimcore\Model\DataObject\BzListingTravels
*/
public function setListing_inherits_from ($listing_inherits_from) {
	$fd = $this->getClass()->getFieldDefinition("listing_inherits_from");
	$currentData = $this->getListing_inherits_from();
	$isEqual = $fd->isEqual($currentData, $listing_inherits_from);
	if (!$isEqual) {
		$this->markFieldDirty("listing_inherits_from", true);
	}
	$this->listing_inherits_from = $fd->preSetData($this, $listing_inherits_from);
	return $this;
}

/**
* Get deal_inherits_from - Deal Inherits From
* @return \Pimcore\Model\DataObject\BzDeals
*/
public function getDeal_inherits_from () {
	$preValue = $this->preGetValue("deal_inherits_from"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->getClass()->getFieldDefinition("deal_inherits_from")->preGetData($this);
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set deal_inherits_from - Deal Inherits From
* @param \Pimcore\Model\DataObject\BzDeals $deal_inherits_from
* @return \Pimcore\Model\DataObject\BzListingTravels
*/
public function setDeal_inherits_from ($deal_inherits_from) {
	$fd = $this->getClass()->getFieldDefinition("deal_inherits_from");
	$currentData = $this->getDeal_inherits_from();
	$isEqual = $fd->isEqual($currentData, $deal_inherits_from);
	if (!$isEqual) {
		$this->markFieldDirty("deal_inherits_from", true);
	}
	$this->deal_inherits_from = $fd->preSetData($this, $deal_inherits_from);
	return $this;
}

/**
* Get event_inherits_from - event_inherits_from
* @return \Pimcore\Model\DataObject\BzEvents
*/
public function getEvent_inherits_from () {
	$preValue = $this->preGetValue("event_inherits_from"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->getClass()->getFieldDefinition("event_inherits_from")->preGetData($this);
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set event_inherits_from - event_inherits_from
* @param \Pimcore\Model\DataObject\BzEvents $event_inherits_from
* @return \Pimcore\Model\DataObject\BzListingTravels
*/
public function setEvent_inherits_from ($event_inherits_from) {
	$fd = $this->getClass()->getFieldDefinition("event_inherits_from");
	$currentData = $this->getEvent_inherits_from();
	$isEqual = $fd->isEqual($currentData, $event_inherits_from);
	if (!$isEqual) {
		$this->markFieldDirty("event_inherits_from", true);
	}
	$this->event_inherits_from = $fd->preSetData($this, $event_inherits_from);
	return $this;
}

/**
* Get audio_inherits_from - audio_inherits_from
* @return \Pimcore\Model\DataObject\BzAudio
*/
public function getAudio_inherits_from () {
	$preValue = $this->preGetValue("audio_inherits_from"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->getClass()->getFieldDefinition("audio_inherits_from")->preGetData($this);
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set audio_inherits_from - audio_inherits_from
* @param \Pimcore\Model\DataObject\BzAudio $audio_inherits_from
* @return \Pimcore\Model\DataObject\BzListingTravels
*/
public function setAudio_inherits_from ($audio_inherits_from) {
	$fd = $this->getClass()->getFieldDefinition("audio_inherits_from");
	$currentData = $this->getAudio_inherits_from();
	$isEqual = $fd->isEqual($currentData, $audio_inherits_from);
	if (!$isEqual) {
		$this->markFieldDirty("audio_inherits_from", true);
	}
	$this->audio_inherits_from = $fd->preSetData($this, $audio_inherits_from);
	return $this;
}

/**
* Get video_inherits_from - video_inherits_from
* @return \Pimcore\Model\DataObject\BzVideo
*/
public function getVideo_inherits_from () {
	$preValue = $this->preGetValue("video_inherits_from"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->getClass()->getFieldDefinition("video_inherits_from")->preGetData($this);
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set video_inherits_from - video_inherits_from
* @param \Pimcore\Model\DataObject\BzVideo $video_inherits_from
* @return \Pimcore\Model\DataObject\BzListingTravels
*/
public function setVideo_inherits_from ($video_inherits_from) {
	$fd = $this->getClass()->getFieldDefinition("video_inherits_from");
	$currentData = $this->getVideo_inherits_from();
	$isEqual = $fd->isEqual($currentData, $video_inherits_from);
	if (!$isEqual) {
		$this->markFieldDirty("video_inherits_from", true);
	}
	$this->video_inherits_from = $fd->preSetData($this, $video_inherits_from);
	return $this;
}

/**
* Get article_inherits_from - Article Inherits From
* @return \Pimcore\Model\DataObject\BzBlogArticle
*/
public function getArticle_inherits_from () {
	$preValue = $this->preGetValue("article_inherits_from"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->getClass()->getFieldDefinition("article_inherits_from")->preGetData($this);
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set article_inherits_from - Article Inherits From
* @param \Pimcore\Model\DataObject\BzBlogArticle $article_inherits_from
* @return \Pimcore\Model\DataObject\BzListingTravels
*/
public function setArticle_inherits_from ($article_inherits_from) {
	$fd = $this->getClass()->getFieldDefinition("article_inherits_from");
	$currentData = $this->getArticle_inherits_from();
	$isEqual = $fd->isEqual($currentData, $article_inherits_from);
	if (!$isEqual) {
		$this->markFieldDirty("article_inherits_from", true);
	}
	$this->article_inherits_from = $fd->preSetData($this, $article_inherits_from);
	return $this;
}

/**
* Get news_inherits_from - News Inherits From
* @return \Pimcore\Model\DataObject\News
*/
public function getNews_inherits_from () {
	$preValue = $this->preGetValue("news_inherits_from"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->getClass()->getFieldDefinition("news_inherits_from")->preGetData($this);
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set news_inherits_from - News Inherits From
* @param \Pimcore\Model\DataObject\News $news_inherits_from
* @return \Pimcore\Model\DataObject\BzListingTravels
*/
public function setNews_inherits_from ($news_inherits_from) {
	$fd = $this->getClass()->getFieldDefinition("news_inherits_from");
	$currentData = $this->getNews_inherits_from();
	$isEqual = $fd->isEqual($currentData, $news_inherits_from);
	if (!$isEqual) {
		$this->markFieldDirty("news_inherits_from", true);
	}
	$this->news_inherits_from = $fd->preSetData($this, $news_inherits_from);
	return $this;
}

protected static $_relationFields = array (
  'inherited_from' => 
  array (
    'type' => 'manyToOneRelation',
  ),
  'market_area_href' => 
  array (
    'type' => 'manyToOneRelation',
  ),
  'domain_href' => 
  array (
    'type' => 'manyToOneRelation',
  ),
  'merchant_href' => 
  array (
    'type' => 'manyToOneRelation',
  ),
  'images' => 
  array (
    'type' => 'manyToManyRelation',
  ),
  'videos_local' => 
  array (
    'type' => 'manyToManyRelation',
  ),
  'bannerImages' => 
  array (
    'type' => 'manyToManyRelation',
  ),
  'tags' => 
  array (
    'type' => 'manyToManyRelation',
  ),
  'category' => 
  array (
    'type' => 'manyToManyRelation',
  ),
  'listing_inherits_from' => 
  array (
    'type' => 'manyToOneRelation',
  ),
  'deal_inherits_from' => 
  array (
    'type' => 'manyToOneRelation',
  ),
  'event_inherits_from' => 
  array (
    'type' => 'manyToOneRelation',
  ),
  'audio_inherits_from' => 
  array (
    'type' => 'manyToOneRelation',
  ),
  'video_inherits_from' => 
  array (
    'type' => 'manyToOneRelation',
  ),
  'article_inherits_from' => 
  array (
    'type' => 'manyToOneRelation',
  ),
  'news_inherits_from' => 
  array (
    'type' => 'manyToOneRelation',
  ),
);

protected $lazyLoadedFields = array (
);

}

