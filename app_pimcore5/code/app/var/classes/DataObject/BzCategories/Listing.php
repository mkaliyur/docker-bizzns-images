<?php 

namespace Pimcore\Model\DataObject\BzCategories;

use Pimcore\Model\DataObject;

/**
 * @method DataObject\BzCategories current()
 * @method DataObject\BzCategories[] load()
 */

class Listing extends DataObject\Listing\Concrete {

protected $classId = "16";
protected $className = "BzCategories";


}
