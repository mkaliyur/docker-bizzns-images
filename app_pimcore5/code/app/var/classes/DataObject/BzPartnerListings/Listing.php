<?php 

namespace Pimcore\Model\DataObject\BzPartnerListings;

use Pimcore\Model\DataObject;

/**
 * @method DataObject\BzPartnerListings current()
 * @method DataObject\BzPartnerListings[] load()
 */

class Listing extends DataObject\Listing\Concrete {

protected $classId = "7";
protected $className = "BzPartnerListings";


}
