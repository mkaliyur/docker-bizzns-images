<?php 

namespace Pimcore\Model\DataObject\BzReservables;

use Pimcore\Model\DataObject;

/**
 * @method DataObject\BzReservables current()
 * @method DataObject\BzReservables[] load()
 */

class Listing extends DataObject\Listing\Concrete {

protected $classId = "43";
protected $className = "BzReservables";


}
