<?php 

/** 
* Generated at: 2019-08-14T11:44:47+02:00
* Inheritance: no
* Variants: no
* Changed by: root (2)
* IP: ::1


Fields Summary: 
- volume_input1 [input]
- volume_input2 [input]
- volume_input3 [input]
- volume_input4 [input]
- volume_input5 [input]
- name [input]
- object_type [input]
*/ 

namespace Pimcore\Model\DataObject;



/**
* @method static \Pimcore\Model\DataObject\BzSystemComponentVolumes\Listing getByVolume_input1 ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\BzSystemComponentVolumes\Listing getByVolume_input2 ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\BzSystemComponentVolumes\Listing getByVolume_input3 ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\BzSystemComponentVolumes\Listing getByVolume_input4 ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\BzSystemComponentVolumes\Listing getByVolume_input5 ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\BzSystemComponentVolumes\Listing getByName ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\BzSystemComponentVolumes\Listing getByObject_type ($value, $limit = 0) 
*/

class BzSystemComponentVolumes extends Concrete implements \Pimcore\Model\DataObject\DirtyIndicatorInterface {



use \Pimcore\Model\DataObject\Traits\DirtyIndicatorTrait;

protected $o_classId = "44";
protected $o_className = "BzSystemComponentVolumes";
protected $volume_input1;
protected $volume_input2;
protected $volume_input3;
protected $volume_input4;
protected $volume_input5;
protected $name;
protected $object_type;


/**
* @param array $values
* @return \Pimcore\Model\DataObject\BzSystemComponentVolumes
*/
public static function create($values = array()) {
	$object = new static();
	$object->setValues($values);
	return $object;
}

/**
* Get volume_input1 - volume_input1
* @return string
*/
public function getVolume_input1 () {
	$preValue = $this->preGetValue("volume_input1"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->volume_input1;
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set volume_input1 - volume_input1
* @param string $volume_input1
* @return \Pimcore\Model\DataObject\BzSystemComponentVolumes
*/
public function setVolume_input1 ($volume_input1) {
	$fd = $this->getClass()->getFieldDefinition("volume_input1");
	$this->volume_input1 = $volume_input1;
	return $this;
}

/**
* Get volume_input2 - volume_input2
* @return string
*/
public function getVolume_input2 () {
	$preValue = $this->preGetValue("volume_input2"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->volume_input2;
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set volume_input2 - volume_input2
* @param string $volume_input2
* @return \Pimcore\Model\DataObject\BzSystemComponentVolumes
*/
public function setVolume_input2 ($volume_input2) {
	$fd = $this->getClass()->getFieldDefinition("volume_input2");
	$this->volume_input2 = $volume_input2;
	return $this;
}

/**
* Get volume_input3 - volume_input3
* @return string
*/
public function getVolume_input3 () {
	$preValue = $this->preGetValue("volume_input3"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->volume_input3;
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set volume_input3 - volume_input3
* @param string $volume_input3
* @return \Pimcore\Model\DataObject\BzSystemComponentVolumes
*/
public function setVolume_input3 ($volume_input3) {
	$fd = $this->getClass()->getFieldDefinition("volume_input3");
	$this->volume_input3 = $volume_input3;
	return $this;
}

/**
* Get volume_input4 - volume_input4
* @return string
*/
public function getVolume_input4 () {
	$preValue = $this->preGetValue("volume_input4"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->volume_input4;
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set volume_input4 - volume_input4
* @param string $volume_input4
* @return \Pimcore\Model\DataObject\BzSystemComponentVolumes
*/
public function setVolume_input4 ($volume_input4) {
	$fd = $this->getClass()->getFieldDefinition("volume_input4");
	$this->volume_input4 = $volume_input4;
	return $this;
}

/**
* Get volume_input5 - volume_input5
* @return string
*/
public function getVolume_input5 () {
	$preValue = $this->preGetValue("volume_input5"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->volume_input5;
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set volume_input5 - volume_input5
* @param string $volume_input5
* @return \Pimcore\Model\DataObject\BzSystemComponentVolumes
*/
public function setVolume_input5 ($volume_input5) {
	$fd = $this->getClass()->getFieldDefinition("volume_input5");
	$this->volume_input5 = $volume_input5;
	return $this;
}

/**
* Get name - name
* @return string
*/
public function getName () {
	$preValue = $this->preGetValue("name"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->name;
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set name - name
* @param string $name
* @return \Pimcore\Model\DataObject\BzSystemComponentVolumes
*/
public function setName ($name) {
	$fd = $this->getClass()->getFieldDefinition("name");
	$this->name = $name;
	return $this;
}

/**
* Get object_type - object_type
* @return string
*/
public function getObject_type () {
	$preValue = $this->preGetValue("object_type"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->object_type;
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set object_type - object_type
* @param string $object_type
* @return \Pimcore\Model\DataObject\BzSystemComponentVolumes
*/
public function setObject_type ($object_type) {
	$fd = $this->getClass()->getFieldDefinition("object_type");
	$this->object_type = $object_type;
	return $this;
}

protected static $_relationFields = array (
);

protected $lazyLoadedFields = array (
);

}

