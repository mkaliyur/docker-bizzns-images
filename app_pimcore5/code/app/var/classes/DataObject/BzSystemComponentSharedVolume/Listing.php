<?php 

namespace Pimcore\Model\DataObject\BzSystemComponentSharedVolume;

use Pimcore\Model\DataObject;

/**
 * @method DataObject\BzSystemComponentSharedVolume current()
 * @method DataObject\BzSystemComponentSharedVolume[] load()
 */

class Listing extends DataObject\Listing\Concrete {

protected $classId = "45";
protected $className = "BzSystemComponentSharedVolume";


}
