<?php 

namespace Pimcore\Model\DataObject\BzBlogArticle;

use Pimcore\Model\DataObject;

/**
 * @method DataObject\BzBlogArticle current()
 * @method DataObject\BzBlogArticle[] load()
 */

class Listing extends DataObject\Listing\Concrete {

protected $classId = "35";
protected $className = "BzBlogArticle";


}
