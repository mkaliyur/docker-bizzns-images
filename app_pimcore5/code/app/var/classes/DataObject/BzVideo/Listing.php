<?php 

namespace Pimcore\Model\DataObject\BzVideo;

use Pimcore\Model\DataObject;

/**
 * @method DataObject\BzVideo current()
 * @method DataObject\BzVideo[] load()
 */

class Listing extends DataObject\Listing\Concrete {

protected $classId = "23";
protected $className = "BzVideo";


}
