<?php 

namespace Pimcore\Model\DataObject\BzPartner;

use Pimcore\Model\DataObject;

/**
 * @method DataObject\BzPartner current()
 * @method DataObject\BzPartner[] load()
 */

class Listing extends DataObject\Listing\Concrete {

protected $classId = "15";
protected $className = "BzPartner";


}
