<?php 

namespace Pimcore\Model\DataObject\BzListingReviews;

use Pimcore\Model\DataObject;

/**
 * @method DataObject\BzListingReviews current()
 * @method DataObject\BzListingReviews[] load()
 */

class Listing extends DataObject\Listing\Concrete {

protected $classId = "10";
protected $className = "BzListingReviews";


}
