<?php 

namespace Pimcore\Model\DataObject\BzListingGroup;

use Pimcore\Model\DataObject;

/**
 * @method DataObject\BzListingGroup current()
 * @method DataObject\BzListingGroup[] load()
 */

class Listing extends DataObject\Listing\Concrete {

protected $classId = "8";
protected $className = "BzListingGroup";


}
