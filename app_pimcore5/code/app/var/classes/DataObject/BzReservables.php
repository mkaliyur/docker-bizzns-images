<?php 

/** 
* Generated at: 2019-06-25T11:35:01+02:00
* Inheritance: no
* Variants: no
* Changed by: root (2)
* IP: ::1


Fields Summary: 
- name [input]
- reservable_id [input]
- category [input]
- price [input]
- status [select]
- action [input]
- typedisplayname [input]
- inventory [slider]
- object_type [input]
- solution_name [input]
- rtype [select]
*/ 

namespace Pimcore\Model\DataObject;



/**
* @method static \Pimcore\Model\DataObject\BzReservables\Listing getByName ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\BzReservables\Listing getByReservable_id ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\BzReservables\Listing getByCategory ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\BzReservables\Listing getByPrice ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\BzReservables\Listing getByStatus ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\BzReservables\Listing getByAction ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\BzReservables\Listing getByTypedisplayname ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\BzReservables\Listing getByInventory ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\BzReservables\Listing getByObject_type ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\BzReservables\Listing getBySolution_name ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\BzReservables\Listing getByRtype ($value, $limit = 0) 
*/

class BzReservables extends Concrete implements \Pimcore\Model\DataObject\DirtyIndicatorInterface {



use \Pimcore\Model\DataObject\Traits\DirtyIndicatorTrait;

protected $o_classId = "43";
protected $o_className = "BzReservables";
protected $name;
protected $reservable_id;
protected $category;
protected $price;
protected $status;
protected $action;
protected $typedisplayname;
protected $inventory;
protected $object_type;
protected $solution_name;
protected $rtype;


/**
* @param array $values
* @return \Pimcore\Model\DataObject\BzReservables
*/
public static function create($values = array()) {
	$object = new static();
	$object->setValues($values);
	return $object;
}

/**
* Get name - Name
* @return string
*/
public function getName () {
	$preValue = $this->preGetValue("name"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->name;
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set name - Name
* @param string $name
* @return \Pimcore\Model\DataObject\BzReservables
*/
public function setName ($name) {
	$fd = $this->getClass()->getFieldDefinition("name");
	$this->name = $name;
	return $this;
}

/**
* Get reservable_id - Reservable Id
* @return string
*/
public function getReservable_id () {
	$preValue = $this->preGetValue("reservable_id"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->reservable_id;
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set reservable_id - Reservable Id
* @param string $reservable_id
* @return \Pimcore\Model\DataObject\BzReservables
*/
public function setReservable_id ($reservable_id) {
	$fd = $this->getClass()->getFieldDefinition("reservable_id");
	$this->reservable_id = $reservable_id;
	return $this;
}

/**
* Get category - Category
* @return string
*/
public function getCategory () {
	$preValue = $this->preGetValue("category"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->category;
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set category - Category
* @param string $category
* @return \Pimcore\Model\DataObject\BzReservables
*/
public function setCategory ($category) {
	$fd = $this->getClass()->getFieldDefinition("category");
	$this->category = $category;
	return $this;
}

/**
* Get price - Price
* @return string
*/
public function getPrice () {
	$preValue = $this->preGetValue("price"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->price;
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set price - Price
* @param string $price
* @return \Pimcore\Model\DataObject\BzReservables
*/
public function setPrice ($price) {
	$fd = $this->getClass()->getFieldDefinition("price");
	$this->price = $price;
	return $this;
}

/**
* Get status - Status
* @return string
*/
public function getStatus () {
	$preValue = $this->preGetValue("status"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->status;
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set status - Status
* @param string $status
* @return \Pimcore\Model\DataObject\BzReservables
*/
public function setStatus ($status) {
	$fd = $this->getClass()->getFieldDefinition("status");
	$this->status = $status;
	return $this;
}

/**
* Get action - action
* @return string
*/
public function getAction () {
	$preValue = $this->preGetValue("action"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->action;
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set action - action
* @param string $action
* @return \Pimcore\Model\DataObject\BzReservables
*/
public function setAction ($action) {
	$fd = $this->getClass()->getFieldDefinition("action");
	$this->action = $action;
	return $this;
}

/**
* Get typedisplayname - Type Display Name
* @return string
*/
public function getTypedisplayname () {
	$preValue = $this->preGetValue("typedisplayname"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->typedisplayname;
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set typedisplayname - Type Display Name
* @param string $typedisplayname
* @return \Pimcore\Model\DataObject\BzReservables
*/
public function setTypedisplayname ($typedisplayname) {
	$fd = $this->getClass()->getFieldDefinition("typedisplayname");
	$this->typedisplayname = $typedisplayname;
	return $this;
}

/**
* Get inventory - inventory
* @return float
*/
public function getInventory () {
	$preValue = $this->preGetValue("inventory"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->inventory;
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set inventory - inventory
* @param float $inventory
* @return \Pimcore\Model\DataObject\BzReservables
*/
public function setInventory ($inventory) {
	$fd = $this->getClass()->getFieldDefinition("inventory");
	$this->inventory = $inventory;
	return $this;
}

/**
* Get object_type - Object Type
* @return string
*/
public function getObject_type () {
	$preValue = $this->preGetValue("object_type"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->object_type;
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set object_type - Object Type
* @param string $object_type
* @return \Pimcore\Model\DataObject\BzReservables
*/
public function setObject_type ($object_type) {
	$fd = $this->getClass()->getFieldDefinition("object_type");
	$this->object_type = $object_type;
	return $this;
}

/**
* Get solution_name - Solution Name
* @return string
*/
public function getSolution_name () {
	$preValue = $this->preGetValue("solution_name"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->solution_name;
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set solution_name - Solution Name
* @param string $solution_name
* @return \Pimcore\Model\DataObject\BzReservables
*/
public function setSolution_name ($solution_name) {
	$fd = $this->getClass()->getFieldDefinition("solution_name");
	$this->solution_name = $solution_name;
	return $this;
}

/**
* Get rtype - rtype
* @return string
*/
public function getRtype () {
	$preValue = $this->preGetValue("rtype"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->rtype;
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set rtype - rtype
* @param string $rtype
* @return \Pimcore\Model\DataObject\BzReservables
*/
public function setRtype ($rtype) {
	$fd = $this->getClass()->getFieldDefinition("rtype");
	$this->rtype = $rtype;
	return $this;
}

protected static $_relationFields = array (
);

protected $lazyLoadedFields = array (
);

}

