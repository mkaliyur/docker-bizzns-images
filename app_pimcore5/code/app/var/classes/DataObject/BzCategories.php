<?php 

/** 
* Generated at: 2019-09-03T12:00:28+02:00
* Inheritance: no
* Variants: no
* Changed by: root (2)
* IP: ::1


Fields Summary: 
- parentCategory [manyToOneRelation]
- name [input]
- object_type [input]
- solution_name [input]
*/ 

namespace Pimcore\Model\DataObject;



/**
* @method static \Pimcore\Model\DataObject\BzCategories\Listing getByParentCategory ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\BzCategories\Listing getByName ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\BzCategories\Listing getByObject_type ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\BzCategories\Listing getBySolution_name ($value, $limit = 0) 
*/

class BzCategories extends Concrete implements \Pimcore\Model\DataObject\DirtyIndicatorInterface {



use \Pimcore\Model\DataObject\Traits\DirtyIndicatorTrait;

protected $o_classId = "16";
protected $o_className = "BzCategories";
protected $parentCategory;
protected $name;
protected $object_type;
protected $solution_name;


/**
* @param array $values
* @return \Pimcore\Model\DataObject\BzCategories
*/
public static function create($values = array()) {
	$object = new static();
	$object->setValues($values);
	return $object;
}

/**
* Get parentCategory - parentCategory
* @return \Pimcore\Model\DataObject\BzCategories
*/
public function getParentCategory () {
	$preValue = $this->preGetValue("parentCategory"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->getClass()->getFieldDefinition("parentCategory")->preGetData($this);
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set parentCategory - parentCategory
* @param \Pimcore\Model\DataObject\BzCategories $parentCategory
* @return \Pimcore\Model\DataObject\BzCategories
*/
public function setParentCategory ($parentCategory) {
	$fd = $this->getClass()->getFieldDefinition("parentCategory");
	$currentData = $this->getParentCategory();
	$isEqual = $fd->isEqual($currentData, $parentCategory);
	if (!$isEqual) {
		$this->markFieldDirty("parentCategory", true);
	}
	$this->parentCategory = $fd->preSetData($this, $parentCategory);
	return $this;
}

/**
* Get name - name
* @return string
*/
public function getName () {
	$preValue = $this->preGetValue("name"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->name;
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set name - name
* @param string $name
* @return \Pimcore\Model\DataObject\BzCategories
*/
public function setName ($name) {
	$fd = $this->getClass()->getFieldDefinition("name");
	$this->name = $name;
	return $this;
}

/**
* Get object_type - Object Type
* @return string
*/
public function getObject_type () {
	$preValue = $this->preGetValue("object_type"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->object_type;
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set object_type - Object Type
* @param string $object_type
* @return \Pimcore\Model\DataObject\BzCategories
*/
public function setObject_type ($object_type) {
	$fd = $this->getClass()->getFieldDefinition("object_type");
	$this->object_type = $object_type;
	return $this;
}

/**
* Get solution_name - Solution Name
* @return string
*/
public function getSolution_name () {
	$preValue = $this->preGetValue("solution_name"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->solution_name;
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set solution_name - Solution Name
* @param string $solution_name
* @return \Pimcore\Model\DataObject\BzCategories
*/
public function setSolution_name ($solution_name) {
	$fd = $this->getClass()->getFieldDefinition("solution_name");
	$this->solution_name = $solution_name;
	return $this;
}

protected static $_relationFields = array (
  'parentCategory' => 
  array (
    'type' => 'manyToOneRelation',
  ),
);

protected $lazyLoadedFields = array (
);

}

