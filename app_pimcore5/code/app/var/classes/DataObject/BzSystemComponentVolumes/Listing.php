<?php 

namespace Pimcore\Model\DataObject\BzSystemComponentVolumes;

use Pimcore\Model\DataObject;

/**
 * @method DataObject\BzSystemComponentVolumes current()
 * @method DataObject\BzSystemComponentVolumes[] load()
 */

class Listing extends DataObject\Listing\Concrete {

protected $classId = "44";
protected $className = "BzSystemComponentVolumes";


}
