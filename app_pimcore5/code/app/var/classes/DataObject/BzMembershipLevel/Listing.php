<?php 

namespace Pimcore\Model\DataObject\BzMembershipLevel;

use Pimcore\Model\DataObject;

/**
 * @method DataObject\BzMembershipLevel current()
 * @method DataObject\BzMembershipLevel[] load()
 */

class Listing extends DataObject\Listing\Concrete {

protected $classId = "28";
protected $className = "BzMembershipLevel";


}
