<?php 

namespace Pimcore\Model\DataObject\BzNews;

use Pimcore\Model\DataObject;

/**
 * @method DataObject\BzNews current()
 * @method DataObject\BzNews[] load()
 */

class Listing extends DataObject\Listing\Concrete {

protected $classId = "41";
protected $className = "BzNews";


}
