<?php 

/** 
* Generated at: 2019-03-29T14:07:57+01:00
* Inheritance: no
* Variants: no
* Changed by: root (2)
* IP: ::1


Fields Summary: 
- PublishDateTime [datetime]
- class_name [input]
- object_id [input]
- object_path [input]
- object_key [input]
- solution_id [input]
- url [input]
- event_type [input]
- is_processed [checkbox]
- name [input]
- solutionname_web [input]
- object_type [input]
*/ 

namespace Pimcore\Model\DataObject;



/**
* @method static \Pimcore\Model\DataObject\BzPublishQueue\Listing getByPublishDateTime ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\BzPublishQueue\Listing getByClass_name ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\BzPublishQueue\Listing getByObject_id ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\BzPublishQueue\Listing getByObject_path ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\BzPublishQueue\Listing getByObject_key ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\BzPublishQueue\Listing getBySolution_id ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\BzPublishQueue\Listing getByUrl ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\BzPublishQueue\Listing getByEvent_type ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\BzPublishQueue\Listing getByIs_processed ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\BzPublishQueue\Listing getByName ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\BzPublishQueue\Listing getBySolutionname_web ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\BzPublishQueue\Listing getByObject_type ($value, $limit = 0) 
*/

class BzPublishQueue extends Concrete implements \Pimcore\Model\DataObject\DirtyIndicatorInterface {



use \Pimcore\Model\DataObject\Traits\DirtyIndicatorTrait;

protected $o_classId = "33";
protected $o_className = "BzPublishQueue";
protected $PublishDateTime;
protected $class_name;
protected $object_id;
protected $object_path;
protected $object_key;
protected $solution_id;
protected $url;
protected $event_type;
protected $is_processed;
protected $name;
protected $solutionname_web;
protected $object_type;


/**
* @param array $values
* @return \Pimcore\Model\DataObject\BzPublishQueue
*/
public static function create($values = array()) {
	$object = new static();
	$object->setValues($values);
	return $object;
}

/**
* Get PublishDateTime - Publish Date Time
* @return \Carbon\Carbon
*/
public function getPublishDateTime () {
	$preValue = $this->preGetValue("PublishDateTime"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->PublishDateTime;
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set PublishDateTime - Publish Date Time
* @param \Carbon\Carbon $PublishDateTime
* @return \Pimcore\Model\DataObject\BzPublishQueue
*/
public function setPublishDateTime ($PublishDateTime) {
	$fd = $this->getClass()->getFieldDefinition("PublishDateTime");
	$this->PublishDateTime = $PublishDateTime;
	return $this;
}

/**
* Get class_name - class Name
* @return string
*/
public function getClass_name () {
	$preValue = $this->preGetValue("class_name"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->class_name;
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set class_name - class Name
* @param string $class_name
* @return \Pimcore\Model\DataObject\BzPublishQueue
*/
public function setClass_name ($class_name) {
	$fd = $this->getClass()->getFieldDefinition("class_name");
	$this->class_name = $class_name;
	return $this;
}

/**
* Get object_id - object id
* @return string
*/
public function getObject_id () {
	$preValue = $this->preGetValue("object_id"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->object_id;
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set object_id - object id
* @param string $object_id
* @return \Pimcore\Model\DataObject\BzPublishQueue
*/
public function setObject_id ($object_id) {
	$fd = $this->getClass()->getFieldDefinition("object_id");
	$this->object_id = $object_id;
	return $this;
}

/**
* Get object_path - object path
* @return string
*/
public function getObject_path () {
	$preValue = $this->preGetValue("object_path"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->object_path;
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set object_path - object path
* @param string $object_path
* @return \Pimcore\Model\DataObject\BzPublishQueue
*/
public function setObject_path ($object_path) {
	$fd = $this->getClass()->getFieldDefinition("object_path");
	$this->object_path = $object_path;
	return $this;
}

/**
* Get object_key - object key
* @return string
*/
public function getObject_key () {
	$preValue = $this->preGetValue("object_key"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->object_key;
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set object_key - object key
* @param string $object_key
* @return \Pimcore\Model\DataObject\BzPublishQueue
*/
public function setObject_key ($object_key) {
	$fd = $this->getClass()->getFieldDefinition("object_key");
	$this->object_key = $object_key;
	return $this;
}

/**
* Get solution_id - solution id
* @return string
*/
public function getSolution_id () {
	$preValue = $this->preGetValue("solution_id"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->solution_id;
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set solution_id - solution id
* @param string $solution_id
* @return \Pimcore\Model\DataObject\BzPublishQueue
*/
public function setSolution_id ($solution_id) {
	$fd = $this->getClass()->getFieldDefinition("solution_id");
	$this->solution_id = $solution_id;
	return $this;
}

/**
* Get url - url
* @return string
*/
public function getUrl () {
	$preValue = $this->preGetValue("url"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->url;
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set url - url
* @param string $url
* @return \Pimcore\Model\DataObject\BzPublishQueue
*/
public function setUrl ($url) {
	$fd = $this->getClass()->getFieldDefinition("url");
	$this->url = $url;
	return $this;
}

/**
* Get event_type - event type
* @return string
*/
public function getEvent_type () {
	$preValue = $this->preGetValue("event_type"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->event_type;
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set event_type - event type
* @param string $event_type
* @return \Pimcore\Model\DataObject\BzPublishQueue
*/
public function setEvent_type ($event_type) {
	$fd = $this->getClass()->getFieldDefinition("event_type");
	$this->event_type = $event_type;
	return $this;
}

/**
* Get is_processed - Is Processed
* @return boolean
*/
public function getIs_processed () {
	$preValue = $this->preGetValue("is_processed"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->is_processed;
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set is_processed - Is Processed
* @param boolean $is_processed
* @return \Pimcore\Model\DataObject\BzPublishQueue
*/
public function setIs_processed ($is_processed) {
	$fd = $this->getClass()->getFieldDefinition("is_processed");
	$this->is_processed = $is_processed;
	return $this;
}

/**
* Get name - name
* @return string
*/
public function getName () {
	$preValue = $this->preGetValue("name"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->name;
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set name - name
* @param string $name
* @return \Pimcore\Model\DataObject\BzPublishQueue
*/
public function setName ($name) {
	$fd = $this->getClass()->getFieldDefinition("name");
	$this->name = $name;
	return $this;
}

/**
* Get solutionname_web - solutionname_web
* @return string
*/
public function getSolutionname_web () {
	$preValue = $this->preGetValue("solutionname_web"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->solutionname_web;
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set solutionname_web - solutionname_web
* @param string $solutionname_web
* @return \Pimcore\Model\DataObject\BzPublishQueue
*/
public function setSolutionname_web ($solutionname_web) {
	$fd = $this->getClass()->getFieldDefinition("solutionname_web");
	$this->solutionname_web = $solutionname_web;
	return $this;
}

/**
* Get object_type - Object Type
* @return string
*/
public function getObject_type () {
	$preValue = $this->preGetValue("object_type"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->object_type;
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set object_type - Object Type
* @param string $object_type
* @return \Pimcore\Model\DataObject\BzPublishQueue
*/
public function setObject_type ($object_type) {
	$fd = $this->getClass()->getFieldDefinition("object_type");
	$this->object_type = $object_type;
	return $this;
}

protected static $_relationFields = array (
);

protected $lazyLoadedFields = array (
);

}

