<?php 

/** 
* Generated at: 2019-08-30T09:57:19+02:00
* Inheritance: yes
* Variants: no
* Changed by: root (2)
* IP: ::1


Fields Summary: 
- inherited_from [manyToOneRelation]
- name [input]
- object_type [input]
- slug [input]
- audioType [select]
- audioFile [manyToOneRelation]
- url [input]
- audio_blog [wysiwyg]
- audio_transcript [wysiwyg]
- market_area_href [manyToOneRelation]
- domain_href [manyToOneRelation]
- merchant_href [manyToOneRelation]
- display_group [input]
- solution_name [input]
- title [input]
- type_display_name [input]
- listing_inherits_from [manyToOneRelation]
- deal_inherits_from [manyToOneRelation]
- event_inherits_from [manyToOneRelation]
- audio_inherits_from [manyToOneRelation]
- video_inherits_from [manyToOneRelation]
- article_inherits_from [manyToOneRelation]
- news_inherits_from [manyToOneRelation]
*/ 

namespace Pimcore\Model\DataObject;



/**
* @method static \Pimcore\Model\DataObject\BzAudio\Listing getByInherited_from ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\BzAudio\Listing getByName ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\BzAudio\Listing getByObject_type ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\BzAudio\Listing getBySlug ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\BzAudio\Listing getByAudioType ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\BzAudio\Listing getByAudioFile ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\BzAudio\Listing getByUrl ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\BzAudio\Listing getByAudio_blog ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\BzAudio\Listing getByAudio_transcript ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\BzAudio\Listing getByMarket_area_href ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\BzAudio\Listing getByDomain_href ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\BzAudio\Listing getByMerchant_href ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\BzAudio\Listing getByDisplay_group ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\BzAudio\Listing getBySolution_name ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\BzAudio\Listing getByTitle ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\BzAudio\Listing getByType_display_name ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\BzAudio\Listing getByListing_inherits_from ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\BzAudio\Listing getByDeal_inherits_from ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\BzAudio\Listing getByEvent_inherits_from ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\BzAudio\Listing getByAudio_inherits_from ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\BzAudio\Listing getByVideo_inherits_from ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\BzAudio\Listing getByArticle_inherits_from ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\BzAudio\Listing getByNews_inherits_from ($value, $limit = 0) 
*/

class BzAudio extends \Website\Model\DataObject\BzConcreteAudio implements \Pimcore\Model\DataObject\DirtyIndicatorInterface {



use \Pimcore\Model\DataObject\Traits\DirtyIndicatorTrait;

protected $o_classId = "24";
protected $o_className = "BzAudio";
protected $inherited_from;
protected $name;
protected $object_type;
protected $slug;
protected $audioType;
protected $audioFile;
protected $url;
protected $audio_blog;
protected $audio_transcript;
protected $market_area_href;
protected $domain_href;
protected $merchant_href;
protected $display_group;
protected $solution_name;
protected $title;
protected $type_display_name;
protected $listing_inherits_from;
protected $deal_inherits_from;
protected $event_inherits_from;
protected $audio_inherits_from;
protected $video_inherits_from;
protected $article_inherits_from;
protected $news_inherits_from;


/**
* @param array $values
* @return \Pimcore\Model\DataObject\BzAudio
*/
public static function create($values = array()) {
	$object = new static();
	$object->setValues($values);
	return $object;
}

/**
* Get inherited_from - Inherited From Audio
* @return \Pimcore\Model\DataObject\BzAudio
*/
public function getInherited_from () {
	$preValue = $this->preGetValue("inherited_from"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->getClass()->getFieldDefinition("inherited_from")->preGetData($this);
	if(\Pimcore\Model\DataObject::doGetInheritedValues() && $this->getClass()->getFieldDefinition("inherited_from")->isEmpty($data)) {
		return $this->getValueFromParent("inherited_from");
	}
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set inherited_from - Inherited From Audio
* @param \Pimcore\Model\DataObject\BzAudio $inherited_from
* @return \Pimcore\Model\DataObject\BzAudio
*/
public function setInherited_from ($inherited_from) {
	$fd = $this->getClass()->getFieldDefinition("inherited_from");
	$currentData = $this->getInherited_from();
	$isEqual = $fd->isEqual($currentData, $inherited_from);
	if (!$isEqual) {
		$this->markFieldDirty("inherited_from", true);
	}
	$this->inherited_from = $fd->preSetData($this, $inherited_from);
	return $this;
}

/**
* Get name - Name
* @return string
*/
public function getName () {
	$preValue = $this->preGetValue("name"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->name;
	if(\Pimcore\Model\DataObject::doGetInheritedValues() && $this->getClass()->getFieldDefinition("name")->isEmpty($data)) {
		return $this->getValueFromParent("name");
	}
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set name - Name
* @param string $name
* @return \Pimcore\Model\DataObject\BzAudio
*/
public function setName ($name) {
	$fd = $this->getClass()->getFieldDefinition("name");
	$this->name = $name;
	return $this;
}

/**
* Get object_type - Object Type
* @return string
*/
public function getObject_type () {
	$preValue = $this->preGetValue("object_type"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->object_type;
	if(\Pimcore\Model\DataObject::doGetInheritedValues() && $this->getClass()->getFieldDefinition("object_type")->isEmpty($data)) {
		return $this->getValueFromParent("object_type");
	}
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set object_type - Object Type
* @param string $object_type
* @return \Pimcore\Model\DataObject\BzAudio
*/
public function setObject_type ($object_type) {
	$fd = $this->getClass()->getFieldDefinition("object_type");
	$this->object_type = $object_type;
	return $this;
}

/**
* Get slug - Slug
* @return string
*/
public function getSlug () {
	$preValue = $this->preGetValue("slug"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->slug;
	if(\Pimcore\Model\DataObject::doGetInheritedValues() && $this->getClass()->getFieldDefinition("slug")->isEmpty($data)) {
		return $this->getValueFromParent("slug");
	}
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set slug - Slug
* @param string $slug
* @return \Pimcore\Model\DataObject\BzAudio
*/
public function setSlug ($slug) {
	$fd = $this->getClass()->getFieldDefinition("slug");
	$this->slug = $slug;
	return $this;
}

/**
* Get audioType - type
* @return string
*/
public function getAudioType () {
	$preValue = $this->preGetValue("audioType"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->audioType;
	if(\Pimcore\Model\DataObject::doGetInheritedValues() && $this->getClass()->getFieldDefinition("audioType")->isEmpty($data)) {
		return $this->getValueFromParent("audioType");
	}
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set audioType - type
* @param string $audioType
* @return \Pimcore\Model\DataObject\BzAudio
*/
public function setAudioType ($audioType) {
	$fd = $this->getClass()->getFieldDefinition("audioType");
	$this->audioType = $audioType;
	return $this;
}

/**
* Get audioFile - Audio Href
* @return \Pimcore\Model\Asset\Audio
*/
public function getAudioFile () {
	$preValue = $this->preGetValue("audioFile"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->getClass()->getFieldDefinition("audioFile")->preGetData($this);
	if(\Pimcore\Model\DataObject::doGetInheritedValues() && $this->getClass()->getFieldDefinition("audioFile")->isEmpty($data)) {
		return $this->getValueFromParent("audioFile");
	}
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set audioFile - Audio Href
* @param \Pimcore\Model\Asset\Audio $audioFile
* @return \Pimcore\Model\DataObject\BzAudio
*/
public function setAudioFile ($audioFile) {
	$fd = $this->getClass()->getFieldDefinition("audioFile");
	$currentData = $this->getAudioFile();
	$isEqual = $fd->isEqual($currentData, $audioFile);
	if (!$isEqual) {
		$this->markFieldDirty("audioFile", true);
	}
	$this->audioFile = $fd->preSetData($this, $audioFile);
	return $this;
}

/**
* Get url - URL
* @return string
*/
public function getUrl () {
	$preValue = $this->preGetValue("url"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->url;
	if(\Pimcore\Model\DataObject::doGetInheritedValues() && $this->getClass()->getFieldDefinition("url")->isEmpty($data)) {
		return $this->getValueFromParent("url");
	}
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set url - URL
* @param string $url
* @return \Pimcore\Model\DataObject\BzAudio
*/
public function setUrl ($url) {
	$fd = $this->getClass()->getFieldDefinition("url");
	$this->url = $url;
	return $this;
}

/**
* Get audio_blog - audio_blog
* @return string
*/
public function getAudio_blog () {
	$preValue = $this->preGetValue("audio_blog"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->getClass()->getFieldDefinition("audio_blog")->preGetData($this);
	if(\Pimcore\Model\DataObject::doGetInheritedValues() && $this->getClass()->getFieldDefinition("audio_blog")->isEmpty($data)) {
		return $this->getValueFromParent("audio_blog");
	}
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set audio_blog - audio_blog
* @param string $audio_blog
* @return \Pimcore\Model\DataObject\BzAudio
*/
public function setAudio_blog ($audio_blog) {
	$fd = $this->getClass()->getFieldDefinition("audio_blog");
	$this->audio_blog = $audio_blog;
	return $this;
}

/**
* Get audio_transcript - Audio Transcript
* @return string
*/
public function getAudio_transcript () {
	$preValue = $this->preGetValue("audio_transcript"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->getClass()->getFieldDefinition("audio_transcript")->preGetData($this);
	if(\Pimcore\Model\DataObject::doGetInheritedValues() && $this->getClass()->getFieldDefinition("audio_transcript")->isEmpty($data)) {
		return $this->getValueFromParent("audio_transcript");
	}
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set audio_transcript - Audio Transcript
* @param string $audio_transcript
* @return \Pimcore\Model\DataObject\BzAudio
*/
public function setAudio_transcript ($audio_transcript) {
	$fd = $this->getClass()->getFieldDefinition("audio_transcript");
	$this->audio_transcript = $audio_transcript;
	return $this;
}

/**
* Get market_area_href - market_area_href
* @return \Pimcore\Model\DataObject\BzMarketArea
*/
public function getMarket_area_href () {
	$preValue = $this->preGetValue("market_area_href"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->getClass()->getFieldDefinition("market_area_href")->preGetData($this);
	if(\Pimcore\Model\DataObject::doGetInheritedValues() && $this->getClass()->getFieldDefinition("market_area_href")->isEmpty($data)) {
		return $this->getValueFromParent("market_area_href");
	}
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set market_area_href - market_area_href
* @param \Pimcore\Model\DataObject\BzMarketArea $market_area_href
* @return \Pimcore\Model\DataObject\BzAudio
*/
public function setMarket_area_href ($market_area_href) {
	$fd = $this->getClass()->getFieldDefinition("market_area_href");
	$currentData = $this->getMarket_area_href();
	$isEqual = $fd->isEqual($currentData, $market_area_href);
	if (!$isEqual) {
		$this->markFieldDirty("market_area_href", true);
	}
	$this->market_area_href = $fd->preSetData($this, $market_area_href);
	return $this;
}

/**
* Get domain_href - Domain Href
* @return \Pimcore\Model\DataObject\BzDomain
*/
public function getDomain_href () {
	$preValue = $this->preGetValue("domain_href"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->getClass()->getFieldDefinition("domain_href")->preGetData($this);
	if(\Pimcore\Model\DataObject::doGetInheritedValues() && $this->getClass()->getFieldDefinition("domain_href")->isEmpty($data)) {
		return $this->getValueFromParent("domain_href");
	}
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set domain_href - Domain Href
* @param \Pimcore\Model\DataObject\BzDomain $domain_href
* @return \Pimcore\Model\DataObject\BzAudio
*/
public function setDomain_href ($domain_href) {
	$fd = $this->getClass()->getFieldDefinition("domain_href");
	$currentData = $this->getDomain_href();
	$isEqual = $fd->isEqual($currentData, $domain_href);
	if (!$isEqual) {
		$this->markFieldDirty("domain_href", true);
	}
	$this->domain_href = $fd->preSetData($this, $domain_href);
	return $this;
}

/**
* Get merchant_href - Merchant Href
* @return \Pimcore\Model\DataObject\BzMerchant
*/
public function getMerchant_href () {
	$preValue = $this->preGetValue("merchant_href"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->getClass()->getFieldDefinition("merchant_href")->preGetData($this);
	if(\Pimcore\Model\DataObject::doGetInheritedValues() && $this->getClass()->getFieldDefinition("merchant_href")->isEmpty($data)) {
		return $this->getValueFromParent("merchant_href");
	}
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set merchant_href - Merchant Href
* @param \Pimcore\Model\DataObject\BzMerchant $merchant_href
* @return \Pimcore\Model\DataObject\BzAudio
*/
public function setMerchant_href ($merchant_href) {
	$fd = $this->getClass()->getFieldDefinition("merchant_href");
	$currentData = $this->getMerchant_href();
	$isEqual = $fd->isEqual($currentData, $merchant_href);
	if (!$isEqual) {
		$this->markFieldDirty("merchant_href", true);
	}
	$this->merchant_href = $fd->preSetData($this, $merchant_href);
	return $this;
}

/**
* Get display_group - display_group
* @return string
*/
public function getDisplay_group () {
	$preValue = $this->preGetValue("display_group"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->display_group;
	if(\Pimcore\Model\DataObject::doGetInheritedValues() && $this->getClass()->getFieldDefinition("display_group")->isEmpty($data)) {
		return $this->getValueFromParent("display_group");
	}
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set display_group - display_group
* @param string $display_group
* @return \Pimcore\Model\DataObject\BzAudio
*/
public function setDisplay_group ($display_group) {
	$fd = $this->getClass()->getFieldDefinition("display_group");
	$this->display_group = $display_group;
	return $this;
}

/**
* Get solution_name - Solution Name
* @return string
*/
public function getSolution_name () {
	$preValue = $this->preGetValue("solution_name"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->solution_name;
	if(\Pimcore\Model\DataObject::doGetInheritedValues() && $this->getClass()->getFieldDefinition("solution_name")->isEmpty($data)) {
		return $this->getValueFromParent("solution_name");
	}
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set solution_name - Solution Name
* @param string $solution_name
* @return \Pimcore\Model\DataObject\BzAudio
*/
public function setSolution_name ($solution_name) {
	$fd = $this->getClass()->getFieldDefinition("solution_name");
	$this->solution_name = $solution_name;
	return $this;
}

/**
* Get title - title
* @return string
*/
public function getTitle () {
	$preValue = $this->preGetValue("title"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->title;
	if(\Pimcore\Model\DataObject::doGetInheritedValues() && $this->getClass()->getFieldDefinition("title")->isEmpty($data)) {
		return $this->getValueFromParent("title");
	}
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set title - title
* @param string $title
* @return \Pimcore\Model\DataObject\BzAudio
*/
public function setTitle ($title) {
	$fd = $this->getClass()->getFieldDefinition("title");
	$this->title = $title;
	return $this;
}

/**
* Get type_display_name - audio
* @return string
*/
public function getType_display_name () {
	$preValue = $this->preGetValue("type_display_name"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->type_display_name;
	if(\Pimcore\Model\DataObject::doGetInheritedValues() && $this->getClass()->getFieldDefinition("type_display_name")->isEmpty($data)) {
		return $this->getValueFromParent("type_display_name");
	}
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set type_display_name - audio
* @param string $type_display_name
* @return \Pimcore\Model\DataObject\BzAudio
*/
public function setType_display_name ($type_display_name) {
	$fd = $this->getClass()->getFieldDefinition("type_display_name");
	$this->type_display_name = $type_display_name;
	return $this;
}

/**
* Get listing_inherits_from - Listing Inherits From
* @return \Pimcore\Model\DataObject\BzListing2
*/
public function getListing_inherits_from () {
	$preValue = $this->preGetValue("listing_inherits_from"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->getClass()->getFieldDefinition("listing_inherits_from")->preGetData($this);
	if(\Pimcore\Model\DataObject::doGetInheritedValues() && $this->getClass()->getFieldDefinition("listing_inherits_from")->isEmpty($data)) {
		return $this->getValueFromParent("listing_inherits_from");
	}
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set listing_inherits_from - Listing Inherits From
* @param \Pimcore\Model\DataObject\BzListing2 $listing_inherits_from
* @return \Pimcore\Model\DataObject\BzAudio
*/
public function setListing_inherits_from ($listing_inherits_from) {
	$fd = $this->getClass()->getFieldDefinition("listing_inherits_from");
	$currentData = $this->getListing_inherits_from();
	$isEqual = $fd->isEqual($currentData, $listing_inherits_from);
	if (!$isEqual) {
		$this->markFieldDirty("listing_inherits_from", true);
	}
	$this->listing_inherits_from = $fd->preSetData($this, $listing_inherits_from);
	return $this;
}

/**
* Get deal_inherits_from - Deal Inherits From
* @return \Pimcore\Model\DataObject\BzDeals
*/
public function getDeal_inherits_from () {
	$preValue = $this->preGetValue("deal_inherits_from"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->getClass()->getFieldDefinition("deal_inherits_from")->preGetData($this);
	if(\Pimcore\Model\DataObject::doGetInheritedValues() && $this->getClass()->getFieldDefinition("deal_inherits_from")->isEmpty($data)) {
		return $this->getValueFromParent("deal_inherits_from");
	}
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set deal_inherits_from - Deal Inherits From
* @param \Pimcore\Model\DataObject\BzDeals $deal_inherits_from
* @return \Pimcore\Model\DataObject\BzAudio
*/
public function setDeal_inherits_from ($deal_inherits_from) {
	$fd = $this->getClass()->getFieldDefinition("deal_inherits_from");
	$currentData = $this->getDeal_inherits_from();
	$isEqual = $fd->isEqual($currentData, $deal_inherits_from);
	if (!$isEqual) {
		$this->markFieldDirty("deal_inherits_from", true);
	}
	$this->deal_inherits_from = $fd->preSetData($this, $deal_inherits_from);
	return $this;
}

/**
* Get event_inherits_from - event_inherits_from
* @return \Pimcore\Model\DataObject\BzEvents
*/
public function getEvent_inherits_from () {
	$preValue = $this->preGetValue("event_inherits_from"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->getClass()->getFieldDefinition("event_inherits_from")->preGetData($this);
	if(\Pimcore\Model\DataObject::doGetInheritedValues() && $this->getClass()->getFieldDefinition("event_inherits_from")->isEmpty($data)) {
		return $this->getValueFromParent("event_inherits_from");
	}
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set event_inherits_from - event_inherits_from
* @param \Pimcore\Model\DataObject\BzEvents $event_inherits_from
* @return \Pimcore\Model\DataObject\BzAudio
*/
public function setEvent_inherits_from ($event_inherits_from) {
	$fd = $this->getClass()->getFieldDefinition("event_inherits_from");
	$currentData = $this->getEvent_inherits_from();
	$isEqual = $fd->isEqual($currentData, $event_inherits_from);
	if (!$isEqual) {
		$this->markFieldDirty("event_inherits_from", true);
	}
	$this->event_inherits_from = $fd->preSetData($this, $event_inherits_from);
	return $this;
}

/**
* Get audio_inherits_from - audio_inherits_from
* @return \Pimcore\Model\DataObject\BzAudio
*/
public function getAudio_inherits_from () {
	$preValue = $this->preGetValue("audio_inherits_from"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->getClass()->getFieldDefinition("audio_inherits_from")->preGetData($this);
	if(\Pimcore\Model\DataObject::doGetInheritedValues() && $this->getClass()->getFieldDefinition("audio_inherits_from")->isEmpty($data)) {
		return $this->getValueFromParent("audio_inherits_from");
	}
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set audio_inherits_from - audio_inherits_from
* @param \Pimcore\Model\DataObject\BzAudio $audio_inherits_from
* @return \Pimcore\Model\DataObject\BzAudio
*/
public function setAudio_inherits_from ($audio_inherits_from) {
	$fd = $this->getClass()->getFieldDefinition("audio_inherits_from");
	$currentData = $this->getAudio_inherits_from();
	$isEqual = $fd->isEqual($currentData, $audio_inherits_from);
	if (!$isEqual) {
		$this->markFieldDirty("audio_inherits_from", true);
	}
	$this->audio_inherits_from = $fd->preSetData($this, $audio_inherits_from);
	return $this;
}

/**
* Get video_inherits_from - video_inherits_from
* @return \Pimcore\Model\DataObject\BzVideo
*/
public function getVideo_inherits_from () {
	$preValue = $this->preGetValue("video_inherits_from"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->getClass()->getFieldDefinition("video_inherits_from")->preGetData($this);
	if(\Pimcore\Model\DataObject::doGetInheritedValues() && $this->getClass()->getFieldDefinition("video_inherits_from")->isEmpty($data)) {
		return $this->getValueFromParent("video_inherits_from");
	}
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set video_inherits_from - video_inherits_from
* @param \Pimcore\Model\DataObject\BzVideo $video_inherits_from
* @return \Pimcore\Model\DataObject\BzAudio
*/
public function setVideo_inherits_from ($video_inherits_from) {
	$fd = $this->getClass()->getFieldDefinition("video_inherits_from");
	$currentData = $this->getVideo_inherits_from();
	$isEqual = $fd->isEqual($currentData, $video_inherits_from);
	if (!$isEqual) {
		$this->markFieldDirty("video_inherits_from", true);
	}
	$this->video_inherits_from = $fd->preSetData($this, $video_inherits_from);
	return $this;
}

/**
* Get article_inherits_from - Article Inherits From
* @return \Pimcore\Model\DataObject\BzBlogArticle
*/
public function getArticle_inherits_from () {
	$preValue = $this->preGetValue("article_inherits_from"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->getClass()->getFieldDefinition("article_inherits_from")->preGetData($this);
	if(\Pimcore\Model\DataObject::doGetInheritedValues() && $this->getClass()->getFieldDefinition("article_inherits_from")->isEmpty($data)) {
		return $this->getValueFromParent("article_inherits_from");
	}
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set article_inherits_from - Article Inherits From
* @param \Pimcore\Model\DataObject\BzBlogArticle $article_inherits_from
* @return \Pimcore\Model\DataObject\BzAudio
*/
public function setArticle_inherits_from ($article_inherits_from) {
	$fd = $this->getClass()->getFieldDefinition("article_inherits_from");
	$currentData = $this->getArticle_inherits_from();
	$isEqual = $fd->isEqual($currentData, $article_inherits_from);
	if (!$isEqual) {
		$this->markFieldDirty("article_inherits_from", true);
	}
	$this->article_inherits_from = $fd->preSetData($this, $article_inherits_from);
	return $this;
}

/**
* Get news_inherits_from - News Inherits From
* @return \Pimcore\Model\DataObject\News
*/
public function getNews_inherits_from () {
	$preValue = $this->preGetValue("news_inherits_from"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->getClass()->getFieldDefinition("news_inherits_from")->preGetData($this);
	if(\Pimcore\Model\DataObject::doGetInheritedValues() && $this->getClass()->getFieldDefinition("news_inherits_from")->isEmpty($data)) {
		return $this->getValueFromParent("news_inherits_from");
	}
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set news_inherits_from - News Inherits From
* @param \Pimcore\Model\DataObject\News $news_inherits_from
* @return \Pimcore\Model\DataObject\BzAudio
*/
public function setNews_inherits_from ($news_inherits_from) {
	$fd = $this->getClass()->getFieldDefinition("news_inherits_from");
	$currentData = $this->getNews_inherits_from();
	$isEqual = $fd->isEqual($currentData, $news_inherits_from);
	if (!$isEqual) {
		$this->markFieldDirty("news_inherits_from", true);
	}
	$this->news_inherits_from = $fd->preSetData($this, $news_inherits_from);
	return $this;
}

protected static $_relationFields = array (
  'inherited_from' => 
  array (
    'type' => 'manyToOneRelation',
  ),
  'audioFile' => 
  array (
    'type' => 'manyToOneRelation',
  ),
  'market_area_href' => 
  array (
    'type' => 'manyToOneRelation',
  ),
  'domain_href' => 
  array (
    'type' => 'manyToOneRelation',
  ),
  'merchant_href' => 
  array (
    'type' => 'manyToOneRelation',
  ),
  'listing_inherits_from' => 
  array (
    'type' => 'manyToOneRelation',
  ),
  'deal_inherits_from' => 
  array (
    'type' => 'manyToOneRelation',
  ),
  'event_inherits_from' => 
  array (
    'type' => 'manyToOneRelation',
  ),
  'audio_inherits_from' => 
  array (
    'type' => 'manyToOneRelation',
  ),
  'video_inherits_from' => 
  array (
    'type' => 'manyToOneRelation',
  ),
  'article_inherits_from' => 
  array (
    'type' => 'manyToOneRelation',
  ),
  'news_inherits_from' => 
  array (
    'type' => 'manyToOneRelation',
  ),
);

protected $lazyLoadedFields = array (
);

}

