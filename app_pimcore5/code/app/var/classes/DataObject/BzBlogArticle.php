<?php 

/** 
* Generated at: 2019-08-30T10:53:22+02:00
* Inheritance: yes
* Variants: no
* Changed by: root (2)
* IP: ::1


Fields Summary: 
- inherited_from [manyToOneRelation]
- market_area_href [manyToOneRelation]
- domain_href [manyToOneRelation]
- merchant_href [manyToOneRelation]
- author [input]
- date [datetime]
- categories [manyToManyObjectRelation]
- tags [manyToManyRelation]
- poster_Image [hotspotimage]
- images [manyToManyRelation]
- title [input]
- object_type [input]
- text [wysiwyg]
- headline [input]
- slug [input]
- IsPromotedToHomePage [checkbox]
- briefSummary [textarea]
- solution_name [input]
- name [input]
- tag_cloud [input]
- type_display_name [input]
- listing_inherits_from [manyToOneRelation]
- deal_inherits_from [manyToOneRelation]
- event_inherits_from [manyToOneRelation]
- audio_inherits_from [manyToOneRelation]
- video_inherits_from [manyToOneRelation]
- news_inherits_from [manyToOneRelation]
- article_inherits_from [manyToOneRelation]
*/ 

namespace Pimcore\Model\DataObject;



/**
* @method static \Pimcore\Model\DataObject\BzBlogArticle\Listing getByInherited_from ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\BzBlogArticle\Listing getByMarket_area_href ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\BzBlogArticle\Listing getByDomain_href ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\BzBlogArticle\Listing getByMerchant_href ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\BzBlogArticle\Listing getByAuthor ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\BzBlogArticle\Listing getByDate ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\BzBlogArticle\Listing getByCategories ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\BzBlogArticle\Listing getByTags ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\BzBlogArticle\Listing getByPoster_Image ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\BzBlogArticle\Listing getByImages ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\BzBlogArticle\Listing getByTitle ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\BzBlogArticle\Listing getByObject_type ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\BzBlogArticle\Listing getByText ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\BzBlogArticle\Listing getByHeadline ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\BzBlogArticle\Listing getBySlug ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\BzBlogArticle\Listing getByIsPromotedToHomePage ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\BzBlogArticle\Listing getByBriefSummary ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\BzBlogArticle\Listing getBySolution_name ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\BzBlogArticle\Listing getByName ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\BzBlogArticle\Listing getByTag_cloud ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\BzBlogArticle\Listing getByType_display_name ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\BzBlogArticle\Listing getByListing_inherits_from ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\BzBlogArticle\Listing getByDeal_inherits_from ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\BzBlogArticle\Listing getByEvent_inherits_from ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\BzBlogArticle\Listing getByAudio_inherits_from ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\BzBlogArticle\Listing getByVideo_inherits_from ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\BzBlogArticle\Listing getByNews_inherits_from ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\BzBlogArticle\Listing getByArticle_inherits_from ($value, $limit = 0) 
*/

class BzBlogArticle extends \Website\Model\DataObject\BzConcreteArticle implements \Pimcore\Model\DataObject\DirtyIndicatorInterface {



use \Pimcore\Model\DataObject\Traits\DirtyIndicatorTrait;

protected $o_classId = "35";
protected $o_className = "BzBlogArticle";
protected $inherited_from;
protected $market_area_href;
protected $domain_href;
protected $merchant_href;
protected $author;
protected $date;
protected $categories;
protected $tags;
protected $poster_Image;
protected $images;
protected $title;
protected $object_type;
protected $text;
protected $headline;
protected $slug;
protected $IsPromotedToHomePage;
protected $briefSummary;
protected $solution_name;
protected $name;
protected $tag_cloud;
protected $type_display_name;
protected $listing_inherits_from;
protected $deal_inherits_from;
protected $event_inherits_from;
protected $audio_inherits_from;
protected $video_inherits_from;
protected $news_inherits_from;
protected $article_inherits_from;


/**
* @param array $values
* @return \Pimcore\Model\DataObject\BzBlogArticle
*/
public static function create($values = array()) {
	$object = new static();
	$object->setValues($values);
	return $object;
}

/**
* Get inherited_from - inherited_from
* @return \Pimcore\Model\DataObject\BzBlogArticle
*/
public function getInherited_from () {
	$preValue = $this->preGetValue("inherited_from"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->getClass()->getFieldDefinition("inherited_from")->preGetData($this);
	if(\Pimcore\Model\DataObject::doGetInheritedValues() && $this->getClass()->getFieldDefinition("inherited_from")->isEmpty($data)) {
		return $this->getValueFromParent("inherited_from");
	}
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set inherited_from - inherited_from
* @param \Pimcore\Model\DataObject\BzBlogArticle $inherited_from
* @return \Pimcore\Model\DataObject\BzBlogArticle
*/
public function setInherited_from ($inherited_from) {
	$fd = $this->getClass()->getFieldDefinition("inherited_from");
	$currentData = $this->getInherited_from();
	$isEqual = $fd->isEqual($currentData, $inherited_from);
	if (!$isEqual) {
		$this->markFieldDirty("inherited_from", true);
	}
	$this->inherited_from = $fd->preSetData($this, $inherited_from);
	return $this;
}

/**
* Get market_area_href - market_area_href
* @return \Pimcore\Model\DataObject\BzMarketArea
*/
public function getMarket_area_href () {
	$preValue = $this->preGetValue("market_area_href"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->getClass()->getFieldDefinition("market_area_href")->preGetData($this);
	if(\Pimcore\Model\DataObject::doGetInheritedValues() && $this->getClass()->getFieldDefinition("market_area_href")->isEmpty($data)) {
		return $this->getValueFromParent("market_area_href");
	}
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set market_area_href - market_area_href
* @param \Pimcore\Model\DataObject\BzMarketArea $market_area_href
* @return \Pimcore\Model\DataObject\BzBlogArticle
*/
public function setMarket_area_href ($market_area_href) {
	$fd = $this->getClass()->getFieldDefinition("market_area_href");
	$currentData = $this->getMarket_area_href();
	$isEqual = $fd->isEqual($currentData, $market_area_href);
	if (!$isEqual) {
		$this->markFieldDirty("market_area_href", true);
	}
	$this->market_area_href = $fd->preSetData($this, $market_area_href);
	return $this;
}

/**
* Get domain_href - domain_href
* @return \Pimcore\Model\DataObject\BzDomain
*/
public function getDomain_href () {
	$preValue = $this->preGetValue("domain_href"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->getClass()->getFieldDefinition("domain_href")->preGetData($this);
	if(\Pimcore\Model\DataObject::doGetInheritedValues() && $this->getClass()->getFieldDefinition("domain_href")->isEmpty($data)) {
		return $this->getValueFromParent("domain_href");
	}
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set domain_href - domain_href
* @param \Pimcore\Model\DataObject\BzDomain $domain_href
* @return \Pimcore\Model\DataObject\BzBlogArticle
*/
public function setDomain_href ($domain_href) {
	$fd = $this->getClass()->getFieldDefinition("domain_href");
	$currentData = $this->getDomain_href();
	$isEqual = $fd->isEqual($currentData, $domain_href);
	if (!$isEqual) {
		$this->markFieldDirty("domain_href", true);
	}
	$this->domain_href = $fd->preSetData($this, $domain_href);
	return $this;
}

/**
* Get merchant_href - merchant_href
* @return \Pimcore\Model\DataObject\BzMerchant
*/
public function getMerchant_href () {
	$preValue = $this->preGetValue("merchant_href"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->getClass()->getFieldDefinition("merchant_href")->preGetData($this);
	if(\Pimcore\Model\DataObject::doGetInheritedValues() && $this->getClass()->getFieldDefinition("merchant_href")->isEmpty($data)) {
		return $this->getValueFromParent("merchant_href");
	}
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set merchant_href - merchant_href
* @param \Pimcore\Model\DataObject\BzMerchant $merchant_href
* @return \Pimcore\Model\DataObject\BzBlogArticle
*/
public function setMerchant_href ($merchant_href) {
	$fd = $this->getClass()->getFieldDefinition("merchant_href");
	$currentData = $this->getMerchant_href();
	$isEqual = $fd->isEqual($currentData, $merchant_href);
	if (!$isEqual) {
		$this->markFieldDirty("merchant_href", true);
	}
	$this->merchant_href = $fd->preSetData($this, $merchant_href);
	return $this;
}

/**
* Get author - author
* @return string
*/
public function getAuthor () {
	$preValue = $this->preGetValue("author"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->author;
	if(\Pimcore\Model\DataObject::doGetInheritedValues() && $this->getClass()->getFieldDefinition("author")->isEmpty($data)) {
		return $this->getValueFromParent("author");
	}
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set author - author
* @param string $author
* @return \Pimcore\Model\DataObject\BzBlogArticle
*/
public function setAuthor ($author) {
	$fd = $this->getClass()->getFieldDefinition("author");
	$this->author = $author;
	return $this;
}

/**
* Get date - Date
* @return \Carbon\Carbon
*/
public function getDate () {
	$preValue = $this->preGetValue("date"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->date;
	if(\Pimcore\Model\DataObject::doGetInheritedValues() && $this->getClass()->getFieldDefinition("date")->isEmpty($data)) {
		return $this->getValueFromParent("date");
	}
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set date - Date
* @param \Carbon\Carbon $date
* @return \Pimcore\Model\DataObject\BzBlogArticle
*/
public function setDate ($date) {
	$fd = $this->getClass()->getFieldDefinition("date");
	$this->date = $date;
	return $this;
}

/**
* Get categories - Categories
* @return \Pimcore\Model\DataObject\BzBlogCategory[]
*/
public function getCategories () {
	$preValue = $this->preGetValue("categories"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->getClass()->getFieldDefinition("categories")->preGetData($this);
	if(\Pimcore\Model\DataObject::doGetInheritedValues() && $this->getClass()->getFieldDefinition("categories")->isEmpty($data)) {
		return $this->getValueFromParent("categories");
	}
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set categories - Categories
* @param \Pimcore\Model\DataObject\BzBlogCategory[] $categories
* @return \Pimcore\Model\DataObject\BzBlogArticle
*/
public function setCategories ($categories) {
	$fd = $this->getClass()->getFieldDefinition("categories");
	$currentData = $this->getCategories();
	$isEqual = $fd->isEqual($currentData, $categories);
	if (!$isEqual) {
		$this->markFieldDirty("categories", true);
	}
	$this->categories = $fd->preSetData($this, $categories);
	return $this;
}

/**
* Get tags - Tags
* @return \Pimcore\Model\DataObject\BzTags[]
*/
public function getTags () {
	$preValue = $this->preGetValue("tags"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->getClass()->getFieldDefinition("tags")->preGetData($this);
	if(\Pimcore\Model\DataObject::doGetInheritedValues() && $this->getClass()->getFieldDefinition("tags")->isEmpty($data)) {
		return $this->getValueFromParent("tags");
	}
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set tags - Tags
* @param \Pimcore\Model\DataObject\BzTags[] $tags
* @return \Pimcore\Model\DataObject\BzBlogArticle
*/
public function setTags ($tags) {
	$fd = $this->getClass()->getFieldDefinition("tags");
	$currentData = $this->getTags();
	$isEqual = $fd->isEqual($currentData, $tags);
	if (!$isEqual) {
		$this->markFieldDirty("tags", true);
	}
	$this->tags = $fd->preSetData($this, $tags);
	return $this;
}

/**
* Get poster_Image - Poster Image
* @return \Pimcore\Model\DataObject\Data\Hotspotimage
*/
public function getPoster_Image () {
	$preValue = $this->preGetValue("poster_Image"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->poster_Image;
	if(\Pimcore\Model\DataObject::doGetInheritedValues() && $this->getClass()->getFieldDefinition("poster_Image")->isEmpty($data)) {
		return $this->getValueFromParent("poster_Image");
	}
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set poster_Image - Poster Image
* @param \Pimcore\Model\DataObject\Data\Hotspotimage $poster_Image
* @return \Pimcore\Model\DataObject\BzBlogArticle
*/
public function setPoster_Image ($poster_Image) {
	$fd = $this->getClass()->getFieldDefinition("poster_Image");
	$this->poster_Image = $poster_Image;
	return $this;
}

/**
* Get images - images
* @return \Pimcore\Model\Asset\Image[]
*/
public function getImages () {
	$preValue = $this->preGetValue("images"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->getClass()->getFieldDefinition("images")->preGetData($this);
	if(\Pimcore\Model\DataObject::doGetInheritedValues() && $this->getClass()->getFieldDefinition("images")->isEmpty($data)) {
		return $this->getValueFromParent("images");
	}
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set images - images
* @param \Pimcore\Model\Asset\Image[] $images
* @return \Pimcore\Model\DataObject\BzBlogArticle
*/
public function setImages ($images) {
	$fd = $this->getClass()->getFieldDefinition("images");
	$currentData = $this->getImages();
	$isEqual = $fd->isEqual($currentData, $images);
	if (!$isEqual) {
		$this->markFieldDirty("images", true);
	}
	$this->images = $fd->preSetData($this, $images);
	return $this;
}

/**
* Get title - title
* @return string
*/
public function getTitle () {
	$preValue = $this->preGetValue("title"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->title;
	if(\Pimcore\Model\DataObject::doGetInheritedValues() && $this->getClass()->getFieldDefinition("title")->isEmpty($data)) {
		return $this->getValueFromParent("title");
	}
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set title - title
* @param string $title
* @return \Pimcore\Model\DataObject\BzBlogArticle
*/
public function setTitle ($title) {
	$fd = $this->getClass()->getFieldDefinition("title");
	$this->title = $title;
	return $this;
}

/**
* Get object_type - Object Type
* @return string
*/
public function getObject_type () {
	$preValue = $this->preGetValue("object_type"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->object_type;
	if(\Pimcore\Model\DataObject::doGetInheritedValues() && $this->getClass()->getFieldDefinition("object_type")->isEmpty($data)) {
		return $this->getValueFromParent("object_type");
	}
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set object_type - Object Type
* @param string $object_type
* @return \Pimcore\Model\DataObject\BzBlogArticle
*/
public function setObject_type ($object_type) {
	$fd = $this->getClass()->getFieldDefinition("object_type");
	$this->object_type = $object_type;
	return $this;
}

/**
* Get text - Text
* @return string
*/
public function getText () {
	$preValue = $this->preGetValue("text"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->getClass()->getFieldDefinition("text")->preGetData($this);
	if(\Pimcore\Model\DataObject::doGetInheritedValues() && $this->getClass()->getFieldDefinition("text")->isEmpty($data)) {
		return $this->getValueFromParent("text");
	}
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set text - Text
* @param string $text
* @return \Pimcore\Model\DataObject\BzBlogArticle
*/
public function setText ($text) {
	$fd = $this->getClass()->getFieldDefinition("text");
	$this->text = $text;
	return $this;
}

/**
* Get headline - Headline
* @return string
*/
public function getHeadline () {
	$preValue = $this->preGetValue("headline"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->headline;
	if(\Pimcore\Model\DataObject::doGetInheritedValues() && $this->getClass()->getFieldDefinition("headline")->isEmpty($data)) {
		return $this->getValueFromParent("headline");
	}
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set headline - Headline
* @param string $headline
* @return \Pimcore\Model\DataObject\BzBlogArticle
*/
public function setHeadline ($headline) {
	$fd = $this->getClass()->getFieldDefinition("headline");
	$this->headline = $headline;
	return $this;
}

/**
* Get slug - Slug
* @return string
*/
public function getSlug () {
	$preValue = $this->preGetValue("slug"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->slug;
	if(\Pimcore\Model\DataObject::doGetInheritedValues() && $this->getClass()->getFieldDefinition("slug")->isEmpty($data)) {
		return $this->getValueFromParent("slug");
	}
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set slug - Slug
* @param string $slug
* @return \Pimcore\Model\DataObject\BzBlogArticle
*/
public function setSlug ($slug) {
	$fd = $this->getClass()->getFieldDefinition("slug");
	$this->slug = $slug;
	return $this;
}

/**
* Get IsPromotedToHomePage - OnHomePage?
* @return boolean
*/
public function getIsPromotedToHomePage () {
	$preValue = $this->preGetValue("IsPromotedToHomePage"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->IsPromotedToHomePage;
	if(\Pimcore\Model\DataObject::doGetInheritedValues() && $this->getClass()->getFieldDefinition("IsPromotedToHomePage")->isEmpty($data)) {
		return $this->getValueFromParent("IsPromotedToHomePage");
	}
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set IsPromotedToHomePage - OnHomePage?
* @param boolean $IsPromotedToHomePage
* @return \Pimcore\Model\DataObject\BzBlogArticle
*/
public function setIsPromotedToHomePage ($IsPromotedToHomePage) {
	$fd = $this->getClass()->getFieldDefinition("IsPromotedToHomePage");
	$this->IsPromotedToHomePage = $IsPromotedToHomePage;
	return $this;
}

/**
* Get briefSummary - briefSummary
* @return string
*/
public function getBriefSummary () {
	$preValue = $this->preGetValue("briefSummary"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->briefSummary;
	if(\Pimcore\Model\DataObject::doGetInheritedValues() && $this->getClass()->getFieldDefinition("briefSummary")->isEmpty($data)) {
		return $this->getValueFromParent("briefSummary");
	}
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set briefSummary - briefSummary
* @param string $briefSummary
* @return \Pimcore\Model\DataObject\BzBlogArticle
*/
public function setBriefSummary ($briefSummary) {
	$fd = $this->getClass()->getFieldDefinition("briefSummary");
	$this->briefSummary = $briefSummary;
	return $this;
}

/**
* Get solution_name - Solution Name
* @return string
*/
public function getSolution_name () {
	$preValue = $this->preGetValue("solution_name"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->solution_name;
	if(\Pimcore\Model\DataObject::doGetInheritedValues() && $this->getClass()->getFieldDefinition("solution_name")->isEmpty($data)) {
		return $this->getValueFromParent("solution_name");
	}
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set solution_name - Solution Name
* @param string $solution_name
* @return \Pimcore\Model\DataObject\BzBlogArticle
*/
public function setSolution_name ($solution_name) {
	$fd = $this->getClass()->getFieldDefinition("solution_name");
	$this->solution_name = $solution_name;
	return $this;
}

/**
* Get name - Name
* @return string
*/
public function getName () {
	$preValue = $this->preGetValue("name"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->name;
	if(\Pimcore\Model\DataObject::doGetInheritedValues() && $this->getClass()->getFieldDefinition("name")->isEmpty($data)) {
		return $this->getValueFromParent("name");
	}
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set name - Name
* @param string $name
* @return \Pimcore\Model\DataObject\BzBlogArticle
*/
public function setName ($name) {
	$fd = $this->getClass()->getFieldDefinition("name");
	$this->name = $name;
	return $this;
}

/**
* Get tag_cloud - tag_cloud
* @return string
*/
public function getTag_cloud () {
	$preValue = $this->preGetValue("tag_cloud"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->tag_cloud;
	if(\Pimcore\Model\DataObject::doGetInheritedValues() && $this->getClass()->getFieldDefinition("tag_cloud")->isEmpty($data)) {
		return $this->getValueFromParent("tag_cloud");
	}
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set tag_cloud - tag_cloud
* @param string $tag_cloud
* @return \Pimcore\Model\DataObject\BzBlogArticle
*/
public function setTag_cloud ($tag_cloud) {
	$fd = $this->getClass()->getFieldDefinition("tag_cloud");
	$this->tag_cloud = $tag_cloud;
	return $this;
}

/**
* Get type_display_name - articles
* @return string
*/
public function getType_display_name () {
	$preValue = $this->preGetValue("type_display_name"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->type_display_name;
	if(\Pimcore\Model\DataObject::doGetInheritedValues() && $this->getClass()->getFieldDefinition("type_display_name")->isEmpty($data)) {
		return $this->getValueFromParent("type_display_name");
	}
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set type_display_name - articles
* @param string $type_display_name
* @return \Pimcore\Model\DataObject\BzBlogArticle
*/
public function setType_display_name ($type_display_name) {
	$fd = $this->getClass()->getFieldDefinition("type_display_name");
	$this->type_display_name = $type_display_name;
	return $this;
}

/**
* Get listing_inherits_from - Listing Inherits From
* @return \Pimcore\Model\DataObject\BzListing2
*/
public function getListing_inherits_from () {
	$preValue = $this->preGetValue("listing_inherits_from"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->getClass()->getFieldDefinition("listing_inherits_from")->preGetData($this);
	if(\Pimcore\Model\DataObject::doGetInheritedValues() && $this->getClass()->getFieldDefinition("listing_inherits_from")->isEmpty($data)) {
		return $this->getValueFromParent("listing_inherits_from");
	}
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set listing_inherits_from - Listing Inherits From
* @param \Pimcore\Model\DataObject\BzListing2 $listing_inherits_from
* @return \Pimcore\Model\DataObject\BzBlogArticle
*/
public function setListing_inherits_from ($listing_inherits_from) {
	$fd = $this->getClass()->getFieldDefinition("listing_inherits_from");
	$currentData = $this->getListing_inherits_from();
	$isEqual = $fd->isEqual($currentData, $listing_inherits_from);
	if (!$isEqual) {
		$this->markFieldDirty("listing_inherits_from", true);
	}
	$this->listing_inherits_from = $fd->preSetData($this, $listing_inherits_from);
	return $this;
}

/**
* Get deal_inherits_from - Deal Inherits From
* @return \Pimcore\Model\DataObject\BzDeals
*/
public function getDeal_inherits_from () {
	$preValue = $this->preGetValue("deal_inherits_from"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->getClass()->getFieldDefinition("deal_inherits_from")->preGetData($this);
	if(\Pimcore\Model\DataObject::doGetInheritedValues() && $this->getClass()->getFieldDefinition("deal_inherits_from")->isEmpty($data)) {
		return $this->getValueFromParent("deal_inherits_from");
	}
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set deal_inherits_from - Deal Inherits From
* @param \Pimcore\Model\DataObject\BzDeals $deal_inherits_from
* @return \Pimcore\Model\DataObject\BzBlogArticle
*/
public function setDeal_inherits_from ($deal_inherits_from) {
	$fd = $this->getClass()->getFieldDefinition("deal_inherits_from");
	$currentData = $this->getDeal_inherits_from();
	$isEqual = $fd->isEqual($currentData, $deal_inherits_from);
	if (!$isEqual) {
		$this->markFieldDirty("deal_inherits_from", true);
	}
	$this->deal_inherits_from = $fd->preSetData($this, $deal_inherits_from);
	return $this;
}

/**
* Get event_inherits_from - Event Inherits From
* @return \Pimcore\Model\DataObject\BzEvents
*/
public function getEvent_inherits_from () {
	$preValue = $this->preGetValue("event_inherits_from"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->getClass()->getFieldDefinition("event_inherits_from")->preGetData($this);
	if(\Pimcore\Model\DataObject::doGetInheritedValues() && $this->getClass()->getFieldDefinition("event_inherits_from")->isEmpty($data)) {
		return $this->getValueFromParent("event_inherits_from");
	}
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set event_inherits_from - Event Inherits From
* @param \Pimcore\Model\DataObject\BzEvents $event_inherits_from
* @return \Pimcore\Model\DataObject\BzBlogArticle
*/
public function setEvent_inherits_from ($event_inherits_from) {
	$fd = $this->getClass()->getFieldDefinition("event_inherits_from");
	$currentData = $this->getEvent_inherits_from();
	$isEqual = $fd->isEqual($currentData, $event_inherits_from);
	if (!$isEqual) {
		$this->markFieldDirty("event_inherits_from", true);
	}
	$this->event_inherits_from = $fd->preSetData($this, $event_inherits_from);
	return $this;
}

/**
* Get audio_inherits_from - Audio Inherits From
* @return \Pimcore\Model\DataObject\BzAudio
*/
public function getAudio_inherits_from () {
	$preValue = $this->preGetValue("audio_inherits_from"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->getClass()->getFieldDefinition("audio_inherits_from")->preGetData($this);
	if(\Pimcore\Model\DataObject::doGetInheritedValues() && $this->getClass()->getFieldDefinition("audio_inherits_from")->isEmpty($data)) {
		return $this->getValueFromParent("audio_inherits_from");
	}
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set audio_inherits_from - Audio Inherits From
* @param \Pimcore\Model\DataObject\BzAudio $audio_inherits_from
* @return \Pimcore\Model\DataObject\BzBlogArticle
*/
public function setAudio_inherits_from ($audio_inherits_from) {
	$fd = $this->getClass()->getFieldDefinition("audio_inherits_from");
	$currentData = $this->getAudio_inherits_from();
	$isEqual = $fd->isEqual($currentData, $audio_inherits_from);
	if (!$isEqual) {
		$this->markFieldDirty("audio_inherits_from", true);
	}
	$this->audio_inherits_from = $fd->preSetData($this, $audio_inherits_from);
	return $this;
}

/**
* Get video_inherits_from - Video Inherits From
* @return \Pimcore\Model\DataObject\BzVideo
*/
public function getVideo_inherits_from () {
	$preValue = $this->preGetValue("video_inherits_from"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->getClass()->getFieldDefinition("video_inherits_from")->preGetData($this);
	if(\Pimcore\Model\DataObject::doGetInheritedValues() && $this->getClass()->getFieldDefinition("video_inherits_from")->isEmpty($data)) {
		return $this->getValueFromParent("video_inherits_from");
	}
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set video_inherits_from - Video Inherits From
* @param \Pimcore\Model\DataObject\BzVideo $video_inherits_from
* @return \Pimcore\Model\DataObject\BzBlogArticle
*/
public function setVideo_inherits_from ($video_inherits_from) {
	$fd = $this->getClass()->getFieldDefinition("video_inherits_from");
	$currentData = $this->getVideo_inherits_from();
	$isEqual = $fd->isEqual($currentData, $video_inherits_from);
	if (!$isEqual) {
		$this->markFieldDirty("video_inherits_from", true);
	}
	$this->video_inherits_from = $fd->preSetData($this, $video_inherits_from);
	return $this;
}

/**
* Get news_inherits_from - News Inherits From
* @return \Pimcore\Model\DataObject\News
*/
public function getNews_inherits_from () {
	$preValue = $this->preGetValue("news_inherits_from"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->getClass()->getFieldDefinition("news_inherits_from")->preGetData($this);
	if(\Pimcore\Model\DataObject::doGetInheritedValues() && $this->getClass()->getFieldDefinition("news_inherits_from")->isEmpty($data)) {
		return $this->getValueFromParent("news_inherits_from");
	}
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set news_inherits_from - News Inherits From
* @param \Pimcore\Model\DataObject\News $news_inherits_from
* @return \Pimcore\Model\DataObject\BzBlogArticle
*/
public function setNews_inherits_from ($news_inherits_from) {
	$fd = $this->getClass()->getFieldDefinition("news_inherits_from");
	$currentData = $this->getNews_inherits_from();
	$isEqual = $fd->isEqual($currentData, $news_inherits_from);
	if (!$isEqual) {
		$this->markFieldDirty("news_inherits_from", true);
	}
	$this->news_inherits_from = $fd->preSetData($this, $news_inherits_from);
	return $this;
}

/**
* Get article_inherits_from - article_inherits_from
* @return \Pimcore\Model\DataObject\BzBlogArticle
*/
public function getArticle_inherits_from () {
	$preValue = $this->preGetValue("article_inherits_from"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->getClass()->getFieldDefinition("article_inherits_from")->preGetData($this);
	if(\Pimcore\Model\DataObject::doGetInheritedValues() && $this->getClass()->getFieldDefinition("article_inherits_from")->isEmpty($data)) {
		return $this->getValueFromParent("article_inherits_from");
	}
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set article_inherits_from - article_inherits_from
* @param \Pimcore\Model\DataObject\BzBlogArticle $article_inherits_from
* @return \Pimcore\Model\DataObject\BzBlogArticle
*/
public function setArticle_inherits_from ($article_inherits_from) {
	$fd = $this->getClass()->getFieldDefinition("article_inherits_from");
	$currentData = $this->getArticle_inherits_from();
	$isEqual = $fd->isEqual($currentData, $article_inherits_from);
	if (!$isEqual) {
		$this->markFieldDirty("article_inherits_from", true);
	}
	$this->article_inherits_from = $fd->preSetData($this, $article_inherits_from);
	return $this;
}

protected static $_relationFields = array (
  'inherited_from' => 
  array (
    'type' => 'manyToOneRelation',
  ),
  'market_area_href' => 
  array (
    'type' => 'manyToOneRelation',
  ),
  'domain_href' => 
  array (
    'type' => 'manyToOneRelation',
  ),
  'merchant_href' => 
  array (
    'type' => 'manyToOneRelation',
  ),
  'categories' => 
  array (
    'type' => 'manyToManyObjectRelation',
  ),
  'tags' => 
  array (
    'type' => 'manyToManyRelation',
  ),
  'images' => 
  array (
    'type' => 'manyToManyRelation',
  ),
  'listing_inherits_from' => 
  array (
    'type' => 'manyToOneRelation',
  ),
  'deal_inherits_from' => 
  array (
    'type' => 'manyToOneRelation',
  ),
  'event_inherits_from' => 
  array (
    'type' => 'manyToOneRelation',
  ),
  'audio_inherits_from' => 
  array (
    'type' => 'manyToOneRelation',
  ),
  'video_inherits_from' => 
  array (
    'type' => 'manyToOneRelation',
  ),
  'news_inherits_from' => 
  array (
    'type' => 'manyToOneRelation',
  ),
  'article_inherits_from' => 
  array (
    'type' => 'manyToOneRelation',
  ),
);

protected $lazyLoadedFields = array (
  0 => 'inherited_from',
  1 => 'market_area_href',
  2 => 'domain_href',
  3 => 'merchant_href',
  4 => 'categories',
  5 => 'tags',
  6 => 'listing_inherits_from',
  7 => 'deal_inherits_from',
  8 => 'event_inherits_from',
  9 => 'audio_inherits_from',
  10 => 'video_inherits_from',
  11 => 'news_inherits_from',
  12 => 'article_inherits_from',
);

}

