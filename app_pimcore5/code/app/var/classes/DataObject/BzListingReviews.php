<?php 

/** 
* Generated at: 2019-09-03T10:27:48+02:00
* Inheritance: yes
* Variants: no
* Changed by: root (2)
* IP: ::1


Fields Summary: 
- inherited_from [manyToOneRelation]
- market_area_href [manyToOneRelation]
- domain_href [manyToOneRelation]
- merchant_href [manyToOneRelation]
- object_type [input]
- solution_name [input]
*/ 

namespace Pimcore\Model\DataObject;



/**
* @method static \Pimcore\Model\DataObject\BzListingReviews\Listing getByInherited_from ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\BzListingReviews\Listing getByMarket_area_href ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\BzListingReviews\Listing getByDomain_href ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\BzListingReviews\Listing getByMerchant_href ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\BzListingReviews\Listing getByObject_type ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\BzListingReviews\Listing getBySolution_name ($value, $limit = 0) 
*/

class BzListingReviews extends \Website\Model\DataObject\BzConcrete implements \Pimcore\Model\DataObject\DirtyIndicatorInterface {



use \Pimcore\Model\DataObject\Traits\DirtyIndicatorTrait;

protected $o_classId = "10";
protected $o_className = "BzListingReviews";
protected $inherited_from;
protected $market_area_href;
protected $domain_href;
protected $merchant_href;
protected $object_type;
protected $solution_name;


/**
* @param array $values
* @return \Pimcore\Model\DataObject\BzListingReviews
*/
public static function create($values = array()) {
	$object = new static();
	$object->setValues($values);
	return $object;
}

/**
* Get inherited_from - Inherited From Listing
* @return \Pimcore\Model\DataObject\BzListingReviews
*/
public function getInherited_from () {
	$preValue = $this->preGetValue("inherited_from"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->getClass()->getFieldDefinition("inherited_from")->preGetData($this);
	if(\Pimcore\Model\DataObject::doGetInheritedValues() && $this->getClass()->getFieldDefinition("inherited_from")->isEmpty($data)) {
		return $this->getValueFromParent("inherited_from");
	}
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set inherited_from - Inherited From Listing
* @param \Pimcore\Model\DataObject\BzListingReviews $inherited_from
* @return \Pimcore\Model\DataObject\BzListingReviews
*/
public function setInherited_from ($inherited_from) {
	$fd = $this->getClass()->getFieldDefinition("inherited_from");
	$currentData = $this->getInherited_from();
	$isEqual = $fd->isEqual($currentData, $inherited_from);
	if (!$isEqual) {
		$this->markFieldDirty("inherited_from", true);
	}
	$this->inherited_from = $fd->preSetData($this, $inherited_from);
	return $this;
}

/**
* Get market_area_href - market_area_href
* @return \Pimcore\Model\DataObject\BzMarketArea
*/
public function getMarket_area_href () {
	$preValue = $this->preGetValue("market_area_href"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->getClass()->getFieldDefinition("market_area_href")->preGetData($this);
	if(\Pimcore\Model\DataObject::doGetInheritedValues() && $this->getClass()->getFieldDefinition("market_area_href")->isEmpty($data)) {
		return $this->getValueFromParent("market_area_href");
	}
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set market_area_href - market_area_href
* @param \Pimcore\Model\DataObject\BzMarketArea $market_area_href
* @return \Pimcore\Model\DataObject\BzListingReviews
*/
public function setMarket_area_href ($market_area_href) {
	$fd = $this->getClass()->getFieldDefinition("market_area_href");
	$currentData = $this->getMarket_area_href();
	$isEqual = $fd->isEqual($currentData, $market_area_href);
	if (!$isEqual) {
		$this->markFieldDirty("market_area_href", true);
	}
	$this->market_area_href = $fd->preSetData($this, $market_area_href);
	return $this;
}

/**
* Get domain_href - Domain Href
* @return \Pimcore\Model\DataObject\BzDomain
*/
public function getDomain_href () {
	$preValue = $this->preGetValue("domain_href"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->getClass()->getFieldDefinition("domain_href")->preGetData($this);
	if(\Pimcore\Model\DataObject::doGetInheritedValues() && $this->getClass()->getFieldDefinition("domain_href")->isEmpty($data)) {
		return $this->getValueFromParent("domain_href");
	}
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set domain_href - Domain Href
* @param \Pimcore\Model\DataObject\BzDomain $domain_href
* @return \Pimcore\Model\DataObject\BzListingReviews
*/
public function setDomain_href ($domain_href) {
	$fd = $this->getClass()->getFieldDefinition("domain_href");
	$currentData = $this->getDomain_href();
	$isEqual = $fd->isEqual($currentData, $domain_href);
	if (!$isEqual) {
		$this->markFieldDirty("domain_href", true);
	}
	$this->domain_href = $fd->preSetData($this, $domain_href);
	return $this;
}

/**
* Get merchant_href - Merchant Href
* @return \Pimcore\Model\DataObject\BzMerchant
*/
public function getMerchant_href () {
	$preValue = $this->preGetValue("merchant_href"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->getClass()->getFieldDefinition("merchant_href")->preGetData($this);
	if(\Pimcore\Model\DataObject::doGetInheritedValues() && $this->getClass()->getFieldDefinition("merchant_href")->isEmpty($data)) {
		return $this->getValueFromParent("merchant_href");
	}
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set merchant_href - Merchant Href
* @param \Pimcore\Model\DataObject\BzMerchant $merchant_href
* @return \Pimcore\Model\DataObject\BzListingReviews
*/
public function setMerchant_href ($merchant_href) {
	$fd = $this->getClass()->getFieldDefinition("merchant_href");
	$currentData = $this->getMerchant_href();
	$isEqual = $fd->isEqual($currentData, $merchant_href);
	if (!$isEqual) {
		$this->markFieldDirty("merchant_href", true);
	}
	$this->merchant_href = $fd->preSetData($this, $merchant_href);
	return $this;
}

/**
* Get object_type - Object Type
* @return string
*/
public function getObject_type () {
	$preValue = $this->preGetValue("object_type"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->object_type;
	if(\Pimcore\Model\DataObject::doGetInheritedValues() && $this->getClass()->getFieldDefinition("object_type")->isEmpty($data)) {
		return $this->getValueFromParent("object_type");
	}
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set object_type - Object Type
* @param string $object_type
* @return \Pimcore\Model\DataObject\BzListingReviews
*/
public function setObject_type ($object_type) {
	$fd = $this->getClass()->getFieldDefinition("object_type");
	$this->object_type = $object_type;
	return $this;
}

/**
* Get solution_name - Solution Name
* @return string
*/
public function getSolution_name () {
	$preValue = $this->preGetValue("solution_name"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->solution_name;
	if(\Pimcore\Model\DataObject::doGetInheritedValues() && $this->getClass()->getFieldDefinition("solution_name")->isEmpty($data)) {
		return $this->getValueFromParent("solution_name");
	}
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set solution_name - Solution Name
* @param string $solution_name
* @return \Pimcore\Model\DataObject\BzListingReviews
*/
public function setSolution_name ($solution_name) {
	$fd = $this->getClass()->getFieldDefinition("solution_name");
	$this->solution_name = $solution_name;
	return $this;
}

protected static $_relationFields = array (
  'inherited_from' => 
  array (
    'type' => 'manyToOneRelation',
  ),
  'market_area_href' => 
  array (
    'type' => 'manyToOneRelation',
  ),
  'domain_href' => 
  array (
    'type' => 'manyToOneRelation',
  ),
  'merchant_href' => 
  array (
    'type' => 'manyToOneRelation',
  ),
);

protected $lazyLoadedFields = array (
);

}

