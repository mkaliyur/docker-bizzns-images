<?php 

namespace Pimcore\Model\DataObject\BzParent;

use Pimcore\Model\DataObject;

/**
 * @method DataObject\BzParent current()
 * @method DataObject\BzParent[] load()
 */

class Listing extends DataObject\Listing\Concrete {

protected $classId = "29";
protected $className = "BzParent";


}
