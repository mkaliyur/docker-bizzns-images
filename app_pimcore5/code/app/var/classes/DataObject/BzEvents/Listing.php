<?php 

namespace Pimcore\Model\DataObject\BzEvents;

use Pimcore\Model\DataObject;

/**
 * @method DataObject\BzEvents current()
 * @method DataObject\BzEvents[] load()
 */

class Listing extends DataObject\Listing\Concrete {

protected $classId = "30";
protected $className = "BzEvents";


}
