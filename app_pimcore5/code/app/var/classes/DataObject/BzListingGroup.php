<?php 

/** 
* Generated at: 2019-09-03T10:27:25+02:00
* Inheritance: no
* Variants: no
* Changed by: root (2)
* IP: ::1
* Listing groups helps to classify lists and charge differently for each group.


Fields Summary: 
- Name [input]
- Description [textarea]
- Title [input]
- domain [manyToOneRelation]
- object_type [input]
- solution_name [input]
*/ 

namespace Pimcore\Model\DataObject;



/**
* @method static \Pimcore\Model\DataObject\BzListingGroup\Listing getByName ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\BzListingGroup\Listing getByDescription ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\BzListingGroup\Listing getByTitle ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\BzListingGroup\Listing getByDomain ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\BzListingGroup\Listing getByObject_type ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\BzListingGroup\Listing getBySolution_name ($value, $limit = 0) 
*/

class BzListingGroup extends Concrete implements \Pimcore\Model\DataObject\DirtyIndicatorInterface {



use \Pimcore\Model\DataObject\Traits\DirtyIndicatorTrait;

protected $o_classId = "8";
protected $o_className = "BzListingGroup";
protected $Name;
protected $Description;
protected $Title;
protected $domain;
protected $object_type;
protected $solution_name;


/**
* @param array $values
* @return \Pimcore\Model\DataObject\BzListingGroup
*/
public static function create($values = array()) {
	$object = new static();
	$object->setValues($values);
	return $object;
}

/**
* Get Name - Name
* @return string
*/
public function getName () {
	$preValue = $this->preGetValue("Name"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->Name;
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set Name - Name
* @param string $Name
* @return \Pimcore\Model\DataObject\BzListingGroup
*/
public function setName ($Name) {
	$fd = $this->getClass()->getFieldDefinition("Name");
	$this->Name = $Name;
	return $this;
}

/**
* Get Description - Description
* @return string
*/
public function getDescription () {
	$preValue = $this->preGetValue("Description"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->Description;
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set Description - Description
* @param string $Description
* @return \Pimcore\Model\DataObject\BzListingGroup
*/
public function setDescription ($Description) {
	$fd = $this->getClass()->getFieldDefinition("Description");
	$this->Description = $Description;
	return $this;
}

/**
* Get Title - Title
* @return string
*/
public function getTitle () {
	$preValue = $this->preGetValue("Title"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->Title;
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set Title - Title
* @param string $Title
* @return \Pimcore\Model\DataObject\BzListingGroup
*/
public function setTitle ($Title) {
	$fd = $this->getClass()->getFieldDefinition("Title");
	$this->Title = $Title;
	return $this;
}

/**
* Get domain - Domain
* @return \Pimcore\Model\DataObject\BzDomain
*/
public function getDomain () {
	$preValue = $this->preGetValue("domain"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->getClass()->getFieldDefinition("domain")->preGetData($this);
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set domain - Domain
* @param \Pimcore\Model\DataObject\BzDomain $domain
* @return \Pimcore\Model\DataObject\BzListingGroup
*/
public function setDomain ($domain) {
	$fd = $this->getClass()->getFieldDefinition("domain");
	$currentData = $this->getDomain();
	$isEqual = $fd->isEqual($currentData, $domain);
	if (!$isEqual) {
		$this->markFieldDirty("domain", true);
	}
	$this->domain = $fd->preSetData($this, $domain);
	return $this;
}

/**
* Get object_type - Object Type
* @return string
*/
public function getObject_type () {
	$preValue = $this->preGetValue("object_type"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->object_type;
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set object_type - Object Type
* @param string $object_type
* @return \Pimcore\Model\DataObject\BzListingGroup
*/
public function setObject_type ($object_type) {
	$fd = $this->getClass()->getFieldDefinition("object_type");
	$this->object_type = $object_type;
	return $this;
}

/**
* Get solution_name - Solution Name
* @return string
*/
public function getSolution_name () {
	$preValue = $this->preGetValue("solution_name"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->solution_name;
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set solution_name - Solution Name
* @param string $solution_name
* @return \Pimcore\Model\DataObject\BzListingGroup
*/
public function setSolution_name ($solution_name) {
	$fd = $this->getClass()->getFieldDefinition("solution_name");
	$this->solution_name = $solution_name;
	return $this;
}

protected static $_relationFields = array (
  'domain' => 
  array (
    'type' => 'manyToOneRelation',
  ),
);

protected $lazyLoadedFields = array (
);

}

