<?php 

/** 
* Generated at: 2019-03-06T12:59:11+01:00


Fields Summary: 
 - RestaurantFeatureList [structuredTable]
*/ 

namespace Pimcore\Model\DataObject\Fieldcollection\Data;

use Pimcore\Model\DataObject;

class RestaurantFeatureList extends DataObject\Fieldcollection\Data\AbstractData implements \Pimcore\Model\DataObject\DirtyIndicatorInterface {

use \Pimcore\Model\DataObject\Traits\DirtyIndicatorTrait;

protected $type = "RestaurantFeatureList";
protected $RestaurantFeatureList;


/**
* Get RestaurantFeatureList - Restaurant FeatureList
* @return \Pimcore\Model\DataObject\Data\StructuredTable
*/
public function getRestaurantFeatureList () {
	$data = $this->RestaurantFeatureList;
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	 return $data;
}

/**
* Set RestaurantFeatureList - Restaurant FeatureList
* @param \Pimcore\Model\DataObject\Data\StructuredTable $RestaurantFeatureList
* @return \Pimcore\Model\DataObject\RestaurantFeatureList
*/
public function setRestaurantFeatureList ($RestaurantFeatureList) {
	$fd = $this->getDefinition()->getFieldDefinition("RestaurantFeatureList");
	$this->RestaurantFeatureList = $RestaurantFeatureList;
	return $this;
}

}

