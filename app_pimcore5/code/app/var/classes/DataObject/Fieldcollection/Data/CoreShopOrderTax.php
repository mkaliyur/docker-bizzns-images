<?php

/**
* Generated at: 2017-11-02T13:06:22+04:00
* IP: 192.168.0.104


Fields Summary:
 - name [input]
 - rate [numeric]
 - amount [numeric]
*/

namespace Pimcore\Model\DataObject\Fieldcollection\Data;

use Pimcore\Model\DataObject;

class CoreShopOrderTax extends \CoreShop\Model\Order\Tax  {

public $type = "CoreShopOrderTax";
public $name;
public $rate;
public $amount;


/**
* Get name - Name
* @return string
*/
public function getName () {
	$data = $this->name;
	 return $data;
}

/**
* Set name - Name
* @param string $name
* @return \Pimcore\Model\DataObject\CoreShopOrderTax
*/
public function setName ($name) {
	$this->name = $name;
	return $this;
}

/**
* Get rate - Rate
* @return float
*/
public function getRate () {
	$data = $this->rate;
	 return $data;
}

/**
* Set rate - Rate
* @param float $rate
* @return \Pimcore\Model\DataObject\CoreShopOrderTax
*/
public function setRate ($rate) {
	$this->rate = $rate;
	return $this;
}

/**
* Get amount - Amount
* @return float
*/
public function getAmount () {
	$data = $this->amount;
	 return $data;
}

/**
* Set amount - Amount
* @param float $amount
* @return \Pimcore\Model\DataObject\CoreShopOrderTax
*/
public function setAmount ($amount) {
	$this->amount = $amount;
	return $this;
}

}
