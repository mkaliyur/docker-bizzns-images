<?php 

/** 
* Generated at: 2019-03-06T12:59:11+01:00


Fields Summary: 
 - hotel_featurelist [structuredTable]
*/ 

namespace Pimcore\Model\DataObject\Fieldcollection\Data;

use Pimcore\Model\DataObject;

class HotelFeatureList extends DataObject\Fieldcollection\Data\AbstractData implements \Pimcore\Model\DataObject\DirtyIndicatorInterface {

use \Pimcore\Model\DataObject\Traits\DirtyIndicatorTrait;

protected $type = "HotelFeatureList";
protected $hotel_featurelist;


/**
* Get hotel_featurelist - Hotel Featurelist
* @return \Pimcore\Model\DataObject\Data\StructuredTable
*/
public function getHotel_featurelist () {
	$data = $this->hotel_featurelist;
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	 return $data;
}

/**
* Set hotel_featurelist - Hotel Featurelist
* @param \Pimcore\Model\DataObject\Data\StructuredTable $hotel_featurelist
* @return \Pimcore\Model\DataObject\HotelFeatureList
*/
public function setHotel_featurelist ($hotel_featurelist) {
	$fd = $this->getDefinition()->getFieldDefinition("hotel_featurelist");
	$this->hotel_featurelist = $hotel_featurelist;
	return $this;
}

}

