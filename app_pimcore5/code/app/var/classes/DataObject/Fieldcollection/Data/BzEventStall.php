<?php 

/** 
* Generated at: 2019-06-20T14:17:39+02:00
* IP: ::1


Fields Summary: 
 - name [input]
 - reservable_id [input]
 - category [input]
 - price [input]
 - status [select]
 - action [input]
 - typedisplayname [input]
 - inventory [slider]
*/ 

namespace Pimcore\Model\DataObject\Fieldcollection\Data;

use Pimcore\Model\DataObject;

class BzEventStall extends DataObject\Fieldcollection\Data\AbstractData implements \Pimcore\Model\DataObject\DirtyIndicatorInterface {

use \Pimcore\Model\DataObject\Traits\DirtyIndicatorTrait;

protected $type = "BzEventStall";
protected $name;
protected $reservable_id;
protected $category;
protected $price;
protected $status;
protected $action;
protected $typedisplayname;
protected $inventory;


/**
* Get name - Name
* @return string
*/
public function getName () {
	$data = $this->name;
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	 return $data;
}

/**
* Set name - Name
* @param string $name
* @return \Pimcore\Model\DataObject\BzEventStall
*/
public function setName ($name) {
	$fd = $this->getDefinition()->getFieldDefinition("name");
	$this->name = $name;
	return $this;
}

/**
* Get reservable_id - Reservable Id
* @return string
*/
public function getReservable_id () {
	$data = $this->reservable_id;
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	 return $data;
}

/**
* Set reservable_id - Reservable Id
* @param string $reservable_id
* @return \Pimcore\Model\DataObject\BzEventStall
*/
public function setReservable_id ($reservable_id) {
	$fd = $this->getDefinition()->getFieldDefinition("reservable_id");
	$this->reservable_id = $reservable_id;
	return $this;
}

/**
* Get category - Category
* @return string
*/
public function getCategory () {
	$data = $this->category;
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	 return $data;
}

/**
* Set category - Category
* @param string $category
* @return \Pimcore\Model\DataObject\BzEventStall
*/
public function setCategory ($category) {
	$fd = $this->getDefinition()->getFieldDefinition("category");
	$this->category = $category;
	return $this;
}

/**
* Get price - Price
* @return string
*/
public function getPrice () {
	$data = $this->price;
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	 return $data;
}

/**
* Set price - Price
* @param string $price
* @return \Pimcore\Model\DataObject\BzEventStall
*/
public function setPrice ($price) {
	$fd = $this->getDefinition()->getFieldDefinition("price");
	$this->price = $price;
	return $this;
}

/**
* Get status - Status
* @return string
*/
public function getStatus () {
	$data = $this->status;
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	 return $data;
}

/**
* Set status - Status
* @param string $status
* @return \Pimcore\Model\DataObject\BzEventStall
*/
public function setStatus ($status) {
	$fd = $this->getDefinition()->getFieldDefinition("status");
	$this->status = $status;
	return $this;
}

/**
* Get action - action
* @return string
*/
public function getAction () {
	$data = $this->action;
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	 return $data;
}

/**
* Set action - action
* @param string $action
* @return \Pimcore\Model\DataObject\BzEventStall
*/
public function setAction ($action) {
	$fd = $this->getDefinition()->getFieldDefinition("action");
	$this->action = $action;
	return $this;
}

/**
* Get typedisplayname - Type Display Name
* @return string
*/
public function getTypedisplayname () {
	$data = $this->typedisplayname;
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	 return $data;
}

/**
* Set typedisplayname - Type Display Name
* @param string $typedisplayname
* @return \Pimcore\Model\DataObject\BzEventStall
*/
public function setTypedisplayname ($typedisplayname) {
	$fd = $this->getDefinition()->getFieldDefinition("typedisplayname");
	$this->typedisplayname = $typedisplayname;
	return $this;
}

/**
* Get inventory - inventory
* @return float
*/
public function getInventory () {
	$data = $this->inventory;
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	 return $data;
}

/**
* Set inventory - inventory
* @param float $inventory
* @return \Pimcore\Model\DataObject\BzEventStall
*/
public function setInventory ($inventory) {
	$fd = $this->getDefinition()->getFieldDefinition("inventory");
	$this->inventory = $inventory;
	return $this;
}

}

