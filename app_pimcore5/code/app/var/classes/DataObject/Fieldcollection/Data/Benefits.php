<?php 

/** 
* Generated at: 2019-03-06T12:59:11+01:00


Fields Summary: 
 - benefitTitle [input]
 - benefitDescription [wysiwyg]
 - benefitImage [manyToOneRelation]
*/ 

namespace Pimcore\Model\DataObject\Fieldcollection\Data;

use Pimcore\Model\DataObject;

class Benefits extends DataObject\Fieldcollection\Data\AbstractData implements \Pimcore\Model\DataObject\DirtyIndicatorInterface {

use \Pimcore\Model\DataObject\Traits\DirtyIndicatorTrait;

protected $type = "benefits";
protected $benefitTitle;
protected $benefitDescription;
protected $benefitImage;


/**
* Get benefitTitle - Benefit Title
* @return string
*/
public function getBenefitTitle () {
	$data = $this->benefitTitle;
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	 return $data;
}

/**
* Set benefitTitle - Benefit Title
* @param string $benefitTitle
* @return \Pimcore\Model\DataObject\Benefits
*/
public function setBenefitTitle ($benefitTitle) {
	$fd = $this->getDefinition()->getFieldDefinition("benefitTitle");
	$this->benefitTitle = $benefitTitle;
	return $this;
}

/**
* Get benefitDescription - Benefit Description
* @return string
*/
public function getBenefitDescription () {
	$container = $this;
	$fd = $this->getDefinition()->getFieldDefinition("benefitDescription");
	$data = $fd->preGetData($container);
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	 return $data;
}

/**
* Set benefitDescription - Benefit Description
* @param string $benefitDescription
* @return \Pimcore\Model\DataObject\Benefits
*/
public function setBenefitDescription ($benefitDescription) {
	$fd = $this->getDefinition()->getFieldDefinition("benefitDescription");
	$this->benefitDescription = $benefitDescription;
	return $this;
}

/**
* Get benefitImage - Benefit Image
* @return \Pimcore\Model\Asset\Image
*/
public function getBenefitImage () {
	$container = $this;
	$fd = $this->getDefinition()->getFieldDefinition("benefitImage");
	$data = $fd->preGetData($container);
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	 return $data;
}

/**
* Set benefitImage - Benefit Image
* @param \Pimcore\Model\Asset\Image $benefitImage
* @return \Pimcore\Model\DataObject\Benefits
*/
public function setBenefitImage ($benefitImage) {
	$fd = $this->getDefinition()->getFieldDefinition("benefitImage");
	$currentData = $this->getBenefitImage();
	$isEqual = $fd->isEqual($currentData, $benefitImage);
	if (!$isEqual) {
		$this->markFieldDirty("benefitImage", true);
	}
	$this->benefitImage = $fd->preSetData($this, $benefitImage);
	return $this;
}

}

