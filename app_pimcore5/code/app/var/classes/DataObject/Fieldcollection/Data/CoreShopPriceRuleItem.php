<?php 

/**
* Generated at: 2017-11-02T13:06:26+04:00
* IP: 192.168.0.104


Fields Summary:
 - priceRule [coreShopPriceRule]
 - voucherCode [input]
 - discount [numeric]
*/

namespace Pimcore\Model\DataObject\Fieldcollection\Data;

use Pimcore\Model\DataObject;

class CoreShopPriceRuleItem extends \CoreShop\Model\PriceRule\Item  {

public $type = "CoreShopPriceRuleItem";
public $priceRule;
public $voucherCode;
public $discount;


/**
* Get priceRule - Price Rule
* @return CoreShop\Model\Cart\PriceRule
*/
public function getPriceRule () {
	$container = $this;
	$fd = $this->getDefinition()->getFieldDefinition("priceRule");
	$data = $fd->preGetData($container);
	 return $data;
}

/**
* Set priceRule - Price Rule
* @param CoreShop\Model\Cart\PriceRule $priceRule
* @return \Pimcore\Model\DataObject\CoreShopPriceRuleItem
*/
public function setPriceRule ($priceRule) {
	$this->priceRule = $this->getDefinition()->getFieldDefinition("priceRule")->preSetData($this, $priceRule);
	return $this;
}

/**
* Get voucherCode - Voucher Code
* @return string
*/
public function getVoucherCode () {
	$data = $this->voucherCode;
	 return $data;
}

/**
* Set voucherCode - Voucher Code
* @param string $voucherCode
* @return \Pimcore\Model\DataObject\CoreShopPriceRuleItem
*/
public function setVoucherCode ($voucherCode) {
	$this->voucherCode = $voucherCode;
	return $this;
}

/**
* Get discount - Discount
* @return float
*/
public function getDiscount () {
	$data = $this->discount;
	 return $data;
}

/**
* Set discount - Discount
* @param float $discount
* @return \Pimcore\Model\DataObject\CoreShopPriceRuleItem
*/
public function setDiscount ($discount) {
	$this->discount = $discount;
	return $this;
}

}
