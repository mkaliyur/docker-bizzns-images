<?php 

/** 
* Generated at: 2019-03-06T12:59:11+01:00


Fields Summary: 
 - video_source [select]
 - video_url [input]
*/ 

namespace Pimcore\Model\DataObject\Fieldcollection\Data;

use Pimcore\Model\DataObject;

class VideoResouces extends DataObject\Fieldcollection\Data\AbstractData implements \Pimcore\Model\DataObject\DirtyIndicatorInterface {

use \Pimcore\Model\DataObject\Traits\DirtyIndicatorTrait;

protected $type = "videoResouces";
protected $video_source;
protected $video_url;


/**
* Get video_source - Video Source
* @return string
*/
public function getVideo_source () {
	$data = $this->video_source;
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	 return $data;
}

/**
* Set video_source - Video Source
* @param string $video_source
* @return \Pimcore\Model\DataObject\VideoResouces
*/
public function setVideo_source ($video_source) {
	$fd = $this->getDefinition()->getFieldDefinition("video_source");
	$this->video_source = $video_source;
	return $this;
}

/**
* Get video_url - video_url
* @return string
*/
public function getVideo_url () {
	$data = $this->video_url;
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	 return $data;
}

/**
* Set video_url - video_url
* @param string $video_url
* @return \Pimcore\Model\DataObject\VideoResouces
*/
public function setVideo_url ($video_url) {
	$fd = $this->getDefinition()->getFieldDefinition("video_url");
	$this->video_url = $video_url;
	return $this;
}

}

