<?php 

/** 
* Generated at: 2019-03-06T12:59:11+01:00


Fields Summary: 
 - domain [select]
*/ 

namespace Pimcore\Model\DataObject\Fieldcollection\Data;

use Pimcore\Model\DataObject;

class Domains extends DataObject\Fieldcollection\Data\AbstractData implements \Pimcore\Model\DataObject\DirtyIndicatorInterface {

use \Pimcore\Model\DataObject\Traits\DirtyIndicatorTrait;

protected $type = "domains";
protected $domain;


/**
* Get domain - Domain
* @return string
*/
public function getDomain () {
	$data = $this->domain;
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	 return $data;
}

/**
* Set domain - Domain
* @param string $domain
* @return \Pimcore\Model\DataObject\Domains
*/
public function setDomain ($domain) {
	$fd = $this->getDefinition()->getFieldDefinition("domain");
	$this->domain = $domain;
	return $this;
}

}

