<?php 

/** 
* Generated at: 2019-03-06T12:59:11+01:00


Fields Summary: 
 - social_media [select]
 - url [input]
*/ 

namespace Pimcore\Model\DataObject\Fieldcollection\Data;

use Pimcore\Model\DataObject;

class SocialMedia extends DataObject\Fieldcollection\Data\AbstractData implements \Pimcore\Model\DataObject\DirtyIndicatorInterface {

use \Pimcore\Model\DataObject\Traits\DirtyIndicatorTrait;

protected $type = "SocialMedia";
protected $social_media;
protected $url;


/**
* Get social_media - Social Media
* @return string
*/
public function getSocial_media () {
	$data = $this->social_media;
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	 return $data;
}

/**
* Set social_media - Social Media
* @param string $social_media
* @return \Pimcore\Model\DataObject\SocialMedia
*/
public function setSocial_media ($social_media) {
	$fd = $this->getDefinition()->getFieldDefinition("social_media");
	$this->social_media = $social_media;
	return $this;
}

/**
* Get url - url
* @return string
*/
public function getUrl () {
	$data = $this->url;
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	 return $data;
}

/**
* Set url - url
* @param string $url
* @return \Pimcore\Model\DataObject\SocialMedia
*/
public function setUrl ($url) {
	$fd = $this->getDefinition()->getFieldDefinition("url");
	$this->url = $url;
	return $this;
}

}

