<?php 

/** 
* Generated at: 2019-04-30T14:43:01+02:00
* IP: ::1


Fields Summary: 
 - amenities [structuredTable]
*/ 

namespace Pimcore\Model\DataObject\Fieldcollection\Data;

use Pimcore\Model\DataObject;

class Amenities extends DataObject\Fieldcollection\Data\AbstractData implements \Pimcore\Model\DataObject\DirtyIndicatorInterface {

use \Pimcore\Model\DataObject\Traits\DirtyIndicatorTrait;

protected $type = "Amenities";
protected $amenities;


/**
* Get amenities - amenities
* @return \Pimcore\Model\DataObject\Data\StructuredTable
*/
public function getAmenities () {
	$data = $this->amenities;
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	 return $data;
}

/**
* Set amenities - amenities
* @param \Pimcore\Model\DataObject\Data\StructuredTable $amenities
* @return \Pimcore\Model\DataObject\Amenities
*/
public function setAmenities ($amenities) {
	$fd = $this->getDefinition()->getFieldDefinition("amenities");
	$this->amenities = $amenities;
	return $this;
}

}

