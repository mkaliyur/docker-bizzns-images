<?php 

/** 
* Generated at: 2019-03-06T12:59:11+01:00


Fields Summary: 
 - Author [input]
 - testimonial_text [wysiwyg]
 - author_image [image]
 - author_website [input]
*/ 

namespace Pimcore\Model\DataObject\Fieldcollection\Data;

use Pimcore\Model\DataObject;

class Testimonials extends DataObject\Fieldcollection\Data\AbstractData implements \Pimcore\Model\DataObject\DirtyIndicatorInterface {

use \Pimcore\Model\DataObject\Traits\DirtyIndicatorTrait;

protected $type = "Testimonials";
protected $Author;
protected $testimonial_text;
protected $author_image;
protected $author_website;


/**
* Get Author - Author
* @return string
*/
public function getAuthor () {
	$data = $this->Author;
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	 return $data;
}

/**
* Set Author - Author
* @param string $Author
* @return \Pimcore\Model\DataObject\Testimonials
*/
public function setAuthor ($Author) {
	$fd = $this->getDefinition()->getFieldDefinition("Author");
	$this->Author = $Author;
	return $this;
}

/**
* Get testimonial_text - Testimonial Text
* @return string
*/
public function getTestimonial_text () {
	$container = $this;
	$fd = $this->getDefinition()->getFieldDefinition("testimonial_text");
	$data = $fd->preGetData($container);
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	 return $data;
}

/**
* Set testimonial_text - Testimonial Text
* @param string $testimonial_text
* @return \Pimcore\Model\DataObject\Testimonials
*/
public function setTestimonial_text ($testimonial_text) {
	$fd = $this->getDefinition()->getFieldDefinition("testimonial_text");
	$this->testimonial_text = $testimonial_text;
	return $this;
}

/**
* Get author_image - author_image
* @return \Pimcore\Model\Asset\Image
*/
public function getAuthor_image () {
	$data = $this->author_image;
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	 return $data;
}

/**
* Set author_image - author_image
* @param \Pimcore\Model\Asset\Image $author_image
* @return \Pimcore\Model\DataObject\Testimonials
*/
public function setAuthor_image ($author_image) {
	$fd = $this->getDefinition()->getFieldDefinition("author_image");
	$this->author_image = $author_image;
	return $this;
}

/**
* Get author_website - author_website
* @return string
*/
public function getAuthor_website () {
	$data = $this->author_website;
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	 return $data;
}

/**
* Set author_website - author_website
* @param string $author_website
* @return \Pimcore\Model\DataObject\Testimonials
*/
public function setAuthor_website ($author_website) {
	$fd = $this->getDefinition()->getFieldDefinition("author_website");
	$this->author_website = $author_website;
	return $this;
}

}

