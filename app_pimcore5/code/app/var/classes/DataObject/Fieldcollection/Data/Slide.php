<?php 

/** 
* Generated at: 2019-03-06T12:59:11+01:00


Fields Summary: 
 - image [image]
 - caption [input]
 - subCaption [input]
*/ 

namespace Pimcore\Model\DataObject\Fieldcollection\Data;

use Pimcore\Model\DataObject;

class Slide extends DataObject\Fieldcollection\Data\AbstractData implements \Pimcore\Model\DataObject\DirtyIndicatorInterface {

use \Pimcore\Model\DataObject\Traits\DirtyIndicatorTrait;

protected $type = "slide";
protected $image;
protected $caption;
protected $subCaption;


/**
* Get image - Slide Image
* @return \Pimcore\Model\Asset\Image
*/
public function getImage () {
	$data = $this->image;
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	 return $data;
}

/**
* Set image - Slide Image
* @param \Pimcore\Model\Asset\Image $image
* @return \Pimcore\Model\DataObject\Slide
*/
public function setImage ($image) {
	$fd = $this->getDefinition()->getFieldDefinition("image");
	$this->image = $image;
	return $this;
}

/**
* Get caption - Lead Caption
* @return string
*/
public function getCaption () {
	$data = $this->caption;
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	 return $data;
}

/**
* Set caption - Lead Caption
* @param string $caption
* @return \Pimcore\Model\DataObject\Slide
*/
public function setCaption ($caption) {
	$fd = $this->getDefinition()->getFieldDefinition("caption");
	$this->caption = $caption;
	return $this;
}

/**
* Get subCaption - Sub-Caption
* @return string
*/
public function getSubCaption () {
	$data = $this->subCaption;
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	 return $data;
}

/**
* Set subCaption - Sub-Caption
* @param string $subCaption
* @return \Pimcore\Model\DataObject\Slide
*/
public function setSubCaption ($subCaption) {
	$fd = $this->getDefinition()->getFieldDefinition("subCaption");
	$this->subCaption = $subCaption;
	return $this;
}

}

