<?php 

/** 
* Generated at: 2019-03-06T12:59:11+01:00


Fields Summary: 
 - setting_name [input]
 - setting_value [input]
*/ 

namespace Pimcore\Model\DataObject\Fieldcollection\Data;

use Pimcore\Model\DataObject;

class Settings extends DataObject\Fieldcollection\Data\AbstractData implements \Pimcore\Model\DataObject\DirtyIndicatorInterface {

use \Pimcore\Model\DataObject\Traits\DirtyIndicatorTrait;

protected $type = "settings";
protected $setting_name;
protected $setting_value;


/**
* Get setting_name - setting_name
* @return string
*/
public function getSetting_name () {
	$data = $this->setting_name;
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	 return $data;
}

/**
* Set setting_name - setting_name
* @param string $setting_name
* @return \Pimcore\Model\DataObject\Settings
*/
public function setSetting_name ($setting_name) {
	$fd = $this->getDefinition()->getFieldDefinition("setting_name");
	$this->setting_name = $setting_name;
	return $this;
}

/**
* Get setting_value - setting_value
* @return string
*/
public function getSetting_value () {
	$data = $this->setting_value;
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	 return $data;
}

/**
* Set setting_value - setting_value
* @param string $setting_value
* @return \Pimcore\Model\DataObject\Settings
*/
public function setSetting_value ($setting_value) {
	$fd = $this->getDefinition()->getFieldDefinition("setting_value");
	$this->setting_value = $setting_value;
	return $this;
}

}

