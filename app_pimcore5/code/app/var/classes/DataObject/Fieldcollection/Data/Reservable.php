<?php 

/** 
* Generated at: 2019-06-04T11:50:25+02:00
* IP: ::1


Fields Summary: 
 - Reservable [structuredTable]
*/ 

namespace Pimcore\Model\DataObject\Fieldcollection\Data;

use Pimcore\Model\DataObject;

class Reservable extends DataObject\Fieldcollection\Data\AbstractData implements \Pimcore\Model\DataObject\DirtyIndicatorInterface {

use \Pimcore\Model\DataObject\Traits\DirtyIndicatorTrait;

protected $type = "Reservable";
protected $Reservable;


/**
* Get Reservable - Reservable
* @return \Pimcore\Model\DataObject\Data\StructuredTable
*/
public function getReservable () {
	$data = $this->Reservable;
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	 return $data;
}

/**
* Set Reservable - Reservable
* @param \Pimcore\Model\DataObject\Data\StructuredTable $Reservable
* @return \Pimcore\Model\DataObject\Reservable
*/
public function setReservable ($Reservable) {
	$fd = $this->getDefinition()->getFieldDefinition("Reservable");
	$this->Reservable = $Reservable;
	return $this;
}

}

