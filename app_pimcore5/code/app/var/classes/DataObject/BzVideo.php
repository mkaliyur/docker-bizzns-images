<?php 

/** 
* Generated at: 2019-08-30T11:40:06+02:00
* Inheritance: yes
* Variants: no
* Changed by: root (2)
* IP: ::1


Fields Summary: 
- listing_inherits_from [manyToOneRelation]
- deal_inherits_from [manyToOneRelation]
- event_inherits_from [manyToOneRelation]
- audio_inherits_from [manyToOneRelation]
- video_inherits_from [manyToOneRelation]
- article_inherits_from [manyToOneRelation]
- news_inherits_from [manyToOneRelation]
- inherited_from [manyToOneRelation]
- videoType [select]
- name [input]
- solution_name [input]
- object_type [input]
- slug [input]
- videoURL [input]
- videoID [input]
- localVideoPath [manyToOneRelation]
- video_blog [wysiwyg]
- video_transcript [wysiwyg]
- isLocal [checkbox]
- market_area_href [manyToOneRelation]
- domain_href [manyToOneRelation]
- merchant_href [manyToOneRelation]
- title [input]
- type_display_name [input]
*/ 

namespace Pimcore\Model\DataObject;



/**
* @method static \Pimcore\Model\DataObject\BzVideo\Listing getByListing_inherits_from ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\BzVideo\Listing getByDeal_inherits_from ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\BzVideo\Listing getByEvent_inherits_from ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\BzVideo\Listing getByAudio_inherits_from ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\BzVideo\Listing getByVideo_inherits_from ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\BzVideo\Listing getByArticle_inherits_from ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\BzVideo\Listing getByNews_inherits_from ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\BzVideo\Listing getByInherited_from ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\BzVideo\Listing getByVideoType ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\BzVideo\Listing getByName ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\BzVideo\Listing getBySolution_name ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\BzVideo\Listing getByObject_type ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\BzVideo\Listing getBySlug ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\BzVideo\Listing getByVideoURL ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\BzVideo\Listing getByVideoID ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\BzVideo\Listing getByLocalVideoPath ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\BzVideo\Listing getByVideo_blog ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\BzVideo\Listing getByVideo_transcript ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\BzVideo\Listing getByIsLocal ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\BzVideo\Listing getByMarket_area_href ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\BzVideo\Listing getByDomain_href ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\BzVideo\Listing getByMerchant_href ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\BzVideo\Listing getByTitle ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\BzVideo\Listing getByType_display_name ($value, $limit = 0) 
*/

class BzVideo extends \Website\Model\DataObject\BzConcreteVideo implements \Pimcore\Model\DataObject\DirtyIndicatorInterface {



use \Pimcore\Model\DataObject\Traits\DirtyIndicatorTrait;

protected $o_classId = "23";
protected $o_className = "BzVideo";
protected $listing_inherits_from;
protected $deal_inherits_from;
protected $event_inherits_from;
protected $audio_inherits_from;
protected $video_inherits_from;
protected $article_inherits_from;
protected $news_inherits_from;
protected $inherited_from;
protected $videoType;
protected $name;
protected $solution_name;
protected $object_type;
protected $slug;
protected $videoURL;
protected $videoID;
protected $localVideoPath;
protected $video_blog;
protected $video_transcript;
protected $isLocal;
protected $market_area_href;
protected $domain_href;
protected $merchant_href;
protected $title;
protected $type_display_name;


/**
* @param array $values
* @return \Pimcore\Model\DataObject\BzVideo
*/
public static function create($values = array()) {
	$object = new static();
	$object->setValues($values);
	return $object;
}

/**
* Get listing_inherits_from - Listing Inherits From
* @return \Pimcore\Model\DataObject\BzListing2
*/
public function getListing_inherits_from () {
	$preValue = $this->preGetValue("listing_inherits_from"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->getClass()->getFieldDefinition("listing_inherits_from")->preGetData($this);
	if(\Pimcore\Model\DataObject::doGetInheritedValues() && $this->getClass()->getFieldDefinition("listing_inherits_from")->isEmpty($data)) {
		return $this->getValueFromParent("listing_inherits_from");
	}
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set listing_inherits_from - Listing Inherits From
* @param \Pimcore\Model\DataObject\BzListing2 $listing_inherits_from
* @return \Pimcore\Model\DataObject\BzVideo
*/
public function setListing_inherits_from ($listing_inherits_from) {
	$fd = $this->getClass()->getFieldDefinition("listing_inherits_from");
	$currentData = $this->getListing_inherits_from();
	$isEqual = $fd->isEqual($currentData, $listing_inherits_from);
	if (!$isEqual) {
		$this->markFieldDirty("listing_inherits_from", true);
	}
	$this->listing_inherits_from = $fd->preSetData($this, $listing_inherits_from);
	return $this;
}

/**
* Get deal_inherits_from - Deal Inherits From
* @return \Pimcore\Model\DataObject\BzDeals
*/
public function getDeal_inherits_from () {
	$preValue = $this->preGetValue("deal_inherits_from"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->getClass()->getFieldDefinition("deal_inherits_from")->preGetData($this);
	if(\Pimcore\Model\DataObject::doGetInheritedValues() && $this->getClass()->getFieldDefinition("deal_inherits_from")->isEmpty($data)) {
		return $this->getValueFromParent("deal_inherits_from");
	}
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set deal_inherits_from - Deal Inherits From
* @param \Pimcore\Model\DataObject\BzDeals $deal_inherits_from
* @return \Pimcore\Model\DataObject\BzVideo
*/
public function setDeal_inherits_from ($deal_inherits_from) {
	$fd = $this->getClass()->getFieldDefinition("deal_inherits_from");
	$currentData = $this->getDeal_inherits_from();
	$isEqual = $fd->isEqual($currentData, $deal_inherits_from);
	if (!$isEqual) {
		$this->markFieldDirty("deal_inherits_from", true);
	}
	$this->deal_inherits_from = $fd->preSetData($this, $deal_inherits_from);
	return $this;
}

/**
* Get event_inherits_from - event_inherits_from
* @return \Pimcore\Model\DataObject\BzEvents
*/
public function getEvent_inherits_from () {
	$preValue = $this->preGetValue("event_inherits_from"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->getClass()->getFieldDefinition("event_inherits_from")->preGetData($this);
	if(\Pimcore\Model\DataObject::doGetInheritedValues() && $this->getClass()->getFieldDefinition("event_inherits_from")->isEmpty($data)) {
		return $this->getValueFromParent("event_inherits_from");
	}
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set event_inherits_from - event_inherits_from
* @param \Pimcore\Model\DataObject\BzEvents $event_inherits_from
* @return \Pimcore\Model\DataObject\BzVideo
*/
public function setEvent_inherits_from ($event_inherits_from) {
	$fd = $this->getClass()->getFieldDefinition("event_inherits_from");
	$currentData = $this->getEvent_inherits_from();
	$isEqual = $fd->isEqual($currentData, $event_inherits_from);
	if (!$isEqual) {
		$this->markFieldDirty("event_inherits_from", true);
	}
	$this->event_inherits_from = $fd->preSetData($this, $event_inherits_from);
	return $this;
}

/**
* Get audio_inherits_from - audio_inherits_from
* @return \Pimcore\Model\DataObject\BzAudio
*/
public function getAudio_inherits_from () {
	$preValue = $this->preGetValue("audio_inherits_from"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->getClass()->getFieldDefinition("audio_inherits_from")->preGetData($this);
	if(\Pimcore\Model\DataObject::doGetInheritedValues() && $this->getClass()->getFieldDefinition("audio_inherits_from")->isEmpty($data)) {
		return $this->getValueFromParent("audio_inherits_from");
	}
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set audio_inherits_from - audio_inherits_from
* @param \Pimcore\Model\DataObject\BzAudio $audio_inherits_from
* @return \Pimcore\Model\DataObject\BzVideo
*/
public function setAudio_inherits_from ($audio_inherits_from) {
	$fd = $this->getClass()->getFieldDefinition("audio_inherits_from");
	$currentData = $this->getAudio_inherits_from();
	$isEqual = $fd->isEqual($currentData, $audio_inherits_from);
	if (!$isEqual) {
		$this->markFieldDirty("audio_inherits_from", true);
	}
	$this->audio_inherits_from = $fd->preSetData($this, $audio_inherits_from);
	return $this;
}

/**
* Get video_inherits_from - video_inherits_from
* @return \Pimcore\Model\DataObject\BzVideo
*/
public function getVideo_inherits_from () {
	$preValue = $this->preGetValue("video_inherits_from"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->getClass()->getFieldDefinition("video_inherits_from")->preGetData($this);
	if(\Pimcore\Model\DataObject::doGetInheritedValues() && $this->getClass()->getFieldDefinition("video_inherits_from")->isEmpty($data)) {
		return $this->getValueFromParent("video_inherits_from");
	}
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set video_inherits_from - video_inherits_from
* @param \Pimcore\Model\DataObject\BzVideo $video_inherits_from
* @return \Pimcore\Model\DataObject\BzVideo
*/
public function setVideo_inherits_from ($video_inherits_from) {
	$fd = $this->getClass()->getFieldDefinition("video_inherits_from");
	$currentData = $this->getVideo_inherits_from();
	$isEqual = $fd->isEqual($currentData, $video_inherits_from);
	if (!$isEqual) {
		$this->markFieldDirty("video_inherits_from", true);
	}
	$this->video_inherits_from = $fd->preSetData($this, $video_inherits_from);
	return $this;
}

/**
* Get article_inherits_from - Article Inherits From
* @return \Pimcore\Model\DataObject\BzBlogArticle
*/
public function getArticle_inherits_from () {
	$preValue = $this->preGetValue("article_inherits_from"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->getClass()->getFieldDefinition("article_inherits_from")->preGetData($this);
	if(\Pimcore\Model\DataObject::doGetInheritedValues() && $this->getClass()->getFieldDefinition("article_inherits_from")->isEmpty($data)) {
		return $this->getValueFromParent("article_inherits_from");
	}
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set article_inherits_from - Article Inherits From
* @param \Pimcore\Model\DataObject\BzBlogArticle $article_inherits_from
* @return \Pimcore\Model\DataObject\BzVideo
*/
public function setArticle_inherits_from ($article_inherits_from) {
	$fd = $this->getClass()->getFieldDefinition("article_inherits_from");
	$currentData = $this->getArticle_inherits_from();
	$isEqual = $fd->isEqual($currentData, $article_inherits_from);
	if (!$isEqual) {
		$this->markFieldDirty("article_inherits_from", true);
	}
	$this->article_inherits_from = $fd->preSetData($this, $article_inherits_from);
	return $this;
}

/**
* Get news_inherits_from - News Inherits From
* @return \Pimcore\Model\DataObject\News
*/
public function getNews_inherits_from () {
	$preValue = $this->preGetValue("news_inherits_from"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->getClass()->getFieldDefinition("news_inherits_from")->preGetData($this);
	if(\Pimcore\Model\DataObject::doGetInheritedValues() && $this->getClass()->getFieldDefinition("news_inherits_from")->isEmpty($data)) {
		return $this->getValueFromParent("news_inherits_from");
	}
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set news_inherits_from - News Inherits From
* @param \Pimcore\Model\DataObject\News $news_inherits_from
* @return \Pimcore\Model\DataObject\BzVideo
*/
public function setNews_inherits_from ($news_inherits_from) {
	$fd = $this->getClass()->getFieldDefinition("news_inherits_from");
	$currentData = $this->getNews_inherits_from();
	$isEqual = $fd->isEqual($currentData, $news_inherits_from);
	if (!$isEqual) {
		$this->markFieldDirty("news_inherits_from", true);
	}
	$this->news_inherits_from = $fd->preSetData($this, $news_inherits_from);
	return $this;
}

/**
* Get inherited_from - Inherited From Video
* @return \Pimcore\Model\DataObject\BzVideo
*/
public function getInherited_from () {
	$preValue = $this->preGetValue("inherited_from"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->getClass()->getFieldDefinition("inherited_from")->preGetData($this);
	if(\Pimcore\Model\DataObject::doGetInheritedValues() && $this->getClass()->getFieldDefinition("inherited_from")->isEmpty($data)) {
		return $this->getValueFromParent("inherited_from");
	}
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set inherited_from - Inherited From Video
* @param \Pimcore\Model\DataObject\BzVideo $inherited_from
* @return \Pimcore\Model\DataObject\BzVideo
*/
public function setInherited_from ($inherited_from) {
	$fd = $this->getClass()->getFieldDefinition("inherited_from");
	$currentData = $this->getInherited_from();
	$isEqual = $fd->isEqual($currentData, $inherited_from);
	if (!$isEqual) {
		$this->markFieldDirty("inherited_from", true);
	}
	$this->inherited_from = $fd->preSetData($this, $inherited_from);
	return $this;
}

/**
* Get videoType - Video Type
* @return string
*/
public function getVideoType () {
	$preValue = $this->preGetValue("videoType"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->videoType;
	if(\Pimcore\Model\DataObject::doGetInheritedValues() && $this->getClass()->getFieldDefinition("videoType")->isEmpty($data)) {
		return $this->getValueFromParent("videoType");
	}
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set videoType - Video Type
* @param string $videoType
* @return \Pimcore\Model\DataObject\BzVideo
*/
public function setVideoType ($videoType) {
	$fd = $this->getClass()->getFieldDefinition("videoType");
	$this->videoType = $videoType;
	return $this;
}

/**
* Get name - Name
* @return string
*/
public function getName () {
	$preValue = $this->preGetValue("name"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->name;
	if(\Pimcore\Model\DataObject::doGetInheritedValues() && $this->getClass()->getFieldDefinition("name")->isEmpty($data)) {
		return $this->getValueFromParent("name");
	}
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set name - Name
* @param string $name
* @return \Pimcore\Model\DataObject\BzVideo
*/
public function setName ($name) {
	$fd = $this->getClass()->getFieldDefinition("name");
	$this->name = $name;
	return $this;
}

/**
* Get solution_name - Solution Name
* @return string
*/
public function getSolution_name () {
	$preValue = $this->preGetValue("solution_name"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->solution_name;
	if(\Pimcore\Model\DataObject::doGetInheritedValues() && $this->getClass()->getFieldDefinition("solution_name")->isEmpty($data)) {
		return $this->getValueFromParent("solution_name");
	}
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set solution_name - Solution Name
* @param string $solution_name
* @return \Pimcore\Model\DataObject\BzVideo
*/
public function setSolution_name ($solution_name) {
	$fd = $this->getClass()->getFieldDefinition("solution_name");
	$this->solution_name = $solution_name;
	return $this;
}

/**
* Get object_type - Object Type
* @return string
*/
public function getObject_type () {
	$preValue = $this->preGetValue("object_type"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->object_type;
	if(\Pimcore\Model\DataObject::doGetInheritedValues() && $this->getClass()->getFieldDefinition("object_type")->isEmpty($data)) {
		return $this->getValueFromParent("object_type");
	}
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set object_type - Object Type
* @param string $object_type
* @return \Pimcore\Model\DataObject\BzVideo
*/
public function setObject_type ($object_type) {
	$fd = $this->getClass()->getFieldDefinition("object_type");
	$this->object_type = $object_type;
	return $this;
}

/**
* Get slug - slug
* @return string
*/
public function getSlug () {
	$preValue = $this->preGetValue("slug"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->slug;
	if(\Pimcore\Model\DataObject::doGetInheritedValues() && $this->getClass()->getFieldDefinition("slug")->isEmpty($data)) {
		return $this->getValueFromParent("slug");
	}
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set slug - slug
* @param string $slug
* @return \Pimcore\Model\DataObject\BzVideo
*/
public function setSlug ($slug) {
	$fd = $this->getClass()->getFieldDefinition("slug");
	$this->slug = $slug;
	return $this;
}

/**
* Get videoURL - Video URL
* @return string
*/
public function getVideoURL () {
	$preValue = $this->preGetValue("videoURL"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->videoURL;
	if(\Pimcore\Model\DataObject::doGetInheritedValues() && $this->getClass()->getFieldDefinition("videoURL")->isEmpty($data)) {
		return $this->getValueFromParent("videoURL");
	}
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set videoURL - Video URL
* @param string $videoURL
* @return \Pimcore\Model\DataObject\BzVideo
*/
public function setVideoURL ($videoURL) {
	$fd = $this->getClass()->getFieldDefinition("videoURL");
	$this->videoURL = $videoURL;
	return $this;
}

/**
* Get videoID - videoID
* @return string
*/
public function getVideoID () {
	$preValue = $this->preGetValue("videoID"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->videoID;
	if(\Pimcore\Model\DataObject::doGetInheritedValues() && $this->getClass()->getFieldDefinition("videoID")->isEmpty($data)) {
		return $this->getValueFromParent("videoID");
	}
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set videoID - videoID
* @param string $videoID
* @return \Pimcore\Model\DataObject\BzVideo
*/
public function setVideoID ($videoID) {
	$fd = $this->getClass()->getFieldDefinition("videoID");
	$this->videoID = $videoID;
	return $this;
}

/**
* Get localVideoPath - Local Video Path
* @return \Pimcore\Model\Asset\Video
*/
public function getLocalVideoPath () {
	$preValue = $this->preGetValue("localVideoPath"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->getClass()->getFieldDefinition("localVideoPath")->preGetData($this);
	if(\Pimcore\Model\DataObject::doGetInheritedValues() && $this->getClass()->getFieldDefinition("localVideoPath")->isEmpty($data)) {
		return $this->getValueFromParent("localVideoPath");
	}
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set localVideoPath - Local Video Path
* @param \Pimcore\Model\Asset\Video $localVideoPath
* @return \Pimcore\Model\DataObject\BzVideo
*/
public function setLocalVideoPath ($localVideoPath) {
	$fd = $this->getClass()->getFieldDefinition("localVideoPath");
	$currentData = $this->getLocalVideoPath();
	$isEqual = $fd->isEqual($currentData, $localVideoPath);
	if (!$isEqual) {
		$this->markFieldDirty("localVideoPath", true);
	}
	$this->localVideoPath = $fd->preSetData($this, $localVideoPath);
	return $this;
}

/**
* Get video_blog - Video Blog
* @return string
*/
public function getVideo_blog () {
	$preValue = $this->preGetValue("video_blog"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->getClass()->getFieldDefinition("video_blog")->preGetData($this);
	if(\Pimcore\Model\DataObject::doGetInheritedValues() && $this->getClass()->getFieldDefinition("video_blog")->isEmpty($data)) {
		return $this->getValueFromParent("video_blog");
	}
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set video_blog - Video Blog
* @param string $video_blog
* @return \Pimcore\Model\DataObject\BzVideo
*/
public function setVideo_blog ($video_blog) {
	$fd = $this->getClass()->getFieldDefinition("video_blog");
	$this->video_blog = $video_blog;
	return $this;
}

/**
* Get video_transcript - video_transcript
* @return string
*/
public function getVideo_transcript () {
	$preValue = $this->preGetValue("video_transcript"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->getClass()->getFieldDefinition("video_transcript")->preGetData($this);
	if(\Pimcore\Model\DataObject::doGetInheritedValues() && $this->getClass()->getFieldDefinition("video_transcript")->isEmpty($data)) {
		return $this->getValueFromParent("video_transcript");
	}
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set video_transcript - video_transcript
* @param string $video_transcript
* @return \Pimcore\Model\DataObject\BzVideo
*/
public function setVideo_transcript ($video_transcript) {
	$fd = $this->getClass()->getFieldDefinition("video_transcript");
	$this->video_transcript = $video_transcript;
	return $this;
}

/**
* Get isLocal - Is Local Hosted Video?
* @return boolean
*/
public function getIsLocal () {
	$preValue = $this->preGetValue("isLocal"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->isLocal;
	if(\Pimcore\Model\DataObject::doGetInheritedValues() && $this->getClass()->getFieldDefinition("isLocal")->isEmpty($data)) {
		return $this->getValueFromParent("isLocal");
	}
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set isLocal - Is Local Hosted Video?
* @param boolean $isLocal
* @return \Pimcore\Model\DataObject\BzVideo
*/
public function setIsLocal ($isLocal) {
	$fd = $this->getClass()->getFieldDefinition("isLocal");
	$this->isLocal = $isLocal;
	return $this;
}

/**
* Get market_area_href - market_area_href
* @return \Pimcore\Model\DataObject\BzMarketArea
*/
public function getMarket_area_href () {
	$preValue = $this->preGetValue("market_area_href"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->getClass()->getFieldDefinition("market_area_href")->preGetData($this);
	if(\Pimcore\Model\DataObject::doGetInheritedValues() && $this->getClass()->getFieldDefinition("market_area_href")->isEmpty($data)) {
		return $this->getValueFromParent("market_area_href");
	}
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set market_area_href - market_area_href
* @param \Pimcore\Model\DataObject\BzMarketArea $market_area_href
* @return \Pimcore\Model\DataObject\BzVideo
*/
public function setMarket_area_href ($market_area_href) {
	$fd = $this->getClass()->getFieldDefinition("market_area_href");
	$currentData = $this->getMarket_area_href();
	$isEqual = $fd->isEqual($currentData, $market_area_href);
	if (!$isEqual) {
		$this->markFieldDirty("market_area_href", true);
	}
	$this->market_area_href = $fd->preSetData($this, $market_area_href);
	return $this;
}

/**
* Get domain_href - Domain Href
* @return \Pimcore\Model\DataObject\BzDomain
*/
public function getDomain_href () {
	$preValue = $this->preGetValue("domain_href"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->getClass()->getFieldDefinition("domain_href")->preGetData($this);
	if(\Pimcore\Model\DataObject::doGetInheritedValues() && $this->getClass()->getFieldDefinition("domain_href")->isEmpty($data)) {
		return $this->getValueFromParent("domain_href");
	}
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set domain_href - Domain Href
* @param \Pimcore\Model\DataObject\BzDomain $domain_href
* @return \Pimcore\Model\DataObject\BzVideo
*/
public function setDomain_href ($domain_href) {
	$fd = $this->getClass()->getFieldDefinition("domain_href");
	$currentData = $this->getDomain_href();
	$isEqual = $fd->isEqual($currentData, $domain_href);
	if (!$isEqual) {
		$this->markFieldDirty("domain_href", true);
	}
	$this->domain_href = $fd->preSetData($this, $domain_href);
	return $this;
}

/**
* Get merchant_href - Merchant Href
* @return \Pimcore\Model\DataObject\BzMerchant
*/
public function getMerchant_href () {
	$preValue = $this->preGetValue("merchant_href"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->getClass()->getFieldDefinition("merchant_href")->preGetData($this);
	if(\Pimcore\Model\DataObject::doGetInheritedValues() && $this->getClass()->getFieldDefinition("merchant_href")->isEmpty($data)) {
		return $this->getValueFromParent("merchant_href");
	}
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set merchant_href - Merchant Href
* @param \Pimcore\Model\DataObject\BzMerchant $merchant_href
* @return \Pimcore\Model\DataObject\BzVideo
*/
public function setMerchant_href ($merchant_href) {
	$fd = $this->getClass()->getFieldDefinition("merchant_href");
	$currentData = $this->getMerchant_href();
	$isEqual = $fd->isEqual($currentData, $merchant_href);
	if (!$isEqual) {
		$this->markFieldDirty("merchant_href", true);
	}
	$this->merchant_href = $fd->preSetData($this, $merchant_href);
	return $this;
}

/**
* Get title - title
* @return string
*/
public function getTitle () {
	$preValue = $this->preGetValue("title"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->title;
	if(\Pimcore\Model\DataObject::doGetInheritedValues() && $this->getClass()->getFieldDefinition("title")->isEmpty($data)) {
		return $this->getValueFromParent("title");
	}
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set title - title
* @param string $title
* @return \Pimcore\Model\DataObject\BzVideo
*/
public function setTitle ($title) {
	$fd = $this->getClass()->getFieldDefinition("title");
	$this->title = $title;
	return $this;
}

/**
* Get type_display_name - video
* @return string
*/
public function getType_display_name () {
	$preValue = $this->preGetValue("type_display_name"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->type_display_name;
	if(\Pimcore\Model\DataObject::doGetInheritedValues() && $this->getClass()->getFieldDefinition("type_display_name")->isEmpty($data)) {
		return $this->getValueFromParent("type_display_name");
	}
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set type_display_name - video
* @param string $type_display_name
* @return \Pimcore\Model\DataObject\BzVideo
*/
public function setType_display_name ($type_display_name) {
	$fd = $this->getClass()->getFieldDefinition("type_display_name");
	$this->type_display_name = $type_display_name;
	return $this;
}

protected static $_relationFields = array (
  'listing_inherits_from' => 
  array (
    'type' => 'manyToOneRelation',
  ),
  'deal_inherits_from' => 
  array (
    'type' => 'manyToOneRelation',
  ),
  'event_inherits_from' => 
  array (
    'type' => 'manyToOneRelation',
  ),
  'audio_inherits_from' => 
  array (
    'type' => 'manyToOneRelation',
  ),
  'video_inherits_from' => 
  array (
    'type' => 'manyToOneRelation',
  ),
  'article_inherits_from' => 
  array (
    'type' => 'manyToOneRelation',
  ),
  'news_inherits_from' => 
  array (
    'type' => 'manyToOneRelation',
  ),
  'inherited_from' => 
  array (
    'type' => 'manyToOneRelation',
  ),
  'localVideoPath' => 
  array (
    'type' => 'manyToOneRelation',
  ),
  'market_area_href' => 
  array (
    'type' => 'manyToOneRelation',
  ),
  'domain_href' => 
  array (
    'type' => 'manyToOneRelation',
  ),
  'merchant_href' => 
  array (
    'type' => 'manyToOneRelation',
  ),
);

protected $lazyLoadedFields = array (
);

}

