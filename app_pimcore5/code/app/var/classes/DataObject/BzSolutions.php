<?php 

/** 
* Generated at: 2019-08-26T12:34:29+02:00
* Inheritance: yes
* Variants: no
* Changed by: root (2)
* IP: ::1


Fields Summary: 
- name [input]
- domain_href [manyToOneRelation]
- market_href [manyToOneRelation]
- solution_name [input]
- display_name [input]
- object_type [input]
- owner [input]
- giturl [input]
- folder [input]
- asset_folder [input]
- gitlab_access_token [input]
- bind_port [input]
- qout_object_update [input]
- qout_document_update [input]
- website_config_file_location [input]
- website_template_db_giturl [input]
- website_template_name [input]
- website_source_url [input]
- website [manyToOneRelation]
- volumes [manyToManyRelation]
- shared_volumes [manyToManyRelation]
- cron_jobs [manyToManyRelation]
*/ 

namespace Pimcore\Model\DataObject;



/**
* @method static \Pimcore\Model\DataObject\BzSolutions\Listing getByName ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\BzSolutions\Listing getByDomain_href ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\BzSolutions\Listing getByMarket_href ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\BzSolutions\Listing getBySolution_name ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\BzSolutions\Listing getByDisplay_name ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\BzSolutions\Listing getByObject_type ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\BzSolutions\Listing getByOwner ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\BzSolutions\Listing getByGiturl ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\BzSolutions\Listing getByFolder ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\BzSolutions\Listing getByAsset_folder ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\BzSolutions\Listing getByGitlab_access_token ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\BzSolutions\Listing getByBind_port ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\BzSolutions\Listing getByQout_object_update ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\BzSolutions\Listing getByQout_document_update ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\BzSolutions\Listing getByWebsite_config_file_location ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\BzSolutions\Listing getByWebsite_template_db_giturl ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\BzSolutions\Listing getByWebsite_template_name ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\BzSolutions\Listing getByWebsite_source_url ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\BzSolutions\Listing getByWebsite ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\BzSolutions\Listing getByVolumes ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\BzSolutions\Listing getByShared_volumes ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\BzSolutions\Listing getByCron_jobs ($value, $limit = 0) 
*/

class BzSolutions extends Concrete implements \Pimcore\Model\DataObject\DirtyIndicatorInterface {



use \Pimcore\Model\DataObject\Traits\DirtyIndicatorTrait;

protected $o_classId = "34";
protected $o_className = "BzSolutions";
protected $name;
protected $domain_href;
protected $market_href;
protected $solution_name;
protected $display_name;
protected $object_type;
protected $owner;
protected $giturl;
protected $folder;
protected $asset_folder;
protected $gitlab_access_token;
protected $bind_port;
protected $qout_object_update;
protected $qout_document_update;
protected $website_config_file_location;
protected $website_template_db_giturl;
protected $website_template_name;
protected $website_source_url;
protected $website;
protected $volumes;
protected $shared_volumes;
protected $cron_jobs;


/**
* @param array $values
* @return \Pimcore\Model\DataObject\BzSolutions
*/
public static function create($values = array()) {
	$object = new static();
	$object->setValues($values);
	return $object;
}

/**
* Get name - name
* @return string
*/
public function getName () {
	$preValue = $this->preGetValue("name"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->name;
	if(\Pimcore\Model\DataObject::doGetInheritedValues() && $this->getClass()->getFieldDefinition("name")->isEmpty($data)) {
		return $this->getValueFromParent("name");
	}
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set name - name
* @param string $name
* @return \Pimcore\Model\DataObject\BzSolutions
*/
public function setName ($name) {
	$fd = $this->getClass()->getFieldDefinition("name");
	$this->name = $name;
	return $this;
}

/**
* Get domain_href - domain_href
* @return \Pimcore\Model\DataObject\BzDomain
*/
public function getDomain_href () {
	$preValue = $this->preGetValue("domain_href"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->getClass()->getFieldDefinition("domain_href")->preGetData($this);
	if(\Pimcore\Model\DataObject::doGetInheritedValues() && $this->getClass()->getFieldDefinition("domain_href")->isEmpty($data)) {
		return $this->getValueFromParent("domain_href");
	}
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set domain_href - domain_href
* @param \Pimcore\Model\DataObject\BzDomain $domain_href
* @return \Pimcore\Model\DataObject\BzSolutions
*/
public function setDomain_href ($domain_href) {
	$fd = $this->getClass()->getFieldDefinition("domain_href");
	$currentData = $this->getDomain_href();
	$isEqual = $fd->isEqual($currentData, $domain_href);
	if (!$isEqual) {
		$this->markFieldDirty("domain_href", true);
	}
	$this->domain_href = $fd->preSetData($this, $domain_href);
	return $this;
}

/**
* Get market_href - market_href
* @return \Pimcore\Model\DataObject\BzMarketArea
*/
public function getMarket_href () {
	$preValue = $this->preGetValue("market_href"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->getClass()->getFieldDefinition("market_href")->preGetData($this);
	if(\Pimcore\Model\DataObject::doGetInheritedValues() && $this->getClass()->getFieldDefinition("market_href")->isEmpty($data)) {
		return $this->getValueFromParent("market_href");
	}
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set market_href - market_href
* @param \Pimcore\Model\DataObject\BzMarketArea $market_href
* @return \Pimcore\Model\DataObject\BzSolutions
*/
public function setMarket_href ($market_href) {
	$fd = $this->getClass()->getFieldDefinition("market_href");
	$currentData = $this->getMarket_href();
	$isEqual = $fd->isEqual($currentData, $market_href);
	if (!$isEqual) {
		$this->markFieldDirty("market_href", true);
	}
	$this->market_href = $fd->preSetData($this, $market_href);
	return $this;
}

/**
* Get solution_name - solution_name
* @return string
*/
public function getSolution_name () {
	$preValue = $this->preGetValue("solution_name"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->solution_name;
	if(\Pimcore\Model\DataObject::doGetInheritedValues() && $this->getClass()->getFieldDefinition("solution_name")->isEmpty($data)) {
		return $this->getValueFromParent("solution_name");
	}
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set solution_name - solution_name
* @param string $solution_name
* @return \Pimcore\Model\DataObject\BzSolutions
*/
public function setSolution_name ($solution_name) {
	$fd = $this->getClass()->getFieldDefinition("solution_name");
	$this->solution_name = $solution_name;
	return $this;
}

/**
* Get display_name - Display Name
* @return string
*/
public function getDisplay_name () {
	$preValue = $this->preGetValue("display_name"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->display_name;
	if(\Pimcore\Model\DataObject::doGetInheritedValues() && $this->getClass()->getFieldDefinition("display_name")->isEmpty($data)) {
		return $this->getValueFromParent("display_name");
	}
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set display_name - Display Name
* @param string $display_name
* @return \Pimcore\Model\DataObject\BzSolutions
*/
public function setDisplay_name ($display_name) {
	$fd = $this->getClass()->getFieldDefinition("display_name");
	$this->display_name = $display_name;
	return $this;
}

/**
* Get object_type - Object Type
* @return string
*/
public function getObject_type () {
	$preValue = $this->preGetValue("object_type"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->object_type;
	if(\Pimcore\Model\DataObject::doGetInheritedValues() && $this->getClass()->getFieldDefinition("object_type")->isEmpty($data)) {
		return $this->getValueFromParent("object_type");
	}
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set object_type - Object Type
* @param string $object_type
* @return \Pimcore\Model\DataObject\BzSolutions
*/
public function setObject_type ($object_type) {
	$fd = $this->getClass()->getFieldDefinition("object_type");
	$this->object_type = $object_type;
	return $this;
}

/**
* Get owner - owner
* @return string
*/
public function getOwner () {
	$preValue = $this->preGetValue("owner"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->owner;
	if(\Pimcore\Model\DataObject::doGetInheritedValues() && $this->getClass()->getFieldDefinition("owner")->isEmpty($data)) {
		return $this->getValueFromParent("owner");
	}
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set owner - owner
* @param string $owner
* @return \Pimcore\Model\DataObject\BzSolutions
*/
public function setOwner ($owner) {
	$fd = $this->getClass()->getFieldDefinition("owner");
	$this->owner = $owner;
	return $this;
}

/**
* Get giturl - giturl
* @return string
*/
public function getGiturl () {
	$preValue = $this->preGetValue("giturl"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->giturl;
	if(\Pimcore\Model\DataObject::doGetInheritedValues() && $this->getClass()->getFieldDefinition("giturl")->isEmpty($data)) {
		return $this->getValueFromParent("giturl");
	}
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set giturl - giturl
* @param string $giturl
* @return \Pimcore\Model\DataObject\BzSolutions
*/
public function setGiturl ($giturl) {
	$fd = $this->getClass()->getFieldDefinition("giturl");
	$this->giturl = $giturl;
	return $this;
}

/**
* Get folder - folder
* @return string
*/
public function getFolder () {
	$preValue = $this->preGetValue("folder"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->folder;
	if(\Pimcore\Model\DataObject::doGetInheritedValues() && $this->getClass()->getFieldDefinition("folder")->isEmpty($data)) {
		return $this->getValueFromParent("folder");
	}
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set folder - folder
* @param string $folder
* @return \Pimcore\Model\DataObject\BzSolutions
*/
public function setFolder ($folder) {
	$fd = $this->getClass()->getFieldDefinition("folder");
	$this->folder = $folder;
	return $this;
}

/**
* Get asset_folder - asset_folder
* @return string
*/
public function getAsset_folder () {
	$preValue = $this->preGetValue("asset_folder"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->asset_folder;
	if(\Pimcore\Model\DataObject::doGetInheritedValues() && $this->getClass()->getFieldDefinition("asset_folder")->isEmpty($data)) {
		return $this->getValueFromParent("asset_folder");
	}
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set asset_folder - asset_folder
* @param string $asset_folder
* @return \Pimcore\Model\DataObject\BzSolutions
*/
public function setAsset_folder ($asset_folder) {
	$fd = $this->getClass()->getFieldDefinition("asset_folder");
	$this->asset_folder = $asset_folder;
	return $this;
}

/**
* Get gitlab_access_token - gitlab_access_token
* @return string
*/
public function getGitlab_access_token () {
	$preValue = $this->preGetValue("gitlab_access_token"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->gitlab_access_token;
	if(\Pimcore\Model\DataObject::doGetInheritedValues() && $this->getClass()->getFieldDefinition("gitlab_access_token")->isEmpty($data)) {
		return $this->getValueFromParent("gitlab_access_token");
	}
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set gitlab_access_token - gitlab_access_token
* @param string $gitlab_access_token
* @return \Pimcore\Model\DataObject\BzSolutions
*/
public function setGitlab_access_token ($gitlab_access_token) {
	$fd = $this->getClass()->getFieldDefinition("gitlab_access_token");
	$this->gitlab_access_token = $gitlab_access_token;
	return $this;
}

/**
* Get bind_port - bind_port
* @return string
*/
public function getBind_port () {
	$preValue = $this->preGetValue("bind_port"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->bind_port;
	if(\Pimcore\Model\DataObject::doGetInheritedValues() && $this->getClass()->getFieldDefinition("bind_port")->isEmpty($data)) {
		return $this->getValueFromParent("bind_port");
	}
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set bind_port - bind_port
* @param string $bind_port
* @return \Pimcore\Model\DataObject\BzSolutions
*/
public function setBind_port ($bind_port) {
	$fd = $this->getClass()->getFieldDefinition("bind_port");
	$this->bind_port = $bind_port;
	return $this;
}

/**
* Get qout_object_update - qout_object_update
* @return string
*/
public function getQout_object_update () {
	$preValue = $this->preGetValue("qout_object_update"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->qout_object_update;
	if(\Pimcore\Model\DataObject::doGetInheritedValues() && $this->getClass()->getFieldDefinition("qout_object_update")->isEmpty($data)) {
		return $this->getValueFromParent("qout_object_update");
	}
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set qout_object_update - qout_object_update
* @param string $qout_object_update
* @return \Pimcore\Model\DataObject\BzSolutions
*/
public function setQout_object_update ($qout_object_update) {
	$fd = $this->getClass()->getFieldDefinition("qout_object_update");
	$this->qout_object_update = $qout_object_update;
	return $this;
}

/**
* Get qout_document_update - qout_document_update
* @return string
*/
public function getQout_document_update () {
	$preValue = $this->preGetValue("qout_document_update"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->qout_document_update;
	if(\Pimcore\Model\DataObject::doGetInheritedValues() && $this->getClass()->getFieldDefinition("qout_document_update")->isEmpty($data)) {
		return $this->getValueFromParent("qout_document_update");
	}
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set qout_document_update - qout_document_update
* @param string $qout_document_update
* @return \Pimcore\Model\DataObject\BzSolutions
*/
public function setQout_document_update ($qout_document_update) {
	$fd = $this->getClass()->getFieldDefinition("qout_document_update");
	$this->qout_document_update = $qout_document_update;
	return $this;
}

/**
* Get website_config_file_location - website_config_file_location
* @return string
*/
public function getWebsite_config_file_location () {
	$preValue = $this->preGetValue("website_config_file_location"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->website_config_file_location;
	if(\Pimcore\Model\DataObject::doGetInheritedValues() && $this->getClass()->getFieldDefinition("website_config_file_location")->isEmpty($data)) {
		return $this->getValueFromParent("website_config_file_location");
	}
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set website_config_file_location - website_config_file_location
* @param string $website_config_file_location
* @return \Pimcore\Model\DataObject\BzSolutions
*/
public function setWebsite_config_file_location ($website_config_file_location) {
	$fd = $this->getClass()->getFieldDefinition("website_config_file_location");
	$this->website_config_file_location = $website_config_file_location;
	return $this;
}

/**
* Get website_template_db_giturl - website_template_db_giturl
* @return string
*/
public function getWebsite_template_db_giturl () {
	$preValue = $this->preGetValue("website_template_db_giturl"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->website_template_db_giturl;
	if(\Pimcore\Model\DataObject::doGetInheritedValues() && $this->getClass()->getFieldDefinition("website_template_db_giturl")->isEmpty($data)) {
		return $this->getValueFromParent("website_template_db_giturl");
	}
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set website_template_db_giturl - website_template_db_giturl
* @param string $website_template_db_giturl
* @return \Pimcore\Model\DataObject\BzSolutions
*/
public function setWebsite_template_db_giturl ($website_template_db_giturl) {
	$fd = $this->getClass()->getFieldDefinition("website_template_db_giturl");
	$this->website_template_db_giturl = $website_template_db_giturl;
	return $this;
}

/**
* Get website_template_name - website_template_name
* @return string
*/
public function getWebsite_template_name () {
	$preValue = $this->preGetValue("website_template_name"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->website_template_name;
	if(\Pimcore\Model\DataObject::doGetInheritedValues() && $this->getClass()->getFieldDefinition("website_template_name")->isEmpty($data)) {
		return $this->getValueFromParent("website_template_name");
	}
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set website_template_name - website_template_name
* @param string $website_template_name
* @return \Pimcore\Model\DataObject\BzSolutions
*/
public function setWebsite_template_name ($website_template_name) {
	$fd = $this->getClass()->getFieldDefinition("website_template_name");
	$this->website_template_name = $website_template_name;
	return $this;
}

/**
* Get website_source_url - website_source_url
* @return string
*/
public function getWebsite_source_url () {
	$preValue = $this->preGetValue("website_source_url"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->website_source_url;
	if(\Pimcore\Model\DataObject::doGetInheritedValues() && $this->getClass()->getFieldDefinition("website_source_url")->isEmpty($data)) {
		return $this->getValueFromParent("website_source_url");
	}
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set website_source_url - website_source_url
* @param string $website_source_url
* @return \Pimcore\Model\DataObject\BzSolutions
*/
public function setWebsite_source_url ($website_source_url) {
	$fd = $this->getClass()->getFieldDefinition("website_source_url");
	$this->website_source_url = $website_source_url;
	return $this;
}

/**
* Get website - website
* @return \Pimcore\Model\DataObject\BzWebsite
*/
public function getWebsite () {
	$preValue = $this->preGetValue("website"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->getClass()->getFieldDefinition("website")->preGetData($this);
	if(\Pimcore\Model\DataObject::doGetInheritedValues() && $this->getClass()->getFieldDefinition("website")->isEmpty($data)) {
		return $this->getValueFromParent("website");
	}
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set website - website
* @param \Pimcore\Model\DataObject\BzWebsite $website
* @return \Pimcore\Model\DataObject\BzSolutions
*/
public function setWebsite ($website) {
	$fd = $this->getClass()->getFieldDefinition("website");
	$currentData = $this->getWebsite();
	$isEqual = $fd->isEqual($currentData, $website);
	if (!$isEqual) {
		$this->markFieldDirty("website", true);
	}
	$this->website = $fd->preSetData($this, $website);
	return $this;
}

/**
* Get volumes - volumes
* @return \Pimcore\Model\DataObject\BzSystemComponentVolumes[]
*/
public function getVolumes () {
	$preValue = $this->preGetValue("volumes"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->getClass()->getFieldDefinition("volumes")->preGetData($this);
	if(\Pimcore\Model\DataObject::doGetInheritedValues() && $this->getClass()->getFieldDefinition("volumes")->isEmpty($data)) {
		return $this->getValueFromParent("volumes");
	}
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set volumes - volumes
* @param \Pimcore\Model\DataObject\BzSystemComponentVolumes[] $volumes
* @return \Pimcore\Model\DataObject\BzSolutions
*/
public function setVolumes ($volumes) {
	$fd = $this->getClass()->getFieldDefinition("volumes");
	$currentData = $this->getVolumes();
	$isEqual = $fd->isEqual($currentData, $volumes);
	if (!$isEqual) {
		$this->markFieldDirty("volumes", true);
	}
	$this->volumes = $fd->preSetData($this, $volumes);
	return $this;
}

/**
* Get shared_volumes - shared_volumes
* @return \Pimcore\Model\DataObject\BzSystemComponentSharedVolume[]
*/
public function getShared_volumes () {
	$preValue = $this->preGetValue("shared_volumes"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->getClass()->getFieldDefinition("shared_volumes")->preGetData($this);
	if(\Pimcore\Model\DataObject::doGetInheritedValues() && $this->getClass()->getFieldDefinition("shared_volumes")->isEmpty($data)) {
		return $this->getValueFromParent("shared_volumes");
	}
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set shared_volumes - shared_volumes
* @param \Pimcore\Model\DataObject\BzSystemComponentSharedVolume[] $shared_volumes
* @return \Pimcore\Model\DataObject\BzSolutions
*/
public function setShared_volumes ($shared_volumes) {
	$fd = $this->getClass()->getFieldDefinition("shared_volumes");
	$currentData = $this->getShared_volumes();
	$isEqual = $fd->isEqual($currentData, $shared_volumes);
	if (!$isEqual) {
		$this->markFieldDirty("shared_volumes", true);
	}
	$this->shared_volumes = $fd->preSetData($this, $shared_volumes);
	return $this;
}

/**
* Get cron_jobs - cron_jobs
* @return \Pimcore\Model\DataObject\BzSystemComponentCronJobs[]
*/
public function getCron_jobs () {
	$preValue = $this->preGetValue("cron_jobs"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->getClass()->getFieldDefinition("cron_jobs")->preGetData($this);
	if(\Pimcore\Model\DataObject::doGetInheritedValues() && $this->getClass()->getFieldDefinition("cron_jobs")->isEmpty($data)) {
		return $this->getValueFromParent("cron_jobs");
	}
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set cron_jobs - cron_jobs
* @param \Pimcore\Model\DataObject\BzSystemComponentCronJobs[] $cron_jobs
* @return \Pimcore\Model\DataObject\BzSolutions
*/
public function setCron_jobs ($cron_jobs) {
	$fd = $this->getClass()->getFieldDefinition("cron_jobs");
	$currentData = $this->getCron_jobs();
	$isEqual = $fd->isEqual($currentData, $cron_jobs);
	if (!$isEqual) {
		$this->markFieldDirty("cron_jobs", true);
	}
	$this->cron_jobs = $fd->preSetData($this, $cron_jobs);
	return $this;
}

protected static $_relationFields = array (
  'domain_href' => 
  array (
    'type' => 'manyToOneRelation',
  ),
  'market_href' => 
  array (
    'type' => 'manyToOneRelation',
  ),
  'website' => 
  array (
    'type' => 'manyToOneRelation',
  ),
  'volumes' => 
  array (
    'type' => 'manyToManyRelation',
  ),
  'shared_volumes' => 
  array (
    'type' => 'manyToManyRelation',
  ),
  'cron_jobs' => 
  array (
    'type' => 'manyToManyRelation',
  ),
);

protected $lazyLoadedFields = array (
  0 => 'website',
  1 => 'volumes',
  2 => 'shared_volumes',
  3 => 'cron_jobs',
);

}

