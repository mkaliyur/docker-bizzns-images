<?php 

/** 
* Generated at: 2019-08-26T14:34:28+02:00
* Inheritance: no
* Variants: no
* Changed by: root (2)
* IP: ::1


Fields Summary: 
- name [input]
- object_type [input]
- solution_name [input]
- pages_href [manyToManyRelation]
*/ 

namespace Pimcore\Model\DataObject;



/**
* @method static \Pimcore\Model\DataObject\BzWebsite\Listing getByName ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\BzWebsite\Listing getByObject_type ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\BzWebsite\Listing getBySolution_name ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\BzWebsite\Listing getByPages_href ($value, $limit = 0) 
*/

class BzWebsite extends Concrete implements \Pimcore\Model\DataObject\DirtyIndicatorInterface {



use \Pimcore\Model\DataObject\Traits\DirtyIndicatorTrait;

protected $o_classId = "51";
protected $o_className = "BzWebsite";
protected $name;
protected $object_type;
protected $solution_name;
protected $pages_href;


/**
* @param array $values
* @return \Pimcore\Model\DataObject\BzWebsite
*/
public static function create($values = array()) {
	$object = new static();
	$object->setValues($values);
	return $object;
}

/**
* Get name - name
* @return string
*/
public function getName () {
	$preValue = $this->preGetValue("name"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->name;
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set name - name
* @param string $name
* @return \Pimcore\Model\DataObject\BzWebsite
*/
public function setName ($name) {
	$fd = $this->getClass()->getFieldDefinition("name");
	$this->name = $name;
	return $this;
}

/**
* Get object_type - object_type
* @return string
*/
public function getObject_type () {
	$preValue = $this->preGetValue("object_type"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->object_type;
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set object_type - object_type
* @param string $object_type
* @return \Pimcore\Model\DataObject\BzWebsite
*/
public function setObject_type ($object_type) {
	$fd = $this->getClass()->getFieldDefinition("object_type");
	$this->object_type = $object_type;
	return $this;
}

/**
* Get solution_name - solution_name
* @return string
*/
public function getSolution_name () {
	$preValue = $this->preGetValue("solution_name"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->solution_name;
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set solution_name - solution_name
* @param string $solution_name
* @return \Pimcore\Model\DataObject\BzWebsite
*/
public function setSolution_name ($solution_name) {
	$fd = $this->getClass()->getFieldDefinition("solution_name");
	$this->solution_name = $solution_name;
	return $this;
}

/**
* Get pages_href - pages_href
* @return \Pimcore\Model\DataObject\BzPage[]
*/
public function getPages_href () {
	$preValue = $this->preGetValue("pages_href"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->getClass()->getFieldDefinition("pages_href")->preGetData($this);
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set pages_href - pages_href
* @param \Pimcore\Model\DataObject\BzPage[] $pages_href
* @return \Pimcore\Model\DataObject\BzWebsite
*/
public function setPages_href ($pages_href) {
	$fd = $this->getClass()->getFieldDefinition("pages_href");
	$currentData = $this->getPages_href();
	$isEqual = $fd->isEqual($currentData, $pages_href);
	if (!$isEqual) {
		$this->markFieldDirty("pages_href", true);
	}
	$this->pages_href = $fd->preSetData($this, $pages_href);
	return $this;
}

protected static $_relationFields = array (
  'pages_href' => 
  array (
    'type' => 'manyToManyRelation',
  ),
);

protected $lazyLoadedFields = array (
);

}

