<?php 

/** 
* Generated at: 2019-09-03T10:25:57+02:00
* Inheritance: no
* Variants: no
* Changed by: root (2)
* IP: ::1


Fields Summary: 
- Name [input]
- solution_name [input]
- object_type [input]
- desc [textarea]
- storageValue [input]
- belongsTo [manyToOneRelation]
- master_listing [manyToOneRelation]
- master_deal [manyToOneRelation]
- master_event [manyToOneRelation]
- master_audio [manyToOneRelation]
- master_video [manyToOneRelation]
- master_article [manyToOneRelation]
- master_news [manyToOneRelation]
*/ 

namespace Pimcore\Model\DataObject;



/**
* @method static \Pimcore\Model\DataObject\BzDomain\Listing getByName ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\BzDomain\Listing getBySolution_name ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\BzDomain\Listing getByObject_type ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\BzDomain\Listing getByDesc ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\BzDomain\Listing getByStorageValue ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\BzDomain\Listing getByBelongsTo ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\BzDomain\Listing getByMaster_listing ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\BzDomain\Listing getByMaster_deal ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\BzDomain\Listing getByMaster_event ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\BzDomain\Listing getByMaster_audio ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\BzDomain\Listing getByMaster_video ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\BzDomain\Listing getByMaster_article ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\BzDomain\Listing getByMaster_news ($value, $limit = 0) 
*/

class BzDomain extends Concrete implements \Pimcore\Model\DataObject\DirtyIndicatorInterface {



use \Pimcore\Model\DataObject\Traits\DirtyIndicatorTrait;

protected $o_classId = "9";
protected $o_className = "BzDomain";
protected $Name;
protected $solution_name;
protected $object_type;
protected $desc;
protected $storageValue;
protected $belongsTo;
protected $master_listing;
protected $master_deal;
protected $master_event;
protected $master_audio;
protected $master_video;
protected $master_article;
protected $master_news;


/**
* @param array $values
* @return \Pimcore\Model\DataObject\BzDomain
*/
public static function create($values = array()) {
	$object = new static();
	$object->setValues($values);
	return $object;
}

/**
* Get Name - Name
* @return string
*/
public function getName () {
	$preValue = $this->preGetValue("Name"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->Name;
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set Name - Name
* @param string $Name
* @return \Pimcore\Model\DataObject\BzDomain
*/
public function setName ($Name) {
	$fd = $this->getClass()->getFieldDefinition("Name");
	$this->Name = $Name;
	return $this;
}

/**
* Get solution_name - Solution Name
* @return string
*/
public function getSolution_name () {
	$preValue = $this->preGetValue("solution_name"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->solution_name;
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set solution_name - Solution Name
* @param string $solution_name
* @return \Pimcore\Model\DataObject\BzDomain
*/
public function setSolution_name ($solution_name) {
	$fd = $this->getClass()->getFieldDefinition("solution_name");
	$this->solution_name = $solution_name;
	return $this;
}

/**
* Get object_type - Object Type
* @return string
*/
public function getObject_type () {
	$preValue = $this->preGetValue("object_type"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->object_type;
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set object_type - Object Type
* @param string $object_type
* @return \Pimcore\Model\DataObject\BzDomain
*/
public function setObject_type ($object_type) {
	$fd = $this->getClass()->getFieldDefinition("object_type");
	$this->object_type = $object_type;
	return $this;
}

/**
* Get desc - desc
* @return string
*/
public function getDesc () {
	$preValue = $this->preGetValue("desc"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->desc;
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set desc - desc
* @param string $desc
* @return \Pimcore\Model\DataObject\BzDomain
*/
public function setDesc ($desc) {
	$fd = $this->getClass()->getFieldDefinition("desc");
	$this->desc = $desc;
	return $this;
}

/**
* Get storageValue - Storage Value
* @return string
*/
public function getStorageValue () {
	$preValue = $this->preGetValue("storageValue"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->storageValue;
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set storageValue - Storage Value
* @param string $storageValue
* @return \Pimcore\Model\DataObject\BzDomain
*/
public function setStorageValue ($storageValue) {
	$fd = $this->getClass()->getFieldDefinition("storageValue");
	$this->storageValue = $storageValue;
	return $this;
}

/**
* Get belongsTo - Belongs To
* @return \Pimcore\Model\DataObject\BzMarketArea
*/
public function getBelongsTo () {
	$preValue = $this->preGetValue("belongsTo"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->getClass()->getFieldDefinition("belongsTo")->preGetData($this);
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set belongsTo - Belongs To
* @param \Pimcore\Model\DataObject\BzMarketArea $belongsTo
* @return \Pimcore\Model\DataObject\BzDomain
*/
public function setBelongsTo ($belongsTo) {
	$fd = $this->getClass()->getFieldDefinition("belongsTo");
	$currentData = $this->getBelongsTo();
	$isEqual = $fd->isEqual($currentData, $belongsTo);
	if (!$isEqual) {
		$this->markFieldDirty("belongsTo", true);
	}
	$this->belongsTo = $fd->preSetData($this, $belongsTo);
	return $this;
}

/**
* Get master_listing - master_listing
* @return \Pimcore\Model\DataObject\BzListing2
*/
public function getMaster_listing () {
	$preValue = $this->preGetValue("master_listing"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->getClass()->getFieldDefinition("master_listing")->preGetData($this);
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set master_listing - master_listing
* @param \Pimcore\Model\DataObject\BzListing2 $master_listing
* @return \Pimcore\Model\DataObject\BzDomain
*/
public function setMaster_listing ($master_listing) {
	$fd = $this->getClass()->getFieldDefinition("master_listing");
	$currentData = $this->getMaster_listing();
	$isEqual = $fd->isEqual($currentData, $master_listing);
	if (!$isEqual) {
		$this->markFieldDirty("master_listing", true);
	}
	$this->master_listing = $fd->preSetData($this, $master_listing);
	return $this;
}

/**
* Get master_deal - master_deal
* @return \Pimcore\Model\DataObject\BzDeals
*/
public function getMaster_deal () {
	$preValue = $this->preGetValue("master_deal"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->getClass()->getFieldDefinition("master_deal")->preGetData($this);
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set master_deal - master_deal
* @param \Pimcore\Model\DataObject\BzDeals $master_deal
* @return \Pimcore\Model\DataObject\BzDomain
*/
public function setMaster_deal ($master_deal) {
	$fd = $this->getClass()->getFieldDefinition("master_deal");
	$currentData = $this->getMaster_deal();
	$isEqual = $fd->isEqual($currentData, $master_deal);
	if (!$isEqual) {
		$this->markFieldDirty("master_deal", true);
	}
	$this->master_deal = $fd->preSetData($this, $master_deal);
	return $this;
}

/**
* Get master_event - master_event
* @return \Pimcore\Model\DataObject\BzEvents
*/
public function getMaster_event () {
	$preValue = $this->preGetValue("master_event"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->getClass()->getFieldDefinition("master_event")->preGetData($this);
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set master_event - master_event
* @param \Pimcore\Model\DataObject\BzEvents $master_event
* @return \Pimcore\Model\DataObject\BzDomain
*/
public function setMaster_event ($master_event) {
	$fd = $this->getClass()->getFieldDefinition("master_event");
	$currentData = $this->getMaster_event();
	$isEqual = $fd->isEqual($currentData, $master_event);
	if (!$isEqual) {
		$this->markFieldDirty("master_event", true);
	}
	$this->master_event = $fd->preSetData($this, $master_event);
	return $this;
}

/**
* Get master_audio - master_audio
* @return \Pimcore\Model\DataObject\BzAudio
*/
public function getMaster_audio () {
	$preValue = $this->preGetValue("master_audio"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->getClass()->getFieldDefinition("master_audio")->preGetData($this);
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set master_audio - master_audio
* @param \Pimcore\Model\DataObject\BzAudio $master_audio
* @return \Pimcore\Model\DataObject\BzDomain
*/
public function setMaster_audio ($master_audio) {
	$fd = $this->getClass()->getFieldDefinition("master_audio");
	$currentData = $this->getMaster_audio();
	$isEqual = $fd->isEqual($currentData, $master_audio);
	if (!$isEqual) {
		$this->markFieldDirty("master_audio", true);
	}
	$this->master_audio = $fd->preSetData($this, $master_audio);
	return $this;
}

/**
* Get master_video - master_video
* @return \Pimcore\Model\DataObject\BzVideo
*/
public function getMaster_video () {
	$preValue = $this->preGetValue("master_video"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->getClass()->getFieldDefinition("master_video")->preGetData($this);
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set master_video - master_video
* @param \Pimcore\Model\DataObject\BzVideo $master_video
* @return \Pimcore\Model\DataObject\BzDomain
*/
public function setMaster_video ($master_video) {
	$fd = $this->getClass()->getFieldDefinition("master_video");
	$currentData = $this->getMaster_video();
	$isEqual = $fd->isEqual($currentData, $master_video);
	if (!$isEqual) {
		$this->markFieldDirty("master_video", true);
	}
	$this->master_video = $fd->preSetData($this, $master_video);
	return $this;
}

/**
* Get master_article - master_article
* @return \Pimcore\Model\DataObject\BzBlogArticle
*/
public function getMaster_article () {
	$preValue = $this->preGetValue("master_article"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->getClass()->getFieldDefinition("master_article")->preGetData($this);
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set master_article - master_article
* @param \Pimcore\Model\DataObject\BzBlogArticle $master_article
* @return \Pimcore\Model\DataObject\BzDomain
*/
public function setMaster_article ($master_article) {
	$fd = $this->getClass()->getFieldDefinition("master_article");
	$currentData = $this->getMaster_article();
	$isEqual = $fd->isEqual($currentData, $master_article);
	if (!$isEqual) {
		$this->markFieldDirty("master_article", true);
	}
	$this->master_article = $fd->preSetData($this, $master_article);
	return $this;
}

/**
* Get master_news - master_news
* @return \Pimcore\Model\DataObject\News
*/
public function getMaster_news () {
	$preValue = $this->preGetValue("master_news"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->getClass()->getFieldDefinition("master_news")->preGetData($this);
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set master_news - master_news
* @param \Pimcore\Model\DataObject\News $master_news
* @return \Pimcore\Model\DataObject\BzDomain
*/
public function setMaster_news ($master_news) {
	$fd = $this->getClass()->getFieldDefinition("master_news");
	$currentData = $this->getMaster_news();
	$isEqual = $fd->isEqual($currentData, $master_news);
	if (!$isEqual) {
		$this->markFieldDirty("master_news", true);
	}
	$this->master_news = $fd->preSetData($this, $master_news);
	return $this;
}

protected static $_relationFields = array (
  'belongsTo' => 
  array (
    'type' => 'manyToOneRelation',
  ),
  'master_listing' => 
  array (
    'type' => 'manyToOneRelation',
  ),
  'master_deal' => 
  array (
    'type' => 'manyToOneRelation',
  ),
  'master_event' => 
  array (
    'type' => 'manyToOneRelation',
  ),
  'master_audio' => 
  array (
    'type' => 'manyToOneRelation',
  ),
  'master_video' => 
  array (
    'type' => 'manyToOneRelation',
  ),
  'master_article' => 
  array (
    'type' => 'manyToOneRelation',
  ),
  'master_news' => 
  array (
    'type' => 'manyToOneRelation',
  ),
);

protected $lazyLoadedFields = array (
  0 => 'master_listing',
  1 => 'master_deal',
  2 => 'master_event',
  3 => 'master_audio',
  4 => 'master_video',
  5 => 'master_article',
  6 => 'master_news',
);

}

