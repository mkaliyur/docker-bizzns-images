<?php 

namespace Pimcore\Model\DataObject\BzAudio;

use Pimcore\Model\DataObject;

/**
 * @method DataObject\BzAudio current()
 * @method DataObject\BzAudio[] load()
 */

class Listing extends DataObject\Listing\Concrete {

protected $classId = "24";
protected $className = "BzAudio";


}
