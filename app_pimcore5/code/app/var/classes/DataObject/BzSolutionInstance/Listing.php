<?php 

namespace Pimcore\Model\DataObject\BzSolutionInstance;

use Pimcore\Model\DataObject;

/**
 * @method DataObject\BzSolutionInstance current()
 * @method DataObject\BzSolutionInstance[] load()
 */

class Listing extends DataObject\Listing\Concrete {

protected $classId = "36";
protected $className = "BzSolutionInstance";


}
