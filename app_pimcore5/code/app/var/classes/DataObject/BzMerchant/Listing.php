<?php 

namespace Pimcore\Model\DataObject\BzMerchant;

use Pimcore\Model\DataObject;

/**
 * @method DataObject\BzMerchant current()
 * @method DataObject\BzMerchant[] load()
 */

class Listing extends DataObject\Listing\Concrete {

protected $classId = "26";
protected $className = "BzMerchant";


}
