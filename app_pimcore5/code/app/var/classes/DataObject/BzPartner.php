<?php 

/** 
* Generated at: 2019-09-03T12:03:25+02:00
* Inheritance: no
* Variants: no
* Changed by: root (2)
* IP: ::1
* Represents the partner who would license a product from Us.


Fields Summary: 
- name [input]
- object_type [input]
- solution_name [input]
- creditOnFile [input]
- isActive [checkbox]
- licenseType [input]
- licenseExpiryDate [date]
*/ 

namespace Pimcore\Model\DataObject;



/**
* @method static \Pimcore\Model\DataObject\BzPartner\Listing getByName ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\BzPartner\Listing getByObject_type ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\BzPartner\Listing getBySolution_name ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\BzPartner\Listing getByCreditOnFile ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\BzPartner\Listing getByIsActive ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\BzPartner\Listing getByLicenseType ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\BzPartner\Listing getByLicenseExpiryDate ($value, $limit = 0) 
*/

class BzPartner extends Concrete implements \Pimcore\Model\DataObject\DirtyIndicatorInterface {



use \Pimcore\Model\DataObject\Traits\DirtyIndicatorTrait;

protected $o_classId = "15";
protected $o_className = "BzPartner";
protected $name;
protected $object_type;
protected $solution_name;
protected $creditOnFile;
protected $isActive;
protected $licenseType;
protected $licenseExpiryDate;


/**
* @param array $values
* @return \Pimcore\Model\DataObject\BzPartner
*/
public static function create($values = array()) {
	$object = new static();
	$object->setValues($values);
	return $object;
}

/**
* Get name - Name
* @return string
*/
public function getName () {
	$preValue = $this->preGetValue("name"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->name;
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set name - Name
* @param string $name
* @return \Pimcore\Model\DataObject\BzPartner
*/
public function setName ($name) {
	$fd = $this->getClass()->getFieldDefinition("name");
	$this->name = $name;
	return $this;
}

/**
* Get object_type - Object Type
* @return string
*/
public function getObject_type () {
	$preValue = $this->preGetValue("object_type"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->object_type;
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set object_type - Object Type
* @param string $object_type
* @return \Pimcore\Model\DataObject\BzPartner
*/
public function setObject_type ($object_type) {
	$fd = $this->getClass()->getFieldDefinition("object_type");
	$this->object_type = $object_type;
	return $this;
}

/**
* Get solution_name - Solution Name
* @return string
*/
public function getSolution_name () {
	$preValue = $this->preGetValue("solution_name"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->solution_name;
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set solution_name - Solution Name
* @param string $solution_name
* @return \Pimcore\Model\DataObject\BzPartner
*/
public function setSolution_name ($solution_name) {
	$fd = $this->getClass()->getFieldDefinition("solution_name");
	$this->solution_name = $solution_name;
	return $this;
}

/**
* Get creditOnFile - Credit On File
* @return string
*/
public function getCreditOnFile () {
	$preValue = $this->preGetValue("creditOnFile"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->creditOnFile;
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set creditOnFile - Credit On File
* @param string $creditOnFile
* @return \Pimcore\Model\DataObject\BzPartner
*/
public function setCreditOnFile ($creditOnFile) {
	$fd = $this->getClass()->getFieldDefinition("creditOnFile");
	$this->creditOnFile = $creditOnFile;
	return $this;
}

/**
* Get isActive - isActive
* @return boolean
*/
public function getIsActive () {
	$preValue = $this->preGetValue("isActive"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->isActive;
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set isActive - isActive
* @param boolean $isActive
* @return \Pimcore\Model\DataObject\BzPartner
*/
public function setIsActive ($isActive) {
	$fd = $this->getClass()->getFieldDefinition("isActive");
	$this->isActive = $isActive;
	return $this;
}

/**
* Get licenseType - License Type
* @return string
*/
public function getLicenseType () {
	$preValue = $this->preGetValue("licenseType"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->licenseType;
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set licenseType - License Type
* @param string $licenseType
* @return \Pimcore\Model\DataObject\BzPartner
*/
public function setLicenseType ($licenseType) {
	$fd = $this->getClass()->getFieldDefinition("licenseType");
	$this->licenseType = $licenseType;
	return $this;
}

/**
* Get licenseExpiryDate - License Expiry Date
* @return \Carbon\Carbon
*/
public function getLicenseExpiryDate () {
	$preValue = $this->preGetValue("licenseExpiryDate"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->licenseExpiryDate;
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set licenseExpiryDate - License Expiry Date
* @param \Carbon\Carbon $licenseExpiryDate
* @return \Pimcore\Model\DataObject\BzPartner
*/
public function setLicenseExpiryDate ($licenseExpiryDate) {
	$fd = $this->getClass()->getFieldDefinition("licenseExpiryDate");
	$this->licenseExpiryDate = $licenseExpiryDate;
	return $this;
}

protected static $_relationFields = array (
);

protected $lazyLoadedFields = array (
);

}

