<?php 

namespace Pimcore\Model\DataObject\BzSystem;

use Pimcore\Model\DataObject;

/**
 * @method DataObject\BzSystem current()
 * @method DataObject\BzSystem[] load()
 */

class Listing extends DataObject\Listing\Concrete {

protected $classId = "27";
protected $className = "BzSystem";


}
