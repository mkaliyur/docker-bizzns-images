<?php 

namespace Pimcore\Model\DataObject\BzMenuItem;

use Pimcore\Model\DataObject;

/**
 * @method DataObject\BzMenuItem current()
 * @method DataObject\BzMenuItem[] load()
 */

class Listing extends DataObject\Listing\Concrete {

protected $classId = "20";
protected $className = "BzMenuItem";


}
