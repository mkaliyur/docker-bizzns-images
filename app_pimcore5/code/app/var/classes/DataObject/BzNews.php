<?php 

/** 
* Generated at: 2019-08-30T11:38:01+02:00
* Inheritance: no
* Variants: no
* Changed by: root (2)
* IP: ::1


Fields Summary: 
- inherited_from [manyToOneRelation]
- market_area_href [manyToOneRelation]
- domain_href [manyToOneRelation]
- merchant_href [manyToOneRelation]
- slug [input]
- title [input]
- object_type [input]
- shortText [textarea]
- text [wysiwyg]
- headline [input]
- topnews [checkbox]
- solution_name [input]
- name [input]
- tag_cloud [input]
- type_display_name [input]
- date [datetime]
- image_1 [image]
- image_2 [image]
- image_3 [image]
- categories [manyToManyRelation]
- external_media [fieldcollections]
- listing_inherits_from [manyToOneRelation]
- deal_inherits_from [manyToOneRelation]
- event_inherits_from [manyToOneRelation]
- audio_inherits_from [manyToOneRelation]
- video_inherits_from [manyToOneRelation]
- article_inherits_from [manyToOneRelation]
- news_inherits_from [manyToOneRelation]
*/ 

namespace Pimcore\Model\DataObject;



/**
* @method static \Pimcore\Model\DataObject\BzNews\Listing getByInherited_from ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\BzNews\Listing getByMarket_area_href ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\BzNews\Listing getByDomain_href ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\BzNews\Listing getByMerchant_href ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\BzNews\Listing getBySlug ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\BzNews\Listing getByTitle ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\BzNews\Listing getByObject_type ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\BzNews\Listing getByShortText ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\BzNews\Listing getByText ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\BzNews\Listing getByHeadline ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\BzNews\Listing getByTopnews ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\BzNews\Listing getBySolution_name ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\BzNews\Listing getByName ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\BzNews\Listing getByTag_cloud ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\BzNews\Listing getByType_display_name ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\BzNews\Listing getByDate ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\BzNews\Listing getByImage_1 ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\BzNews\Listing getByImage_2 ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\BzNews\Listing getByImage_3 ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\BzNews\Listing getByCategories ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\BzNews\Listing getByExternal_media ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\BzNews\Listing getByListing_inherits_from ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\BzNews\Listing getByDeal_inherits_from ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\BzNews\Listing getByEvent_inherits_from ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\BzNews\Listing getByAudio_inherits_from ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\BzNews\Listing getByVideo_inherits_from ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\BzNews\Listing getByArticle_inherits_from ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\BzNews\Listing getByNews_inherits_from ($value, $limit = 0) 
*/

class BzNews extends \Website\Model\DataObject\BzConcreteNews implements \Pimcore\Model\DataObject\DirtyIndicatorInterface {



use \Pimcore\Model\DataObject\Traits\DirtyIndicatorTrait;

protected $o_classId = "41";
protected $o_className = "BzNews";
protected $inherited_from;
protected $market_area_href;
protected $domain_href;
protected $merchant_href;
protected $slug;
protected $title;
protected $object_type;
protected $shortText;
protected $text;
protected $headline;
protected $topnews;
protected $solution_name;
protected $name;
protected $tag_cloud;
protected $type_display_name;
protected $date;
protected $image_1;
protected $image_2;
protected $image_3;
protected $categories;
protected $external_media;
protected $listing_inherits_from;
protected $deal_inherits_from;
protected $event_inherits_from;
protected $audio_inherits_from;
protected $video_inherits_from;
protected $article_inherits_from;
protected $news_inherits_from;


/**
* @param array $values
* @return \Pimcore\Model\DataObject\BzNews
*/
public static function create($values = array()) {
	$object = new static();
	$object->setValues($values);
	return $object;
}

/**
* Get inherited_from - Inherited From BzNews
* @return \Pimcore\Model\DataObject\BzNews
*/
public function getInherited_from () {
	$preValue = $this->preGetValue("inherited_from"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->getClass()->getFieldDefinition("inherited_from")->preGetData($this);
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set inherited_from - Inherited From BzNews
* @param \Pimcore\Model\DataObject\BzNews $inherited_from
* @return \Pimcore\Model\DataObject\BzNews
*/
public function setInherited_from ($inherited_from) {
	$fd = $this->getClass()->getFieldDefinition("inherited_from");
	$currentData = $this->getInherited_from();
	$isEqual = $fd->isEqual($currentData, $inherited_from);
	if (!$isEqual) {
		$this->markFieldDirty("inherited_from", true);
	}
	$this->inherited_from = $fd->preSetData($this, $inherited_from);
	return $this;
}

/**
* Get market_area_href - market_area_href
* @return \Pimcore\Model\DataObject\BzMarketArea
*/
public function getMarket_area_href () {
	$preValue = $this->preGetValue("market_area_href"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->getClass()->getFieldDefinition("market_area_href")->preGetData($this);
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set market_area_href - market_area_href
* @param \Pimcore\Model\DataObject\BzMarketArea $market_area_href
* @return \Pimcore\Model\DataObject\BzNews
*/
public function setMarket_area_href ($market_area_href) {
	$fd = $this->getClass()->getFieldDefinition("market_area_href");
	$currentData = $this->getMarket_area_href();
	$isEqual = $fd->isEqual($currentData, $market_area_href);
	if (!$isEqual) {
		$this->markFieldDirty("market_area_href", true);
	}
	$this->market_area_href = $fd->preSetData($this, $market_area_href);
	return $this;
}

/**
* Get domain_href - Domain Href
* @return \Pimcore\Model\DataObject\BzDomain
*/
public function getDomain_href () {
	$preValue = $this->preGetValue("domain_href"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->getClass()->getFieldDefinition("domain_href")->preGetData($this);
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set domain_href - Domain Href
* @param \Pimcore\Model\DataObject\BzDomain $domain_href
* @return \Pimcore\Model\DataObject\BzNews
*/
public function setDomain_href ($domain_href) {
	$fd = $this->getClass()->getFieldDefinition("domain_href");
	$currentData = $this->getDomain_href();
	$isEqual = $fd->isEqual($currentData, $domain_href);
	if (!$isEqual) {
		$this->markFieldDirty("domain_href", true);
	}
	$this->domain_href = $fd->preSetData($this, $domain_href);
	return $this;
}

/**
* Get merchant_href - Merchant Href
* @return \Pimcore\Model\DataObject\BzMerchant
*/
public function getMerchant_href () {
	$preValue = $this->preGetValue("merchant_href"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->getClass()->getFieldDefinition("merchant_href")->preGetData($this);
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set merchant_href - Merchant Href
* @param \Pimcore\Model\DataObject\BzMerchant $merchant_href
* @return \Pimcore\Model\DataObject\BzNews
*/
public function setMerchant_href ($merchant_href) {
	$fd = $this->getClass()->getFieldDefinition("merchant_href");
	$currentData = $this->getMerchant_href();
	$isEqual = $fd->isEqual($currentData, $merchant_href);
	if (!$isEqual) {
		$this->markFieldDirty("merchant_href", true);
	}
	$this->merchant_href = $fd->preSetData($this, $merchant_href);
	return $this;
}

/**
* Get slug - Slug
* @return string
*/
public function getSlug () {
	$preValue = $this->preGetValue("slug"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->slug;
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set slug - Slug
* @param string $slug
* @return \Pimcore\Model\DataObject\BzNews
*/
public function setSlug ($slug) {
	$fd = $this->getClass()->getFieldDefinition("slug");
	$this->slug = $slug;
	return $this;
}

/**
* Get title - Title
* @return string
*/
public function getTitle () {
	$preValue = $this->preGetValue("title"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->title;
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set title - Title
* @param string $title
* @return \Pimcore\Model\DataObject\BzNews
*/
public function setTitle ($title) {
	$fd = $this->getClass()->getFieldDefinition("title");
	$this->title = $title;
	return $this;
}

/**
* Get object_type - Object Type
* @return string
*/
public function getObject_type () {
	$preValue = $this->preGetValue("object_type"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->object_type;
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set object_type - Object Type
* @param string $object_type
* @return \Pimcore\Model\DataObject\BzNews
*/
public function setObject_type ($object_type) {
	$fd = $this->getClass()->getFieldDefinition("object_type");
	$this->object_type = $object_type;
	return $this;
}

/**
* Get shortText - Short Text
* @return string
*/
public function getShortText () {
	$preValue = $this->preGetValue("shortText"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->shortText;
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set shortText - Short Text
* @param string $shortText
* @return \Pimcore\Model\DataObject\BzNews
*/
public function setShortText ($shortText) {
	$fd = $this->getClass()->getFieldDefinition("shortText");
	$this->shortText = $shortText;
	return $this;
}

/**
* Get text - Text
* @return string
*/
public function getText () {
	$preValue = $this->preGetValue("text"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->getClass()->getFieldDefinition("text")->preGetData($this);
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set text - Text
* @param string $text
* @return \Pimcore\Model\DataObject\BzNews
*/
public function setText ($text) {
	$fd = $this->getClass()->getFieldDefinition("text");
	$this->text = $text;
	return $this;
}

/**
* Get headline - headline
* @return string
*/
public function getHeadline () {
	$preValue = $this->preGetValue("headline"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->headline;
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set headline - headline
* @param string $headline
* @return \Pimcore\Model\DataObject\BzNews
*/
public function setHeadline ($headline) {
	$fd = $this->getClass()->getFieldDefinition("headline");
	$this->headline = $headline;
	return $this;
}

/**
* Get topnews - topnews
* @return boolean
*/
public function getTopnews () {
	$preValue = $this->preGetValue("topnews"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->topnews;
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set topnews - topnews
* @param boolean $topnews
* @return \Pimcore\Model\DataObject\BzNews
*/
public function setTopnews ($topnews) {
	$fd = $this->getClass()->getFieldDefinition("topnews");
	$this->topnews = $topnews;
	return $this;
}

/**
* Get solution_name - Solution Name
* @return string
*/
public function getSolution_name () {
	$preValue = $this->preGetValue("solution_name"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->solution_name;
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set solution_name - Solution Name
* @param string $solution_name
* @return \Pimcore\Model\DataObject\BzNews
*/
public function setSolution_name ($solution_name) {
	$fd = $this->getClass()->getFieldDefinition("solution_name");
	$this->solution_name = $solution_name;
	return $this;
}

/**
* Get name - Name
* @return string
*/
public function getName () {
	$preValue = $this->preGetValue("name"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->name;
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set name - Name
* @param string $name
* @return \Pimcore\Model\DataObject\BzNews
*/
public function setName ($name) {
	$fd = $this->getClass()->getFieldDefinition("name");
	$this->name = $name;
	return $this;
}

/**
* Get tag_cloud - tag_cloud
* @return string
*/
public function getTag_cloud () {
	$preValue = $this->preGetValue("tag_cloud"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->tag_cloud;
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set tag_cloud - tag_cloud
* @param string $tag_cloud
* @return \Pimcore\Model\DataObject\BzNews
*/
public function setTag_cloud ($tag_cloud) {
	$fd = $this->getClass()->getFieldDefinition("tag_cloud");
	$this->tag_cloud = $tag_cloud;
	return $this;
}

/**
* Get type_display_name - news
* @return string
*/
public function getType_display_name () {
	$preValue = $this->preGetValue("type_display_name"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->type_display_name;
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set type_display_name - news
* @param string $type_display_name
* @return \Pimcore\Model\DataObject\BzNews
*/
public function setType_display_name ($type_display_name) {
	$fd = $this->getClass()->getFieldDefinition("type_display_name");
	$this->type_display_name = $type_display_name;
	return $this;
}

/**
* Get date - Date
* @return \Carbon\Carbon
*/
public function getDate () {
	$preValue = $this->preGetValue("date"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->date;
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set date - Date
* @param \Carbon\Carbon $date
* @return \Pimcore\Model\DataObject\BzNews
*/
public function setDate ($date) {
	$fd = $this->getClass()->getFieldDefinition("date");
	$this->date = $date;
	return $this;
}

/**
* Get image_1 - Image
* @return \Pimcore\Model\Asset\Image
*/
public function getImage_1 () {
	$preValue = $this->preGetValue("image_1"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->image_1;
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set image_1 - Image
* @param \Pimcore\Model\Asset\Image $image_1
* @return \Pimcore\Model\DataObject\BzNews
*/
public function setImage_1 ($image_1) {
	$fd = $this->getClass()->getFieldDefinition("image_1");
	$this->image_1 = $image_1;
	return $this;
}

/**
* Get image_2 - Image
* @return \Pimcore\Model\Asset\Image
*/
public function getImage_2 () {
	$preValue = $this->preGetValue("image_2"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->image_2;
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set image_2 - Image
* @param \Pimcore\Model\Asset\Image $image_2
* @return \Pimcore\Model\DataObject\BzNews
*/
public function setImage_2 ($image_2) {
	$fd = $this->getClass()->getFieldDefinition("image_2");
	$this->image_2 = $image_2;
	return $this;
}

/**
* Get image_3 - Image
* @return \Pimcore\Model\Asset\Image
*/
public function getImage_3 () {
	$preValue = $this->preGetValue("image_3"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->image_3;
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set image_3 - Image
* @param \Pimcore\Model\Asset\Image $image_3
* @return \Pimcore\Model\DataObject\BzNews
*/
public function setImage_3 ($image_3) {
	$fd = $this->getClass()->getFieldDefinition("image_3");
	$this->image_3 = $image_3;
	return $this;
}

/**
* Get categories - categories
* @return \Pimcore\Model\DataObject\BzNewsCategory[]
*/
public function getCategories () {
	$preValue = $this->preGetValue("categories"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->getClass()->getFieldDefinition("categories")->preGetData($this);
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set categories - categories
* @param \Pimcore\Model\DataObject\BzNewsCategory[] $categories
* @return \Pimcore\Model\DataObject\BzNews
*/
public function setCategories ($categories) {
	$fd = $this->getClass()->getFieldDefinition("categories");
	$currentData = $this->getCategories();
	$isEqual = $fd->isEqual($currentData, $categories);
	if (!$isEqual) {
		$this->markFieldDirty("categories", true);
	}
	$this->categories = $fd->preSetData($this, $categories);
	return $this;
}

/**
* @return \Pimcore\Model\DataObject\Fieldcollection
*/
public function getExternal_media () {
	$preValue = $this->preGetValue("external_media"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { return $preValue;}
	$data = $this->getClass()->getFieldDefinition("external_media")->preGetData($this);
	 return $data;
}

/**
* Set external_media - External Media
* @param \Pimcore\Model\DataObject\Fieldcollection $external_media
* @return \Pimcore\Model\DataObject\BzNews
*/
public function setExternal_media ($external_media) {
	$fd = $this->getClass()->getFieldDefinition("external_media");
	$this->external_media = $fd->preSetData($this, $external_media);
	return $this;
}

/**
* Get listing_inherits_from - Listing Inherits From
* @return \Pimcore\Model\DataObject\BzListing2
*/
public function getListing_inherits_from () {
	$preValue = $this->preGetValue("listing_inherits_from"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->getClass()->getFieldDefinition("listing_inherits_from")->preGetData($this);
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set listing_inherits_from - Listing Inherits From
* @param \Pimcore\Model\DataObject\BzListing2 $listing_inherits_from
* @return \Pimcore\Model\DataObject\BzNews
*/
public function setListing_inherits_from ($listing_inherits_from) {
	$fd = $this->getClass()->getFieldDefinition("listing_inherits_from");
	$currentData = $this->getListing_inherits_from();
	$isEqual = $fd->isEqual($currentData, $listing_inherits_from);
	if (!$isEqual) {
		$this->markFieldDirty("listing_inherits_from", true);
	}
	$this->listing_inherits_from = $fd->preSetData($this, $listing_inherits_from);
	return $this;
}

/**
* Get deal_inherits_from - Deal Inherits From
* @return \Pimcore\Model\DataObject\BzDeals
*/
public function getDeal_inherits_from () {
	$preValue = $this->preGetValue("deal_inherits_from"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->getClass()->getFieldDefinition("deal_inherits_from")->preGetData($this);
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set deal_inherits_from - Deal Inherits From
* @param \Pimcore\Model\DataObject\BzDeals $deal_inherits_from
* @return \Pimcore\Model\DataObject\BzNews
*/
public function setDeal_inherits_from ($deal_inherits_from) {
	$fd = $this->getClass()->getFieldDefinition("deal_inherits_from");
	$currentData = $this->getDeal_inherits_from();
	$isEqual = $fd->isEqual($currentData, $deal_inherits_from);
	if (!$isEqual) {
		$this->markFieldDirty("deal_inherits_from", true);
	}
	$this->deal_inherits_from = $fd->preSetData($this, $deal_inherits_from);
	return $this;
}

/**
* Get event_inherits_from - event_inherits_from
* @return \Pimcore\Model\DataObject\BzEvents
*/
public function getEvent_inherits_from () {
	$preValue = $this->preGetValue("event_inherits_from"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->getClass()->getFieldDefinition("event_inherits_from")->preGetData($this);
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set event_inherits_from - event_inherits_from
* @param \Pimcore\Model\DataObject\BzEvents $event_inherits_from
* @return \Pimcore\Model\DataObject\BzNews
*/
public function setEvent_inherits_from ($event_inherits_from) {
	$fd = $this->getClass()->getFieldDefinition("event_inherits_from");
	$currentData = $this->getEvent_inherits_from();
	$isEqual = $fd->isEqual($currentData, $event_inherits_from);
	if (!$isEqual) {
		$this->markFieldDirty("event_inherits_from", true);
	}
	$this->event_inherits_from = $fd->preSetData($this, $event_inherits_from);
	return $this;
}

/**
* Get audio_inherits_from - audio_inherits_from
* @return \Pimcore\Model\DataObject\BzAudio
*/
public function getAudio_inherits_from () {
	$preValue = $this->preGetValue("audio_inherits_from"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->getClass()->getFieldDefinition("audio_inherits_from")->preGetData($this);
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set audio_inherits_from - audio_inherits_from
* @param \Pimcore\Model\DataObject\BzAudio $audio_inherits_from
* @return \Pimcore\Model\DataObject\BzNews
*/
public function setAudio_inherits_from ($audio_inherits_from) {
	$fd = $this->getClass()->getFieldDefinition("audio_inherits_from");
	$currentData = $this->getAudio_inherits_from();
	$isEqual = $fd->isEqual($currentData, $audio_inherits_from);
	if (!$isEqual) {
		$this->markFieldDirty("audio_inherits_from", true);
	}
	$this->audio_inherits_from = $fd->preSetData($this, $audio_inherits_from);
	return $this;
}

/**
* Get video_inherits_from - video_inherits_from
* @return \Pimcore\Model\DataObject\BzVideo
*/
public function getVideo_inherits_from () {
	$preValue = $this->preGetValue("video_inherits_from"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->getClass()->getFieldDefinition("video_inherits_from")->preGetData($this);
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set video_inherits_from - video_inherits_from
* @param \Pimcore\Model\DataObject\BzVideo $video_inherits_from
* @return \Pimcore\Model\DataObject\BzNews
*/
public function setVideo_inherits_from ($video_inherits_from) {
	$fd = $this->getClass()->getFieldDefinition("video_inherits_from");
	$currentData = $this->getVideo_inherits_from();
	$isEqual = $fd->isEqual($currentData, $video_inherits_from);
	if (!$isEqual) {
		$this->markFieldDirty("video_inherits_from", true);
	}
	$this->video_inherits_from = $fd->preSetData($this, $video_inherits_from);
	return $this;
}

/**
* Get article_inherits_from - Article Inherits From
* @return \Pimcore\Model\DataObject\BzBlogArticle
*/
public function getArticle_inherits_from () {
	$preValue = $this->preGetValue("article_inherits_from"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->getClass()->getFieldDefinition("article_inherits_from")->preGetData($this);
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set article_inherits_from - Article Inherits From
* @param \Pimcore\Model\DataObject\BzBlogArticle $article_inherits_from
* @return \Pimcore\Model\DataObject\BzNews
*/
public function setArticle_inherits_from ($article_inherits_from) {
	$fd = $this->getClass()->getFieldDefinition("article_inherits_from");
	$currentData = $this->getArticle_inherits_from();
	$isEqual = $fd->isEqual($currentData, $article_inherits_from);
	if (!$isEqual) {
		$this->markFieldDirty("article_inherits_from", true);
	}
	$this->article_inherits_from = $fd->preSetData($this, $article_inherits_from);
	return $this;
}

/**
* Get news_inherits_from - News Inherits From
* @return \Pimcore\Model\DataObject\BzNews
*/
public function getNews_inherits_from () {
	$preValue = $this->preGetValue("news_inherits_from"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->getClass()->getFieldDefinition("news_inherits_from")->preGetData($this);
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set news_inherits_from - News Inherits From
* @param \Pimcore\Model\DataObject\BzNews $news_inherits_from
* @return \Pimcore\Model\DataObject\BzNews
*/
public function setNews_inherits_from ($news_inherits_from) {
	$fd = $this->getClass()->getFieldDefinition("news_inherits_from");
	$currentData = $this->getNews_inherits_from();
	$isEqual = $fd->isEqual($currentData, $news_inherits_from);
	if (!$isEqual) {
		$this->markFieldDirty("news_inherits_from", true);
	}
	$this->news_inherits_from = $fd->preSetData($this, $news_inherits_from);
	return $this;
}

protected static $_relationFields = array (
  'inherited_from' => 
  array (
    'type' => 'manyToOneRelation',
  ),
  'market_area_href' => 
  array (
    'type' => 'manyToOneRelation',
  ),
  'domain_href' => 
  array (
    'type' => 'manyToOneRelation',
  ),
  'merchant_href' => 
  array (
    'type' => 'manyToOneRelation',
  ),
  'categories' => 
  array (
    'type' => 'manyToManyRelation',
  ),
  'listing_inherits_from' => 
  array (
    'type' => 'manyToOneRelation',
  ),
  'deal_inherits_from' => 
  array (
    'type' => 'manyToOneRelation',
  ),
  'event_inherits_from' => 
  array (
    'type' => 'manyToOneRelation',
  ),
  'audio_inherits_from' => 
  array (
    'type' => 'manyToOneRelation',
  ),
  'video_inherits_from' => 
  array (
    'type' => 'manyToOneRelation',
  ),
  'article_inherits_from' => 
  array (
    'type' => 'manyToOneRelation',
  ),
  'news_inherits_from' => 
  array (
    'type' => 'manyToOneRelation',
  ),
);

protected $lazyLoadedFields = array (
);

}

