<?php 

return [
    4 => [
        "id" => 4,
        "name" => "demo_login",
        "pattern" => "@^/(de|en)/secure/login\$@",
        "reverse" => "/%_locale/secure/login",
        "module" => "AppBundle",
        "controller" => "@AppBundle\\Controller\\SecureController",
        "action" => "login",
        "variables" => "_locale",
        "defaults" => "_locale=en",
        "siteId" => [

        ],
        "priority" => 0,
        "legacy" => FALSE,
        "creationDate" => 1490874634,
        "modificationDate" => 1521027176
    ],
    5 => [
        "id" => 5,
        "name" => "demo_logout",
        "pattern" => "@^/(de|en)/secure/logout\$@",
        "reverse" => "/%_locale/secure/logout",
        "module" => "AppBundle",
        "controller" => "@AppBundle\\Controller\\SecureController",
        "action" => "logout",
        "variables" => "_locale",
        "defaults" => "_locale=en",
        "siteId" => [

        ],
        "priority" => 0,
        "legacy" => FALSE,
        "creationDate" => 1490874774,
        "modificationDate" => 1521027193
    ],
    6 => [
        "id" => 6,
        "name" => "listings_by_id",
        "pattern" => "@/mysoretravels/listings/([\\d]+)@",
        "reverse" => "/mysoretravels/listings/%listingid",
        "module" => "AppBundle",
        "controller" => "@AppBundle\\Controller\\ListingController",
        "action" => "detail",
        "variables" => "listingid",
        "defaults" => "",
        "siteId" => [

        ],
        "priority" => 1,
        "legacy" => FALSE,
        "creationDate" => 1519823788,
        "modificationDate" => 1521008971
    ],
    7 => [
        "id" => 7,
        "name" => "blog-by-id",
        "pattern" => "@/mysoretravels/blog/([\\d]+)@",
        "reverse" => "/mysoretravels/blog/%blogid",
        "module" => "AppBundle",
        "controller" => "@AppBundle\\Controller\\BlogController",
        "action" => "detailJson",
        "variables" => "blogid",
        "defaults" => "",
        "siteId" => [

        ],
        "priority" => 1,
        "legacy" => FALSE,
        "creationDate" => 1521009039,
        "modificationDate" => 1521027533
    ],
    8 => [
        "id" => 8,
        "name" => "news-by-id",
        "pattern" => "@/mysoretravels/news/([\\d]+)@",
        "reverse" => "/mysoretravels/news/%newsid",
        "module" => "AppBundle",
        "controller" => "@AppBundle\\Controller\\NewsController",
        "action" => "detail",
        "variables" => "newsid",
        "defaults" => "",
        "siteId" => [

        ],
        "priority" => 0,
        "legacy" => FALSE,
        "creationDate" => 1521009116,
        "modificationDate" => 1521009167
    ],
    9 => [
        "id" => 9,
        "name" => "deal-by-id",
        "pattern" => "@/mysoretravels/deal/([\\d]+)@",
        "reverse" => "/mysoretravels/deals/%dealid",
        "module" => "AppBundle",
        "controller" => "@AppBundle\\Controller\\DealController",
        "action" => "dealDetail",
        "variables" => "dealid",
        "defaults" => "",
        "siteId" => [

        ],
        "priority" => 0,
        "legacy" => FALSE,
        "creationDate" => 1521011084,
        "modificationDate" => 1521011200
    ],
    10 => [
        "id" => 10,
        "name" => "video-by-id",
        "pattern" => "@/mysoretravels/video/([\\d]+)@",
        "reverse" => "/mysoretravels/video/%videoid",
        "module" => "AppBundle",
        "controller" => "@AppBundle\\Controller\\BlogController",
        "action" => "detailVideoJson",
        "variables" => "videoid",
        "defaults" => "",
        "siteId" => [

        ],
        "priority" => 0,
        "legacy" => FALSE,
        "creationDate" => 1521011212,
        "modificationDate" => 1521027496
    ],
    11 => [
        "id" => 11,
        "name" => "event-by-id",
        "pattern" => "@/mysoretravels/event/([\\d]+)@",
        "reverse" => "/mysoretravels/event/%eventid",
        "module" => "AppBundle",
        "controller" => "@AppBundle\\Controller\\EventController",
        "action" => "eventDetail",
        "variables" => "eventid",
        "defaults" => "",
        "siteId" => [

        ],
        "priority" => 0,
        "legacy" => FALSE,
        "creationDate" => 1521015119,
        "modificationDate" => 1529917721
    ],
    12 => [
        "id" => 12,
        "name" => "audio-by-id",
        "pattern" => "@/mysoretravels/audios/([\\d]+)@",
        "reverse" => "/mysoretravels/audios/%audioid",
        "module" => "AppBundle",
        "controller" => "@AppBundle\\Controller\\BlogController",
        "action" => "detailAudioJson",
        "variables" => "audioid",
        "defaults" => "",
        "siteId" => [

        ],
        "priority" => 0,
        "legacy" => FALSE,
        "creationDate" => 1521015167,
        "modificationDate" => 1530863196
    ],
    13 => [
        "id" => 13,
        "name" => "deal-by-id",
        "pattern" => "@/mysore_travels/bzdeals/([\\d]+)@",
        "reverse" => "/mysore_travels/bzdeals/%dealid",
        "module" => "AppBundle",
        "controller" => "@AppBundle\\Controller\\DealController",
        "action" => "dealDetail",
        "variables" => "dealid",
        "defaults" => "",
        "siteId" => [

        ],
        "priority" => 0,
        "legacy" => FALSE,
        "creationDate" => 1524657116,
        "modificationDate" => 1553667016
    ],
    14 => [
        "id" => 14,
        "name" => "solutions",
        "pattern" => "@/system/domain/([\\w]+)/market/([\\w]+)/solutions@",
        "reverse" => "/system/domain/%domainname/market/%marketname/solutions",
        "module" => "AppBundle",
        "controller" => "@AppBundle\\Controller\\SolutionController",
        "action" => "solutionAdminPage",
        "variables" => "domainname,marketname",
        "defaults" => "",
        "siteId" => [

        ],
        "priority" => 0,
        "legacy" => FALSE,
        "creationDate" => 1525151243,
        "modificationDate" => 1527659902
    ],
    15 => [
        "id" => 15,
        "name" => "merchants",
        "pattern" => "@/system/solutions/([\\w]+)/merchant/([\\w]+)@",
        "reverse" => "/system/solutions/%solution_name/merchant/%merchant_name",
        "module" => "AppBundle",
        "controller" => "@AppBundle\\Controller\\MerchantController",
        "action" => "merchantAdminPage",
        "variables" => "solution_name,merchant_name",
        "defaults" => "",
        "siteId" => [

        ],
        "priority" => 0,
        "legacy" => FALSE,
        "creationDate" => 1525427058,
        "modificationDate" => 1527659927
    ],
    16 => [
        "id" => 16,
        "name" => "solution-instance-detail",
        "pattern" => "@/system/instances/([\\d]+)@",
        "reverse" => "/system/instances/%instanceid",
        "module" => "AppBundle",
        "controller" => "@AppBundle\\Controller\\SolutionInstanceController",
        "action" => "solutionInstanceDetail",
        "variables" => "instanceid",
        "defaults" => "",
        "siteId" => [

        ],
        "priority" => 0,
        "legacy" => FALSE,
        "creationDate" => 1528708711,
        "modificationDate" => 1529308732
    ],
    17 => [
        "id" => 17,
        "name" => "solution-instance-request",
        "pattern" => "@/system/instance/register@",
        "reverse" => "@/system/instance/register@",
        "module" => "AppBundle",
        "controller" => "@AppBundle\\Controller\\SolutionInstanceController",
        "action" => "registerNewSolutionInstance",
        "variables" => "",
        "defaults" => "",
        "siteId" => [

        ],
        "priority" => 0,
        "legacy" => FALSE,
        "creationDate" => 1529307969,
        "modificationDate" => 1529319563
    ],
    18 => [
        "id" => 18,
        "name" => "domaincreate",
        "pattern" => "@/system/domain/create@",
        "reverse" => "@/system/domain/create@",
        "module" => "AppBundle",
        "controller" => "@AppBundle\\Controller\\DomainMarketController",
        "action" => "newDomainCreate",
        "variables" => "",
        "defaults" => "",
        "siteId" => [

        ],
        "priority" => 0,
        "legacy" => FALSE,
        "creationDate" => 1529400713,
        "modificationDate" => 1529412334
    ],
    19 => [
        "id" => 19,
        "name" => "marketcreate",
        "pattern" => "@/system/market/create@",
        "reverse" => "@/system/market/create@",
        "module" => "AppBundle",
        "controller" => "@AppBundle\\Controller\\DomainMarketController",
        "action" => "newMarketCreate",
        "variables" => "",
        "defaults" => NULL,
        "siteId" => [

        ],
        "priority" => 0,
        "legacy" => FALSE,
        "creationDate" => 1529411723,
        "modificationDate" => 1529412340
    ],
    20 => [
        "id" => 20,
        "name" => "solutioncreate",
        "pattern" => "@/system/solution/create@",
        "reverse" => "@/system/solution/create@",
        "module" => "AppBundle",
        "controller" => "@AppBundle\\Controller\\SolutionController",
        "action" => "newSolutionCreate",
        "variables" => "",
        "defaults" => "",
        "siteId" => [

        ],
        "priority" => 0,
        "legacy" => FALSE,
        "creationDate" => 1529476373,
        "modificationDate" => 1529650006
    ],
    21 => [
        "id" => 21,
        "name" => "merchantcreate",
        "pattern" => "@/system/merchant/create@",
        "reverse" => "@/system/merchant/create@",
        "module" => "AppBundle",
        "controller" => "@AppBundle\\Controller\\MerchantController",
        "action" => "newMerchantCreate",
        "variables" => "",
        "defaults" => "",
        "siteId" => [

        ],
        "priority" => 0,
        "legacy" => FALSE,
        "creationDate" => 1529476388,
        "modificationDate" => 1529496265
    ],
    22 => [
        "id" => 22,
        "name" => "listings-by-id-builders",
        "pattern" => "@/mysore_builders/bzlisting2/([\\d]+)@",
        "reverse" => "/mysore_builders/bzlisting2/%listingid",
        "module" => "AppBundle",
        "controller" => "@AppBundle\\Controller\\ListingController",
        "action" => "detail",
        "variables" => "listingid",
        "defaults" => "",
        "siteId" => [

        ],
        "priority" => 1,
        "legacy" => FALSE,
        "creationDate" => 1529571898,
        "modificationDate" => 1542349192
    ],
    23 => [
        "id" => 23,
        "name" => "blog-by-id-builders",
        "pattern" => "@/mysore_builders/bzblogarticle/([\\d]+)@",
        "reverse" => "/mysore_builders/bzblogarticle/%blogid",
        "module" => "AppBundle",
        "controller" => "@AppBundle\\Controller\\BlogController",
        "action" => "detailJson",
        "variables" => "blogid",
        "defaults" => "",
        "siteId" => [

        ],
        "priority" => 0,
        "legacy" => FALSE,
        "creationDate" => 1529574563,
        "modificationDate" => 1542349199
    ],
    24 => [
        "id" => 24,
        "name" => "news-by-id-builders",
        "pattern" => "@/mysore_builders/bznews/([\\d]+)@",
        "reverse" => "/mysore_builders/bznews/%newsid",
        "module" => "AppBundle",
        "controller" => "@AppBundle\\Controller\\NewsController",
        "action" => "detail",
        "variables" => "newsid",
        "defaults" => NULL,
        "siteId" => [

        ],
        "priority" => 0,
        "legacy" => FALSE,
        "creationDate" => 1529574597,
        "modificationDate" => 1557213414
    ],
    25 => [
        "id" => 25,
        "name" => "deal-by-id-builders",
        "pattern" => "@/mysore_builders/bzdeals/([\\d]+)@",
        "reverse" => "/mysore_builders/bzdeals/%dealid",
        "module" => "AppBundle",
        "controller" => "@AppBundle\\Controller\\DealController",
        "action" => "dealDetail",
        "variables" => "dealid",
        "defaults" => "",
        "siteId" => [

        ],
        "priority" => 0,
        "legacy" => FALSE,
        "creationDate" => 1529574641,
        "modificationDate" => 1542349207
    ],
    26 => [
        "id" => 26,
        "name" => "video-by-id-builders",
        "pattern" => "@/mysore_builders/bzvideo/([\\d]+)@",
        "reverse" => "/mysore_builders/bzvideo/%videoid",
        "module" => "AppBundle",
        "controller" => "@AppBundle\\Controller\\BlogController",
        "action" => "detailVideoJson",
        "variables" => "videoid",
        "defaults" => "",
        "siteId" => [

        ],
        "priority" => 0,
        "legacy" => FALSE,
        "creationDate" => 1529574673,
        "modificationDate" => 1542349215
    ],
    27 => [
        "id" => 27,
        "name" => "event-by-id-builders",
        "pattern" => "@/mysore_builders/bzevents/([\\d]+)@",
        "reverse" => "/mysore_builders/bzevents/%eventid",
        "module" => "AppBundle",
        "controller" => "@AppBundle\\Controller\\EventController",
        "action" => "eventDetail",
        "variables" => "eventid",
        "defaults" => NULL,
        "siteId" => [

        ],
        "priority" => 0,
        "legacy" => FALSE,
        "creationDate" => 1529574715,
        "modificationDate" => 1542349221
    ],
    28 => [
        "id" => 28,
        "name" => "audio-by-id-builders",
        "pattern" => "@/mysore_builders/bzaudio/([\\d]+)@",
        "reverse" => "/mysore_builders/bzaudio/%audioid",
        "module" => "AppBundle",
        "controller" => "@AppBundle\\Controller\\BlogController",
        "action" => "detailAudioJson",
        "variables" => "audioid",
        "defaults" => "",
        "siteId" => [

        ],
        "priority" => 0,
        "legacy" => FALSE,
        "creationDate" => 1529574745,
        "modificationDate" => 1542349225
    ],
    29 => [
        "id" => 29,
        "name" => "solution-home",
        "pattern" => "@/system/([\\w]+)/([\\w]+)/index@",
        "reverse" => "/system/%domainname/%marketname/index",
        "module" => "AppBundle",
        "controller" => "@AppBundle\\Controller\\WebsiteController",
        "action" => "index",
        "variables" => "domainname,marketname",
        "defaults" => "",
        "siteId" => [

        ],
        "priority" => 0,
        "legacy" => FALSE,
        "creationDate" => 1531473067,
        "modificationDate" => 1531474079
    ],
    30 => [
        "id" => 30,
        "name" => "listing-builders",
        "pattern" => "@/mysore_builders/bzlistingbuilders/([\\d]+)@",
        "reverse" => "/mysore_builders/bzlistingbuilders/%listingid",
        "module" => "AppBundle",
        "controller" => "@AppBundle\\Controller\\BuildersListingController",
        "action" => "detail",
        "variables" => "listingid",
        "defaults" => NULL,
        "siteId" => [

        ],
        "priority" => 0,
        "legacy" => FALSE,
        "creationDate" => 1535442395,
        "modificationDate" => 1556623146
    ],
    31 => [
        "id" => 31,
        "name" => "reservables",
        "pattern" => "@/mysore_builders/bzreservables/([\\d]+)@",
        "reverse" => "/mysore_builders/bzreservables/%reservableid",
        "module" => "AppBundle",
        "controller" => "@AppBundle\\Controller\\EventController",
        "action" => "eventReservable",
        "variables" => "reservableid",
        "defaults" => "",
        "siteId" => [

        ],
        "priority" => 0,
        "legacy" => FALSE,
        "creationDate" => 1556622966,
        "modificationDate" => 1561376863
    ],
    32 => [
        "id" => 32,
        "name" => "website-generation",
        "pattern" => "@/system/websites@",
        "reverse" => "@/system/websites@",
        "module" => "AppBundle",
        "controller" => "@AppBundle\\Controller\\BzSystemController",
        "action" => "getSolutionList",
        "variables" => "",
        "defaults" => "",
        "siteId" => [

        ],
        "priority" => 0,
        "legacy" => FALSE,
        "creationDate" => 1565589769,
        "modificationDate" => 1566194855
    ],
    33 => [
        "id" => 33,
        "name" => "website-data",
        "pattern" => "@/system/data/([\\w]+)/([\\w]+)@",
        "reverse" => "@/system/data/%marketname/%domainname@",
        "module" => "AppBundle",
        "controller" => "@AppBundle\\Controller\\BzSystemController",
        "action" => "getWebsiteData",
        "variables" => "marketname,domainname",
        "defaults" => "",
        "siteId" => [

        ],
        "priority" => 0,
        "legacy" => FALSE,
        "creationDate" => 1566194824,
        "modificationDate" => 1566207565
    ]
];
